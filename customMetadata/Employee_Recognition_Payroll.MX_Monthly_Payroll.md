<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MX Monthly Payroll</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Auto_Gross_Percentage__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Currency__c</field>
        <value xsi:type="xsd:string">MXN</value>
    </values>
    <values>
        <field>Day_of_the_Month__c</field>
        <value xsi:type="xsd:string">1</value>
    </values>
    <values>
        <field>Email_Template_for_Reminder__c</field>
        <value xsi:type="xsd:string">General_Payroll_Reminder</value>
    </values>
    <values>
        <field>Email_Template_for_Report__c</field>
        <value xsi:type="xsd:string">General_Payroll_Notification</value>
    </values>
    <values>
        <field>No_Gross_Up_Possible__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Notes__c</field>
        <value xsi:type="xsd:string">Mexico</value>
    </values>
    <values>
        <field>Payroll_Reference__c</field>
        <value xsi:type="xsd:string">MX Monthly Payroll</value>
    </values>
    <values>
        <field>Recipients__c</field>
        <value xsi:type="xsd:string">cynthia.espinoza@experian.com,tyaga.pati@experian.com,manoj.gopu@experian.com,Shenai.kotecha@experian.com</value>
    </values>
    <values>
        <field>Report_Layout__c</field>
        <value xsi:type="xsd:string">Payroll_Report</value>
    </values>
    <values>
        <field>Report_Type__c</field>
        <value xsi:type="xsd:string">XLS</value>
    </values>
    <values>
        <field>Sandbox_Recipients__c</field>
        <value xsi:type="xsd:string">Shenai.Kotecha@experian.com</value>
    </values>
    <values>
        <field>Test_Case__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
