<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>EMEA</label>
    <protected>false</protected>
    <values>
        <field>Chatter_groups__c</field>
        <value xsi:type="xsd:string">All Experian
All EMEA</value>
    </values>
</CustomMetadata>
