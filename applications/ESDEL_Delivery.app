<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Spain Delivery Workstream - New Custom App for Delivery Team in Spain includes Accounts/Contact/Projects/Cases etc</description>
    <formFactors>Large</formFactors>
    <label>Delivery</label>
    <tab>standard-Chatter</tab>
    <tab>standard-UserProfile</tab>
    <tab>standard-CollaborationGroup</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Case</tab>
    <tab>ESDEL_Delivery_Project__c</tab>
    <tab>ESDEL_Delivery_Product__c</tab>
    <tab>ESDEL_Timecard__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Document</tab>
    <tab>Lesson_Learned__c</tab>
</CustomApplication>
