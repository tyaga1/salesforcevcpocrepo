<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Includes all the tabs and objects required to build new Business Process Templates</description>
    <formFactors>Large</formFactors>
    <label>Business Process Builder</label>
    <tab>Business_Process_Template__c</tab>
    <tab>Business_Process_Template_Item__c</tab>
    <tab>standard-report</tab>
    <tab>Lesson_Learned__c</tab>
</CustomApplication>
