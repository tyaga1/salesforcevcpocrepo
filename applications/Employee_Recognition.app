<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>One Experian Recognition</label>
    <tab>standard-Chatter</tab>
    <tab>standard-UserProfile</tab>
    <tab>standard-CollaborationGroup</tab>
    <tab>standard-File</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Nomination</tab>
    <tab>Nomination__c</tab>
    <tab>standard-Case</tab>
    <tab>Lesson_Learned__c</tab>
</CustomApplication>
