<!--
/**=====================================================================
 * Experian
 * Name: ManyThanksAction
 * Description: VF Action to allow saying thanks to multiple people in the 
                process. Saves a lot of time if posting for a group of people.
 * Created Date: 06 Feb 2017
 * Created By: Arun Jayaseelan - UC Innovation
 *
 * Date Modified      Modified By           Description of the update
 * 18th Jul, 2017     Manish Singh          Case #02427918  Changed the query filter to exclude customer community users from the recipient 
 * =====================================================================*/
 -->
<apex:component controller="ManyThanksController" access="global" allowDML="true" >
  <!-- Including the jQuery library to prevent issues accessing the CDN version - This is v 1.10.2 -->
  <apex:includeScript value="{!URLFOR($Resource.ManyThanksComponents,'jQuery/jquery.min.js')}"/>
  <script>
  var $j = jQuery.noConflict();
  </script>
  
  <!-- Select Box JS/CSS -->
  <apex:includeScript value="{!URLFOR($Resource.ManyThanksComponents, 'SelectBox/select2.min.js')}" />
  <apex:stylesheet value="{!URLFOR($Resource.ManyThanksComponents, 'SelectBox/select2.css')}" />
  <!-- Chosen Extension JS/CSS -->
  <apex:includeScript value="{!URLFOR($Resource.ManyThanksComponents,'Chosen/chosen.jquery.min.js')}" />
  <apex:stylesheet value="{!URLFOR($Resource.ManyThanksComponents,'Chosen/chosen.min.css')}" />
  <!-- Image Select Chosen Ext JS/CSS -->
  <apex:includeScript value="{!URLFOR($Resource.ManyThanksComponents,'ChosenImageSelect/ImageSelect.jquery.js')}" />
  <apex:stylesheet value="{!URLFOR($Resource.ManyThanksComponents,'ChosenImageSelect/ImageSelect.css')}" />
  <!-- Autosize textareas -->
  <apex:includeScript value="{!URLFOR($Resource.ManyThanksComponents,'Autosize/autosize.min.js')}" />

  <apex:includeScript value="/canvas/sdk/js/publisher.js" />
  <script>
  function refreshFeed() {
    resizeCanvas();
    Sfdc.canvas.publisher.publish({name: 'publisher.refresh', payload: {feed: true}});
  }
  function resizeCanvas() {
    Sfdc.canvas.publisher.resize();
  }
  Sfdc.canvas(function() {
    resizeCanvas();
  });
  </script>
  <style>
    body {
      margin: 0px !important;
    }
    * {
      padding: 0;
      margin: 0;
      border: 0;
    }
    .blended_grid {
      display: block;
      width: 100%;
      overflow: auto;
      margin: 0 auto;
    }
    .pageHeader {
      float: left;
      clear: none;
      min-height: 50px;
      width: 100%;
      border-bottom: 1px solid #c7ccce;
      padding-bottom: 7px;
    }
    .pageLeftMenu {
      float: left;
      clear: none;
      //height: 250px;
      height:auto;
      max-width: 340px; //hay 190 original
      
      border-right: 1px solid #c7ccce;
      padding-top: 7px;
      padding-right: 7px;
      width:50%;        //hay
    }
    .pageContent {
      float: left;
      clear: none;
      height: auto;
      margin: 5px;
      padding-top: 7px;
      width:50%;
    }
    .pageFooter {
      border-top: 1px solid #c7ccce;
      float: left;
      clear: none;
      //height: 80px;
      height:auto;
      width: 100%;
      padding-top: 7px;
    }
    .thanksMessage {
      min-width: 290px; 
      width:100%;
      border:0px;
    }
    
     @media screen and (max-width: 680px) {
        .pageLeftMenu, .pageContent, .pageFooter  {
            width:95%;
        }
    }
  </style>
  <apex:form >
    <apex:outputPanel styleClass="blended_grid" id="wholePage">
      <div class="pageHeader">
        <apex:pageMessages />
        <apex:outputPanel id="checkStatus">
          <apex:repeat value="{!userIdMap}" var="k">
            <c:AutoCompleteV2Filter SObject="User"
              labelField="Name"
              valueField="Id"
              queryFilter="IsActive = true AND License__c != \'Customer Community Plus Login\' AND License__c != \'Customer Community Plus\' AND Id != \'{!$User.Id}\'"
              targetField="{!userIdMap[k]}"
              placeholderText="{!$Label.ManyThanks_Person_Placeholder}" 
              importJquery="false"
              syncManualEntry="false"
              allowClear="true"
              minLength="3" 
              style="width:190px"
            />
            <apex:actionStatus id="addAnotherStatus">
              <apex:facet name="start"><apex:image value="/img/loading.gif" /></apex:facet>
              <apex:facet name="stop"><apex:commandButton status="addAnotherStatus" value=" {!$Label.ManyThanks_Add} + " action="{!addAnother}"  reRender="checkStatus" rendered="{!k==UserIdMapSize}" oncomplete="resizeCanvas();" /></apex:facet>
            </apex:actionStatus>
          </apex:repeat>
        </apex:outputPanel>
      </div>
      <div class="pageLeftMenu">
        <apex:inputHidden value="{!badgeId}" id="customBadgeId" />
        <select class="my-select" id="customSelectBadge" data-placeholder="{!$Label.ManyThanks_Available_Badges}" >
          <apex:repeat value="{!allBadges}" var="b">
            <option data-img-src="{!b.ImageUrl}" value="{!b.Id}" data-badge-desc="{!b.Description}">{!b.Name}</option>
          </apex:repeat>
        </select>
        <br />
        <br />
        <div id="badgeImageDiv" style="width:100%; text-align:center;" >
          <img src="/s.gif" id="badgeImage" height="75" width="75" alt="" title="" />
          <p id="badgeImageDesc"></p>
        </div>
        <script>
          function addSelectedBadge(bId) {
            document.getElementById('{!$Component.customBadgeId}').value = bId;
            var holdingImg = $j('#customSelectBadge').find(':selected').attr('data-img-src');
            var holdingImgDesc = $j('#customSelectBadge').find(':selected').attr('data-badge-desc');
            $j('#badgeImage').attr('src', holdingImg).attr('alt',holdingImgDesc).attr('title',holdingImgDesc);
            $j('#badgeImageDesc').empty().append(holdingImgDesc);
          }
          $j("#customSelectBadge").val("{!badgeId}");
          $j("#customSelectBadge").chosen({ width: '190px', disable_search: true});
          $j("#customSelectBadge").on('change', function(evt, params) {
            addSelectedBadge(params.selected);
          });
          if ("{!badgeId}" !== "") {
            addSelectedBadge("{!badgeId}");
          }
        </script>
      </div>
      <div class="pageContent">
        <apex:inputTextarea rows="8" styleClass="thanksMessage" onblur="resizeCanvas();" value="{!thanksMessage}" html-placeholder="{!$Label.ManyThanks_Message_Placeholder}" /><br />
        <script>
          autosize($j('textarea'));
          var ta = document.querySelector('textarea');
          ta.addEventListener('autosize:resized', function(){
            resizeCanvas();
          });
        </script>
      </div>
      <div class="pageFooter">
        <apex:repeat value="{!groupIdMap}" var="k">
          <c:AutoCompleteV2Filter SObject="CollaborationGroup"
            labelField="Name"
            valueField="Id"
            queryFilter="IsArchived = false"
            targetField="{!groupIdMap[k]}"
            placeholderText="{!$Label.ManyThanks_Post_To_Group_Placeholder}" 
            importJquery="false"
            syncManualEntry="false"
            allowClear="true"
            minLength="3"
            style="width:100%"
          />
        </apex:repeat>
        <br />
        <br />
        <div style="text-align:center; width: 100%;">
          <apex:commandButton style="width:50%; font-weight:bold; height:30px;" 
            action="{!addBadges}" 
            rerender="wholePage" 
            value=" {!$Label.ManyThanks_Say_Thanks} " 
            oncomplete="refreshFeed();" 
            id="sayThanks" 
            onclick="setTimeout('document.getElementById(\''+this.id+'\').className=\'btnDisabled\';', 1);  setTimeout('document.getElementById(\''+this.id+'\').disabled=true;', 50);" 
          />
        </div>
      </div>
    </apex:outputPanel>
    <br />
  </apex:form>
</apex:component>