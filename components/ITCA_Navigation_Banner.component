<!--
/**=====================================================================
 * Experian
 * Name: ITCA_Navigation_Banner
 * Description: 
 * Created Date: Jul. 31st 2017
 * Created By: James Wills
 * 
 *  Date Modified      Modified By                  Description of the update
 *  5th Aug 2017       James Wills                  Updated for new style-sheets.
 *  22nd Aug 2017      Alexander McCall             Issues I1434 and I1431
=====================================================================*/
 -->
<apex:component controller="ITCANavigationBannerController">
  
  <apex:attribute type="ITCABannerInfoClass" name="currentActiveTab1" required="true" description="The Data." assignTo="{!BannerInfoLocal}"/>
    
  <apex:form ><!--apex:outputText value="Value from Component is: {!testString}"/-->
  <nav id="global-header" class="navbar navbar-experian">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".header-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="logo-image">
          <!--<a href="/business-bucket/business-bucket.html" border="0"><img border="0" title="Experian" src="/site-images/content/expbrand-logo.png" class="logo-image-size" alt="Experian"></a>-->
          <!--<a href="/business-bucket/business-bucket.html" border="0"><img border="0" title="Experian" src="http://www.experian.com/global-images/responsive/exp-logo.png" class="logo-image-size" alt="Experian"></a>-->
          <a class="navbar-brand" href="/"><i class="icon i-logo exp-logo-color"></i></a>
        </div>
      </div>
      <div class="collapse navbar-collapse header-collapse">
        <div id="navGrp">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="../apex/ITCA_Home_Page" class="" data-toggle="" data-target="" role="button" aria-haspopup="true" aria-expanded="false">Home</a>
            </li>
            <li class="dropdown">
             <apex:outputText rendered="{!NOT(profileExists)}"><a href="" class="dropdown-toggle" data-toggle="" data-target="" role="button" aria-haspopup="true" aria-expanded="false">My Skills</a></apex:outputText>
              <ul class="dropdown-menu">
                <li><a href="../apex/ITCA_buildProfile_page">Create Profile</a></li>
              </ul>
            </li>
             <li class="dropdown">
             <apex:outputText rendered="{!profileExists}"><a href="" class="dropdown-toggle" data-toggle="" data-target="" role="button" aria-haspopup="true" aria-expanded="false">My Skills</a></apex:outputText>
              <ul class="dropdown-menu">
                <li><a href="../apex/ITCA_Profile_Update_Skills">Update Profile</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <apex:actionFunction name="isSummaryAction_AF"         action="{!isSummaryAction}"         reRender="My_Journey_Map"/>
              <apex:actionFunction name="compareSkillSetAction_AF"   action="{!compareSkillSetAction}"   reRender="My_Journey_Map"/>
              <apex:actionFunction name="matchSkillSetsAction_AF"    action="{!matchSkillSetsAction}"    reRender="My_Journey_Map"/>
              <apex:actionFunction name="myJourneyMapAction_AF"      action="{!myJourneyMapAction}"      reRender="My_Journey_Map"/>
              <apex:actionFunction name="compareCareerAreaAction_AF" action="{!compareCareerAreaAction}" reRender="My_Journey_Map"/>              
              <a href="" class="" data-toggle="" role="button" aria-haspopup="true" aria-expanded="false">My Journey</a>
              <ul class="dropdown-menu">
                <li><a href="../apex/ITCA_Home_Page#my-summary" onClick="isSummaryAction_JS();">My Profile</a></li>
                <!--<li><a href="professional-level.html#professional-level"> Professional Level </a></li>-->                
                <li><a href="../apex/ITCA_Home_Page#my-summary" onClick="compareSkillSetAction_JS();">Compare Skill Set</a></li>
                <li><a href="../apex/ITCA_Home_Page#my-summary" onClick="matchSkillSetsAction_JS();">Match Skill Set</a></li>                
                <li><a href="../apex/ITCA_Home_Page#my-summary" onClick="myJourneyMapAction_JS();">Create Journey Map</a></li>
                <li><a href="../apex/ITCA_Home_Page#my-summary" onClick="compareCareerAreaAction_JS();">Compare Career Area</a></li>
              </ul>
            </li>               
            <li class="dropdown">                                  
              <apex:outputText rendered="{!isManager}"><a href="" class="dropdown-toggle" data-toggle="" data-target="" role="button" aria-haspopup="true" aria-expanded="false">My Team</a></apex:outputText>
                <ul class="dropdown-menu">
                  <li><a href="../apex/ITCA_Team_Summary">Team Summary</a></li>
                  <li class="slds-dropdown__item">
                  <apex:repeat value="{!teamList}" var="tl">
                    <a href="{!$Site.Prefix}/apex/ITCA_Team_Member?selectedCareerProfileID={!tl.profileID}" role="menuitem">
                      <p class="slds-truncate"><apex:outputText value="{!tl.Name}"></apex:outputText></p>
                    </a>
                  </apex:repeat>
                </li>      
              </ul>                                 
            </li>             
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="" data-target="" role="button" aria-haspopup="true" aria-expanded="false">Resources</a>
              <ul class="dropdown-menu">
                <li><a href="../apex/ITCA_Resources_How_To_Use_ITCA">How to use the ITCA</a></li>
                <li><a target="_blank" href=" http://zoomglobal/services/gtsportal/_layouts/15/WopiFrame2.aspx?sourcedoc=/services/gtsportal/Documents/SFIA%20Link.pdf&action=default">About SFIA</a></li>
                <li><a href="..apex/ITCA_IT_Professional_Skills">IT Professional Skills</a></li>
                <li><a href="../apex/ITCA_Experian_Way">Experian Way</a></li>
                <li><a href="../apex/ITCA_Skill_Matrix">SFIA Directory</a></li>
                <li><a href="../apex/ITCA_Chatter?id=0F90n0000008ULY">ITCA Chatter Group</a></li>
                <li><a href="../apex/ITCA_FAQs">FAQs</a></li>
                <li><a target="_blank" href="../apex/EmployeeCommunity_Help_Page">Contact Us</a></li>
              </ul>
            </li>
            <!-- <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="" data-target="" role="button" aria-haspopup="true" aria-expanded="false">ITCA Network</a>
              <ul class="dropdown-menu">
                <li><a href="../apex/ITCA_Chatter?id=0F90H000000UKlo">ITCA Chatter Group</a></li>                
                <li><a href="../apex/ITCA_FAQs">FAQs</a></li>
                <li><a href="#">Submit your ideas</a></li> AM Phase 2
              </ul>
            </li> -->
          </ul>
        </div>
      </div>
    </div>
  </nav>
  </apex:form>
  <script>
    function isSummaryAction_JS(){
      isSummaryAction_AF();
      tabName = "apex__activeTabCookie=isSummary; path=/";
      document.cookie = tabName;
      //return false;
    }
    
    function compareSkillSetAction_JS(){
      compareSkillSetAction_AF();
      tabName = "apex__activeTabCookie=isCompareSKillSet; path=/";
      document.cookie = tabName;
      //return false;
    }            
    
    function matchSkillSetsAction_JS(){
      matchSkillSetsAction_AF();
      tabName = "apex__activeTabCookie=isMatchSKills; path=/";
      document.cookie = tabName;
      //window.location.replace("../apex/ITCA_Home_Page#my-summary");
      //return false;
    }

    
    function myJourneyMapAction_JS(){
      myJourneyMapAction_AF();
      tabName = "apex__activeTabCookie=isMyJourneyMap; path=/";
      document.cookie = tabName;
      //return false;
    }
    
    function compareCareerAreaAction_JS(){
      compareCareerAreaAction_AF();
      tabName = "apex__activeTabCookie=isCompareCareerArea; path=/";
      document.cookie = tabName;
      //return false;
    }
    
  </script>
</apex:component>