<!--
/**=====================================================================
 * Name: AccountPlanPDFDashboard
 * Description: 
 * Created Date: Jan. 25th, 2016
 * Created By: James Wills
 * 
 * Date Modified         Modified By            Description of the update
 * Jan. 25th, 2017       James Wills            Created.
 * Feb. 17th, 2017       James Wills            Case 02194059: Put Account_Plan__c field values into apex:outputField tags so that translations would be displayed.
 * =====================================================================*/
-->
<apex:component controller="AccountPlanPDFComponentsController">

      <!-- Account Dashboard Section on PDF -->
      <table class="table-bordered" style="width:94%;border-spacing: 0 !important;">
      <tr><td colspan="2" style="padding-left:0px !important;"> <div class="title"> {!$Label.ACCOUNTPLANNING_PDF_Account_Dashboard}</div></td></tr>
         <tr>
            <td width="350px" style="font-size: 13px; font-weight: bold;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_Indicator}
            </td>
            <td width="180px" style="font-size: 13px; font-weight: bold;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_Status}
            </td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px; text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_RISKS}
            </td>
            <!-- Dynamically Decides the color of the section -->
            <td width="180px" style="{!IF(accountPlanObj.Risks__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Risks__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Risks__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Risks__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px; text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_REFERENCE}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Reference__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Reference__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Reference__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Reference__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_EXECUTIVE}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Executive__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Executive__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Executive__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Executive__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_ONE_EXPERIAN}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.One_Experian__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.One_Experian__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.One_Experian__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.One_Experian__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_CLIENT_PLAN}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Client_Plan__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Client_Plan__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Client_Plan__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Client_Plan__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px; text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_FACE_TO_FACE}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Face_to_Face__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Face_to_Face__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Face_to_Face__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Face_to_Face__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_CONTACT_PLAN}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Contact_Plan__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Contact_Plan__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Contact_Plan__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Contact_Plan__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px; text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_HEALTH_STATUS}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Health_Status__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Health_Status__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Health_Status__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Health_Status__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_REVENUE}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Revenue__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Revenue__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Revenue__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Revenue__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_OPPORTUNITIES_Dashboard_Text}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Opportunities__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Opportunities__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Opportunities__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Opportunities__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_EXPIRATION}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Expiration__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Expiration__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Expiration__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Expiration__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px; text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_CONTRACTS}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Contracts__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Contracts__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Contracts__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Contracts__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_PAYMENT_ISSUES}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Payment_Issues__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Payment_Issues__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Payment_Issues__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Payment_Issues__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_STABILITY}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Stability__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Stability__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Stability__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Stability__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_SOLUTIONS}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Solutions__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Solutions__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Solutions__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Solutions__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_VALUE}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Value__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Value__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Value__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Value__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_NPS}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.NPS__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.NPS__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.NPS__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.NPS__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_INTERNAL_ACCOUNT_TEAM}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Internal_Account_Team__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Internal_Account_Team__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Internal_Account_Team__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Internal_Account_Team__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_INDUSTRY_EXPERTISE}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Industry_Expertise__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Industry_Expertise__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Industry_Expertise__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Industry_Expertise__c}"/></td>
         </tr>
         <tr>
            <td width="350px" style="font-size: 13px;text-align:left;padding-left : 10px;">
               {!$Label.ACCOUNTPLANNING_PDF_MAJOR_WINS}
            </td>
            <td width="180px" style="{!IF(accountPlanObj.Major_Wins__c == 'Strength', 'background-color:#b6d7a8', IF(accountPlanObj.Major_Wins__c == 'Negative', 'background-color:#ea9999;', IF(accountPlanObj.Major_Wins__c == 'Neutral', 'background-color:#ffe599;', 'background-color:#FFFFFF;')))};padding-left : 10px;"><apex:outputField value="{!accountPlanObj.Major_Wins__c}"/></td>
         </tr>
      </table>

      <br />
      <br />
      <div class="page-break" />
      
</apex:component>