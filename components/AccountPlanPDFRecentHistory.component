<!--
/**=====================================================================
 * Name: AccountPlanPDFRecentHistory
 * Description: 
 * Created Date: Jan. 25th, 2016
 * Created By: James Wills
 * 
 * Date Modified         Modified By            Description of the update
 * Jan. 25th, 2017       James Wills            Created.
 * Feb. 8th, 2017        James Wills            Case:02194059 Changed Custom labels to Field labels
 * Feb. 16th, 2017       James Wills            Case:02255727 - Update to allow display of rich-text format (for DO).
 * =====================================================================*/
-->
<apex:component controller="AccountPlanPDFComponentsController">

  <!--apex:attribute name="accountPlan1" description="" type="Account_Plan__c" required="true" assignTo="{!accountPlanobj}"/-->
  
  <apex:attribute name="showSummaryrecentHistory1" description="" type="Boolean" assignTo="{!showSummaryrecentHistory}"/>
  <apex:attribute name="showRecentRelationshipSuccess1" description="" type="Boolean" assignTo="{!showRecentRelationshipSuccess}"/>
  <apex:attribute name="showRelationshipObjNotAchieved1" description="" type="Boolean" assignTo="{!showRelationshipObjNotAchieved}"/>
  <apex:attribute name="showRelationshipLessonsLearned1" description="" type="Boolean" assignTo="{!showRelationshipLessonsLearned}"/>  
  <apex:attribute name="showAdditionalInformation1" description="" type="Boolean" assignTo="{!showAdditionalInformation}"/>

      <!-- Recent History Section on PDF -->
      <table class="table-bordered" style="width:94%;border-spacing: 0 !important;margin-top:30px !important">
         <tr><td colspan="2" style="padding-left:0px !important;"> <div class="title"> {!$Label.ACCOUNTPLANNING_PDF_Recent_History}</div></td></tr>
         <apex:outputText rendered="{!showSummaryRecentHistory}">
           <tr>
             <td width="172.5px"  style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">
               {!$ObjectType.Account_Plan__c.fields.Summary_Recent_History__c.label}
               </td>
             <td width="517.5px" style = "padding-left : 10px;"><apex:outputText escape="false" value="{!accountPlanObj.Summary_Recent_History__c}"/></td>
           </tr>
         </apex:outputText>
         <apex:outputText rendered="{!showRecentRelationshipSuccess}">
           <tr>
             <td width="172.5px" style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">
               {!$ObjectType.Account_Plan__c.fields.Recent_Successes__c.label}
               </td>
             <td width="517.5px" style = "padding-left : 10px;"><apex:outputText escape="false" value="{!accountPlanObj.Recent_Successes__c}"/></td>
           </tr>
         </apex:outputText>
         <apex:outputText rendered="{!showRelationshipObjNotAchieved}">
           <tr>
             <td width="172.5px"  style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">
               {!$ObjectType.Account_Plan__c.fields.Objectives_Not_Achieved__c.label}
               </td>
             <td width="517.5px" style = "padding-left : 10px;"><apex:outputText escape="false" value="{!accountPlanObj.Objectives_Not_Achieved__c}"/></td>
           </tr>
         </apex:outputText>
         <apex:outputText rendered="{!showRelationshipLessonsLearned}">
           <tr>
              <td width="172.5px"  style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">
                 {!$ObjectType.Account_Plan__c.fields.Lessons_Learned__c.label}
              </td>
              <td width="517.5px" style = "padding-left : 10px;"><apex:outputText escape="false" value="{!accountPlanObj.Lessons_Learned__c}"/></td>
           </tr>
         </apex:outputText>
         <apex:outputText rendered="{!showAdditionalInformation}">
           <tr>
             <td width="172.5px"  style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">
               {!$ObjectType.Account_Plan__c.fields.Additional_Information__c.label}
               </td>
             <td width="517.5px" style = "padding-left : 10px;"><apex:outputText escape="false" value="{!accountPlanObj.Additional_Information__c}"/></td>
           </tr>
         </apex:outputText>
         
      </table>
      
      <br />
      <br />
      <div class="page-break" />
      
</apex:component>