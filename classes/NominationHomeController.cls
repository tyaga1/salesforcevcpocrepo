/*=============================================================================
     * Experian
     * Name: NominationHomeController
     * Description: 
     * Created Date: 3 Nov 2016
     * Created By: Paul Kissick
     *
     * Date Modified      Modified By           Description of the update
     * 10 Nov 2016        Paul Kissick          Added support to return a message from another page.
     * 14 Nov 2016        Tyaga Pati            Added functions for returning Thanks badges for and by user.
     =============================================================================*/
    
    public with sharing class NominationHomeController {
    
      // Added support to load a message from another page.
      public NominationHomeController() {
        if (ApexPages.currentPage().getParameters().containsKey('msg')) {
          msg = ApexPages.currentPage().getParameters().get('msg');
          hasMsg = true;
        }
      }
    
      //===========================================================================
      // Each Section is referenced here. Create another one, and initialise using
      // a query in the initialisePage() method.
      //===========================================================================
      public SectionPaging myPendingApproval {get;set;}
      public SectionPaging myRewards {get;set;}
      public SectionPaging mySubmissions {get;set;}
      public SectionPaging myBadges{get;set;}
      public SectionPaging myGivenBadges {get;set;}
      
      
      
      public Boolean hasMsg {get{if(hasMsg == null) hasMsg = false; return hasMsg;}set;}
      public String msg {get{if(msg == null) msg = ''; return msg;}set;} 
      
      //===========================================================================
      // Called on apex:page action - loads the queries
      //===========================================================================
      public PageReference initialisePage() {
        myPendingApproval = new SectionPaging(
          ' SELECT Id, Nominee__r.FullPhotoUrl, Nominee__r.SmallPhotoUrl, Nominee__c, Nominee__r.Title, Status__c, Justification__c, createdDate,  '+ 
          ' Nominees_Manager__r.Name, Payroll_Award_Date__c, Nominee__r.Name, Manager_Approved_Date__c, Approver__c, ' +
          ' LastModifiedDate, Type__c, Requestor__c, Team_Name__c ' +
          ' FROM Nomination__c ' +
          ' WHERE OwnerId = \''+UserInfo.getUserId()+'\' ' +
          ' AND Status__c = \''+NominationHelper.NomConstants.get('PendingApproval')+'\' ' +
          ' ORDER BY CreatedDate DESC'
        );
        myRewards = new SectionPaging(
          ' SELECT Id, Nominee__r.FullPhotoUrl, Nominee__r.SmallPhotoUrl, Nominee__c, Nominee__r.Title, ' +
          '        Type__c, Status__c, Requestor__c, Nominees_Manager__r.Name, Payroll_Award_Date__c,  ' +
          '        Nominee__r.Name, Manager_Approved_Date__c, Approver__c, Badge__r.Name, Badge__r.Recognition_Category__c, Badge__r.ImageUrl ' +
          ' FROM Nomination__c ' +
          ' WHERE Nominee__c != null ' +
          ' AND Nominee__c = \''+UserInfo.getUserId()+'\' ' +
          ' AND Nominee_Viewable__c = true ' +
          ' ORDER BY CreatedDate DESC '
          , 3
        );
        mySubmissions = new SectionPaging(
          'SELECT Id, Nominee__r.FullPhotoUrl, Nominee__r.SmallPhotoUrl, Nominee__c, Nominee__r.Title, ' +
          '       Status__c, Nominees_Manager__r.Name, Payroll_Award_Date__c, Nominee__r.Name, Manager_Approved_Date__c, Type__c, ' +
          '       Team_Name__c, Badge__r.ImageUrl ' +
          ' FROM Nomination__c ' +
          ' WHERE Requestor__c != null ' +
          ' AND Requestor__c = \''+UserInfo.getUserId()+'\' ' +
          ' ORDER BY CreatedDate DESC'
        );
        
   myBadges = new SectionPaging(
      'SELECT ImageUrl, GiverId, RecipientId, CreatedDate, DefinitionId, Description, Message, SourceId, LastViewedDate, Id '+ 
      'FROM WorkBadge where RecipientId != null ' +
      ' AND RecipientId = \''+UserInfo.getUserId()+'\' ' +
      ' ORDER BY CreatedDate DESC'
    );

    myGivenBadges = new SectionPaging(
      'SELECT ImageUrl, GiverId, RecipientId, CreatedDate, DefinitionId, Description, Message, SourceId, LastViewedDate, Id '+ 
      'FROM WorkBadge where GiverId!= null ' +
      ' AND GiverId = \''+UserInfo.getUserId()+'\' ' +
      ' ORDER BY CreatedDate DESC'
    );
    return null;
     
   }
      
      
      //===========================================================================
      // Navigate to the 'New' reward page.
      //===========================================================================
      public PageReference navToNewRewardPage() {
        return Page.NominationNew;
      }
      
      //===========================================================================
      //TP: Nav Function for Manager Dashboard
      //===========================================================================
      public PageReference navToManagerDashboard() {
        // Never code full urls!
        // https://experian--supportdev.cs22.my.salesforce.com
        // Consider a way we can get this id without hard coding!
           User objUser = [select id,Oracle_IsManager__c from User where Id=:Userinfo.getUserId()]; //Added by MG
           set<string> setPermissionName = new set<string>();
           for(PermissionSetAssignment pAssign:[SELECT AssigneeId, PermissionSetId, PermissionSet.Label,PermissionSet.Name FROM PermissionSetAssignment where AssigneeId =:Userinfo.getUserId()]){
           setPermissionName.add(pAssign.PermissionSet.Name);
        }
        
        if(objUser.Oracle_IsManager__c == TRUE && !setPermissionName.contains('Recognition_Coordinator')){
        List<Dashboard> dash = [
          SELECT Id 
          FROM Dashboard 
          WHERE DeveloperName = 'Manager_Recognition_Award_Committee_Dashboard'
          LIMIT 1
        ];
        if (!dash.isEmpty()) {
          ApexPages.StandardController tmpCon = new ApexPages.StandardController(dash.get(0));
          return tmpCon.view();

        }
        
       }
         else{   
             List<Dashboard> dash = [
              SELECT Id 
              FROM Dashboard 
              WHERE DeveloperName = 'Recognition_Award_Committee_Dashboard2'
              LIMIT 1
            ];
            if (!dash.isEmpty()) {
              ApexPages.StandardController tmpCon = new ApexPages.StandardController(dash.get(0));
              return tmpCon.view();
            }
        }
        return null;
      }
      
      //===========================================================================
      // TP: Nav Function for Rec Coordinator Dashboard
      // TODO: Fix this to reference the correct dashboard!
      //===========================================================================
      public PageReference navToRecognitionCoordinatorDashboard() {
      
        List<Report> repRecognitionRev= [
          SELECT Id 
          FROM Report
          WHERE DeveloperName = 'Recognition_Coordinator_Pending_Report'
          LIMIT 1
        ];
      
       if (!repRecognitionRev.isEmpty()) {
          ApexPages.StandardController tmpRep = new ApexPages.StandardController(repRecognitionRev.get(0));
          return tmpRep.view();
        }
        return null;
      
      }
      
      //===========================================================================
      // 
      //===========================================================================
      public List<Nomination__c> getRecentNominations() {
        return [
          SELECT Id, Nominee__r.FullPhotoUrl, Nominee__r.SmallPhotoUrl, Nominee__c, Nominee__r.Title, 
                 Status__c, Nominees_Manager__r.Name, Payroll_Award_Date__c, Type__C, Requestor__c, Nominee__r.Name, 
                 Manager_Approved_Date__c
          FROM Nomination__c
          WHERE LastViewedDate != null 
          ORDER BY LastViewedDate DESC 
          limit 10
        ];
      }
      
      //===========================================================================
      // Inner class to handle multiple sections on the page without recoding every
      // time. 
      // Note: This class return nomination records in getNomRecords().
      // Create new methods if return different sobjects.
      //===========================================================================
      public class SectionPaging {
        
        public String initQuery {get;set;}
        public Integer defaultSize {get{if (defaultSize == null) defaultSize = 5; return defaultSize;}set;}
        public Integer totalSize {get;set;}
        public Integer totalSizeTwo {get;set;}
        public Integer totalPages {get;set;}
        public Integer totalPagesTwo {get;set;}
        
        public Integer gotoPageNum {get;set;}
        public Integer gotoPageNumTwo {get;set;}
        
        public SectionPaging(String q) {
          initQuery = q;
        }
        
       public SectionPaging(String q, Integer s) {
          initQuery = q;
          defaultSize = s;
        }
        
        
        
       /* public ApexPages.StandardSetController setConThanks {
          get {
          system.debug('this is executing tyaga' + initQuery);
            if (setConThanks == null) {
            
             List<WorkBadge> BadgeList = Database.Query(initQuery);
             system.debug('Tyaga22222: the badge records are : ' + BadgeList );
              
              /*setCon = new ApexPages.StandardSetController(
                Database.getQueryLocator(initQuery)
              );*/
        //      if(BadgeList <> null)
         //     setConThanks = new ApexPages.StandardSetController(BadgeList);
         //     setConThanks.setPageSize(defaultSize); 
         //     totalSizeTwo = setConThanks.getResultSize();
        //      totalPagesTwo = Integer.valueOf(Math.ceil(Decimal.valueOf(totalSizeTwo) / Decimal.valueOf(defaultSize)));
     //       }
      //     return setConThanks;
      //    }
   //       set;
     //   }   
        
        //===========================================================================
        // 
        //===========================================================================
        public ApexPages.StandardSetController setCon {
          get {
            if (setCon == null) {
            
              //List<Nomination__C> NomList = Database.Query(initQuery);
              List<sObject> sObjectList = Database.Query(initQuery);
            
              //system.debug('Tyaga11111: the nominations records are : ' + NomList );
              /*setCon = new ApexPages.StandardSetController(
                Database.getQueryLocator(initQuery)
              );*/
              
              
              //if(sObjectList <> null )
              setCon = new ApexPages.StandardSetController(sObjectList);
            
              setCon.setPageSize(defaultSize); 
              totalSize = setCon.getResultSize();
              totalPages = Integer.valueOf(Math.ceil(Decimal.valueOf(totalSize) / Decimal.valueOf(defaultSize)));
            }
            return setCon;
          }
          set;
        }
        
        //===========================================================================
        // 
        //===========================================================================
       
        
        public List<Nomination__c> getNomRecords() {
          return (List<Nomination__c>)setCon.getRecords();
        }
        
                
        public List<WorkBadge> getBadgeRecords() {
          return (List<WorkBadge>)setCon.getRecords();
        }
        public List<Integer> getPageList() {
          List<Integer> pages = new List<Integer>();
          for (Integer i = 1; i <= totalPages; i++) {
            pages.add(i);
          }
          return pages;
        }
        
        
        public PageReference gotoPage() {
          if (gotoPageNum != null) {
            setCon.setPageNumber(gotoPageNum);
          }
          return null;
        }
      
        public Integer getCurrentPage() {
          return setCon.getPageNumber();
        }
      
        public Boolean getHasNext() {
          return setCon.getHasNext();
        }
      
        public Boolean getHasPrevious() {
          return setCon.getHasPrevious();
        }
      
        public PageReference getNext() {
          setCon.next();
          return null;
        }
      
        public PageReference getPrevious() {
          setCon.previous();
          return null;
        }

        
      }  
      
    }