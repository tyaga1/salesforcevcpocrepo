public with sharing class CommunityCaseDetailCntrl {

public static String DEFAULT_SITE_NAME = Site.getName();
    public static Id NETWORK_ID = (Network.getNetworkId() != null) ? Network.getNetworkId() : UserInfo.getOrganizationId();
    public static Integer MAX_REASON_LENGTH = 2000;
    public static Integer MAX_COMMENT_LENGTH =Schema.CaseComment.fields.CommentBody.getDescribe().getLength();
    static String CASE_COMMENTS = 'casecomment';
    static String CASE_ARTICLES = 'casearticle';
    static String ATTACHMENTS  = 'attachment';
    static String NO_RELATED_LISTS = 'no_related_lists';

    //************************************************************************
    // Properties & data members
    public String type{ get;set; }
    public Boolean mylist{ get;set; }
    public String title{ get;set; }
    public String pageHeader{ get;set; }
    public Boolean readOnCases{ get;set; }
    public Boolean canAccess{ get;set; }
    public Boolean showCloseBtn{ get;set; }

    public List<String> accessErrorMsgs{ get;set; }

    public String relatedLists {get;set;}

    public list<CaseComment> comments{get;set;}

    public Boolean showColumn1 {get;set;}
    public Boolean showColumn2 {get;set;}

    public Boolean showList1 {get;set;}
    public Boolean showList2 {get;set;}

    public list<string> colOrder1 {get;set;}
    public list<string> colOrder2 {get;set;}

    public Map<String,Map<String,String>> caseLayout {get;set;}

    public Case   aCase {get; set;}
    public CaseComment   aCComment {get; set;}

    public String orgUrl {get;set;}
    public boolean isInternalCommunity {get;set;}

    //added an instance varaible for the standard controller
    private ApexPages.StandardController controller {get; set;}
    public String screenComment {get; set;}


    //************************************************************************
    // Methods


    public CommunityCaseDetailCntrl(ApexPages.StandardController controller) {
    this.controller = controller;
        this.aCase = (Case)controller.getRecord();
        restartCaseComment();
        comments=CommunityCaseDetailCntrl.getCaseComments(this.aCase.Id);

    }
     public PageReference saveNewComment(){
        PageReference retPage = null;

        try{
            //aCComment.CommentBody=screenComment ;
        //CHECK the length of the caseComments field
        // Use DML options to enable email delivery
            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.EmailHeader.triggerAutoResponseEmail = true;
            dmlOpts.EmailHeader.triggerOtherEmail = true;
            dmlOpts.EmailHeader.triggerUserEmail = true;

            if ( ! String.isNotBlank(this.aCComment.CommentBody)  ){
                ApexPages.addMessage(new ApexPages.Message(
                    ApexPages.Severity.ERROR,
                    'Case Comment Empty'
                ));
                return retPage;
            }

            if (this.aCComment.CommentBody.length() > MAX_COMMENT_LENGTH){
                //String msg = Label.hd_error_caseComment_max_length.replace('{0}',String.valueOf(MAX_COMMENT_LENGTH));
                    ApexPages.addMessage(new ApexPages.Message(
                    ApexPages.Severity.ERROR,
                    'Maximum Comment Length is '+ MAX_COMMENT_LENGTH
                ));
                return retPage;
            }

            //insert this.aCComment;
            
            //aCComment.CommentBody=screenComment ;
            
            database.insert(this.aCComment, dmlOpts); //insert the case comment (with the DML options)
            //insert this.aCComment;
            comments = CommunityCaseDetailCntrl.getCaseComments(this.aCase.Id);
            restartCaseComment();
            //retPage = new pageReference('/apex/CommunityCaseDetailPage?id='+this.aCase.Id);

        }catch(Exception e){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        return retPage;
    }
 private void restartCaseComment(){

            this.aCComment = new CaseComment();
            this.aCComment.ParentId = this.aCase.Id;
            this.aCComment.IsPublished = true;
            screenComment ='';
    }

public static list<CaseComment> getCaseComments(Id caseId){
         list<CaseComment> res;
         if(String.isNotBlank(caseId)){
             res = [    SELECT CommentBody,CreatedById,CreatedDate,Id,IsPublished,createdby.name,createdby.alias,createdby.firstname
                        FROM CaseComment
                        WHERE IsPublished = true and ParentId = :caseId
                        ORDER BY CreatedDate desc];
         }
         return res;
         }

}