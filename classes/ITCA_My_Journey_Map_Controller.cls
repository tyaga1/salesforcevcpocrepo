/**=====================================================================
 * Experian
 * Name: ITCA_My_Journey_Map_Controller
 * Description: This is the Apex Controller for the ITCA_My_Journey_Map VF Component.
 * Created Date: August 2017
 * Created By: James Wills
 *
 * Date Modified      Modified By           Description of the update
 * 29th July 2017     James Wills           ITCA:W-008961 - Build list of Employees for Manager - removed to VF Component ITCA_Navigation_Banner
 * 30th July          Richard Joseph        Added employeeSkillDisplay, employeeSkillDisplaySort, compareSkillSet. compareCareerArea
 * 2nd August 2017    Alexander McCall      Updated working to 'Skill Set' and 'Career Area' on drop downs
 * 3rd August 2017    James Wills           ITCA:W-008961 - Updated for new requirements.
 * 8th August 2017    James Wills           Tidying up code.
 * 11th August 2017   James Wills           ITCA:W-009246 Updated to allow editing technical skills.
 * 30th August 2017   James Wills           Removing redundant code in preparation for test classes.
 * 1st September 2017 James Wills           Tidied up.
 * =====================================================================*/
global with sharing class ITCA_My_Journey_Map_Controller{

  public ITCABannerInfoClass bannerInfoLocal {get;set;}
  public list<Career_Architecture_User_Profile__c> currentUserProfile                {get;set;}  

  public String                                    level1_selectedCareerArea         {get;set;}
  public String                                    level2_selectedSkillSet           {get;set;}
 
  public enum activePanel                     {isSummary, isMatchSKills, isCompareSKillSet, isCompareCareerArea}  
  public String currentActiveTab              {get;set;}
 
 
  public class userSkillInfo{    
    public ID skillID                         {get;set;}
    public String skillName                   {get;set;}
    public String skillSet                    {get;set;}
    public String currentSkillValue           {get;set;}
    public String currentLevel                {get;set;}
    public Integer nextSkillvalue             {get;set;}
    
    public Map<String,String> skillLevels_Map {get;set;}
     
  }
  
  public String selectedSkillId               {get;set;}
  
  
  public ITCABannerInfoClass.employeeSkillDisplay selectedESDS {get;set;}
  
  public List<userSkillInfo> skillOverview_List{get{

      List<userSkillInfo> skillOverview_List = new List<userSKillInfo>();
      List<Career_Architecture_Skills_Plan__c> casp_List = [SELECT id, Level__c, Skill__r.Name, Skill__r.Level1__c, Skill__r.Level2__c, Skill__r.Level3__c, Skill__r.Level4__c, Skill__r.Level5__c,
                                                            Skill__r.Level6__c, Skill__r.Level7__c, Skill__r.Max_Skill_Level__c, Experian_Skill_Set__r.Name
                                                            FROM Career_Architecture_Skills_Plan__c
                                                            WHERE Skill__c = :selectedSkillId AND
                                                            Career_Architecture_User_Profile__r.Employee__c = :UserInfo.getUserId()];
      if(!casp_List.isEmpty()){                                                     
        for(Career_Architecture_Skills_Plan__c casp : casp_list){
          userSkillInfo usi     = new userSkillInfo();
          usi.skillID           = casp.id;
          usi.skillName         = casp.Skill__r.Name;
          usi.skillSet          = casp.Experian_Skill_Set__r.Name;
          usi.currentLevel      = getCurrentSkillLevel(casp);
          usi.currentSkillValue = casp.Level__c.substring(casp.Level__c.length()-1,casp.Level__c.length());

          if(Integer.valueOf(usi.currentSkillValue) < casp.Skill__r.Max_Skill_Level__c){
            usi.nextSkillValue  = Integer.valueOf(usi.currentSkillValue)+1;
          } else {
            usi.nextSkillValue  = Integer.valueOf(usi.currentSkillValue);
          }
          usi.skillLevels_Map   = buildSkillLevelsMapforModal(casp);

         skillOverview_List.add(usi);
        }
      } else {
        List<ProfileSkill> ps_List = [SELECT id, Name, Level1__c, Level2__c, Level3__c, Level4__c, Level5__c,Level6__c, Level7__c, Max_Skill_Level__c
                                      FROM ProfileSkill
                                      WHERE id = :selectedSkillId];
        if(!ps_list.isEmpty()){
          for(ProfileSkill ps : ps_List){
            userSkillInfo usi   = new userSkillInfo();
            usi.skillID         = ps.id;
            usi.skillName       = ps.Name;
          
            usi.skillLevels_Map = buildSkillLevelsMapforModal_FromSkill(ps);

            skillOverview_List.add(usi);
          }
        } else {
           List<Experian_Skill_Set__c> es_List = [SELECT id, Name FROM Experian_Skill_Set__c WHERE id =:selectedSkillId];

           for(Experian_Skill_Set__c es : es_List){
             userSkillInfo usi   = new userSkillInfo();
             usi.skillID         = es.id;
             usi.skillName       = es.Name;
             
             usi.skillLevels_Map = buildSkillLevelsMapforModal_FromExpSkillSet(es);
             
             skillOverview_List.add(usi);
           }           
        }
      }
      return skillOverview_List;
    }
    set;    
  }
  
  public Map<ID,ProfileSkill> skillMaxLevels_Map {get{ return new Map<ID, ProfileSkill>([SELECT id, Name, Max_Skill_Level__c FROM ProfileSkill]);} set;}
  
  public String userProfileCurrentLevel {get{ return itcaTeamSummaryController.getProfileLevel(casp_List, skillMaxLevels_Map);} set;}
  
  
  public String employeeNameForProfile {get{
      if(!currentUserProfile.isEmpty()){
        return currentUserProfile[0].Employee__r.Name + '\'s';
      }
      return null;
    }
  set;}
      
  
  public List<Career_Architecture_Skills_Plan__c> casp_List {get{ return new list<Career_Architecture_Skills_Plan__c>([SELECT Id, Skill__r.Id, Skill__r.Name, Skill__r.sfia_Skill__c,
                                                                                                      Skill__r.Level1__c,Skill__r.Level2__c,
                                                                                                      Skill__r.Level3__c,skill__r.Level4__c,Skill__r.Level5__c,
                                                                                                      Skill__r.Level6__c,Skill__r.Level7__c,
                                                                                                      Level__c, Experian_Skill_Set__r.Name, Experian_Skill_Set__r.Career_Area__c,
                                                                                                      Career_Architecture_User_Profile__c,
                                                                                                      Skill__r.Max_Skill_Level__c
                                                                                                      FROM Career_Architecture_Skills_Plan__c 
                                                                                                      WHERE Career_Architecture_User_Profile__c 
                                                                                                      IN :currentUserProfile
                                                                                                      ORDER BY Skill__r.Name]);} set;}        
  //Compare Skillsets
  public Set<String> level1_SelectedCareerAreas_Set{
    get{
      if(level1_selectedCareerArea==null || level1_selectedCareerArea=='Select Career Area'){      
        Set<String> options_Set = new Set<String>();
        for(SelectOption sel : level1_CareerAreaOptions){
          options_Set.add(sel.getLabel());
        }      
        return options_Set;
      }
      return new Set<String>{level1_selectedCareerArea};
    }        
  set;}
        
  public Set<String> level2_selectedSkillSets_Set{
    get{
       if(level2_selectedSkillSet==null || level2_selectedSkillSet=='Select Skill Set'){
         Set<String> options_Set = new Set<String>();
         for(SelectOption sel : level2_skillSetOptions){
           options_Set.add(sel.getValue());
         }      
         return options_Set;
       }    
       return new Set<String>{level2_selectedSkillSet};
     }           
  set;}    
          
  public List<SelectOption> level1_CareerAreaOptions{
    get{
      List<SelectOption> selList = new List<SelectOption>();
      
      Schema.DescribeFieldResult fieldResult = Experian_Skill_Set__c.Career_Area__c.getDescribe();
      List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();     
      selList.add(new SelectOption('Select Career Area', 'Select Career Area'));     
      for( Schema.PicklistEntry f : ple){
        selList.add(new SelectOption(f.getValue(), f.getLabel()));
      }                  
      return selList;
    }          
    set;}
          
  public List<SelectOption> level2_skillSetOptions {
    get{
      List<SelectOption> selList = new List<SelectOption>();                                  
      List<Experian_Skill_Set__c> skillOptions_List = [SELECT id, Name FROM Experian_Skill_Set__c];
      selList.add(new SelectOption('Select Skill Set', 'Select Skill Set'));                
      for(Experian_Skill_Set__c ess : skillOptions_List){
        SelectOption sel = new SelectOption(ess.id, ess.Name);
        selList.add(sel);
      }      
      return selList;
    }               
    set;
  }
                
  
  ////////////////////////////////////////////////////////////////////////////////////////////

  public ITCA_My_Journey_Map_Controller(){   

     currentUserProfile= new List<Career_Architecture_User_Profile__c>([SELECT id, Employee__r.Name, role__c, State__c, Status__c, Manager_Comments__c, Date_Discussed_with_Employee__c
                                                                       FROM Career_Architecture_User_Profile__c 
                                                                       WHERE State__c = 'Current' AND Employee__c = :UserInfo.getUserId() LIMIT 1]);    
    if(bannerInfoLocal!=null){
      Cookie activeTabCookie = new Cookie('activeTabCookie', bannerInfoLocal.currentActiveTab, null,-1,false);
      ApexPages.currentPage().setCookies(new Cookie[]{activeTabCookie});    
    }
  }
  
  public Map<String,String> buildSkillLevelsMapForModal_FromExpSkillSet(Experian_Skill_Set__c es){
    Map<String,String> skillLevels_Map = new Map<String,String>();
    
    List<Career_Architecture_Skills_Plan__c> casp_List = [SELECT id, Skill__r.Name FROM Career_Architecture_Skills_Plan__c
                                                          WHERE Experian_Skill_Set__c = :es.id AND
                                                          Career_Architecture_User_Profile__r.Employee__c = :UserInfo.getUserId()];
    Integer i=0;
    for(Career_Architecture_Skills_Plan__c casp : casp_List){
      i++;
      skillLevels_Map.put(String.valueOf(i), casp.Skill__r.Name);
    }
    return skillLevels_Map;
  }
    
  public String getCurrentSkillLevel(Career_Architecture_Skills_Plan__c casp){
    String currentLevel;
    if(casp.Level__c=='Level1'){
      return casp.Skill__r.Level1__c;
    }else if(casp.Level__c=='Level2'){
      return casp.Skill__r.Level2__c;
    }else if(casp.Level__c=='Level3'){
      return casp.Skill__r.Level3__c;
    }else if(casp.Level__c=='Level4'){
      return casp.Skill__r.Level4__c;
    }else if(casp.Level__c=='Level5'){
      return casp.Skill__r.Level5__c;
    }else if(casp.Level__c=='Level6'){
      return casp.Skill__r.Level6__c;
    }else if(casp.Level__c=='Level7'){                 
      return casp.Skill__r.Level7__c;
    }    
    return currentLevel;
  }
    
  public Map<String,String> buildSkillLevelsMapForModal(Career_Architecture_Skills_Plan__c casp){
    Map<String,String> skillLevels_Map = new Map<String,String>();
    
    if(casp.Skill__r.Level1__c!=null){
      skillLevels_Map.put('1', casp.Skill__r.Level1__c);
    }
    if(casp.Skill__r.Level2__c!=null){
      skillLevels_Map.put('2', casp.Skill__r.Level2__c);
    }
    if(casp.Skill__r.Level3__c!=null){
      skillLevels_Map.put('3', casp.Skill__r.Level3__c);
    }
    if(casp.Skill__r.Level4__c!=null){
      skillLevels_Map.put('4', casp.Skill__r.Level4__c);
    }
    if(casp.Skill__r.Level5__c!=null){
      skillLevels_Map.put('5', casp.Skill__r.Level5__c);
    }
    if(casp.Skill__r.Level6__c!=null){
      skillLevels_Map.put('6', casp.Skill__r.Level6__c);
    }
    if(casp.Skill__r.Level7__c!=null){
      skillLevels_Map.put('7', casp.Skill__r.Level7__c);
    }    
    return skillLevels_Map;
  }
  
  public Map<String,String> buildSkillLevelsMapForModal_FromSkill(ProfileSkill ps){
    Map<String,String> skillLevels_Map = new Map<String,String>();
    
    if(ps.Level1__c!=null){
      skillLevels_Map.put('1', ps.Level1__c);
    }
    if(ps.Level2__c!=null){
      skillLevels_Map.put('2', ps.Level2__c);
    }
    if(ps.Level3__c!=null){
      skillLevels_Map.put('3', ps.Level3__c);
    }
    if(ps.Level4__c!=null){
      skillLevels_Map.put('4', ps.Level4__c);
    }
    if(ps.Level5__c!=null){
      skillLevels_Map.put('5', ps.Level5__c);
    }
    if(ps.Level6__c!=null){
      skillLevels_Map.put('6', ps.Level6__c);
    }
    if(ps.Level7__c!=null){
      skillLevels_Map.put('7', ps.Level7__c);
    }    
    return skillLevels_Map;
  }
}