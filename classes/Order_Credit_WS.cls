/**=====================================================================
 * Appirio, Inc
 * Name: Order_Credit_WS 
 * Description: Web-Service to Credit an Order
 * Created Date: Jul 08th, 2015
 * Created By: Naresh Kr Ojha (Appirio) for Task T-417697
 * 
 * Date Modified        Modified By              Description of the update
 * Jul 09th, 2015       Naresh Kr Ojha           T-417869: Updated class to make related assets Credited.
 * Aug 12th, 2015       Terri Jiles              Included same update from Case #999643 to be consistent with global process
 * Aug 12th, 2015       Terri Jiles              Renamed Order Credit WS

 =====================================================================*/


global without sharing class Order_Credit_WS {
  static Order__c creditOrder;

  static List<Group> copsList = [SELECT Id, DeveloperName FROM Group
                                 WHERE Id in (SELECT UserOrGroupId FROM GroupMember WHERE Group.Name =: Constants.GROUP_EDQ_COPS)];
  
  static Set<String> copsRoles = new Set<String>{'Global_Admin'};

  //End Case #630336
  static User currentUser {
    get {
      if (currentUser == null) {
        currentUser = [SELECT Id, UserRole.DeveloperName FROM User WHERE Id =: UserInfo.getUserId()];
      }
      return currentUser;
    }
    set;
  }

  webservice static String creditOrder (Id recordId) {
    //Start Case #630336
    for(Group cR : copsList) {
      // System.debug('copsList' + cR);
      String copRole = cR.DeveloperName;
      //system.debug( 'sObject to String  conversion : '  + copRole ) ;
      copsRoles.add(copRole);
      system.debug( 'FinalList: '+copsRoles) ;
    }
    //End Case #630336
    if (String.isBlank(currentUser.UserRoleId) || !copsRoles.contains(currentUser.UserRole.DeveloperName)) {
      return Label.Opportunity_Reopen_Not_a_COPS_User;
    }
    
    String respString = '';
    for (Order__c ord : [SELECT ID, Credited_Date__c, Type__c, Transactional_Sale__c, Locked__c,
                           (SELECT Id, Credited_Date__c, Credited__c, Status FROM Assets__r)
                         FROM Order__c WHERE ID =: recordId]) {
      creditOrder = ord;
      if (ord.Type__c == Constants.ORDER_TYPE_CREDITED) {
        return Label.ORDER_CREDIT_ALREADY_CREDITED;
      }
      if (ord.Transactional_Sale__c == false || ord.Locked__c == false) {
        return Label.ORDER_CREDIT_TRANSACTIONAL_REQUIRED;
      }
    }
    
    try {
      if (creditOrder != null) {
        creditOrder.Type__c = Constants.ORDER_TYPE_CREDITED;
        creditOrder.Credited_Date__c = System.today();
        update creditOrder;
        List<Asset> assets = new List<Asset>();
        for (Asset asst : creditOrder.Assets__r) {
          asst.Credited__c = true;
          asst.Credited_Date__c = System.today();
          //asst.Status = Constants.ASSET_TYPE_CREDITED;  Case #999643
          assets.add(asst);
        }
        if (assets.size() > 0) {
          update assets;
        }
        respString = Label.SAAS_ORD_CREDITED; 
      }
    } 
    catch (DmlException ex) {
      ApexLogHandler.createLogAndSave('OrderWebservices','creditOrder', ex.getStackTraceString(), ex);
      return Label.ORDER_CREDIT_SUCCESS;
    }
    return respString;
  }
}