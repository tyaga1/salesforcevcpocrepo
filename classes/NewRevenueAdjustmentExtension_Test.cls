/**=====================================================================
 * Appirio, Inc
 * Name: NewRevenueAdjustmentExtension_Test
 * Description: T-425827
 * 
 * Created Date: Aug 12th, 2015
 * Created By: Arpita Bose (Appirio)
 * 
 * Date Modified      Modified By                  Description of the update
 * Feb 9th, 2016      Paul Kissick                 Case 01185249: Rewrote to support new page.
*  =====================================================================*/
@isTest
private class NewRevenueAdjustmentExtension_Test {
  
  private static String testUserEmail = 'kjasfhaskhsfd87987987@experian.com';
    
  private static testMethod void testController(){    
     Product2 testProd = [
      SELECT Id, Family, Global_Business_Line__c, Business_Line__c 
      FROM Product2 
      WHERE IsActive = true 
      AND Name LIKE 'TestProduct%' LIMIT 1
    ];
    
    User testUser = [SELECT Id FROM User WHERE Email = :testUserEmail LIMIT 1];
    
    system.runAs(testUser) {
      
      Account testAcc = [SELECT Id FROM Account WHERE Name = 'TEST ACCOUNT NAME 123'];
    
      Revenue_Adjustment__c revAdj = new Revenue_Adjustment__c(Account__c = testAcc.Id);
    
      ApexPages.StandardController con = new ApexPages.StandardController(revAdj);
    
      NewRevenueAdjustmentExtension revExt = new NewRevenueAdjustmentExtension(con);
      
      system.assertEquals(1,revExt.getResultSize(),'Products not found.');
      system.assertNotEquals(0,revExt.getPageSize(),'Page size must be greater than 0.');
      system.assertEquals(1,revExt.getPageNumber(),'Page number should be 1.');
      system.assertEquals(1,revExt.getTotalPages(),'There should only be 1 page.');
      system.assertEquals(1,revExt.getResultSize(),'Products not found.');
      
      revExt.goToFirstPage();
      revExt.goToLastPage();
      revExt.goToNextPage();
      revExt.goToNext2Page();
      revExt.goToPrevPage();
      revExt.goToPrev2Page();
      
      system.assertEquals(1,revExt.getRecords().size());
      
      revExt.holdingProduct.productFamily = testProd.Family;
      revExt.holdingProduct.globalBusinessLine = testProd.Global_Business_Line__c;
      revExt.holdingProduct.businessLine = testProd.Business_Line__c;
      

      revExt.sortRecordset();
      
      system.assert(revExt.save() == null,'This shouldnt redirect.');
       
    }
      
  }
    
  @testSetup
  private static void testDataSetup() {
    
    TriggerSettings__c pmTrig = new TriggerSettings__c(Name = 'ProductMasterTrigger', IsActive__c = false);
    insert pmTrig;
    
    
    
    User testUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    testUser.Email = testUserEmail;
    insert testUser;
    
    CPQ_Settings__c testCPQSetting = new CPQ_Settings__c (
      Name = 'CPQ',
      Company_Code__c = 'Experian',
      CPQ_API_Access_Word__c = 'Accessword',
      CPQ_API_Endpoint__c= 'https://test.webcomcpq.com/',
      CPQ_API_UserName__c= 'TestUser#Experian'
    );
    insert testCPQSetting; 
    
    
    Country__c testCountry = Test_Utils.createCountry(true);
    Region__c testRegion = Test_Utils.createRegion(true);
    
    Schema.PicklistEntry prodFamily = Product2.sObjectType.getDescribe().fields.getMap().get('Family').getDescribe().getPicklistValues()[0];
    Schema.PicklistEntry gblPicklist = Product2.sObjectType.getDescribe().fields.getMap().get('Global_Business_Line__c').getDescribe().getPicklistValues()[0];
    Schema.PicklistEntry blPicklist = Product2.sObjectType.getDescribe().fields.getMap().get('Business_Line__c').getDescribe().getPicklistValues()[0];
        
    Product_Master__c pm = Test_Utils.createProductMaster(true);
    
    Product2 prod = Test_Utils.createProduct();
    prod.Family = prodFamily.getValue();
    prod.Global_Business_Line__c = gblPicklist.getValue();
    prod.Business_Line__c = blPicklist.getValue();
    prod.Product_Master__c = pm.Id;
    insert prod;
    PricebookEntry pbe = Test_Utils.insertPricebookEntry(prod.Id, Test.getStandardPricebookId(), 'USD');
    
    Product_Country__c pc = Test_Utils.createProductCountry(false, testCountry.Id);
    pc.Product__c = prod.Id;
    insert pc;
    
    Product_Region__c rg = Test_Utils.createProductRegion(false, testRegion.Id);
    rg.Product__c = prod.Id;
    insert rg;
    
    Account tstAcc = Test_Utils.createAccount();
    tstAcc.Name = 'TEST ACCOUNT NAME 123';
    insert tstAcc;
    
    system.runAs(new User(Id = UserInfo.getUserId())) {
      testUser.Region__c = testRegion.Name;
      testUser.Country__c = testCountry.Name;
      update testUser;
    }
    
  }

}