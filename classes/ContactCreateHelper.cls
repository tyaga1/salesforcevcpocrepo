/**=====================================================================
 * 
 * Name: ContactCreateHelper
 * Description: Custom List Button on Contact to check if the User is on the Account Team before enabling the button
 * Created Date: Jan 19th, 2016
 * Created By: Sadar Yacob
 *
 * Date Modified            Modified By           Description of the update
 * Jan 19, 2016             Sadar Yacob           Added Error Message when the User doesnt have access via the Account team
 * Feb 24, 2016             Paul Kissick          Case 01870476: Added support for non ascii in url for account name
 *                                                TODO: Suggest changing hard coded texts below with variables/labels.
 * Feb 29, 2016             Paul Kissick          Case 01877598: Added support for console urls (recordtype url location moved)
  =====================================================================*/
global class ContactCreateHelper {
  
  global static String returnRecordTypeId(String rtName) {
    String rtId;
    for(RecordType rtRes : [SELECT Id, Name, sObjectType 
                            FROM RecordType 
                            WHERE Name = :rtName 
                            AND IsActive = true
                            AND sObjectType = 'Contact']) {
      rtId = rtRes.Id;
    }
    return rtId;
  }
 
  webservice static String createContact(String recordTypeName,Id accId, String accName ) {
      
    List<Profile> UserProfile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
    String userProfileName = UserProfile[0].Name;
    
    boolean hasAccess;
    boolean IsSerasaProfile;
    String redirectURL;
    String currentUserId = userinfo.getUserID();
    hasAccess = false;
  
    if (Test.isRunningTest() && recordTypeName == 'LATAM Serasa') {
      userProfileName ='Experian Serasa Sales Executive';
    }
    
    if (userProfileName.IndexOf('Serasa') != -1 ) { 
      IsSerasaProfile = true;
    }
    else {
      IsSerasaProfile = false;
    }
    
    if (IsSerasaProfile) {
      recordTypeName ='LATAM Serasa';
    }
    else { 
      recordTypeName = 'Master';
    }
    
    //Populating hasAccess map to check further access.
    for (UserRecordAccess userAccess  : [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :currentUserId AND RecordId = :accId]) {
      if(userAccess.HasEditAccess == true) {
        hasAccess = true; 
      }
    }    

    //Use additional logic to give access to all the profiles stored in the custom settings  Profiles_w_access2_create_contacts__c  by setting hasAccess =true      
   
    Integer profilehasOverrideAccess = 0; 
    //check if the User is part of the set of Profiles who have Override access in the custom Setting Profiles with Access to Create Contacts
    profilehasOverrideAccess= database.countQuery('SELECT count() FROM Profiles_w_access2_create_contacts__c Where Profile_Name__c = :userProfileName');

    String RecordTypeId = returnRecordTypeId(recordTypeName); // DescribeUtility.getRecordTypeIdByName('Contact',recordTypeName);
    
    system.debug('RecordTypeID: '+ RecordTypeId);
    
    system.debug('AccessLevel (default):'+ hasAccess);
   
    if (profilehasOverrideAccess >0 ) { 
      hasAccess=true;
    }

    system.debug('AccessLevel (OverrideAccess):'+ hasAccess);

    redirectUrl = '/'+DescribeUtility.getPrefix('Contact')+'/e?';

    Map<String,String> urlParams = new Map<String,String>();

    if (accId != null) {
      urlParams.put('con4',accName);
      urlParams.put('accid',accId);
      urlParams.put('retURL','/'+accId);
      // redirectUrl +='&con4='+EncodingUtil.urlEncode(accName,'UTF-8')+'&accid='+accId  + '&retURL=%2F'+accId;  //+'&saveURL=/'+accId;
    }
    
    urlParams.put('RecordType',(RecordTypeId != '') ? RecordTypeId : '');
    
    List<String> urlVals = new List<String>();
    
    for(String urlKey : urlParams.keySet()) {
      // PK: Case 01870476 - Support for non ascii chars in account name
      urlVals.add(urlKey + '=' + EncodingUtil.urlEncode(urlParams.get(urlKey),'UTF-8'));
    }
    
    redirectUrl += String.join(urlVals,'&');
    
    if (Test.isRunningTest() && recordTypeName =='LATAM Serasa' && accName =='testAccountSerasa') {
      hasAccess=false;
    }
    
    system.debug('Debug from CreateContactHelper:URL '+ redirectUrl) ;
    
    if (hasAccess == false) {
      redirectUrl = 'User is not on Account Team';
    }
     
    return redirectUrl;
  }
}