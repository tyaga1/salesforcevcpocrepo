/*=====================================================================
 * Experian
 * Name: opportunityLogACallCustomController
 * Description: Case-#02004426: Log a call activities: default activity contact to be opportunity's primary contact.
 *
 * Created Date: June 29th, 2016
 * Created By: Manoj (Experian)
 *
 * Date Modified            Modified By                 Description of the update
  23rd Feb 2017             Manoj Gopu                  Updated the class to Update the Contact when log a call is Used from Contact and Similarly account as well 
 ======================================================================*/

public class opportunityLogACallCustomController {
       //This method is used to redirect to Activity page by clicking on Log A Call button in opportunity page
    public PageReference redirect() {
        //get the opportunity Id from the URL
        string optyId=ApexPages.currentPage().getParameters().get('opyId');     
        string conId=ApexPages.currentPage().getParameters().get('conId');
        string accId=ApexPages.currentPage().getParameters().get('accId');
        
        //Get the default record type id from the custom setting
       // string objRec=Record_Type_Ids__c.getInstance().Task_Standard_Task__c;
                
        List<Schema.RecordTypeInfo> infos = Schema.SObjectType.Task.RecordTypeInfos;
        Id defaultRecordTypeId;
        
        //check each one
        for (Schema.RecordTypeInfo info : infos) {
          if (info.DefaultRecordTypeMapping) {
            defaultRecordTypeId = info.RecordTypeId;
          }
        }
        if(optyId!=null && optyId!=''){
            //get the primary contacts in the specified opportunity to populate the primary contact
            list<OpportunityContactRole> oppConRole=[select id,OpportunityId,ContactId,IsPrimary from OpportunityContactRole where OpportunityId=:optyId AND IsPrimary=true limit 1];
            //If primary contact exist, it will redirect to the Activity page by populating the contact
            
            if(oppConRole!=null && oppConRole.size()>0)
            {
                PageReference p=new PageReference('/00T/e?title=Call&who_id='+oppConRole[0].ContactId+'&what_id='+optyId+'&followup=1&tsk5=Call&retURL=%2F'+optyId+'&RecordType='+defaultRecordTypeId+'&ent=Task');
                return p;
            }
            else
            {
                //If primary contact does not exist, then it will redirect to the Activity without populating the contact   
                PageReference p=new PageReference('/00T/e?title=Call&what_id='+optyId+'&followup=1&tsk5=Call&retURL=%2F'+optyId+'&RecordType='+defaultRecordTypeId+'&ent=Task');
                return p;
            }   
        }
        if(conId!=null && conId!=''){
            PageReference p=new PageReference('/00T/e?title=Call&who_id='+conId+'&followup=1&tsk5=Call&retURL=%2F'+conId+'&RecordType='+defaultRecordTypeId+'&ent=Task');
            return p;
        }
        if(accId!=null && accId!=''){
            PageReference p=new PageReference('/00T/e?title=Call&what_id='+accId+'&followup=1&tsk5=Call&retURL=%2F'+accId+'&RecordType='+defaultRecordTypeId+'&ent=Task');
            return p;
        }
        
        return null;
    }
    
    public opportunityLogACallCustomController()
    {
    
    }

}