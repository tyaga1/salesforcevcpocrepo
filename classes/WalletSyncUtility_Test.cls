/**=====================================================================
 * Experian
 * Name: WalletSyncUtility_Test
 * Description: 
 * 
 * Created Date: 12th August, 2015
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class WalletSyncUtility_Test {

  static testMethod void testWalletSyncUtils() {
    
    Global_Settings__c gs = new Global_Settings__c(Name = 'Global');
    insert gs;
    
    system.assertEquals(false,WalletSyncUtility.isProcessingRunning());
    system.assertEquals(false,WalletSyncUtility.isUploadingRunning());
    
    gs.WalletIntegn_StartDate__c = system.now().addMinutes(-40);
    update gs;
    system.assertEquals(true,WalletSyncUtility.isUploadingRunning());
    
    gs.WalletIntegn_CompletionDate__c = system.now().addMinutes(-30);
    update gs;
    system.assertEquals(false,WalletSyncUtility.isUploadingRunning());
    
    gs.WalletIntegn_StartDate__c = system.now().addMinutes(-20);
    update gs;
    system.assertEquals(true,WalletSyncUtility.isUploadingRunning());
    
    gs.WalletIntegn_CompletionDate__c = system.now().addMinutes(-10);
    update gs;
    system.assertEquals(false,WalletSyncUtility.isUploadingRunning());
    
    system.assertEquals(false,WalletSyncUtility.isProcessingRunning());
    WalletSyncUtility.processingStarted();
    
    system.assertEquals(true,WalletSyncUtility.isProcessingRunning());
    WalletSyncUtility.processingCompleted();
    
    system.assertEquals(false,WalletSyncUtility.isProcessingRunning());
    
    
  }

}