/**=====================================================================
 * Appirio, Inc
 * Name: TriggerState_Test
 * Description: Test Class for TriggerState
 * Created Date: Sept 09, 2015
 * Created By: Naresh Kumar Ojha (Appirio)
 *
 * Date Modified      Modified By                Description of the update
 * Jan 29th, 2016     Paul Kissick               Test failing for some reason - Fixed 
 * Apr 7th, 2016      Paul Kissick               Case 01932085: Fixing Test User Email Domain
 =====================================================================*/
@isTest
private class TriggerState_Test {

 static testMethod void test_isActive() {

    // create User
    Profile p = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN];
    
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    insert testUser1;

    system.runAs(testUser1) {
      Boolean activeTrigger1 = false;
      Boolean activeTrigger2 = false;
      Boolean activeTrigger3 = false;
      
      Test.startTest();
      
      activeTrigger1 = TriggerState.isActive('AccountPlanTestTrigger');
      activeTrigger2 = TriggerState.isActive('TestTrigger');
      activeTrigger3 = TriggerState.isActive('AccountTrigger');

      Test.stopTest();
      
      system.assertEquals(true, activeTrigger1);
      system.assertEquals(false, activeTrigger2);
      system.assertEquals(true, activeTrigger3);
    }
  }
  
  @testSetup
  private static void setupData() {
    List<TriggerSettings__c> triggerSettings = new List<TriggerSettings__c>();
    TriggerSettings__c aptt = new TriggerSettings__c(Name = 'AccountPlanTestTrigger');
    aptt.IsActive__c = true;
    triggerSettings.add(aptt);

    TriggerSettings__c tt = new TriggerSettings__c(Name = 'TestTrigger');
    tt.IsActive__c = false;
    triggerSettings.add(tt);

    insert triggerSettings;
  }
  
}