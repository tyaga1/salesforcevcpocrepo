/**=====================================================================
 * Name: AccountPlanPDFGeneratorExtension_Test
 *
 * Description: 
 *
 * Created Date: Feb. 20th, 2016
 * Created By: James Wills
 * 
 * Date Modified         Modified By            Description of the update
 * Feb. 20th, 2017       James Wills            Case #01999757 Created.
 * =====================================================================*/
@isTest
private class AccountPlanPDFGeneratorExtension_Test{

   private static Set<String> validFieldsForList = new Set<String>{Label.ACCOUNT_PLAN_PDF_CUST_INFO,
                                                   Label.ACCOUNT_PLAN_PDF_OBJECTIVES, 
                                                   Label.ACCOUNT_PLAN_PDF_CLIENT_INFORMATION,
                                                   Label.ACCOUNT_PLAN_PDF_RECENT_HISTORY,
                                                   Label.ACCOUNT_PLAN_PDF_PENETRATION,
                                                   Label.ACCOUNT_PLAN_PDF_RELATIONSHIP,
                                                   Label.ACCOUNT_PLAN_PDF_RELATIONSHIP_RADAR,
                                                   Label.ACCOUNT_PLAN_PDF_DASHBOARD,
                                                   Label.ACCOUNT_PLAN_PDF_COMPETITORS,
                                                   Label.ACCOUNT_PLAN_PDF_CURRENT_PROVIDERS,
                                                   Label.ACCOUNT_PLAN_PDF_SWOT_ANALYSIS,
                                                   Label.ACCOUNT_PLAN_PDF_OPPORTUNITIES,
                                                   Label.ACCOUNT_PLAN_PDF_CRITICAL_SUCCESS_FACTORS,
                                                   Label.ACCOUNT_PLAN_PDF_NOTES,
                                                   Label.ACCOUNT_PLAN_PDF_ACCOUNT_TEAM,
                                                   Label.ACCOUNT_PLAN_PDF_CONTACTS};


  
  private static testmethod void testGenerateAccountPlanPDF(){
   
    // start test     
    Test.startTest();
      PageReference pageRef = Page.AccountPlanPDFGenerator;        
      Test.setCurrentPage(pageRef);
      Account_Plan__c newAccountPlan=[SELECT id FROM Account_Plan__c LIMIT 1];
      ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)newAccountPlan);
      AccountPlanPDFGeneratorExtension apPDFGenerator = new AccountPlanPDFGeneratorExtension(stdController); 
      

                
      apPDFGenerator.selectedSchemaId=[SELECT id FROM Account_Plan_PDF_Data__c LIMIT 1].id;
      apPDFGenerator.deleteSchema();
  
      apPDFGenerator.selectedSchemaId=[SELECT id FROM Account_Plan_PDF_Data__c LIMIT 1].id;
      apPDFGenerator.deleteSchema();
  
      //apPDFGenerator.selectedSchemaId=[SELECT id FROM Account_Plan_PDF_Data__c WHERE id=:apPDFGenerator.selectedSchemaId LIMIT 1].id;
      
      system.assert([SELECT id FROM Account_Plan_PDF_Data__c LIMIT 1].isEmpty()==true, 'AccountPlanPDFGeneratorExtension_Test: AccountPlanPDFGeneratorExtension.deleteSchema() has not deleted the selected PDF design.');
  
    Test.stopTest();
    
  }
  
  private static testmethod void test_PDFGeneratorScreenNavigation(){
  
    String type='Account_Plan_PDF_Data__c';

    Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    Schema.SObjectType pdfDataSchema = schemaMap.get(type);
    Map<String, Schema.SObjectField> fieldMap = pdfDataSchema.getDescribe().fields.getMap();  
        
    
    // start test     
    Test.startTest();
      PageReference pageRef = Page.AccountPlanPDFGenerator;        
      Test.setCurrentPage(pageRef);
      Account_Plan__c newAccountPlan=[SELECT id FROM Account_Plan__c LIMIT 1];
      ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)newAccountPlan);
      AccountPlanPDFGeneratorExtension apPDFGenerator = new AccountPlanPDFGeneratorExtension(stdController); 
      
      apPDFGenerator.selectedSectionFromScreen_List = new List<String>();      
      apPDFGenerator.deSelectedSectionFromScreen_List = new List<String>();
      
      apPDFGenerator.selectedSchemaId=[SELECT id FROM Account_Plan_PDF_Data__c LIMIT 1].id;
      
      List<SelectOption> getSel = apPDFGenerator.getSelectedSectionsListFromData();
      
      system.assert(apPDFGenerator.selectedSectionClass_List.isEmpty()==false,                 'AccountPlanPDFGeneratorExtension_Test: selectedSectionClass_List.size() is empty.');
      system.assert(apPDFGenerator.pdfBuildInstructionsUnselected_SelectList.isEmpty()==false, 'AccountPlanPDFGeneratorExtension_Test: pdfBuildInstructionsUnselected_SelectList is empty.');
      
      for (String fieldName: fieldMap.keySet()){            
        apPDFGenerator.pdfBuildInstructionsUnselected_SelectList.add(new SelectOption(fieldName,fieldMap.get(fieldName).getDescribe().getLabel()));                 
      }
      
      apPDFGenerator.selectSection();
      
      for (String fieldName: fieldMap.keySet()){
        apPDFGenerator.selectedSectionFromScreen_List.add(fieldName);
      }

      apPDFGenerator.selectSection();                
      apPDFGenerator.deSelectSection();
      
      apPDFGenerator.deSelectedSectionFromScreen_List = apPDFGenerator.selectedSectionFromScreen_List;
      
      apPDFGenerator.deSelectSection();      
      apPDFGenerator.promoteSection();            
               
      for (String fieldName: fieldMap.keySet()){
         apPDFGenerator.selectedSectionsList.add(new SelectOption(fieldName,fieldMap.get(fieldName).getDescribe().getLabel()));
      }      
      apPDFGenerator.deSelectedSectionFromScreen_List = new List<String>();
      apPDFGenerator.deSelectedSectionFromScreen_List.add(apPDFGenerator.selectedSectionsList[1].getValue());

      apPDFGenerator.promoteSection();
      System.assert(apPDFGenerator.deSelectedSectionFromScreen_List[apPDFGenerator.deSelectedSectionFromScreen_List.size()-1]!=apPDFGenerator.selectedSectionsList[1].getValue(),'AccountPlanPDFGeneratorExtension_Test: Error with promoteSection()');
      //apPDFGenerator.selectedSectionFromScreen = Label.ACCOUNT_PLAN_PDF_CLIENT_INFORMATION;
      apPDFGenerator.demoteSection();
      System.assert(apPDFGenerator.deSelectedSectionFromScreen_List[apPDFGenerator.deSelectedSectionFromScreen_List.size()-1]==apPDFGenerator.selectedSectionsList[1].getValue(),'AccountPlanPDFGeneratorExtension_Test: Error with demoteSection()');
      
      //Navigation
      apPDFGenerator.selectAll();
      System.assert(apPDFGenerator.showCustInfo==true, 'AccountPlanPDFGeneratorExtension_Test: apPDFGenerator.showCustInfo not set to true.');
      
      apPDFGenerator.deSelectAll();
      System.assert(apPDFGenerator.showCustInfo==false, 'AccountPlanPDFGeneratorExtension_Test: apPDFGenerator.showCustInfo not set to false.');
      
      apPDFGenerator.addAllSections();
      for(SelectOption selectedSection : apPDFGenerator.selectedSectionsList){
        System.assert(validFieldsForList.contains(selectedSection.getValue())==true,'AccountPlanPDFGeneratorExtension_Test: apPDFGenerator.selectedSectionsList is missing a value that should be present.');
      } 
            
      apPDFGenerator.removeAllSections();
      for(SelectOption selectedSection : apPDFGenerator.selectedSectionsList){
        System.assert(validFieldsForList.contains(selectedSection.getValue())==false,'AccountPlanPDFGeneratorExtension_Test: apPDFGenerator.selectedSectionsList has a value that should be absent.');
      } 
      
      apPDFGenerator.selectAllCustInfo();
      system.assert(apPDFGenerator.showExperianGoals==true,'AccountPlanPDFGeneratorExtension_Test: selectAllCustInfo() - not all values set to true.');        
      apPDFGenerator.deSelectAllCustInfo(); 
      system.assert(apPDFGenerator.showExperianGoals==false,'AccountPlanPDFGeneratorExtension_Test: selectAllCustInfo() - not all values set to false.');
      
      apPDFGenerator.selectAllClientInfo();
      system.assert(apPDFGenerator.showClientsCoreBusiness==true,'AccountPlanPDFGeneratorExtension_Test: selectAllClientInfo() - not all values set to true.'); 
      apPDFGenerator.deSelectAllClientInfo();  
      system.assert(apPDFGenerator.showClientsCoreBusiness==false,'AccountPlanPDFGeneratorExtension_Test: selectAllClientInfo() - not all values set to false.');
      
      apPDFGenerator.selectAllRecentHistory();
      system.assert(apPDFGenerator.showSummaryRecentHistory ==true,'AccountPlanPDFGeneratorExtension_Test: selectAllRecentHistory() - not all values set to true.');
      apPDFGenerator.deSelectAllRecentHistory();
      system.assert(apPDFGenerator.showSummaryRecentHistory ==false,'AccountPlanPDFGeneratorExtension_Test: selectAllRecentHistory() - not all values set to false.');
      
      PageReference apPage = apPDFGenerator.backToAccountPlan();
      
    Test.stopTest();
  
  }
 
  private static testmethod void test_PDFGeneratorCreatePDF(){
  
    // start test     
    Test.startTest();
      PageReference pageRef = Page.AccountPlanPDFGenerator;        
      Test.setCurrentPage(pageRef);
      Account_Plan__c newAccountPlan=[SELECT id FROM Account_Plan__c LIMIT 1];
      ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)newAccountPlan);
      AccountPlanPDFGeneratorExtension apPDFGenerator = new AccountPlanPDFGeneratorExtension(stdController); 

      apPDFGenerator.selectedSchemaId=[SELECT id FROM Account_Plan_PDF_Data__c WHERE Show_Clients_Objectives__c = false LIMIT 1].id;
      apPDFGenerator.selectedSchema=[SELECT id FROM Account_Plan_PDF_Data__c LIMIT 1];
      
      List<SelectOption> getItemsList = apPDFGenerator.getItems();
      
      apPDFGenerator.selectAll();
      
      apPDFGenerator.selectAllCustInfo();  
      apPDFGenerator.selectAllClientInfo();  
      apPDFGenerator.selectAllRecentHistory();      
      apPDFGenerator.saveSchema();
      System.assert([SELECT id FROM Account_Plan_PDF_Data__c WHERE id=:apPDFGenerator.selectedSchemaId AND Show_Clients_Objectives__c =true].isEmpty()==false, 'AccountPlanPDFGeneratorExtension_Test: Account Plan PDF Design was not updated correctly.');
            
      apPDFGenerator.selectAll();      
      apPDFGenerator.updateSavedSchema(apPDFGenerator.selectedSchema);
      
      apPDFGenerator.accountPlanId=newAccountPlan.id;
      
      PageReference newPageRef = apPDFGenerator.buildPDFDesign();
        
      
      apPDFGenerator.selectedSchema.AccountPlanPDFClientInformation__c =2;
      apPDFGenerator.saveMessage();
      //apPDFGenerator.saveSchema();
      //apPDFGenerator.saveMessage();
      
      apPDFGenerator.newUserSchema='New Schema';
      apPDFGenerator.saveSchema(); 
      System.assert([SELECT id FROM Account_Plan_PDF_Data__c WHERE Name =:apPDFGenerator.newUserSchema].isEmpty()==true, 'AccountPlanPDFGeneratorExtension_Test: new Account Plan PDF Design not saved.');           
    
    Test.stopTest();

  }
  
 
 
  @testSetup
  private static void setupData(){
    Account_Plan__c newAccountPlan = new Account_Plan__c();
    insert newAccountPlan;   
                                                                      
    List<Account_Plan_SWOT__c> apSWOTList = new List<Account_Plan_SWOT__c>();   
    List<String> swotType =new List<String>{'Strength','Weakness','Opportunity','Threat'};

    Integer i;
    for(i=0;i<swotType.size();i++){
      Account_Plan_SWOT__c newAccountPlanSWOT = new Account_Plan_SWOT__c(Account_Plan__c=newAccountPlan.id,
                                                                         Type__c=swotType[i],
                                                                         Who__c='Experian',
                                                                         Importance__c='5');
      apSWOTList.add(newAccountPlanSWOT);
    }
    for(i=0;i<swotType.size();i++){
      Account_Plan_SWOT__c newAccountPlanSWOT = new Account_Plan_SWOT__c(Account_Plan__c=newAccountPlan.id,
                                                                         Type__c=swotType[i],
                                                                         Who__c='Client',
                                                                         Importance__c='5');
      apSWOTList.add(newAccountPlanSWOT);
    }   
   
    insert apSWOTList;
 
    Account_Plan_PDF_Data__c apPDFData = new Account_Plan_PDF_Data__c();
   
    apPDFData.AccountPlanPDFCustInfo__c               =1;
    apPDFData.AccountPlanPDFClientInformation__c      =0; 
    apPDFData.AccountPlanPDFContacts__c               =0; 
    apPDFData.AccountPlanPDFAccountTeam__c            =0;
    apPDFData.AccountPlanPDFCompetitors__c            =0; 
    apPDFData.AccountPlanPDFCurrentProviders__c       =0; 
    apPDFData.AccountPlanPDFDashboard__c              =0;
    apPDFData.AccountPlanPDFNotes__c                  =0; 
    apPDFData.AccountPlanPDFObjectives__c             =0;
    apPDFData.AccountPlanPDFOpportunities__c          =0; 
    apPDFData.AccountPlanPDFPenetration__c            =0; 
    apPDFData.AccountPlanPDFRecentHistory__c          =0; 
    apPDFData.AccountPlanPDFRelationship__c           =0;
    apPDFData.AccountPlanPDFRelationshipRadar__c      =0;
    apPDFData.AccountPlanPDFSWOTAnalysis__c           =0;
    apPDFData.AccountPlanPDFCriticalSuccessFactors__c =0;
   
    insert apPDFData;
   
    Account_Plan_PDF_Data__c apPDFData2 = new Account_Plan_PDF_Data__c();
   
    apPDFData2.Name                                   ='Test PDF Create';
   
    apPDFData2.AccountPlanPDFCustInfo__c               =1;
    apPDFData2.AccountPlanPDFClientInformation__c      =2; 
    apPDFData2.AccountPlanPDFContacts__c               =3; 
    apPDFData2.AccountPlanPDFAccountTeam__c            =4;
    apPDFData2.AccountPlanPDFCompetitors__c            =5; 
    apPDFData2.AccountPlanPDFCurrentProviders__c       =6; 
    apPDFData2.AccountPlanPDFDashboard__c              =7;
    apPDFData2.AccountPlanPDFNotes__c                  =8; 
    apPDFData2.AccountPlanPDFObjectives__c             =9;
    apPDFData2.AccountPlanPDFOpportunities__c          =10; 
    apPDFData2.AccountPlanPDFPenetration__c            =11; 
    apPDFData2.AccountPlanPDFRecentHistory__c          =12; 
    apPDFData2.AccountPlanPDFRelationship__c           =13;
    apPDFData2.AccountPlanPDFRelationshipRadar__c      =14;
    apPDFData2.AccountPlanPDFSWOTAnalysis__c           =15;
    apPDFData2.AccountPlanPDFCriticalSuccessFactors__c =16;
    apPDFData2.Show_Clients_Objectives__c              =false;
    
    insert apPDFData2;
 
  } 
  
}