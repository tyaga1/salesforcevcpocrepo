/**=====================================================================
 * Experian
 * Name: CaseTriggerHandler_BusinessHours
 * Description: Specific handling of business hour settings
 * Created Date: Nov 2nd, 2015
 * Created By: Paul Kissick
 * 
 * Date Modified        Modified By          Description of the update
 * Apr 14th, 2016       Paul Kissick         Case 01661370 - Moved all BusinessHoursId related function into this class
 * Apr 18th, 2016       Sadar Yacob          Case 01859707 - Renamed Initial_Resp_Time_Bus_Hrs__c to   Initial_Resp_Time_Bus_Mins__c
 * Apr 25th, 2016       Paul Kissick         Case 01794099 - Added some optimisations for record type queries
 * May 24th, 2016       Paul Kissick         Case 01999003 - Bug in calculateCaseMetrics 
 * Aug 12th, 2016       Paul Kissick         Case 01985974 - Adding support for bypassing if Business Hours already set
 =====================================================================**/
public class CaseTriggerHandler_BusinessHours {
  
  // Store these in static variables to prevent constant requerying
  public static Map<String,BusinessHours> businessHoursMap = new Map<String,BusinessHours>();
  public static Map<Id,String> rtIdToNameMap = new Map<Id,String>();
  public static Map<Id,String> userRoleMap = new Map<Id,String>();
  public static Map<Id,String> queueNameMap = new Map<Id,String>(); 
  
  public static BusinessHours defaultBusinessHours;

  public static void populateBusinessHourId (List<Case> newCases, Map<Id,Case> oldCaseMap) {
    
    List<Business_Hour_Assignment__c> allBsAssigns = new List<Business_Hour_Assignment__c>();
    
    Set<String> bhNames = new Set<String>();
    for (Business_Hour_Assignment__c bha : Business_Hour_Assignment__c.getAll().values()) {
      // This If statement is designed to prevent defaulting for every case. Remove it if we want to use a default
      if (String.isNotBlank(bha.Record_Type_Name__c) ||
          String.isNotBlank(bha.Role_Name__c) ||
          String.isNotBlank(bha.Queue_Name__c)) {
        allBsAssigns.add(bha);
        bhNames.add(bha.Business_Hours_Name__c);
      }
    }
    if (bhNames.isEmpty()) {
      // No business hour assignments, so leave.
      return;
    }
    
    // SOQL Query 1 - Populate map of business hour name to id
    if (businessHoursMap.isEmpty() && !bhNames.isEmpty()) {
      for (BusinessHours hour : [SELECT Id, Name, IsDefault
                                 FROM BusinessHours 
                                 WHERE Name IN :bhNames OR IsDefault = true]) {
        businessHoursMap.put(hour.Name, hour);
        if (hour.IsDefault && defaultBusinessHours == null) {
          defaultBusinessHours = hour;
        }
      }
    }
    
    // Set<Id> newRecordTypeIds = new Set<Id>();
    Set<Id> newQueueIds = new Set<Id>();
    Set<Id> newUserIds = new Set<Id>();
    
    for (Case c : newCases) {
      if (c.RecordTypeId != null && !rtIdToNameMap.containsKey(c.RecordTypeId)) {
        rtIdToNameMap.put(c.RecordTypeId, CaseTriggerHandler.caseRecordTypeMap.get(c.RecordTypeId).Name);
      }
      if (c.OwnerId.getsObjectType() == Group.sobjectType && !queueNameMap.containsKey(c.OwnerId)) {
        // group/queue owned case...
        newQueueIds.add(c.OwnerId);
      }
      if (c.OwnerId.getsObjectType() == User.sobjectType && !userRoleMap.containsKey(c.OwnerId)) {
        // group/queue owned case...
        newUserIds.add(c.OwnerId);
      }
    }
    
    // Load into the queueNameMap
    if (newQueueIds.size() > 0) {
      // SOQL Query 3 - Return the group names and store
      for (Group q : [SELECT Id, Name
                       FROM Group
                       WHERE Id IN :newQueueIds]) {
        queueNameMap.put(q.Id, q.Name);
      }
    }
    
    if (newUserIds.size() > 0) {
      // SOQL Query 4 - Return the role names and store
      for (User usr : [SELECT userRoleId,userRole.Name ,Id 
                       FROM User
                       WHERE Id IN :newUserIds]) {
        if (usr.UserRoleId != null) {
          userRoleMap.put(usr.Id, usr.userRole.Name);
        }
      }
    }
    
    
    // Now, cycle through each new case and depending on the settings, assign a business hour Id
    for (Case newCase : newCases) {
      
      system.debug('current business hour id '+newCase.BusinessHoursId);
      
      String caseRecordTypeName;
      String caseQueueName;
      String caseRoleName;
      
      // Only replace the business hours if currently set the default.
      if ((defaultBusinessHours != null && (newCase.BusinessHoursId == defaultBusinessHours.Id || newCase.BusinessHoursId == null)) || 
          defaultBusinessHours == null) {
      
        if (rtIdToNameMap.containsKey(newCase.RecordTypeId)) {
          caseRecordTypeName = rtIdToNameMap.get(newCase.RecordTypeId);
        }
        if (queueNameMap.containsKey(newCase.OwnerId)) {
          caseQueueName = queueNameMap.get(newCase.OwnerId);
        }
        if (userRoleMap.containsKey(newCase.OwnerId)) {
          caseRoleName = userRoleMap.get(newCase.OwnerId);
        }
        
        for (Business_Hour_Assignment__c bha : allBsAssigns) {
          // Either we match on a recordtype name OR there is no record type name to match to...
          if ((String.isNotBlank(bha.Record_Type_Name__c) && String.isNotBlank(caseRecordTypeName) && caseRecordTypeName == bha.Record_Type_Name__c) || 
              String.isBlank(bha.Record_Type_Name__c)) {
            // we have a match, now check the rest...
            if ((String.isNotBlank(bha.Queue_Name__c) && String.isNotBlank(caseQueueName) && caseQueueName == bha.Queue_Name__c) ||
                (String.isNotBlank(bha.Role_Name__c) && String.isNotBlank(caseRoleName) && caseRoleName == bha.Role_Name__c) ||
                (String.isBlank(bha.Role_Name__c) && String.isBlank(bha.Queue_Name__c))) {
              // MATCHED
              if (bha.Is_Afternoon_Assignment_Only__c == false) {
                newCase.BusinessHoursId = businessHoursMap.get(bha.Business_Hours_Name__c).Id;
              }
              else {
                newCase.Business_Hours_Afternoon__c = businessHoursMap.get(bha.Business_Hours_Name__c).Id;
              }
              continue;
            }
          }
        }
      }
      if (newCase.BusinessHoursId != null) {
        // If this field is empty, always populate, or if the business hours have changed
        if (newCase.Active_Open_Time__c == null || (oldCaseMap != null && oldCaseMap.get(newCase.Id).BusinessHoursId != newCase.BusinessHoursId)) {
          // Now also set the active open time (new field for EDQ)
          Datetime activeOpenTime = (newCase.CreatedDate == null) ? system.now() : newCase.CreatedDate;
          newCase.Active_Open_Time__c = System.BusinessHours.nextStartDate(newCase.BusinessHoursId, activeOpenTime);
          system.debug('Setting Active Open Time to: '+newCase.Active_Open_Time__c);
        }
      }
    }
  }
  
  
  //=========================================================================
  // T-428069 & T-428073 - Method catculates case close time difference 
  // and updates case.Case_Close_Time__c & Case_Response_Time__c
  // Case 01194688: Adding check for Business Hours Diff (in Minutes)
  //=========================================================================
  public static void calculateCaseMetrics(List<Case> lstNew, Map<Id, Case> oldMap) {
    Case tempCase;
    Decimal closeTimeDiff;
    Decimal responseTimeDiff;
    Decimal bhTimeDiff;
    DateTime oldClosedDate;
    DateTime oldResponseDate;
    
    Map<Id, Case> caseMap = new Map<Id, Case>();
        
    // Loop through case list
    for (Case cse : lstNew) {
      // Case 01859707: MRodriguez
      // PK: Adding check for oldmap != null  as it broke all case creation
      if (oldMap != null &&
          oldMap.get(cse.ID).Status == Label.Case_Status_New && 
          oldMap.get(cse.ID).Status != cse.Status && 
          cse.BusinessHoursId != null &&
          cse.Initial_Resp_Time_Bus_Mins__c == null) {
        if (!caseMap.containsKey(cse.Id)) {
          caseMap.put(cse.Id, new Case(Id = cse.Id));
        }
        DateTime statusChange = System.BusinessHours.nextStartDate(cse.BusinessHoursId, DateTime.now());
        // Calculate the difference between newChangedTime and staticTime using the BusinessHours
        Decimal businessMillisecondsDiff = System.BusinessHours.diff(cse.BusinessHoursId, cse.createdDate, statusChange);

        caseMap.get(cse.Id).Initial_Resp_Time_Bus_Mins__c = businessMillisecondsDiff / (1000.0*60.0); //In Minutes
      }


      oldClosedDate = (oldMap != null) ? oldMap.get(cse.id).closedDate : null;
      oldResponseDate = (oldMap != null) ? oldMap.get(cse.id).Initial_Response_Date__c : null;
      closeTimeDiff = calculateTimeDifference(oldClosedDate, cse.closedDate, cse.createdDate, cse.Case_Close_Time__c, cse.businessHoursId);
      responseTimeDiff = calculateTimeDifference(oldResponseDate, cse.Initial_Response_Date__c, cse.createdDate, cse.Case_Response_Time__c, cse.businessHoursId);
      bhTimeDiff = (oldMap != null && cse.ClosedDate != null && cse.Minutes_To_Close__c == null) ? System.BusinessHours.diff(cse.BusinessHoursId, cse.createdDate, cse.closedDate) : null;

      // If difference is not null update Case_Close_Time__c/Case_Response_Time__c
      if (closeTimeDiff != null || responseTimeDiff != null || bhTimeDiff != null) {
        if (!caseMap.containsKey(cse.Id)) {
          caseMap.put(cse.Id, new Case(id = cse.id));
        }
        if (closeTimeDiff != null) {
          caseMap.get(cse.Id).Case_Close_Time__c = closeTimeDiff;
        }
        if (responseTimeDiff != null) {
          caseMap.get(cse.Id).Case_Response_Time__c = responseTimeDiff;
        }
        if (bhTimeDiff != null) {
          caseMap.get(cse.Id).Minutes_To_Close__c = Integer.valueOf(bhTimeDiff / (1000*60)); // Case 01194688
        }
      }
    }

    // Update cases
    if (!caseMap.isEmpty()) {
      try {
        update caseMap.values();
      }
      catch (Exception ex) {
        system.debug('[CaseTriggerHandler:calculateCaseMetrics]' + ex.getMessage()); 
        ApexLogHandler.createLogAndSave('CaseTriggerHandler','calculateCaseMetrics', ex.getStackTraceString(), ex);
      }
    }
  }
  
  //===========================================================================
  // As per I-185957
  // PK: Added extra if checks for BusinessHoursId being completed and Active 
  //  Open being completed.
  // Case 01661968 - Adding fix for checking closed status
  // Case 01817252 - Adding fix to reset Active Close Time to null when case is re-opened
  //===========================================================================
  public static void populateActiveCloseTimeForEDQ(list<Case> newCases, Map<ID, Case> oldMap) {
    Boolean caseHasBeenClosed = false;
    
    Set<String> closedStatuses = new Set<String>();
    
    for (CaseStatus cStatus : CaseTriggerHandler.getClosedCaseStatuses()) {
      closedStatuses.add(cStatus.MasterLabel);                          
    }
    
    for (Case cs : newCases) {
      if (closedStatuses.contains(cs.Status) && 
          oldMap.get(cs.ID).Status != cs.Status && 
          cs.BusinessHoursId != null && 
          cs.Active_Open_Time__c != null) {
        caseHasBeenClosed = true;
      }
      //Case # 01817252 : Reset the Active Close Time, Time Taken on Case, when Case is reopened 
      else if (closedStatuses.contains(oldMap.get(cs.ID).Status) && 
               oldMap.get(cs.ID).Status != cs.Status && 
               cs.BusinessHoursId != null && 
               cs.Active_Open_Time__c != null) {
        cs.Active_Close_Time__c = null;
        cs.Time_Taken_On_Case__c = null;
        system.debug('Case not closed: '+cs.Id);
        system.debug(cs);
      }
    }
    if (!caseHasBeenClosed) {
      // No case has been closed, so exit here.
      return;
    }
    
    for (Case cs : newCases) {
      if (closedStatuses.contains(cs.Status) && 
          oldMap.get(cs.ID).Status != cs.Status && 
          cs.BusinessHoursId != null && 
          cs.Active_Open_Time__c != null && 
          cs.Active_Close_Time__c == null) {
        cs.Active_Close_Time__c = System.BusinessHours.nextStartDate(cs.BusinessHoursId, DateTime.now());
        
        // Calculate the difference between newChangedTime and staticTime using the BusinessHours
        Decimal businessMillisecondsDiff = System.BusinessHours.diff(cs.BusinessHoursId, cs.Active_Open_Time__c, cs.Active_Close_Time__c);        
            
        String outputParam = convertMilliSecondsToDaysHoursMin(businessMillisecondsDiff, cs.BusinessHoursId);

        cs.Time_Taken_On_Case__c = outputParam;
      }
    }
  }
  
  //===========================================================================
  // Case 01661370 - New method to preset new case fields
  //=========================================================================== 
  public static void initialiseTimeOpenCaseFields(List<Case> newCases) {
    for (Case c : newCases) {
      c.Last_Status_Change__c = system.now();
      c.Time_While_Open__c = 0;
    }
  }
  
  //===========================================================================
  // Case 01661370 - New method to calculate Time While Open, 
  // and reset Last Status Change
  //=========================================================================== 
  public static void calculateTimeWhileOpen(List<Case> newCases, Map<Id,Case> oldCaseMap) {
    Set<String> closedStatuses = new Set<String>();
    
    for (CaseStatus cStatus : CaseTriggerHandler.getClosedCaseStatuses()) {
      closedStatuses.add(cStatus.MasterLabel);                          
    }
    
    for (Case updCase : newCases) {
      // Has status changed?
      if (oldCaseMap.get(updCase.Id).Status != updCase.Status && updCase.Last_Status_Change__c != null) {
        // Only update the time while open if the case was open!
        if (!closedStatuses.contains(oldCaseMap.get(updCase.Id).Status)) {
          Id busHoursId = updCase.BusinessHoursId;
          if (updCase.BusinessHoursId != null) {
            Decimal timeSinceLastStatus = BusinessHours.diff(busHoursId, updCase.Last_Status_Change__c, system.now()); // Return in minutes
            system.debug(timeSinceLastStatus + ' in milliseconds');
            updCase.Time_While_Open__c += timeSinceLastStatus.divide(60000,3);
          }
        }
        updCase.Last_Status_Change__c = system.now();
      }
    }
  }
  
  //===========================================================================
  // Converts milliseconds to Day:Hour:Min
  // Case #01661968 - Adding fix for days being locked to 8 hours. 
  //=========================================================================== 
  public static String convertMilliSecondsToDaysHoursMin(Decimal milliseconsParam, Id businessHourId) {
    String output = '{0}:{1}:{2}';
    List<String> outputParams = new String[]{'00','00','00'};
    if (milliseconsParam > 0) {
      Long different = milliseconsParam.longValue();
      Long secondsInMilli = 1000;
      Long minutesInMilli = secondsInMilli * 60;
      Long hoursInMilli = minutesInMilli * 60;
      Long daysInMilli = hoursInMilli * 8;
      if (businessHourIdToHoursPerDayMap.containsKey(businessHourId)) {
        daysInMilli = Integer.valueOf(hoursInMilli * (businessHourIdToHoursPerDayMap.get(businessHourId)));
      }
    
      Long elapsedDays = different / daysInMilli;
      different = Math.mod(different, daysInMilli);
      outputParams.set(0, String.valueOf(elapsedDays).leftPad(2,'0'));
      
      Long elapsedHours = different / hoursInMilli;
      different = Math.mod(different, hoursInMilli);
      outputParams.set(1, String.valueOf(elapsedHours).leftPad(2,'0'));
        
      Long elapsedMinutes = different / minutesInMilli;
      different = Math.mod(different, minutesInMilli);
      outputParams.set(2, String.valueOf(elapsedMinutes).leftPad(2,'0'));
    }
    
    return String.format(output, outputParams);
  }
  
  //=========================================================================
  // Generic method to calculate difference between two date/time using BusinessHours
  //=========================================================================
  private static Decimal calculateTimeDifference (DateTime oldChangedTime, DateTime newChangedTime, 
                                                            DateTime staticTime, Decimal timeDifference, String businessHoursId) {     
    decimal businessDaysDiff;
    decimal businessMillisecondsDiff;
    if (oldChangedTime == null && newChangedTime != null && timeDifference == null && businessHoursId != null) {
      // Calculate the difference between newChangedTime and staticTime using the BusinessHours
      businessMillisecondsDiff = System.BusinessHours.diff(businessHoursId, staticTime, newChangedTime);        
      // Convert miliseconds in hours then in days                        
      businessDaysDiff = (businessMillisecondsDiff / (1000.0*60.0*60.0)) / 24.0; 
      return businessDaysDiff;
    }           
    else {
      return null;
    }
  } 
  
  
  //=========================================================================
  // Case 01661968 - Adding method to get number of working hours per day
  //=========================================================================
  public static Map<Id,Decimal> businessHourIdToHoursPerDayMap {get{
    if (businessHourIdToHoursPerDayMap == null) {
      businessHourIdToHoursPerDayMap = new Map<Id,Decimal>();
      for (BusinessHours bh : [SELECT Id, MondayStartTime, MondayEndTime, Name
                              FROM BusinessHours
                              WHERE IsActive = true]) {
        // Now calculcate a time diff based on the monday start and end time.
        Datetime sDT = Datetime.newInstanceGMT(Date.newInstance(2001, 1, 1), bh.MondayStartTime);
        Datetime eDT = Datetime.newInstanceGMT(Date.newInstance(2001, 1, 1), bh.MondayEndTime);
        Long milisecsDiff = eDT.getTime() - sDT.getTime();
        // system.debug(bh.MondayStartTime + ' - ' +bh.MondayEndTime + ' is '+milisecsDiff+' for '+bh.Name);
        Decimal hoursDiff = milisecsDiff / (1000.0 * 60.0 * 60.0);
        if (bh.MondayStartTime == bh.MondayEndTime) {
          hoursDiff = 24.0; // Lock to 24 hour days if currently full time
        }
        businessHourIdToHoursPerDayMap.put(bh.Id, hoursDiff);
      }
    }
    return businessHourIdToHoursPerDayMap;
  }set;}
  
  
}