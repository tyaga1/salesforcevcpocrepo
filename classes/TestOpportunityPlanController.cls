/**=====================================================================
 * Experian
 * Name: TestOpportunityPlanController
 * Description: Test Class for OpportunityPlanController
 * Created Date: 23rd July 2015
 * Created By: UC Innovation
 *
 * Date Modified      Modified By        Description of the update
 * Oct 26th, 2015     Paul Kissick       Case 01139502: Fixing ownership check
 =====================================================================*/
@isTest
private class TestOpportunityPlanController {
    
  /*
    need to test 4 functions, Save(), Cancel(), addClientGoal(), addSalesObjective()
  */
  
  private static Opportunity objOpportunity; // NLG - June 25, 2014
  private static Date tod = Date.today();

  public static testMethod void test_CREATE_OpportunityPlan() {
    String message;
    Boolean contains;
    Integer quantity;
    // set up data here
    
    createOpportunity();
    
    Opportunity_Plan__c oplan = new Opportunity_Plan__c(); // createOpportunityPlan();
    
    objOpportunity = [SELECT Id, Name, AccountId, OwnerId, Budget__c, CloseDate FROM Opportunity WHERE Name = 'objOpportunity' LIMIT 1];

    // set up opportunity & opportunity plan
    PageReference pageRef = Page.OpportunityPlan;
    // put the id of the test data here
    pageRef.getParameters().put('oppId', objOpportunity.Id);
    Test.setCurrentPage(pageRef);
          
    // standard takes a reference to Opportunity_Plan__c, so set up data model first
    ApexPages.StandardController sd = new ApexPages.StandardController(oplan);
    OpportunityPlanController controller = new OpportunityPlanController(sd);

    // verift constructor set up values
    //system.assert(oplan.OwnerId == objOpportunity.OwnerId); // got this one // PK: Case 01139502
    system.assertEquals(oplan.Name, 'OP-' + objOpportunity.Name); // got this one
    system.assertEquals(oplan.Opportunity_Name__c, objOpportunity.Id); // got this one
    system.assertEquals(oplan.Account_Name__c, objOpportunity.AccountId);  // got this one
    system.assertEquals(oplan.Opportunity_Owner__c, objOpportunity.OwnerId);    // got this one
    system.assertEquals(oplan.Opportunity_Client_Budget__c, objOpportunity.Budget__c);  // got this one
    system.assertEquals(oplan.Opportunity_Expected_Close_Date__c, objOpportunity.CloseDate ); // got this one

    // we can add 4 more boxes for client goal without error messages
    controller.addClientGoal();
    controller.addClientGoal();
    controller.addClientGoal();
    controller.addClientGoal();
    message = Label.Opp_Plan_Five_Per_Section; //'You have reached the maximum of 5 entries for this section, no more can be added.';
    contains = containMessage(ApexPages.getMessages(), message);
    quantity = numberOfMessage(ApexPages.getMessages(), message);
    system.assertEquals(false, contains);
    system.assertEquals(0, quantity);

    // we can add 4 more boxes for sales objective without error messages
    controller.addSalesObjective();
    controller.addSalesObjective();
    controller.addSalesObjective();
    controller.addSalesObjective();
    contains = containMessage(ApexPages.getMessages(), message);
    quantity = numberOfMessage(ApexPages.getMessages(), message);
    system.assertEquals(false, contains);
    system.assertEquals(0, quantity);

    // we can't add the 6th boxes for sales objective, we will see the error message
    controller.addSalesObjective();
    contains = containMessage(ApexPages.getMessages(), message);
    quantity = numberOfMessage(ApexPages.getMessages(), message);
    system.assertEquals(true, contains);
    system.assertEquals(1, quantity);


    // test SAVE()
    message = Label.Opp_Plan_Sales_Objective_Missing; //'Please fill in all Sales Objective before saving.';
    oplan.Sales_Objective_1__c = 'Objective 1';
    oplan.Sales_Objective_2__c = 'Objective 2';
    oplan.Sales_Objective_3__c = null;
    oplan.Sales_Objective_4__c = 'Objective 4';
    oplan.Sales_Objective_5__c = 'Objective 5';

    contains = containMessage(ApexPages.getMessages(), message);
    quantity = numberOfMessage(ApexPages.getMessages(), message);
    system.assertEquals(false, contains);
    system.assertEquals(0, quantity);
    // before the call, doesn't contain the message
    controller.save();
    contains = containMessage(ApexPages.getMessages(), message);
    quantity = numberOfMessage(ApexPages.getMessages(), message);
    system.assertEquals(true, contains);
    system.assertEquals(1, quantity);


    // test successfully SAVE()
    oplan.Sales_Objective_3__c = 'Objective 3';
    
    oplan.Client_Goal_1__c = 'client goal 1';
    oplan.Client_Goal_2__c = 'client goal 2';
    oplan.Client_Goal_3__c = 'client goal 3';
    oplan.Client_Goal_4__c = 'client goal 4';
    oplan.Client_Goal_5__c = 'client goal 5';
    
    system.assertNotEquals(null, controller.save());  // Should save fine and create the opp plan record
    
    system.assertEquals(1,[SELECT COUNT() FROM Opportunity_Plan__c WHERE Opportunity_Name__c = :objOpportunity.Id]);
    
  }
    
  private static Boolean containMessage(ApexPages.Message[] pageMessages, String message_input){
    Boolean messageFound = false;
    for(ApexPages.Message message : pageMessages) {
      if(message.getSummary().contains(message_input) == true) {
        messageFound = true;        
      }
    }
    return messageFound;
  }

  private static Integer numberOfMessage(ApexPages.Message[] pageMessages, String message_input){
    Integer messageFound = 0;
    for (ApexPages.Message message : pageMessages) {
      if (message.getSummary().contains(message_input) == true) {
        messageFound++;
      }
    }
    return messageFound;
  }

  @isTest 
  static void test_EDIT_OpportunityPlan() {
    // set up data here
    Opportunity_Plan__c oplan = createOpportunityPlan();

    PageReference pageRef = Page.OpportunityPlan;
    // it is in edit mode if it comes with the id=opportunity.id
    pageRef.getParameters().put('id', oplan.Id);
    Test.setCurrentPage(pageRef);
      
    // standard takes a reference to Opportunity_Plan__c, so set up data model first
    ApexPages.StandardController sd = new ApexPages.StandardController(oplan);
    OpportunityPlanController controller = new OpportunityPlanController(sd);
    //String nextPage = controller.save().getUrl();

    // after executing this line of code, it will exceed 5 client goals, and thus yields the following error message
    Boolean contains;
    String message;
    Integer quantity;

    message = Label.Opp_Plan_Five_Per_Section; // 'You have reached the maximum of 5 entries for this section, no more can be added.';
    contains = containMessage(ApexPages.getMessages(), message);
    quantity = numberOfMessage(ApexPages.getMessages(), message);
    system.assertEquals(false, contains);
    system.assertEquals(0, quantity);
    // before the call, doesn't contain the message
    controller.addClientGoal(); 
    contains = containMessage(ApexPages.getMessages(), message);
    quantity = numberOfMessage(ApexPages.getMessages(), message);
    system.assertEquals(true, contains);
    system.assertEquals(1, quantity);
    //// after the call, contain the message
    //// now do the salesobjective
    //quantity = numberOfMessage(ApexPages.getMessages(), message);
    //system.assert( quantity == 1 );
    //controller.addSalesObjective();
    //quantity = numberOfMessage(ApexPages.getMessages(), message);

    
    // test SAVE()
    message = Label.Opp_Plan_Client_Goals_Missing; //'Please fill in all Client Goals before saving.';
    oplan.Client_Goal_3__c = null;
    oplan.Sales_Objective_3__c = null;

    contains = containMessage(ApexPages.getMessages(), message);
    quantity = numberOfMessage(ApexPages.getMessages(), message);
    system.assertEquals(false, contains);
    system.assertEquals(0, quantity);
    // before the call, doesn't contain the message
    controller.save();
    contains = containMessage(ApexPages.getMessages(), message);
    quantity = numberOfMessage(ApexPages.getMessages(), message);
    system.assertEquals(true, contains);
    system.assertEquals(1, quantity);

    // test CANCEL()
    PageReference cancel = controller.cancel();
    system.assertEquals(('/' + objOpportunity.Id), cancel.getUrl());
  }
  
  static void createOpportunity() {
    User sysAdmin = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    insert sysAdmin;
    //Account objAccount = createAccount();
    Account objAccount = Test_Utils.insertAccount();
    //system.assertNotEquals(null, objAccount.Id, 'Failed to insert Account record');
    system.debug('acc>>' +objAccount);
    // Name=Test Account0.6446376540173728, Id=001e000000en0bDAAQ

    objOpportunity = Test_Utils.insertOpportunity(objAccount.Id);
    system.debug('objOpportunity>>' +objOpportunity.Id);

    objOpportunity.OwnerId = sysAdmin.Id;
    objOpportunity.Name = 'objOpportunity';    
    objOpportunity.Budget__c = '1,001 - 10,000';
    objOpportunity.CloseDate = tod;
    update objOpportunity;
    
  }

  static Opportunity_Plan__c createOpportunityPlan() {
    
    createOpportunity();
    
    //system.assertEquals(null, objOpportunity.Id, 'Failed to insert Opportunity record');
    //objOpportunity = createOpportunity(objAccount.Id);
    system.debug('opp>>' +objOpportunity.Id);

    List<Opp_Plan_Score_Calc__c> listOppPlanCalc = new List<Opp_Plan_Score_Calc__c>();

    Opp_Plan_Score_Calc__c oppPlanCalc_InformationScoring = Test_Utils.insertOppPlanScoreCalc('Information Scoring', false);
    listOppPlanCalc.add(oppPlanCalc_InformationScoring);
    Opp_Plan_Score_Calc__c oppPlanCalc_QualificationScoring = Test_Utils.insertOppPlanScoreCalc('Qualification Scoring', false);
    listOppPlanCalc.add(oppPlanCalc_QualificationScoring);
    Opp_Plan_Score_Calc__c oppPlanCalc_BuyingCentre = Test_Utils.insertOppPlanScoreCalc('Buying Centre', false);
    listOppPlanCalc.add(oppPlanCalc_BuyingCentre);
    Opp_Plan_Score_Calc__c oppPlanCalc_CompetitionScoring = Test_Utils.insertOppPlanScoreCalc('Competition Scoring', false);
    listOppPlanCalc.add(oppPlanCalc_CompetitionScoring);
    Opp_Plan_Score_Calc__c oppPlanCalc_SummaryPosition = Test_Utils.insertOppPlanScoreCalc('Summary Position', false);
    listOppPlanCalc.add(oppPlanCalc_SummaryPosition);
    Opp_Plan_Score_Calc__c oppPlanCalc_SolutionAtGlance = Test_Utils.insertOppPlanScoreCalc('Solution at a Glance', false);
    listOppPlanCalc.add(oppPlanCalc_SolutionAtGlance);
    Opp_Plan_Score_Calc__c oppPlanCalc_JointActionPlan = Test_Utils.insertOppPlanScoreCalc('Joint Action Plan', false);
    listOppPlanCalc.add(oppPlanCalc_JointActionPlan);
    Opp_Plan_Score_Calc__c oppPlanCalc_ValueProposition = Test_Utils.insertOppPlanScoreCalc('Value Proposition', false);
    listOppPlanCalc.add(oppPlanCalc_ValueProposition);
    Opp_Plan_Score_Calc__c oppPlanCalc_ActionPlan = Test_Utils.insertOppPlanScoreCalc('Action Plan', false);
    listOppPlanCalc.add(oppPlanCalc_ActionPlan);
    insert listOppPlanCalc;

    List<Opp_Plan_Score_Sub_Calc__c> listSubOppPlanScoreSubCalc = new List<Opp_Plan_Score_Sub_Calc__c>();
    Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc1 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_InformationScoring.Id, false);
    Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc2 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_QualificationScoring.Id, false);
    Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc3 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_BuyingCentre.Id, false);
    Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc4 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_SummaryPosition.Id, false);
    Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc5 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_SolutionAtGlance.Id, false);
    Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc6 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_JointActionPlan.Id, false);
    Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc7 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_ValueProposition.Id, false);
    Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc8 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_ActionPlan.Id, false);
    Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc9 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_CompetitionScoring.Id, false);

    listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc1);
    listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc2);
    listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc3);
    listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc4);
    listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc5);
    listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc6);
    listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc7);
    listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc8);
    listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc9);
    insert listSubOppPlanScoreSubCalc;

    /*Opp_Plan_Score_Calc__c oppPlnScore = new Opp_Plan_Score_Calc__c ();
    oppPlnScore.Name = 'Information Scoring';
    oppPlnScore.Expected_Score__c = 2;
    insert oppPlnScore;
    system.debug('oppPlnScore>>' +oppPlnScore);

    Opp_Plan_Score_Sub_Calc__c oppPlnScoreSub = new Opp_Plan_Score_Sub_Calc__c();
    oppPlnScoreSub.Name = 'Benefits';
    oppPlnScoreSub.Calculation_Field__c = 'Benefits__c';
    oppPlnScoreSub.Object_API_Name__c = 'Opportunity_Plan__c';
    insert oppPlnScoreSub;*/

    Opportunity_Plan__c objOpportunityPlan = new Opportunity_Plan__c(
      Name = 'Test Plan - 001',
      Account_Name__c = objOpportunity.AccountId,
      Opportunity_Name__c = objOpportunity.Id,
      Client_Goal_1__c = 'client goal 1',
      Client_Goal_2__c = 'client goal 2',
      Client_Goal_3__c = 'client goal 3',
      Client_Goal_4__c = 'client goal 4',
      Client_Goal_5__c = 'client goal 5',
      Exp_Risk_1__c = 'Exp risk 1',
      Exp_Risk_2__c = 'Exp risk 2',
      Exp_Risk_3__c = 'Exp risk 3',
      Exp_Risk_4__c = 'Exp risk 4',
      Exp_Risk_5__c = 'Exp risk 5',
      Exp_Strength_1__c = '',
      Exp_Strength_2__c = '',
      Exp_Strength_3__c = '',
      Exp_Strength_4__c = '',
      Exp_Strength_5__c = '',
      Sales_Objective_1__c = 'objective 1',
      Sales_Objective_2__c = 'objective 2',
      Sales_Objective_3__c = 'objective 3',
      Sales_Objective_4__c = 'objective 4',
      Sales_Objective_5__c = 'objective 5',
      CG_1_Importance__c = '1',
      CG_2_Importance__c = '2',
      CG_3_Importance__c = '3',
      CG_4_Importance__c = '4',
      CG_5_Importance__c = '5',
      Opportunity_Expected_Close_Date__c = tod, // NLG June 25, 2014
      Risk_1_Rating__c = '1',
      Risk_2_Rating__c = '2',
      Risk_3_Rating__c = '3',
      Risk_4_Rating__c = '4',
      Risk_5_Rating__c = '5',
      SO_1_Importance__c = '1',
      SO_2_Importance__c = '2',
      SO_3_Importance__c = '3',
      SO_4_Importance__c = '4',
      SO_5_Importance__c = '5',
      Solution_Fulfils_Requirements__c = '',
      Opportunity_Client_Budget__c = '1,001 - 10,000'
    );
    insert objOpportunityPlan;
    system.assertNotEquals(null, objOpportunityPlan.Id, 'Failed to insert Opportunity Plan record');
    return objOpportunityPlan;
  }
}