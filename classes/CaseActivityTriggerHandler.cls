/**=====================================================================
 * Experian, Inc
 * Name: CaseActivityTriggerHandler
 * Description: W-007318
 * Created Date: Apr 26th, 2017
 * Created By: Manoj Gopu
 * 
 * Date Modified                Modified By                  Description of the update
 * 20 July 2017                 Manoj Gopu                   Updated expected date calculation based on logged in User Timezone
   =====================================================================*/

public class CaseActivityTriggerHandler{

  //=========================================================================
  // Before Insert Call
  //=========================================================================
    public static void beforeInsert(list<Case_Activity__c> newActivities,boolean isDataAdmin){
        if(isDataAdmin == false){
            if(CaseTriggerHandler.isCaseCustomActivity == false){
                populateSerasaAssignmentOwner(newActivities,true);
            }            
        }       
    }
  //=========================================================================
  // Before Update Call
  //=========================================================================
    public static void beforeUpdate(list<Case_Activity__c> newActivities,map<Id,Case_Activity__c> oldMap,boolean isDataAdmin){
        
    }
    
    //Populate the Activity Owner 
    public static void populateSerasaAssignmentOwner(list<Case_Activity__c> newActivities,boolean isInsert){
        set<string> setIsland = new set<string>();
        set<string> setType = new set<string>();
        set<string> setSubType = new set<string>();
        set<string> setCaseIds = new set<string>();
        for(Case_Activity__c ca:newActivities){        
            setIsland.add(ca.Island__c);
            setType.add(ca.Type__c);
            setSubType.add(ca.Sub_Type__c);  
            setCaseIds.add(ca.Case__c);
        }
        if(setIsland.isEmpty())
            return ;
        
        map<string,string> mapCaseCombination = new map<string,string>();
        for(Case cs:[select id,Island__c,Serasa_Case_Reason__c,Serasa_Secondary_Case_Reason__c,Serasa_Case_Third_Reason__c from Case where Id IN:setCaseIds]){
            if(cs.Serasa_Case_Third_Reason__c!=null && cs.Serasa_Case_Third_Reason__c!='')
                mapCaseCombination.put(cs.Id,cs.Island__c+'::'+cs.Serasa_Case_Reason__c+'::'+cs.Serasa_Secondary_Case_Reason__c+'::'+cs.Serasa_Case_Third_Reason__c);
            else
                mapCaseCombination.put(cs.Id,cs.Island__c+'::'+cs.Serasa_Case_Reason__c+'::'+cs.Serasa_Secondary_Case_Reason__c);
        }
        // Used to store the Combination of Island Type and Subtype values  
        map<string,Serasa_Assignment__c> mapSerasa = new map<string,Serasa_Assignment__c>();
        map<string,Serasa_Assignment__c> mapSerasaManualActivity = new map<string,Serasa_Assignment__c>();
        for(Serasa_Assignment__c objAss:[select id,Island__c,Case_Reason_Serasa__c,Secondary_Case_Reason_Serasa__c,Tertiary_Case_Reason_Serasa__c,Assign_To__c,Activity_Owner__c,
                    SLA__c,Activity_Island__c, Type__c,Sub_Type__c, Activity_SLA__c from Serasa_Assignment__c where Activity_Island__c IN:setIsland AND Sub_Type__c IN:setSubType]){ 
            mapSerasaManualActivity.put(objAss.Activity_Island__c+'::'+objAss.Sub_Type__c,objAss);
            if(objAss.Tertiary_Case_Reason_Serasa__c!=null && objAss.Tertiary_Case_Reason_Serasa__c!='')
                mapSerasa.put(objAss.Island__c+'::'+objAss.Case_Reason_Serasa__c+'::'+objAss.Secondary_Case_Reason_Serasa__c+'::'+objAss.Tertiary_Case_Reason_Serasa__c+'::'+objAss.Activity_Island__c+'::'+objAss.Type__c+'::'+objAss.Sub_Type__c,objAss);
            else
                mapSerasa.put(objAss.Island__c+'::'+objAss.Case_Reason_Serasa__c+'::'+objAss.Secondary_Case_Reason_Serasa__c+'::'+objAss.Activity_Island__c+'::'+objAss.Type__c+'::'+objAss.Sub_Type__c,objAss);
        }
        // store the Queue names and ids in map
        map<string,string> mapQueues = new map<string,string>();
        for(Group gr:[select Id,Name from Group where Type = 'Queue']){
            mapQueues.put(gr.Name,gr.Id);
        } 
        List<Holiday> holidaysLst = [SELECT ActivityDate FROM Holiday];      
        for(Case_Activity__c ca:newActivities){ 
            //checking for the combination in map,if yes owner and SLA will be populated. 
            if(mapCaseCombination.containsKey(ca.Case__c)){
                string strComb = mapCaseCombination.get(ca.Case__c);
                Serasa_Assignment__c serasaAss = new Serasa_Assignment__c();
                boolean isFound = false;
                if(mapSerasa.containsKey(strComb+'::'+ca.Island__c+'::'+ca.Type__c+'::'+ca.Sub_Type__c)){
                    isFound = true;
                    serasaAss = mapSerasa.get(strComb+'::'+ca.Island__c+'::'+ca.Type__c+'::'+ca.Sub_Type__c);                                     
                }
                else if(mapSerasaManualActivity.containsKey(ca.Island__c+'::'+ca.Sub_Type__c)){
                    serasaAss = mapSerasaManualActivity.get(ca.Island__c+'::'+ca.Sub_Type__c);
                    isFound = true;
                }
                if(isFound == true){
                    ca.SLA__c = serasaAss.Activity_SLA__c;
                
                    if(mapQueues.containsKey(serasaAss.Activity_Owner__c))
                        ca.OwnerId = mapQueues.get(serasaAss.Activity_Owner__c);  
                }
            }
            if(ca.SLA__c==null)
                return ;
                
            Integer hlCount=0;
            DateTime dTime= system.now();
            Date startdate = date.newinstance(dTime.year(), dTime.month(), dTime.day());
            Date enddate = startdate + integer.valueof(ca.SLA__c/24);
            for(Holiday hl:holidaysLst){
                if(hl.activityDate >= startdate && hl.activityDate <= enddate){
                    hlCount++;      
                }
            }
            Integer differenceBetTwoDate = startdate.daysBetween(enddate);                                 
            for(Integer i = 0; i <= differenceBetTwoDate; i++) {
            
                date nameOfTheDay = StartDate + i;   
                datetime dt=DateTime.newInstance(nameOfTheDay, Time.newInstance(0, 0, 0, 0));         
                if(dt.format('EEEE') == 'Sunday' || dt.format('EEEE') == 'Saturday') {                        
                    hlCount++;
                    differenceBetTwoDate++;
                }
            }
            enddate = enddate + hlCount;
            ca.Expected_Date__c = enddate; 
           /* DateTime dTime= system.now();
            Long msec = (Long)ca.SLA__c*60*60*1000;
            DateTime dt = BusinessHours.add(Custom_Fields_Ids__c.getOrgDefaults().Serasa_business_hours_id__c,dTime,msec);          
            ca.Expected_Date__c = date.newinstance(dt.year(),dt.month(),dt.day()); */         
        }
    }    
}