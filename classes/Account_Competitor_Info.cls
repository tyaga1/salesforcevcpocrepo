public class Account_Competitor_Info
{
    public ID CompetitorID {get;set;}
    public string CompetitorName {get;set;}
    public string CompetingFor {get;set;}
    public string CompetingForID {get;set;}
    public string GBL {get;set;}
    public string BU {get;set;}
    public Boolean LostTo {get;set;}
	public Boolean IsAttachedToOppBelongingToAccount {get;set;}
}