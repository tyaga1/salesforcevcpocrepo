/**=====================================================================
 * Appirio, Inc
 * Test Class Name: OpportunityPlanContactTrigger_Test
 * Class Name: OpportunityPlanContactTrigger & OpportunityPlanContactTriggerHandler
 * Created Date: May 23rd, 2014
 * Created By: Naresh kr Ojha (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * Aug 25th, 2015               Paul Kissick                 Adding additional contacts to support new required field.
 * Sep 08th, 2015               Naresh Kr Ojha               Added test_linkManagerContact & test_linkManagerContact2 methods
 * Sep 21st, 2015               Paul Kissick                 I-179463: Duplicate Management Failures
 =====================================================================*/
@isTest
private class OpportunityPlanContactTrigger_Test {

  static testMethod void testMethod1() {
  	Account account = Test_Utils.insertAccount();
  	Contact contact = Test_Utils.insertContact(account.ID);
    Opportunity opportunity = Test_Utils.insertOpportunity(account.id);
    Opportunity opportunity2 = Test_Utils.insertOpportunity(account.id);        

    Opportunity_Plan__c oppPlan = Test_Utils.insertOpportunityPlan(true, opportunity.id);
    Opportunity_Plan__c oppPlan2 = Test_Utils.insertOpportunityPlan(true, opportunity2.id);
    
    Opportunity_Plan_Contact__c oppPlanCont = new Opportunity_Plan_Contact__c();
    oppPlanCont.Opportunity_Plan__c = oppPlan.ID;
    oppPlanCont.Contact__c = contact.ID;
    insert oppPlanCont;

    Opportunity_Plan_Contact__c oppPlanCont2 = new Opportunity_Plan_Contact__c();
    oppPlanCont2.Opportunity_Plan__c = oppPlan2.ID;
    oppPlanCont2.Contact__c = contact.ID;
    insert oppPlanCont2;
      
          
    //  Plan_Contact_Relationship__c planContRel = new Plan_Contact_Relationship__c();
    //    planContRel.Relationship__c = 'Positive';
    //    planContRel.Contact_1__c = oppPlanCont.ID; 
    //    planContRel.Contact_2__c = oppPlanCont2.ID;
    //    insert planContRel;
      
    Plan_Contact_Relationship__c planContRel = Test_Utils.createPlanContactRelationship(false,oppPlanCont ,oppPlanCont2,'Positive');
      
        
      //To query deleted plan contact relationships.
      ID oldOppPlanID = oppPlanCont2.ID;
      
      Test.startTest();
        delete oppPlanCont2;
      Test.stopTest();
      //Assert to check deleted associated records. 
      System.assertEquals([SELECT count() FROM Plan_Contact_Relationship__c WHERE Contact_2__c =: oldOppPlanID], 0);
  }
  
  static testMethod void testMethod2() {
  	Account account = Test_Utils.insertAccount();
    Contact contact = Test_Utils.insertContact(account.ID);
    Opportunity opportunity = Test_Utils.insertOpportunity(account.id);
    Opportunity opportunity2 = Test_Utils.insertOpportunity(account.id);        

    Opportunity_Plan__c oppPlan = Test_Utils.insertOpportunityPlan(true, opportunity.id);
    Opportunity_Plan__c oppPlan2 = Test_Utils.insertOpportunityPlan(true, opportunity2.id);
    
    Opportunity_Plan_Contact__c oppPlanCont = new Opportunity_Plan_Contact__c();
      oppPlanCont.Opportunity_Plan__c = oppPlan.ID;
      oppPlanCont.Contact__c = contact.ID;
      insert oppPlanCont;

      Opportunity_Plan_Contact__c oppPlanCont2 = new Opportunity_Plan_Contact__c();
      oppPlanCont2.Opportunity_Plan__c = oppPlan2.ID;
      oppPlanCont2.Contact__c = contact.ID;
      insert oppPlanCont2;
            
      
      //Plan_Contact_Relationship__c planContRel = new Plan_Contact_Relationship__c();
      //planContRel.Relationship__c = 'Positive';
      //planContRel.Contact_1__c = oppPlanCont.ID; 
      //planContRel.Contact_2__c = oppPlanCont2.ID;
      //insert planContRel;
      
      
      
     Plan_Contact_Relationship__c planContRel = Test_Utils.createPlanContactRelationship(false,oppPlanCont ,oppPlanCont2,'Positive');
             
     
      
        
      //To query deleted plan contact relationships.
      ID oldOppPlanID = oppPlanCont2.ID;
      
      Test.startTest();
        delete oppPlanCont2;
          //Assert to check deleted associated records. 
          System.assertEquals([SELECT count() FROM Plan_Contact_Relationship__c WHERE Contact_2__c =: oldOppPlanID], 0);
        //undelete Test
        undelete oppPlanCont2;
      Test.stopTest();
      
      //Assert to check deleted associated records. 
      //System.assertNotEquals([SELECT count() FROM Plan_Contact_Relationship__c WHERE Contact_2__c =: oldOppPlanID], 0);
  }

  //===========================================================================
  // Test linkManager functionality
  //===========================================================================
  static testMethod void test_linkManagerContact() {
    Account account = Test_Utils.insertAccount();
    Contact contact = Test_Utils.insertContact(account.ID);

    Contact reportsToContact = Test_Utils.insertContact(account.ID);
    contact.ReportsToId = reportsToContact.ID;
    update contact;

    Opportunity opportunity = Test_Utils.insertOpportunity(account.id);
    Opportunity opportunity2 = Test_Utils.insertOpportunity(account.id);        
    
    //Create OpportunityContactRole
    List<OpportunityContactRole> opptyContactRoles = new List<OpportunityContactRole>();
    OpportunityContactRole oppConRole = Test_Utils.insertOpportunityCR(false, contact.ID, opportunity.ID);
    opptyContactRoles.add(oppConRole);
    oppConRole = Test_Utils.insertOpportunityCR(false, reportsToContact.ID, opportunity.ID);
    opptyContactRoles.add(oppConRole);
    oppConRole = Test_Utils.insertOpportunityCR(false, contact.ID, opportunity2.ID);
    opptyContactRoles.add(oppConRole);
    oppConRole = Test_Utils.insertOpportunityCR(false, reportsToContact.ID, opportunity2.ID);
    opptyContactRoles.add(oppConRole);
    insert opptyContactRoles;

    Opportunity_Plan__c oppPlan = Test_Utils.insertOpportunityPlan(true, opportunity.id);
    Opportunity_Plan__c oppPlan2 = Test_Utils.insertOpportunityPlan(true, opportunity2.id);
    List<Opportunity_Plan_Contact__c> opptyPlanContactList = new List<Opportunity_Plan_Contact__c>();

    Opportunity_Plan_Contact__c oppPlanCont = new Opportunity_Plan_Contact__c();
    oppPlanCont.Opportunity_Plan__c = oppPlan.ID;
    oppPlanCont.Contact__c = contact.ID;
    opptyPlanContactList.add(oppPlanCont);

    Opportunity_Plan_Contact__c oppPlanCont2 = new Opportunity_Plan_Contact__c();
    oppPlanCont2.Opportunity_Plan__c = oppPlan2.ID;
    oppPlanCont2.Contact__c = contact.ID;
    opptyPlanContactList.add(oppPlanCont2);
    insert opptyPlanContactList;

    Plan_Contact_Relationship__c planContRel = Test_Utils.createPlanContactRelationship(false, oppPlanCont, oppPlanCont2, 'Positive');
    opptyPlanContactList.get(0).Link_Manager__c = true;
    update opptyPlanContactList;
    
    opptyPlanContactList = [SELECT ID, Reports_To__c FROM Opportunity_Plan_Contact__c WHERE ID IN: opptyPlanContactList];
    //Link changer updates first Oppty Plan Contact's reports to field where another one is remaining null
    System.assertNotEquals(opptyPlanContactList.get(0).Reports_To__c, null);
    System.assertEquals(opptyPlanContactList.get(1).Reports_To__c, null);
  }  

  //===========================================================================
  // Test linkManager functionality
  //===========================================================================
  static testMethod void test_linkManagerContact2() {
    Account account = Test_Utils.insertAccount();
    Contact reportsToContact = Test_Utils.insertContact(account.ID);
    
    Database.DMLOptions dml = new Database.DMLOptions();
    dml.DuplicateRuleHeader.allowSave = true;

    List<Contact> contactList = new List<Contact>();
    Contact contact = Test_Utils.createContact(account.ID);
    contact.ReportsToId = reportsToContact.ID;
    contactList.add(contact);
    Contact contact1 = Test_Utils.createContact(account.ID);
    contact1.ReportsToId = reportsToContact.ID;
    contactList.add(contact1);
    Database.insert(contactList,dml);

    Opportunity opportunity = Test_Utils.insertOpportunity(account.id);
    Opportunity opportunity2 = Test_Utils.insertOpportunity(account.id);        
    
    //Create OpportunityContactRole
    List<OpportunityContactRole> opptyContactRoles = new List<OpportunityContactRole>();
    OpportunityContactRole oppConRole = Test_Utils.insertOpportunityCR(false, contact.ID, opportunity.ID);
    opptyContactRoles.add(oppConRole);
    oppConRole = Test_Utils.insertOpportunityCR(false, contact1.ID, opportunity.ID);
    opptyContactRoles.add(oppConRole);
    oppConRole = Test_Utils.insertOpportunityCR(false, contact.ID, opportunity2.ID);
    opptyContactRoles.add(oppConRole);
    oppConRole = Test_Utils.insertOpportunityCR(false, contact1.ID, opportunity2.ID);
    opptyContactRoles.add(oppConRole);
    insert opptyContactRoles;
    
    List<Opportunity_Plan__c> opptyPlanList = new List<Opportunity_Plan__c>();
    Opportunity_Plan__c oppPlan = Test_Utils.insertOpportunityPlan(false, opportunity.id);
    opptyPlanList.add(oppPlan);
    Opportunity_Plan__c oppPlan2 = Test_Utils.insertOpportunityPlan(false, opportunity2.id);
    opptyPlanList.add(oppPlan2);
    insert opptyPlanList;

    List<Opportunity_Plan_Contact__c> opptyPlanContactList = new List<Opportunity_Plan_Contact__c>();

    Opportunity_Plan_Contact__c oppPlanCont = new Opportunity_Plan_Contact__c();
    oppPlanCont.Opportunity_Plan__c = oppPlan.ID;
    oppPlanCont.Contact__c = contact.ID;
    opptyPlanContactList.add(oppPlanCont);

    Opportunity_Plan_Contact__c oppPlanCont2 = new Opportunity_Plan_Contact__c();
    oppPlanCont2.Opportunity_Plan__c = oppPlan2.ID;
    oppPlanCont2.Contact__c = contact.ID;
    opptyPlanContactList.add(oppPlanCont2);

    oppPlanCont = new Opportunity_Plan_Contact__c();
    oppPlanCont.Opportunity_Plan__c = oppPlan.ID;
    oppPlanCont.Contact__c = contact1.ID;
    opptyPlanContactList.add(oppPlanCont);

    oppPlanCont2 = new Opportunity_Plan_Contact__c();
    oppPlanCont2.Opportunity_Plan__c = oppPlan2.ID;
    oppPlanCont2.Contact__c = contact1.ID;
    opptyPlanContactList.add(oppPlanCont2);
    insert opptyPlanContactList;

    Plan_Contact_Relationship__c planContRel = Test_Utils.createPlanContactRelationship(false, oppPlanCont, oppPlanCont2, 'Positive');
    opptyPlanContactList.get(0).Link_Manager__c = true;
    update opptyPlanContactList;
    
    opptyPlanContactList = [SELECT ID, Reports_To__c FROM Opportunity_Plan_Contact__c WHERE ID IN: opptyPlanContactList];
    //Link changer updates first Oppty Plan Contact's reports to field where another one is remaining null
    System.assertNotEquals(opptyPlanContactList.get(0).Reports_To__c, null);
    System.assertEquals(opptyPlanContactList.get(1).Reports_To__c, null);
  }
}