/**=====================================================================
 * Experian
 * Name: AccountEDQDependencyCleaner01
 * Description: The following batch class is designed to be scheduled to run every day.
                    This class will get all Accounts with an EDQ Dependency and clean them if no longer related to EDQ
 * Created Date: 6/17/2015
 * Created By: Diego Olarte (Experian)
 *
 * Date Modified                Modified By                  Description of the update
 * Nov 9th, 2015                Paul Kissick                 Case 01266075: Optimisations of queries
 * Jun 6th, 2016                Paul Kissick                 Case 02013456: Fixed too many subquery count
 =====================================================================*/
global class AccountEDQDependencyCleaner01 implements Database.Batchable<sObject> {

  global Database.Querylocator start ( Database.BatchableContext bc ) {

    return Database.getQueryLocator ([
      SELECT Id, SaaS__c, EDQ_Dependency_Exists__c,
       (
        SELECT Id, EDQ_On_Demand__c
        FROM Contacts
        WHERE EDQ_On_Demand__c = true
        LIMIT 1
       ),(
        SELECT Id, Owner_s_Business_Unit__c
        FROM Opportunities
        WHERE Owner_s_Business_Unit__c LIKE '%Data Quality%'
        LIMIT 1
       )
      FROM Account
      WHERE EDQ_Dependency_Exists__c = true
      AND SaaS__c = false
    ]);
  }

  global void execute (Database.BatchableContext bc, List<Account> scope) {

    List<Account> accountsList = new List<Account>();

    Integer contactCount;
    Integer opportunityCount;
    Boolean hasEDQrecords = false;
    
    for (Account acc : scope) {

      contactCount = 0;
      opportunityCount = 0;
      hasEDQrecords = false;

      contactCount = (acc.Contacts != null) ? acc.Contacts.size() : 0;
      opportunityCount = (acc.Opportunities != null) ? acc.Opportunities.size() : 0;

      hasEDQrecords = ((ContactCount+OpportunityCount) > 0);

      if (!hasEDQrecords) {
        accountsList.add(
          new Account(
            Id = acc.Id,
            EDQ_Dependency_Exists__c = false
          )
        );
      }
    }

    if (!accountsList.isEmpty()) {
      try {
        update accountsList;
      }
      catch (DMLException ex) {
        apexLogHandler.createLogAndSave('AccountEDQDependencyCleaner01','execute', ex.getStackTraceString(), ex);
      }
    }

  }

  //To process things after finishing batch
  global void finish (Database.BatchableContext bc) {

    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'AccountEDQDependencyCleaner01', true);

    if (!Test.isRunningTest()) {
      system.scheduleBatch(new AccountEDQDependencyCleaner02(), 'Account - EDQ Dependency Cleanup 2 '+String.valueOf(Datetime.now().getTime()), 1, ScopeSizeUtility.getScopeSizeForClass('AccountEDQDependencyCleaner02'));
    }

  }

}