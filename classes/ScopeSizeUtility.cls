/**=====================================================================
 * Experian
 * Name: ScopeSizeUtility
 * Description: Utility class to access Batch Class Scope Size custom object
 *   Just add records to Batch_Class_Scope_Size__c with the class name and size required.
 * Created Date: 11 Aug, 2015
 * Created By: Paul Kissick 
 * 
 * Date Modified      Modified By                Description of the update
 =====================================================================*/
public class ScopeSizeUtility {
  
  public static Integer defaultScopeSize = 200;

  public static Integer getScopeSizeForClass(String className) {
    if (String.isNotBlank(className)) {
      Map<String,Batch_Class_Scope_Size__c> classMap = Batch_Class_Scope_Size__c.getAll();
      if (classMap.containsKey(className)) {
        Batch_Class_Scope_Size__c bcss = classMap.get(className);
        return Integer.valueOf(bcss.Scope_Size__c);
      }
    }
    return defaultScopeSize;
  }

}