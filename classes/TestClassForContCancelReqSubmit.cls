/**=====================================================================
 * UC Innovation
 * Name: TestClassForSerasaMainframeContractWSReq
 * Description:  
 *
 * Created Date: 15 May, 2017
 * Created By: Charlie Park
 =====================================================================*/

@isTest
private class TestClassForContCancelReqSubmit {
	
	static testMethod void testCreateCancellationRecords() {
	    Account a1 = [SELECT Name,Id FROM Account LIMIT 1];

	    Case testCase = new Case();
	    testCase.AccountId = a1.Id;
	    testCase.Account_CNPJ__c = '000000166';
	    testCase.Subject = 'test';
	    testCase.Description = 'test';

	    insert testCase;

	    Test.setMock(HttpCalloutMock.class, new SerasaMainframeContractWSRequest.MockCalloutContract());

	    Test.startTest();
	    
	    ApexPages.currentPage().getParameters().put('AccountID',a1.Id);
	    ApexPages.currentPage().getParameters().put('CaseCNPJ',a1.Name);
	    ApexPages.currentPage().getParameters().put('id',testCase.id);

	    ContCancelReqSubmitController s = new ContCancelReqSubmitController(new ApexPages.StandardController(testCase));

	    s.listOfContracts[0].selected = true;
	    s.createCancellationRecords();

	    List<Contract_Cancellation_Request__c> listOfCancels = [SELECT id FROM Contract_Cancellation_Request__c];

	    system.assertNotEquals(0, listOfCancels.size());
	}

	@testSetup
	static void setupTestData() {
	    Webservice_Endpoint__c wsEnd = new Webservice_Endpoint__c(Name = 'SerasaMainframe-Contract',Username__c = 'ausername', URL__c = 'https://somewhere.com', Timeout__c = 10, Active__c = true);
	    insert wsEnd;
	    
	    Account a1 = Test_utils.insertAccount();

	    WebserviceEndpointUtil.createWebserviceKey(wsEnd);
	    WebserviceEndpointUtil.setWebservicePassword(wsEnd,'apassword');
	    
	}
	
}