/*======================================================================================
 * Experian Plc.
 * Name: BatchUserLastActivityUpdate
 * Description: Case #5237 - Batch job to update the Days since Activity field based on 
 *                           LastLoginDate
 * Created Date: July 30th, 2014
 * Created By: James Weatherall
 * 
 * Date Modified                Modified By                  Description of the update
 * Sep 11th, 2015               Paul Kissick                 Improvements for test coverage
 * Apr 4th, 2016                Paul Kissick                 Case 01927884: Added Improvements
 * Nov. 1st, 2016               James Wills                  Case 02108241: Include Users who have never logged in
 =======================================================================================*/

global class BatchUserLastActivityUpdate implements Database.Batchable<sObject>, Database.StateFul {
    
  global BatchUserLastActivityUpdate() {
  }
  
  @testVisible global List<User> testUsers;
  
  global List<String> updateErrors;
    
  //==============================================================
  // start method
  //==============================================================
  global Database.QueryLocator start(Database.BatchableContext BC) {
    updateErrors = new List<String>();
        
    String query = 'SELECT Id, Name, LastLoginDate, Days_Since_Activity__c, CreatedDate ' +
                   'FROM User ' +
                   'WHERE isActive = true';
                   
    if (Test.isRunningTest()) {
      query += ' AND Id IN :testUsers';
    }
    //else { //Case 02108241: Commented out section
    //  query += ' AND LastLoginDate != null';
    //}    
    return Database.getQueryLocator(query);    
  }
  
  //==============================================================
  // execute method
  //==============================================================
  global void execute(Database.BatchableContext BC, List<User> scope) {    
    List<User> usersToUpdate = new List<User>();
      
    for (User u : scope) {
      Date    lastLogin   = (u.LastLoginDate != null) ? u.LastLoginDate.date() : u.CreatedDate.date();
      Integer daysBetween = lastLogin.daysBetween(Date.today());
      
      //system.debug('User "'+u.Name+'", Last Login: "'+lastLogin+'", Days Between: '+daysBetween);
      
      usersToUpdate.add(new User(Id = u.Id, 
                                 Days_Since_Activity__c = daysBetween));
      
    }
    
    List<Database.SaveResult> updateSaveRes = Database.update(usersToUpdate,false);
    for (Database.SaveResult sr : updateSaveRes) {
      if (!sr.isSuccess()) {
        updateErrors.addAll(BatchHelper.parseErrors(sr.getErrors()));
      }
    }
  }

  //==============================================================
  // finish method
  //==============================================================
  global void finish(Database.BatchableContext BC) {
    
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchUserLastActivityUpdate', false);
    
    String emailBody = '';
    
    if (updateErrors.size() > 0 || Test.isRunningTest()) {      
      bh.batchHasErrors = true;
      
      if ( updateErrors.size() > 0) {
        emailBody += '\nThe following errors were observed when updating records:\n';
        emailBody += String.join(updateErrors,'\n');
      }      
      bh.emailBody += emailBody;
    }    
    
    bh.sendEmail();
  }  
}