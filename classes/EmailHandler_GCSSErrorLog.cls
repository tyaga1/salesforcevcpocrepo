/**=====================================================================
 * Experian
 * Name: EmailHandler_GCSSErrorLog
 * Description: Email handler to create GCSS specific cases for error logs
 * Created Date: 7 March 2016
 * Created By: Paul Kissick
 *
 * Date Modified       Modified By                    Description of the update
 * Mar 16th, 2016      Paul Kissick                   Case 01906209 : Adding support for type to change to GCSS
 =====================================================================*/
global class EmailHandler_GCSSErrorLog implements Messaging.InboundEmailHandler {

  global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
    Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
    try {    
      Case c = new Case();
    
      String gcssType = '';
      Pattern gcssTypePatt = Pattern.compile('(GCS|GCSS).*Support');
      
      List<Schema.PicklistEntry> caseTypePLE = Case.Type.getDescribe().getPicklistValues();
      for(Schema.PicklistEntry f : caseTypePLE) {
        String typeVal = f.getValue();
        if (gcssTypePatt.matcher(typeVal).matches()) {
          gcssType = typeVal;
        }
      }
    
      c.Origin = 'Email';
      c.OwnerId = Experian_Global__c.getInstance().GCS_Queue_ID__c;
      c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Salesforce.com Support').getRecordTypeId();
      c.Priority = 'Medium';
      c.Status = 'Open';
      c.Type = gcssType;
      c.Subject = (String.isNotBlank(email.subject)) ? email.subject.mid(0,Case.Subject.getDescribe().getLength()) : 'No Subject';
      c.Description = (String.isNotBlank(email.plainTextBody)) ? email.plainTextBody.mid(0,Case.Description.getDescribe().getLength()) : 'No Email Body';
      c.Technical_Impact_Salesforce__c = true;
      c.Reason = 'Other area';
      c.Secondary_Case_Reason__c = 'System / technical issues';
    
      insert c;
      
      List<Attachment> attsToInsert = new List<Attachment>();
      
      if (String.isNotBlank(email.htmlBody)) {
        attsToInsert.add(
          new Attachment(
            Name = 'EmailBody.html',
            ParentId = c.Id,
            Body = Blob.valueOf(email.htmlBody)
          )
        );
      }
      
      // Check for, and insert any attachments.
      if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
      
        List<Messaging.InboundEmail.BinaryAttachment> atts = email.binaryAttachments;
      
        for(Messaging.InboundEmail.BinaryAttachment att : atts) {
          Attachment a = new Attachment();
          a.ParentId = c.Id;
          a.Body = att.Body;
          a.Name = att.FileName;
          attsToInsert.add(a);
        }
      
      }
      insert attsToInsert;
    }
    catch (Exception e) {
      result.success = false;
      result.message = 'System Error Occurred: '+e.getMessage();
    }
    
    return result;
  }
}