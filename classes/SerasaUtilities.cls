/**=====================================================================
 * Appirio Inc
 * Name: SerasaUtilities.cls
 * Description: T-432280: Utility Class for CNPJ and CPF Validations 
 * Created Date: 03rd Sep 2015
 * Created By: Vikash Goyal(Appirio)
 =====================================================================*/
public class SerasaUtilities{
	
  // Method to validate CNPJ Number
  public static Boolean validateCNPJNumber(String cnpjNum){
    if(cnpjNum.length() == 14) {
	      
      // Convert string to character array 
      String[] chars = cnpjNum.split('');
      Integer[] cnpjNumArr = new Integer[]{};
      chars.remove(0);
      for(String char1 : chars){
        cnpjNumArr.add(Integer.valueOf(char1));
      }
	          
      // Get 1st Checksum digit
      Integer V1 = 5 * cnpjNumArr[0] + 4 * cnpjNumArr[1] + 3 * cnpjNumArr[2] 
	                      + 2 * cnpjNumArr[3] + 9 * cnpjNumArr[4] + 8 * cnpjNumArr[5] 
	                      + 7 * cnpjNumArr[6] + 6 * cnpjNumArr[7] + 5 * cnpjNumArr[8] 
	                      + 4 * cnpjNumArr[9]  + 3 * cnpjNumArr[10] + 2 * cnpjNumArr[11]  ;
      V1 = 11 - (Math.mod(V1, 11));
      if ( V1 >= 10 ){
          v1 = 0;
      }
                   
      // Get 2nd checksum digit
      Integer V2 = 6 * cnpjNumArr[0] + 5 * cnpjNumArr[1] + 4 * cnpjNumArr[2] 
                  + 3 * cnpjNumArr[3] + 2 * cnpjNumArr[4] 
                  + 9 * cnpjNumArr[5] + 8 * cnpjNumArr[6] + 7 * cnpjNumArr[7] 
                  + 6 * cnpjNumArr[8] + 5 * cnpjNumArr[9]  + 4 * cnpjNumArr[10] 
                  + 3 * cnpjNumArr[11]  + 2 * cnpjNumArr[12];
      V2 = 11- (Math.mod(V2, 11));
      if ( V2 >= 10 ){
          V2 = 0; 
      }
  
      if (V1 == cnpjNumArr[12] && V2 == cnpjNumArr[13]) { 
          return true;
      }
    }  
    return false;               
  }

  // Method to validate CPF Number
  public static Boolean validateCPFNumber(String cpfNum){
    if(cpfNum.isNumeric() && cpfNum.length() == 11) {
      
      // Convert string to character array 
      String[] chars = cpfNum.split('');
      Integer[] cpfNumArr = new Integer[]{};
      chars.remove(0);
      for(String char1 : chars){
        cpfNumArr.add(Integer.valueOf(char1));
      }
      
      //
      Integer V1 = 10 * cpfNumArr[0] + 9 * cpfNumArr[1] + 8 * cpfNumArr[2] + 7 * cpfNumArr[3] + 6 * cpfNumArr[4] +
                   5 * cpfNumArr[5] + 4 * cpfNumArr[6] + 3 * cpfNumArr[7] + 2 * cpfNumArr[8];
      V1 = 11 - Math.mod(V1,11) ;
      V1 = Math.mod(V1,11);
      V1 = Math.mod(V1,10);                
      
      //
      Integer V2 = 11 * cpfNumArr[0] + 10 * cpfNumArr[1] + 9 * cpfNumArr[2] + 8 * cpfNumArr[3] + 7 * cpfNumArr[4] +
                   6 * cpfNumArr[5] + 5 * cpfNumArr[6] + 4 * cpfNumArr[7] + 3 * cpfNumArr[8] + 2 * cpfNumArr[9];
      V2 = 11 - Math.mod(V2,11) ;
      V2 = Math.mod(V2,11);
      V2 = Math.mod(V2,10);
      
      if (V1 == cpfNumArr[9] && V2 == cpfNumArr[10]) {
        return true;
      }                
    }
    return false;
  }
}