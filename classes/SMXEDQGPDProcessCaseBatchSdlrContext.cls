/**=====================================================================
 * Experian
 * Name: SMXEDQGPDProcessCaseBatchSdlrContext 
 * Description: 
 * Created Date: 19/04/2017
 * Created By: Diego Olarte
 * 
 * Date Modified     Modified By        Description of the update
 *
 =====================================================================*/
 
global class SMXEDQGPDProcessCaseBatchSdlrContext implements Schedulable {

  global SMXEDQGPDProcessCaseBatchSdlrContext(){}
  
  global void execute(SchedulableContext ctx){
    SMXEDQGPDProcessCaseBatch b = new SMXEDQGPDProcessCaseBatch();
    if (!Test.isrunningTest()) {
      Database.executeBatch(b);
    }
  }

}