public with sharing class ResellerCloneCaseBtnController {
	@AuraEnabled
    public static String findRecordType(String caseID) {
        Case thisCase = [SELECT id, recordType.Name FROM Case WHERE id=:caseID];
        return thisCase.recordType.Name;
    }
}