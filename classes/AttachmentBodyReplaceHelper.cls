/*
* To replace the body of new attachments with the body of old attachments by referencing certain values introduced in the Description fields
* The new Attachments must be loaded first. After the first load, the newly inserted row Id's are going to be used.
* The old Attachments must be updated with a description with the batch identifier preceded by ABR_, for example: 'ABR_OPTY_01_OLD,' and 
*    the ID of the newly created related record. The comma is necessary to split the global batch identifier from the ID of the new attachment.
*
* 1: Get all possible data from Old attachments (except body)
* 2: Create New Attachments data, Make sure all reference columns are set: 
*    owner, created owner and dates, modified owner and dates, Description set, body set to a one char text file.
* 3: Insert New Attachments.
* 4: Update old attachments with new description which includes the ID of the new attachments, separated by comma.
* 5: Run this process.
* 6: Validate by querying for ABR_%.
* 7: Update new attachment description with a suitable one.
* 8: Delete Old Attachments.
*/ 
global class AttachmentBodyReplaceHelper implements Schedulable {
    
    global void execute(SchedulableContext sc){
        AttachmentBodyReplaceHelper.ABR();
    }
    
    @future
    public static void ABR(){
        String emailForResponse = Label.AttachmentBodyReplace_Email;
        String globalBatchIdentifier = Label.AttachmentBodyReplace_Batch;
        
        List<String> params = Label.AttachmentBodyReplace_Params.Split(',');
        Integer qLimit = 1000;
        Decimal hPercentLess = 15;
        if(params.size() > 1){
            try {
                qLimit = Integer.valueOf(params[0]);
                hPercentLess = Decimal.valueOf(params[1]);
                System.debug('Attachment Body Replace Params set. qlimit: ' + qLimit + '; hPercentLess: ' + hPercentLess);
            } catch(Exception e) {
                System.debug('Attachment Body Replace Params were not assigned. qLimit default: 1000; hPercentLess default: 1.15');
            }
        }
        
        String likeOldIdentifier = 'ABR_' + globalBatchIdentifier + '_OLD%';
        Integer attSizesSum = 0;
        Map<Id,Id> oldNewIdsMatchMap = new Map<Id,Id>(); //this is to populate new Ids from old ones and make the relation
        Map<Id,Attachment> oldAttsMap, newAttsMap;
        String ExceptionTextIf = NULL, newAttIdsForEmail = '', remainingAttsToProcess = '';
        Integer limitHeapSize = Integer.valueOf(System.Limits.getLimitHeapSize() / (1 + hPercentLess / 100));
        try{
            if(Global_Settings__c.getValues('Global').AttachmentBodyReplace_Ended__c == true){
                setEndedGlobalSettingValue(false);
                //to get how many rows to process, we cannot process more than 15 MiB due to heap restrictions
                List<Attachment> oldAttToMatch = [SELECT Id, BodyLength
                                                  FROM Attachment 
                                                  WHERE Description LIKE :likeOldIdentifier
                                                  AND BodyLength < :limitHeapSize
                                                  LIMIT :qLimit
                                                 ]; // This is to increment in a loop and get how many rows to get
                
                for(Attachment a : oldAttToMatch){ //get the limit to obtain only these IDs
                    attSizesSum += a.BodyLength;
                    if(attSizesSum <= limitHeapSize){ //this is less or equal 15 MiB
                        oldNewIdsMatchMap.put(a.Id, NULL);
                    } else {
                        break;
                    }
                }
                
                //Main map with the attachment data, get old atts
                oldAttsMap = new Map<Id,Attachment>([SELECT Id,CreatedDate,Description,Body 
                                                     FROM Attachment 
                                                     WHERE Id IN :oldNewIdsMatchMap.keySet()
                                                    ]);
                for(Id idMatch : oldNewIdsMatchMap.keySet()){
                    Attachment attMatch;
                    if(oldAttsMap.containsKey(idMatch)){
                        attMatch = oldAttsMap.get(idMatch);
                        List<String> strSplit = attMatch.Description.split(','); // this is splitting the new attachment information <batch>,<new att id>
                        Id newAttId;
                        if(strSplit.size() > 1){
                            newAttId = strSplit[1]; // get the new Att Id from the old Atts Information
                            if(newAttId <> NULL){
                                oldNewIdsMatchMap.put(attMatch.Id, newAttId);
                                newAttIdsForEmail += newAttId + ' ';
                            }
                        } 
                    }
                }
                //GET new atts that were populated from the oldAttToMatch match map
                newAttsMap = new Map<Id,Attachment>([SELECT Id,CreatedDate,Description,Body 
                                                     FROM Attachment 
                                                     WHERE Id IN :oldNewIdsMatchMap.values() // << the values are the new IDs
                                                    ]);
                
                for(Id oldId : oldNewIdsMatchMap.keySet()){
                    Id newId = oldNewIdsMatchMap.get(oldId);
                    if(oldAttsMap.containsKey(oldId) && newAttsMap.containsKey(newId)){
                        Attachment oldAtt = oldAttsMap.get(oldId);
                        Attachment newAtt = newAttsMap.get(newId);
                        
                        oldAtt.Description = 'ABR_UPDATED_OLD,' + globalBatchIdentifier + ',' + newAtt.Id;
                        newAtt.Description = 'ABR_UPDATED_NEW,' + globalBatchIdentifier + ',' + oldAtt.Id;
                        newAtt.Body = oldAtt.Body;
                    } else {
                        System.debug('KEY Not found for old attachment: ' + oldId);
                    }
                }
                
                update newAttsMap.values();
                update oldAttsMap.values();
                
                remainingAttsToProcess = String.valueOf([SELECT COUNT(Id)cnt FROM Attachment 
                                                         WHERE Description 
                                                         LIKE :likeOldIdentifier 
                                                         AND BodyLength < :limitHeapSize
                                                        ][0].get('cnt'));
                
                System.debug('Remaining attachments to process: ' + remainingAttsToProcess);
                
                setEndedGlobalSettingValue(true);
            }            
        } catch(Exception e){
            System.debug('ABR Exception: ' + e);
            ExceptionTextIf = e.getMessage() + '\n' + e.getStackTraceString();
        } finally {
            //String heapSizePercent = (((Decimal)System.Limits.getHeapSize() / (Decimal)System.Limits.getLimitHeapSize()) * 100).setScale(2).toPlainString();
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = emailForResponse.split(';');
            mail.setToAddresses(toAddresses);
            mail.setReplyTo('noreply@example.com');
            mail.setSenderDisplayName('Attachment Body Replace');
            mail.setSubject('Attachment Body Replace Finished [' + globalBatchIdentifier + ']');
            mail.setPlainTextBody(
                'Attachment Body Replace Job ' + globalBatchIdentifier + ' has finished. \n\n' +
                'Current Timestamp: ' + Datetime.now() + ' \n' +
                'CPU Time: ' + System.Limits.getCpuTime() + ' ms. \n' +
                'Heap Size: ' + System.Limits.getHeapSize() + ' of ' + System.Limits.getLimitHeapSize() + ' \n' + //', ' + heapSizePercent + '% \n' +
                'Percent saved from heap: ' + hPercentLess + ' \n' +
                'SOQL Queries: ' + System.Limits.getQueries() + ' \n' +
                'DML Statements: ' + System.Limits.getDMLStatements() + ' \n' +
                'Main Query Limit: ' + qLimit + ' \n' +
                'Rows In Old Query Map: ' + oldAttsMap.size() + ' \n' +
                'Rows In New Query Map: ' + newAttsMap.size() + ' \n' +
                'Remaining attachments to process: ' + remainingAttsToProcess + ' \n' +
                'Process went through with no previous one hanging? ' + Global_Settings__c.getValues('Global').AttachmentBodyReplace_Ended__c + ' \n\n' +
                'New Attachment Ids: \n' + newAttIdsForEmail + ' \n\n' +
                'Exception Text If: ' + ExceptionTextIf
            );
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
    
    public static Boolean setEndedGlobalSettingValue(Boolean value){
        update new Global_Settings__c(Id = 'a0Li0000002reovEAA', AttachmentBodyReplace_Ended__c = value);
        return Global_Settings__c.getValues('Global').AttachmentBodyReplace_Ended__c;
    }
}