/**=====================================================================
 * Experian
 * Name: SMXEDQGPDProcessCaseBtchSdlrCntxtTest
 * Description: 
 * Created Date: 19/04/2017
 * Created By: Diego Olarte
 * 
 * Date Modified     Modified By        Description of the update
 =====================================================================*/
@istest(seeAllData=TRUE)
global class SMXEDQGPDProcessCaseBtchSdlrCntxtTest{
    
    static testmethod void SmxcaseScheduler(){
        test.starttest();
        SMXEDQGPDProcessCaseBatchSdlrContext sc= new SMXEDQGPDProcessCaseBatchSdlrContext();
        String schedule = '0 0 4 * * ?';
        system.schedule('Scheduled Process Nomination Cases', schedule, sc);
        test.stoptest();
    }
    
}