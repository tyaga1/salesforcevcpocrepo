/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global interface MetadataProviderInterface {
    SObject CreateNewObject();
    String GetDefaultRecordTypeId();
    String GetSObjectKeyPrefix();
    String GetSObjectLabel();
    String GetSObjectPluralLabel();
    Boolean HasMiddleNameField();
    Boolean HasSuffixField();
    Boolean IsPersonAccountsOn();
    Boolean IsPersonAccountsOnStatic();
    Boolean IsRecordTypesOn();
    Boolean IsStateAndCountryPicklistsOn();
}
