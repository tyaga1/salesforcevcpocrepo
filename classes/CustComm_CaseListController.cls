public with sharing class CustComm_CaseListController {


    @AuraEnabled
    public static List<caseWrapper> findAll(String orderBy, String order, String status) {
        List<caseWrapper> caseList = new List<caseWrapper>();
        if(status == 'Open'){
          try {
                //RJ Added an new field Business_Service__c
              String query = 'SELECT id, CaseNumber, SNOW_Case_No__c, Source_System__c, Customer_Case_Number__c, Subject, toLabel(Status), CreatedDate, LastModifiedDate,Business_Service__c FROM Case Where RecordType.Name = \'Service Central\' AND isClosed = false Order By '
                          + orderBy + ' ' + order + ' LIMIT 5000';
              caseList = convertDates(Database.query(query));
          }
          catch (exception e) {
              throw new AuraHandledException('Some exception happened on server side controller with the following error:\n\n'+ e.getMessage());
          }
        }else if(status == 'Closed'){
          try {
                //RJ added an new field Business_Service__c
              String query = 'SELECT id, CaseNumber, SNOW_Case_No__c, Source_System__c, Customer_Case_Number__c, Subject, toLabel(Status), CreatedDate, LastModifiedDate,Business_Service__c FROM Case Where RecordType.Name = \'Service Central\' AND CreatedDate >= LAST_N_YEARS:2 AND isClosed = true Order By '
                          + orderBy + ' ' + order;
              caseList = convertDates(Database.query(query));
          }
          catch (exception e) {
              throw new AuraHandledException('Some exception happened on server side controller with the following error:\n\n'+ e.getMessage());
          }
        }
        
        return caseList;
       
    }

    public static List<caseWrapper> convertDates(List<Case> caseList){
        TimeZone userTimezone = UserInfo.getTimeZone();
        List<caseWrapper> caseWrapperList = new List<caseWrapper>();
        for(case c:caseList){
            caseWrapper cw = new caseWrapper();
            cw.Id = c.Id;

            if (c.Source_System__c == 'ServiceNow') {
                cw.CaseNumber = c.SNOW_Case_No__c;
            }
            else {
                cw.CaseNumber = c.CaseNumber;
            }
            
            if (c.Customer_Case_Number__c != null && c.Customer_Case_Number__c != '') {
                cw.CaseLink = c.Customer_Case_Number__c.unescapeHtml4().substringBetween('<a href=\"','\" target=');
            }
            else {
                cw.CaseLink = '';
            }

            cw.Subject = c.Subject;
            cw.Status = c.Status;
            //RJ added Business_Service__c
            cw.BusinessService  = c.Business_Service__c;
            cw.CreatedDate = c.CreatedDate.format('yyyy-MM-dd HH:mm:ss', userTimezone.getID());
            cw.LastModifiedDate = c.LastModifiedDate.format('yyyy-MM-dd HH:mm:ss', userTimezone.getID());
            cw.DateOnlyCreatedDate = c.CreatedDate.format('yyyy-MM-dd', userTimezone.getID());
            cw.DateOnlyLastModifiedDate = c.LastModifiedDate.format('yyyy-MM-dd', userTimezone.getID());
            caseWrapperList.add(cw);   
        }
        return caseWrapperList;
    }

    /****Wrapper Classes***/
    public class caseWrapper{
        @AuraEnabled
        public String Id {get; set;}
        @AuraEnabled
        public String CaseNumber {get; set;}
        @AuraEnabled
        public String Subject {get; set;}
        @AuraEnabled
        public String Status {get; set;}
         //RJ added Business_Service__c
        @AuraEnabled
        public String BusinessService {get; set;} 
        @AuraEnabled    
        public String CreatedDate {get; set;}
        @AuraEnabled
        public String LastModifiedDate {get; set;}
        @AuraEnabled
        public String CaseLink {get; set;}
        @AuraEnabled
        public String DateOnlyCreatedDate {get; set;}
        @AuraEnabled
        public String DateOnlyLastModifiedDate {get; set;}
        @AuraEnabled
        public String CreatedBy {get; set;}

        public caseWrapper(){

        }
    }


}