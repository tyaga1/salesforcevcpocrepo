/**=====================================================================
 * Name: AccountPlan_ClientBackground_Ctlr_Test
 * Description: See case #01848189 - Account Planning Enhancements
 * Created Date: Apr. 20th, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By            Description of the update
 * Apr. 20th, 2016        James Wills           Case #01848189: Created
 * May 6th, 2016         James Wills            Changed modifiers on class and test method  
 * June 7th, 2016        James Wills            Put in changes for setting up test data to prevent mixed DML error messages.
  =====================================================================*/
@isTest
private class AccountPlan_ClientBackground_Ctlr_Test{
   
   
   private static testmethod void test_buildAccountTeamOpenCasesList(){
   
    List<Account_Plan__c> newAccountPlanList = [SELECT id, Name, Account__c FROM Account_Plan__c];
    
    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)newAccountPlanList[0]);    
    AccountPlan_ClientBackground_Controller controller = new AccountPlan_ClientBackground_Controller(stdController);

    //ApexPages.StandardController accController = new ApexPages.StandardController((sObject)testAccount1);

    Test.startTest();
      
      //1
      controller.clientBackgroundTabUpdateCookies();
      
      //2
      controller.buildAccountTeamOpenCasesList();
      
      //3  
      controller.viewCase();
      
      //4
      controller.viewAccountPlanCaseListFull();
      
      //5
      controller.buildAccountTeamOpenCasesList();
      Integer sizeOfCaseList = controller.openCasesForAccountTeamList.size();
      controller.seeMoreCases();      
      controller.buildAccountTeamOpenCasesList();
      
      system.assert(sizeOfCaseList + (Integer)Account_Plan_Related_List_Sizes__c.getInstance().Account_Plan_Case_List__c == controller.openCasesForAccountTeamList.size(), 'AccountPlan_ClientBackground_Ctlr_Test: There was a problem with the method seeMoreCases(): ' 
                   + sizeOfCaseList + ' ' + controller.openCasesForAccountTeamList.size());  
      
      
      //6
      controller.backToAccountPlan();

    
    Test.stopTest();
  
  
  }
  
  @testSetup
  private static void setupTestData(){
  
    User thisUser = [SELECT id FROM User WHERE id =:UserInfo.getUserId()];
  
    //Create data
    Account testAccount1 = Test_Utils.createAccount();  
    Contact con = Test_Utils.createContact(testAccount1.Id);
    Profile profile1 = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN];
    User user1 = Test_Utils.createUser(profile1, 'test@experian.com', 'Test1');
    
    System.runAs(thisUser){
      insert testAccount1;
      insert con;
      insert user1;    
    }
    
    Account_Plan__c newAccountPlan = new Account_Plan__c(Account__c = testAccount1.id);
    
    insert newAccountPlan;
    
    
    System.runAs(thisUser){
      String naCCMRecordType = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_CASE, Constants.CASE_REC_TYPE_NA_CCM_Support);
    
      List<Case> caseList = new List<Case>();
      Integer i=0;
      for(i=0;i<25;i++){
        Case newCaseForAccount = new Case(OwnerId      = user1.Id,
                                        AccountId    = testAccount1.id,
                                        RecordTypeId = naCCMRecordType);   
        caseList.add(newCaseForAccount);
      }        
      insert caseList;
    }
    
    Account_Plan_Related_List_Sizes__c custListSizes = new Account_Plan_Related_List_Sizes__c(
      Account_Plan_Case_List__c = 10
    );
    insert custListSizes;
    
  }
  
}