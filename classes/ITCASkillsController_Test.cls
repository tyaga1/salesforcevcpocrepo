/**=====================================================================
 * Name: ITCASkillsController_Test
 * Description: Test class for ITCASkillsController
 * Created Date: August 30th 2017
 * Created By: Malcolm Russell
 * 
 *  Date Modified      Modified By                  Description of the update
 *  
 * =====================================================================*/
@isTest
private class ITCASkillsController_Test {

    static testMethod void myUnitTest1() {
        
    //Experian_Skill_Set__c ExSkillSet = new Experian_Skill_Set__c(Name='Big Data Development',Career_Area__c='Data');
    Experian_Skill_Set__c ExSkillSet = [select id from Experian_Skill_Set__c limit 1];

        //insert ExSkillSet; 
        createprofile(); 
    User tu = [select id from user where email='test1234@experian.com'];
    // System.RunAs(tu){
    PageReference pageRef = Page.ITCA_Skill_Matrix;
        Test.setCurrentPage(pageRef);
    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)ExSkillSet);
    
    ITCASkillsController ext = new ITCASkillsController(stdController);
    
    
    
    List<SelectOption> level1So =ext.level1_CareerAreaOptions;
    List<SelectOption> level2So =ext.level2_skillSetOptions;
    List<SelectOption> level3So =ext.level3_sfiaLevelOptions;
    List<SelectOption> level4So =ext.level4_careerLevelOptions;
    List<SelectOption> level5So =ext.level5_sfiaSkillOptions;
    
    List<ITCASkillsController.careerLevelsForExperianSkill> careerLevelsWrapperList = ext.careerLevelsWrapperList;  
    Map<String, Map<String, List<ITCASkillsController.careerLevelsForExperianSkill>>> careerLevelsWrapperMap=ext.careerLevelsWrapperMap;
    ext.level1_SelectedCareerArea='Applications';
    ext.level2_selectedSkillSet='Software Development';
    ext.level3_SelectedSFIALevel='Level4';
    ext.level4_SelectedCareerLevel='Professional';
    ext.level5_selectedSFIASkill=[SELECT id FROM ProfileSkill LIMIT 1].id;
    
    
    Set<String> level1 = ext.level1_SelectedCareerAreas_Set;
    Set<String> level2 = ext.level2_selectedSkillSets_Set;
    Set<String> level3 = ext.level3_selectedSFIALevels_Set;
    Set<String> level4 = ext.level4_selectedCareerLevels_Set;
    Set<String> level5 = ext.level5_selectedSFIASkill_Set;
    
    ext.resetSkillList();
    
    list<Skill_Set_To_Skill__c> malc = new list<Skill_Set_To_Skill__c>( [SELECT id, Experian_Skill_Set__c, Experian_Skill_Set__r.Career_Area__c, Experian_Skill_Set__r.Name, Level__c, Experian_Role__c, Skill__c  
                                                    FROM Skill_Set_To_Skill__c]);
                                                    
    system.debug('malc:::' + malc);
    
    
    
    list<Experian_Skill_Set__c> malc2 = new list<Experian_Skill_Set__c>( [SELECT id, Career_Area__c,Name 
                                                    FROM Experian_Skill_Set__c]);
                                                    
    system.debug('malc2:::' + malc2);
    
    //}
  }
  
  @testSetup
  private static void createData(){
    
    buildProfileSkills();
    buildExperianSkillSet();
    buildSkillSetToSKill();
   
   
  }

  private static void buildProfileSkills(){
  
    ProfileSkill ps1 = new ProfileSkill(Name='Apex', 
                                        Level5__c='Maintains Code.', 
                                        Level6__c='Writes test classes.', 
                                        Level7__c='Designs Code.',
                                        Sfia_Skill__c=false);
                                        
    ProfileSkill ps2 = new ProfileSkill(Name='Programming & Software Development', 
                                        Level5__c='Maintains Code.', 
                                        Level6__c='Writes test classes.', 
                                        Level7__c='Designs Code.',
                                        Sfia_Skill__c=true);                                  

    List<ProfileSkill> ps_List = new List<ProfileSkill>{ps1,ps2};
    insert ps_List;
    
  }

  private static void buildExperianSkillSet(){
    Experian_Skill_Set__c exp1 = new Experian_Skill_Set__c(Name='Software Development', Career_Area__c='Applications');
    insert exp1;
  
  }

  private static void buildSkillSetToSKill(){
    List<ProfileSkill> psList = [SELECT id, Name FROM ProfileSkill];
    List<Experian_Skill_Set__c> essList = [SELECT id FROM Experian_Skill_Set__c];
   
    Skill_Set_to_Skill__c sts1 = new Skill_Set_to_Skill__c(Name='Software Development - Application Support', 
                                                          Skill__c=[SELECT id FROM ProfileSkill where Sfia_Skill__c=true LIMIT 1].id, 
                                                          Experian_Role__c='Professional',
                                                          Experian_Skill_Set__c=essList[0].id,
                                                          Level__c='Level4');
    insert sts1;
  
 }

  private static void createProfile(){
  
    Profile p = [SELECT Id FROM Profile WHERE Name=: Constants.PROFILE_SYS_ADMIN ];
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    insert testUser1;
    
    Career_Architecture_User_Profile__c caup1 = new Career_Architecture_User_Profile__c(//Career_Area__c=,
                                                                                        //Date_Discussed_with_Employee__c=,
                                                                                        //Discussed_with_Employee__c=,
                                                                                        Employee__c= UserInfo.getUserId(),
                                                                                        //Manager_Comments__c=,
                                                                                        //Role__c=,
                                                                                        State__c='Current',
                                                                                        Status__c='Submitted to Manager');
    insert caup1;
     
    Career_Architecture_Skills_Plan__c casp1 = new Career_Architecture_Skills_Plan__c(Career_Architecture_User_Profile__c=caup1.id,
                                                                                      Experian_Skill_Set__c=[SELECT id FROM Experian_Skill_Set__c LIMIT 1].id,
                                                                                      Level__c='Level4',
                                                                                      Skill__c=[SELECT id FROM ProfileSkill LIMIT 1].id  );
    
    insert casp1;
  }
    
   
}