/*=============================================================================
 * Experian
 * Name: BatchProjectEDQRevenueUpdate_Test 
 * Description: Case :02187106 : Test class for BatchProjectEDQRevenueUpdate
 * Created Date:16th Jan 2017
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
 ============================================================================*/
@isTest
private class BatchProjectEDQRevenueUpdate_Test {
    static testmethod void testCreateDeliveryLine(){
        Project__c proj1 = Test_Utils.insertProject(true);      
        
        Delivery_Line__c dl = Test_Utils.insertDeliveryLine(false, proj1.Id);
        dl.Revenue__c = 200;
        insert dl;
        
        Database.executeBatch(new BatchProjectEDQRevenueUpdate());
    }
}