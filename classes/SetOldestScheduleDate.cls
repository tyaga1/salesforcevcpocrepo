/**=====================================================================
 * Appirio, Inc
 * Name: SetOldestScheduleDate
 * Description: The following batch class is designed to be scheduled to run every day.
                    This class will get all Opportunity line item schedules and determine the oldest one to add its date to the line item
 * Created Date: Aug 4th, 2015
 * Created By: Diewgo Olarte (Experian)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
global class SetOldestScheduleDate implements Database.Batchable<sObject> {

  global Database.Querylocator start ( Database.BatchableContext bc ) {
  
    String query = 'select id, SystemModStamp, Oldest_Schedule__c from OpportunityLineItem';
   
    return Database.getQueryLocator (query);
  }

  global void execute (Database.BatchableContext bc, List<OpportunityLineItem> scope) {

    List<Date> dlist = new List<Date>();
    integer i=0;
    
    List<OpportunityLineItem> oppLineItemtoUpdate = new List<OpportunityLineItem>();
    
    for(OpportunityLineItem oli:[select id, SystemModStamp, Oldest_Schedule__c,Product_Days_to_Bill__c,Opportunity.CloseDate,(select id,ScheduleDate,OpportunityLineItemId
                                from OpportunityLineItemSchedules) from OpportunityLineItem where SystemModStamp >= LAST_N_DAYS:3 AND id in :scope])
            {
                for(OpportunityLineItemSchedule olis : oli.OpportunityLineItemSchedules) 
                    {
                            dlist.add(olis.ScheduleDate);
                    }
               if(dlist.size() > 0) { 
                dlist.sort();
                oli.Oldest_Schedule__c = dlist[0];
                
                date cDate = oli.Opportunity.CloseDate;
                date oSchedule = oli.Oldest_Schedule__c;
                integer dtoBill = cDate.daysBetween(oSchedule);

                oli.Product_Days_to_Bill__c = dtoBill;
                
                oppLineItemtoUpdate.add(oli);
                
                dlist.clear();
                i=0;}
            } 
  
      update oppLineItemtoUpdate;
          
    }   

    //To process things after finishing batch
    global void finish (Database.BatchableContext bc) {
      AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                               TotalJobItems, CreatedBy.Email
                        FROM AsyncApexJob WHERE Id =: BC.getJobId()];
      
      System.debug('\n[SumScheduleRevenueCurrentFY: finish]: [The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.]]');

      }
    
}