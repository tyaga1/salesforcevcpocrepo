/**=====================================================================
 * Appirio, Inc
 * Name: BatchAccountSegmentUserUpdate_Test
 * Description: Test class for Batch job for updating Account Segment and/or Opportunities 
                of a particular user if Reorg is processed
 * Created Date: Sep 9th, 2015
 * Created By: Noopur Sundriyal
 *
 * Date Modified                Modified By                Description of the update
 * 21 Sep, 2015                 Noopur                     Added the asserts in the class.   
 =======================================================================*/
@isTest
private class BatchAccountSegmentUserUpdate_Test {

    static testMethod void myUnitTest() {
      test.StartTest();
      BatchAccountSegmentUserUpdate bat = new BatchAccountSegmentUserUpdate();
      Database.executeBatch(bat);
      test.StopTest();        
    }
    
    @testSetup 
    static void createTestData() {
      Account testAcc = Test_Utils.createAccount();
      insert testAcc;
  
      Hierarchy__c grandParentHierarchy = new Hierarchy__c();
      grandParentHierarchy.Type__c = 'Global Business Line';
      grandParentHierarchy.Value__c = 'Grand Parent';
      grandParentHierarchy.Unique_Key__c = 'Global Business Line-Grand Parent';
      insert grandParentHierarchy;
  
      Hierarchy__c parentHierarchy = new Hierarchy__c();
      parentHierarchy.Type__c = 'Business Line';
      parentHierarchy.Value__c = 'Parent';
      parentHierarchy.Unique_Key__c = 'Business Line-Parent';
      parentHierarchy.Parent__c = grandParentHierarchy.Id;
      insert parentHierarchy;
  
      Hierarchy__c childHierarchy = new Hierarchy__c();
      childHierarchy.Type__c = 'Business Unit';
      childHierarchy.Value__c = 'Child';
      childHierarchy.Parent__c = parentHierarchy.Id;
      childHierarchy.Unique_Key__c = 'Business Unit-Child';
      insert childHierarchy;
  
      Account_Segment__c segment = Test_Utils.insertAccountSegment(false, testAcc.Id, grandParentHierarchy.Id, null);
      segment.Type__c = 'Global Business Line';
      segment.Value__c = 'Grand Parent';
      insert segment;
  
      Account_Segment__c segment2 = Test_Utils.insertAccountSegment(false, testAcc.Id, parentHierarchy.Id, segment.Id);
      segment2.Type__c = 'Business Line';
      segment2.Value__c = 'Parent';
      insert segment2;
  
      Account_Segment__c segment3 = Test_Utils.insertAccountSegment(false, testAcc.Id, childHierarchy.Id, segment2.Id);
      segment3.Type__c = 'Business Unit';
      segment3.Value__c = 'Child';
      insert segment3;
  
      Account_Segmentation_Mapping__c mapping = new Account_Segmentation_Mapping__c();
      mapping.Name = 'Child';
      mapping.Global_Business_Line__c = 'Grand Parent';
      mapping.Business_Line__c = 'Parent';
      mapping.Business_Unit__c = 'Child';
      mapping.Common_View_Name__c = 'CSDA';
      mapping.Field_Set_API_Name__c = 'CSDA';
      insert mapping;
      
      TriggerSettings__c insertTriggerSettings = Test_Utils.insertTriggerSettings(Constants.USER_TRIGGER);
      insertTriggerSettings.IsActive__c = false;
      update insertTriggerSettings;
      
      User usr = [Select Id,UserReOrgProcessed__c From User where Id = :userinfo.getUserId()];
      usr.UserReOrgProcessed__c = true;
      usr.Business_Unit__c = segment3.Id;
      usr.Business_Line__c = segment2.Id;
      usr.Global_Business_Line__c = segment.Id;
      update usr;
    }
}