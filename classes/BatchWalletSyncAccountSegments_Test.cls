/**=====================================================================
 * Appirio, Inc
 * Name: BatchWalletSyncAccountSegments_Test
 * Description: Test class for Batch job for updating Account Segment from Wallet Sync
 * Created Date: Aug 24th, 2015
 * Created By: Noopur Sundriyal
 *
 * Date Modified                Modified By                  Description of the update
 * Oct 1st, 2015                Paul Kissick                 Changing lastStartDate to Wallet_Sync_Processing_Last_Run__c
 =======================================================================*/
@isTest
private class BatchWalletSyncAccountSegments_Test {

  static testMethod void myUnitTest() {
    Test.startTest();
    BatchWalletSyncAccountSegments batWS = new BatchWalletSyncAccountSegments();
    Database.executeBatch(batWS);
    Test.stopTest();
      
    // Verify The Results
    List<WalletSync__c> wSyncs = [SELECT Id,CNPJ_Number__c,Agency_Name__c,Business_Area__c,
                                         Business_Group__c,Channel__c, Region__c,Sector__c,
                                         Segment1__c,Segment2__c,Segment3__c 
                                  FROM WalletSync__c 
                                  WHERE CNPJ_Number__c = '08811226001903'];
    for (Account_Segment__c accSeg : [SELECT LATAM_Main_Activity__c, LATAM_Agency_Name__c, LATAM_Client_Since__c, 
                                             LATAM_Client_Situation__c, LATAM_Channel__c, LATAM_Segment1__c, 
                                             LATAM_Segment2__c, LATAM_Segment3__c, LATAM_Business_Group__c, LATAM_Region__c,
                                             LATAM_Sector__c, LATAM_Business_Area__c
                                      FROM Account_Segment__c
                                      WHERE Account__r.CNPJ_Number__c = '08811226001903']) {
      system.assertEquals(accSeg.LATAM_Agency_Name__c,    wSyncs[0].Agency_Name__c);
      system.assertEquals(accSeg.LATAM_Business_Area__c,  wSyncs[0].Business_Area__c);
      system.assertEquals(accSeg.LATAM_Business_Group__c, wSyncs[0].Business_Group__c);
      system.assertEquals(accSeg.LATAM_Channel__c,        wSyncs[0].Channel__c);
      system.assertEquals(accSeg.LATAM_Region__c,         wSyncs[0].Region__c);
      system.assertEquals(accSeg.LATAM_Sector__c,         wSyncs[0].Sector__c);
      system.assertEquals(accSeg.LATAM_Segment1__c,       wSyncs[0].Segment1__c);
      system.assertEquals(accSeg.LATAM_Segment2__c,       wSyncs[0].Segment2__c);
      system.assertEquals(accSeg.LATAM_Segment3__c,       wSyncs[0].Segment3__c);
    }
  }
    
  @testSetup
  static void createData() {
    Account testAcc = Test_Utils.createAccount();
    testAcc.CNPJ_Number__c = '08811226001903';
    insert testAcc;

    List<Account_Segment__c> accSegList = new List<Account_Segment__c>();

    Hierarchy__c grandParentHierarchy = new Hierarchy__c();
    grandParentHierarchy.Type__c = 'Global Business Line';
    grandParentHierarchy.Value__c = 'Grand Parent';
    grandParentHierarchy.Unique_Key__c = 'My BU';
    insert grandParentHierarchy;
    
    Hierarchy__c parentHierarchy = new Hierarchy__c();
    parentHierarchy.Type__c = 'Business Line';
    parentHierarchy.Value__c = 'Parent';
    parentHierarchy.Unique_Key__c = 'My BU Blah';
    parentHierarchy.Parent__c = grandParentHierarchy.Id;
    insert parentHierarchy;

    Hierarchy__c childHierarchy = new Hierarchy__c();
    childHierarchy.Type__c = 'Business Unit';
    childHierarchy.Value__c = 'Child';
    childHierarchy.Parent__c = parentHierarchy.Id;
    childHierarchy.Unique_Key__c = 'My BU Blah2';
    insert childHierarchy;

    Account_Segment__c segment = Test_Utils.insertAccountSegment(false, testAcc.Id, grandParentHierarchy.Id, null);
    accSegList.add(segment);

    Account_Segment__c segment2 = Test_Utils.insertAccountSegment(false, testAcc.Id, parentHierarchy.Id, segment.Id);
    accSegList.add(segment2);

    Account_Segment__c segment3 = Test_Utils.insertAccountSegment(false, testAcc.Id, childHierarchy.Id, segment2.Id);
    accSegList.add(segment3);

    insert accSegList;        
    WalletSync__c wSync = new WalletSync__c();
    //wSync.Processed__c = false;
    wSync.CNPJ_Number__c = '08811226001903';
    wSync.Account__c = testAcc.Id;
    wSync.LegacyCRM_Account_ID__c = 'testttt';
    wSync.Agency_Name__c = 'testAgency';
    wSync.Business_Area__c = 'BArea';
    wSync.Business_Group__c = 'BGroup';
    wSync.Channel__c = 'Channel';
    wSync.Region__c = 'Region';
    wSync.Sector__c = 'sec1';
    wSync.Segment1__c = 'SEG1';
    wSync.Segment2__c = 'SEG2';
    wSync.Segment3__c = 'SEG3';
    wSync.Last_Processed_Date__c = Datetime.now().addMinutes(-20);
    insert wSync;
    
    Global_Settings__c globalSetting = Global_Settings__c.getInstance('Global');
    globalSetting.Batch_Failures_Email__c = 'test@email.com';
    globalSetting.Wallet_Sync_Processing_Last_Run__c = system.now().addDays(-1);
    update globalSetting;
  }
  
   
}