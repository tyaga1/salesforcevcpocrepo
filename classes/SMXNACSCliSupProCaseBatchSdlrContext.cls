/**=====================================================================
 * Experian
 * Name: SMXNACSCliSupProCaseBatchSdlrContext 
 * Description: 
 * Created Date: 18/01/2016
 * Created By: Diego Olarte
 * 
 * Date Modified     Modified By        Description of the update
 *
 =====================================================================*/
 
global class SMXNACSCliSupProCaseBatchSdlrContext implements Schedulable {

  global SMXNACSCliSupProCaseBatchSdlrContext(){}
  
  global void execute(SchedulableContext ctx){
    SMXNACSClientSupportProcessCaseBatch b = new SMXNACSClientSupportProcessCaseBatch();
    if (!Test.isrunningTest()) {
      Database.executeBatch(b);
    }
  }

}