/*=============================================================================
 * Experian
 * Name: ScheduleRegionalScorecardProcess
 * Description: 
 * Created Date: 24 May 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 

 =============================================================================*/

global class ScheduleRegionalScorecardProcess implements Schedulable {

  global void execute(SchedulableContext sc) {
    // Consider making a single method call here, or batch.
    RegionalScorecardUtils.startBatch();
  }
  
}