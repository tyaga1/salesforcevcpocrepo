public class SchedulerUATReminderForAgileAccelerator implements Schedulable
{
	public void execute(SchedulableContext sc)
    {
        Database.executeBatch(new BatchUATReminderForAgileAccelerator());
    }
}