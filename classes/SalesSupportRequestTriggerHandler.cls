/**=============================================================================
 * Experian plc
 * Name: SalesSupportRequestTriggerHandler
 * Description: Case #9748
 * Created Date: Dec 8th, 2014
 * Created By: James Weatherall
 * 
 * Date Modified        Modified By                  Description of the update
 * Dec 12th, 2014    James Weatherall       Changed method to createSalesSupportRelatedItems
 *                            Added code to bring Contacts through.
 * Mar 5th, 2014      James Weatherall       Case #00009748: Added beforeUpdate, beforeInsert and setOwnerManager methods
 * Dec 13th, 2016     Manoj Gopu             Case #02196955: Added new Method setOwnerToOpprtunityTeam and calling in beforeUpdate, AfterInsert
 * Dec 27th, 2016     Diego Olarte           Case #02184830: Updated Method createSalesSupportRequest to get the Decision Maker as default for Automotive
 * Jan 31st, 2017     Sanket Vaidya          Case #02265406: Added addPreSales_NA_EDQ_OwnerToOpportunityTeam()
 * Mar 7th, 2017      Sanket Vaidya          Case #02300472: Added condition [OpportunityContactRoles.size() > 0] to check list has a value and not empty.
 * May 3rd, 2017      Sanket Vaidya          Case #02308838: Serasa MS - Added create_Project_when_SSR_Is_Saved_As_Evaluation_Complete()
 * Aug 08th, 2017     Manoj Gopu             Case #1340777:  Updated setOwnerToOpprtunityTeam for New SSR Presales Request ANZ DQT
 =============================================================================*/

public class SalesSupportRequestTriggerHandler {

  public static set<Id> oppTeamMemberIds;  

  public static void beforeInsert(List<Sales_Support_Request__c> newList) {
    setOwnerManager(newList);
    createSalesSupportRequest(newList);
  }    
    
  public static void beforeUpdate(Map<Id, Sales_Support_Request__c> newMap, Map<Id, Sales_Support_Request__c> oldMap) {
    setOwnerManager(newMap.values());
    setOwnerToOpprtunityTeam(newMap.values(),oldMap); // Added by Manoj      
    create_Project_when_SSR_Is_Saved_As_Evaluation_Complete(newMap);  
  }
    
  public static void afterInsert(Map<Id, Sales_Support_Request__c> newMap) {
    createSalesSupportRelatedItems(newMap);
    setOwnerToOpprtunityTeam(newMap.values(),null); //Added by Manoj        
  }

  public static void afterUpdate(Map<Id, Sales_Support_Request__c> newMap)
  {
    addPreSales_NA_EDQ_OwnerToOpportunityTeam(newMap);
  }
  
  public static void createSalesSupportRequest(List<Sales_Support_Request__c> newList)
  {
      Set<Id> setOpportunityId = new Set<Id>();        
      
      list<Sales_Support_Request__c> lstUpdateSalesSupportRequest = new list<Sales_Support_Request__c>();
      Map<Id, OpportunityContactRole> mapOppContacts = new Map<Id, OpportunityContactRole>();
          
          Id devRecordTypeId = Schema.SObjectType.Sales_Support_Request__c.getRecordTypeInfosByName().get(label.Automotive_Support_Request).getRecordTypeId();
          system.debug('DO Record Type:' + devRecordTypeId);
          
          for(Sales_Support_Request__c ssr : newList)
          {
              if(ssr.RecordtypeId==devRecordTypeId && ssr.Opportunity__c != null){
                  setOpportunityId.add(ssr.Opportunity__c);   
              }
          }

          for(Opportunity opp : [select Id, (Select Id, ContactId from OpportunityContactRoles where Role = 'Decision Maker' Limit 1) from Opportunity where Id in: setOpportunityId])
          {
            if(opp.OpportunityContactRoles.size() > 0)
            {
              mapOppContacts.put(opp.Id, opp.OpportunityContactRoles);
            }
          } 

          for(Sales_Support_Request__c ssr : newList)
          {
              if(ssr.RecordtypeId==devRecordTypeId && ssr.Opportunity__c != null)
              {
                  if(!mapOppContacts.isEmpty() && mapOppContacts.containsKey(ssr.Opportunity__c))
                  {
                      ssr.Client_Contact__c = mapOppContacts.get(ssr.Opportunity__c).ContactId;
                  }    
              }
          }      
  }
    
  public static void createSalesSupportRelatedItems(Map<Id, Sales_Support_Request__c> newMap)
  {
    List<Sales_Support_Competitor__c> lstSalesSupportComps = new List<Sales_Support_Competitor__c>();      
    List<Sales_Support_Key_Contact__c> lstSalesSupportKCs = new List<Sales_Support_Key_Contact__c>();      
    Map<Id, Sales_Support_Request__c> mapValidSSRs = new Map<Id, Sales_Support_Request__c>();
    Map<Id, List<Competitor__c>> mapOppComps = new Map<Id, List<Competitor__c>>();
    Map<Id, List<OpportunityContactRole>> mapOppContacts = new Map<Id, List<OpportunityContactRole>>();
    Set<Id> oppIds = new Set<Id>();      
      
    // Identify the Opp Ids
    for(Sales_Support_Request__c ssr : newMap.values()) {
      if(ssr.Opportunity__c != null) {
        mapValidSSRs.put(ssr.Id, ssr);
        oppIds.add(ssr.Opportunity__c);
        System.debug('Opp to Test Id:' + ssr.Opportunity__c);
      }
    }
    
    // Fetch all associated Competitors
    for(Opportunity opp : [select Id, (Select Id from Competitors__r), (Select Id, ContactId from OpportunityContactRoles) from Opportunity where Id in: oppIds]) {
      mapOppComps.put(opp.Id, opp.Competitors__r);
      mapOppContacts.put(opp.Id, opp.OpportunityContactRoles);
    }
    
    Set<Id> setContacts = new Set<Id>();
      
    for(Sales_Support_Request__c ssr : mapValidSSRs.values()) {
      for(Competitor__c comp : mapOppComps.get(ssr.Opportunity__c)) {
        lstSalesSupportComps.add(new Sales_Support_Competitor__c(Sales_Support_Request__c = ssr.Id, Opportunity_Competitor__c = comp.Id));    
      }
      // Loop around Contact Roles. If a Contact has multiple roles then only add them once.
      for(OpportunityContactRole cont : mapOppContacts.get(ssr.Opportunity__c)) {
        if(!setContacts.contains(cont.ContactId)) {
          setContacts.add(cont.ContactId);
          lstSalesSupportKCs.add(new Sales_Support_Key_Contact__c(Sales_Support_Request__c = ssr.Id, Contact__c = cont.ContactId));        
        }        
      }
      setContacts.clear();
    }
      
    if(lstSalesSupportComps.size() > 0) {
      try {
        insert lstSalesSupportComps;
      } catch(DMLException ex) {
          System.debug('\n[SalesSupportRequestTriggerHandler: createSalesSupportRelatedItems : Competitors]: ['+ex.getMessage()+']]');   
      }
    } 
      
    if(lstSalesSupportKCs.size() > 0) {
      try {
        insert lstSalesSupportKCs;
      } catch(DMLException ex) {
          System.debug('\n[SalesSupportRequestTriggerHandler: createSalesSupportRelatedItems : Key Contacts]: ['+ex.getMessage()+']]');   
      }
    } 
  }
    
  public static void setOwnerManager(List<Sales_Support_Request__c> newList) {
    Set<Id> ownerIds = new Set<Id>();
    Map<Id, User> ownerManagersMap = New Map<Id, User>();

    for(Sales_Support_Request__c owners : newList)
    {
      if(!ownerIds.contains(owners.OwnerId)) {
        ownerIds.add(owners.OwnerId);
      }
    }
      
    if(ownerIds.size() > 0) {
      ownerManagersMap.putAll([select ManagerId from User where Id IN: ownerIds]);
    }
      
    for(Sales_Support_Request__c ssr : newList)
    {
      try {
        ssr.Owner_Manager__c = ownerManagersMap.get(ssr.OwnerId).ManagerId;  
      } catch(Exception ex) {
        System.debug('\n[SalesSupportRequestTriggerHandler: setOwnerManager ]: ['+ex.getMessage()+']]'); 
      }          
    }
  }
  
  public static void setOwnerToOpprtunityTeam(List<Sales_Support_Request__c> newList,map<Id,Sales_Support_Request__c> oldMap){
     
     Map<String,set<String>> mapOppToOwnerid = New Map<String,set<String>>();//Used to store Opp id with the ownerIds   
     for(Sales_Support_Request__c ssr : newList)
     {
        If(ssr.Opportunity__c != null &&string.valueOf(ssr.OwnerId).startsWith('005') && ((ssr.Assign_Owner_to_Opportunity_Team__c == True &&               
                (oldMap == null || 
                  (oldMap!=null && 
                    (ssr.Assign_Owner_to_Opportunity_Team__c != oldMap.get(ssr.Id).Assign_Owner_to_Opportunity_Team__c || 
                      ssr.Opportunity__c != oldMap.get(ssr.Id).Opportunity__c || 
                      ssr.OwnerId != oldMap.get(ssr.Id).OwnerId)
                    )
                  )
                )
                ||
                (ssr.RecordTypeId == Record_Type_Ids__c.getInstance().SSR_Presales_Request_ANZ_DQT__c || (oldMap == null || (oldMap!=null && (ssr.Opportunity__c != oldMap.get(ssr.Id).Opportunity__c ||  ssr.OwnerId != oldMap.get(ssr.Id).OwnerId))))))
        {
            if(mapOppToOwnerid.containsKey(ssr.Opportunity__c))
            {
                set<string> lstOwn = mapOppToOwnerid.get(ssr.Opportunity__c);
                lstOwn.add(ssr.OwnerId);
                mapOppToOwnerid.put(ssr.Opportunity__c,lstOwn);         
            }
            else
            {
                set<string> lstOwn = new set<string>();
                lstOwn.add(ssr.OwnerId);
                mapOppToOwnerid.put(ssr.Opportunity__c,lstOwn); 
            }       
        }
     }
     if(mapOppToOwnerid.isEmpty())
        return;
     
     set<string> setOppOwner = new set<string>();
     //Get the existing opportunity team members
     for(opportunityTeamMember oppTeam:[select id,OpportunityId,UserId from opportunityTeamMember where OpportunityId=:mapOppToOwnerid.keySet()]){
        setOppOwner.add(oppTeam.OpportunityId+'::'+oppTeam.UserId);
     }
     
     list<opportunityTeamMember> newOppTeamMember = new list<opportunityTeamMember>();
     
     for(string strOppId:mapOppToOwnerid.keySet()){
        for(string strOwnId:mapOppToOwnerid.get(strOppId)){
             if(!(setOppOwner!=null && setOppOwner.contains(strOppId+'::'+strOwnId))){//Checking whether the owner is already in Team member
                 opportunityTeamMember newOppMem = new opportunityTeamMember();                 
                 newOppMem.OpportunityId=strOppId;
                 newOppMem.UserId = strOwnId;
                 newOppMem.TeamMemberRole = Constants.Opportunity_Team_Role_Consultant ;  
                 newOppMem.OpportunityAccessLevel = 'Edit';               
                 newOppTeamMember.add(newOppMem);                                  
             }
        }
     }
    
     if(!newOppTeamMember.isEmpty())
        Database.insert(newOppTeamMember);
  }

  public static void addPreSales_NA_EDQ_OwnerToOpportunityTeam(Map<Id, Sales_Support_Request__c> newMap)
  {
    List<OpportunityTeamMember> listNewOppTeamMembers = new List<OpportunityTeamMember>();

    Map<Id,Id> mapOpportunityAndOwner = new Map<Id,Id>();

    for(Sales_Support_Request__c ssr : [Select Id, Opportunity__r.Id, OwnerId from Sales_Support_Request__c where Id in :newMap.keySet()])
    {
        mapOpportunityAndOwner.put(ssr.Opportunity__r.Id, ssr.OwnerId);
    }

    List<AggregateResult> aggResult = [SELECT User.Name NM, UserId, Count(Id) FROM OpportunityTeamMember 
                                                      WHERE
                                                          OpportunityId =: mapOpportunityAndOwner.keySet() 
                                                          AND UserId in :mapOpportunityAndOwner.values()
                                                          group by User.Name, UserID]; 

    Map<String, Integer> mapUserIdToCount = new Map<String, Integer>();
    for(AggregateResult ar: aggResult)
    {
        mapUserIdToCount.put(String.valueOf(ar.get('UserId')), Integer.valueOf(ar.get('expr0')));
    }

    for(Sales_Support_Request__c ssr: newMap.values())
    {   
      if(ssr.Opportunity__c != null)
      {
        if(ssr.RecordTypeID == Record_Type_Ids__c.getInstance().SSR_Presales_Request_NA_EDQ__c
            && string.valueOf(ssr.OwnerId).startsWith('005')) //NA EDQ Pre Sales Support       
        {

          if(!mapUserIdToCount.containsKey(ssr.OwnerId))
          {              
              OpportunityTeamMember newOppTeamMember = new OpportunityTeamMember();                 
              newOppTeamMember.OpportunityId = ssr.Opportunity__c;
              newOppTeamMember.UserId = ssr.OwnerId;
              newOppTeamMember.TeamMemberRole = Constants.TEAM_ROLE_PRE_SALES_CONSULTANT ;  
              newOppTeamMember.OpportunityAccessLevel = 'Edit';               
              listNewOppTeamMembers.add(newOppTeamMember);                  
          }         
        }
      }
    }
    
    if(! listNewOppTeamMembers.isEmpty())
    {        
        UPSERT listNewOppTeamMembers;
    }
  }  

  public static void create_Project_when_SSR_Is_Saved_As_Evaluation_Complete(Map<Id, Sales_Support_Request__c> newMap)
  {
      Map<Id, Project__c> newProjects = new Map<Id, Project__c>();
      
      for(Sales_Support_Request__c ssr : newMap.values())
      {
          if(String.isNotEmpty(ssr.Status__c) && ssr.Status__c.equals('Evaluation Complete') && ssr.Future_Project__c == null)
          {
              Project__c prj = new Project__c();
              prj.Opportunity__c = ssr.Opportunity__c;
              prj.Type__c = ssr.Type__c;
              prj.Project_End_Date__c = ssr.Due_Date__c;
              prj.Business_Unit__c = ssr.Business_Unit__c;
              prj.Order__c = ssr.Order__c;
              prj.Name = 'Project created from SSR : ' + ssr.Name;
              prj.Account__c = ssr.Account_Id__c;
              //prj.Sales_Contact__c = ssr.Sales_Contact__c;
              prj.Client_Contact__c = ssr.Client_Contact__c;
              prj.Status__c = 'Unassigned';
              prj.Start_Date__c = Date.today();     
              prj.Sales_Support_Request__c = ssr.Id;
              prj.RecordTypeId = Record_Type_Ids__c.getInstance().Project_Serasa_Project__c;            
              newProjects.put(ssr.Id, prj);
          }
      }
      
      if(newProjects.size() > 0)
      {                  
          List<Project__c> listNewProjects = new List<Project__c>(newProjects.values());
          INSERT listNewProjects;
                
          for(Id ssrId : newProjects.keySet())
          {
              Sales_Support_Request__c ssr = newMap.get(ssrId);
              ssr.Future_Project__c = newProjects.get(ssrId).Id;              
          }
      }
  }     
}