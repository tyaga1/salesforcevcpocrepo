/**=====================================================================
 * Appirio, Inc
 * Name: CheckExistingAccount
 * Description: Webservice to check Existing Account/Contact/Address__c 
 *              as per parameter and if not in system, it creates new and
 *              returns values as per task T-413779
 * Created Date: June 25th, 2015
 * Created By: Naresh Kr Ojha (Appirio)
 *
 * Date Modified      Modified By                  Description of the update
 * Aug 17th           Terri Jiles                  broke into smaller methods
 * Aug 22nd           Naresh Kr Ojha               Implemented various scenarios
 * Aug 22nd           Terri Jiles                  Modified Scenarios for matching an account
 * Aug 23rd           Terri Jiles                  Modified Address scenarios
 * Aug 26th           Terri Jiles                  I-177367: Commented out SFDCContactId from request, no matches done by Contact Id because SaaS Platform will not store Contact Id
 * Aug 28th           Terri Jiles                  I-177957: Added checks for postalcode and province
 * Aug 28th           Terri Jiles                  I-177362: Add validations
 * Sep 1st            Terri Jiles                  I-178293: Added Contact Id to response
 * Sep 2nd            Terri Jiles                  I-179007: Fixed Null pointer exception error and corrected matchOrCreateAddress method
 * Sep 4th            Terri Jiles                  Modified matchByAccountNameAndAddress to query address fields instead of account address fields and added comments
 * Sep 23rd           Naresh Kr Ojha               As per issue : I-181553, default account is SaaS.
 * Jun 24th, 2016     Manoj Gopu                   Case #01947180 - Remove EDQ specific Contact object fields - MG COMMENTED CODE OUT
/**=====================================================================*/

global class CheckExistingAccount {
  //Main method to process request and returns response
  WebService static WSResponse MatchingCreatingAccountsContactsWS(WSAccount ReqAccount, 
                                                                  WSContact ReqContact, 
                                                                  WSAddress ReqAddress) {
    

    ResponseAccount resAccount = new ResponseAccount(); //[2015-09-01 TTJ changed from WSAccount to ResponseAccount]
    ResponseContact resContact = new ResponseContact(); //[2015-09-01 TTJ changed from WSContact to ResponseContact]
    ResponseAddress resAddress = new ResponseAddress();
    
    //account related variables
    Account accountObj = new Account();
    Boolean accountIsMatched = true;
    String actionAccount = null;
    String matchedByAccount = null;

    //contact related variables
    Contact contactObj = new Contact();
    String actionContact = null;

    //address related variables
    Address__c addressObj = new Address__c();
    String actionAddress = null;
    Boolean findAddress = true;

    //account address related variables
    Account_Address__c accountAddressObj = new Account_Address__c();
    String actionAccountAddress = Constants.ACC_WS_ACCOUNT_NO_ADDRESS;
    //Boolean hasAccountRegisteredAddress = false;
    //Account_Address__c registeredAccountAddressObj = new Account_Address__c();

    //contact address related variables
    Contact_Address__c contactAddressObj = new Contact_Address__c();
    //String actionContactAddress = Constants.ACC_WS_ACCOUNT_NO_ADDRESS ;
    Boolean needToFindContactAddress = true;
    //Boolean hasContactRegisteredAddress = false;
    //Contact_Address__c registeredContactAddressObj = new Contact_Address__c();

    Map<Boolean, String> validationResultMap = validateRequest(ReqAccount, ReqContact, ReqAddress);
    Boolean hasPassValidation = true;
    ResponseValidationError  validateErrorMsg = new ResponseValidationError();

    if (!validationResultMap.containsKey(hasPassValidation)) {
      hasPassValidation = false;
      validateErrorMsg.Error = validationResultMap.get(hasPassValidation);
    }


    if (hasPassValidation) {
      
      //Attempt to match on accountId
      Map<Boolean, Account> accountMatchByIdMap = matchAccountById(ReqAccount);
      if (!accountMatchByIdMap.isEmpty() && accountMatchByIdMap.containsKey(accountIsMatched) ) {
        accountObj = accountMatchByIdMap.get(accountIsMatched);
        actionAccount = Constants.ACC_WS_ACTION_MATCHED;
        matchedByAccount = Constants.ACC_WS_ACCOUNT_MATCHED_BY_ID;
        System.debug('~~~matchedByAccount~~~ '+ matchedByAccount + ' ' + accountObj.Id);
      } else {

        //Attempt to match on account name and address
        Map<Boolean, Account_Address__c> accountMatchByNameAddressMap = matchAccountByNameAddress(ReqAccount, ReqAddress);
        if (!accountMatchByNameAddressMap.isEmpty() && accountMatchByNameAddressMap.containsKey(accountIsMatched) ) {
          accountObj.Id = accountMatchByNameAddressMap.get(accountIsMatched).Account__c;
          accountObj.Name = accountMatchByNameAddressMap.get(accountIsMatched).Account__r.Name;
          actionAccount = Constants.ACC_WS_ACTION_MATCHED;
          matchedByAccount = Constants.ACC_WS_ACCOUNT_MATCHED_BY_NAME_AND_ADDRESS;

          addressObj.Id = accountMatchByNameAddressMap.get(accountIsMatched).Address__c;
          actionAddress = Constants.ACC_WS_ACTION_MATCHED;

          accountAddressObj.Id = accountMatchByNameAddressMap.get(accountIsMatched).Id;
          actionAccountAddress = Constants.ACC_WS_ACTION_MATCHED;
          findAddress = false;
          System.debug('~~~matchedByAccount~~~ '+ matchedByAccount + ' ' + accountObj.Id);
        } else {
          accountIsMatched = false;
        }
      }

      //Attempt to create account
      if (accountIsMatched == false) {
        Map<String, Account> accountCreateMap = createAccount(ReqAccount);
        if (!accountCreateMap.isEmpty() && accountCreateMap.containsKey(Constants.ACC_WS_ACTION_CREATED)) {
          //Create Account
          accountObj = accountCreateMap.get(Constants.ACC_WS_ACTION_CREATED);
          actionAccount = Constants.ACC_WS_ACTION_CREATED;
          System.debug('~~~created Account~~~ ' + accountObj.Id);

        } else if (!accountCreateMap.isEmpty()) {
          //Error - Unable to create new account
          actionAccount = (new List<String>(accountCreateMap.keySet()))[0];
          findAddress = false;
          System.debug('~~~Unable to Create Account~~~ ' + actionAccount);
        }
      }

    } else {
      //Error - No Account provided in address
      actionAccount = Constants.ACC_WS_ACCOUNT_ERROR + ' ' + Constants.ACC_WS_ACCOUNT_NO_ACCOUNT;
      findAddress = false;
      System.debug('~~~Account Not Provided~~~');
    }

    //Match or create Contact
    if (hasPassValidation) {
      if (ReqContact != null) {
        Map<String, Contact> contactMap = matchOrCreateContact(ReqContact, accountObj.ID);
        System.debug('**contactMap***'+contactMap);
 
        if (contactMap != null && !contactMap.isEmpty()) {
          if(contactMap.containsKey(Constants.ACC_WS_ACTION_MATCHED)) {
            actionContact = Constants.ACC_WS_ACTION_MATCHED;
            ContactObj = contactMap.get(Constants.ACC_WS_ACTION_MATCHED);
          } else if (contactMap.containsKey(Constants.ACC_WS_ACTION_CREATED)) {
            actionContact = Constants.ACC_WS_ACTION_CREATED;
            ContactObj = contactMap.get(Constants.ACC_WS_ACTION_CREATED);
          } else {
            actionContact = (new List<String>(contactMap.keySet()))[0];
          }
        }
      } else {
        actionContact = Constants.ACC_WS_ACCOUNT_NO_CONTACT;
      }
    }


    //create account response
    resAccount = setAccountResponse(ReqAccount, actionAccount, matchedByAccount, accountObj);
    //Create contact response
    resContact = setContactResponse(contactObj, actionContact, ReqContact);

    resAddress = setAddressResponse(findAddress, ReqAddress, addressObj, actionAddress,
      needToFindContactAddress, contactObj, accountObj, actionAccount,
      actionAccountAddress, accountAddressObj);

    WSResponse newResponse = new WSResponse(resAccount, resContact, resAddress, validateErrorMsg);
    return newResponse;
  } 
  
 
  //matches account by Id
  private static Map<Boolean, Account> matchAccountById (WSAccount accountReq) {
      Map<Boolean, Account> resultMap = new Map<Boolean, Account>();
      Boolean isAccountMatched = false;
      Account acc = new Account();

      //match account
      if (accountReq != null && !String.isBlank(accountReq.SFDCAccountId)) {
        List<Account>accLst = [SELECT ID, Name, Industry, Sector__c, Region__c, CurrencyISOCode, Company_Registration__c,
                        (SELECT Id, Address__c, Address_Type__c 
                          FROM Account_Address__r) 
                       FROM Account 
                       WHERE Id =: accountReq.SFDCAccountId
                       LIMIT 1]; 
        if (accLst.size()>0) {
          acc = accLst[0];
          isAccountMatched = true;
        }
      } 

      resultMap.put(isAccountMatched, acc); 
      return resultMap;
  }

  //matchAccountByNameAddress attempts to match an account address record by 
  //- Account Name and 
  //- All of the Address fields from the request
  private static Map<Boolean, Account_Address__c> matchAccountByNameAddress (WSAccount accountReq, WSAddress addressReq) {
    Map<Boolean, Account_Address__c> resultMap = new Map<Boolean, Account_Address__c>();
    Boolean isAccountMatched = false;
    Account_Address__c acc = new Account_Address__c();

    if (accountReq != null && addressReq != null) {
      List<Account_Address__c> accLst = [SELECT ID, Account__c, Address__c, Account__r.Name, Address_Type__c
             FROM Account_Address__c
             WHERE Account__r.Name =: accountReq.Name
             AND Address__r.Address_1__c =: addressReq.Address1
             AND Address__r.Address_2__c =: addressReq.Address2
             AND Address__r.City__c =: addressReq.City
             AND (Address__r.State__c =: addressReq.State OR Address__r.Province__c =: addressReq.State)
             AND (Address__r.Postcode__c =: addressReq.PostalCode OR Address__r.Zip__c =: addressReq.PostalCode)
             AND Address__r.Country__c =: addressReq.Country
             LIMIT 1];
      if (accLst.size() > 0) {
        isAccountMatched = true;
        acc = accLst[0];
      }
    }

    resultMap.put(isAccountMatched, acc);
    return resultMap;

  }

  //createAccount creates a new Account
  private static Map<String, Account> createAccount(WSAccount accountReq) {
    Map<String, Account> resultMap = new Map<String, Account>();

    Account newAccount = new Account();
    if (accountReq != null) {
      newAccount = new Account(Name = accountReq.Name);
      newAccount.Industry = accountReq.Industry;
      newAccount.Sector__c = accountReq.Sector;
      newAccount.Region__c = accountReq.Region;
      newAccount.SaaS__c = true; //As per issue : I-181553
      newAccount.SaaS_Timestamp__c = DateTime.now();
      Database.SaveResult sr = Database.insert(newAccount, false);
      System.debug('~~~sr.isSuccess()~~~'+sr.isSuccess());
      if (!sr.isSuccess()) {
        //error 
        Database.DMLOptions dml = new Database.DMLOptions(); 
        dml.DuplicateRuleHeader.AllowSave = true;
        Database.SaveResult sr2 = Database.insert(newAccount, dml);
        System.debug('~~~DupInsert~~~'+sr2.getErrors());
        resultMap.put(Constants.ACC_WS_ACCOUNT_ERROR + ' ' + sr2.getErrors(), null);
      } else {
        //create account
        resultMap.put(Constants.ACC_WS_ACTION_CREATED, newAccount);
        System.debug('~~~newAccount~~~'+newAccount);
      }
    }
    else {
      resultMap.put(Constants.ACC_WS_ACCOUNT_ERROR + ' ' + Constants.ACC_WS_ACCOUNT_NO_ACCOUNT, newAccount);
    }
    return resultMap;
  }

    //matches contact from request or creates a new contact
    private static Map<String, Contact> matchOrCreateContact(WSContact contactReq, Id sfdcAccountId) {
      Map<String, Contact> resultMap = new Map<String, Contact>();
      Boolean isContactMatched = false;
      Contact cont = new Contact();

      if (contactReq != null && !String.isBlank(contactReq.Email)) {
        List<Contact> contLst = [SELECT ID, AccountId, FirstName, LastName, Phone, Email,
                          (SELECT Id, Address_Type__c, Address__c 
                            FROM Contact_Addresses__r )
                         FROM Contact 
                         WHERE AccountId =: sfdcAccountId
                          AND Email =: contactReq.Email
                         LIMIT 1]; 
        if (contLst.size() > 0) {
          isContactMatched = true;
          cont = contLst[0];
            
          resultMap.put(Constants.ACC_WS_ACTION_MATCHED, cont);
        }
      }
      System.debug('==resultMap==='+resultMap); 
      if (isContactMatched == false ) {
          Contact newContact = new Contact(FirstName = contactReq.FirstName, LastName = contactReq.LastName);
          newContact.Phone = contactReq.Phone;
          newContact.Email = contactReq.Email;
          newContact.AccountId = sfdcAccountId;
          //newContact.SaaS__c = true;
          //insert newContact;
          Database.SaveResult sr = Database.insert(newContact, false);
          if (!sr.isSuccess()) {
            Database.DMLOptions dml = new Database.DMLOptions(); 
            dml.DuplicateRuleHeader.AllowSave = true;
            Database.SaveResult sr2 = Database.insert(newContact, dml);
            resultMap.put(Constants.ACC_WS_ACCOUNT_ERROR + ' ' + sr2.getErrors(), null);
          } else {
            System.debug('~~~~newContact~~~'+newContact);
            resultMap.put(Constants.ACC_WS_ACTION_CREATED, newContact);
          }

        }   
        System.debug('~~~isContactMatched~~~'+isContactMatched);
        return resultMap;   
      }
  
    //matches address record provided in the request or creates a new address record
    private static Map<String, Address__c> matchOrCreateAddress(WSAddress addressReq) {
      Map<String, Address__c> resultMap = new Map<String, Address__c>();
      Boolean isAddressMatched = false;
      Address__c addressRec = new Address__c();
      System.debug('~~~ matchOrCreateAddress: inside matchORCreateAddress');
      
      if (addressReq != null 
          && !String.isBlank(addressReq.Address1) 
          //&& !String.isBlank(addressReq.City) 
          //&& !String.isBlank(addressReq.State)
          && !String.isBlank(addressReq.Country)
          //&& !String.isBlank(addressReq.PostalCode)
          ) {

        System.debug('~~~ matchOrCreateAddress: inside addressReq != null');
        
        //2015-08-28 TTJ added checks for Province and Postcode
        List<Address__c> addressRecLst = [SELECT Id, Address_1__c, Address_2__c, City__c, State__c, Zip__c, Country__c
                               FROM Address__c 
                               WHERE Address_1__c =:addressReq.Address1
                                AND Address_2__c =: addressReq.Address2
                                AND City__c =: addressReq.City
                                AND (State__c =: addressReq.State OR Province__c =:addressReq.State)
                                AND (Zip__c =: addressReq.PostalCode OR Postcode__c =: addressReq.PostalCode)
                                AND Country__c =: addressReq.Country
                               LIMIT 1];
        if (addressRecLst.size() > 0) {
          System.debug('~~~ matchOrCreateAddress: inside addressRecList.size() > 0');
          isAddressMatched = true;
          addressRec = addressRecLst[0];  
        }
         
        //matched
        if (isAddressMatched == true) {
          resultmap.put(Constants.ACC_WS_ACTION_MATCHED, addressRec);
          System.debug('~~~ matchOrCreateAddress: inside isAddressMatched == true');
        } else {
          System.debug('~~~ matchORCreateAddress: inside else of isAddressMatched == true, where new address is being created');
          Address__c newAddress = new Address__c();
          newAddress.Address_1__c = addressReq.Address1;
          newAddress.Address_2__c = addressReq.Address2;
          newAddress.City__c = addressReq.City;
          newAddress.State__c = addressReq.State; //2015-08-28 TTJ added
          newAddress.Province__c = addressReq.State;
          newAddress.Zip__c = addressReq.PostalCode;
          newAddress.Postcode__c = addressReq.PostalCode; //2015-08-28 TTJ added
          newAddress.Country__c = addressReq.Country;
       
          Database.SaveResult sr = Database.insert(newAddress, false);
          System.debug('~~~sr.isSuccess()~~~'+sr.isSuccess());
          
          //error
          if (!sr.isSuccess()) {
            System.debug('~~~ matchOrCreateAddress: inside !sr.isSuccess');
            Database.DMLOptions dml = new Database.DMLOptions(); 
            dml.DuplicateRuleHeader.AllowSave = true;
            Database.SaveResult sr2 = Database.insert(newAddress, dml);
            System.debug('~~~DupInsert~~~'+sr2.getErrors());
            resultMap.put(Constants.ACC_WS_ACCOUNT_ERROR + ' ' + sr2.getErrors(), null);
          } else {
          //created
            System.debug('~~~ matchOrCreateAddress: Inside else of !sr.isScuccess, created address record successfully!!');
            resultMap.put(Constants.ACC_WS_ACTION_CREATED, newAddress);
          }
        }
      } else {
        //no valid address provided
        resultMap.put(Constants.ACC_WS_ACCOUNT_NO_ADDRESS, null);
      }

      return resultMap;
    }

  //validateRequest validates the request has the minimal amount of information required to match records:
  //Scenarios considered valid are
  //1. Has SFDCAccountId and Email
  //2. Has Account Name, Address 1, Country, and Email
  // if the above conditions are met, request is considered valid, else request is considered invalid
  private static Map<Boolean, String> validateRequest(WSAccount accountReq, WSContact contactReq,
    WSAddress addressReq) {

    Boolean hasEmail = false;
    Boolean hasAddress = false;
    Boolean hasAccId = false;
    Boolean hasAccName = false;
    Map<Boolean, String> resultMap = new Map<Boolean, String>();

    //validate contact info
    if (contactReq != null && !String.isBlank(contactReq.Email)) {
      String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; // source: <a href="http://www.regular-expressions.info/email.html" target="_blank" rel="nofollow">http://www.regular-expressions.info/email.html</a>
      Pattern emailPattern = Pattern.compile(emailRegex);
      Matcher emailMatcher = emailPattern.matcher(contactReq.Email);

      if (emailMatcher.matches()) {
        hasEmail = true;
      }
    }

    //validate account info
    if (accountReq != null) {
      if(!String.isBlank(accountReq.SFDCAccountID)) {
        hasAccId = true;
      }

      if(!String.isBlank(accountReq.Name)) {
        hasAccName = true;
      }
    }

    //validate address info
    if (addressReq != null) {
      if (!String.isBlank(addressReq.Country) && !String.isBlank(addressReq.Address1)) {
        hasAddress = true;
      }
    }

    if (hasEmail == true && hasAccId == true) {
      resultMap.put(true, null);
    } else if (hasEmail && hasAccName && hasAddress) {
      resultMap.put(true, null);
    } else {
      resultMap.put(false, Constants.ACC_WS_ACTION_MISSING_MINIMAL_INFO);
    }

    return resultMap;
  }

  //setAccountResponse constructs output account response
  private static ResponseAccount setAccountResponse(WSAccount ReqAccount, String actionAccount, 
    String matchedByAccount, Account accountObj) {
    ResponseAccount resAccount = new ResponseAccount(); //[2015-09-01 TTJ changed to use ResponseAccount instead of WSAccount]

    //create account response
    resAccount.SaaSAccountID = (ReqAccount != null && !String.IsBlank(ReqAccount.SaaSAccountID) ? ReqAccount.SaaSAccountID : null);
    resAccount.Action = actionAccount;
    resAccount.MatchedBy = matchedByAccount;
    if (accountObj != null && !String.isBlank(accountObj.Id)) { 
      resAccount.Name = (!String.isBlank(accountObj.Name) ? accountObj.Name : null);
      resAccount.SFDCAccountID = accountObj.Id;
    } else {
      resAccount.Name = (ReqAccount != null && !String.isBlank(ReqAccount.Name) ? ReqAccount.Name : null);
    }

    return resAccount;
  }

  //setContactResponse constructs output contact response
  private static ResponseContact setContactResponse(Contact contactObj, String actionContact, WSContact ReqContact) {
    ResponseContact resContact = new ResponseContact(); //[2015-09-01 TTJ changed to use ResponseContact instead of WSContact ]
    
    //Create contact response
    resContact.Action = actionContact; 
    if (contactObj != null && !String.isBlank(contactObj.Id)) {
      resContact.SFDCContactID = contactObj.ID;
      resContact.Email = (!String.isBlank(contactObj.Email) ? contactObj.Email : null);
    } else {
      resContact.Email = (ReqContact != null && !String.isBlank(ReqContact.Email) ? ReqContact.Email : null);      
    }

    return resContact;
  }

  //setAddressResponse does the following
  // - Finds an matching or creates an address, if account wasn't matched by Name and Address
  // --- Checks if the address is associated with the account
  // --- If it is, determines if the address is a secondary or registered address
  // - Creates a new account address, if the address is not already associated with the account
  // - Creates or matches an existing contact address
  // - Constructs address response
  private static ResponseAddress setAddressResponse(Boolean findAddress, 
    WSAddress ReqAddress, Address__c addressObj, String actionAddress, 
    Boolean needToFindContactAddress, Contact contactObj, Account accountObj, String actionAccount,
    String actionAccountAddress, Account_Address__c accountAddressObj) {
    System.debug('~~~ setAddressResponse: inside setAddressResponse');
    
    ResponseAddress resAddress = new ResponseAddress();
        
    //account address related variables
    //Account_Address__c accountAddressObj = new Account_Address__c();
    //String actionAccountAddress = Constants.ACC_WS_ACCOUNT_NO_ADDRESS;
    Boolean hasAccountRegisteredAddress = false;
    Account_Address__c registeredAccountAddressObj = new Account_Address__c();

    //contact address related variables
    Contact_Address__c contactAddressObj = new Contact_Address__c();
    String actionContactAddress = Constants.ACC_WS_ACCOUNT_NO_ADDRESS ;
    //Boolean needToFindContactAddress = true;
    Boolean hasContactRegisteredAddress = false;
    Contact_Address__c registeredContactAddressObj = new Contact_Address__c();

    if (findAddress == true) {
      //Match Or Create address record
      System.debug('~~~ setAddressResponse: inside findAddress = true');
      Map<String, Address__c> addressMap = matchOrCreateAddress(ReqAddress);
      if (addressMap != null && !addressMap.isEmpty()) {
        System.debug('~~~ setAddressResponse: inside if addressMap != null && !addressMap.isEmpty');

        actionAddress = (new List<String> (addressMap.keySet()))[0];
        addressObj = addressMap.get(actionAddress);
      
        //Find an existing matching account address or an existing registered address
        if (accountObj.Account_Address__r != null && accountObj.Account_Address__r.size() > 0) {
          System.debug('~~~ setAddressResponse: inside accountObj.Account_Address__r != null && accountObj.Account_Address__r.size() > 0');
          for (Account_Address__c accAdd : accountObj.Account_Address__r) {
            if (addressObj != null && !String.isBlank(addressObj.Id) 
                && accAdd.Address__c == addressObj.Id) {
              actionAccountAddress = Constants.ACC_WS_ACTION_MATCHED;
              accountAddressObj = accAdd;
              System.debug('~~~Matched Account Address~~~ ' + accAdd.Id);
              break;
            }
            if (accAdd.Address_Type__c == Constants.ACC_WS_ACCOUNT_ADDRESS_TYPE_REG) {
              hasAccountRegisteredAddress = true;
              registeredAccountAddressObj = accAdd;
              System.debug('~~~hasAccountRegisteredAddress account address id~~~ ' + accAdd.Id );
              System.debug('~~~hasAccountRegisteredAddress address id~~~ ' + accAdd.Address__c);
              System.debug('~~~registeredAccountAddressObj~~~ ' + registeredAccountAddressObj);
            }
          }
        } 

        //Create new Account Address
        if (addressObj != null && !String.isBlank(addressObj.Id) && actionAccountAddress != Constants.ACC_WS_ACTION_MATCHED) {
          System.debug('~~~ setAddressResponse: inside addressObj != null && !String.isBlank(addressObj.Id) && actionAccountAddress != Constants.ACC_WS_ACTION_MATCHED');
          Account_Address__c accAddress = new Account_Address__c();
          accAddress.Account__c = accountObj.Id;
          accAddress.Address__c = addressObj.Id;
          accAddress.Address_Type__c = (hasAccountRegisteredAddress == true ? Constants.ACC_WS_ACCOUNT_ADDRESS_TYPE_SEC : Constants.ACC_WS_ACCOUNT_ADDRESS_TYPE_REG);

          Database.SaveResult sr = Database.insert(accAddress, false);
          if (!sr.isSuccess()) {
            System.debug('~~~ setAddressResponse: inside !sr.Success');
            Database.DMLOptions dml = new Database.DMLOptions(); 
            dml.DuplicateRuleHeader.AllowSave = true;
            Database.SaveResult sr2 = Database.insert(accAddress, dml);
            actionAccountAddress = Constants.ACC_WS_ACCOUNT_ERROR + ' ' + sr2.getErrors();
            accountAddressObj = accAddress;
          } else {
            System.debug('~~~~accAddress~~~'+accAddress);
            actionAccountAddress = Constants.ACC_WS_ACTION_CREATED;
            accountAddressObj = accAddress;
          }
        }

        //Since no address was provided for matching, 
        //Default Address using the matched account's registered address
        if (actionAddress == Constants.ACC_WS_ACCOUNT_NO_ADDRESS 
            && hasAccountRegisteredAddress == true
            && actionAccount == Constants.ACC_WS_ACTION_MATCHED) {
          //default address
          addressObj = (New Address__c(Id=registeredAccountAddressObj.Address__c));
          actionAddress = Constants.ACC_WS_ACTION_DEFAULT_ADDRESS;

          //default account address
          accountAddressObj = registeredAccountAddressObj;
          actionAccountAddress = Constants.ACC_WS_ACTION_DEFAULT_ADDRESS;
          needToFindContactAddress = false;
        } 
      }
    }

    //Create Address response
    resAddress.ActionAddress = actionAddress;
    //resAddress.SFDCAddressID = accountAddressObj.Id;
    resAddress.SFDCAddressID = (addressObj != null && !String.isBlank(addressObj.Id) ? addressObj.Id : null); //

    //Crease Account Address response
    resAddress.ActionAccountAddress = actionAccountAddress;
    resAddress.SFDCAccountAddressID = (accountAddressObj != null && !String.isBlank(accountAddressObj.Id) ? accountAddressObj.Id : null);


    //create or match contact address
    if (needToFindContactAddress == true) {
      System.debug('~~~ Create or Match Contact Address ~~~');  
      if (addressObj != null && !String.isBlank(addressObj.Id) ) {
        System.debug('~~~ AddressObj.Id ~~~' + addressObj.Id);
        if (contactObj.Contact_Addresses__r != null && contactObj.Contact_Addresses__r.size() > 0) {
      System.debug('~~~ Check existing contact address records size ~~~~ ' + contactObj.Contact_Addresses__r.size());
            
          for (Contact_Address__c conAdd : contactObj.Contact_Addresses__r) {
            System.debug('~~~ conAdd ~~~ ' + conAdd);
            if (conAdd.Address__c == addressObj.Id) {
              System.debug('~~~ found match ~~~');  
              actionContactAddress = Constants.ACC_WS_ACTION_MATCHED;  
              contactAddressObj = conAdd;
              break;
            }
            if (conAdd.Address_Type__c == Constants.ACC_WS_ACCOUNT_ADDRESS_TYPE_REG) {
              System.debug('~~~ Contact has existing registered address ~~~');
              hasContactRegisteredAddress = true;
            }
          }
        }

        //Create Contact address    
        if (actionContactAddress != Constants.ACC_WS_ACTION_MATCHED) {
          System.debug('~~~ creating contact address ~~~~');  
          Contact_Address__c newConAdd = new Contact_Address__c();
          newConAdd.Contact__c = ContactObj.Id;
          newConAdd.Address__c = addressObj.Id;
          newConAdd.Address_Type__c = (hasContactRegisteredAddress == true ? Constants.ACC_WS_ACCOUNT_ADDRESS_TYPE_SEC : Constants.ACC_WS_ACCOUNT_ADDRESS_TYPE_REG);
          
          Database.SaveResult sr = Database.insert(newConAdd, false);
          if (!sr.isSuccess()) {
            System.debug('~~~ unable to create contact address ~~~');  
            Database.DMLOptions dml = new Database.DMLOptions(); 
            dml.DuplicateRuleHeader.AllowSave = true;
            Database.SaveResult sr2 = Database.insert(newConAdd, dml);
            System.debug('~~~DupInsert~~~'+sr2.getErrors());
            actionContactAddress = Constants.ACC_WS_ACCOUNT_ERROR + ' ' + sr2.getErrors();
            contactAddressObj = newConAdd;
          } else {
          //created
            System.debug('~~~ able to create contact address~~~');
            actionContactAddress = Constants.ACC_WS_ACTION_CREATED;
            contactAddressObj = newConAdd;
          }
        }

      } else {
        actionContactAddress = Constants.ACC_WS_ACCOUNT_NO_ADDRESS;
      }

    }
    resAddress.ActionContactAddress = actionContactAddress;
    resAddress.SFDCContactAddressID = (contactAddressObj != null && !String.isBlank(contactAddressObj.Id) ? contactAddressObj.Id : null);

    return resAddress;

  }
  
  //Account as webservice request parameter
  global class WSAccount {
    WebService String SFDCAccountID;
    WebService String SaaSAccountID;
    WebService String Name;
    WebService String Industry;
    WebService String Sector;
    WebService String Region;
    WebService String AccountCurrency;
    WebService String CompanyRegNumber;
    String Action;
    String MatchedBy;
  }
  //Contact as webservice request parameter
  global class WSContact {
    //WebService String SFDCContactID; [2015-08-26 TTJ commented out I-177367]
    WebService String FirstName;
    WebService String LastName;
    WebService String Phone;
    WebService String Email;
    String Action;
  }

  //Address as webservice request parameter
  global class WSAddress {
    //WebService String SFDCAddressID;
    WebService String Address1;
    WebService String Address2;
    WebService String City;
    WebService String State;
    WebService String PostalCode;
    WebService String Country;
    String Action;
  }
  
  //Account as webservice response
  global class ResponseAccount {
    WebService String SFDCAccountID;
    WebService String SaaSAccountID;
    WebService String Name;
    //WebService String AccountCurrency; //2015-09-01 TTJ removed
    WebService String Action;
    WebService String MatchedBy; //2015-08-22 TTJ added
  }

  //Contact as webservice response
  global class ResponseContact {
    WebService String SFDCContactID;
    WebService String Email;
    WebService String Action;
  }
  
  //Address as webservice response
  global class ResponseAddress {
    WebService String SFDCAddressID;
    WebService String SFDCAccountAddressID;
    WebService String SFDCContactAddressID;
    WebService String ActionAddress; //[2015-08-17 TTJ added]
    WebService String ActionAccountAddress; //[2015-08-17 TTJ added]
    WebService String ActionContactAddress; //[2015-08-17 TTJ added]

    //WebService String Error;
  }
  
  //Response validation error as webserivce response  
  global class ResponseValidationError {
    WebService String Error; //[2015=08-28 TTJ added]
  }  

  //Response wrapper class
  global class WSResponse {
    WebService ResponseAccount RES_Account;
    WebService ResponseContact RES_Contact;
    WebService ResponseAddress RES_Address;
    WebService ResponseValidationError RES_ValidationError; //[2015=08-28 TTJ added]
    
    //Constructor
    WSResponse(ResponseAccount acc, ResponseContact cont, ResponseAddress add, ResponseValidationError err) { //[2015-09-01 TTJ changed to use the 'Response' objects instead of the 'WS' objects]
      //Creating response objects
      RES_Account = new ResponseAccount();
      RES_Account.SFDCAccountID = acc.SFDCAccountId;
      RES_Account.SaaSAccountID = acc.SaaSAccountID;
      RES_Account.Name = acc.Name;
      //RES_Account.AccountCurrency = acc.AccountCurrency;
      RES_Account.Action = acc.Action;
      RES_Account.MatchedBy = acc.MatchedBy;

      RES_Contact = new ResponseContact();
      RES_Contact.SFDCContactID = cont.SFDCContactID;
      RES_Contact.Email = cont.Email;
      RES_Contact.Action = cont.Action;

      RES_Address = new ResponseAddress();
      RES_Address = add;

      RES_ValidationError = new ResponseValidationError(); //[2015=08-28 TTJ added]
      RES_ValidationError = err;
    }
  }
}