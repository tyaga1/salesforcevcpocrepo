@isTest
private class StratComm_NewArticleTopicEmail_Test {
    
    @isTest static void test_method_one() {
        Account testAccount1 = Test_Utils.createAccount();
        insert testAccount1;

        Contact testContact = Test_Utils.createContact(testAccount1.id);
        testContact.FirstName='Experian';
        testContact.LastName='Community Contact';
        insert testContact;

        Employee_Article__kav testArticle = new Employee_Article__kav();
        testArticle.Title = 'test';
        testArticle.UrlName = 'test';
        insert testArticle;

        StratComm_NewArticleTopicEmail ctrller = new StratComm_NewArticleTopicEmail();
        StratComm_NewArticleTopicEmail.sendEmail(testArticle.id, 'test', 'test');

    }
    
    
}