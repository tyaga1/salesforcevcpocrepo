/******************************************************************************
 * Name: expcommunity_FAQ_ctrl.cls
 * Created Date: 1/26/2017
 * Created By: Hay Win
 * Description : Controller for expcommunity_FAQ_cmpt for new Exp Community
 *               to create Platform support or new user cases
 * Change Log- 
 ****************************************************************************/
 
public with sharing class expcommunity_FAQ_ctrl{ 
   
    public List<Employee_Community_News__c> faq_cat{get;set;}
    public List<Employee_Community_News__c> faq{get;set;}
    public Map<String,List<Employee_Community_News__c>> catMap {get;set;}
    public Set<String> catName {get;set;}
    public String appName {get;set;}
    
    public expcommunity_FAQ_ctrl() {
        faq_cat = new List<Employee_Community_News__c>();
        faq  = new List<Employee_Community_News__c>();   
        catName = new Set<String>();     
    }
   
    
   public boolean getsearchFAQ() {
       String appQuery = '';
        if (appName != null && appName != '') {
            appQuery = ' AND Application_Name__c =: appName ';
        } else {
            appQuery = ' AND Application_Name__c = \'EITSHome\' ';
        }
        
        String queryStr = 'Select ID, Detail__c, Headline__c, Application_Name__c , FAQ_Category__c, FAQ_Category_Order__c from Employee_Community_News__c where Content_Type__c = \'FAQ\'' + appQuery +' ORDER BY FAQ_Category_Order__c asc, FAQ_Detail_Order__c asc';
        faq_cat = Database.query(queryStr);
        //faq_cat= [Select ID, Detail__c, Headline__c, FAQ_Category__c, FAQ_Category_Order__c from Employee_Community_News__c where Content_Type__c = 'FAQ' ORDER BY FAQ_Category_Order__c asc, FAQ_Detail_Order__c asc];
        
        catMap = new Map<String,List<Employee_Community_News__c>>();
        for (Employee_Community_News__c ques: faq_cat) {        
        
            if(!catMap.containsKey(ques.FAQ_Category__c)) {
            catName.add(ques.FAQ_Category__c);
                catMap.put(ques.FAQ_Category__c, new List<Employee_Community_News__c>());
            }
            
            catMap.get(ques.FAQ_Category__c).add(ques);
        }
        
        return true;
   }
    
    
}