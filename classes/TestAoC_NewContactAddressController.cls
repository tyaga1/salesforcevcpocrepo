/**=====================================================================
 * Appirio Inc
 * Name: TestAoC_NewContactAddressController.cls
 * Description: 
 * Created Date: Nov 20, 2013
 * Created By: Mohammed Irfan (Appirio)
 *
 * Date Modified         Modified By          Description of the update
 * January 28th, 2014    Nathalie Le Guay     Adding duplicate check tests
 * Mar 04th, 2014        Arpita Bose(Appirio) T-243282: Added Constants in place of String
 * Mar 20th, 2014        Arpita Bose(Appirio) T-253906: Fixed test class
 * Aprl 22nd, 2014       Aditi Bhardwaj       Modified myUnitTest method to increase coverage 
 * May 1st, 2014         Arpita Bose          Updated myUnitTest method to increase coverage 
 * Sep 11th, 2014        Naresh kr Ojha       Test class fix for issue: I-130073
 * Aug 4th, 2015         Paul Kissick         Case #1084152: Duplicate Management / Reformatting
 * Apr 7th, 2016         Paul Kissick         Case 01932085: Fixing Test User Email Domain
 * Apr 15th,2016         Sadar Yacob          State and Country Picklist implementation use valid values
 * Dec 6th ,2016         Sadar Yacob          Remove QAS v4 code components  
 * Mar 15th, 2017        Ryan (Weijie) Hu     Add support for testing creating consumer contact
 * Jun 06th, 2017        Manoj Gopu           Fixed test class Failure Inserted Custom Setting
 =====================================================================*/
@isTest
private class TestAoC_NewContactAddressController {

  static testMethod void myUnitTest() {
    
    Contact con = [SELECT Id,Name,FirstName,LastName FROM Contact WHERE FirstName = '00TestContact0' AND LastName = 'Sweden' LIMIT 1];
    Account acc = [SELECT Id, Name FROM Account WHERE Name = '00TestAccount0' LIMIT 1];
    Address__c addr = new Address__c(Address_1__c='Test Addr1', Address_2__c='Test Addr2',Address_3__c='Test Addr3',
                                        State__c='Texas',City__c='test city',zip__c='test zip',Country__c='United States of America');
    insert addr;
       
    Contact_Address__c conAddrRel = new Contact_Address__c(Contact__c=con.id,Address__c=addr.id);
    insert conAddrRel;
    
    ApexPages.StandardController sc = new ApexPages.StandardController(con);
       
    // call constructor without passing conId
    ApexPages.currentPage().getParameters().put('conFName', 'Test FName');
    ApexPages.currentPage().getParameters().put('conLName', 'Test LName');
    ApexPages.currentPage().getParameters().put('conEmail', 'test@test.com');
    ApexPages.currentPage().getParameters().put('accId', acc.Id);
    
    Serasa_User_Profiles__c objCustStt = new Serasa_User_Profiles__c();
    objCustStt.Name = 'Serasa Customer Care Profiles';
    objCustStt.Profiles__c = 'test,test1';
    insert objCustStt;

    AddOrCreateNewContactAddressController cntAdd = new AddOrCreateNewContactAddressController(sc); 
       
    ApexPages.currentPage().getParameters().put('conId',con.id);
    ApexPages.currentPage().getParameters().put('action',Label.CSS_Operation_AddAddress);
    ApexPages.currentPage().getParameters().put('addrId',conAddrRel.Id+';'+addr.Id);  
        
    AddOrCreateNewContactAddressController cnt = new AddOrCreateNewContactAddressController(sc);
    cnt.address.Validation_Status__c='test';
    cnt.performSave();

    ApexPages.currentPage().getParameters().put('addrId',conAddrRel.id+';'+addr.id); 
    ApexPages.currentPage().getParameters().put('action',Label.CSS_Operation_AddAddress);
    cnt.performSave();
        
    ApexPages.currentPage().getParameters().put('action',Label.CSS_Operation_NewContactAddress);
    cnt.performSave();
    cnt.cancel();
    cnt.blankCall();
    cnt.accountUpdateId();
    cnt.updateAddress(); // NLG - SPRINT 2 DEPLOYMENT - uncomment past March 25th
    cnt.address.Id = null;
    cnt.address.Validation_Status__c = null;
    cnt.performSave();

  }
  
  
  static testMethod void performDuplicateChecks() {
    // Assumes duplicate checking is enabled!
    
    Contact contact = new Contact();
    
    Serasa_User_Profiles__c objCustStt = new Serasa_User_Profiles__c();
    objCustStt.Name = 'Serasa Customer Care Profiles';
    objCustStt.Profiles__c = 'test,test1';
    insert objCustStt;
    
    ApexPages.StandardController sc = new ApexPages.StandardController(contact); 
       
    ApexPages.currentPage().getParameters().put('action',Label.CSS_Operation_NewContactAddress);
    ApexPages.currentPage().getParameters().put('bypassQAS','1');
    
    AddOrCreateNewContactAddressController cnt = new AddOrCreateNewContactAddressController(sc);
    
    cnt.contact.FirstName = '00TestContact0';
    cnt.contact.LastName = 'Sweden';
    
    cnt.address = new Address__c(Address_1__c='819 40th Avenue', Address_2__c='',Address_3__c='',
                          State__c='California',City__c='San Francisco',zip__c='94121',Country__c='United States of America');
    cnt.address.Validation_Status__c='Registered';
    
    cnt.performSave();
    //system.debug(cnt.getDuplicateRecords());
    cnt.performSaveAnyway();
    
    // system.assertEquals(1,cnt.getDuplicateRecords());
    
    // Omitting an assertion as we need to have the duplicate checking enabled, and not sure how we test for that in apex.         
  }

  static testMethod void testNewContactAddressAndNewContactExisting() {
    
    Contact contact = [SELECT Id, AccountId FROM Contact WHERE FirstName = '00TestContact0' AND LastName = 'Sweden'];
    
    ApexPages.currentPage().getParameters().put('conId',contact.id);
    ApexPages.currentPage().getParameters().put('action',Label.CSS_Operation_AddAddress);

    ApexPages.StandardController sc = new ApexPages.StandardController(contact);
    
    Serasa_User_Profiles__c objCustStt = new Serasa_User_Profiles__c();
    objCustStt.Name = 'Serasa Customer Care Profiles';
    objCustStt.Profiles__c = 'test,test1';
    insert objCustStt;

    AddOrCreateNewContactAddressController cnt = new AddOrCreateNewContactAddressController(sc);

    // Assign the address selected as being unique
    cnt.address = new Address__c(Address_1__c='818 40th Avenue', Address_2__c='',Address_3__c='',
                          State__c='California',City__c='San Francisco',zip__c='94121',Country__c='United States of America');
    cnt.address.Validation_Status__c='test';

    // This should create a Contact, an Address record and an Account_Address__c junction record
    cnt.performSave();
    
    // Unique Address which we want to verify will not be created as a duplicate
    Address__c addressVerif = [SELECT Id, Address_Id__c FROM Address__c WHERE Id =: cnt.address.Id];
    Contact_Address__c contactAddressFirst = [SELECT Id, Address__r.Id, Address__r.Address_Id__c
                                              FROM Contact_Address__c
                                              WHERE Contact__c =: cnt.contact.Id AND Address__c =: cnt.address.Id];

    // Address_Id__c is the generated key
    system.assertNotEquals(null, addressVerif.Address_Id__c);
    
    // Checks on the junction object
    system.assertEquals(addressVerif.Id, contactAddressFirst.Address__c);
    
    // Specifying that a new address needs to be created
    ApexPages.currentPage().getParameters().put('action', Label.CSS_Operation_NewContactAddress);
    
    ApexPages.StandardController scNew = new ApexPages.StandardController(new Contact());
    cnt = new AddOrCreateNewContactAddressController(scNew);
    
    // Create new Contact
    cnt.contact = new Contact(FirstName= '00TestDuplicateContact0',LastName = 'Sweden');

    // Assign the same address as for the first Contact
    cnt.address = new Address__c(Address_1__c='818 40th Avenue', Address_2__c='',Address_3__c='',
                          State__c='California',City__c='San Francisco',zip__c='94121',Country__c='United States of America');
    cnt.address.Validation_Status__c='test';

    Test.StartTest();
    // This should use the existing address record
    cnt.performSave();
    Test.StopTest();

    // Getting the created Contact_Address__c junction record. We want to verify it links to the existing Address__c record
    Contact_Address__c contactAddressSecond = [SELECT Id, Address__r.Id, Address__r.Address_Id__c
                                               FROM Contact_Address__c
                                               WHERE Contact__c =: cnt.contact.Id AND Address__c =: cnt.address.Id];
    system.assertEquals(addressVerif.Id, contactAddressSecond.Address__c);
  }


  static testMethod void testNewContactAddressAndNewAccountExisting() {
    
    Contact contact = [SELECT Id, AccountId FROM Contact WHERE FirstName = '00TestContact0' AND LastName = 'Sweden'];
    
    ApexPages.currentPage().getParameters().put('conId',contact.id);
    ApexPages.currentPage().getParameters().put('action',Label.CSS_Operation_AddAddress);
    
    ApexPages.StandardController sc = new ApexPages.StandardController(contact);
    
    Serasa_User_Profiles__c objCustStt = new Serasa_User_Profiles__c();
    objCustStt.Name = 'Serasa Customer Care Profiles';
    objCustStt.Profiles__c = 'test,test1';
    insert objCustStt;

    AddOrCreateNewContactAddressController cnt = new AddOrCreateNewContactAddressController(sc);

    // Assign the address selected as being unique
    cnt.address = new Address__c(Address_1__c='818 40th Avenue', Address_2__c='',Address_3__c='',
                          State__c='California',City__c='San Francisco',zip__c='94121',Country__c='United States of America');
    cnt.address.Validation_Status__c='test';

    // This should create a Contact, an Address record and an Account_Address__c junction record
    cnt.performSave();
    
    // Unique Address which we want to verify will not be created as a duplicate
    Address__c addressVerif = [SELECT Id, Address_Id__c FROM Address__c WHERE Id =: cnt.address.Id];
    Contact_Address__c contactAddressFirst = [SELECT Id, Address__r.Id, Address__r.Address_Id__c
                                              FROM Contact_Address__c
                                              WHERE Contact__c =: cnt.contact.Id AND Address__c =: cnt.address.Id];

    // Address_Id__c is the generated key
    system.assertNotEquals(null, addressVerif.Address_Id__c);
    
    // Checks on the junction object
    system.assertEquals(addressVerif.Id, contactAddressFirst.Address__c);

  }
  

  static testMethod void testContactRedirect_SerasaUserConsumerContact() {
    // create test data
    Profile p = [select id from profile where name=: Constants.PROFILE_EXP_SERASA_FINANCE ];
    PermissionSet perSet = [SELECT Id, Name FROM PermissionSet
                              WHERE Name =: Constants.PERMISSIONSET_EDQ_SAAS_DEPLOYMENT_MANAGER];
                                
    User testUser = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    testUser.EmployeeNumber = 'E1234';
    insert testUser;
    
    PermissionSetAssignment psetAssgn = new PermissionSetAssignment (AssigneeId = testUser.Id, PermissionSetId = perSet.Id);
    
    Serasa_User_Profiles__c custSettings = new Serasa_User_Profiles__c(Name = Constants.SERASA, Profiles__c= Constants.PROFILE_EXP_SERASA_FINANCE) ;
    insert custSettings;
     
    Account account = Test_Utils.insertAccount();
    account.Serasa_Consumer_Active_Account__c = true;
    update account;
    Contact con = Test_Utils.createContact(account.ID);
    insert con;
    
    Serasa_User_Profiles__c objCustStt = new Serasa_User_Profiles__c();
    objCustStt.Name = 'Serasa Customer Care Profiles';
    objCustStt.Profiles__c = 'test,test1';
    insert objCustStt;
    
    system.runAs(testUser) {
    Test.startTest();
    
      RecordType consumer_contact_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Consumer_Contact'];

      ApexPages.currentPage().getParameters().put('accId',account.id);
      ApexPages.currentPage().getParameters().put('RecordType', consumer_contact_rt.Id);
      ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)con);
      AddOrCreateNewContactAddressController controller = new AddOrCreateNewContactAddressController(stdController);
      
      Test.stopTest();
    }
  }

  // private static Contact contact;
  //private static Address__c address;
  //private static Account acc;
  
  @testSetup
  private static void createTestData() {
    Global_Settings__c custSettings = new Global_Settings__c(name=Constants.GLOBAL_SETTING,Smart_Search_Query_Limit__c=250);
    insert custSettings;
    
    Account acc = new Account(Name = '00TestAccount0',BillingCountry = 'Sweden');
    insert acc;
    Contact contact = new Contact(FirstName= '00TestContact0',LastName = 'Sweden');
    insert contact;
    Address__c address = new Address__c(Address_1__c='818 40th Avenue', Address_2__c='',Address_3__c='',
                          State__c='California',City__c='San Francisco',zip__c='94121',Country__c='United States of America');
    insert address;
    
    Profile p = [SELECT id from profile where name =: Constants.PROFILE_SYS_ADMIN ];
    User testUser = new User(alias = 'testUser', email='standarduser' + Math.random()  + '@experian.com',
                       emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                       localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', 
                       username='teststandarduser' + Math.random() + '@experian.global.test', IsActive=true,
                       CompanyName = 'test Company');
    testUser.Country__c = 'United States of America';                
    insert testUser;
  }
}