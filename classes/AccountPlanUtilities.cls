/**=====================================================================
 * Name: AccountPlanUtilities
 * Description: This class is to be used for methods that are common to the Account Plan area of functionality.
 * Created Date: June 3rd, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By           Description of the update
 * June 3rd, 2016        James Wills           AP - Saving Child Objects - Return to correct tab: Created
 * June 8th, 2016        James Wills           Case #1192655 Show the Business Unit alongside each AE under Account Team
  =====================================================================*/

public class AccountPlanUtilities{
  
  //Called by all of the Account Plan sub-tab classes.
  //This method is used to set Cookies that are used to land the user back on the correct Account Plan tab after they leave and then return to the plan.
  public static PageReference setAccountPlanCookies(ID accountPlanID, String currentUserAccountPlanTab){
    Cookie accountPlanLastVisitedCookie = new Cookie('accountPlanLastVisited', accountPlanId, null,-1,false);
    ApexPages.currentPage().setCookies(new Cookie[]{accountPlanLastVisitedCookie});
  
    Cookie tabLastVisitedCookie = new Cookie('tabLastVisited', currentUserAccountPlanTab, null,-1,false);
    ApexPages.currentPage().setCookies(new Cookie[]{tabLastVisitedCookie});
  
    return null;
  }

  //This method is used by all of the Account Plan sub-tab classes that support related-lists where a deletion from the list can be done 
  public static PageReference deleteAccountPlanRelatedListEntry(ID selectedId, String accountPlanObject, String accountPlanTab) {

    String queryString = 'SELECT Id, Name FROM ' + accountPlanObject + ' WHERE Id = :selectedId';
    try{
      if ( selectedId != null ) {               
        sObject objectToDel = Database.query(queryString);  
        delete objectToDel;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.INFO, accountPlanTab + ' list entry has been deleted.'));        
      }
    }
    catch ( exception ex) {
      apexLogHandler.createLogAndSave(accountPlanTab,'deleteAccountPlanRelatedListEntry', ex.getStackTraceString(), ex);
      ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, ex.getMessage()));
    }
    return null;
  }


  //Case #1192655 Show the Business Unit alongside each AE under Account Team - Method moved here from AccountTeamMembersList Class
  //This method is used to make sure that when an AccountTeamMember is deleted from an Account by the user, 
  //the equivalent Account_Plan_Team__c records are deleted from all of the Account Plans for the Account.
  public static void deleteAccountPlanTeams(AccountTeamMember atmToDelete){
    List<Account_Plan_Team__c> accountPlanTeamNewMembersToDelete = new List<Account_Plan_Team__c>();
    
    ID accountTeamMemberUID = [SELECT id, UserId FROM AccountTeamMember WHERE id = :atmToDelete.Id].UserId;
    String accountTeamMemberUIDString = accountTeamMemberUID;
           accountTeamMemberUIDString = accountTeamMemberUIDString.substring(0,15);
    
    for(Account_Plan__c accPlan : [SELECT id FROM Account_Plan__c WHERE Account__c = :atmToDelete.AccountId]){          
       //Search through all Salesforce User Account_Team_Plan__c records       
       for(Account_Plan_Team__c aptMember : [SELECT id, User__c FROM Account_Plan_Team__c WHERE Account_Plan__c = :accPlan.id AND User__c!=null]){

         String accountPlanTeamUIDString = aptMember.User__c;
                accountPlanTeamUIDString = accountPlanTeamUIDString.substring(0,15);

         if(accountPlanTeamUIDString==accountTeamMemberUIDString){
           accountPlanTeamNewMembersToDelete.add(aptMember);
           //ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'accountPlanTeam found: ' + accountPlanTeamUIDString ));
         }      
       }       
    }
       
    if(accountPlanTeamNewMembersToDelete.size()>0){
      try{
        delete accountPlanTeamNewMembersToDelete;
      } catch(DMLException e){
        system.debug('Problem deleting Account Plan Team Member.' );
        system.debug('\n AccountTeamMembersList - deleteAccountPlanTeams: '+ e.getLineNumber() + ' Stack:' + e.getStackTraceString() );
      }
    }
  }
  //Case #1192655 Show the Business Unit alongside each AE under Account Team


  public static String checkAccess(ID recordID){
      
      String currentUserId = userinfo.getUserID();
      Boolean userHasReadAccess   = false;
      Boolean userHasEditAccess   = false;
      Boolean userHasDeleteAccess = false;
      
      String accessType ='';
        
      for (UserRecordAccess userAccess  : [SELECT RecordId, HasReadAccess, HasEditAccess, HasDeleteAccess FROM UserRecordAccess WHERE UserId = :currentUserId 
                                                                                                                AND RecordId = :recordID]) {
            if(userAccess.HasReadAccess == true){
              userHasReadAccess = true;
            }            
            if(userAccess.HasEditAccess == true){
              userHasEditAccess = true; 
            }
            if (userAccess.HasDeleteAccess ==true){
              userHasDeleteAccess = true;
            }
       }   
       
      List<Profile> UserProfile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
      String userProfileName = UserProfile[0].Name;
    
      integer profilehasOverrideAccess =0; 
      //check if the User is part of the set of Profiles who have Override access in the custom Setting to add others to Account Team
      profilehasOverrideAccess= database.countQuery('SELECT count() FROM Profiles_w_override_access_2_AcctContact__c Where Profile_Name__c = :userProfileName');
        
      //System.Debug('CurrentUserhasAccess to Account(before Profile check):' + userhasAccess);  
      if (profilehasOverrideAccess > 0 ){ 
        userHasReadAccess   = true;
        userHasEditAccess   = true; 
        userHasDeleteAccess = true;
      }
       
      System.Debug('CurrentUser has Override Access to Account:' + profilehasOverrideAccess); 
      //System.Debug('CurrentUserhasAccess to Account:(AfterOverridecheck)' + userhasAccess);  
      
      if(userHasReadAccess==true && userHasEditAccess==false && userHasDeleteAccess==false){
        accessType='Read';
      } else if(userHasEditAccess==true && userHasDeleteAccess==false){
        accessType='Read/Edit';
      } else if(userHasEditAccess==true && userHasDeleteAccess==true){
        accessType='Read/Edit/Delete';
      } else if(userHasEditAccess==false && userHasDeleteAccess==true){
        accessType='Read/Delete';
      }
      
      return accessType;
   }

}