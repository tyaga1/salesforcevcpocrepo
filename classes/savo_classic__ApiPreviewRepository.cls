/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ApiPreviewRepository {
    global ApiPreviewRepository() {

    }
    webService static String GetImageUrl(Integer documentId, Integer documentPreviewId, String previewType) {
        return null;
    }
    webService static String GetUserImageUrl(Integer userId) {
        return null;
    }
}
