/**=====================================================================
 * Experian
 * Name: SavoOpportunityExt_Test
 * Description: SavoOpportunityExt
 * Created Date: Sep 7th, 2015
 * Created By: Paul Kissick
 * 
 * Date Modified        Modified By                   Description of the update
 * Aug 9th, 2016        Paul Kissick                  CRM2:W-005396:Adding fix for 'Decider'. Wasn't referencing the constant 
 =====================================================================*/
@isTest
private class SavoOpportunityExt_Test {

  static testMethod void myUnitTest() {
    
    Test.startTest();
    
    Opportunity opp1 = [SELECT Id, StageName FROM Opportunity LIMIT 1];
    
    ApexPages.StandardController stdCon = new ApexPages.StandardController(opp1);
    SavoOpportunityExt soe = new SavoOpportunityExt(stdCon);
    
    soe.getProducts();
    system.assertNotEquals('',soe.getAssetTypes());
    
    system.assertEquals(1,soe.getCompetitors().size(),'Competitor count incorrect');
    
    system.assertNotEquals('',soe.stageNameTranslation.get(opp1.StageName));
    
    system.assertNotEquals('',soe.getListTags());
    
    system.assertNotEquals('',soe.getEMEAOrderURL());
    
    Test.stopTest();
    
  }
  
  @testSetup private static void prepareData() {
    
    User myUser = [SELECT Id,Region__c FROM User WHERE Id = :UserInfo.getUserId()];
    myUser.Region__c = 'TestRegion';
    update myUser;
    
    Account tstAcc = Test_utils.createAccount();
    Account tstAccComp = Test_utils.createAccount();
    tstAccComp.Name = 'Competitor 1';
    tstAccComp.Is_Competitor__c = true;
    tstAccComp.FUSE_Tag_Id__c = '234737';
    insert new List<Account>{tstAcc, tstAccComp};
    
    Address__c add = Test_Utils.insertAddress(true);
    Account_Address__c accAdd = Test_Utils.insertAccountAddress(true, add.Id, tstAcc.Id);
    
    Contact testContact = Test_Utils.insertContact(tstAcc.Id);
    
    Opportunity tstOpp = Test_utils.createOpportunity(tstAcc.ID);
    tstOpp.CloseDate = Date.today().addDays(7);

    insert new List<Opportunity>{tstOpp};
    
    OpportunityContactRole ocr = Test_utils.insertOpportunityContactRole(true, tstOpp.Id, testContact.Id, Constants.DECIDER, true);
    
    Competitor__c comp = new Competitor__c(Account__c = tstAccComp.Id, Opportunity__c = tstOpp.Id);
    insert comp;
    
    CPQ_Settings__c CPQSettings = new CPQ_Settings__c();
    CPQSettings.Company_Code__c = 'Experian';
    CPQSettings.Name = 'CPQ';
    CPQSettings.CPQ_API_Endpoint__c = 'https://rssandbox.webcomcpq.com/wsapi/WsSrv.asmx';
    CPQSettings.CPQ_API_UserName__c = 'richard.joseph#ExperianGlobal';
    CPQSettings.CPQ_API_Access_Word__c = 'password';
    insert CPQSettings;
    
    Product2 product = Test_Utils.createProduct();
    product.FUSE_Tag_Id__c = '756577';
    product.Name = 'Product Test 83839320';
    // product.Product_Master__c = pm.Id;
    insert product;
    
    Product_Master__c pm = new Product_Master__c();
    pm.FUSE_product_Page_URL__c = 'http://test.com';
    pm.Name = 'Master Product Test 83839320';
    pm.Active__c = true;
    pm.Product_Name__c = 'Product Test 83839320';
    insert pm;
    
    PricebookEntry stdPricebookEntry = Test_Utils.insertPricebookEntry(product.Id, Test.getStandardPricebookId(), Constants.CURRENCY_USD);
    
    OpportunityLineItem tstOli = Test_utils.createOpportunityLineItem(tstOpp.Id, stdPricebookEntry.Id , tstOpp.Type);
    tstOli.Start_Date__c = Date.today().addDays(5);
    tstOli.End_Date__c = Date.today().addDays(120);
        
    insert new List<OpportunityLineItem>{tstOli};
    
    Savo_Tags__c st1 = new Savo_Tags__c(Name = 'Push to CRM', FUSE_Tag_Id__c = '1245365');
    Savo_Tags__c st2 = new Savo_Tags__c(Name = 'TestRegion', FUSE_Tag_Id__c = '857357');
    insert new List<Savo_Tags__c> {st1,st2};
    
  }
  
}