/*=====================================================================
 * Experian
 * Name: AllAttachments_Test
 * Description: This class is used to Test the functionality of AllAttachments
 *
 * Created Date: July 14th, 2016
 * Created By: Manoj (Experian)
 *
 * Date Modified          Modified By            Description of the update
 * July 14th 2016         Manoj Gopu             CRM2:W-005491 - Consolidated view of attachments on the account record
 * Sep 08th 2016          Manoj Gopu             CRM2:W-005491 - Added asserts to the test methods.
 * Jan  26th 2017		  Sanket Vaidya			 Case#02165224 - Order by create date - Added Test_Column_Sorting1() and Test_Column_Sorting2()
 ======================================================================*/
@isTest(seeAllData=true)
private class AllAttachments_Test{
    static testMethod void myUnitTest() {
        Test.startTest();          
            Account acc = Test_Utils.insertAccount();           
            Attachment att=new Attachment();
            att.Name='Test1';
            att.Body=Blob.valueOf('Test Attachment Body');
            att.ParentId=acc.Id;
            insert att;
                                  
            Opportunity opty=new Opportunity();
            opty.Name='Test Opportunity';
            opty.StageName='Qualify';
            opty.AccountId=acc.Id;
            opty.CloseDate=system.today().addDays(30);
            insert opty;
            
            Attachment att1=new Attachment();
            att1.Name='Test2';
            att1.Body=Blob.valueOf('Test Attachment Body');
            att1.ParentId=opty.Id;
            insert att1;
            
            Account_segment__c accSeg=new Account_segment__c();
            accSeg.Account__c=acc.Id;
            accSeg.Name='Test segment';
            insert accSeg;
            
            Attachment att2=new Attachment();
            att2.Name='Test3';
            att2.Body=Blob.valueOf('Test Attachment Body');
            att2.ParentId=accSeg.Id;
            insert att2;
            
            Confidential_information__c confInfo=new Confidential_information__c();
            confInfo.Account__c=acc.Id;
            confInfo.Name='Test Confidential';
            insert confInfo;
            
            Attachment att3=new Attachment();
            att3.Name='Test4';
            att3.Body=Blob.valueOf('Test Attachment Body');
            att3.ParentId=confInfo.Id;
            insert att3;
            
            Membership__c memInfo=new Membership__c();
            memInfo.Account__c=acc.Id;
            //memInfo.Name='Test Membership';
            insert memInfo;
            
            Attachment att4=new Attachment();
            att4.Name='Test5';
            att4.Body=Blob.valueOf('Test Attachment Body');
            att4.ParentId=memInfo.Id;
            insert att4;
            
            Account_Plan__c accplan=new Account_Plan__c();
            accplan.Account__c=acc.Id;
            accplan.Name='Test Membership';
            insert accplan;
            
            Attachment att5=new Attachment();
            att5.Name='Test6';
            att5.Body=Blob.valueOf('Test Attachment Body');
            att5.ParentId=accplan.Id;
            insert att5;
                        
            ApexPages.currentPage().getParameters().put('Id',acc.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(new Account());
            
            AllAttachments objCont=new AllAttachments(sc);
            string sortexp=objCont.sortExpression;
            System.assertEquals(objCont.getSortDirection(), 'ASC');
            objCont.sortExpression='Object Name';
            string str=objCont.getSortDirection();
            System.assertEquals(objCont.getSortDirection(), 'DESC');
            objCont.displayAttachments();   
            system.assertEquals(objCont.totallistsize,6);                 
            objCont.sortExpression='Object Name';
            objCont.displayAttachments();    
                        
            objCont.sortExpression='';  
            string st1r=objCont.getSortDirection();            
            
            PageReference pf=objCont.nextBtnClick();
            System.assertEquals(pf, null);
            System.assertEquals(objCont.getPageNumber(), 2);
            objCont.previousBtnClick();
            System.assertEquals(objCont.getPageNumber(), 1);
            integer i=objCont.getPageNumber();
            System.assertEquals(i, 1);
            System.assertEquals(objCont.getPageSize(),50);
            System.assertEquals(objCont.getPreviousButtonEnabled(),true);
            System.assertEquals(objCont.getNextButtonDisabled(),true);
            System.assertEquals(objCont.getTotalPageNumber(),1);
            Integer newPageIndex=1;
            objCont.BindData(newPageIndex);
            objCont.pageData(newPageIndex);
            objCont.LastpageData(newPageIndex);
            objCont.LastbtnClick();
            System.assertEquals(objCont.getPageNumber(), 1);
            objCont.FirstbtnClick();  
            System.assertEquals(objCont.getPageNumber(), 1);          
            
        Test.stoptest();    
    
    }
    static testMethod void myUnitTest1() {
        Test.startTest();         
            Account acc = Test_Utils.insertAccount();           
            Attachment att=new Attachment();
            att.Name='Test1';
            att.Body=Blob.valueOf('Test Attachment Body');
            att.ParentId=acc.Id;
            insert att;
                                  
            Opportunity opty=new Opportunity();
            opty.Name='Test Opportunity';
            opty.StageName='Qualify';
            opty.AccountId=acc.Id;
            opty.CloseDate=system.today().addDays(30);
            insert opty;
            
            Attachment att1=new Attachment();
            att1.Name='Test2';
            att1.Body=Blob.valueOf('Test Attachment Body');
            att1.ParentId=opty.Id;
            insert att1;
            
            Account_segment__c accSeg=new Account_segment__c();
            accSeg.Account__c=acc.Id;
            accSeg.Name='Test segment';
            insert accSeg;
            
            Attachment att2=new Attachment();
            att2.Name='Test3';
            att2.Body=Blob.valueOf('Test Attachment Body');
            att2.ParentId=accSeg.Id;
            insert att2;
            
            Confidential_information__c confInfo=new Confidential_information__c();
            confInfo.Account__c=acc.Id;
            confInfo.Name='Test Confidential';
            insert confInfo;
            
            Attachment att3=new Attachment();
            att3.Name='Test4';
            att3.Body=Blob.valueOf('Test Attachment Body');
            att3.ParentId=confInfo.Id;
            insert att3;
            
            Membership__c memInfo=new Membership__c();
            memInfo.Account__c=acc.Id;
            //memInfo.Name='Test Membership';
            insert memInfo;
            
            Attachment att4=new Attachment();
            att4.Name='Test5';
            att4.Body=Blob.valueOf('Test Attachment Body');
            att4.ParentId=memInfo.Id;
            insert att4;
            
            Account_Plan__c accplan=new Account_Plan__c();
            accplan.Account__c=acc.Id;
            accplan.Name='Test Membership';
            insert accplan;
            
            Attachment att5=new Attachment();
            att5.Name='Test6';
            att5.Body=Blob.valueOf('Test Attachment Body');
            att5.ParentId=accplan.Id;
            insert att5;
                        
            ApexPages.currentPage().getParameters().put('Id',acc.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(new Account());
            
            AllAttachments objCont=new AllAttachments(sc);
            objCont.sortExpression='Title';            
            objCont.displayAttachments();
            system.assertEquals(objCont.totallistsize,6);
            System.assertEquals(objCont.getSortDirection(), 'ASC');
            
            objCont.sortExpression='Title';            
            objCont.displayAttachments();
            System.assertEquals(objCont.getSortDirection(), 'DESC');
            Test.stoptest();
    }
    
    static testMethod void Test_Column_Sorting1() {
        Test.startTest();      
            Account acc = Test_Utils.insertAccount();           
            Attachment att=new Attachment();
            att.Name='Test1';
            att.Body=Blob.valueOf('Test Attachment Body');
            att.ParentId=acc.Id;
            insert att;
                                  
            Opportunity opty=new Opportunity();
            opty.Name='Test Opportunity';
            opty.StageName='Qualify';
            opty.AccountId=acc.Id;
            opty.CloseDate=system.today().addDays(30);
            insert opty;
            
            Attachment att1=new Attachment();
            att1.Name='Test2';
            att1.Body=Blob.valueOf('Test Attachment Body');
            att1.ParentId=opty.Id;
            insert att1;
            
            Account_segment__c accSeg=new Account_segment__c();
            accSeg.Account__c=acc.Id;
            accSeg.Name='Test segment';
            insert accSeg;
            
            Attachment att2=new Attachment();
            att2.Name='Test3';
            att2.Body=Blob.valueOf('Test Attachment Body');
            att2.ParentId=accSeg.Id;
            insert att2;
            
            Confidential_information__c confInfo=new Confidential_information__c();
            confInfo.Account__c=acc.Id;
            confInfo.Name='Test Confidential';
            insert confInfo;
            
            Attachment att3=new Attachment();
            att3.Name='Test4';
            att3.Body=Blob.valueOf('Test Attachment Body');
            att3.ParentId=confInfo.Id;
            insert att3;
            
            Membership__c memInfo=new Membership__c();
            memInfo.Account__c=acc.Id;
            //memInfo.Name='Test Membership';
            insert memInfo;
            
            Attachment att4=new Attachment();
            att4.Name='Test5';
            att4.Body=Blob.valueOf('Test Attachment Body');
            att4.ParentId=memInfo.Id;
            insert att4;
            
            Account_Plan__c accplan=new Account_Plan__c();
            accplan.Account__c=acc.Id;
            accplan.Name='Test Membership';
            insert accplan;
            
            Attachment att5=new Attachment();
            att5.Name='Test6';
            att5.Body=Blob.valueOf('Test Attachment Body');
            att5.ParentId=accplan.Id;
            insert att5;
                        
            ApexPages.currentPage().getParameters().put('Id',acc.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(new Account());
            
            AllAttachments objCont=new AllAttachments(sc);
            objCont.sortExpression='Business Unit';            
            objCont.displayAttachments();  
            system.assertEquals(objCont.totallistsize,6);
            System.assertEquals(objCont.getSortDirection(), 'ASC');       
            
            objCont.sortExpression='Business Unit';            
            objCont.displayAttachments();
            System.assertEquals(objCont.getSortDirection(), 'DESC');
        
        	/*Test ASC and DESC order */
        	objCont.sortExpression='Type';            
            objCont.displayAttachments();
            System.assertEquals(objCont.getSortDirection(), 'ASC');
        	
        	objCont.sortExpression='Type';            
            objCont.displayAttachments();
            System.assertEquals(objCont.getSortDirection(), 'DESC');
        
        	        
        
        Test.stopTest();
    }
    
      static testMethod void Test_Column_Sorting2() {
        Test.startTest();      
            Account acc = Test_Utils.insertAccount();           
            Attachment att=new Attachment();
            att.Name='Test1';
            att.Body=Blob.valueOf('Test Attachment Body');
            att.ParentId=acc.Id;
            insert att;
                                  
            Opportunity opty=new Opportunity();
            opty.Name='Test Opportunity';
            opty.StageName='Qualify';
            opty.AccountId=acc.Id;
            opty.CloseDate=system.today().addDays(30);
            insert opty;
            
            Attachment att1=new Attachment();
            att1.Name='Test2';
            att1.Body=Blob.valueOf('Test Attachment Body');
            att1.ParentId=opty.Id;
            insert att1;
            
            Account_segment__c accSeg=new Account_segment__c();
            accSeg.Account__c=acc.Id;
            accSeg.Name='Test segment';
            insert accSeg;
            
            Attachment att2=new Attachment();
            att2.Name='Test3';
            att2.Body=Blob.valueOf('Test Attachment Body');
            att2.ParentId=accSeg.Id;
            insert att2;
            
            Confidential_information__c confInfo=new Confidential_information__c();
            confInfo.Account__c=acc.Id;
            confInfo.Name='Test Confidential';
            insert confInfo;
            
            Attachment att3=new Attachment();
            att3.Name='Test4';
            att3.Body=Blob.valueOf('Test Attachment Body');
            att3.ParentId=confInfo.Id;
            insert att3;
            
            Membership__c memInfo=new Membership__c();
            memInfo.Account__c=acc.Id;
            //memInfo.Name='Test Membership';
            insert memInfo;
            
            Attachment att4=new Attachment();
            att4.Name='Test5';
            att4.Body=Blob.valueOf('Test Attachment Body');
            att4.ParentId=memInfo.Id;
            insert att4;
            
            Account_Plan__c accplan=new Account_Plan__c();
            accplan.Account__c=acc.Id;
            accplan.Name='Test Membership';
            insert accplan;
            
            Attachment att5=new Attachment();
            att5.Name='Test6';
            att5.Body=Blob.valueOf('Test Attachment Body');
            att5.ParentId=accplan.Id;
            insert att5;
                        
            ApexPages.currentPage().getParameters().put('Id',acc.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(new Account());
            
            AllAttachments objCont=new AllAttachments(sc);
                   
        	objCont.sortExpression='CreatedDate';            
            objCont.displayAttachments();
            System.assertEquals(objCont.getSortDirection(), 'ASC');
        
        	objCont.sortExpression='CreatedDate';            
            objCont.displayAttachments();
            System.assertEquals(objCont.getSortDirection(), 'DESC');
        
        	objCont.sortExpression='CreatedBy';            
            objCont.displayAttachments();
            System.assertEquals(objCont.getSortDirection(), 'ASC');
        
        	objCont.sortExpression='CreatedBy';            
            objCont.displayAttachments();
            System.assertEquals(objCont.getSortDirection(), 'DESC');
        
        	objCont.sortExpression='CreatorRegion';            
            objCont.displayAttachments();
            System.assertEquals(objCont.getSortDirection(), 'ASC');
        
        	objCont.sortExpression='CreatorRegion';            
            objCont.displayAttachments();
            System.assertEquals(objCont.getSortDirection(), 'DESC');
        
        
        Test.stopTest();
    }
    
    static testMethod void myUnitTest4() {
        Test.startTest();
            Account acc = Test_Utils.insertAccount();
            ApexPages.currentPage().getParameters().put('Id',acc.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(new Account());
            
            AllAttachments objCont=new AllAttachments(sc);
            objCont.displayAttachments();  
            system.assertEquals(objCont.totallistsize,0); 
            objCont.getNextButtonDisabled();
        Test.stopTest();
    }
}