/**=====================================================================
 * Experian
 * Name: BatchCaseMinutesToCloseFix_Test
 * Description: 
 * Created Date: 28 Jan 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By        Description of the update

 =====================================================================*/
@isTest
private class BatchCaseMinutesToCloseFix_Test {

  static testMethod void testBatchNormal() {
    
    system.assertEquals(0,[SELECT COUNT() FROM Case WHERE Minutes_To_Close__c != null]);
    
    Test.startTest();
    
    BatchCaseMinutesToCloseFix b = new BatchCaseMinutesToCloseFix();
    Database.executeBatch(b);
    
    Test.stopTest();
    
    system.assertEquals(0,[SELECT COUNT() FROM Case WHERE Minutes_To_Close__c = null]);
  }
  
  static testMethod void testBatchCustom() {
    
    system.assertEquals(0,[SELECT COUNT() FROM Case WHERE Minutes_To_Close__c != null]);
    
    Test.startTest();
    
    BatchCaseMinutesToCloseFix b = new BatchCaseMinutesToCloseFix();
    b.customQuery = 'SELECT Id, BusinessHoursId, Minutes_To_Close__c, CreatedDate, ClosedDate FROM Case';
    Database.executeBatch(b);
    
    Test.stopTest();
    
    system.assertEquals(0,[SELECT COUNT() FROM Case WHERE Minutes_To_Close__c = null]);
  }
  
  @testSetup
  static void setupTestData() {
    IsDataAdmin__c setting = new IsDataAdmin__c(SetupOwnerId = UserInfo.getUserId(), IsDataAdmin__c = true);
    insert setting;
    
    BusinessHours bhNotDefault = [SELECT ID FROM BusinessHours WHERE IsActive = true AND IsDefault = false LIMIT 1];
    
    List<CaseStatus> closedCaseStatusList = [
      SELECT Id,MasterLabel 
      FROM CaseStatus 
      WHERE IsClosed = true
    ];
    
    Case case1 = new Case(BusinessHoursId = bhNotDefault.Id, CreatedDate = Datetime.now().addDays(-7), ClosedDate = Datetime.now(), Status = closedCaseStatusList[0].MasterLabel);
    Case case2 = new Case(BusinessHoursId = bhNotDefault.Id, CreatedDate = Datetime.now().addDays(-8), ClosedDate = Datetime.now(), Status = closedCaseStatusList[0].MasterLabel);
    Case case3 = new Case(BusinessHoursId = bhNotDefault.Id, CreatedDate = Datetime.now().addDays(-9), ClosedDate = Datetime.now(), Status = closedCaseStatusList[0].MasterLabel);
    insert new List<Case>{case1,case2,case3};
    
  }
}