/**=====================================================================
 * Name: ScheduleReportMonthly_Test
 * Description: Code coverage for ScheduleReportMonthly and ScheduleReportWeeklyUnpaidNoms
 * Created Date: Jan 26th, 2017
 * Created By: Manoj Gopu - Case : 02268605
 * 
 * Date Modified     Modified By        Description of the update
 
 =====================================================================*/
@isTest
private class ScheduleReportMonthly_Test{
    static testMethod void myUnitTest() {
        Test.startTest();          
            
            ScheduleReportWeeklyUnpaidNoms s = new ScheduleReportWeeklyUnpaidNoms();
            s.execute(null);
            
            ScheduleReportMonthly s1 = new ScheduleReportMonthly();
            s1.execute(null);
            
        Test.stopTest();
    }
    
}