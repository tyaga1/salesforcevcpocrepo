/**=====================================================================
 * Experian
 * Name: SMXProcessCaseBatch 
 * Description: 
 * Created Date: Unknown
 * Created By: Unknown
 * 
 * Date Modified     Modified By        Description of the update
 * Jul 22nd, 2015    Paul Kissick       Case #01040868 - Fixing nested soql query within for loop
 * Aug 17th, 2015    Paul Kissick       Case #01101628 - Adding checks for case status not duplicated and case owner being a queue
 * Feb 2nd, 2016     James Wills        Case #01266714 - Updated references to Case Record Types following name changes.
 * Mar 1st, 2016     Paul Kissick       Case #01879223 - Extending contact email check to remove any ending in experian.com
 * July 12th,2016(QA) Tyaga Pati        CRM2:W-005363  - Added code to update the Account on Survey record as part of the nomination record creation process.
 * July 12th,2017    Diego Olarte       Case #13378726 - Added code to Exclude EMEA Records                                        
 =====================================================================*/

global class SMXProcessCaseBatch implements Database.Batchable<Case>, Database.AllowsCallouts{

  // PK: Moving this out of the class and into the start method
  // case [] AssetsToUpdate = [select Id, ContactId from Case where isClosed=true and closedDate >= YESTERDAY and RecordTypeId IN (select id from RecordType where name IN ('EDQ Case', 'EDQ Commercial Case', 'EDQ GPD Case')) and ContactId != null and ContactId != ''];

  global Iterable<Case> start(database.batchablecontext BC){
    if (Test.isRunningTest()) {
      case cs = SMXProcessCaseBatchTest.prepareTestData();
      List<Case> casesToUpdate = new List<Case>();
      casesToUpdate.add(cs);
      return casesToUpdate; 
    }
    else{
      // PK: Query now includes the Email and related survey/feedback records. 
      return [
        SELECT Id, ContactId,Contact.AccountId, Contact.Email, (SELECT Id FROM Survey__r)
        FROM Case 
        WHERE IsClosed = true 
        AND ClosedDate >= YESTERDAY 
        AND Status != 'Closed - Duplicate'
        AND Case_Owned_by_Queue__c = false
        AND EDQ_Support_Region__c != 'NA'
        AND EDQ_Support_Region__c != 'EMEA' //Case #13378726: Added by DO
        AND RecordTypeId IN (
          SELECT Id 
          FROM RecordType 
          WHERE Name IN ('EDQ Technical Support', 'EDQ Commercial Support', 'EDQ GPD Support')//Case #01266714 James Wills;
        ) 
        AND ContactId != null 
        AND ContactId != ''
      ];
    }
    //return AssetsToUpdate;
  }

  global void execute(Database.BatchableContext BC, List<Case> scope){
 
    String strSurveyName = 'EDQ Support Experience';
    String strSurveyId = 'EXPERIAN_106615';
    
    List <Feedback__c> feedbackList = new List<Feedback__c>();
    Long lgSeed = System.currentTimeMillis();
    // Set<Id> setContactIds = new Set<Id>(); // PK: Removing as not used anywhere
    Boolean result = false;
    for(Case cs : scope){
      
      // PK: Removing and placing into initial query. Saves SOQL calls
      // List<Feedback__c> lstSurvey = [select Id from Feedback__c where case__c =: cs.Id ];
      List<Feedback__c> lstSurvey = cs.Survey__r;
      
      // PK: Removing and adding to initial query. Saves SOQL calls.
      // Contact con=[select email from Contact where id=: cs.ContactId];
      
      // PK: Adding check for email address not blank
      if(lstSurvey.isEmpty() && (String.isNotBlank(cs.Contact.Email) && !cs.Contact.Email.endsWithIgnoreCase('experian.com'))) {
        lgSeed = lgSeed + 1;
        Feedback__c feedback = new Feedback__c();
        feedback.Case__c = cs.Id;
        feedback.Name = 'P_' + lgSeed;
        feedback.Contact__c = cs.ContactId; //ContactName 
        feedback.Account__c = cs.Contact.AccountId; 
        feedback.Status__c = 'Nominated';               
        feedback.DataCollectionName__c = strSurveyName;
        feedback.DataCollectionId__c = strSurveyId;
        feedbackList.add(feedback);        
      }
    }
    insert feedbackList;
  }

  global void finish(Database.BatchableContext info){
    if(Test.isRunningTest()){
      SMXProcessCaseBatchTest.ClearTestData();
    }
  }//global void finish loop
    
  
}