public with sharing class StratComm_NewArticleTopicEmail{

    public StratComm_NewArticleTopicEmail(){
    }
    
    public static void sendEmail(Id articleId, String articleType, String topicName){
         List<User> recipients = [Select Id, Name, Profile.Name From User Where Profile.Name = 'Xperian Global Community Strategic'];
         List<String> recipientIds = new List<String>();
         for(User u : recipients){
            recipientIds.add(u.Id);
        }
        List<Contact> defaultContact = [select id from contact where name ='Experian Community Contact' limit 1];
        List<EmailTemplate> templateIdList = [SELECT Id FROM EmailTemplate
                                                        WHERE DeveloperName = 'Guide_Article_Notification'];
                                                        
        List<Employee_Article__kav> guideArticles = [Select Id From Employee_Article__kav Where Id = :articleId];
       
        
        List<Id> articleIdList = new List<Id>();
        
        //if(ta.EntityType == 'Employee_Article' && topicMap.get(ta.TopicId).Name == 'GSA'){
            //system.debug('Entered If');
            //articleIdList.add(ta.EntityId);
            
        //}
        List<Messaging.SingleEmailMessage> mailingList = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SendEmailResult> mailingResults = new List<Messaging.SendEmailResult>();
        
        for(Employee_Article__kav guideArticle : guideArticles ){
            mailingList.add(createEmail(defaultContact[0].Id, recipientIds, templateIdList[0].Id));
            system.debug('The Mailing List: '+ mailingList);
        
        }
        
        if(!mailingList.isEmpty()){
            mailingResults = Messaging.sendEmail(mailingList, false);
            system.debug('the sent email results: ' + mailingResults);
        }
 
    }
    
    public static Messaging.SingleEmailMessage createEmail(Id contactId, List<Id> bccAddressList, Id emailTemplateId){
            System.debug('createEmail called');
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setTemplateId(emailTemplateId);
            email.setTargetObjectId(contactId);
            email.setBccAddresses(bccAddressList);
            system.debug('The email: ' + email);
            return email;
        }
}