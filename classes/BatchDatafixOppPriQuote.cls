/*=============================================================================
 * Experian
 * Name: BatchDatafixOppPriQuote
 * Description: Temporary batch class to fix existing data for CRM2
 * Created Date: 2 Sep 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 * To run:
 Dev console as Data Admin user:
 
 Database.executeBatch(new BatchDatafixOppPriQuote(), ScopeSizeUtility.getScopeSizeForClass('BatchDatafixOppPriQuote'));
 
 =============================================================================*/

public class BatchDatafixOppPriQuote implements Database.Batchable<sObject>, Database.Stateful {

  public Database.Querylocator start (Database.Batchablecontext bc) {
    return Database.getQueryLocator([
      SELECT Opportunity__c 
      FROM Quote__c 
      WHERE Primary__c = true 
      AND Status__c = 'Approved'
      AND Opportunity__c != null
    ]);
  }
  
  public void execute (Database.BatchableContext bc, List<Quote__c> scope) {
    Map<Id, Opportunity> oppsToUpdate = new Map<Id, Opportunity>();
    for (Quote__c q : scope) {
      oppsToUpdate.put(q.Opportunity__c, new Opportunity(Id = q.Opportunity__c, Primary_Quote_has_been_Approved__c = true));
    }
    Database.update(oppsToUpdate.values(), false);
  }
  
  public void finish (Database.BatchableContext bc) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchDatafixOppPriQuote', true);
  }
  
  @isTest
  private static void testBatch() {
    // Create account, then opp, then quote, and set as approved. It should then flag the opp as quote approved.
    
    IsDataAdmin__c ida = Test_Utils.insertIsDataAdmin(true);
    Account a1 = Test_Utils.insertAccount();
    Opportunity o1 = Test_Utils.insertOpportunity(a1.Id);
    Opportunity o2 = Test_Utils.insertOpportunity(a1.Id);
    Quote__c q1 = new Quote__c(Opportunity__c = o1.Id, Status__c = 'Approved', Primary__c = true);
    Quote__c q2 = new Quote__c(Opportunity__c = o2.Id, Status__c = 'Approved', Primary__c = false);
    
    insert new List<Quote__c>{q1,q2};
    
    Test.startTest();
    
    Database.executeBatch(new BatchDatafixOppPriQuote());
    
    Test.stopTest();
    
    system.assertEquals(1, [SELECT COUNT() FROM Opportunity WHERE Primary_Quote_has_been_Approved__c = true], 'Opp Not set to Approved.');
    
  }
  
}