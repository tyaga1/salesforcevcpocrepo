/**=====================================================================
 * Experian
 * Name: contactAssetsConsoleExtension
 * Description: Controller for contactAssestsConsole page, which is displayed as a sidebar componant on Sales Console opportunity pages. 
 * 
 * Created Date: Sep 19th, 2017
 * Created By: Malcolm Russell
 * 
 * Date Modified        Modified By                  Description of the update
 *
 =====================================================================*/
public with sharing class contactAssetsConsoleExtension {

public Id conaccId {get; set;}

public contactAssetsConsoleExtension(ApexPages.StandardController controller) {

  contact c =(contact)controller.getRecord();
  system.debug('AccountID:::' + c.accountID);
  conaccId = c.accountID;
}

Public Asset[] accountAssets{get{
  Asset[] accass=[Select a.Renewal_Sale_Price__c, a.Renewal_EDQ_Margin__c, a.Renewal_Date__c, a.Quantity, a.Partner__r.Name, a.Partner__c, 
                  a.Direct_Renewal__c, a.CRM_Product_Name__c, a.Application__c,a.AccountId,a.Renewal_Total_Royalty__c
                  From Asset a
                  where a.AccountId=:conaccId and Status__c in ('Live','Scheduled','Delayed')  and a.Order_Owner_BU_Stamp__c like '%Data Quality%'];
  if(accass.size() >0){
  return accass;
  }
  else{return null;}
}set;}

}