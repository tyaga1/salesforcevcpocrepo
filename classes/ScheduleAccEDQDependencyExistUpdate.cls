/**=====================================================================
 * Experian
 * Name: ScheduleAccEDQDependencyExistUpdate 
 * Description: Case:02136424 Schedule BatchAccEDQDependencyExistUpdate to run daily
 * Created Date: 06th Dec 2016
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
 ============================================================================*/
public class ScheduleAccEDQDependencyExistUpdate implements Schedulable {
  public void execute(SchedulableContext SC) {
    BatchAccEDQDependencyExistUpdate batch = new BatchAccEDQDependencyExistUpdate(); 
    Database.executeBatch(batch, 200); 
  }
}
      // Code to run in dev console 
     //  System.schedule('AccEDQDependencyExistUpdate', '0 0 21 * * ?', new ScheduleAccEDQDependencyExistUpdate());