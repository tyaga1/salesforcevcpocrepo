/******************************************************************************
 * Name: expcommunity_subcomm.cls
 * Created Date: 2/14/2017
 * Created By: Richard Joseph
 * Description : testclass for expcommunity_Col_Cntrl
 * Change Log- 
 ****************************************************************************/

@isTest
private class TestclassForexpcommunity_Col_Cntrl{

    static testMethod void testMethodEmpColCntrl() {
    
    
    Profile getProfile = [SELECT Id FROM Profile where name ='Standard Platform User' limit 1]; 
        Profile stdProfile = [SELECT Id FROM Profile where name ='Standard User' limit 1]; 
        User newUser = new User(Alias = 'testu', Email='sampleusertest@experian.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = getProfile.Id, phone = '555555555',
        TimeZoneSidKey='America/Los_Angeles', UserName='sampleusertest.@experian.com', Department ='test');
        insert newUser; 
    Test.startTest();
        
        
        Employee_Community_News__c featQuote = new Employee_Community_News__c(Name ='test1',  isActive__c = true, Content_Type__c  = 'Annoncements', isFeaturedArticle__c  = true, Application_Name__c = 'ConneX');
        Employee_Community_News__c quote1 = new Employee_Community_News__c(Name ='test2',  isActive__c = true, Content_Type__c  = 'FAQ', isFeaturedArticle__c  = false , Application_Name__c = 'EITSHome');
        Employee_Community_News__c quote2 = new Employee_Community_News__c(Name ='test3',  isActive__c = true, Content_Type__c  = 'FAQ', isFeaturedArticle__c  = false, Application_Name__c = '');   
        insert featQuote;
        insert quote1;
        insert quote2;             
        
         System.runAs(newUser) {
             expcommunity_Col_Cntrl cntlCall = new expcommunity_Col_Cntrl();
            cntlCall .applicationName='ConneX';
            cntlCall .contentType='Annoncement';
            cntlCall = new expcommunity_Col_Cntrl();
            system.assertequals(cntlCall.getcomptDatasizehasValue(), true);
            
         }
         
         Test.stopTest();
        
    }
    
}