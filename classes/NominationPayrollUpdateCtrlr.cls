/*=============================================================================
 * Experian
 * Name: NominationPayrollUpdateCtrlr
 * Description: 
 * Created Date: 19 Sep 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * Sep 26th, 2016     Paul Kissick          Added support to return exceptions to the user
 * Nov 11th, 2016     Paul Kissick          Reordered report to Employee Number
 * Nov 15th, 2016     Paul Kissick          Fixed problem found in large volume testing with Viewstate error.
 =============================================================================*/

public with sharing class NominationPayrollUpdateCtrlr {
  
  private String reportGuid;
  
  @testVisible private static String defaultLayout = 'Payroll_Report';
  
  public List<Nomination__c> nominationsToUpdate {get; set;}
  
  //===========================================================================
  // Return the FieldSet Payroll_Report for use on the page and the query below.
  //=========================================================================== 
  public List<Schema.FieldSetMember> reportFields {get{
    if (reportFields == null) {
      String reportLayout = defaultLayout;
      if (ApexPages.currentPage().getParameters().containsKey('layout') && String.isNotBlank(ApexPages.currentPage().getParameters().get('layout'))) {
        reportLayout = ApexPages.currentPage().getParameters().get('layout');
      }
      reportFields = SObjectType.Nomination__c.FieldSets.getMap().get(reportLayout).getFields();
    }
    return reportFields;
  } set;}
  
  //===========================================================================
  // Constructor - Load GUID into variable
  //===========================================================================
  public NominationPayrollUpdateCtrlr() {
    if (ApexPages.currentPage().getParameters().containsKey('guid')) {
      reportGuid = (String)ApexPages.currentPage().getParameters().get('guid');
    }
  }
  
  //===========================================================================
  // Update the nominations to reflect the added grossed values.
  //===========================================================================
  public PageReference saveChanges() {
    try {
      update nominationsToUpdate;
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Rewards updated successfully.'));
    }
    catch (Exception e) {
      // Error gets returned to the page anyway....
      system.debug(e.getMessage());
    }
    return null;
  }
  
  //===========================================================================
  // Redirect for the 'Done' button to take them somewhere, currently the home page
  //===========================================================================
  public PageReference finished() {
    return new PageReference('/home/home.jsp');
  }
  
  //===========================================================================
  // Loads the Report from the supplied GUID (page parameter)
  //===========================================================================
  public PageReference loadReport() {
    
    if (String.isNotBlank(reportGuid)) {
      String query = 'SELECT ';
      
      List<String> qFields = new List<String>();
      Boolean grossField = false;
      
      for (Schema.FieldSetMember f : reportFields) {
        qFields.add(f.getFieldPath());
        if (f.getFieldPath().equalsIgnoreCase(Nomination__c.Payroll_Gross_Amount__c.getDescribe().getLocalName())) {
          grossField = true;
        } 
      }
      // Always add the gross amount field.
      if (!grossField) {
        qFields.add(Nomination__c.Payroll_Gross_Amount__c.getDescribe().getLocalName());
      }
      
      nominationsToUpdate = (List<Nomination__c>)Database.query(
        query + String.join(qFields,',') 
         + ' FROM Nomination__c ' 
         + ' WHERE Payroll_Batch_GUID__c = \''+String.escapeSingleQuotes(reportGuid)+'\' '
         // + ' ORDER BY Id ASC '  // Replaced ID ordering with Employee Number (Integer) - New field for ordering this report only.
         + ' ORDER BY Employee_Number_Integer__c ASC '
      );
    }
    return null;
  }
  
  //===========================================================================
  // New method to return the table as dynamic components.
  // An issue was found when testing large volumes where we would get a viewstate
  // error, even though the page was relatively light on fields.
  // Post was found about using Dynamic Components in VF to reduce the 'internal' 
  // viewstate size due to use of repeat on the table.
  // http://salesforce.stackexchange.com/questions/4537/how-to-reduce-a-large-internal-view-state-what-is-in-the-internal-view-state
  //===========================================================================
  public Component.Apex.PageBlockTable getTheTable() {
    Component.Apex.PageBlockTable pbt = new Component.Apex.PageBlockTable();
    pbt.expressions.value = '{!nominationsToUpdate}';
    pbt.var = 'n';
    /*Component.Apex.Column tnc = new Component.Apex.Column();
    tnc.headervalue = 'Number';
    tnc.width = '5%';
    tnc.expressions.value = '{!FLOOR(rowNum)}';
    Component.Apex.Variable rowVar = new Component.Apex.Variable(var = 'rowNum');
    rowVar.expressions.value = '{!rowNum+1}';
    tnc.childComponents.add(rowVar);
    pbt.childComponents.add(tnc);*/
    
    for (Schema.FieldSetMember fsm : reportFields) {
      if (fsm.getRequired()) {
        Component.Apex.Column tc = new Component.Apex.Column();
        tc.headerValue = fsm.getLabel();
        tc.expressions.value = '{!n.'+fsm.getFieldPath()+'}';
        pbt.childComponents.add(tc);
      }
    }
    Component.Apex.Column tgc = new Component.Apex.Column();
    tgc.headerValue = Nomination__c.Payroll_Gross_Amount__c.getDescribe().getLabel();
    Component.Apex.InputField gInp = new Component.Apex.InputField();
    gInp.styleClass = 'payrollGross';
    gInp.expressions.value = '{!n.Payroll_Gross_Amount__c}';
    gInp.onchange = 'grossValueUpdated();';
    tgc.childComponents.add(gInp);
    pbt.childComponents.add(tgc);
    return pbt;
  }

}