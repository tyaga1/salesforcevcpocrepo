/**=====================================================================
 * Experian
 * Name: FailureNotificationUtility
 * Description: Case 01234035 - Requires a defaulting of recipients for failure notifications
 * Created Date: 9 Nov 2015
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update

 =====================================================================*/
 
public with sharing class FailureNotificationUtility {
  
  @TestVisible private static String defaultNoRecipEmail = 'noone@experian.com.never';
  
  //===========================================================================
  // Method to return the default recipients for this failure notification.
  // Params: @string className
  // Return: List<String>
  //===========================================================================
  public static List<String> retrieveRecipients(String className) {
    
    List<String> recips = new List<String>();
    
    List<String> tmpRecips = new List<String>();
    
    if (String.isBlank(className)) {
      return new List<String>{defaultNoRecipEmail};
    }
    
    try {
      if (Batch_Class_Failure_Notifications__c.getAll().containsKey(className)) {
	      Batch_Class_Failure_Notifications__c notif = Batch_Class_Failure_Notifications__c.getValues(className);
	      if (String.isNotBlank(notif.Recipients__c)) {
	        tmpRecips = notif.Recipients__c.split(',');
	      }
      }
    }
    catch (Exception e) {
      system.debug(e.getMessage());
    }
    
    if (tmpRecips.size() == 0) {
      if (Global_Settings__c.getAll().containsKey(Constants.GLOBAL_SETTING)) {
	      Global_Settings__c custSettings = Global_Settings__c.getValues(Constants.GLOBAL_SETTING);
	      if (String.isNotBlank(custSettings.Batch_Failures_Email__c)) {
	        tmpRecips = custSettings.Batch_Failures_Email__c.split(',');
	      }
      }
    }
    
    if (tmpRecips.size() == 0) {
      tmpRecips.add(defaultNoRecipEmail);
    }
    
    for(String r : tmpRecips) {
      recips.add(r.trim());
    }
    
    return recips;
  }
  
}