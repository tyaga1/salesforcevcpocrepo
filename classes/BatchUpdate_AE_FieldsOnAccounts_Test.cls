/**=====================================================================
 * Experian
 * Name: BatchUpdate_AE_FieldsOnAccounts_Test
 * Description: To test the BatchUpdate_AE_FieldsOnAccounts class that updates the BIS and CIS fields on Accounts
 *              when an isDataAdmin user updates then on the AssignmentTeam, thus circumventing the AssignmentTeamTriggerHandler, which
 *              performs this update for normal users.
 * Created Date: Sept. 18th, 2017
 * Created By: James Wills
 *
 * Date Modified                Modified By                  Description of the update
 *
  =====================================================================*/
@isTest
public class BatchUpdate_AE_FieldsOnAccounts_Test {

  private static Account                    testAcc;
  private static Assignment_Team__c         assgnTeam, assgnTeam2;

  //================================================================================
  // Test method to verify AccountTeamMembers and AccountShares are created properly 
  //================================================================================
  public static testmethod void testAddAccAssigTeamMemberToAccTeam(){
  
    List<User> lstUser = [
      SELECT Id, email , UserRole.Name,UserRoleId
      FROM User 
      WHERE IsActive = true 
      AND Email LIKE '%test1234@experian.com'
      ORDER BY email
    ];
    
    User thisUser = [SELECT id FROM User WHERE id =:UserInfo.getUserId()];
    
    //Need to run as an isDataAdmin as the AssignmentTeamTriggerHandler will update the Accounts and we won't get the coverage
    //The class was written to update Accounts when isDataAdmin users changed the AE's so this is valid.
    IsDataAdmin__c ida = new IsDataAdmin__c();
    ida.SetupOwnerId = thisUser.id;
    ida.IsDataAdmin__c = true;
    insert ida;
    
    System.runAs(thisUser){
    
      List<Account> accounts = [SELECT Id,Name FROM Account];
      accounts[0].BIS_Account_Executive__c = lstUser[0].id;//First User
      accounts[0].CIS_Account_Executive__c = lstUser[1].id;//Second User

      update accounts;    
    
      List<Assignment_Team__c> at_List = [SELECT id, Name, Account_Executive__c FROM Assignment_Team__c ORDER By Name];
      for(Assignment_Team__c at : at_List){
        if(at.Name.contains('BIS')){
          at.Account_Executive__c = lstUser[1].id;//Second User
        } else if(at.Name.contains('CIS')){
          at.Account_Executive__c = lstUser[0].id;//First User
        }
      }
      update at_List;
    }
    
    DateTime dt = [SELECT id, Time__c FROM Batch_Class_Timestamp__c].Time__c;
    
    Test.startTest();
      
      BatchUpdate_AE_FieldsOnAccounts b2 = new BatchUpdate_AE_FieldsOnAccounts();
      ID batchedprocessid2 = Database.executeBatch(b2);
      
    Test.stopTest();
        
    
    System.assert(String.valueOf([SELECT id, BIS_Account_Executive__c FROM Account][0].BIS_Account_Executive__c).substring(0,18) ==
                                                                          String.valueOf([SELECT id, Account_Executive__c 
                                                                          FROM Assignment_Team__c 
                                                                          WHERE Name LIKE 'BIS%'].Account_Executive__c).substring(0,18),
                                                                         'Batch class did not update Account Executive for BIS Assignment Team.');

    System.assert(String.valueOf([SELECT id, CIS_Account_Executive__c FROM Account][0].CIS_Account_Executive__c).substring(0,18) ==
                                                                          String.valueOf([SELECT id, Account_Executive__c 
                                                                          FROM Assignment_Team__c 
                                                                          WHERE Name LIKE 'CIS%'].Account_Executive__c).substring(0,18),
                                                                         'Batch class did not update Account Executive for CIS Assignment Team.');                                             
                                                                         
    
  }

  
  //==========================================================
  // Creates test data: Assignment_Team__c & members
  // This method is also used by BatchAssignmentTeam_Test.cls
  //==========================================================
  @testSetup
  static void createTestData() {
    // create test data
    User thisUser = [SELECT id FROM User WHERE id =:UserInfo.getUserId()];
    
    // create users
    Profile p = [SELECT Id FROM Profile WHERE Name=: Constants.PROFILE_SYS_ADMIN];
    List<User> lstUser = Test_Utils.createUsers(p, 'test1234@experian.com', 'T-AE', 2);
    insert lstUser;
    
    
    System.runAs(thisUser){
      // create accounts
      testAcc = Test_Utils.createAccount();
      testAcc.ownerId = userinfo.getuserId();   
    
      List<Account> accountList = new List<Account>();    
      accountList.add(testAcc);
      insert accountList;

      // create assignment teams
      assgnTeam = Test_Utils.insertAssignmentTeam(false, lstUser.get(0).Id);//Make the first user the AE of the Assignment Team
      assgnTeam.ownerId = userinfo.getuserId();
    
      assgnTeam2 = Test_Utils.insertAssignmentTeam(false, lstUser.get(1).Id);//InsideSales:W-007994
      assgnTeam2.ownerId = userinfo.getuserId();

      List<Assignment_Team__c> assignmentTeams = new List<Assignment_Team__c>();
      assignmentTeams.add(assgnTeam);
      assignmentTeams.add(assgnTeam2);

      assgnTeam.Name  = 'BIS - Growth';//InsideSales:W-007994
      assgnTeam2.Name = 'CIS - Growth';//InsideSales:W-007994

      insert assignmentTeams;
    
      createAccountAssignmentTeams();
    
    
      Batch_Class_Timestamp__c bc = new Batch_Class_Timestamp__c();
      bc.Name    = 'BatchUpdate_AE_FieldsOnAccountsLastRun';
      bc.Time__c =  System.now()-1;
      insert bc;
    }
  }

  //==========================================================
  // CreateAccountAssignmentTeams
  //==========================================================
  public static void createAccountAssignmentTeams() {
    
    List<Account_Assignment_Team__c> accountAssignTeamList= new List<Account_Assignment_Team__c>();
    Account_Assignment_Team__c accAssgnTeam1 = Test_utils.insertAccAssignmentTeam(false, assgnTeam.Id,  testAcc.Id);
    Account_Assignment_Team__c accAssgnTeam2 = Test_utils.insertAccAssignmentTeam(false, assgnTeam2.Id, testAcc.Id);

    accountAssignTeamList.add(accAssgnTeam1);
    accountAssignTeamList.add(accAssgnTeam2);

    insert accountAssignTeamList;

  }

  
}