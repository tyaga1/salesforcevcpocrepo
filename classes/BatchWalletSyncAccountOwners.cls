/**=====================================================================
 * Experian
 * Name: BatchWalletSyncAccountOwners
 * Description: Take the updates to WalletSync and reset the account ownership where required.
 *              On the WalletSync__c object, CNPJ_Number__c is the root number. We need this to identify
 *              all relavent accounts to update (e.g. branches)
 *
 *    NOTE: BatchWalletSyncUserLookups must run before this batch as it prepares this batch!
 *
 * Created Date: 6 Aug 2015
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 * Aug 28th, 2015               Paul Kissick                 Added Previous Account Supervisor to the processing
 * Sep 1st, 2015                Paul Kissick                 Replaced constants with Custom Labels.
 * Sep 10th, 2015               Paul Kissick                 Added try/catch around email sending
 * Sep 29th, 2015               Paul Kissick                 Changing the next step at the end to fire Segment fixes based on new/changed Account Team Members
 * Oct 1st, 2015                Paul Kissick                 Changing lastStartDate to Wallet_Sync_Processing_Last_Run__c
 * Nov 9th, 2015                Paul Kissick                 Case 01234035: Adding FailureNotificationUtility
 * Nov 27th, 2015               Paul Kissick                 Case 01266075: Replacing Global_Setting__c with new Batch_Class_Timestamp__c
 * Jun 15th, 2016               Paul Kissick                 Case 02024883: Fixing issue due to changes to AccountTeamMember object (v37.0)
 =====================================================================*/
global class BatchWalletSyncAccountOwners implements Database.Batchable<sObject>, Database.Stateful  {
  
  private static String GLOBAL_SETTING = 'Global';
  @testVisible private static String TEAM_ROLE_ACCOUNT_MANAGER = System.Label.WalletSync_Account_Owner_Role; // 'Account Manager'; // These are needed as constants can't be updated when batches are scheduled
  @testVisible private static String TEAM_ROLE_STRAT_ACCOUNT_MANAGER = System.Label.WalletSync_Account_Supervisor_Role; // 'Strategic Account Manager';
  private static String ACCESS_LEVEL_EDIT = 'Edit';
  private static String ACCESS_LEVEL_READ = 'Read';
  private static String ACCESS_LEVEL_NONE = 'None';
  
  global List<String> deleteErrors;
  global List<String> insertErrors;
  
  global Database.QueryLocator start(Database.BatchableContext BC) {
    deleteErrors = new List<String>();
    insertErrors = new List<String>();
    
    Datetime lastStartDate = WalletSyncUtility.getWalletSyncProcessingLastRun();
    
    return Database.getQueryLocator([
      SELECT Id, Account__c, 
       Account_Owner__c, Account_Owner__r.IsActive, Previous_Account_Owner__c,
       Account_Supervisor__c, Account_Supervisor__r.IsActive, Previous_Account_Supervisor__c
      FROM WalletSync__c
      WHERE Last_Processed_Date__c >= :lastStartDate
      AND Account__c != null
    ]);
  }
  
  global void execute(Database.BatchableContext BC, List<WalletSync__c> wsObjects){
    
    Map<Id, Id> accIdToAccountOwnerId = new Map<Id, Id>();
    Map<Id, Id> accIdToPreviousAccountOwnerId = new Map<Id, Id>();
    Map<Id, Id> accIdToAccountSupervisorId = new Map<Id, Id>();
    Map<Id, Id> accIdToPreviousAccountSupervisorId = new Map<Id, Id>();
    
    Set<Id> accountIdSet = new Set<Id>();
    
    for (WalletSync__c ws : wsObjects) {
      // If the account owner is not null and is active, add this to the account...
      if (ws.Account_Owner__c != null && ws.Account_Owner__r.IsActive) {
        accIdToAccountOwnerId.put(ws.Account__c,ws.Account_Owner__c);
	      // If the previous is not null, and there is a new owner, remove the old one...
	      // Also check the previous owner isn't the same as the new owner.
	      if (ws.Previous_Account_Owner__c != null && ws.Previous_Account_Owner__c != ws.Account_Owner__c) {
	        accIdToPreviousAccountOwnerId.put(ws.Account__c,ws.Previous_Account_Owner__c);
	      }
      }
      // Add the account supervisor
      if (ws.Account_Supervisor__c != null && ws.Account_Supervisor__r.IsActive) {
        accIdToAccountSupervisorId.put(ws.Account__c,ws.Account_Supervisor__c);
        if (ws.Previous_Account_Supervisor__c != null && ws.Previous_Account_Supervisor__c != ws.Account_Supervisor__c) {
          accIdToPreviousAccountSupervisorId.put(ws.Account__c,ws.Previous_Account_Supervisor__c);
        }
      }
      accountIdSet.add(ws.Account__c);
    }
    
    // Now find the account owners (accountteammember) to add on from account owner, and account supervisor too.
    // TEAM_ROLE_ACCOUNT_MANAGER
    // TEAM_ROLE_STRAT_ACCOUNT_MANAGER
    
    // Note: It is possible that removing the current account owner will leave them with another role, 
    //  so this shouldn't delete the share, but should delete the member record..
    Map<Id, List<AccountTeamMember>> accountIdToTeamMembers = new Map<Id, List<AccountTeamMember>>();

    // Pull back all the accounts we need, then loop to map cnpj to set of ids
    for (Account a : [SELECT Id,CNPJ_Number__c,
                        (SELECT Id, UserId, User.IsActive, TeamMemberRole 
                         FROM AccountTeamMembers)
                      FROM Account
                      WHERE Id IN :accountIdSet]) {
      // Build the map of current account members by account
      accountIdToTeamMembers.put(a.Id,a.AccountTeamMembers);
    }
    
    List<AccountTeamMember> newAccountTeamMembers = new List<AccountTeamMember>();
    List<AccountShare> newAccountShares = new List<AccountShare>();
    
    Set<Id> accountIdsToCleanSharesOwnerSet = new Set<Id>(); // This set is used to remove current 'Account Managers'
    Set<Id> accountIdsToCleanSharesSupervisorSet = new Set<Id>(); // This set is used to remove current 'Account Supervisors'
    
    List<Id> accountTeamIdsToDelete = new List<Id>();
    
    for(Id accId : accountIdSet) {
      
      // now see if the account owner needs to be added 
      if (accIdToAccountOwnerId.containsKey(accId)) {
        // okay, take each account and create a team member record for each account, will also need shares...
        newAccountTeamMembers.addAll(createAccountTeamMembers(accIdToAccountOwnerId.get(accId),new Set<Id>{accId},TEAM_ROLE_ACCOUNT_MANAGER));
        // newAccountShares.addAll(createAccountShares(accIdToAccountOwnerId.get(accId),new Set<Id>{accId}));
      }
      
      // Add the new account owner
      if (accIdToAccountSupervisorId.containsKey(accId)) {
        // okay, take each account and create a team member record for each account, will also need shares...
        newAccountTeamMembers.addAll(createAccountTeamMembers(accIdToAccountSupervisorId.get(accId),new Set<Id>{accId},TEAM_ROLE_STRAT_ACCOUNT_MANAGER));
        // newAccountShares.addAll(createAccountShares(accIdToAccountSupervisorId.get(accId),new Set<Id>{accId}));
      }
      
      // Remove the Previous Account Owner
      if (accIdToPreviousAccountOwnerId.containsKey(accId)) {
        Id prevAccountOwnerId = accIdToPreviousAccountOwnerId.get(accId);
        accountIdsToCleanSharesOwnerSet.add(accId);
        // We need to check the prev owner isn't on the account for another reason...
	      if (accountIdToTeamMembers.containsKey(accId) && accountIdToTeamMembers.get(accId) != null && accountIdToTeamMembers.get(accId).size() > 0) {
	        for (AccountTeamMember atm : accountIdToTeamMembers.get(accId)) {
	          if (atm.UserId == prevAccountOwnerId && atm.TeamMemberRole != TEAM_ROLE_ACCOUNT_MANAGER) {
	            // Previous owner also has another role on this account, so remove it from the set to clear the account shares...
	            accountIdsToCleanSharesOwnerSet.remove(accId);
	          }
	          // While we're here, save the ID for the account owner account team member to delete later...
	          if (atm.UserId == prevAccountOwnerId && atm.TeamMemberRole == TEAM_ROLE_ACCOUNT_MANAGER) {
	            accountTeamIdsToDelete.add(atm.Id);
	          }
	        }
	      }
      }
      
      // Remove previous account supervisor
      if (accIdToPreviousAccountSupervisorId.containsKey(accId)) {
        Id prevAccountSupervisorId = accIdToPreviousAccountSupervisorId.get(accId);
        accountIdsToCleanSharesSupervisorSet.add(accId);
        // We need to check the prev supervisor isn't on the account for another reason...
        if (accountIdToTeamMembers.containsKey(accId) && accountIdToTeamMembers.get(accId) != null && accountIdToTeamMembers.get(accId).size() > 0) {
          for (AccountTeamMember atm : accountIdToTeamMembers.get(accId)) {
            if (atm.UserId == prevAccountSupervisorId && atm.TeamMemberRole != TEAM_ROLE_STRAT_ACCOUNT_MANAGER) {
              // Previous owner also has another role on this account, so remove it from the set to clear the account shares...
              accountIdsToCleanSharesSupervisorSet.remove(accId);
            }
            // While we're here, save the ID for the account owner account team member to delete later...
            if (atm.UserId == prevAccountSupervisorId && atm.TeamMemberRole == TEAM_ROLE_STRAT_ACCOUNT_MANAGER) {
              accountTeamIdsToDelete.add(atm.Id);
            }
          }
        }
      }
    }
    
    // before we do any updates, remove the old ones, in case the updates include the previous owner and they are changed to strat owner
    
    List<Database.DeleteResult> delTeamsRes = Database.delete(accountTeamIdsToDelete,false);
    for (Database.DeleteResult dr : delTeamsRes) {
      if (!dr.isSuccess()) {
        for (Database.Error err : dr.getErrors()) {
          deleteErrors.add(err.getMessage());
        }
      }
    }
        
    List<Database.DeleteResult> delSharesRes = Database.delete([
      SELECT Id 
      FROM AccountShare 
      WHERE (
        AccountId IN :accountIdsToCleanSharesOwnerSet 
        AND UserOrGroupId IN :accIdToPreviousAccountOwnerId.values() 
      ) OR (
        AccountId IN :accountIdsToCleanSharesSupervisorSet
        AND UserOrGroupId IN :accIdToPreviousAccountSupervisorId.values() 
      )
    ],false);
    
    for (Database.DeleteResult dr : delSharesRes) {
      if (!dr.isSuccess()) {
        for (Database.Error err : dr.getErrors()) {
          deleteErrors.add(err.getMessage());
        }
      }
    }
    
    // Finally, insert the new records
    
    List<Database.SaveResult> addAccTeamRes = Database.insert(newAccountTeamMembers, false);
    for (Database.SaveResult sr : addAccTeamRes) {
      if (!sr.isSuccess()) {
        for (Database.Error err : sr.getErrors()) {
          insertErrors.add(err.getMessage());
        }
      }
    }
    
    List<Database.SaveResult> addAccShareRes = Database.insert(newAccountShares, false);
    for (Database.SaveResult sr : addAccShareRes) {
      if (!sr.isSuccess()) {
        for (Database.Error err : sr.getErrors()) {
          insertErrors.add(err.getMessage());
        }
      }
    }
  }
  
  private List<AccountTeamMember> createAccountTeamMembers(Id uId, Set<Id> accountIds, String role) {
    List<AccountTeamMember> atmList = new List<AccountTeamMember>();
    for (Id accId : accountIds) {
      atmList.add(
        new AccountTeamMember(
          UserId = uId,
          AccountId = accId,
          TeamMemberRole = role,
          AccountAccessLevel = ACCESS_LEVEL_EDIT,
          OpportunityAccessLevel = ACCESS_LEVEL_EDIT,  // Check this level is appropriate
          CaseAccessLevel = ACCESS_LEVEL_NONE
        )
      );
    }
    return atmList;
  }
  
  /*
  private List<AccountShare> createAccountShares(Id uId, Set<Id> accountIds) {
    List<AccountShare> aShareList = new List<AccountShare>();
    for(Id accId : accountIds) {
      aShareList.add(new AccountShare(
        UserOrGroupId = uId,
        AccountId = accId,
        AccountAccessLevel = ACCESS_LEVEL_EDIT,
        OpportunityAccessLevel = ACCESS_LEVEL_EDIT,  // Check this level is appropriate
        CaseAccessLevel = ACCESS_LEVEL_NONE
      ));
    }
    return aShareList;
  }
  */
  
  global void finish(Database.BatchableContext BC){
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(BC.getJobId(), 'BatchWalletSyncAccountOwners', false);
	    
    if (!deleteErrors.isEmpty() || !insertErrors.isEmpty()) {
      bh.batchHasErrors = true;
      if (!insertErrors.isEmpty()) {
        bh.emailBody += '\nThe following errors were observed when inserting records:\n';
        bh.emailBody += String.join(insertErrors,'\n');
      }
      if (!deleteErrors.isEmpty()) {
        bh.emailBody += '\nThe following errors were observed when deleting records:\n';
        bh.emailBody += String.join(deleteErrors,'\n');
      }
    }
	    
    bh.sendEmail();
    
    if (!Test.isRunningTest()) { // Must be within this check as tests cannot start other batches
      // THIS BATCH, WHEN FINISHED, STARTS THE NEXT BATCH OF OPPORTUNITY OWNERSHIP
      // Moving this to BatchAccountSegmentCreationViaATM
      BatchAccountSegmentCreationViaATM fixSegments = new BatchAccountSegmentCreationViaATM();
      fixSegments.runningWalletSync = true;
      system.scheduleBatch(fixSegments, 'BatchAccountSegmentCreationViaATM'+String.valueOf(Datetime.now().getTime()),0,ScopeSizeUtility.getScopeSizeForClass('BatchAccountSegmentCreationViaATM'));
    }
    
  }
  
  
}