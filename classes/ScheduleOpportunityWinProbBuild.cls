/*=============================================================================
 * Experian
 * Name: ScheduleOpportunityWinProbBuild
 * Description: W-005423: Schedule class for BatchOpportunityWinProbBuild
 *              Run monthly, e.g. 2nd of the month, every month.          
 * Created Date: 12 Jul 2016
 * Created By: Paul Kissick
 *
 * Date Modified       Modified By           Description of the update
 * 12th July 2016(QA)  Paul Kissick          CRM20:W-005423: Schedule class for BatchOpportunityWinProbBuild   
 =============================================================================*/

global class ScheduleOpportunityWinProbBuild implements Schedulable {

  global void execute(SchedulableContext sc) {
    BatchOpportunityWinProbBuild b = new BatchOpportunityWinProbBuild(); 
    Database.executeBatch(b, ScopeSizeUtility.getScopeSizeForClass('BatchOpportunityWinProbBuild')); 
  }
  
}