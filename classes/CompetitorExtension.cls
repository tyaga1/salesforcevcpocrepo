public class CompetitorExtension
{
    private final Competitor__c competitor;
    private ApexPages.StandardController controller;
    
    public CompetitorExtension(ApexPages.StandardController stdController)
    {
        this.controller = stdController;
        this.competitor = (Competitor__c) stdController.getRecord();   
        
        String opportunityId = ApexPages.currentPage().getParameters().get('oid');
        this.competitor.Opportunity__c = opportunityId;

		this.competitor.Primary_Competitor__c = getPrimaryCompetitor();        
    }
    
    public Boolean getPrimaryCompetitor()
    {
        Boolean returnValue = false;
        
        if(String.isEmpty(this.competitor.Opportunity__c))
        {
            returnValue = false;
        }
        else
        {
            Integer cntr = [SELECT COUNT() FROM COMPETITOR__c Where Opportunity__c =: this.competitor.Opportunity__c];
            
            if(cntr > 0) 
            {
                returnValue = false;
            }
            else
            {
                returnValue = true;
            }
        }
        
        return returnValue;
    }

    public PageReference saveAndReturnToOpp()
    {
        controller.save();
        PageReference oppPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL')); 
        oppPage.setRedirect(true);
        return oppPage;
    }
}