@isTest
private class StratComm_CaseCommentList_Test {
    
    @isTest static void getCaseCommentsTest() {
        Account testAccount1 = Test_Utils.createAccount();
        insert testAccount1;
 
        Case c1 = new Case(
                        AccountId = testAccount1.Id,
                        Subject = 'Test Case 1',
                        Description = 'Test Description 1',
                        status = 'Closed-Complete');

        insert c1;

        CaseComment cc1 = new CaseComment(
                        CommentBody='test comment', 
                        isPublished=true,
                        ParentId=c1.Id);

        insert cc1;
        
        Attachment testAttachment = new Attachment();
    testAttachment.ParentId = c1.id;
    testAttachment.Name = 'test';
    testAttachment.Body = Blob.valueof('test');
    insert testAttachment;
    

        StratComm_CaseCommentList_Controller.findCaseComments(c1.Id);
        StratComm_CaseCommentList_Controller.checkIfCaseIsClosed(c1.Id);
        
        StratComm_CaseAttachmentList_Controller.checkIfCaseIsClosed(c1.id); 
        StratComm_CaseAttachmentList_Controller.findCaseAttachments(c1.id);
        
    }
    
    
}