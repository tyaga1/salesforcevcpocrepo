/*=============================================================================
 * Experian
 * Name: CaseTriggerHandler_Entitlements
 * Description: Case 01794099 - Separate class to handle entitlements on cases.
 * Created Date: 29 Apr 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * Aug 12th, 2016     Paul Kissick          Case 01985974 - Adding Entitlement Process Name to Case
 * Nov 3rd, 2016      Paul Kissick          Case 02195991 - Adding check for future/batch
 =============================================================================*/

public class CaseTriggerHandler_Entitlements {
  
  public static Boolean executedEntitlementRelatedFields = false;
  
  //===========================================================================
  // addActiveEntitlements - Case 01794099
  // Find entitlements on Accounts and attach to cases if one exists. 
  // Added support for pulling the Business Hours from the entitlement onto the case
  //===========================================================================
  public static void addActiveEntitlements (List<Case> newList, Map<Id, Case> oldMap) {
    
    Map<Id, Id> accountToEntitlementMap = new Map<Id, Id> ();
    
    for (Case c : newList) {
      // Account is known, but entitlement is empty.
      if (c.AccountId != null && c.EntitlementId == null) {
        accountToEntitlementMap.put(c.AccountId, null);
      }
    }
    
    // Leave if nothing more to do
    if (accountToEntitlementMap.isEmpty()) {
      return;
    }
    
    // Only return active entitlements for the accounts found.
    Map<Id, Entitlement> activeEntitlementMap = new Map<Id, Entitlement>([
      SELECT Id, AccountId, BusinessHoursId
      FROM Entitlement
      WHERE AccountId IN :accountToEntitlementMap.keySet()
      AND AssetId = null
      AND Status = 'Active'
      ORDER BY CreatedDate DESC
    ]);
    
    // Leave if nothing found.
    if (activeEntitlementMap.isEmpty()) {
      return;
    }
    
    for (Entitlement ent : activeEntitlementMap.values()) {
      accountToEntitlementMap.put(ent.AccountId, ent.Id);
    }
    
    for (Case c : newList) {
      if (c.AccountId != null && c.EntitlementId == null && accountToEntitlementMap.containsKey(c.AccountId)) {
        c.EntitlementId = accountToEntitlementMap.get(c.AccountId);
        if (c.EntitlementId != null && activeEntitlementMap.get(c.EntitlementId).BusinessHoursId != null) {
          c.BusinessHoursId = activeEntitlementMap.get(c.EntitlementId).BusinessHoursId;
        }
        system.debug('** Assigning entitlement ' + c.EntitlementId + ' to case ' + c.Id);
      }
    }
  }
  
  //===========================================================================
  // addEntitlementRelatedFields - Case 01794099
  // Add related fields from entitlement to case record, when entitlement is applied. 
  //===========================================================================
  public static void addEntitlementRelatedFields (List<Case> newList, Map<Id, Case> oldMap) {
        
    if (executedEntitlementRelatedFields) {
      return;
    }
    
    Map<Id, Id> caseToEntitlementMap = new Map<Id, Id>();
    
    for (Case c : newList) {
      if ((oldMap == null && c.EntitlementId != null) || 
          (oldMap != null && oldMap.get(c.Id).EntitlementId != c.EntitlementId)) {
        if (c.EntitlementId == null) {
          // clear the fields.
          c.EMS_Customer_Type__c = null;
          c.Entitlement_Escalation_Manager__c = null;
          c.Entitlement_Process_Name__c = null;
        }
        else {
          caseToEntitlementMap.put(c.Id, c.EntitlementId);
        }
      }
    }
    
    if (caseToEntitlementMap.isEmpty()) {
      return;
    }
    
    Map<Id, Entitlement> entitlementMap = new Map<Id, Entitlement>([
      SELECT Id, Escalation_Manager__c, EMS_Customer_Type__c, SlaProcess.Name
      FROM Entitlement
      WHERE Id IN :caseToEntitlementMap.values()
    ]);
    
    if (entitlementMap.isEmpty()) {
      return;
    }
    
    Set<Id> caseIdSet = new Set<Id>();
    
    for (Case c : newList) {
      if (caseToEntitlementMap.containsKey(c.Id)) {
        executedEntitlementRelatedFields = true;
        
        Entitlement ent = entitlementMap.get(caseToEntitlementMap.get(c.Id));
        
        c.Entitlement_Escalation_Manager__c = ent.Escalation_Manager__c;
        c.EMS_Customer_Type__c = ent.EMS_Customer_Type__c;
        c.Entitlement_Process_Name__c = ent.SlaProcess.Name;
        caseIdSet.add(c.Id);
      }
    }
    
    if (!caseIdSet.isEmpty()) {
      // Case 02195991 - Only run when not in future or batch call
      if (!system.isBatch() && !system.isFuture()) {
        asyncSetTargetResolutionFromEntitlement(caseIdSet);
      }
    }
  }
  
  //===========================================================================
  // setCaseMilestonesIfClosed - Case 01794099
  // Used by Entitlements to set Completed on any still open milestones. Needed 
  // because Salesforce automatically deletes any open milestones when a case
  // exits the entitlement process (is closed)
  //===========================================================================
  public static void setCaseMilestonesIfClosed(List<Case> newList, Map<Id,Case> oldMap) {
    
    Set<Id> caseIdSet = new Set<Id>();
    Datetime completionDate = system.now();
    Set<String> closedStatuses = CaseTriggerHandler.getClosedStatusSet();
    
    for (Case c : newList) {
      if (closedStatuses.contains(c.Status) && 
          !closedStatuses.contains(oldMap.get(c.Id).Status) && 
          c.Status != oldMap.get(c.Id).Status && 
          c.SlaStartDate <= completionDate && 
          c.SlaExitDate == null &&
          c.EntitlementId != null) {
        caseIdSet.add(c.Id);
      }
    }
    
    // Leave if no cases are found to update.
    if (caseIdSet.isEmpty()) {
      return;
    }
    List<CaseMilestone> caseMilestonesToUpd = [
      SELECT Id, CompletionDate
      FROM CaseMilestone
      WHERE CaseId IN :caseIdSet
      AND CompletionDate = null
    ];
    if (!caseMileStonesToUpd.isEmpty()) {
      for (CaseMilestone cm : caseMileStonesToUpd) {
        cm.CompletionDate = completionDate;
      }
      try {
        update caseMileStonesToUpd;
      }
      catch (DMLException ex) {
        ApexLogHandler.createLogAndSave('CaseTriggerHandler', 'setCaseMilestonesIfClosed', ex.getStackTraceString(), ex);
      }
    }
  }
  
  //===========================================================================
  // asyncSetTargetResolutionFromEntitlement - Case 01794099
  // Pulls the target resolution time from the case milestones onto the case.
  // In future since this caused a race condition when adding to normal
  // execution.
  //===========================================================================
  @future
  public static void asyncSetTargetResolutionFromEntitlement (Set<Id> caseIdSet) {
    if (caseIdSet == null || (caseIdSet != null && caseIdSet.isEmpty())) {
      return;
    }
    List<Case> updateCaseList = new List<Case>();
    for (Case c : [SELECT Id, Target_Resolution_Time__c,
                    (SELECT Id, TargetDate, MilestoneType.Name 
                     FROM CaseMilestones
                     WHERE MilestoneType.Name = :EntitlementMilestoneResolutionTime.RESOLUTION_TIME) 
                   FROM Case WHERE Id IN :caseIdSet]) {
      if (c.CaseMilestones != null && !c.CaseMilestones.isEmpty() && c.CaseMilestones.size() == 1) {
        CaseMilestone cmRes = c.CaseMilestones[0];
        updateCaseList.add(
          new Case(
            Id = c.Id, 
            Target_Resolution_Time__c = cmRes.TargetDate
          )
        );
      }
    }
    try {
      update updateCaseList;
    }
    catch (DMLException ex) {
      apexLogHandler.createLogAndSave('CaseTriggerHandler', 'asyncSetTargetResolutionFromEntitlement', ex.getStackTraceString(), ex);
    }
  }
  
}