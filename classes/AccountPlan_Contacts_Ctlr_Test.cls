/**=====================================================================
 * Name: AccountPlan_Contacts_Ctlr_Test
 * Description: See Case #01949954 AP - List views
 * Created Date: May 26th, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By           Description of the update
 * May 26th, 2016        James Wills           Case #01949954 AP - List views: Created
 *
  =====================================================================*/
@isTest
private Class AccountPlan_Contacts_Ctlr_Test{
  
  private static testMethod void testAccountPlanContactListAdditionAndDeletion(){
    
    Integer sizeOfAccountPlanContactList = 0;    
    
    List<Account_Plan__c> newAccountPlanList = [SELECT id, Name FROM Account_Plan__c];
    
    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)newAccountPlanList[0]);       
    AccountPlan_Contacts_Controller controller = new AccountPlan_Contacts_Controller(stdController);  
     
    Test.startTest();

      //1      
      controller.viewAccountPlanContactListFull();
      controller.buildAccountPlanContactsWrapperList();
      List<Account_Plan_Contact__c> apcList = [SELECT id FROM Account_Plan_Contact__c LIMIT 10];
      sizeOfAccountPlanContactList= apcList.size();      
      system.assert(sizeOfAccountPlanContactList == controller.accountPlanContactsWrapperList.size(), 'AccountPlan_Contacts_Ctlr_Test: The accountPlanContactsWrapperList list was not built properly: ' 
                   + sizeOfAccountPlanContactList + ' ' + controller.accountPlanContactsWrapperList.size());    
      
      //2
      controller.newAccountPlanContact();
      controller.buildAccountPlanContactsWrapperList(); 
      apcList = [SELECT id FROM Account_Plan_Contact__c LIMIT 10];
      sizeOfAccountPlanContactList= apcList.size();
      system.assert(sizeOfAccountPlanContactList == controller.accountPlanContactsWrapperList.size(), 'AccountPlan_Contacts_Ctlr_Test: There was a problem with the method newAccountPlanContact(): ' 
                   + sizeOfAccountPlanContactList + ' ' + controller.accountPlanContactsWrapperList.size());    
      //3
      controller.selectedIDToDelete=apcList[0].id;
      controller.doDeleteAccountPlanContact();
      controller.buildAccountPlanContactsWrapperList();
      apcList = [SELECT id FROM Account_Plan_Contact__c LIMIT 10];
      sizeOfAccountPlanContactList= apcList.size();
      system.assert(sizeOfAccountPlanContactList == controller.accountPlanContactsWrapperList.size(), 'AccountPlan_Contacts_Ctlr_Test: There was a problem with the method doDeleteAccountPlanContact(): ' 
                   + sizeOfAccountPlanContactList + ' ' + controller.accountPlanContactsWrapperList.size());    

      //4  
      sizeOfAccountPlanContactList= apcList.size();          
      controller.seeMoreContacts();
      controller.buildAccountPlanContactsWrapperList();

      //apcList = [SELECT id FROM Account_Plan_Contact__c LIMIT 10];
      //sizeOfAccountPlanContactList= apcList.size();
      system.assert(sizeOfAccountPlanContactList + (Integer)Account_Plan_Related_List_Sizes__c.getInstance().Account_Plan_Contacts_List__c == controller.accountPlanContactsWrapperList.size(), 'AccountPlan_Contacts_Ctlr_Test: There was a problem with the method seeMoreContacts(): ' 
                   + sizeOfAccountPlanContactList + ' ' + controller.accountPlanContactsWrapperList.size() + ' ' + controller.contactsLimit + ' ' + controller.additionalContactsToView);  
                     
    Test.stopTest();
    
    
  }

  @testSetup
  private static void setupTestData(){

    Account testAccount1 = Test_Utils.insertAccount();  
    Account testAccount2 = Test_Utils.insertAccount();  
    testAccount2.Ultimate_Parent_Account__c = testAccount1.id;
    update testAccount2;
    
    Account_Plan__c newAccountPlan = new Account_Plan__c(Account__c = testAccount1.id);
    insert newAccountPlan;

    List<Account_Plan_Contact__c> apcList = new List<Account_Plan_Contact__c>();
    Integer i=0;
    for(i=0;i<25;i++){
      Account_Plan_Contact__c apc1 = new Account_Plan_Contact__c(Account_Plan__c=newAccountPlan.id);
      apcList.add(apc1);
    }        
    insert apcList;
    
    Custom_Fields_Ids__c customFields = new Custom_Fields_Ids__c(
      AP_Account_Plan_Contact_Rec__c = 'a10',
      AP_Account_Plan_Contact__c = 'CF00Ni000000EMhX6'
    );
    insert customFields;
  
    Account_Plan_Related_List_Sizes__c custListSizes = new Account_Plan_Related_List_Sizes__c(
      Account_Plan_Contacts_List__c = 10
    );
    insert custListSizes;
  }
  
  
}