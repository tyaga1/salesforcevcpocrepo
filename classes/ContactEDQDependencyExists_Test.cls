/**********************************************************************************************
 * Experian 
 * Name         : ContactEDQDependencyExists_Test
 * Created By   : Diego Olarte (Experian)
 * Purpose      : Test class of scheduler class "ScheduleEDQDependencyValidation01" & ContactEDQDependencyExists
 * Created Date : September 7th, 2015
 *
 * Date Modified                Modified By                 Description of the update
 * Nov 11th, 2015               Paul Kissick                Case 01266075: Removed bad global setting entry
***********************************************************************************************/

@isTest
private class ContactEDQDependencyExists_Test {

    static testMethod void testSchedulable() {
        test.startTest();
            ContactEDQDependencyExists obj=new ContactEDQDependencyExists();
        test.stopTest();
    }
    
  /*static testMethod void testSchedulable() {
      
    test.startTest();
    // Schedule the test job
    String CRON_EXP = '0 0 0 * * ?';
    String jobId = System.schedule('EDQ Dependency Hly Processor test', CRON_EXP, new ScheduleEDQDependencyValidation01());
      
    // Get the information from the CronTrigger API object  
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime
                      FROM CronTrigger 
                      WHERE id = :jobId];     
    test.stopTest();
    // Verify the expressions are the same  
    System.assertEquals(CRON_EXP, ct.CronExpression);
    // Verify the job has not run  
    System.assertEquals(0, ct.TimesTriggered);
  }
    
    
  private static testMethod void testContactEDQDependency() {
    Test.startTest();
    ContactEDQDependencyExists batchToProcess = new ContactEDQDependencyExists();
    database.executebatch(batchToProcess,10);
    Test.stopTest();
    system.assertEquals(1,[SELECT COUNT() FROM Contact WHERE EDQ_On_Demand__c = true]);
  }
  
  @testSetup
  private static void setupData() {
       
    Account tstAcc = Test_utils.insertEDQAccount(true);
    tstAcc.EDQ_Dependency_Exists__c = false;
                
    update new List<Account>{tstAcc};
    
    Contact tstCon = Test_utils.insertEDQContact(tstAcc.ID,true);
        
    update new List<Contact>{tstCon};
    
  }*/
  
}