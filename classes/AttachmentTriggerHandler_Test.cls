/**=====================================================================
 * Experian
 * Name: AttachmentTriggerHandler_Test
 * Description: Tests for Handler for Attachment Trigger
 * Created Date: Oct 28th, 2015
 * Created By: Paul Kissick 
 * 
 * Date Modified      Modified By                Description of the update
 * 22nd June 2016     Richard Joseph             Removing the assert and making the methods obsolete
 * Feb  01st 2017     Manoj Gopu                 ITSM: W-006704: Added new Test method testCaseAttachment() to improve the Code Coverage
 =====================================================================*/
@isTest
private class AttachmentTriggerHandler_Test {

  static testMethod void dealAttachmentTest() {
    
   
    String textFile = 'Here is some text to test with. It should be safely encrypted and when decrypted, will be the same as this!';
    
    Account acc = Test_Utils.insertAccount();
    
    Deal__c deal = new Deal__c(Project_Name__c = 'Test Project Name');
    insert deal;
    
        
    Attachment att1 = new Attachment(ParentId = deal.Id, Name = 'Test Att.txt', ContentType = 'text/plain', Body = Blob.valueOf(textFile));
    Attachment att2 = new Attachment(ParentId = acc.Id, Name = 'Test Att Account.txt', ContentType = 'text/plain', Body = Blob.valueOf('Some text here'));
    insert new List<Attachment>{att1,att2};
    
    //RJ - Commented below
    //system.assertNotEquals(Blob.valueOf(textFile),[SELECT Body FROM Attachment WHERE Id = :att1.Id].Body);
    
    //system.assertNotEquals(null, [SELECT Encryption_Key__c FROM Deal__c WHERE Id = :deal.Id].Encryption_Key__c);
    
  }
  
  static testMethod void testNormalAttachment() {
    
    Account a = Test_Utils.insertAccount();
    
    Attachment at1 = new Attachment(ParentId = a.Id, Body = Blob.valueOf('TEST'), Name = 'Attachment.txt');
    insert at1;
    
    system.assertEquals(1,[SELECT COUNT() FROM Attachment WHERE ParentId = :a.Id]);
  }
  
   static testMethod void testCaseAttachment() {
      Test.startTest();
      //create 1 account
      Account acc1 = Test_Utils.insertAccount();
      //create contact
      Contact contact1 = Test_Utils.insertContact(acc1.Id);
      ESDEL_Delivery_Project__c deliveryProject = Test_Utils.insertDeliveryProject(true, 'project1', acc1.id, contact1.id, 'bord1');
       
      Case newCase = Test_Utils.insertCase (false, deliveryProject.id, acc1.id, 'bord1');
      newCase.Type = Constants.CASE_TYPE_SPAIN_DELIVERY_TASK;
      insert newCase;
      
      Attachment att1 = new Attachment(ParentId = newCase.Id, Name = 'Test Att.txt', ContentType = 'text/plain', Body = Blob.valueOf('Some text here1'));
      Attachment att2 = new Attachment(ParentId = newCase.Id, Name = 'Test Att Account.txt', ContentType = 'text/plain', Body = Blob.valueOf('Some text here'));
      insert new List<Attachment>{att1,att2};
        
      delete att1;
      
      //RJ added 
      
      Case testITSMCase = Test_Utils.insertCase (false, null, acc1.id, 'ITSMTEST');
      
      ID rectypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.RECORDTYPE_Service_Central).getRecordTypeId(); 
      testITSMCase .RecordTypeId =rectypeid ;
      Insert testITSMCase ;
       Attachment att3 = new Attachment(ParentId = testITSMCase.Id, Name = 'Test Att Account.txt', ContentType = 'text/plain', Body = Blob.valueOf('Some text here'));
      insert att3;
      
      Delete att3;
      //RJ added ends
      Test.stopTest();
    }
  
  static testMethod void testConfidentialInfoAtts() {   
    
    Account a1 = Test_Utils.insertAccount();
    Account a2 = Test_Utils.insertAccount();
    Account a3 = Test_Utils.insertAccount();
    
    Confidential_Information__c ci1a1 = new Confidential_Information__c(Name = 'Secret Info 1', Account__c = a1.Id);
    Confidential_Information__c ci2a1 = new Confidential_Information__c(Name = 'Secret Info 2', Account__c = a1.Id);
    Confidential_Information__c ci1a2 = new Confidential_Information__c(Name = 'Secret Info 1', Account__c = a2.Id);
    
    insert new List<Confidential_Information__c>{ci1a1,ci2a1,ci1a2};
    
    Test.startTest();
    
    Attachment at1 = new Attachment(ParentId = ci1a1.Id, Body = Blob.valueOf('TEST'), Name = 'Attachment.txt');
    Attachment at2 = new Attachment(ParentId = ci1a1.Id, Body = Blob.valueOf('TEST'), Name = 'Attachment.txt');
    Attachment at3 = new Attachment(ParentId = ci2a1.Id, Body = Blob.valueOf('TEST'), Name = 'Attachment.txt');
    Attachment at4 = new Attachment(ParentId = ci1a2.Id, Body = Blob.valueOf('TEST'), Name = 'Attachment.txt');
    Attachment at5 = new Attachment(ParentId = a3.Id, Body = Blob.valueOf('TEST'), Name = 'Attachment.txt');
    insert new List<Attachment>{at1,at2,at3,at4,at5};
    
    Test.stopTest();
    
    system.assertEquals(2,[SELECT Attachment_Count__c FROM Confidential_Information__c WHERE Id = :ci1a1.Id].Attachment_Count__c);
    system.assertEquals(1,[SELECT Attachment_Count__c FROM Confidential_Information__c WHERE Id = :ci2a1.Id].Attachment_Count__c);
    system.assertEquals(1,[SELECT Attachment_Count__c FROM Confidential_Information__c WHERE Id = :ci1a2.Id].Attachment_Count__c);
    
    delete at2;
    
    system.assertEquals(1,[SELECT Attachment_Count__c FROM Confidential_Information__c WHERE Id = :ci1a1.Id].Attachment_Count__c);
    
    undelete at2;
    
    system.assertEquals(2,[SELECT Attachment_Count__c FROM Confidential_Information__c WHERE Id = :ci1a1.Id].Attachment_Count__c);
    
  }
}