/**=====================================================================
 * Experian
 * Name: BatchOpportunityLIScheduleRevenue_Test (was SumScheduleRevenueCurrentFY_Test)
 * Description: Test class for BatchOpportunityLIScheduleRevenue batch
 * Created Date: 19 Jan 2016
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 *
 =====================================================================*/
@isTest
private class BatchOpportunityLIScheduleRevenue_Test {

  static testMethod void testScheduler() {
    Test.startTest();
    // Schedule the test job
    String CRON_EXP = '0 0 0 3 9 ? '+String.valueOf(Date.today().year()+1); // Test next year
    
    String jobId = system.schedule('ScheduleBatchOpportunityLIScheduleRevenu-TESTJOB'+String.valueOf(Datetime.now().getTime()), CRON_EXP, new ScheduleBatchOpportunityLIScheduleRevenu());
      
    // Get the information from the CronTrigger API object  
    CronTrigger ct = [
      SELECT Id, CronExpression, TimesTriggered, NextFireTime
      FROM CronTrigger 
      WHERE Id = :jobId
    ];
    Test.stopTest();
    // Verify the expressions are the same  
    system.assertEquals(CRON_EXP, ct.CronExpression);
    // Verify the job has not run  
    system.assertEquals(0, ct.TimesTriggered);
  }
  
  static testMethod void testBatchStandard() {
    Test.startTest();
    
    BatchOpportunityLIScheduleRevenue b = new BatchOpportunityLIScheduleRevenue();
    Database.executeBatch(b,10);
    
    Test.stopTest();
    
    List<String> fieldNames = new List<String>(BatchOpportunityLIScheduleRevenue.revTotalsMapClean.keySet());
    
    List<OpportunityLineItem> checkOlis = (List<OpportunityLineItem>)Database.query('SELECT '+String.join(fieldNames,',')+' FROM OpportunityLineItem');
    system.assert(checkOlis.size() > 0, 'OpportunityLineItem not found');
    
    for(OpportunityLineItem oli :checkOlis) {
      for(String fName : fieldNames) {
        system.assertEquals(100.0, (Decimal)oli.get(fName), fName + ' should be 100.0');
      }
    }
  }
  
  static testMethod void testBatchAllOpen() {
    Test.startTest();
    
    BatchOpportunityLIScheduleRevenue b = new BatchOpportunityLIScheduleRevenue();
    b.runAllOpenOpps = true;
    Database.executeBatch(b,10);
    
    Test.stopTest();
    
    List<String> fieldNames = new List<String>(BatchOpportunityLIScheduleRevenue.revTotalsMapClean.keySet());
    
    List<OpportunityLineItem> checkOlis = (List<OpportunityLineItem>)Database.query('SELECT '+String.join(fieldNames,',')+' FROM OpportunityLineItem');
    system.assert(checkOlis.size() > 0, 'OpportunityLineItem not found');
    
    for(OpportunityLineItem oli :checkOlis) {
      for(String fName : fieldNames) {
        system.assertEquals(100.0, (Decimal)oli.get(fName), fName + ' should be 100.0');
      }
    }
  }
  
  static testMethod void testBatchAllOpps() {
    Test.startTest();
    
    BatchOpportunityLIScheduleRevenue b = new BatchOpportunityLIScheduleRevenue();
    b.runAllOpps = true;
    Database.executeBatch(b,10);
    
    Test.stopTest();
    
    List<String> fieldNames = new List<String>(BatchOpportunityLIScheduleRevenue.revTotalsMapClean.keySet());
    
    List<OpportunityLineItem> checkOlis = (List<OpportunityLineItem>)Database.query('SELECT '+String.join(fieldNames,',')+' FROM OpportunityLineItem');
    system.assert(checkOlis.size() > 0, 'OpportunityLineItem not found');
    
    for(OpportunityLineItem oli :checkOlis) {
      for(String fName : fieldNames) {
        system.assertEquals(100.0, (Decimal)oli.get(fName), fName + ' should be 100.0');
      }
    }
  }
  
  static testMethod void testBatchCloseRange() {
    Test.startTest();
    
    BatchOpportunityLIScheduleRevenue b = new BatchOpportunityLIScheduleRevenue();
    b.closeDateStart = Date.today().addDays(-60);
    b.closeDateEnd = Date.today().addDays(60);
    Database.executeBatch(b,10);
    
    Test.stopTest();
    
    List<String> fieldNames = new List<String>(BatchOpportunityLIScheduleRevenue.revTotalsMapClean.keySet());
    
    List<OpportunityLineItem> checkOlis = (List<OpportunityLineItem>)Database.query('SELECT '+String.join(fieldNames,',')+' FROM OpportunityLineItem');
    system.assert(checkOlis.size() > 0, 'OpportunityLineItem not found');
    
    for(OpportunityLineItem oli :checkOlis) {
      for(String fName : fieldNames) {
        system.assertEquals(100.0, (Decimal)oli.get(fName), fName + ' should be 100.0');
      }
    }
  }
  
  @testSetup
  static void setupTestData() {
    Account tstAcc = Test_utils.createAccount();
    
    insert new List<Account>{tstAcc};
    
    Opportunity tstOpp = Test_utils.createOpportunity(tstAcc.ID);
    tstOpp.CloseDate = Date.today().addDays(7);
    Opportunity tstOpp2 = Test_utils.createOpportunity(tstAcc.ID);
    tstOpp2.CloseDate = Date.today().addDays(7);
    
    insert new List<Opportunity>{tstOpp,tstOpp2};
    
    Product2 product = Test_Utils.insertProduct();
    Product.CanUseRevenueSchedule = True;
    update product;
    PricebookEntry stdPricebookEntry = Test_Utils.insertPricebookEntry(product.Id, Test.getStandardPricebookId(), Constants.CURRENCY_USD);
    
    Date scheduleFrom = Date.today().addDays(-390).toStartOfMonth();
    Date scheduleTo = Date.today().addDays(780).toStartOfMonth().addDays(-1); // end of previous month
    
    OpportunityLineItem tstOli = Test_utils.createOpportunityLineItem(tstOpp.Id, stdPricebookEntry.Id , tstOpp.Type);
    tstOli.Start_Date__c = scheduleFrom;
    tstOli.End_Date__c = scheduleTo;
    OpportunityLineItem tstOli2 = Test_utils.createOpportunityLineItem(tstOpp2.Id, stdPricebookEntry.Id , tstOpp2.Type);
    tstOli2.Start_Date__c = scheduleFrom;
    tstOli2.End_Date__c = scheduleTo;
    
    List<OpportunityLineItem> oliLst = new List<OpportunityLineItem>{tstOli,tstOli2};
    
    insert oliLst;
    
    List<OpportunityLineItemSchedule> oliSchs = new List<OpportunityLineItemSchedule>();
    
    // Rev will be 100 USD per month for the duration of the schedule
    Integer monthsCount = scheduleFrom.monthsBetween(scheduleTo);
    for(OpportunityLineItem oli : oliLst) {
      for(Integer cm = 0; cm < monthsCount; cm++) {
        Date currentMonth = scheduleFrom.addMonths(cm);
        oliSchs.add(new OpportunityLineItemSchedule(
          OpportunityLineItemId = oli.Id,
          Type = 'Revenue',
          Revenue = 100.00,
          ScheduleDate = currentMonth,
          Description = 'According to invoice.'
        ));
      }
    }
    
    insert oliSchs;
  }
}