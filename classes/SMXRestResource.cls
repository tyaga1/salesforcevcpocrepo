/**********************************************************************************************
 * Appirio, Inc 
 * Name         : SMXRestResource
 * Created By   : Unknown
 * Purpose      : 
 * Created Date : Unknown
 *
 * Date Modified                Modified By                 Description of the update
 * 11 Aug, 2015                 Paul Kissick                Case #01093025 - Fixed getConvertDateTime method & formatting
 * Jul 17th, 2016(QA)           Tyaga Pati                  CRM 2.0 W-005364/01790367 - Function added to Create Activities when Survey Response is sent by Satmatrix
***********************************************************************************************/
@RestResource(urlMapping='/UpdateFeedback/*')
global with sharing class SMXRestResource {
  
  @HttpPost
  global static String doPost(String strActionCode,Decimal decScore,String strComment,String strStatus,String strSubStatus,String strFeedbackReceivedDate,String strInvitationSentDate,String strFirstReminderDate,String strSecondReminderDate,String strThirdReminderDate, String strPersonId,String strDcname, String strDcid) {
    RestRequest req = RestContext.request;
    String strFbkId= req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    strFbkId = EncodingUtil.urlDecode(strFbkId,'UTF-8');
    return updateFeedback(strFbkId,strActionCode,decScore,strComment,strStatus,strSubStatus,strFeedbackReceivedDate,strInvitationSentDate,strFirstReminderDate,strSecondReminderDate,strThirdReminderDate,strPersonId,strDcname,strDcid);         
  }
  
  public static String updateFeedback(String strFbkId,String strActionCode,Decimal decScore,String strComment,String strStatus,String strSubStatus,String strFeedbackReceivedDate,String strInvitationSentDate,String strFirstReminderDate,String strSecondReminderDate,String strThirdReminderDate,String strPersonId,String strDcname, String strDcid) {
    String strReturnVal = '';
    String strFeedbackReceivedDateParsed = '';
    String strInvitationSentDateParsed = '';
    String strFirstReminderDateParsed = '';
    String strSecondReminderDateParsed = '';
    String strThirdReminderDateParsed = '';
    List <Feedback__c> lstFeedback= [
      SELECT Id 
      FROM Feedback__c 
      WHERE Name = :strFbkId
    ];
    String strContactRecordStatus = 'Failure';
    if (lstFeedback.isEmpty()) {      
      strContactRecordStatus = createSurveyRecord( strFbkId, strStatus, strPersonId, strSubStatus,strDcname,  strDcid);
    }
    else {
      strContactRecordStatus = 'Success';
    }
    
    if (strContactRecordStatus == 'Success') {
      if (strActionCode == 'UpdateESRStatus') {
        Feedback__c Feedback = [
          SELECT Name, Status__c, StatusDescription__c, ResponseReceivedDate__c, FirstReminderDate__c, SecondReminderDate__c, ThirdReminderDate__c 
          FROM Feedback__c 
          WHERE Name = :strFbkId
        ];
        Feedback.Status__c = strStatus;
        Feedback.StatusDescription__c = strSubStatus;
        if(String.isNotBlank(strFeedbackReceivedDate)) {
          Datetime dtme = getConvertDateTime(strFeedbackReceivedDate);         
          Feedback.ResponseReceivedDate__c = dtme;
        }
        if(String.isNotBlank(strInvitationSentDate)) {
          Datetime dtme = getConvertDateTime(strInvitationSentDate);               
          Feedback.Invitation_Sent_Date__c = dtme;
        }
        if(String.isNotBlank(strFirstReminderDate)) {
          Datetime dtme = getConvertDateTime(strFirstReminderDate);               
          Feedback.FirstReminderDate__c = dtme;
        }
        if(String.isNotBlank(strSecondReminderDate)) {
          Datetime dtme = getConvertDateTime(strSecondReminderDate);
          Feedback.SecondReminderDate__c = dtme;
        }
        if (String.isNotBlank(strThirdReminderDate)) {
          Datetime dtme = getConvertDateTime(strThirdReminderDate);
          Feedback.ThirdReminderDate__c = dtme;
        }
        update Feedback;
        strReturnVal = Feedback.Status__c;
      }
      else if (strActionCode == 'UpdateFeedbackDetails') {
        String strSFDCServer = '';
        SMXConfiguration__c smx_config = SMXConfiguration__c.getValues('Configuration');
        if(smx_config != null) {
          strSFDCServer = smx_config.URL_SALESFORCE__c;
        }
        Feedback__c Feedback = [
          SELECT Name, PrimaryScore__c, PrimaryComment__c, Status__c, StatusDescription__c, SurveyDetailsURL__c, Contact__c
          FROM Feedback__c 
          WHERE Name = :strFbkId
        ];
        Feedback.PrimaryScore__c= decScore;
        Feedback.PrimaryComment__c = strComment;
        Feedback.Status__c = 'Invitation Delivered';     
        Feedback.StatusDescription__c = 'Response Received';
        Feedback.SurveyDetailsURL__c = strSFDCServer + '/apex/SMXSurveyDetails?ProvID='+strFbkId;
        update Feedback;

        //Update Contact TableNPS Score and Comment
        if (Feedback.Contact__c != null) {
          Contact contact = [
            SELECT PrimaryScore__c, PrimaryComment__c 
            FROM Contact 
            WHERE Id = :Feedback.Contact__c
          ];
          contact.PrimaryScore__c= decScore;
          contact.PrimaryComment__c= strComment;
          contact.Survey_name__c=strDcname;
          update contact;
          strReturnVal = contact.PrimaryComment__c;
          
          //Call Activity Creation Function with Survey Id, Survey contact Id and Survey Account Id
          //createNewActivity(Feedback.Id, Feedback.Contact__r.AccountId,Feedback.Contact__c);
        }
      }
    }
    return strReturnVal; 
  }
  //**********************************************************************************************
// TP Case 01790367 : This function will Create Activity on the Survey Record. 
 //********************************************************************************************* 
 global static String createNewActivity(Id SurveyRecId, Id SurveyAcntId, Id SurveyConId) {
    Id SurveyCon = SurveyConId;
    //String Description = ActivityText;      
    List<Contact_Team__c> BUlistTemp = [
      SELECT Business_Unit__c 
      FROM Contact_Team__c 
      WHERE (Business_Unit__c LIKE '%BIS' OR Business_Unit__c LIKE '%CIS') 
      AND Contact__c = :SurveyConId
    ];
    List<String> BUlst = new List<String>();
    //return 'The size of BU List is '+BUlst;
    if (BUlistTemp.size() > 0) {
      for(Contact_Team__c BU: BUlistTemp) {
        //return BU.Business_Unit__c;
        BUlst.add(BU.Business_Unit__c);
      }
    }

    //Query AccountTeam to Find all the members with matching Business Unit as in the List Above.
    // PK NOTE 2016-03-23: Should this be list instead of set?
    Set<AccountTeamMember> ActAsgnLst = new Set<AccountTeamMember>([
      SELECT UserId 
      FROM AccountTeamMember 
      WHERE AccountId = :SurveyAcntId 
      AND User.Business_Unit__c in:BUlst
    ]);
            
    //Iterate through the Set of Assignees above and create Activities for them.   
    List<Task> SatMRespTask = New List<Task>();
    if (ActAsgnLst.size() > 0) {
      for(AccountTeamMember ActAssignee:ActAsgnLst) {
        Task TaskTemp = new Task();
        TaskTemp.OwnerId = ActAssignee.UserId;
        TaskTemp.WhatId = SurveyAcntId;
        TaskTemp.Subject = 'Client Survey Response from Satmetrix';
        TaskTemp.Status = 'Open';
        TaskTemp.Priority = 'Normal';
        TaskTemp.Type = 'Client Survey';
        TaskTemp.WhoId =  SurveyConId;
        TaskTemp.ParentSurvey__c = [select Id from feedback__C where Id =:SurveyRecId].Id;
        TaskTemp .RecordTypeId = [Select Id,SobjectType,Name From RecordType where Name =:'SurveyRecordType' and SobjectType =:'Task' limit 1].Id; //'012180000000Dbs'; 
        SatMRespTask.add(TaskTemp);
      } 
    }   
    insert SatMRespTask;
    return 'Following Activities Were Created '+SatMRespTask;
  } 
 
  //**********************************************************************************************
  // PK Case 1093025 : Fix this to work for different sizes of string for the date.
  //*********************************************************************************************
  public static DateTime getConvertDateTime(string strDT) {
    // build the datetime based on the char positions as this broke very easily
    // e.g. 2015-08-11 08:46
    Integer yr = Integer.valueOf(strDT.mid(0,4));
    Integer mo = Integer.valueOf(strDT.mid(5,2));
    Integer dy = Integer.valueOf(strDT.mid(8,2));
    Integer hr = (strDT.length()>12) ? Integer.valueOf(strDT.mid(11,2)) : 0;
    Integer mi = (strDT.length()>14) ? Integer.valueOf(strDT.mid(14,2)) : 0;
    Integer se = (strDT.length()>17) ? Integer.valueOf(strDT.mid(17,2)) : 0;
    return DateTime.newInstanceGMT(yr, mo, dy, hr, mi, se);
  }
  
  public static String createSurveyRecord(String strFbkId, String strStatus, String strPersonId, String strSubStatus, String strDcname, String strDcid) {
    List<Contact> lstContact = new List<Contact>();
    List<Case> lstcase = new List<Case>();
    Set<String> setContactEmail = new Set<String>();
    lstContact = [
      SELECT Id 
      FROM Contact 
      WHERE Id = :strPersonId
    ];
    if (!lstContact.isEmpty()) {
      Feedback__c feedbackListSurvey = new Feedback__c();
      feedbackListSurvey.Name = strFbkId;
      feedbackListSurvey.Contact__c =lstContact[0].Id; //ContactName 
      feedbackListSurvey.Status__c = strStatus;               
      feedbackListSurvey.StatusDescription__c=strSubStatus;
      feedbackListSurvey.DataCollectionName__c = strDcname;
      feedbackListSurvey.DataCollectionId__c = strDcid;
      insert feedbackListSurvey; 
      return 'Success';
    }
    else {
      return 'Failure';
    }
  }
  
  
  /***** Test Methods Start Here *****/
  
  static testMethod void testGetConvertDateTime(){
    system.assertEquals(Datetime.newInstanceGMT(2012,1,1,10,25,20),SMXRestResource.getConvertDateTime('2012-01-01 10:25:20'));
    system.assertEquals(Datetime.newInstanceGMT(2012,1,1,10,25,0),SMXRestResource.getConvertDateTime('2012-01-01 10:25'));
  }
  
  @isTest
  static void testDoPost(){
  
    User testExperianUser = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    Experian_Global__c expGlobal = new Experian_Global__c();
    expGlobal.OwnerId__c = testExperianUser.ID;
    insert expGlobal;
    // Create Contact first
    Contact c = new Contact();
    c.put('FirstName','SMXTestCtctFName');
    c.put('LastName','SMXTestCtctLName');
    c.put('MailingCountry','SMXTestCoountry');
    insert c;
    
    SMXConfiguration__c smxCfg = new SMXConfiguration__c(
      Name = 'Configuration', 
      URL_SALESFORCE__c = 'https://experian.my.salesforce.com',
      ID_XP_ENTERPRISE__c = 'EXPERIAN',
      TEST_MODE__c = false,
      URL_CREATEPARTY_SERVICE__c = 'https://experian.staging.satmetrix.com/app/core/ws/extservices/user/createPartyEntity',
      URL_DCMETADATA_SERVICE__c = 'https://experian.staging.satmetrix.com/app/core/ws/extservices/user/getDCMetaDetails?',
      URL_JSECURITY_CHECK__c = 'https://experian.staging.satmetrix.com/app/core/j_satmetrix_security_check',
      URL_NOMINATE_CONTACTS__c = 'https://experian.staging.satmetrix.com/app/core/ws/extservices/com.satmetrix.core.server.fbk.provider.FbkProviderEntityUtilService/NOMINATE_CONTACTS',
      URL_SALESFORCE_API_SERVER__c = 'https://experian--dev.cs15.my.salesforce.com/services/Soap/u/19.0/00De0000005OwmJ',
      XP_SECURITYTOKEN__c = 'sdfgdfgh',
      XP_SERVER__c = 'experian.staging.satmetrix.com'
    );
    insert smxCfg;
    
    system.assertEquals('Success',SMXRestResource.createSurveyRecord('23985723985732', 'Test_Nominated', c.Id, 'Test Description', 'Test Survey Name', '123456'));
    system.assertEquals('Failure',SMXRestResource.createSurveyRecord('23985723985735', 'Test_Nominated', null, 'Test Description', 'Test Survey Name', '123456'));
    
    Feedback__c fb = [SELECT Id,Name FROM Feedback__c WHERE Contact__c = :c.Id LIMIT 1];
    
    String strFeedbackId =  fb.Name;
    System.RestContext.request = new RestRequest();
    RestContext.request.requestURI = '/UpdateFeedback/' + strFeedbackId;
    
    SMXRestResource.doPost('UpdateESRStatus',9,'Test Comment','Delivered','Not Started','2012-01-01 10:25:20','2012-01-01 10:25:20','2012-01-01 10:25:20','2012-01-01 10:25:20','2012-01-01 10:25:20','ABCDEFGIJ123456789','SMX TEST DC','TEST_1288');
    SMXRestResource.doPost('UpdateFeedbackDetails',9,'Test Comment','Delivered','Not Started','2012-01-01 10:25:20','2012-01-01 10:25:20','2012-01-01 10:25:20','2012-01-01 10:25:20','2012-01-01 10:25:20','ABCDEFGIJ123456789','SMX TEST DC','TEST_1288');
    
  }


}