/*******************
Created BY : Richard Joseph.
Created Date: Jun 20th 2016.
Desc: Trigger handler for Deal.
Change Log:

*************/
public class DealTriggerHandler {
    
     public static void beforeInsert(List<Deal__c> newDeals) {
         For(Deal__c DealRec:newDeals){
             DealRec.Board_Reporting_DQ_Trigger__c=DealTriggerHandler.returnBRDQValue(DealRec);
             DealRec.Reporting_DQ_Trigger__c=DealTriggerHandler.returnBRDQValue(DealRec);
         }
         
     }
    public static void beforeUpdate(Map<ID, Deal__c> newMap, Map<ID, Deal__c> oldMap) {
        
        For(Deal__c DealRec:newMap.Values()){
             DealRec.Board_Reporting_DQ_Trigger__c=DealTriggerHandler.returnBRDQValue(DealRec);
             DealRec.Reporting_DQ_Trigger__c=DealTriggerHandler.returnReportDQValue(DealRec);
         }
}


Private static integer returnBRDQValue(Deal__c dealRec)
{
 Integer returnvalue = 0;
 
 returnvalue =  (String.isBlank(dealRec.Board_Stage__c)? 0: 1) ;
 
    returnvalue = returnvalue+ (String.isBlank(dealRec.Review_Stage_Type__c)? 0: 1) ;  
    returnvalue = returnvalue+ ((dealRec.POS_Board_Date__c== null)? 0: 1) ; 
    returnvalue = returnvalue+ ((dealRec.Regional_SPC_date__c== null)? 0: 1) ; 
    returnvalue = returnvalue+ ((dealRec.Global_SPC_date__c== null)? 0: 1) ; 
    returnvalue = returnvalue+ (String.isBlank(dealRec.RNS_announcement__c)? 0: 1) ; 
    returnvalue = returnvalue+ (String.isBlank(dealRec.Board_report_comments__c)? 0: 1) ;  
    
    return returnvalue;
    
}
Private static integer returnReportDQValue(Deal__c dealRec)
{
 Integer returnvalue = 0;
 
 returnvalue =  (String.isBlank(dealRec.Sub_Stage__c)? 0: 1) ;
 
    returnvalue = returnvalue+ (String.isBlank(dealRec.Review_Stage_Type__c)? 0: 1) ;  
    returnvalue = returnvalue+ (String.isBlank(dealRec.Stage__c)? 0: 1) ;
    returnvalue = returnvalue+ (String.isBlank(dealRec.Type__c)? 0: 1) ;
    returnvalue = returnvalue+ (String.isBlank(dealRec.Strategic_Priority__c)? 0: 1) ;
    returnvalue = returnvalue+ (String.isBlank(dealRec.Region__c)? 0: 1) ;
    returnvalue = returnvalue+ (String.isBlank(dealRec.Segment__c)? 0: 1) ;
    returnvalue = returnvalue+ (String.isBlank(dealRec.Ownership_type__c)? 0: 1) ;
    returnvalue = returnvalue+ (String.isBlank(dealRec.Strategic_Rationale__c)? 0: 1) ;
    returnvalue = returnvalue+ (String.isBlank(dealRec.Deal_sponsor__c)? 0: 1) ;
    returnvalue = returnvalue+ (String.isBlank(dealRec.Reference_year__c)? 0: 1) ;
    returnvalue = returnvalue+ (String.isBlank(dealRec.Trim_Last_Action_Text__c )? 0: 1) ;
    returnvalue = returnvalue+ (String.isBlank(dealRec.Trim_Next_Action_Text__c )? 0: 1) ;
    returnvalue = returnvalue+ (String.isBlank(dealRec.Deal_manager__c )? 0: 1) ;
       returnvalue = returnvalue+ (dealRec.Current_year_revenue__c>0? 1: 0) ;
        returnvalue = returnvalue+ (dealRec.Current_year_EBITDA__c>0? 1: 0) ;
         returnvalue = returnvalue+ (dealRec.Maximum_transaction_value__c>0 ? 1: 0) ;
          returnvalue = returnvalue+ (dealRec.Minimum_transaction_value__c >0 ? 1: 0) ;
    
    
    
    
    return returnvalue;
    
}

}