/******************************************************************************
 * Appirio, Inc
 * Name: ContractsConfidentialInformationExt
 * Description: T-359158: Extension class to show confidential information as RL on CSDA Cases
 * Created Date: Feb 04th, 2015
 * Created By: Naresh Kr Ojha(Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * Feb 12th, 2015               Arpita Bose(Appirio)         T-361211 : Added code for urlString for mapping Case.Opportunity with CI.Opportunity
 * Feb 13th, 2015               Arpita Bose                  S-277736 : Added CreatedDate in query
 * Jan 19th, 2016               Tyaga Pati                   Changes were made to the URL configuration as part of Cases # 01277221 and Case# 01022575
 * Mar 7th,  2016               Ben Burdick                  I added a constraint to the where clause on Confidential_Information__c( and Account__c <> null) Case #1825864 
 * Feb 7th,  2017               Sanket Vaidya                Case#02029822 New columns and condition to display related CIs to for the case. Commented the unused code.
 * Sep 13th, 2017               Malcolm Russell              Case#02153859 Add Opportunity account details to URL if Opportunity exists                                                              
 ******************************************************************************/
public with sharing class ContractsConfidentialInformationExt {
  
  public List<Confidential_Information__c> confInfoList {get;set;} 
  public Case myCase {get;set;}
  public Boolean fullView {get;set;}
  public String urlString {get;set;}
  private boolean hasError ;
  
  //==========================================================================
  // Constructor
  //==========================================================================
  public ContractsConfidentialInformationExt(ApexPages.StandardController controller) {
    urlString = '';
    hasError = false;
    fullView = false;
    String caseID = controller.getId();
    myCase = (Case) controller.getRecord();
    
    if (myCase.Id != null) {
      myCase = [SELECT Id, Opportunity__c, Account.Id,Account.Name,Account__c, Account__r.name, Account__r.Id, Opportunity__r.Name, Opportunity__r.Id,Opportunity__r.accountId, Opportunity__r.account.name, CaseNumber 
                FROM Case WHERE ID =: myCase.Id];//Account.Id. query the standard field.

      confInfoList = new List<Confidential_Information__c>();
      for (Confidential_Information__c confInfo : [SELECT RecordTypeId, Opportunity__r.Name, Name, Membership__r.Name,
                                                            Id, Contract__c, Contract_Document__c, Account__c, Account__r.Name,
                                                            CreatedDate //S-277736
                                                     FROM Confidential_Information__c
                                                     WHERE Case__c =: myCase.ID
                                                          Order By CreatedDate DESC]) {
      confInfoList.add(confInfo);
    }

      /*if (myCase.Opportunity__c != null) {
        confInfoList = new List<Confidential_Information__c>();
        for (Confidential_Information__c confInfo : [SELECT RecordTypeId, Opportunity__c, Name, Membership__r.Name,
                                                            Id, Contract__c, Contract_Document__c, Account__c, Account__r.Name,
                                                            CreatedDate //S-277736
                                                     FROM Confidential_Information__c
                                                     WHERE Opportunity__c =: myCase.Opportunity__c
                                                          Order By CreatedDate DESC]) {
          confInfoList.add(confInfo);
          
        }
      }
      else{
     
        confInfoList = new List<Confidential_Information__c>();
        for (Confidential_Information__c confInfo : [SELECT RecordTypeId, Opportunity__c, Name, Membership__r.Name,
                                                            Id, Contract__c, Contract_Document__c, Account__c, Account__r.Name,
                                                            CreatedDate //S-277736
                                                     FROM Confidential_Information__c
                                                     WHERE Account__c =: myCase.Account.Id and Account__c <> null  // BWB Case #1825864
                                                          Order By CreatedDate DESC]) {
          confInfoList.add(confInfo);
          
        }
      }*/
      
      
      if (Apexpages.currentPage().getParameters().get('view') == 'full') {
        fullView = true;
      }
      
      String conInfoObjPrefix = Custom_Object_Prefixes__c.getInstance().Confidential_Information__c;
      
      If(myCase.Opportunity__c != null)
          {
              urlString = '/'+conInfoObjPrefix+'/e?CF' + Custom_Fields_Ids__c.getInstance().Conf_Info_Opportunity__c+'_lkid='+ myCase.Opportunity__r.Id;
              urlString += '&CF' + Custom_Fields_Ids__c.getInstance().Conf_Info_Opportunity__c + '=' + myCase.Opportunity__r.Name;
              urlString += '&CF' + Custom_Fields_Ids__c.getInstance().Conf_Info_Case__c + '=' + myCase.CaseNumber;
              urlString += '&' + Custom_Fields_Ids__c.getInstance().Conf_Info_Synch_OTM__c + '=1';
              urlString += '&CF'+ Custom_Fields_Ids__c.getInstance().Confidential_Information_Account__c+'_lkid=' + myCase.Opportunity__r.AccountId;
              urlString += '&CF' + Custom_Fields_Ids__c.getInstance().Confidential_Information_Account__c +'=' + myCase.Opportunity__r.Account.name;
              urlString += '&RecordType=' + Record_Type_Ids__c.getInstance().Confidential_Information_Opportunity__c;
              //urlString += '&retURL=' + caseID ;
              //urlString += '&saveURL=' + caseID;
          }  
      else
          {
              urlString = '/'+conInfoObjPrefix+'/e?CF' + Custom_Fields_Ids__c.getInstance().Confidential_Information_Account__c+'_lkid='+ myCase.Account.Id;
              urlString += '&CF' + Custom_Fields_Ids__c.getInstance().Confidential_Information_Account__c + '=' + myCase.Account.Name;
              urlString += '&CF' + Custom_Fields_Ids__c.getInstance().Conf_Info_Case__c + '=' + myCase.CaseNumber;
              urlString += '&' + Custom_Fields_Ids__c.getInstance().Conf_Info_Synch_OTM__c + '=1';
              urlString += '&RecordType=' + Record_Type_Ids__c.getInstance().Confidential_Information_Opportunity__c;
              //urlString += '&retURL=' + caseID ;
              //urlString += '&saveURL=' + caseID;
      
      
          }
    }
  }

}