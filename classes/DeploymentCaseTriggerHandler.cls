/**********************************************************************************
 * Experian
 * Name: DeploymentCaseTriggerHandler
 * Description: Trigger Handler for Deployment_Cases__c
 *             
 * Created Date: Oct 20th, 2015
 * Created By: Paul Kissick
 * 
 * Date Modified       Modified By                  Description of the update
 * Nov 18th, 2015      Paul Kissick                 Case 01244379: Adding support for stories
 * Jul 25th, 2016      Paul Kissick                 Adding support for AA work
 *********************************************************************************/
public with sharing class DeploymentCaseTriggerHandler {

  public static void afterDelete(Map<Id,Deployment_Cases__c> oldMap) {
    cleanupDeploymentComponents(oldMap);
  }
  
  public static void cleanupDeploymentComponents(Map<Id,Deployment_Cases__c> deploymentCases) {
    // at this point we know 2 things, the original case and the deployment request. Use these to get the deployment components to cleanup.
    
    Set<Id> caseIds = new Set<Id>();
    Set<Id> storyIds = new Set<Id>();
    Set<Id> workIds = new Set<Id>();
    
    Set<Id> caseDeploymentRequestSlotIds = new Set<Id>();
    Set<Id> storyDeploymentRequestSlotIds = new Set<Id>();
    Set<Id> workDeploymentRequestSlotIds = new Set<Id>();
    
    for(Deployment_Cases__c dc : deploymentCases.values()) {
      if (dc.Case__c != null && dc.Deployment_Request_Slot__c != null) {
        caseIds.add(dc.Case__c);
        caseDeploymentRequestSlotIds.add(dc.Deployment_Request_Slot__c);
      }
      if (dc.Story_Name__c != null && dc.Deployment_Request_Slot__c != null) {
        storyIds.add(dc.Story_Name__c);
        storyDeploymentRequestSlotIds.add(dc.Deployment_Request_Slot__c);
      }
      if (dc.Work__c != null && dc.Deployment_Request_Slot__c != null) {
        workIds.add(dc.Work__c);
        workDeploymentRequestSlotIds.add(dc.Deployment_Request_Slot__c);
      }
    }
    
    if (!caseIds.isEmpty() && !caseDeploymentRequestSlotIds.isEmpty()) {
      List<Deployment_Component__c> deployComps = [
        SELECT Id FROM Deployment_Component__c
        WHERE Case_Component__c IN (
          SELECT Id
          FROM Case_Component__c
          WHERE Case_Number__c IN :caseIds
        )
        AND Slot__c IN :caseDeploymentRequestSlotIds
      ];
      
      try {
        delete deployComps;
      }
      catch (Exception e) {
        system.debug('DeploymentCaseTriggerHandler in method cleanupDeploymentComponents() Error:' + e.getMessage());
        ApexLogHandler.createLogAndSave('DeploymentCaseTriggerHandler','cleanupDeploymentComponents', e.getStackTraceString(), e);
      }
    }
    // Stories
    if (!storyIds.isEmpty() && !storyDeploymentRequestSlotIds.isEmpty()) {
      List<Deployment_Component__c> deployCompsStory = [
        SELECT Id FROM Deployment_Component__c
        WHERE Case_Component__c IN (
          SELECT Id
          FROM Case_Component__c
          WHERE Story__c IN :storyIds
        )
        AND Slot__c IN :storyDeploymentRequestSlotIds
      ];
      
      try {
        delete deployCompsStory;
      }
      catch (Exception e) {
        system.debug('DeploymentCaseTriggerHandler in method cleanupDeploymentComponents() Error:' + e.getMessage());
        ApexLogHandler.createLogAndSave('DeploymentCaseTriggerHandler','cleanupDeploymentComponents', e.getStackTraceString(), e);
      }
    }
    // AA work
    if (!workIds.isEmpty() && !workDeploymentRequestSlotIds.isEmpty()) {
      List<Deployment_Component__c> deployCompsWork = [
        SELECT Id FROM Deployment_Component__c
        WHERE Case_Component__c IN (
          SELECT Id
          FROM Case_Component__c
          WHERE User_Story_AA__c IN :workIds
        )
        AND Slot__c IN :workDeploymentRequestSlotIds
      ];
      
      try {
        delete deployCompsWork;
      }
      catch (Exception e) {
        system.debug('DeploymentCaseTriggerHandler in method cleanupDeploymentComponents() Error:' + e.getMessage());
        ApexLogHandler.createLogAndSave('DeploymentCaseTriggerHandler','cleanupDeploymentComponents', e.getStackTraceString(), e);
      }
    }
  }
  
}