/**=====================================================================
 * Experian
 * Name: ContactTeamTriggerHandler_Test
 * Description: Case #01252420 Test class for ContactTeamTrigger.trigger and ContactTeamTriggerHandler Apex Class
 * Created Date: Jan 14th, 2016 
 * Created By: James Wills
 * 
 * Date Modified      Modified By                  Description of the update
 * Apr 7th, 2016      Paul Kissick                 Case 01932085: Fixing Test User Email Domain
 * Jul 19th, 2016(QA) Paul Kissick                 CRM2:W-005406: Cleaned up class a bit
 * Sep 28th, 2016     Paul Kissick                 W-005955: Added tests for unique relationships
 * TODO: PK - This test seems overly complicated and hard to follow.
 *=====================================================================*/

@isTest
private class ContactTeamTriggerHandler_Test{

  //Class used to keep track of running totals to compare with fields on Contact Object and ensure that they have been set correctly.
  //Only one instance of this Class is created per test.  
  private class ContactTeamRecordsCountClass {
    Integer contactTeamRecordsCount       = 0;
    Integer contactTeamRecordsActiveCount = 0;
  }
  
  static testMethod void testUniqueRelationship() {
    
    User usr1 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    
    Account a = Test_Utils.insertAccount();
    Contact c = Test_Utils.insertContact(a.Id);
    
    Contact_Team__c ct1 = Test_Utils.createContactTeam(c.Id, usr1.Id);
    Boolean foundError = false;
    try {
      insert ct1;
    }
    catch (Exception e) {
      foundError = true;
    }
    
    system.assertEquals(false, foundError, 'No error should have been found, since this is a new relationship.');
    
    foundError = false;
    
    Contact_Team__c ct2 = Test_Utils.createContactTeam(c.Id, usr1.Id);
    
    try {
      insert ct2;
    }
    catch (Exception e) {
      foundError = true;
    }
    
    system.assertEquals(true, foundError, 'No error caught, this should have been caught!');
    
  }

  //TEST METHOD 1
  static testMethod void testInsertDeleteUpdateContactTeams1() {

    //start of create data for tests section          
    ContactTeamRecordsCountClass contactTeamRecordsCounters = new ContactTeamRecordsCountClass();     
    Contact newContact = createDataForTest(contactTeamRecordsCounters);

    Profile p = [select id from profile where name=: Constants.PROFILE_SYS_ADMIN ];     
    User activeUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    activeUser1.isActive = True;
    insert activeUser1;
      
    User inactiveUser1 = Test_Utils.createUser(p, 'test1235@experian.com', 'test1');
    inactiveUser1.isActive = False;
    insert inactiveUser1;  
     
    //end of create data for tests section
      
    Test.startTest();

    //TEST NUMBER 1 - afterInsert #1 (active User)
    Contact_Team__c contactTeamRecord1_Active = insertContactTeam_Active(newContact, activeUser1, contactTeamRecordsCounters);
    checkContactTotals(newContact, contactTeamRecordsCounters, 'TEST NUMBER 1a - afterInsert #1 (active User)');

    //TEST NUMBER 1 - afterInsert #2 (inactive User)    
    Contact_Team__c contactTeamRecord2_inActive = insertContactTeam_inActive(newContact, inactiveUser1, contactTeamRecordsCounters);  
    checkContactTotals(newContact, contactTeamRecordsCounters, 'TEST NUMBER 1b - afterInsert #2 (inactive User)');        

    //TEST NUMBER 1 - afterDelete #1 (inactive User)    
    deleteContactTeam_inActive(contactTeamRecord2_inActive, contactTeamRecordsCounters);
    checkContactTotals(newContact, contactTeamRecordsCounters, 'TEST NUMBER 1c - afterDelete #1 (inactive User)');

    //TEST NUMBER 1 - afterDelete #2 (active User)    
    deleteContactTeam_Active(contactTeamRecord1_Active, contactTeamRecordsCounters);  
    checkContactTotals(newContact, contactTeamRecordsCounters, 'TEST NUMBER 1d - afterDelete #2 (active User)');
     
    //TEST NUMBER 1 - afterInsert #3 (active User)
    Contact_Team__c contactTeamRecord3_Active = insertContactTeam_Active(newContact, activeUser1, contactTeamRecordsCounters);          
    checkContactTotals(newContact, contactTeamRecordsCounters, 'TEST NUMBER 1e afterInsert #1 (active User)');
        
    //TEST NUMBER 1 - afterUpdate #1 (active User)
    updateContactTeam_Active(contactTeamRecord3_Active, contactTeamRecordsCounters);    
     
    Test.stopTest();
     
    checkContactTotals(newContact, contactTeamRecordsCounters, 'TEST NUMBER 1f afterUpdate #1 (active User)');
  }

  //TEST METHOD 2
  static testMethod void testInsertDeleteUpdateContactTeams_Bulk() {

    //start of create data for tests section          
    ContactTeamRecordsCountClass contactTeamRecordsCounters = new ContactTeamRecordsCountClass();     
    Contact newContact = createDataForTest(contactTeamRecordsCounters);

    Profile p = [select id from profile where name=: Constants.PROFILE_SYS_ADMIN ];     
    User activeUser1 = Test_Utils.createUser(p, 'test1235@experian.com', 'test1');
    activeUser1.isActive = True;
    insert activeUser1;  
     
    User inactiveUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    inactiveUser1.isActive = False;
    insert inactiveUser1;  
             
    //end of create data for tests section
       
    Test.startTest();      
  
    Integer amountRecordsToInsert = 50;

    insertContactTeamBulk(newContact, p, amountRecordsToInsert, true, contactTeamRecordsCounters);       //insert 50 Contact_Teams__c where the associated User.isActive==true
    insertContactTeamBulk(newContact, p, amountRecordsToInsert, false, contactTeamRecordsCounters);      //insert 50 Contact_Teams__c where the associated User.isActive==false     

    //TEST NUMBER 2 - afterDelete (bulk)
    deleteContactTeams_with_inactiveUserRecords(contactTeamRecordsCounters); //delete Contact_Team__c records where the associated User.isActive==false

    Test.stopTest();
     
    checkContactTotals(newContact, contactTeamRecordsCounters, '2');
    
  }
  
    
  //TEST METHOD 3
  static testMethod void testUpdateContactTeam_RelationShipOwnerUsers() {
    
    //Updating the User from isActive from 'true' to 'false' (and vice-versa) will change the number of active users associated with a Contact_Team__c.  
    //This test is to ensure that the Active total updates accordingly.
    //  
    //The update to the User Object has to be tested separately as updating a User in the same section as the insert of a non-setup Object produces mixed DML error.
    //Putting the update into an @future method inside a Test section also doesn't work in this instance and in any case inserting a User causes the 
    //User Trigger to call another @future method, which is not permitted.
    
    Contact_Team__c contactTeamRecord4_Active = new Contact_Team__c();
    User activeUser1 = new User();
    ContactTeamRecordsCountClass contactTeamRecordsCounters = new ContactTeamRecordsCountClass();
    Global_Settings__c custSettings = new Global_Settings__c(name= Constants.GLOBAL_SETTING, Account_Team_Member_Default_Role__c = 'Sales Rep');       
    Account newAccount = new Account(Name = 'TestAccount001', BillingCountry = 'Sweden');    
    Contact newContact = new Contact(FirstName = 'Bob', LastName = 'Smith', accountId=newAccount.Id);     
   
    //Running as self avoids the mixed DML error when a user update is performed in the Test section.
    system.runAs(new User(Id = UserInfo.getUserId())) {
      
      //start of create data for tests section   
      Profile p = [select id from profile where name=: Constants.PROFILE_SYS_ADMIN ];     
      activeUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
      activeUser1.isActive = True;
      insert activeUser1;
     
      User inactiveUser1 = Test_Utils.createUser(p, 'test1235@experian.com', 'test1');
      inactiveUser1.isActive = False;
      insert inactiveUser1;     
      insert custSettings;      
      insert newAccount;          
      insert newContact;     
      //The Contact Trigger will add a Contact_Team__c record associated with an Active user. 

      //This section takes into account any Contact_Team__c records created by Triggers fired as a result of creating the data for the test
      List<Contact_Team__c> contactTeamList = [SELECT id, Relationship_Owner__r.isActive FROM Contact_Team__c WHERE Contact__c = :newContact.id];
     
      //The two parameters in the ContactTeamRecordsCountClass will keep track of the insertions/deletetions/updates and will be the expected 
      //values for Total_Contact_Relationship_Count__c and Active_Contact_Relationship_Count__c (except for bulk testing in TEST NUMBER 8 below where another pair of parameters will be used).         
      
      //This section takes into account any Contact_Team__c records created by other Triggers
      for (Contact_Team__c contactTeam : contactTeamList) {
        contactTeamRecordsCounters.contactTeamRecordsCount++;
        if (contactTeam.Relationship_Owner__r.isActive=true) {
          contactTeamRecordsCounters.contactTeamRecordsActiveCount++;       
        }
      }
   }

   //Set up data for test
   contactTeamRecord4_Active = insertContactTeam_Active(newContact, activeUser1, contactTeamRecordsCounters);    
      
   Test.startTest();
     
   //TEST NUMBER 3a - afterUpdate #2       
   updateContactTeam_MakeInactive(contactTeamRecord4_Active, activeUser1, contactTeamRecordsCounters);    

   //TEST NUMBER 3b - afterUpdate #3
   updateContactTeam_MakeActive(contactTeamRecord4_Active, activeUser1, contactTeamRecordsCounters);    
          
   Test.stopTest();
   
   checkContactTotals(newContact, contactTeamRecordsCounters, '3');  
    
  }
  
  //UTILITY METHODS
  private static Contact createDataForTest(ContactTeamRecordsCountClass contactTeamRecordsCounters) {
     Global_Settings__c custSettings = new Global_Settings__c(name= Constants.GLOBAL_SETTING, Account_Team_Member_Default_Role__c = 'Sales Rep');
     insert custSettings;
       
     Account newAccount = new Account(Name = 'TestAccount001', BillingCountry = 'Sweden');
     insert newAccount;
     
     Contact newContact = new Contact(FirstName = 'Bob', LastName = 'Smith', accountId=newAccount.Id);     
     insert newContact;   
     //The Contact Trigger will add a Contact_Team__c record associated with an Active user.     
     
     //This section takes into account any Contact_Team__c records created by Triggers fired as a result of creating the data for the test
     List<Contact_Team__c> contactTeamList = [SELECT id, Relationship_Owner__r.isActive FROM Contact_Team__c WHERE Contact__c = :newContact.id];     
     
     //The two parameters in the ContactTeamRecordsCountClass will keep track of the insertions/deletetions/updates and will be the expected 
     //values for Total_Contact_Relationship_Count__c and Active_Contact_Relationship_Count__c (except for bulk testing in TEST NUMBER 8 below where another pair of parameters will be used).
     
     //This section takes into account any Contact_Team__c records created by other Triggers
     for (Contact_Team__c contactTeam : contactTeamList) {
       contactTeamRecordsCounters.contactTeamRecordsCount++;
       if (contactTeam.Relationship_Owner__r.isActive=true) {
         contactTeamRecordsCounters.contactTeamRecordsActiveCount++;       
       }
     }
     
     return newContact;
  }
  
  //This method will create and return a Contact_Team__c record with an association with an Active user, updating the counters used to check the counters
  //on the parent Contact record (For testing afterInsert functionality of ContactTeamTrigger)
  //Used by TEST NUMBER 1 - afterInsert #1, 
  //        TEST NUMBER 5 - afterInsert #3
  private static Contact_Team__c insertContactTeam_Active(Contact newContact, User activeUser, ContactTeamRecordsCountClass contactTeamRecordsCounters) {
    Contact_Team__c newContactTeam = new Contact_Team__c(Contact__c = newContact.id, Relationship_Owner__c = activeUser.ID);
    List<Contact_Team__c> contactTeamsToInsert = new List<Contact_Team__c>();
    try {
      contactTeamsToInsert.add(newContactTeam);       
      insert contactTeamsToInsert;
       
      contactTeamRecordsCounters.contactTeamRecordsCount++;
      contactTeamRecordsCounters.contactTeamRecordsActiveCount++;
    } 
    catch(DMLException e) {
      system.debug('Could not insert active Contact_Team__c record: ' + e.getMessage());
    }
    return newContactTeam;
  }

  //This method will create and return a Contact_Team__c record with an association with an inActive user, updating one of the counters used to check one of the counters
  //on the parent Contact record (For testing afterInsert functionality of ContactTeamTrigger)
  //Used by TEST NUMBER 2 - afterInsert #2 
  private static Contact_Team__c insertContactTeam_Inactive(Contact newContact, User inactiveUser, ContactTeamRecordsCountClass contactTeamRecordsCounters) {
    Contact_Team__c newContactTeam = new Contact_Team__c(Contact__c = newContact.id, Relationship_Owner__c = inactiveUser.ID);
    List<Contact_Team__c> contactTeamsToInsert = new List<Contact_Team__c>();
    
    try {
      contactTeamsToInsert.add(newContactTeam);
      insert contactTeamsToInsert;
       
      contactTeamRecordsCounters.contactTeamRecordsCount++;     
    }
    catch(DMLException e) {
      system.debug('Could not insert inactive Contact_Team__c record: ' + e.getMessage());
    }
    return newContactTeam;
  }
  
  //This method will delete a Contact_Team__c record with an association with an Active user, updating the counters used to check the counters
  //on the parent Contact record (For testing afterDelete functionality of ContactTeamTrigger)
  //Used by TEST NUMBER 3 - afterDelete #1   
  private static void deleteContactTeam_Active(Contact_Team__c contactTeamRecord, ContactTeamRecordsCountClass contactTeamRecordsCounters) {    
    List<Contact_Team__c> contactTeamsToDelete = new List<Contact_Team__c>();
    try {
      contactTeamsToDelete.add(contactTeamRecord);
      delete contactTeamsToDelete;       
      
      contactTeamRecordsCounters.contactTeamRecordsCount--;
      contactTeamRecordsCounters.contactTeamRecordsActiveCount--;
    } 
    catch(DMLException e) {
      system.debug('Could not delete active Contact_Team__c record: ' + e.getMessage());
    }
  }

  //This method will delete a Contact_Team__c record with an association with an inActive user, updating the counters used to check the counters
  //on the parent Contact record (For testing afterDelete functionality of ContactTeamTrigger)
  //Used by TEST NUMBER 4 - afterDelete #2
  private static void deleteContactTeam_inActive(Contact_Team__c contactTeamRecord, ContactTeamRecordsCountClass contactTeamRecordsCounters) {
    List<Contact_Team__c> contactTeamsToDelete = new List<Contact_Team__c>();
    try {    
      contactTeamsToDelete.add(contactTeamRecord);
      delete contactTeamsToDelete;     
       
      contactTeamRecordsCounters.contactTeamRecordsCount--;
    } 
    catch(DMLException e) {
      system.debug('Could not delete inactive Contact_Team__c record: ' + e.getMessage());
    }
  }
  
  //Test what happens when the contactTeamRecord.Relationship_Owner__c field is set to 'NULL' ()
  //Used by TEST NUMBER 6 - afterUpdate #1  
  private static void updateContactTeam_Active(Contact_Team__c contactTeamRecord, ContactTeamRecordsCountClass contactTeamRecordsCounters) {
    List<Contact_Team__c> contactTeamsBeforeUpdate = new List<Contact_Team__c>();
    List<Contact_Team__c> contactTeamsAfterUpdate = new List<Contact_Team__c>();
    
    Contact_Team__c contactTeamRecordBeforeUpdate = new Contact_Team__c(Relationship_Owner__c = contactTeamRecord.Relationship_Owner__c);     
    contactTeamRecord.Relationship_Owner__c = NULL;
    
    contactTeamsBeforeUpdate.add(contactTeamRecordBeforeUpdate);
    contactTeamsAfterUpdate.add(contactTeamRecord);
    
    try {            
      update contactTeamRecord;
      
      contactTeamRecordsCounters.contactTeamRecordsActiveCount--;       
    }
    catch(DMLException e) {
      system.debug('Could not update active Contact_Team__c record: ' + e.getMessage());
    }
  }
            
  //This will insert the specified number of Contact_Team__c records into a list
  //TEST NUMBER 7 - afterInsert (bulk)  
  private static void insertContactTeamBulk(Contact newContact, Profile p, Integer numberRecordsToInsert, Boolean userIsActive, ContactTeamRecordsCountClass contactTeamRecordsCounters) {
       
    List<Contact_Team__c> contactTeamListForBulkTest = new List<Contact_Team__c>();
    List<User> usersForBulkInsert = Test_Utils.CreateUsers(p, 'test@experian.com', 'Test', numberRecordsToInsert);
    for (User userForBulkInsert : usersForBulkInsert) {
      userForBulkInsert.isActive = userIsActive;
    }
    try {     
      insert usersForBulkInsert;     
    }
    catch(DMLException e) {
      system.debug('Could not insert User records in bulk: ' + e.getMessage());
    }
     
    Integer i;     
    for (i = 0; i < numberRecordsToInsert; i++) {
      Contact_Team__c newContactTeamForBulk = new Contact_Team__c(Contact__c = newContact.id, Relationship_Owner__c = usersForBulkInsert[i].ID);
      contactTeamListForBulkTest.add(newContactTeamForBulk);    
    }
     
    try {
      insert contactTeamListForBulkTest;
      system.debug('Just inserted ' + contactTeamListForBulkTest.size() + 'Contact_Team__c records.');
       
      for (Contact_Team__c contactTeam : contactTeamListForBulkTest) {
        contactTeamRecordsCounters.contactTeamRecordsCount++;
        if (userIsActive == true) {
          contactTeamRecordsCounters.contactTeamRecordsActiveCount++;
        } 
      } 
    } 
    catch(DMLException e) {
      system.debug('Could not insert Contact_Team__c records in bulk: ' + e.getMessage());
    }
  }

  //TEST NUMBER 8 - afterDelete (bulk)
  private static void deleteContactTeams_with_inactiveUserRecords(ContactTeamRecordsCountClass contactTeamRecordsCounters) {
      
    List<Contact_Team__c> contactTeamsToDelete = [SELECT id, Contact__c, Relationship_Owner__r.isActive FROM Contact_Team__c WHERE Relationship_Owner__r.isActive=false];
    Integer numberOfContactTeamsToDelete = contactTeamsToDelete.size();
    try { 
      delete contactTeamsToDelete;
      
      contactTeamRecordsCounters.contactTeamRecordsCount = contactTeamRecordsCounters.contactTeamRecordsCount - numberOfContactTeamsToDelete;
    } 
    catch(DMLException e) {
      system.debug('Could not delete Contact_Team__c records in bulk: ' + e.getMessage());
    }
  }

  //This method will set the User in the Relationship_Owner__c field of a Contact_Record__c to isActive = false (For testing afterUpdate functionality of ContactTeamTrigger)
  //Used by TEST NUMBER 9 - afterUpdate #2   
  private static void updateContactTeam_MakeInactive(Contact_Team__c contactTeamRecord, User activeUser, ContactTeamRecordsCountClass contactTeamRecordsCounters) {
    List<Contact_Team__c> contactTeamsBeforeUpdate = new List<Contact_Team__c>();
    List<Contact_Team__c> contactTeamsAfterUpdate = new List<Contact_Team__c>();
    
    Contact_Team__c contactTeamRecordBeforeUpdate = new Contact_Team__c(Relationship_Owner__c = contactTeamRecord.Relationship_Owner__c);     
    
    contactTeamsBeforeUpdate.add(contactTeamRecordBeforeUpdate);
    contactTeamsAfterUpdate.add(contactTeamRecord);
    
    try {
      system.runAs(new User(Id = UserInfo.getUserId())) { 
        updateUser(activeUser.id, false);
      }
      
      contactTeamRecordsCounters.contactTeamRecordsActiveCount--;       
    } 
    catch(DMLException e) {
      system.debug('Could not update active Contact_Team__c record: ' + e.getMessage());
    }
  } 
  
  //This method will set the User in the Relationship_Owner__c field of a Contact_Record__c to isActive = true (For testing afterUpdate functionality of ContactTeamTrigger)
  //Used by TEST NUMBER 10 - afterUpdate #3     
  private static void updateContactTeam_MakeActive(Contact_Team__c contactTeamRecord, User activeUser, ContactTeamRecordsCountClass contactTeamRecordsCounters) {
    List<Contact_Team__c> contactTeamsBeforeUpdate = new List<Contact_Team__c>();
    List<Contact_Team__c> contactTeamsAfterUpdate = new List<Contact_Team__c>();
    
    Contact_Team__c contactTeamRecordBeforeUpdate = new Contact_Team__c(Relationship_Owner__c = contactTeamRecord.Relationship_Owner__c);    
    contactTeamRecord.Relationship_Owner__c = activeUser.Id;
        
    contactTeamsBeforeUpdate.add(contactTeamRecordBeforeUpdate);
    contactTeamsAfterUpdate.add(contactTeamRecord);
    
    Map<ID, User> oldUserMap = new Map<ID, User>{activeUser.ID => activeUser};    
        
    try{ 
      system.runAs(new User(Id = UserInfo.getUserId())) {  
        updateUser(activeUser.id, true); 
      }
      contactTeamRecordsCounters.contactTeamRecordsActiveCount++;        
    } 
    catch(DMLException e) {
      system.debug('Could not update inactive Contact_Team__c record: ' + e.getMessage());
    }
  } 
   
  private static void updateUser(ID userID, Boolean isActive) {
    List<User> userToUpdate = [SELECT id, isActive FROM User WHERE ID =:userID LIMIT 1];
    system.debug('Inside updateUser()');
        
    if (!userToUpdate.isEmpty()) {
      userToUpdate[0].isActive = isActive;
      update UserToUpdate[0];
      system.debug('User.isActive just updated to ' + isActive);
    }    
  }
   

  //This method will check the values of the parent Contact record following afterInsert, afterUpdate and afterDelete actions of the ContactTeamTrigger Trigger
  //The assertions will fail if the counter values on the parent Contact record are not equal to the counters used in this Test Class.
  //Used by TEST NUMBER 1..11
  private static void checkContactTotals(Contact contactToTest, ContactTeamRecordsCountClass contactTeamRecordsCounters, String testNumber) {
    List<Contact> updatedContacts = [
      SELECT Name, Total_Contact_Relationship_Count__c, Active_Contact_Relationship_Count__c 
      FROM Contact 
      WHERE ID = :contactToTest.id 
      LIMIT 1
    ];
    
    if (updatedContacts.size() > 0) {
      for (Contact updatedContact : updatedContacts) {
        system.assertEquals(contactTeamRecordsCounters.contactTeamRecordsCount, updatedContact.Total_Contact_Relationship_Count__c,  'TEST NUMBER' + testNumber + ' Contact:\'' + updatedContact.Name + '\': updatedContact.Total_Contact_Relationship_Count__c not equal to expected value of '  + contactTeamRecordsCounters.contactTeamRecordsCount  + '. Value is: ' + updatedContact.Total_Contact_Relationship_Count__c);
        system.assertEquals(contactTeamRecordsCounters.contactTeamRecordsActiveCount, updatedContact.Active_Contact_Relationship_Count__c, 'TEST NUMBER' + testNumber + ' Contact:\'' + updatedContact.Name + '\': updatedContact.Active_Contact_Relationship_Count__c not equal to expected value of ' + contactTeamRecordsCounters.contactTeamRecordsActiveCount + '. Value is: ' + updatedContact.Active_Contact_Relationship_Count__c);     
        //Use these debug statements for checking the values produced by this method - when uncommented they could make the log hit the 2MB limit if more than 4 or 5 tests carried out (test will still run but some debug statements could be missing).
        //System.debug('TEST NUMBER' + testNumber + ' Contact:\'' + updatedContact.Name + '\': updatedContact.Total_Contact_Relationship_Count__c value is: '  + updatedContact.Total_Contact_Relationship_Count__c + '. Expected value is: ' +  contactTeamRecordsCounters.contactTeamRecordsCount);
        //System.debug('TEST NUMBER' + testNumber + ' Contact:\'' + updatedContact.Name + '\': updatedContact.Active_Contact_Relationship_Count__c value is: ' + updatedContact.Active_Contact_Relationship_Count__c + '. Expected value is: ' + contactTeamRecordsCounters.contactTeamRecordsActiveCount);
      }
    }
  }
  //UTILITY METHODS
  
}