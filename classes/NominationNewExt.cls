/**=====================================================================
 * Experian
 * Name: NominationNewExt
 * Description: Extension for NominationNew page
 * Created Date: 6th Oct, 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By                Description of the update
 * 2nd Nov, 2016      Paul Kissick               New option of Category when adding
 * 7th Nov, 2016      Paul Kissick               Added comments, and reorganised class
 * 11th Nov, 2016     Paul Kissick               I409: Fixed issue with knowning which recipient caused an exception.
 =====================================================================*/
public class NominationNewExt {
  
  public class NominationException extends Exception {}
  
  //===========================================================================
  // Inner class describing the NomEntry (Nomination Entry) information stored.
  //===========================================================================
  public class NomEntry {
    public String nomName {get; set;} // Name
    public String nomId {get; set;} // Id
    public String nomPhotoUrl {get; set;} // Profile Photo
    public String nomTitle {get; set;} // Title Photo  
    public NomEntry() {
    }
  }
  
  private String nomStatus = NominationHelper.NomConstants.get('Submitted'); // Standard status for new nominations
  
  public String justificationText {get; set;} // Message / Justification
  public String teamName {get; set;} // Team Name
  
  public String spotAward {get; set;} // Holds the selected spot award value
  
  public String badgeId {get; set;} // Holds the selected badge id to apply
  
  public String selectNomType { get { if (selectNomType == null) selectNomType = ''; return selectNomType; } set; }
  
  public String selectedCategory { get { if (selectedCategory == null) selectedCategory = ''; return selectedCategory; } set; }
  
  private ApexPages.StandardController stdController;
  
  // Hold the Team Sponsor information
  public NomEntry sponsorEntry {get { if (sponsorEntry == null) { sponsorEntry = new NomEntry(); } return sponsorEntry; } set; }
  
  public List<NomEntry> allNomEntries {get{
    if (allNomEntries == null) {
      allNomEntries = new List<NomEntry>{new NomEntry()};
    }
    return allNomEntries;
  }set;}
  
  // Custom error messages to return to the page in a more friendly format.
  public List<String> errorMessages { get { if (errorMessages == null) errorMessages = new List<String>(); return errorMessages; } set; }
  public List<String> confirmMessages { get { if (confirmMessages == null) confirmMessages = new List<String>(); return confirmMessages; } set; }
  
  //===========================================================================
  // Returns all the Recognition specific badges.
  //===========================================================================
  public List<WorkBadgeDefinition> recogBadges {
    get {
      if (recogBadges == null) {
        recogBadges = [
          SELECT Id, Name, IsCompanyWide, Description, ImageUrl, IsActive, Recognition_Category__c, Recognition_Detail__c
          FROM WorkBadgeDefinition
          WHERE IsActive = true 
          AND IsCompanyWide = true
          AND Recognition_Category__c != null
          ORDER BY Name
        ];
      }
      return recogBadges;
    }
    set;
  }
  
  // Constructor
  public NominationNewExt() {
  }
  // Constructor
  public NominationNewExt(ApexPages.StandardController s) {
    stdController = s;
  }
  
  //===========================================================================
  // Simple getter to return all subordinates of the current user in a string
  // in JSON format. e.g. ["00500100101800sla","005.....",...]
  //===========================================================================
  public String getSubordinatesJson() {
    List<Id> subIds = new List<Id>();
    Map<Id, User> mySubs = new Map<Id, User>([
      SELECT Id
      FROM User
      WHERE IsActive = true
      AND ManagerId = :UserInfo.getUserId()
    ]);
    subIds.addAll(mySubs.keySet());
    return JSON.serialize(subIds);
  }
  
  //===========================================================================
  // Customised Types list, using preset values, for a given value.
  // Helps make the code cleaner on the VF page.
  //===========================================================================
  public List<SelectOption> getNominationTypes() {
    List<SelectOption> so = new List<SelectOption>();
    so.add(new SelectOption('', 'Choose...')); // Change to label
    for (Schema.PicklistEntry nt : Nomination__c.Type__c.getDescribe().getPicklistValues()) {
      if (nt.getValue().equalsIgnoreCase(NominationHelper.NomConstants.get('Level1ProfileBadge'))) {
        so.add(new SelectOption('1', nt.getLabel()));
      }
      if (nt.getValue().equalsIgnoreCase(NominationHelper.NomConstants.get('Level2SpotAward'))) {
        so.add(new SelectOption('2', nt.getLabel()));
      }
      if (nt.getValue().equalsIgnoreCase(NominationHelper.NomConstants.get('Level3Individual'))) {
        so.add(new SelectOption('3a', nt.getLabel()));
      }
      if (nt.getValue().equalsIgnoreCase(NominationHelper.NomConstants.get('Level3Team'))) {
        so.add(new SelectOption('3b', nt.getLabel()));
      }
      if (nt.getValue().equalsIgnoreCase(NominationHelper.NomConstants.get('Level4Elite')) && NominationHelper.isRecogCoord == true) {
        so.add(new SelectOption('4', nt.getLabel()));
      }
    }
    return so;
  }
  
  //===========================================================================
  // Returns the spot award options, with conversions to the users currency
  //===========================================================================
  public List<SelectOption> getSpotAwards() {
    List<SelectOption> so = new List<SelectOption>();
    so.add(new SelectOption('','Choose...')); // TODO: Change to label
    
    String myCurr = UserInfo.getDefaultCurrency();
    Decimal exchRate = 1.0;
    if (myCurr != Constants.CORPORATE_ISOCODE) {
      exchRate = [SELECT IsoCode, ConversionRate FROM CurrencyType WHERE IsoCode = :myCurr].ConversionRate;
    }
    
    for (Schema.PicklistEntry pe : Nomination__c.Spot_Award_Amount__c.getDescribe().getPicklistValues()) {
      String peLabel = pe.getLabel();
      if (myCurr != Constants.CORPORATE_ISOCODE) {
        Decimal usdVal = Decimal.valueOf(peLabel.removeStart('$'));
        peLabel += ' ('+myCurr + ' ' + (usdVal * exchRate).round(System.RoundingMode.HALF_UP)+')';
      }
      so.add(new SelectOption(pe.getValue(), peLabel));
    }
    return so;
  }
  
  //===========================================================================
  // Returns all the 'category__c' options.
  //===========================================================================
  public List<SelectOption> getCategories() {
    List<SelectOption> so = new List<SelectOption>();
    so.add(new SelectOption('', 'Choose...')); // TODO: Change to label
    for (Schema.PicklistEntry pe : Nomination__c.Category__c.getDescribe().getPicklistValues()) {
      so.add(new SelectOption(pe.getValue(), pe.getLabel()));
    }
    return so;
  }
  
  //===========================================================================
  // Recent Nominations Listing
  //===========================================================================
  public List<Nomination__c> getRecentNominationList() {
    return [
      SELECT Nominee__c, Nominee__r.Name, Nominee__r.SmallPhotoUrl, Nominee__r.FullPhotoUrl, Type__c, Nominee__r.Title 
      FROM Nomination__c
      WHERE Nominee__c != null
      ORDER BY CreatedDate DESC
      LIMIT 5
    ];
  }
  
  //===========================================================================
  // On page load, if the user has the Permission Set, Recognition Data Admin,
  // redirect to the standard 'new' page, instead of this one.
  //===========================================================================
  public PageReference checkDataAdmin() {
    if (NominationHelper.isRecogDataAdmin) {
      PageReference pr = new PageReference('/'+Nomination__c.sObjectType.getDescribe().getKeyPrefix()+'/e');
      pr.getParameters().put('nooverride','1');
      pr.getParameters().put('retURL','/'+Nomination__c.sObjectType.getDescribe().getKeyPrefix()+'/o');
      pr.setRedirect(true);
      return pr;
    }
    return null;
  }
  
  //===========================================================================
  // If the 'last' nomEntry is not blank, add another empty one.
  //===========================================================================
  public PageReference addAnother() {
    if (String.isNotBlank(allNomEntries.get(allNomEntries.size()-1).nomId)) {
      allNomEntries.add(new NomEntry());
    }
    return null;
  }
  
  //===========================================================================
  // Submit this form, added extra verifications.
  //===========================================================================
  public PageReference submitNominationRec() {
    
    Boolean errorFound = false;
    errorMessages = new List<String>();
    confirmMessages = new List<String>();
    
    if (String.isBlank(allNomEntries.get(0).nomId)) {
      errorFound = true;
      errorMessages.add('Add someone to thank or reward.');  // TODO: Change to label
    }
    
    if (String.isBlank(badgeId)) {
      errorFound = true;
      errorMessages.add('No badge selected.');
    }
    
    if (errorFound) {
      return null;
    }
    
    Savepoint sp = Database.setSavepoint();
    try {
      // Profile Badge Only
      if ('1'.equals(selectNomType)) {
        Map<String, Id> recipIdMap = new Map<String, Id>();
        Map<String, Id> giverIdMap = new Map<String, Id>();
        Map<String, String> msgMap = new Map<String, String>();
        Map<String, Id> badgeIdMap = new Map<String, Id>();

        for (NomEntry ne : allNomEntries) {
          if (String.isNotBlank(ne.nomId)) {
            recipIdMap.put(ne.nomId, ne.nomId);
            giverIdMap.put(ne.nomId, UserInfo.getUserId());
            msgMap.put(ne.nomId, justificationText);
            badgeIdMap.put(ne.nomId, badgeId);
          }
        }

        Boolean allGood = NominationHelper.postBadgeToProfilesMany(
          recipIdMap,
          giverIdMap,
          msgMap,
          badgeIdMap,
          null
        );
        if (allGood) {
          resetForm();
          confirmMessages.add('Badge'+(recipIdMap.size()>1 ? 's' : '')+' posted to profile.'); // TODO: Change to label
        }
        else {
          errorMessages.add('Failed to give badge'+(recipIdMap.size()>1 ? 's' : '')+'.'); // TODO: Change to label
        }
      }
      
      // SPOT AWARD OR INDIV HALF YEAR OR ELITE NOM
      if ('2'.equals(selectNomType) || '3a'.equals(selectNomType) || '4'.equals(selectNomType)) {
        Id nomUserId;
        Boolean isSubord = false;
        try {
          nomUserId = (Id)allNomEntries.get(0).nomId;
        }
        catch (Exception e) {
        }
        Nomination__c nom = new Nomination__c(
          Status__c = nomStatus,
          Requestor__c = UserInfo.getUserId(),
          Nominee__c = nomUserId,
          Badge__c = (Id)badgeId,
          Justification__c = justificationText,
          Category__c = selectedCategory
        );
        if ('2'.equals(selectNomType)) {
          nom.Type__c = NominationHelper.NomConstants.get('Level2SpotAward');
        }
        else if ('3a'.equals(selectNomType)) {
          nom.Type__c = NominationHelper.NomConstants.get('Level3Individual');
        }
        else if ('4'.equals(selectNomType)) {
          nom.Type__c = NominationHelper.NomConstants.get('Level4Elite');
        }
        if (String.isNotBlank(getSubordinatesJson()) && getSubordinatesJson().contains(nom.Nominee__c)) {
          if ('2'.equals(selectNomType)) {
            nom.Spot_Award_Amount__c = spotAward;
          }
          isSubord = true;
        }
        try {
          insert nom;
          system.debug('Tyaga the nomination was submitted' + nom);
          String successMsg = 'Nomination received.'; // TODO: Change to label
          if (isSubord) {
            if ('2'.equals(selectNomType)) {
              successMsg = 'Spot Award awarded successfully.'; // TODO: Change to label
            }
            if ('3a'.equals(selectNomType)) {
              successMsg = 'Half Year Nomination awarded successfully.'; // TODO: Change to label
            }
          }
          if ('4'.equals(selectNomType)) {
            successMsg = 'Elite Winner awarded successfully'; // TODO: Change to label
          }
          resetForm();
          confirmMessages.add(successMsg);
        }
        catch (DMLException ex) {
          Database.rollBack(sp);
          ApexPages.addMessages(ex);
        }
      }
      
      // TEAM NOMINATION....
      if ('3b'.equals(selectNomType)) {
        Set<Id> nomIdSet = new Set<Id>();
        for (NomEntry ne : allNomEntries) {
          if (String.isNotBlank(ne.nomId)) {
            try {
              nomIdSet.add((Id)ne.nomId);
            }
            catch (Exception e) {
              system.debug('Trouble converting this to an id: '+ne.nomId);
            }
          }
        }
        createTeamNomination((Id)sponsorEntry.nomId, nomIdSet, teamName, (Id)badgeId, justificationText, selectedCategory, sp);
      }
    }
    catch (Exception ex) {
      ApexPages.addMessages(ex);
    }
    for (ApexPages.Message msg : ApexPages.getMessages()) {
      errorMessages.add(msg.getSummary());
    }
    if (!confirmMessages.isEmpty() && errorMessages.isEmpty()) {
      PageReference pr = Page.NominationHome;
      pr.getParameters().put('msg', String.join(confirmMessages,'\n'));
      return pr;
    }
    return null;
  }
  
  public PageReference resetForm() {
    justificationText = null;
    spotAward = null;
    teamName = null;
    allNomEntries = new List<NomEntry>{new NomEntry()};
    sponsorEntry = new NomEntry();
    errorMessages = new List<String>();
    badgeId = null;
    confirmMessages = new List<String>();
    selectNomType = null;
    selectedCategory = null;
    return null;
  }
  
  public void createTeamNomination(Id projSponId, Set<Id> nomineeIds, String teamName, Id badgeId, String jText, String categ, Savepoint sp) {
    try {
      List<Nomination__c> newNoms = new List<Nomination__c>();
      Nomination__c masterNomination = new Nomination__c(
        Type__c = NominationHelper.NomConstants.get('Level3Team'),
        Status__c = nomStatus,
        Requestor__c = UserInfo.getUserId(),
        Justification__c = jText,
        Project_Sponsor__c = projSponId,
        Team_Name__c = teamName,
        Badge__c = badgeId,
        Category__c = categ
      );
      Database.SaveResult resMaster = Database.insert(masterNomination, false);
      if (!resMaster.isSuccess()) {
        for (Database.Error err : resMaster.getErrors()) {
          throw new NominationException(err.getMessage());
        }
      }
      for (Id userId : nomineeIds) {
        Nomination__c membNom = masterNomination.clone(false, true, false, false);
        membNom.Nominee__c = userId;
        membNom.Master_Nomination__c = masterNomination.Id;
        newNoms.add(membNom);
      }
      List<Database.SaveResult> cNomRes = Database.insert(newNoms, false);
      Map<Id, List<String>> nomineesWithErrors = new Map<Id, List<String>>();
      Integer idx = 0;
      for (Database.SaveResult res : cNomRes) {
        if (!res.isSuccess()) {
          nomineesWithErrors.put(newNoms.get(idx).Nominee__c, new List<String>());
          for (Database.Error err : res.getErrors()) {
            nomineesWithErrors.get(newNoms.get(idx).Nominee__c).add(err.getMessage());
          }
        }
        idx++;
      }
      if (!nomineesWithErrors.isEmpty()) {
        String excDetails = '';
        Map<Id, User> nNameMap = new Map<Id, User>([
          SELECT Id, Name
          FROM User
          WHERE Id IN :nomineesWithErrors.keySet()
        ]);
        for (Id nId : nomineesWithErrors.keySet()) {
          excDetails += 'Recipient: '+nNameMap.get(nId).Name + ' - '+String.join(nomineesWithErrors.get(nId), '; ') + '\n'; // TODO: Change recipient to label
        }
        throw new NominationException(excDetails);
      }
      resetForm();
      confirmMessages.add('Team Nomination received.'); // TODO: Change to label
    }
    catch (NominationException ex) {
      Database.rollBack(sp);
      ApexPages.addMessages(ex);
    }
    catch (DMLException ex) {
      Database.rollBack(sp);
      ApexPages.addMessages(ex);
    }
  }
 
}