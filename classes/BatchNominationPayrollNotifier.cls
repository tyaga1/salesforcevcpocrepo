/*=============================================================================
 * Experian
 * Name: BatchNominationPayrollNotifier
 * Description: Sends email to the Payroll recipients for any valid nominations.
                
                An email template, 'General Payroll Notification', is used to bespoke the email.
                
                Attachments are sent in XLS/HTML format or CSV format.
                
                ** Batches are currently limited to 200 per report/email.
                ** It is possible to increase this to 1000, but that is the limit supported by the updater VF page.
                
                When executed in a sandbox, we prefix and use different email addresses to make
                sure these files are not processed where possible.
                
                It's possible to configure multiple runs for a payroll be creating extra 'Employee
                Payroll Recognition' records with different days.
                
                The Label of the EPR record is used as the recipient name.
                
                Schedule is created managed through this single batch class.
                
                Start this using (to run at 1am daily)
                *** Please ensure the user running this is in the GMT timezone. ***
                
                system.schedule('Employee Recognition - Send Payroll Emails', '0 0 1 * * ?', new BatchNominationPayrollNotifier());
                
                
                
 * Created Date: 16 Sep 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * Sep 19th, 2016     Paul Kissick          Added checks for running in a sandbox
 * Sep 22nd, 2016     Paul Kissick          Added support for separate report layouts
 * Nov 11th, 2016     Paul Kissick          Request to order report by Employee Number (which is a text field!)
 * Nov 15th, 2016     Paul Kissick          Resolving issues found through mass report sending.
 * Nov 15th, 2016     Paul Kissick          Adding support for either CSV or XLS files
 * Feb 23rd, 2017     Tyaga Pati            Code changes to Take in to Account eligible record count before sending out Reminder emails.
 =============================================================================*/

public class BatchNominationPayrollNotifier implements Database.Batchable<sObject>, Database.Stateful, Schedulable {

  public Enum RunProcess {
    PrepareReport,            // 1. Prepare batches to be sent during SendReport phase
    SendReport,            // 2. Send report emails for rewards not already notified 
    SendReminder           // 3. Send reminder emails for rewards notified but with no grossed value yet.
  }
  
  public String sandboxEmailText = '<p style="color:red;"><b>THIS EMAIL IS NOT FROM PRODUCTION - DO NOT PROCESS</b></p>';
  public String sandboxEmailSubject = 'DO NOT PROCESS - ';
  public String currentPayroll;
  
  private Datetime processStartTime;
  
  public List<String> errorsFound;
  public List<String> empPayrolls;
  public List<String> batchGuidList;
  
  public List<RunProcess> startingProcesses;  // List of which processes to run for the whole batch of payroll 
  
  public String reportFileNameFormat = '{0}Rewards{1}.{2}'; // recipName+'Rewards'+DateTime.now().format('yyyyMMdd')+'.csv/.xls'

  public Boolean isSandbox = true; // Default to true and will be set to false when run in production.
  
  public EmailTemplate currentTemplate;
  public String currentBatchGuid;
  
  public Employee_Recognition_Payroll__mdt holdingPayroll;
  
  private PageReference reportPageRef;
  private RunProcess currentProcess;
  public RunProcess lastProcess;
  
  public List<RunProcess> remainingProcesses; // For the current payroll, what's left to run! 
  
  public class MyProcessException extends Exception {}
  
  public BatchNominationPayrollNotifier () {
    checkSandbox();
  }
  
  public BatchNominationPayrollNotifier (List<String> payrollDevNames, Datetime sTime) {
    empPayrolls = payrollDevNames;
    processStartTime = sTime;
    checkSandbox();
  }
  
  private void checkSandbox() {
    // Will populate the isSandbox variable
    isSandbox = [SELECT IsSandbox FROM Organization WHERE Id = :UserInfo.getOrganizationId()].IsSandbox;
  }
  
  //===========================================================================
  // Find all Payroll entries to process for the current day
  //===========================================================================
  public void execute (SchedulableContext sc) {
    startBatch(false);
  }
  
  public void startBatch(Boolean forceRun) {
    List<String> payrollsToRun = new List<String>();
    // Execution Day
    String dayOfMonth = String.valueOf(Date.today().day());
    
    List<String> whereClauses = new List<String>();
    whereClauses.add('Test_Case__c '+(Test.isRunningTest() ? '!=' : '=')+' null');
    if (forceRun == false) {
      whereClauses.add('Active__c = true');
      whereClauses.add('Day_of_the_Month__c = :dayOfMonth');
    }
    
    List<Employee_Recognition_Payroll__mdt> payrolls = (List<Employee_Recognition_Payroll__mdt>)Database.query(
      ' SELECT DeveloperName ' +
      ' FROM Employee_Recognition_Payroll__mdt ' + 
      (!whereClauses.isEmpty() ? ' WHERE '+String.join(whereClauses, ' AND ') : '')
    );
    
    if (payrolls.isEmpty()) {
      return;
    }
    
    for (Employee_Recognition_Payroll__mdt empPayr : payrolls) {
      payrollsToRun.add(empPayr.DeveloperName);
    }
       
    // Initialise the batch run for this day, and prepare all payrolls to run.
    system.debug('Tyaga : The function is being called with Payroll entried');
    BatchNominationPayrollNotifier b = new BatchNominationPayrollNotifier(payrollsToRun, Datetime.now());
    if (!Test.isRunningTest()) {
      Database.executeBatch(b, ScopeSizeUtility.getScopeSizeForClass('BatchNominationPayrollNotifier'));
    }
  }
  
  
  public void init() {
    
    if (processStartTime == null) {
      processStartTime = Datetime.now();
    }
    
    if (startingProcesses != null && (remainingProcesses == null || remainingProcesses.isEmpty())) {
    
      remainingProcesses = new List<RunProcess>(startingProcesses);
    
    }
    
    if (startingProcesses == null && remainingProcesses == null) {
      startingProcesses = new List<RunProcess>{
          RunProcess.PrepareReport, RunProcess.SendReport, RunProcess.SendReminder
        };
        remainingProcesses = new List<RunProcess>{
        RunProcess.PrepareReport, RunProcess.SendReport, RunProcess.SendReminder
      };
      
    }
    
    errorsFound = new List<String>();
    if (batchGuidList == null) {
      batchGuidList = new List<String>();
    }
  }
  
  public void loadEmailTemplate(String devName) {
    // Load the email template into memory
    currentTemplate = [
      SELECT Id, Subject, HtmlValue, Body 
      FROM EmailTemplate 
      WHERE DeveloperName = :devName
      LIMIT 1
    ];
    
  }
  
  //===========================================================================
  // Standard batch start, initialise some variables, load current payroll into
  // memory to run.
  //===========================================================================
  public Database.Querylocator start (Database.Batchablecontext bc) {
    
    init();
    
    if (!remainingProcesses.isEmpty()) {
    
      system.debug(batchGuidList);
      system.debug(lastProcess);
      if (batchGuidList != null && !batchGuidList.isEmpty() && (lastProcess == RunProcess.SendReport || lastProcess == RunProcess.PrepareReport)) {
        currentProcess = remainingProcesses.get(0);
      }
      if (currentProcess == null) {
         system.debug('tyaga : is this one called first?');
        currentProcess = remainingProcesses.remove(0);
      } 
    }
    
    system.debug(currentProcess);
    
    // Extract the next payroll dev name to process.
    if (empPayrolls != null && !empPayrolls.isEmpty()) {
      if (remainingProcesses.isEmpty()) {
        currentPayroll = empPayrolls.remove(0);
      }
      else {
        currentPayroll = empPayrolls.get(0);
      }
    }
    
    system.debug(currentPayroll);
    // Load this payroll into memory
    holdingPayroll = [
      SELECT DeveloperName, MasterLabel, Day_of_the_Month__c, Report_Layout__c, 
             Sandbox_Recipients__c, Recipients__c, Payroll_Reference__c,
             Email_Template_for_Report__c, Email_Template_for_Reminder__c,
             No_Gross_Up_Possible__c, Report_Type__c
      FROM Employee_Recognition_Payroll__mdt
      WHERE DeveloperName = :currentPayroll
    ];
    
    system.debug(holdingPayroll);
    system.debug(currentProcess);
    String payrollRef;
    
    if (holdingPayroll != null && holdingPayroll.DeveloperName != null) {
      payrollRef = holdingPayroll.Payroll_Reference__c;
    }
    
    if (currentProcess == RunProcess.PrepareReport) {
      return getValidNominations(payrollRef);
    }
    
    if (currentProcess == RunProcess.SendReport) {
      if (batchGuidList != null && !batchGuidList.isEmpty()) {
        currentBatchGuid = batchGuidList.remove(0);
      }
      system.debug(currentBatchGuid);
      loadEmailTemplate(holdingPayroll.Email_Template_for_Report__c);
      return getNominationsForBatch(payrollRef, currentBatchGuid);
    }
    
    if (currentProcess == RunProcess.SendReminder) {
      loadEmailTemplate(holdingPayroll.Email_Template_for_Reminder__c);
      //return getReminderNominations();
      //TP: Replaced with Below Function which will return the number of Payroll Records that still dont have gross amount updated.
      return getValidReminderNominations(payrollRef);
      
    }
    
    return getValidNominations('');
  }
  
  //===========================================================================
  // Return the valid nominations found from the payroll ref.Will return none
  // if the payrollRef is null/empty
  //===========================================================================
  private Database.Querylocator getValidNominations(String payrollRef) {
    return Database.getQueryLocator([
      SELECT Id
      FROM Nomination__c
      WHERE Oracle_Payroll_Name__c = :payrollRef
      AND Oracle_Payroll_Name__c != null
      AND Payroll_Notified__c = false
      AND Nominee__c != null
      AND Notify_Payroll__c = true  // Formula field to know if the reward is good to send to payroll.
      ORDER BY Employee_Number_Integer__c ASC
    ]);
  }
  
  //===========================================================================
  //===========================================================================
  private Database.Querylocator getNominationsForBatch(String payrollRef, String batchGuid) {
    if (String.isBlank(batchGuid)) {
      return Database.getQueryLocator([SELECT Id FROM Nomination__c WHERE Id = null]);
    }
    return Database.getQueryLocator([
      SELECT Id
      FROM Nomination__c
      WHERE Oracle_Payroll_Name__c = :payrollRef
      AND Oracle_Payroll_Name__c != null
      AND Payroll_Batch_GUID__c = :batchGuid
      AND Nominee__c != null
      AND Notify_Payroll__c = true  // Formula field to know if the reward is good to send to payroll.
      ORDER BY Employee_Number_Integer__c ASC
    ]);
  }
  
  //===========================================================================
  // Return the valid nominations found from the payroll ref. Will return none
  // if the payrollRef is null/empty
  //===========================================================================
  private Database.Querylocator getReminderNominations() {
    return Database.getQueryLocator([
      SELECT Id
      FROM Organization
    ]);
  }
    //===========================================================================
  // Return the valid nominations found from the payroll ref that should be part
  // of the reminder notification. if the payrollRef is null/empty
  //===========================================================================
  private Database.Querylocator getValidReminderNominations(String payrollRef) {
    return Database.getQueryLocator([
      SELECT Id
      FROM Nomination__c
      WHERE Oracle_Payroll_Name__c = :payrollRef
      AND Oracle_Payroll_Name__c != null
      AND Payroll_Notified_Date__c <:processStartTime
      AND Nominee__c != null
      AND Notify_Payroll__c = true
      AND Payroll_Notified__c = true
      AND Payroll_Gross_Amount__c = null      
    ]);
  }
  
  //===========================================================================
  // For all the nominations in the batch, send an email to the payroll with the
  // attached report.
  //===========================================================================
  public void execute (Database.BatchableContext bc, List<sObject> scope) {
    if (currentProcess == RunProcess.PrepareReport) {
      processPrepareReport((List<Nomination__c>)scope);
    }
    if (currentProcess == RunProcess.SendReport) {
      processSendReport((List<Nomination__c>)scope);
    }
    if (currentProcess == RunProcess.SendReminder) {
      //processSendReminder();
      system.debug('The process reminder is being called  ');
      processSendReminder((List<Nomination__c>)scope);
    }
  }
  
  //===========================================================================
  // For each batch, create a unique batch guid and add this to a var to access
  // later.
  //===========================================================================
  public void processPrepareReport(List<Nomination__c> scope) {
    // GUID for this batch, to add to the nominations and help with response processing.
    String batchGuid = BatchHelper.newGuid();
    
    // At this point, all the ids are good, so mark them as sent!
    for (Nomination__c nom : scope) {
      nom.Payroll_Notified__c = true;
      nom.Payroll_Notified_Date__c = Datetime.now();
      nom.Payroll_Batch_GUID__c = batchGuid;
    }
    List<String> saveErrors = new List<String>();
    List<Database.SaveResult> srList = Database.update(scope, true);
    for (Integer i = 0; i < srList.size(); i++) {
      if (!srList.get(i).isSuccess()) {
        // DML operation failed
        system.debug(srList.get(i).getErrors());
        saveErrors.add('Nomination Id: ' + scope.get(i).Id + '\n' + BatchHelper.parseErrors(srList.get(i).getErrors()));
      }
    }
    if (saveErrors.isEmpty()) {
      batchGuidList.add(batchGuid);
    }
    else {
      errorsFound.addAll(saveErrors);
    }
  }
  
  //===========================================================================
  // 
  //===========================================================================
  public void processSendReport (List<Nomination__c> scope) {
    // Quick way to get id from the scope
    List<Id> idsToPass = new List<Id>((new Map<Id, Nomination__c>(scope)).keySet());
    
    //
    // Render the report
    Blob reportContent = getSummaryFile(idsToPass, currentBatchGuid, holdingPayroll.Report_Layout__c, holdingPayroll.No_Gross_Up_Possible__c, false, holdingPayroll.Report_Type__c);
    
    // Validate the report content contains the Ids we've passed
    // Commenting out because this is no longer required.
    //if (!validateReportContent(reportContent, idsToPass)) {
      //errorsFound.add('Please review this report: ' + URL.getSalesforceBaseUrl().toExternalForm() + reportPageRef.getUrl());
      //return;
    //}
    
    // Used to select the 'sandbox' email address if running in a sandbox, i.e. not production data!
    String recipEmail = (isSandbox) ? holdingPayroll.Sandbox_Recipients__c : holdingPayroll.Recipients__c;
    
    if (String.isBlank(recipEmail)) {
      errorsFound.add('No recipients found for '+holdingPayroll.MasterLabel);
      return;
    }
    
    // Send the email to the payroll mailbox
    if (!sendPayrollEmail(holdingPayroll.MasterLabel, recipEmail, reportContent, false, holdingPayroll.Report_Type__c, currentBatchGuid)) {
      return;
    }
    
  }
  //TP: Changed Function Definition to make sure 
  public void processSendReminder (List<Nomination__c> scope) {
    // public void processSendReminder () {
    // Quick way to get id from the scope
    // List<Id> idsToPass = new List<Id>((new Map<Id, Nomination__c>(scope)).keySet());
    // 
    // payrollRef, processStartTime
    //
    // Render the report
    
    //Check if the Scope has any records then only proceed with the Reminder emails. Otherwise skip to the next payroll country.
    List<Id> idsToPass = new List<Id>((new Map<Id, Nomination__c>(scope)).keySet());
    if(idsToPass.isEmpty()){
       return;// Skip to next if there are no records associated with this payroll country.
    }
    Blob reportContent = getReminderReport(holdingPayroll.Payroll_Reference__c, holdingPayroll.Report_Layout__c, holdingPayroll.No_Gross_Up_Possible__c, processStartTime);
    
    // Validate the report content contains the Ids we've passed
    //if (!validateReportContent(reportContent, idsToPass)) {
    //  errorsFound.add('Please review this report: ' + URL.getSalesforceBaseUrl().toExternalForm() + reportPageRef.getUrl());
    //  return;
    //}
    
    // Used to select the 'sandbox' email address if running in a sandbox, i.e. not production data!
    String recipEmail = (isSandbox) ? holdingPayroll.Sandbox_Recipients__c : holdingPayroll.Recipients__c;
    
    if (String.isBlank(recipEmail)) {
      errorsFound.add('No recipients found for '+holdingPayroll.MasterLabel);
      return;
    }
    
    // Send the email to the payroll mailbox
    if (!sendPayrollEmail(holdingPayroll.MasterLabel, recipEmail, reportContent, true, '', '')) {
      return;
    }
  }
  
  //===========================================================================
  // To make sure the report generated contains the IDs we should expect.
  //===========================================================================
  /*
  public Boolean validateReportContent(Blob cont, List<Id> idsToCheck) {
    
    Boolean allGood = true;
    
    // The rendered blob needs to be checked as a string, so convert it.
    String contentStr = cont.toString();
    
    if (String.isBlank(contentStr)) {
      errorsFound.add('Report is blank!');
      allGood = false;
    }
    
    // No id's to check or something else wrong...
    if (idsToCheck == null || (idsToCheck != null && idsToCheck.isEmpty())) {
      errorsFound.add('IDs list is null or empty');
      allGood = false;
    }
    
    // Check each individual id.
    for (Id idToCheck : idsToCheck) {
      if (!contentStr.containsIgnoreCase((String)idToCheck)) {
        errorsFound.add(idToCheck + ' not found in report.');
        allGood = false;
      }
    }
    return allGood;
  }
  */
  
  
  //===========================================================================
  // FINISH
  //===========================================================================
  public void finish (Database.BatchableContext bc) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchNominationPayrollNotifier', false);
    
    if ((errorsFound != null && !errorsFound.isEmpty()) || Test.isRunningTest()) {
      bh.emailBody += 'The following errors were found while processing.\n\n';
      bh.emailBody += String.join(errorsFound, '\n');
      bh.batchHasErrors = true; // Make sure we tell the helper we've added errors!
      system.debug(bh.emailBody);
    }
    
    bh.sendEmail();
    
    // Prepare the next batch if there are more to do.
    if (empPayrolls != null && !empPayrolls.isEmpty()) {
      BatchNominationPayrollNotifier b = new BatchNominationPayrollNotifier(empPayrolls, processStartTime);
      b.remainingProcesses = remainingProcesses;
      b.startingProcesses = startingProcesses;
      b.lastProcess = currentProcess; // Store the current process as the 'last' process when this is started.
      b.batchGuidList = (batchGuidList != null && !batchGuidList.isEmpty()) ? batchGuidList : null;
      system.debug('remainingProcesses: '+remainingProcesses);
      system.debug('startingProcesses: '+startingProcesses);
      system.debug('currentProcess: '+currentProcess);
      system.debug('batchGuidList: '+batchGuidList);
      
      if (!Test.isRunningTest()) {
        Database.executeBatch(b, ScopeSizeUtility.getScopeSizeForClass('BatchNominationPayrollNotifier'));
      }
    }
  }
  
  //===========================================================================
  // Find and replace for [Variables] in the template, since we can't use {}
  //===========================================================================
  @testVisible private String replacePlaceholders(String inpStr, Map<String, String> placeHolders) {
    for (String placeHolder : placeHolders.keySet()) {
      inpStr = inpStr.replace('['+placeHolder+']', placeHolders.get(placeHolder));
    }
    return inpStr;
  }
  
  //===========================================================================
  // Send the email to the payroll recipient, with the attachment.
  //===========================================================================
  @testVisible private Boolean sendPayrollEmail (String recipName, String recipEmail, Blob attachBody, Boolean insertAttToBody, String reportType, String batchGuid) {
    
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    mail.setSaveAsActivity(false);
    
    List<String> recipients = new List<String>();
    if (String.isBlank(recipEmail)) {
      errorsFound.add('No recipients provided.');
      return false;
    }
    for (String recip : recipEmail.split(',')) {
      recipients.add(recip.trim());
    }
    mail.setToAddresses(recipients);
    
    PageReference updatePr = Page.NominationPayrollUpdate;
    updatePr.getParameters().put('guid', batchGuid);
    
    Map<String, String> placeHolders = new Map<String, String>{
      'OraclePayrollName' => recipName,
      'CurrentMonth' => DateTime.now().format('MMM yy'),
      'UpdateURL' => URL.getSalesforceBaseUrl().toExternalForm() + updatePr.getUrl()
    };
    
    String emailBodyHtml = replacePlaceholders(currentTemplate.HtmlValue, placeHolders);
    String emailSubject = replacePlaceholders(currentTemplate.Subject, placeHolders);
    
    emailBodyHtml = ((isSandbox) ? sandboxEmailText : '') + emailBodyHtml + ((isSandbox) ? sandboxEmailText : '');
    emailSubject = ((isSandbox) ? sandboxEmailSubject : '') + emailSubject;
    
    if (insertAttToBody == true) {
      String reportData = attachBody.toString();
      emailBodyHtml += reportData;
    }
    
    mail.setSubject(emailSubject);
    mail.setBccSender(false);
    mail.setUseSignature(false);
    mail.setHtmlBody(emailBodyHtml);
    
    if (insertAttToBody == false) {
        Messaging.EmailFileAttachment att = new Messaging.EmailFileAttachment();
        att.setBody(attachBody);
        att.setContentType('text/html');
        
        // Attachment file name, which may be changed.
        att.setFileName(String.format(reportFileNameFormat, new String[]{recipName, DateTime.now().format('yyyyMMdd'), ('CSV'.equals(reportType) ? 'csv' : 'xls')}));
        
        mail.setFileAttachments(new List<Messaging.EmailFileAttachment>{att});
    }
    
    if (String.isNotBlank(recipEmail) && recipEmail.endsWithIgnoreCase('@example.com')) {
      // If the email address is a tester, just return as true! 
      return true;
    }
    // We only send one email so this should return that success!
    Boolean isSuccess = false; 
    if (!Test.isRunningTest()) {
      List<Messaging.SendEmailResult> sendRes = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }, true);
      if (!sendRes.get(0).isSuccess()) {
        for (Messaging.SendEmailError sendErr : sendRes.get(0).getErrors()) errorsFound.add(sendErr.getMessage());
      }
      isSuccess = sendRes.get(0).isSuccess();
    }
    else {
      // Always return true for tests.
      // This is to get around the Send Email turned off within Sandbox environments.
      isSuccess = true;
    }
    return isSuccess;
    
  }
  
  @testVisible private Blob getReminderReport(String payrollName, String reportLayout, Boolean noGross, Datetime beforeDate) {
    Blob blobVal;
    PageReference pr;
    try {
      pr = page.NominationPayrollReminder;
      pr.getParameters().put('layout', reportLayout);
      pr.getParameters().put('payrollname', payrollName);
      pr.getParameters().put('beforedate', JSON.serialize(beforeDate));
      // pr.getParameters().put('nogross', (noGross) ? 'yes' : 'no');
      reportPageRef = pr;
      blobVal = (!Test.isRunningTest()) ? pr.getContent() : Blob.valueOf(' Here are the IDs listed so we can test... ');
      
      // Call this method to remove a Salesforce added <head> tag which is not compatible with excel xml importing.
      // blobVal = cleanHtml(blobVal);
    }
    catch (Exception e) {
      errorsFound.add(e.getMessage());
    }
    return blobVal;
  } 
  
  //===========================================================================
  // Generate the summary report/file and return as a blob.
  //===========================================================================
  @testVisible private Blob getSummaryFile(List<Id> idsToGet, String guid, String reportLayout, Boolean noGross, Boolean isReminder, String reportType) {
    Blob blobVal;
    PageReference pr;
    try {
      if (isReminder == false) {
        if (String.isNotBlank(reportType)) {
          if ('CSV'.equals(reportType)) {
            pr = Page.NominationPayrollReportCSV;
          }
        }
        // Default type is XLS, unless specified
        if ('XLS'.equals(reportType) || String.isBlank(reportType)) {
          pr = Page.NominationPayrollReportXLS;
        }
      }
      else {
        pr = page.NominationPayrollReminder;
      }
      pr.getParameters().put('layout', reportLayout);
      pr.getParameters().put('guid', guid);
      pr.getParameters().put('nogross', (noGross) ? 'yes' : 'no');
      reportPageRef = pr;
      blobVal = (!Test.isRunningTest()) ? pr.getContent() : Blob.valueOf(' Here are the IDs listed so we can test... '+String.join(idsToGet,', '));
      
      // Call this method to remove a Salesforce added <head> tag which is not compatible with excel xml importing.
      blobVal = cleanHtml(blobVal);
    }
    catch (Exception e) {
      errorsFound.add(e.getMessage());
    }
    return blobVal;
  } 
  
  //===========================================================================
  // Remove anything not compatible with Excel, e.g. the <head> tag.
  //===========================================================================
  private Blob cleanHtml(Blob input) {
    
    String blobStr = input.toString();
    
    // remove head tag, since it's not compatible...
    // <head><script src="/static/111213/js/perf/stub.js" type="text/javascript"></script></head>
    
    //Matcher regex = Pattern.compile('(?si)<head>.+?</head>').matcher(blobStr);
    String firstPart = blobStr.mid(0,500);
    
    Matcher regex = Pattern.compile('(?si)<head>.+?</head>').matcher(firstPart);
    
    String output = regex.replaceAll('') + blobStr.mid(500, blobStr.length());
    // String output = regex.replaceAll('');
    output = output.replace('ss:StyleID=""','');
    return Blob.valueOf(output);
    
  }
  
}