/**=====================================================================
 * Experian, Inc
 * Name:          AccountPlanHelperClass
 * Created Date:  Mar 19, 2016
 * Created By:    Tyaga Pati (Experian)
 * Description:   This is a Helper Class Called from the AccountPlanHierController Class to take a selected List of Account and 
                  a Primary Account and Create a Account plan record. This Account plan Record will have multiple Accounts
                  associated with it. 
 *                
 * Date Modified             Modified By                  Description of the update
 * Mar 19, 2016              Tyaga Pati(Experian)    
 * June 10, 2016             Tyaga Pati(Experian)         01976432 Added function for Account plan Edit Functionality:  
 =====================================================================*/ 
 
global class AccountPlanHelperClass {
   Public Static Account_Plan__c CreateAccountPlan(List<String> AccountList,Id accountId){
         List<Account> AccountListForPlan = New List<Account>();
         AccountListForPlan= [select Name,Id from 
                                 Account where 
                                 Id in:AccountList];
    Account_Plan__c AccountPlanObj= New Account_Plan__c();
    String TempName = 'AP - ';
    String TempName1 = [select Name from Account where Id = :accountId].Name;
    AccountPlanObj.Name = TempName + TempName1 ;
    AccountPlanObj.CurrencyIsoCode = 'USD';
    AccountPlanObj.Account__c = accountId;
    Insert AccountPlanObj;
    List<Account_Account_Plan_Junction__c>  AcPlanJunLst = New List<Account_Account_Plan_Junction__c>();
    for(Account Accnt: AccountListForPlan){
        Account_Account_Plan_Junction__c Acnt_AcntPlan = new Account_Account_Plan_Junction__c();
        Acnt_AcntPlan.Account__c = Accnt.Id;
         Acnt_AcntPlan.Account_Plan__c = AccountPlanObj.Id;
         AcPlanJunLst.add(Acnt_AcntPlan);
       }
    
    if(AcPlanJunLst.size()>0){   
        try{
              Insert(AcPlanJunLst);
          }Catch(DMLException e){
             system.debug('\n AccountplanHelper Class Failed to Insert Account Plan  '+ e.getMessage());
           }
     } 
     else{
        ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'Account plan Insert Failed.'));
    }
    
   return AccountPlanObj;
  }
  
  //Update Existing Account Plan If User Decides to Add More Accounts Or Remove Existing Accounts. This process Would Start With Hitting The Edit Button On the Plan
  //The Inputs for this function are the new Primary Account, The new set of Accounts to be associated and the Existing Plan Record Id.
  
    Public Static Account_Plan__c UpdateAccountPlan(List<String> AccountList,Id accountId, Id accountPlan){
       Set<Account> planNewAccLst= New Set<Account>([select Name,Id from Account where Id in:AccountList]);
       Set<Id> planNewAccIDLst= New Set<Id>();
       Set<Id> planAccIDtoRemove= New Set<Id>();
       for(Account planAcc : planNewAccLst){
           planNewAccIDLst.add(planAcc.Id);
          }
       system.debug('Tyaga the code has come till here And the New Account Id list for the plan is ' + planNewAccIDLst);   
       Account_Plan__c AccountPlanObj= [select Id, Name from Account_Plan__C where Id = :accountPlan];
       AccountPlanObj.Account__c = accountId;
       Update AccountPlanObj;
       List<Account_Account_Plan_Junction__c>  acPlanJunListOld = New List<Account_Account_Plan_Junction__c>([select Account__C from Account_Account_Plan_Junction__c where Account_plan__C = :accountPlan]);
       Set<Id> planExisAccIDLst = New Set<Id>();
       for(Account_Account_Plan_Junction__c tempJunId :acPlanJunListOld){
           planExisAccIDLst.add(tempJunId.Account__c);  
         } 
       //Compare the two sets to get the final set of Accounts to be added.  
       system.debug('Tyaga this is how the Original New Account List looks like ' + planNewAccIDLst);  
       system.debug('Tyaga this is how the Existing Account List looks like ' + planExisAccIDLst);
         
       for(Id accID: planExisAccIDLst){
           if(!planNewAccIDLst.contains(accID)){
                   planAccIDtoRemove.add(accID);
               }     
          }
       for(Id accID: planExisAccIDLst){
           if(planNewAccIDLst.contains(accID)){
                   planNewAccIDLst.remove(accID);
               }     
          }   
          
       system.debug('Tyaga this is how the updated New Account List Looks Like ' + planNewAccIDLst);
       /*for(Id accID1: planNewAccIDLst){
           if(!planExisAccIDLst.contains(accID1)){
                   planNewAccIDLst.add(accID1);
               }
          }*/
        //Delete the Existing Entries from the AccountplanJunction object List  
        List<Account_Account_Plan_Junction__c>  AcPlanJunLstToRemove = New List<Account_Account_Plan_Junction__c>([select Account__C, Id from Account_Account_Plan_Junction__c where Account_plan__C = :accountPlan 
                                                                                                            and Account__C in :planAccIDtoRemove ]);
          
        Delete AcPlanJunLstToRemove; 
        //Insert AccountPlanObj;
        List<Account_Account_Plan_Junction__c>  AcPlanJunLst = New List<Account_Account_Plan_Junction__c>();
        for(Id AccntId: planNewAccIDLst){
            Account_Account_Plan_Junction__c Acnt_AcntPlan = new Account_Account_Plan_Junction__c();
            Acnt_AcntPlan.Account__c = AccntId;
             Acnt_AcntPlan.Account_Plan__c = AccountPlanObj.Id;
             AcPlanJunLst.add(Acnt_AcntPlan);
           }
        if(AcPlanJunLst.size()>0){   
            try{
                  Insert(AcPlanJunLst);
              }Catch(DMLException e){
                 system.debug('\n AccountplanHelper Class Failed to Insert Account Plan  '+ e.getMessage());
               }
        } 
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'Account plan Insert Failed.'));
        }
        
   return AccountPlanObj;
  }  
  
  
  

  //case #01848189 - Account Planning Enhancements
  public Static void updateAccountPlanTeamMembers(ID accountID, Account_Plan__c accPlan){

    List<Account_Plan_Team__c> accountPlanTeamNewMembers = new List<Account_Plan_Team__c>();
    
    for(AccountTeamMember atMember : [SELECT id, TeamMemberRole, UserId FROM AccountTeamMember WHERE AccountId = :accountID AND User.isActive = True]){
              
         Account_Plan_Team__c newAPT = new Account_Plan_Team__c(
                                             Account_Plan__c      = accPlan.id,
                                             Account_Team_Role__c = atMember.TeamMemberRole,     
                                             User__c              = atMember.UserId
                                             );
         accountPlanTeamNewMembers.add(newAPT);
    }
    
    if(accountPlanTeamNewMembers.size()>0){
      try{
        insert accountPlanTeamNewMembers;
      } Catch(DMLException e){
        system.debug('Problem adding Account Plan Team Members.' );
        system.debug('\n AccountPlanHelperClass - updateAccountPlanTeamMembers: '+ e.getLineNumber() + ' Stack:' + e.getStackTraceString() );
      }
    } else {
       ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'Records not added.'));
    }
  
  }
  //case #01848189 - Account Planning Enhancements

}