@isTest
private class RecognitionOnProfileTabController_Test {

	private static testMethod void test() {
        
        Test.startTest();
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            User u = new User(Alias = 'stdtest', Email='test@test.in.experian.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testing123@test.in.experian.com');
            insert u;
            
            Test.setCurrentPageReference(new PageReference('UserProfilePage?u='));
            System.currentPageReference().getParameters().put('sfdc.userId', u.id);
            
            Document badgeImage = new Document(
                FolderId = UserInfo.getUserId(),
                Name = 'BadgeImage',
                DeveloperName = 'BadgeImage',
                Body = Blob.valueOf('Test Image')
            );
            
            insert badgeImage;
            
            WorkBadgeDefinition wrkDef = new WorkBadgeDefinition();
            wrkDef.Name = 'Test Badge';
            wrkDef.Description = 'Test Badge desc';
            wrkDef.ImageUrl = badgeImage.Id;
            wrkDef.IsActive = true;
            
            insert wrkDef;
            
            WorkThanks wrkThanks = new WorkThanks();
            wrkThanks.GiverId = UserInfo.getUserId();
            wrkThanks.Message = 'Test Thanks';
            wrkThanks.OwnerId = u.Id;
            
            insert wrkThanks;
            
            WorkBadge wrkBadge = new WorkBadge ();
            wrkBadge.DefinitionId = wrkDef.Id;
            wrkBadge.RecipientId = u.Id;
            wrkBadge.SourceId = wrkThanks.Id;
            
            insert wrkBadge;
            
            WorkBadge badges = [SELECT ImageUrl, GiverId, Definition.Name, Giver.Name, RecipientId, CreatedDate, LastModifiedDate, DefinitionId, Description, Message, 
                                SourceId, LastViewedDate, Id, Nomination__c 
                                FROM WorkBadge where RecipientId != null 
                                AND RecipientId =: u.Id
                                ORDER BY CreatedDate DESC];
                                
            RecognitionOnProfileTabController recogContr = new RecognitionOnProfileTabController(new ApexPages.StandardController(badges));
	
        }
    }

}