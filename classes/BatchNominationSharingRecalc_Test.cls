/*=============================================================================
 * Experian
 * Name: BatchNominationSharingRecalc_Test
 * Description: Test class for BatchNominationSharingRecalc
 * Created Date: 7 Nov 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/
@isTest
private class BatchNominationSharingRecalc_Test {
  
  private static String giverEmail = 'kjsdhfkjsfkdsfjiou3904u3@experian.com';
  private static String recipEmail = 'sdkfnsdkjfdf90834908394@experian.com';
    
  @isTest
  static void testBatch() {
    
    Test.startTest();
    
    BatchNominationSharingRecalc b = new BatchNominationSharingRecalc();
    Database.executeBatch(b);
    
    Test.stopTest();
    
    // There should be a share created for both the nominator and nominee
    system.assertEquals(1, [SELECT COUNT() FROM Nomination__Share WHERE UserOrGroup.Email = :recipEmail AND RowCause = :Schema.Nomination__Share.rowCause.Nominee__c]);
    system.assertEquals(1, [SELECT COUNT() FROM Nomination__Share WHERE UserOrGroup.Email = :giverEmail AND RowCause = :Schema.Nomination__Share.rowCause.Nominator__c]);
    
  }
  
  @testSetup
  private static void createData() {
    
    IsDataAdmin__c ida = new IsDataAdmin__c(SetupOwnerId = UserInfo.getUserId(), IsDataAdmin__c = true);
    insert ida;
    
    // Assign the permission set to a test user to create some data
    User daUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    User giverUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    giverUser.Email = giverEmail;
    User recipUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    recipUser.Email = recipEmail;
    User managUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    insert new List<User>{daUser, giverUser, recipUser, managUser};
    
    recipUser.ManagerId = managUser.Id;
    update recipUser;
    
    PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'Recognition_Data_Admin' LIMIT 1];
    system.runAs(new User(Id = UserInfo.getUserId())) {
      PermissionSetAssignment psa = new PermissionSetAssignment(
        PermissionSetId = ps.Id,
        AssigneeId = daUser.Id
      );
      insert psa;
    }
    
    system.runAs(daUser) {
      
      Nomination__c l2spot = new Nomination__c(
        Status__c = NominationHelper.NomConstants.get('Approved'),
        Type__c = NominationHelper.NomConstants.get('Level2SpotAward'),
        Nominee__c = recipUser.Id,
        Requestor__c = giverUser.Id,
        Nominees_Manager__c = managUser.Id,
        OwnerId = managUser.Id,
        Spot_Award_Amount__c = '$50'
      );
      
      insert l2spot;
      
    }
    
    delete ida;
    
  }
/*
  public Database.Querylocator start (Database.Batchablecontext bc) {
    return Database.getQueryLocator([
      SELECT Id, Nominee__c, Nominee_Viewable__c, Master_Nomination__c, Requestor__c, Project_Sponsor__c, Nominees_Manager__c 
      FROM Nomination__c
    ]);
  }
  
  public void execute (Database.BatchableContext bc, List<Nomination__c> scope) {
    // Delete existing shares...
    List<Nomination__Share> oldNomShares = [
      SELECT Id 
      FROM Nomination__Share 
      WHERE ParentId IN :(new Map<Id, Nomination__c>(scope)).keySet()
      AND (RowCause = :Schema.Nomination__Share.rowCause.Nominator__c OR
           RowCause = :Schema.Nomination__Share.rowCause.Team_Member_Manager__c OR
           RowCause = :Schema.Nomination__Share.rowCause.Project_Sponsor__c OR
           RowCause = :Schema.Nomination__Share.rowCause.Nominee__c)
    ];
    
    delete oldNomShares;
    
    NominationHelper.createNominationShares(scope);
    
    List<Nomination__c> nomineeViews = new List<Nomination__c>();
    for (Nomination__c n : scope) {
      if (n.Nominee_Viewable__c) {
        nomineeViews.add(n);
      }
    }
    
    NominationHelper.createNomineeShares(nomineeViews);
  }
  
  public void finish (Database.BatchableContext bc) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchNominationSharingRecalc', true);
  }
  
}

public class CLASS_NAME {
*/
}