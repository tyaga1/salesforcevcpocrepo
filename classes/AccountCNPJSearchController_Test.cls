/**=====================================================================
 * Appirio
 * Name: AccountCNPJSearchController_Test
 * Description: 
 * Created Date: 28 July 2015
 * Created By: Parul Gupta
 *
 * Date Modified                Modified By                  Description of the update
 * Jul 29, 2015                 Arpita Bose                  T-421831: Updated
 * Nov 11th, 2015               Paul Kissick                 Case 01209492: Testing integrations
 =====================================================================*///

@isTest
private class AccountCNPJSearchController_Test {

    static testMethod void testAccountCNPJSearchControllerNoSvc() {
      // Insert account     
      Account account = Test_Utils.insertAccount();
      ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)account);
      AccountCNPJSearchController controller = new AccountCNPJSearchController(stdController);
      
      Webservice_Endpoint__c wsEnd = [SELECT Active__c FROM Webservice_Endpoint__c WHERE Name = 'SerasaMainframe' LIMIT 1];
      wsEnd.Active__c = false;
      update wsEnd;
      
      Test.startTest();
      
      // Search for invalid
      controller.cnpjNum = '05504829000146';
      controller.searchAccounts();
      system.assert(controller.searchResults.size() == 0);
      
      // Search for valid
      controller.cnpjNum = '05504829000145';
      controller.searchAccounts();
      system.assert(controller.searchResults.size() == 0);
      
      account = Test_Utils.createAccount();
      account.CNPJ_Number__c = '05504829000145';
      insert account;
      
      controller.cnpjNum = '05504829000145';
      controller.searchAccounts();
      system.assert(controller.searchResults.size() > 0);
      
      String nextPage = controller.resetResults().getURL(); 
      System.assertEquals('/apex/AccountCNPJSearchPage', nextPage);
      
      system.assertNotEquals(null, controller.processButtonClick());
      
      Test.stopTest();
    }
    
    static testMethod void testAccountCNPJSearchControllerWithSvc() {
      // Insert account     
      
      ApexPages.StandardController stdController = new ApexPages.StandardController(new Account());
      AccountCNPJSearchController controller = new AccountCNPJSearchController(stdController);
      
      Test.setMock(HttpCalloutMock.class, new SerasaMainframeWSRequest.MockCalloutCNPJ());
    
      Test.startTest();
      
      controller.cnpjNum = '00000042400066';
      controller.searchAccounts();
      system.assert(controller.searchResults.size() == 0);
      system.assertEquals(true, controller.serviceResults.success);
      system.assertEquals(1, controller.serviceResults.cnpjResults.size());
      
      system.assertNotEquals(null, controller.createFromServiceClick());
      
      Test.stopTest();
    }
    
    @testSetup
    static void setupData() {
      
      Account account = Test_Utils.insertAccount();
      
      Webservice_Endpoint__c wsEnd = new Webservice_Endpoint__c(Name = 'SerasaMainframe',Username__c = 'ausername', URL__c = 'https://somewhere.com', Timeout__c = 10, Active__c = true);
      insert wsEnd;
    
      WebserviceEndpointUtil.createWebserviceKey(wsEnd);
      WebserviceEndpointUtil.setWebservicePassword(wsEnd,'apassword');
    }
}