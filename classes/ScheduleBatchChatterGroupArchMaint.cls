/*=============================================================================
 * Experian
 * Name: ScheduleBatchChatterGroupArchMaint
 * Description: Schedules the BatchChatterGroupArchiveMaintenance process
 * Created Date: 2016-05-31
 * Created By: Diego Olarte
 *
 * Date Modified      Modified By           Description of the update
 * 

 =============================================================================*/

global class ScheduleBatchChatterGroupArchMaint implements Schedulable {

  global void execute(SchedulableContext sc) {
    
    Database.executeBatch(new BatchChatterGroupArchiveMaintenance(), ScopeSizeUtility.getScopeSizeForClass('BatchChatterGroupArchiveMaintenance')); 

  }
  
}