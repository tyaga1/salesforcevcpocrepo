public with sharing class ResellerComm_ConfidentialInfoEmail {

    //private final Confidential_Information__c confInfo;    
    private final List<Profile> stratProfileList = [Select Id From Profile Where Name = 'Xperian Global Community Strategic'];
    private final List<Profile> resellerProfileList = [Select Id From Profile Where Name = 'Xperian Global Community Resellers'];
    public Integer contactCount {get; set;}
    public String finalConfInfo {get; set;}
    public String docType {get; set;}
    /*
    public ResellerComm_ConfidentialInfoEmail(String confInfoId) {
        contactCount = 0;
        List<Confidential_Information__c> confInfoList = [Select Id, Account__c From Confidential_Information__c Where Id = :confInfoId];
        contactCount = countContactList(confInfoList[0], stratProfileList[0].id);         
    }
    */
    public ResellerComm_ConfidentialInfoEmail(ApexPages.StandardController stdController) {
        contactCount = 0;
        Id confInfoId = stdController.getId();
        finalConfInfo = confInfoId;

        List<Confidential_Information__c> confInfoList = [Select Id, Account__c, Document_Type__c From Confidential_Information__c Where Id = :confInfoId];
        docType =  confInfoList[0].Document_Type__c;
        system.debug('number of qualifying contacts: ' + contactCount);
        contactCount = countContactList(confInfoList[0], stratProfileList[0].id, resellerProfileList[0].id);
    }



    public static Integer countContactList(Confidential_Information__c confInfo, Id stratProfileId, Id resellerProfileId){
        System.debug('createContactList called');
        Integer Count = 0;
        Id profileIdByDocType = (confInfo.Document_Type__c == 'Strat Clients Document')?stratProfileId:resellerProfileId;
        List<Contact> relatedContacts = [Select Id, Is_Community_User__c From Contact 
                                         Where AccountId =: confInfo.Account__c 
                                         AND Is_Community_User__c = true];
        
        List<Id> conIds = new List<Id>();

        For(Contact con : relatedContacts){
            conIds.add(con.Id);
        }

        List<User> contactUserList = [Select Id, ProfileId, ContactId From User Where ContactId IN :conIds];
        Map<Id,User> contactUserMap = new Map<Id,User>();
        For(user u :  contactUserList){
            contactUserMap.put(u.ContactId,u);
        }

        List<Id> bccAddressList = new List<Id>();
        For(Contact con : relatedContacts){
            if(contactUserMap.get(con.Id).ProfileId == profileIdByDocType){
                Count = Count+1;
            }
        }

        return Count;
    }

    

    public void sendEmail(){
        System.debug('sendEmail called');
        List<Confidential_Information__c> confInfoList = [Select Id, Account__c, Document_Type__c From Confidential_Information__c Where Id = :finalConfInfo];
        List<Profile> stratProfileList = [Select Id From Profile Where Name = 'Xperian Global Community Strategic'];
        List<Profile> resellerProfileList = [Select Id From Profile Where Name = 'Xperian Global Community Resellers'];
        Id profileIdByDocType = (confInfoList[0].Document_Type__c == 'Strat Clients Document')?stratProfileList[0].Id:resellerProfileList[0].Id;
        String defaultRecipientId = Label.Community_Default_Email_Recipient;
        List<EmailTemplate> templateIdList = [SELECT Id FROM EmailTemplate
                                                        WHERE DeveloperName = 'Confidential_Info_Notification'
                                                        ];

        List<Contact> relatedContacts = [Select Id, Is_Community_User__c From Contact 
                                         Where AccountId =: confInfoList[0].Account__c 
                                         AND Is_Community_User__c = true];

        List<Id> conIds = new List<Id>();

        For(Contact con : relatedContacts){
            conIds.add(con.Id);
        }

        List<User> contactUserList = [Select Id, ProfileId, ContactId From User Where ContactId IN :conIds];
        Map<Id,User> contactUserMap = new Map<Id,User>();
        For(user u :  contactUserList){
            contactUserMap.put(u.ContactId,u);
        }

        List<Id> bccAddressList = new List<Id>();
        For(Contact con : relatedContacts){
            if(contactUserMap.get(con.Id).ProfileId == profileIdByDocType){
                bccAddressList.add(contactUserMap.get(con.Id).Id);
            }
        }

        if(!bccAddressList.isEmpty()){
            List<Messaging.SingleEmailMessage> mailingList = new List<Messaging.SingleEmailMessage>();
            List<Messaging.SendEmailResult> mailingResults = new List<Messaging.SendEmailResult>();

            mailingList.add(createEmail(defaultRecipientId, bccAddressList, templateIdList[0].Id));

            if(!mailingList.isEmpty()){
                mailingResults = Messaging.sendEmail(mailingList, false);
                system.debug('the sent email results: ' + mailingResults);
            } 

        }

    }

    public static Messaging.SingleEmailMessage createEmail(String defaultRecipientId, List<Id> bccAddressList, Id emailTemplateId){
        System.debug('createEmail called');
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTemplateId(emailTemplateId);
        email.setTargetObjectId(defaultRecipientId);
        email.setBccAddresses(bccAddressList);
        email.setTreatTargetObjectAsRecipient(false);

        return email;
    }

}