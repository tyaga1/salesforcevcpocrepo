/**=====================================================================
 * Experian
 * Name: TaskFollowOnExtension
 * Description: CRM2:W-005428
 * The autorun method of this class is called from the Visualforce page TaskFollowOnPage, which in turn is called from a Custom button labelled 'Create Follow-On Task' on the Task Standard page.
 * The purpose of the class is to present a standard Task page to the user with the Campaign field populated.  This was not previously being done and so the Campaign was not typically being
 * recorded for Tasks generated from the standard 'Create Follow-On Task' on the Task standard page.
 *
 * Created Date: Aug. 9th, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By           Description of the update
 * Aug. 9th 2016         James Wills           CRM2:W-005428: There are issues around being able to track a lead
 * Aug  23rd 2016        Tyaga Pati            CRM2:W-005599: Updating the Parenttaskid and making ISlead false.
 * Sept. 7th 2016        James Wills           CRM2:W-005599: Tidying up and resolving test class failure.
 * Dec  14th 2016        Manoj Gopu            Case:02108066: Added code to update the Task Status to completed when creating Follow Up Task
  =====================================================================*/
public class TaskFollowOnExtension{   
    @testVisible private String parentID ;
       
    public TaskFollowOnExtension(ApexPages.StandardController stdController) {        
      parentID =  stdController.getId();
    } 
    
    public PageReference autoRun(){  
             
       String decision = ApexPages.currentPage().getParameters().get('Decision');
      //Clone the Parent Task to get the Campaign__c field value
       if(decision == 'yes'){
           List<Task> lstTask = [SELECT Id, Status FROM Task WHERE Id = :parentID ];
        
          if (lstTask != null && lstTask.size() > 0) {
            for (Task t : lstTask) {
              t.Status = Constants.TASK_STATUS_COMPLETED;
            }
            update lstTask;
          }
      }
      PageReference pageRef = new PageReference('/' + parentID + '/e?clone=1&retURL=%2F' + parentID);

      //Initialise field values before displaying to user
      pageRef.getParameters().put('tsk3', '');                //Related To
      pageRef.getParameters().put('tsk4', '');                //Due Date
      pageRef.getParameters().put('tsk6', '');                //Comments
      pageRef.getParameters().put('tsk10', '--None--');       //Type
      pageRef.getParameters().put('tsk12', Constants.TASK_STATUS_NOT_STARTED);//Status
      pageRef.getParameters().put('tsk13', 'Normal');         //Priority
      pageRef.getParameters().put(Custom_Fields_Ids__c.getInstance().ParentTaskIdField__c, parentID );
      pageRef.getParameters().put(Custom_Fields_Ids__c.getInstance().Task_Seresa_Expense__c, 'False');//Seresa Expense
      pageRef.getParameters().put(Custom_Fields_Ids__c.getInstance().Task_Outcomes__c,'');            //Outcomes
      pageRef.getParameters().put(Custom_Fields_Ids__c.getInstance().Task_Phone_Ext__c, '');          //Phone Ext
      pageRef.getParameters().put(Custom_Fields_Ids__c.getInstance().IsLeadTaskField__c, 'False');    //IsLead          
            
      return pageRef;    
    }

}