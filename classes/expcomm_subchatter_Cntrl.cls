/*
 *    Used in: expcomm_subchatter_cmpt Visualforce Component
 *    Getting group ID using attribute value from component 
 *
 * @author    Hay Mun Win
 * @version   1.0
 * @since     1.0
 */

public with sharing class expcomm_subchatter_Cntrl{ 
    public PageReference findGroup() {
        return null;
    }


   
    public String groupID {get;set;}       //use for component
    
    public expcomm_subchatter_Cntrl() {
    }
    
     /**
     * If groupID is null, use current UserID
     * else, use that groupID
     *
     * @param      None
     * @return     None
     * @since      1.0
     */ 
    
    public boolean getfindGroup(){
    
        if (groupID != null && groupID != '') {
            groupID = groupID;
        } else {
            groupID = UserInfo.getUserId();
        }
        
        return true;
    }
    
    public void refresh() {
        System.debug('refresh');
    }
}