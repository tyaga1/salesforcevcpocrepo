@isTest
private class ResellerCloneCaseBtnControllerTest {
	
	@isTest static void test_method_one() {
		Account testAccount1 = Test_Utils.createAccount();
	 	insert testAccount1;

	 	String serviceCentralId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Central').getRecordTypeId();

	 	Case c1 = new Case(
					RecordTypeId = serviceCentralId,
					AccountId = testAccount1.Id,
					Subject = 'Test Case 1',
					Description = 'Test Description 1',
					status = 'Closed-Complete');

		insert c1;


		ResellerCloneCaseBtnController.findRecordType(c1.id);
	}
	
}