/**=====================================================================
 * Name: WorkBadgeTriggerHandler
 * Description: Handler class for WorkBadgeTrigger
 * Date: March 29th 2016
 * Before Insert : To Populate recepients manager in Badge recevied 
 **/      

public class WorkBadgeTriggerHandler {
  //Process beforeInsert Operations.
  public static void beforeInsert(list<WorkBadge> newLst) {
      
      populateRecipientMngrDetail(newLst);
      
  }
  
  private static void populateRecipientMngrDetail(list<WorkBadge> newWorkBadgeLst)
  {
      Set<id> UserManagerSet = new set<id>();
      For(WorkBadge BadgeReceivedRec:newWorkBadgeLst){
          
          UserManagerSet.add(BadgeReceivedRec.RecipientId);
          
          
      }
      
      Map<id,User> userMap = new Map<id,User>([Select id, ManagerId from  user where Id In :UserManagerSet]);
      
       For(WorkBadge BadgeReceivedRec:newWorkBadgeLst){
          
          if (userMap.containsKey(BadgeReceivedRec.RecipientId))
          BadgeReceivedRec.Recipient_s_Manager__c= userMap.get(BadgeReceivedRec.RecipientId).ManagerId;
          
          
      }
        
        
        
      
  }
  
}