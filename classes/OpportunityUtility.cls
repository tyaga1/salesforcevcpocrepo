/**=====================================================================
 * Appirio, Inc
 * Name: OpportunityUtility
 * Description: T-258164: Check opportunity dates with revenue schedules
 * Created Date: 10th April, 2014
 * Created By: Naresh Kr Ojha (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * Apr 29, 2014                 Nathalie Le Guay             Add OPPTY_ERR_CONTRACT_START_PRIOR_TO_CLOSEDATE error
 * May 12, 2016                 Sadar Yacob                  Case #01822826 : Give PO Required Error message if PO Required = YES on the Opty
 * May 13, 2016                 Paul Kissick                 Case #01220695 : Add alerts for EDQ based on Product pricing issues
 * Jun 1, 2016                  Paul Kissick                 Case #01984048 : Created method to check the running user has access to create
 * Jun 14, 2016                 Diego Olarte                 Case #01975917 : Expand PO requirement for All regions
 * Jul 20, 2016(QA)             Tyaga Pati                   CRM2:W-005403: Method to check if any contacts are Inactive or No Longer with Company.
 * Jul 28, 2016(QA)             Cristian Torres              CRM2:W-005403: Display message for No longer with company and Inactive contacts
 =====================================================================*/
public with sharing class OpportunityUtility {
  
  public static Boolean canCreateOpportunity(Id contactId) {
    Boolean hasAccess = false;
    Id accountId;
    if (contactId != null) {
      for (Contact c : [SELECT AccountId FROM Contact WHERE Id = :contactId]) {
        accountId = c.AccountId;
      }
      Integer testCount = [
        SELECT COUNT()
        FROM SetupEntityAccess
        WHERE SetupEntityType = 'CustomPermission'
        AND SetupEntityId IN (
          SELECT Id
          FROM CustomPermission
          WHERE DeveloperName = 'Administration_Profile'
        )
        AND ParentId IN (
          SELECT PermissionSetId
          FROM PermissionSetAssignment
          WHERE AssigneeId = :UserInfo.getUserId()
        )
      ];
      //Check if user has Admin profile
      hasAccess = (testCount == 0) ? false : true;
      if (hasAccess == false) {
        for (UserRecordAccess userAccess : [SELECT RecordId, HasEditAccess
                                            FROM UserRecordAccess
                                            WHERE UserId = :UserInfo.getUserId()
                                            AND RecordId = :accountId]) {
          if (userAccess.HasEditAccess == true) {
            hasAccess = true;
          }
        }
      }
    }
    return hasAccess;
  }
    
  //Checks opportunity dates 
  public static List<String> checkOpportunityDates(Id opportunityId) {
    Set<ID> opptyLineIDs = new Set<ID>();
    List<String> errorList = new List<String>();
    Opportunity currentOpportunity;
    Boolean hasOLIearlyDate = false;
    Boolean hasScheduleEarlierThanContractDate = false;
    Boolean hasScheduleEarlierThanCloseDate = false;
    Set<String> edqProductCheckRegions = new Set<String>{
      Constants.REGION_UKI,
      Constants.REGION_NA
    };
    if (Test.isRunningTest()) {
      edqProductCheckRegions.add(Constants.REGION_EMEA);
      edqProductCheckRegions.add(Constants.REGION_APAC);
      edqProductCheckRegions.add(Constants.REGION_GLOBAL);
    }
      
      
    // Case 01220695 - Adding more fields to the query below
    for (Opportunity oppty : [SELECT Id, OwnerId, CloseDate, Contract_Start_Date__c, PO_Required__c, Owner_s_Region__c, 
                                     Account.Opportunity_PO_Required_By_Default__c,PO_Number__c, Type, RecordType.Name,
                                     (SELECT Id, Start_Date__c, PricebookEntry.Product2.Name,
                                             EDQ_Margin_Valid__c, Opportunity_Product_is_Renewal__c,
                                             Partner_Amount_Valid__c, Renewal_EDQ_Margin_Valid__c, Pricing_Fields_Valid__c,
                                             Pricing_Fields_Valid_Detail__c, Renewal_Partner_Amount_Valid__c,
                                             Renewal_Sales_Price_Valid__c, Sales_Price_Valid__c
                                      FROM OpportunityLineItems) 
                              FROM Opportunity 
                              WHERE Id = :opportunityId]) {
      // Case 01220695 - Adding checks for EDQ only, on 'standard' opps (not free trial)
      if (Constants.EDQ.equalsIgnoreCase(BusinessUnitUtility.getBusinessUnit(oppty.OwnerId)) && 
          oppty.RecordType.Name.equals(Constants.RECORDTYPE_STANDARDS) && 
          edqProductCheckRegions.contains(oppty.Owner_s_Region__c)) {
        Boolean oppProductRenewal = false;
        if (oppty.OpportunityLineItems != null && !oppty.OpportunityLineItems.isEmpty()) {
          List<String> issueList = new List<String>();
          for (OpportunityLineItem oli : oppty.OpportunityLineItems) {
            issueList = new List<String>();
            if (oli.Opportunity_Product_is_Renewal__c) {
              oppProductRenewal = true;
            }
            if (oli.Sales_Price_Valid__c == false) {
              issueList.add('Sales Price not valid');
            }
            if (oli.Renewal_Sales_Price_Valid__c == false) {
              issueList.add('Renewal Sales Price not valid');
            }
            if (oli.Partner_Amount_Valid__c == false) {
              issueList.add('Partner Amount not valid');
            }
            if (oli.Renewal_Partner_Amount_Valid__c == false) {
              issueList.add('Renewal Partner Amount not valid');
            }
            if (oli.EDQ_Margin_Valid__c == false) {
              issueList.add('EDQ Margin not valid');
            }
            if (oli.Renewal_EDQ_Margin_Valid__c == false) {
              issueList.add('Renewal EDQ Margin not valid');
            }
            if (oli.Pricing_Fields_Valid__c == false) {
              issueList.add(oli.Pricing_Fields_Valid_Detail__c.stripHtmlTags());
            }
            if (!issueList.isEmpty()) {
              errorList.add('Problems found with '+oli.PricebookEntry.Product2.Name +' ('+oli.Id+') : '+String.join(issueList,', '));
            }
          }
          if (oppProductRenewal && !Constants.OPPTY_TYPE_RENEWAL.equalsIgnoreCase(oppty.Type)) {
            errorList.add('Opportunity must be a '+Constants.OPPTY_TYPE_RENEWAL);
          }
        }
      }
      if (oppty.Contract_Start_Date__c < oppty.CloseDate) {
        system.debug('--debug0.1--'+Label.OPPTY_ERR_CONTRACT_START_PRIOR_TO_CLOSEDATE);
        errorList.add(Label.OPPTY_ERR_CONTRACT_START_PRIOR_TO_CLOSEDATE);
      }
      
      //oppty.Account.Opportunity_PO_Required_By_Default__c == true && (oppty.PO_Required__c == 'No' || oppty.PO_Required__c == null || || oppty.PO_Required__c == 'Yes') --> Dont need to check if Account's PO Requird is Set to TRUE 
      if (oppty.PO_Required__c == 'Yes' && String.isBlank(oppty.PO_Number__c)) {
        errorList.add(Label.PO_REQUIRED_ON_OPTY_FOR_APAC);
      }
                                  
      for (OpportunityLineItem oli: oppty.OpportunityLineItems) {
        opptyLineIDs.add(oli.ID);
        ////A start date on an opp line item is before the expected close date
        if (oli.Start_Date__c < oppty.CloseDate) {
          if (hasOLIearlyDate) {
            break;
          }
          system.debug(oli.ID+'--debug1.0--'+Label.OPPTY_ERR_OLIE_DATE_PRIOR_OPPTY_CLOSE_DATE);
          errorList.add(Label.OPPTY_ERR_OLIE_DATE_PRIOR_OPPTY_CLOSE_DATE);
          hasOLIearlyDate = true;
        }
      }
      currentOpportunity = oppty;
    }
    
    if (!opptyLineIDs.isEmpty()) {
      for (OpportunityLineItemSchedule ols : [SELECT OpportunityLineItemId, Id, ScheduleDate
                                              FROM OpportunityLineItemSchedule
                                              WHERE OpportunityLineItemId IN: opptyLineIDs]) {
                                            
        //A OpportunityLineItemSchedule Schedule date is before the expected close date
        if (ols.ScheduleDate < currentOpportunity.CloseDate) {
          if (!hasScheduleEarlierThanCloseDate) {
            system.debug('--debug1.1--'+Label.OPPTY_ERR_REVSCH_DATE_PRIOR_OPPTY_CLOSE_DATE);
            errorList.add(Label.OPPTY_ERR_REVSCH_DATE_PRIOR_OPPTY_CLOSE_DATE);
            hasScheduleEarlierThanCloseDate = true;
          }
        }
        //A revenue schedule date is before the contract start date        
        if (ols.ScheduleDate < currentOpportunity.Contract_Start_Date__c) {
          if (!hasScheduleEarlierThanContractDate) {
            System.debug('--debug1.2--'+Label.OPPTY_ERR_REVSCH_DATE_PRIOR_OPPTY_CONTRACT_STRT_DATE);
            errorList.add(Label.OPPTY_ERR_REVSCH_DATE_PRIOR_OPPTY_CONTRACT_STRT_DATE);
            hasScheduleEarlierThanContractDate = true;
          }
        }
      }
    }
    
     
    //Removed part of code that was causing an problem when saving the opportunity
    return errorList;
  }
  
  //===========================================================================
  // Checks Contact Roles
  // PK: Reviewed and found problems. Fixed myself.
  //=========================================================================== 
  public static List<String> checkOpportunityContactRoles(Id opportunityId) {      
    
    //Check the contact roles if the status is either No longer with Company or Inactive then 
    //display the warning in the VF
    
    List<String> errorList = new List<String>();
    //Query for Opportunity Contact Roles  
    List<OpportunityContactRole> contactRoles = [
      SELECT ContactId, Contact.Status__c, Contact.Name
      FROM OpportunityContactRole
      WHERE OpportunityID = :opportunityId
      AND Contact.Status__c != null
    ];

    if (!contactRoles.isEmpty()) {
      Set<Id> contactInactiveIdSet = new Set<Id>();
      Set<Id> contactMovedOnIdSet = new Set<Id>();
      
      List<String> inactiveContacts = new List<String>();
      List<String> noLongerCompanyContacts = new List<String>();
      
      // Check status of each contact 
      // for (Contact c: contactRolesList) {
      for (OpportunityContactRole ocr : contactRoles) {
        if (ocr.Contact.Status__c.equalsIgnoreCase(Constants.CONTACT_STATUS_LEFT)) {
          if (!contactMovedOnIdSet.contains(ocr.ContactId)) {
            contactMovedOnIdSet.add(ocr.ContactId);
            noLongerCompanyContacts.add(ocr.Contact.Name);
          }
        }
        if (ocr.Contact.Status__c.startsWithIgnoreCase(Constants.STATUS_INACTIVE)) {
          if (!contactInactiveIdSet.contains(ocr.ContactId)) {
            contactInactiveIdSet.add(ocr.ContactId);
            inactiveContacts.add(ocr.Contact.Name);
          }
        }
      }
      if (!inactiveContacts.isEmpty()) {
        errorList.add(Label.OPPTY_ERR_INACTIVE_CONTACTS  + String.join(inactiveContacts,', ') + '. ');
      }
      if (!noLongerCompanyContacts.isEmpty()) {
        errorList.add(Label.OPPTY_ERR_NO_LONGER_COMPANY_CONTACTS  + String.join(noLongerCompanyContacts,', ') + '. ');
      }
    }
    system.debug('Class Opportunity Utility, Method: checkOpportunityContactRoles. Error List: ' + errorList );
    return errorList;
  }  

  public static list<Opportunity> getRelatedOpportunities(Account_Segment__c accSeg) {
    if (accSeg.Segment_Type__c == 'Region') {
      return accSeg.Opportunities_Regions__r;
    } 
    else if (accSeg.Segment_Type__c == 'Country') {
      return accSeg.Opportunities_Countries__r;
    } 
    else if (accSeg.Segment_Type__c == 'Business Unit') {
      return accSeg.Opportunities_Business_Units__r;
    } 
    else if (accSeg.Segment_Type__c == 'Business Line') {
      return accSeg.Opportunities_Business_Lines__r;
    } 
    else if (accSeg.Segment_Type__c == 'Global Business Line') {
      return accSeg.Opportunities_Global_Lines_of_Business__r;
    } 
    else {
      return new list<Opportunity>();
    }
  }
  
  public static Set<Id> getAllRelatedOrderIds(list<Account_Segment__c> accSegList) {
    Set<Id> orderIds = new Set<Id>();
    for (Account_Segment__c accSeg : accSegList) {
      for (Order__c ordr : accSeg.Orders_Business_Lines__r) {
        orderIds.add(ordr.Id);
      }
      for (Order__c ordr : accSeg.Orders_Business_Units__r) {
        orderIds.add(ordr.Id);
      }
      for (Order__c ordr : accSeg.Orders_Countries__r) {
        orderIds.add(ordr.Id);
      }
      for (Order__c ordr : accSeg.Orders_Global_Business_Lines__r) {
        orderIds.add(ordr.Id);
      }
      for (Order__c ordr : accSeg.Orders_Regions__r) {
        orderIds.add(ordr.Id);
      }
    }
    return orderIds;
  }
  
  public static list<Order__c> getRelatedOrders(Account_Segment__c accSeg) {
    if (accSeg.Segment_Type__c == 'Region') {
      return accSeg.Orders_Regions__r;
    } 
    else if (accSeg.Segment_Type__c == 'Country') {
      return accSeg.Orders_Countries__r;
    }
    else if (accSeg.Segment_Type__c == 'Business Unit') {
      return accSeg.Orders_Business_Units__r;
    }
    else if (accSeg.Segment_Type__c == 'Business Line') {
      return accSeg.Orders_Business_Lines__r;
    }
    else if (accSeg.Segment_Type__c == 'Global Business Line') {
      return accSeg.Orders_Global_Business_Lines__r;
    } 
    else {
      return new list<Order__c>();
    }
  }
  
}