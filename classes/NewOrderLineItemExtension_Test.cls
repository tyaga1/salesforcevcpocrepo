/**=====================================================================
 * Experian
 * Name: NewOrderLineItemExtension_Test
 * Description: Case 01185249
 * 
 * Created Date: Feb 12th, 2016
 * Created By: Paul Kissick
 * 
 * Date Modified      Modified By                  Description of the update
*  =====================================================================*/
@isTest
private class NewOrderLineItemExtension_Test {
  
  private static String testUserEmail = 'ksdjfhnskjhnfd9o9898@experian.com';
  
  
  private static testMethod void testExtension() {
    
    Product2 testProd = [
      SELECT Id, Family, Global_Business_Line__c, Business_Line__c 
      FROM Product2 
      WHERE IsActive = true 
      AND Name LIKE 'TestProduct%' LIMIT 1
    ];
    
    User testUser = [SELECT Id FROM User WHERE Email = :testUserEmail LIMIT 1];
    
    system.runAs(testUser) {
      
      Order__c tstOrder = [SELECT Id FROM Order__c WHERE Name = 'Test Order 1234'];
    
      Order_Line_Item__c oli = new Order_Line_Item__c(Order__c = tstOrder.Id);
    
      ApexPages.StandardController con = new ApexPages.StandardController(oli);
    
      NewOrderLineItemExtension oliExt = new NewOrderLineItemExtension(con);
      
      system.assertEquals(1,oliExt.getResultSize(),'Products not found.');
      system.assertNotEquals(0,oliExt.getPageSize(),'Page size must be greater than 0.');
      system.assertEquals(1,oliExt.getPageNumber(),'Page number should be 1.');
      system.assertEquals(1,oliExt.getTotalPages(),'There should only be 1 page.');
      system.assertEquals(1,oliExt.getResultSize(),'Products not found.');
      
      oliExt.goToFirstPage();
      oliExt.goToLastPage();
      oliExt.goToNextPage();
      oliExt.goToNext2Page();
      oliExt.goToPrevPage();
      oliExt.goToPrev2Page();
      
      system.assertEquals(1,oliExt.getRecords().size());
      
      oliExt.holdingProduct.productFamily = testProd.Family;
      oliExt.holdingProduct.globalBusinessLine = testProd.Global_Business_Line__c;
      oliExt.holdingProduct.businessLine = testProd.Business_Line__c;
      
      oliExt.toggleFollowedProducts();
      oliExt.sortRecordset();
      
      system.assert(oliExt.redirect() == null,'This shouldnt redirect.');
       
    }
  }
  
  @testSetup
  private static void testDataSetup() {
    
    TriggerSettings__c pmTrig = new TriggerSettings__c(Name = 'ProductMasterTrigger', IsActive__c = false);
    insert pmTrig;
    
    
    User testUser = Test_Utils.createUser(Constants.PROFILE_EXP_SALES_EXEC);
    testUser.Email = testUserEmail;
    insert testUser;
    
    CPQ_Settings__c testCPQSetting = new CPQ_Settings__c (
      Name = 'CPQ',
      Company_Code__c = 'Experian',
      CPQ_API_Access_Word__c = 'Accessword',
      CPQ_API_Endpoint__c= 'https://test.webcomcpq.com/',
      CPQ_API_UserName__c= 'TestUser#Experian'
    );
    insert testCPQSetting; 
    
    
    Country__c testCountry = Test_Utils.createCountry(true);
    Region__c testRegion = Test_Utils.createRegion(true);
    
    Schema.PicklistEntry prodFamily = Product2.sObjectType.getDescribe().fields.getMap().get('Family').getDescribe().getPicklistValues()[0];
    Schema.PicklistEntry gblPicklist = Product2.sObjectType.getDescribe().fields.getMap().get('Global_Business_Line__c').getDescribe().getPicklistValues()[0];
    Schema.PicklistEntry blPicklist = Product2.sObjectType.getDescribe().fields.getMap().get('Business_Line__c').getDescribe().getPicklistValues()[0];
        
    Product_Master__c pm = Test_Utils.createProductMaster(true);
    
    Product2 prod = Test_Utils.createProduct();
    prod.Family = prodFamily.getValue();
    prod.Global_Business_Line__c = gblPicklist.getValue();
    prod.Business_Line__c = blPicklist.getValue();
    prod.Product_Master__c = pm.Id;
    insert prod;
    PricebookEntry pbe = Test_Utils.insertPricebookEntry(prod.Id, Test.getStandardPricebookId(), 'USD');
    
    Product_Country__c pc = Test_Utils.createProductCountry(false, testCountry.Id);
    pc.Product__c = prod.Id;
    insert pc;
    
    Product_Region__c rg = Test_Utils.createProductRegion(false, testRegion.Id);
    rg.Product__c = prod.Id;
    insert rg;
    
    Account tstAcc = Test_Utils.insertAccount();
    Order__c tstOrder = Test_Utils.insertOrder(false, tstAcc.Id, null,null);
    tstOrder.Name = 'Test Order 1234';
    tstOrder.Transactional_Sale__c = true;
    tstOrder.Contract_Start_Date__c = Date.today();
    tstOrder.Contract_End_Date__c = Date.today().addDays(30);
    insert tstOrder;
    
    system.runAs(new User(Id = UserInfo.getUserId())) {
      testUser.Region__c = testRegion.Name;
      testUser.Country__c = testCountry.Name;
      update testUser;
    }
    
  }
  
}