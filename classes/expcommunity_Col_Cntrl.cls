/******************************************************************************
 * Name: expcommunity_subcomm.cls
 * Created Date: 2/14/2017
 * Created By: Richard
 * Description : Controller for expcommunity_Col_cmp component for new Exp Community
 * Change Log- 
 ****************************************************************************/

public with sharing class expcommunity_Col_Cntrl{ 



    public List<Employee_Community_News__c> comptData {get;set;}
    public string applicationName{get;set;}
    public string contentType{get;set;}
    public Boolean getcomptDatasizehasValue() {
        return (comptData.size()>0 ? true : false ); 
    }
    
    public expcommunity_Col_Cntrl() {
        
        comptData = new List<Employee_Community_News__c>();
        string QueryString = 'Select Id, Detail__c,Name from  Employee_Community_News__c where isActive__c = true ';
        
        if (applicationName != null && applicationName.trim() != '')
                QueryString= QueryString + 'AND  aplication_name__c = ' + applicationName;
            
            else
                QueryString= QueryString + 'AND application_name__c  = \'EITSHome\' '     ;
                
                if (contentType != null && contentType.trim() != '')
                QueryString= QueryString + 'AND Content_type__c ='+contentType ;
            
            
             comptData = Database.query(QueryString);
        
    }
    
    /* public boolean getfindSubComm(){
         String featureString = '';
         if (featured == true) {
             featureString =  ' AND Feature__c =: featured';
         }
         System.debug('featured ' + featured );
         System.debug('featureString ' + featureString );         
         
         String commQuery = 'Select Id, link__c, Name, description__c, Image_Path__c from ExpCommunity_Apps__c where Sub_Communities__c = true' + featureString + ' Order by Order__c asc';

*/
}