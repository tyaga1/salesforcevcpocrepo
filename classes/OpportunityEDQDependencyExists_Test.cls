/**********************************************************************************************
 * Experian 
 * Name         : OpportunityEDQDependencyExists_Test
 * Created By   : Diego Olarte (Experian)
 * Purpose      : Test class of scheduler class "ScheduleEDQDependencyValidation02" & OpportunityEDQDependencyExists
 * Created Date : September 8th, 2015
 *
 * Date Modified                Modified By                 Description of the update
 * Nov 11th, 2015               Paul Kissick                Case 01266075: Removed bad global setting entry, and testSchedulable
***********************************************************************************************/

@isTest
private class OpportunityEDQDependencyExists_Test {
    
  private static testMethod void testOpportunityEDQDependency() {
    Test.startTest();
    OpportunityEDQDependencyExists batchToProcess = new OpportunityEDQDependencyExists();
    database.executebatch(batchToProcess,10);
    Test.stopTest();
    
    system.assertEquals(1,[SELECT COUNT() FROM Opportunity WHERE Owner_s_Business_Unit__c LIKE '%Data Quality%' AND IsWon = true]);
  }
  
  @testSetup
  private static void setupData() {
       
    Account tstAcc = Test_utils.insertEDQAccount(true);
    tstAcc.EDQ_Dependency_Exists__c = false;
                
    update new List<Account>{tstAcc};
    
    User tstUser = Test_utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser.Business_Unit__c = 'NA MS Data Quality';
        
    insert new List<User>{tstUser};
    IsDataAdmin__c IsdataAdmin = new IsDataAdmin__c(SetupOwnerId = UserInfo.getUserId(), IsDataAdmin__c = true);
    
    insert IsdataAdmin;
    Opportunity tstOpp = Test_utils.createOpportunity(tstAcc.ID);
    tstOpp.OwnerId = tstUser.Id;
    tstOpp.StageName = Constants.OPPTY_STAGE_7;
        
    insert new List<Opportunity>{tstOpp};
        
    delete IsdataAdmin;
    
  }
  
}