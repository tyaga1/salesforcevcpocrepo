/**=====================================================================
 * Experian
 * Name: contactHeaderConsoleExtension
 * Description: Controller for ContactHeaderConsole page, which is displayed as a header on Sales Console contact pages. 
 * 
 * Created Date: Sep 14th, 2017
 * Created By: Malcolm Russell
 * 
 * Date Modified        Modified By                  Description of the update
 *
 =====================================================================*/
public with sharing class contactHeaderConsoleExtension {

public Id accountId {get; set;}
Public string totalSpendEDQ {get; set;}

public contactHeaderConsoleExtension(ApexPages.StandardController controller) {

  contact c =(contact)controller.getRecord();
   accountId = c.AccountId;
   
    AggregateResult[] groupedResults = [SELECT format(sum(Renewal_Sale_Price__c)) TotalPrice, sum(Renewal_Sale_Price__c) sumPrice FROM Asset where Order_Owner_BU_Stamp__c=:Constants.UK_I_MS_DATA_QUALITY and Status__c not in('Cancelled', 'Credited', 'Expired', 'Replaced', 'Admin Change') and accountid=:accountId] ; 
  if(groupedResults.size() >0){
  
  if ((decimal)groupedResults[0].get('sumPrice') !=null){
  
  if(userinfo.getDefaultCurrency() !='USD'){
  
   Decimal conversionRate = [SELECT conversionrate FROM currencytype WHERE isocode = :userinfo.getDefaultCurrency() LIMIT 1].conversionRate;
  
   decimal totalpriceconv= ((decimal)groupedResults[0].get('sumPrice') * conversionRate).setscale(2);
   
   totalSpendEDQ = (string)groupedResults[0].get('TotalPrice') + ' (' + userinfo.getDefaultCurrency() +' '+ totalpriceconv.format() +')' ; 
  
  }
  else {  totalSpendEDQ =  (string)groupedResults[0].get('TotalPrice');}
  }
  else{totalSpendEDQ= null;}
  }
  else{totalSpendEDQ= null;}
}

Public Account_Segment__c accountSegment{get{
  Account_Segment__c[] accountSegList=[select Total_Won__c,Business_Unit_Lead__c from Account_Segment__c where value__c=:Constants.UK_I_MS_DATA_QUALITY and Account__c=:accountid];
  if(accountSegList.size() >0){
  return accountSegList[0];
  }
  else{return null;}
}set;}

Public AccountTeamMember accountTeamMember{get{
  AccountTeamMember[] accountTeamList=[select user.Name, UserID from AccountTeamMember where TeamMemberRole='Renewal Owner' and user.business_unit__c=:Constants.UK_I_MS_DATA_QUALITY and AccountId=:accountid];
  if(accountTeamList.size() >0){
  return accountTeamList[0];
  }
  else{return null;}
}set;}
/*
Public decimal totalSpendEDQ{get{
   AggregateResult[] groupedResults = [SELECT sum(Price) TotalPrice FROM Asset where Order_Owner_BU_Stamp__c=:Constants.UK_I_MS_DATA_QUALITY and Status__c not in('Cancelled', 'Credited', 'Expired', 'Replaced', 'Admin Change') and accountid=:accountId] ; 
  if(groupedResults.size() >0){
  return (decimal)groupedResults[0].get('TotalPrice');
  }
  else{return null;}
}set;}
*/
}