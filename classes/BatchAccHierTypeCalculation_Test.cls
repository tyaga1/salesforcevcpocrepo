/*=============================================================================
 * Experian
 * Name: BatchAccHierTypeCalculation_Test
 * Description:                        
 * Created Date: 28 Aug 2016
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
 * Aug 29th 2016      Manoj Gopu            Create a view based on the child accounts to show the overall relationship for a company.
 * Sep 02nd 2016      Manoj Gopu            Added asserts to the test method
 ============================================================================*/

@isTest
private class BatchAccHierTypeCalculation_Test{
    static testMethod void myUnitTest() {
    Test.startTest();
        Global_Settings__c objGlob=new Global_Settings__c();
        objGlob.Name='Global';
        objGlob.Opp_Stage_4_Name__c='Propose';
        objGlob.Opp_Stage_3_Name__c='Qualify';
        objGlob.Account_Team_Member_Default_Role__c='Decider';
        insert objGlob;
            
        Account acc=new Account();
        acc.Name='Test Account';
        acc.Account_Type__c='Client';
        insert acc;
        
        Account acc1=new Account();
        acc1.Name='Test Account1';
        acc1.Account_Type__c='Client';
        acc1.ParentId=acc.Id;
        insert acc1;
        
        
        Account acc2=new Account();
        acc2.Name='Test Account2';
        acc2.Account_Type__c='Prospect';   
        acc2.ParentId=acc.Id;     
        insert acc2;
        
        Account acc3=new Account();
        acc3.Name='Test Account3';
        acc3.Account_Type__c='Prospect';   
        acc3.ParentId=acc2.Id;     
        insert acc3;
        
        Account acc4=new Account();
        acc4.Name='Test Account4';
        acc4.Account_Type__c='Former Client';
        insert acc4;
        
        Account acc5=new Account();
        acc5.Name='Test Account5';
        acc5.Account_Type__c='Former Client';
        acc5.ParentId=acc4.Id;
        insert acc5;
        
        /*ScheduleAccHierTypeCalculation scheduleAccTypes = new  ScheduleAccHierTypeCalculation();                
        String sch = '0 0 0 * * ?'; 
        system.schedule('Test Account Type Job', sch, scheduleAccTypes); */
         Database.executeBatch(new BatchAccHierTypeCalculation());  
         
      Test.stopTest();         
        
        list<Account> lstClientAccs=[select id from Account where Account_Hierarchy_Relationship_Type__c='Client'];     
        system.assertEquals(lstClientAccs.size(),2);
        
        list<Account> lstFormClientAccs=[select id from Account where Account_Hierarchy_Relationship_Type__c='Former Client'];      
        system.assertEquals(lstFormClientAccs.size(),2);
        
        list<Account> lstProsAccs=[select id from Account where Account_Hierarchy_Relationship_Type__c='Prospect'];     
        system.assertEquals(lstProsAccs.size(),2);
   
    
   }
}