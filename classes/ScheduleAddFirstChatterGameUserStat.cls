global class ScheduleAddFirstChatterGameUserStat implements Schedulable
{
  /*
  * Author:     Diego Olarte (Experian)
  * Description:  The following class is for scheduling the 'AddFirstChatterGameUserStat.cls' class to run at specific intervals.
  */  
  
  global void execute(SchedulableContext sc)
  {
    AddFirstChatterGameUserStat batchToProcess = new AddFirstChatterGameUserStat();
    database.executebatch(batchToProcess);
  }
}