/*=============================================================================
 * Experian
 * Name: UserCloneController
 * Description: W-005416 : Controller to handle the user 'cloning'
 * Created Date: 13 Jul 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 13 Jul 2016 (QA)   Paul Kissick          CRM2:W-005416 - New User set up - clone permissions,chatter groups from another user
 * 15 Jul 2016 (QA)   Paul Kissick          CRM2:W-005416 - Adding fix for url when finishing off, to go back to user (not profile) + enhancements
 * 25 Jul 2016 (QA)   Paul Kissick          CRM2:W-005416 - Adding more enhancements
 =============================================================================*/

public class UserCloneController {
  
  // Store 2 users, cloneToUser is the one to clone into.
  public User cloneToUser {get{if (cloneToUser == null) cloneToUser = new User(); return cloneToUser;}set;}
  public User cloneFromUser {get{if (cloneFromUser == null) cloneFromUser = new User(); return cloneFromUser;}set;}
  
  public Boolean allowClone {get{if (allowClone == null) allowClone = false; return allowClone;} private set;}
    
  // Initialise
  public UserCloneController() {
    if (ApexPages.currentPage().getParameters().containsKey('Id')) {
      Id userId = ApexPages.currentPage().getParameters().get('Id');
      cloneToUser = [
        SELECT Id, Name, UserType
        FROM User
        WHERE Id = :userId
      ];
      
      allowClone = (cloneToUser.UserType.equals('Standard')); // Sets to true if cloning to a standard user. 
      loadCurrentMembership();
    }
  }
  
  // Used in the 'Finished' button
  public PageReference backToUser() {
    PageReference pr = new ApexPages.StandardController(cloneToUser).view();
    pr.getParameters().put('noredirect','1');
    return pr;
  }
  
  private void setSelection(List<SettingWrapper> sw, Boolean state) {
    for (SettingWrapper s : sw) {
      s.selected = state;
    }
  }
  
  public PageReference selectAllPerms() {
    setSelection(availablePermSets, true);
    return null;
  }
  
  public PageReference deselectAllPerms() {
    setSelection(availablePermSets, false);
    return null;
  }
  
  public PageReference selectAllGroups() {
    setSelection(availablePublicGroups, true);
    return null;
  }
  
  public PageReference deselectAllGroups() {
    setSelection(availablePublicGroups, false);
    return null;
  }
  
  public PageReference selectAllQueues() {
    setSelection(availableQueues, true);
    return null;
  }

  public PageReference deselectAllQueues() {
    setSelection(availableQueues, false);
    return null;
  }
  
  public PageReference selectAllChatter() {
    setSelection(availableChatterGroups, true);
    return null;
  }

  public PageReference deselectAllChatter() {
    setSelection(availableChatterGroups, false);
    return null;
  }
  
  // Loads the current membership of the cloneToUser
  public void loadCurrentMembership() {
    loadUserMembership(cloneToUser.Id, currentPermSets, currentPublicGroups, currentQueues, currentChatterGroups);
  }
  
  // Take all the selected groups, etc, and add these to the user.
  public PageReference applySelections() {
    
    List<PermissionSetAssignment> newPermSets = new List<PermissionSetAssignment>();
    List<GroupMember> newGroupMembs = new List<GroupMember>();
    List<CollaborationGroupMember> newChatterGroupMembs = new List<CollaborationGroupMember>();
    
    for (SettingWrapper psw : availablePermSets) {
      if (psw.selected) {
        newPermSets.add(new PermissionSetAssignment(PermissionSetId = psw.settingId, AssigneeId = cloneToUser.Id));
      }
    }
    for (SettingWrapper pgw : availablePublicGroups) {
      if (pgw.selected) {
        newGroupMembs.add(new GroupMember(GroupId = pgw.settingId, UserOrGroupId = cloneToUser.Id));
      }
    }
    for (SettingWrapper qw : availableQueues) {
      if (qw.selected) {
        newGroupMembs.add(new GroupMember(GroupId = qw.settingId, UserOrGroupId = cloneToUser.Id));
      }
    }
    for (SettingWrapper cgw : availableChatterGroups) {
      if (cgw.selected) {
        newChatterGroupMembs.add(new CollaborationGroupMember(CollaborationGroupId = cgw.settingId, MemberId = cloneToUser.Id));
      }
    }
    
    doInserts(newPermSets);
    doInserts(newGroupMembs);
    doInserts(newChatterGroupMembs);
    
    loadCurrentMembership();
    loadFromUserMembership();
    return null;
  }
  
  private void doInserts(List<sObject> objs) {
    List<Database.SaveResult> res = Database.insert(objs, false);
    for (Database.SaveResult sr : res) {
      if (!sr.isSuccess()) {
        for (Database.Error er : sr.getErrors()) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, 'Error occurred: '+er.getMessage()));
        }
      }
    }
  }
  
  
  public PageReference loadFromUserMembership() {
    
    loadUserMembership(cloneFromUser.ManagerId, availablePermSets, availablePublicGroups, availableQueues, availableChatterGroups);
    
    // for each of the available options, if the destination user has it, remove it...
    List<SettingWrapper> holdingSettings = new List<SettingWrapper>();
    Boolean foundSetting = false;
    
    for (SettingWrapper sw : availablePermSets) {
      foundSetting = false;
      for (SettingWrapper csw : currentPermSets) {
        if (sw.settingId == csw.settingId) {
          foundSetting = true;
        }
      }
      if (foundSetting == false) {
        holdingSettings.add(sw);
      }
    }
    availablePermSets.clear();
    availablePermSets.addAll(holdingSettings);
    holdingSettings.clear();
    
    for (SettingWrapper sw : availablePublicGroups) {
      foundSetting = false;
      for (SettingWrapper csw : currentPublicGroups) {
        if (sw.settingId == csw.settingId) {
          foundSetting = true;
        }
      }
      if (foundSetting == false) {
        holdingSettings.add(sw);
      }
    }
    availablePublicGroups.clear();
    availablePublicGroups.addAll(holdingSettings);
    holdingSettings.clear();
    
    for (SettingWrapper sw : availableQueues) {
      foundSetting = false;
      for (SettingWrapper csw : currentQueues) {
        if (sw.settingId == csw.settingId) {
          foundSetting = true;
        }
      }
      if (foundSetting == false) {
        holdingSettings.add(sw);
      }
    }
    availableQueues.clear();
    availableQueues.addAll(holdingSettings);
    holdingSettings.clear();
    
    
    for (SettingWrapper sw : availableChatterGroups) {
      foundSetting = false;
      for (SettingWrapper csw : currentChatterGroups) {
        if (sw.settingId == csw.settingId) {
          foundSetting = true;
        }
      }
      if (foundSetting == false) {
        holdingSettings.add(sw);
      }
    }
    availableChatterGroups.clear();
    availableChatterGroups.addAll(holdingSettings);
    holdingSettings.clear();
    
    
    
    return null;
  }
  
  public void loadUserMembership(Id userId, List<SettingWrapper> permSets, List<SettingWrapper> publicGroups, List<SettingWrapper> queues, List<SettingWrapper> chatterGroups) {
    
    permSets.clear();
    publicGroups.clear();
    queues.clear();
    chatterGroups.clear();
    
    for (PermissionSet ps : [SELECT Id, Label 
                             FROM PermissionSet 
                             WHERE Id IN (
                               SELECT PermissionSetId 
                               FROM PermissionSetAssignment 
                               WHERE AssigneeId = :userId
                             ) 
                             AND IsOwnedByProfile = false
                             ORDER BY Label
                             ]) {
      permSets.add(new SettingWrapper(ps));
    }
    //selectAllPerms();
    
    Map<Id, Group> allGroups = new Map<Id, Group>([
      SELECT Id, Name 
      FROM Group 
      WHERE Id IN (
        SELECT GroupId 
        FROM GroupMember 
        WHERE UserOrGroupId = :userId
      )
      AND RelatedId = null
      ORDER BY Name
    ]);
    
    Set<Id> queueIds = new Set<Id>();
    
    for (QueueSobject q : [SELECT Id, QueueId 
                           FROM QueueSobject 
                           WHERE QueueId IN :allGroups.keySet()]) {
      queueIds.add(q.QueueId);
    }
    
    
    for (Group g : allGroups.values()) {
      if (queueIds.contains(g.Id)) {
        queues.add(new SettingWrapper(g));
      }
      else {
        publicGroups.add(new SettingWrapper(g));
      }
    }
    
    //selectAllGroups();
    //selectAllQueues();
    
    for (CollaborationGroup chg : [SELECT Id, Name 
                                   FROM CollaborationGroup 
                                   WHERE Id IN (
                                     SELECT CollaborationGroupId 
                                     FROM CollaborationGroupMember 
                                     WHERE MemberId = :userId
                                   )
                                   ORDER BY Name]) {
      chatterGroups.add(new SettingWrapper(chg));
    }
    
  }
  
  public List<SettingWrapper> availablePermSets {get{if (availablePermSets == null) availablePermSets = new List<SettingWrapper>(); return availablePermSets;}set;}
  public List<SettingWrapper> availablePublicGroups {get{if (availablePublicGroups == null) availablePublicGroups = new List<SettingWrapper>(); return availablePublicGroups;}set;}
  public List<SettingWrapper> availableQueues {get{if (availableQueues == null) availableQueues = new List<SettingWrapper>(); return availableQueues;}set;}
  public List<SettingWrapper> availableChatterGroups {get{if (availableChatterGroups == null) availableChatterGroups = new List<SettingWrapper>(); return availableChatterGroups;}set;}
  
  public List<SettingWrapper> currentPermSets {get{if (currentPermSets == null) currentPermSets = new List<SettingWrapper>(); return currentPermSets;}set;}
  public List<SettingWrapper> currentPublicGroups {get{if (currentPublicGroups == null) currentPublicGroups = new List<SettingWrapper>(); return currentPublicGroups;}set;}
  public List<SettingWrapper> currentQueues {get{if (currentQueues == null) currentQueues = new List<SettingWrapper>(); return currentQueues;}set;}
  public List<SettingWrapper> currentChatterGroups {get{if (currentChatterGroups == null) currentChatterGroups = new List<SettingWrapper>(); return currentChatterGroups;}set;}
  
  public class SettingWrapper {
    public Id settingId {get;set;}
    public String settingName {get;set;}
    public Boolean selected {get;set;}
    public SettingWrapper(PermissionSet p) {
      settingId = p.Id;
      settingName = p.Label;
      selected = true;
    }
    public SettingWrapper(Group g) {
      settingId = g.Id;
      settingName = g.Name;
      selected = true;
    }
    public SettingWrapper(CollaborationGroup c) {
      settingId = c.Id;
      settingName = c.Name;
      selected = false;
    }
  }
  
}