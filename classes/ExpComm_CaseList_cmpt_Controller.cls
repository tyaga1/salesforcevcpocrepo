/******************************************************************************
 * Name: ExpComm_CaseList_cmpt_Controller.cls
 * Created Date: 2/17/2017
 * Created By: Hay Win, UCInnovation
 * Description : Controller for CaseList page for expcomm new community
 * Change Log- 
 ****************************************************************************/
 
public with sharing class ExpComm_CaseList_cmpt_Controller{


    @AuraEnabled
    public static List<caseWrapper> findAll(String orderBy, String order) {
        List<caseWrapper> caseList = new List<caseWrapper>();
        try {
            String query = 'SELECT id, CaseNumber, Subject, Status, CreatedDate, LastModifiedDate FROM Case Where createdByID = \'' + UserInfo.getUserId()
                         + '\' AND RecordType.Name = \'Platform Support\' Order By ' + orderBy + ' ' + order + ', CreatedDate DESC LIMIT 500';
                        
            caseList = constructWrapperAndMergeCases(Database.query(query), ExpComm_CaseList_cmpt_Controller.getServiceNowCases());
        }
        catch (exception e) {

            if (e.getTypeName() == 'System.AuraHandledException') {
                throw e;
            }
            else {
                throw new AuraHandledException('Some exception happened on server side controller with the following error:\n\n'+ e.getMessage());
            }
        }
        return caseList;
       
    }

    public static List<ServiceNowCase> getServiceNowCases() {

        List<ServiceNowCase> serviceNowCaseList = new List<ServiceNowCase>();

        Http http = new Http();
        HttpRequest req = ExpComm_CaseList_cmpt_Controller.createHTTPRequest();

        try {
            HttpResponse res = http.send(req);

            if (res.getStatusCode() != 200) {
                throw new AuraHandledException('Service Now Cases API callout was not successful:\n\n' + 'Status Code: ' + res.getStatusCode() + '\n\n' + 'Status Message: ' + res.getBody());
            }

            String responJSONstr = res.getBody().replace('\"request.number\":', '\"request_number\":');
            responJSONstr = responJSONstr.replace('\"request.sys_id\":', '\"request_sys_id\":');

            // Parse JSON response to get all the totalPrice field values.
            JSONParser parser = JSON.createParser(responJSONstr);
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                    while (parser.nextToken() != null) {
                        if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                            serviceNowCaseList.add((ServiceNowCase)parser.readValueAs(ServiceNowCase.class));
                            parser.skipChildren();
                        }
                    }
                }
            }
        }
        catch (exception e) {
            if (e.getTypeName() == 'System.AuraHandledException') {
                throw e;
            }
            else {
                throw new AuraHandledException('Service Now Cases API callout was not successful:\n\n' + e.getMessage());
            }
        }

        return serviceNowCaseList;
    }

    public static HttpRequest createHTTPRequest() {
        HttpRequest req = new HttpRequest();
        String jsonBody;

        User currentUser = [SELECT Id, LAN_ID__c, EmployeeNumber, Email FROM User WHERE Id =: UserInfo.getUserId()];
        String dataLimit = '500';

        /*
         *      {0} : {$user.lan_ID__c}
         *      {1} : {$user.EmployeeNumber}
         *      {2} : {$user.Email}
         *      {3} : {limit}
         */
        //String urlPlaceholder = 'callout:Service_Now/api/now/table/sc_req_item?sysparm_query=opened_by.user_name%3D{0}%5EORopened_by.employee_number%3D{1}%5EORopened_by.email%3D{2}&sysparm_fields=parent%2Crequest%2Copened_at%2Cclosed_at%2Csys_updated_on%2Crequest.link%2Crequest.number%2Cshort_description%2Cstage&sysparm_display_value=true&sysparm_limit={3}';
        //RJ Changes starts
        
        //String urlPlaceholder = 'callout:Service_Now/api/now/table/sc_req_item?sysparm_query=opened_by.user_name%3D{0}%5EORopened_by.employee_number%3D{1}%5EORopened_by.email%3D{2}&sysparm_fields=opened_at%2Cclosed_at%2Csys_updated_on%2Crequest.link%2Crequest.number%2Crequest.sys_id%2Cshort_description%2Cstage&sysparm_display_value=true&sysparm_limit={3}';
        
        String sysParmQuery = null;
        
        if (currentuser.Lan_id__c != null)
                sysParmQuery = 'sysparm_query=opened_by.user_name%3D{0}';
          
         if(currentuser.EmployeeNumber != null){
         
             if (sysParmQuery == null )
                 sysParmQuery = 'sysparm_query=opened_by.employee_number%3D{1}';
             else
                 sysParmQuery += '%5EORopened_by.employee_number%3D{1}';
         
         }
         
          if(currentuser.email!= null){
         
             if (sysParmQuery == null )
                 sysParmQuery = 'sysparm_query=opened_by.email%3D{2}';
             else
                 sysParmQuery += '%5EORopened_by.email%3D{2}';
         
         }


        String urlPlaceholder = 'callout:Service_Now/api/now/table/sc_req_item?'+sysParmQuery +'&sysparm_fields=opened_at%2Cclosed_at%2Csys_updated_on%2Crequest.link%2Crequest.number%2Crequest.sys_id%2Cshort_description%2Cstage&sysparm_display_value=true&sysparm_limit={3}';

        //RJ Ends here
        
        List<String> fillers = new String[]{currentUser.LAN_ID__c, currentUser.EmployeeNumber, currentUser.Email, dataLimit};

        String endpointURL = String.format(urlPlaceholder, fillers);

        //Blob headerValue = Blob.valueOf('SF_EMP_API:=+_43WYuK[kJ');
        //String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);

        req.setEndpoint(endpointURL);
        req.setMethod('GET');
        //req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type', 'Application/JSON');
        System.debug('Richard endpointURL'+endpointURL);
        return req;
    }

    public static List<caseWrapper> constructWrapperAndMergeCases(List<Case> salesforceCaseList, List<ServiceNowCase> serviceNowCaseList){
        TimeZone userTimezone = UserInfo.getTimeZone();
        List<caseWrapper> caseWrapperList = new List<caseWrapper>();

        String ServiceNowUrlTargerSystem = Label.ExpComm_ServiceNow_URL_target_system;

        String CommunityPathPrefix = Site.getPathPrefix();
        if (CommunityPathPrefix == null || CommunityPathPrefix == '') {
            CommunityPathPrefix = '/apex';
        }

        for(case c : salesforceCaseList) {
            caseWrapper cw = new caseWrapper(false);
            cw.Id = c.Id;
            cw.CaseNumber = c.CaseNumber;
            cw.Subject = c.Subject;
            cw.Status = c.Status;
            cw.CreatedDate = c.CreatedDate.format('yyyy-MM-dd HH:mm:ss', userTimezone.getID());
            cw.LastModifiedDate = c.LastModifiedDate.format('yyyy-MM-dd HH:mm:ss', userTimezone.getID());
            cw.Link = URL.getSalesforceBaseUrl().toExternalForm() + CommunityPathPrefix + '/ExpComm_viewcase?id=' + c.Id;
            caseWrapperList.add(cw);   
        }

        for (ServiceNowCase sc : serviceNowCaseList) {
            caseWrapper cw = new caseWrapper(true);
            cw.Id = sc.request_sys_id;
            cw.CaseNumber = sc.request_number;
            cw.Subject = sc.short_description;
            cw.Status = sc.stage;
            //cw.CreatedDate = sc.opened_at;
            //cw.LastModifiedDate = sc.sys_updated_on;
            cw.CreatedDate = convertPstToUserTimeZone(sc.opened_at, userTimezone);
            cw.LastModifiedDate = convertPstToUserTimeZone(sc.sys_updated_on, userTimezone);
            cw.Link = 'https://' + ServiceNowUrlTargerSystem + '/nav_to.do?uri=/sc_request.do?sys_id=' + cw.Id;
            caseWrapperList.add(cw);
        }

        return caseWrapperList;
    }

    public static String convertPstToUserTimeZone(String pstTimeZoneString, TimeZone userTimezone) {
        Datetime pstDateTime = Datetime.valueOfGmt(pstTimeZoneString);
        return pstDateTime.format('yyyy-MM-dd HH:mm:ss', userTimezone.getID());
    }

    /****Wrapper Classes***/
    public class caseWrapper{
        @AuraEnabled
        public String Id {get; set;}
        @AuraEnabled
        public String CaseNumber {get; set;}
        @AuraEnabled
        public String Subject {get; set;}
        @AuraEnabled
        public String Status {get; set;}
        @AuraEnabled
        public String CreatedDate {get; set;}
        @AuraEnabled
        public String LastModifiedDate {get; set;}
        @AuraEnabled
        public String CaseOrigin {get; set;}
        @AuraEnabled
        public String Link {get; set;}
        
        public caseWrapper(Boolean isServiceNowCase){
            if (isServiceNowCase == true) {
                CaseOrigin = 'ServiceNow Case';
            }
            else {
                CaseOrigin = 'Salesforce Case';
            }
        }
    }

    /*  Inner Class for service now  */
    public class ServiceNowCase {
        public String opened_at;
        public String short_description;
        public String closed_at;
        public String request_sys_id;
        public String request_number;
        public String stage;
        public String sys_updated_on;

    }

    //public class ServiceNowCaseRequestField {
    //    public String link;
    //    public String value;
    //}

}