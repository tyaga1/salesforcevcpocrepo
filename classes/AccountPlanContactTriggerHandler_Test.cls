/**=====================================================================
 * Appirio, Inc
 * Name: AccountPlanContactTriggerHandler_Test
 * Description: T-435624, Test class for AccountContactTriggerHandler
 * Created Date: Sept 28th, 2015
 * Created By: Jagjeet Singh (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * Sept 29th,2015               Jagjeet Singh(Appirio)       getting the error message from Custom Label.
 * Apr  29th,2016               James Wills                  Case #01848189 Account Planning Project. Deprecated validation for ACCOUNTPLANNING_VALIDATION_MSG_ON_DELETE
 =====================================================================*/
@isTest
private class AccountPlanContactTriggerHandler_Test {
    //test Method for Currency ISO Codes.
    static testMethod void testSyncCurrencyISOCodes(){
      User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
      insert testUser1; 
      
      // insert account
      Account account = Test_Utils.insertAccount();
      
      // create account plan
      Account_Plan__c accountPlan = Test_Utils.insertAccountPlan(false, account.id);
      accountPlan.Name = 'TestAccountPlan';
      insert accountPlan;
      
      Account_Plan_Contact__c apc = new Account_Plan_Contact__c(Account_Plan__c = accountPlan.id,
                                       CurrencyIsoCode = 'USD');
      insert apc;
               
      accountPlan.Name = 'TestAccountPlan1';
      accountPlan.CurrencyIsoCode = 'INR';    
      update accountplan;
  
      System.assertEquals([SELECT CurrencyIsoCode FROM Account_Plan_Contact__c WHERE Account_Plan__c =: accountPlan.ID].CurrencyIsoCode, accountPlan.CurrencyIsoCode);
    }
    
    
    //test the Validate Records Negative Scenario.
    static testMethod void testValidateRecordsToBeDeleted_Negative(){
      //start test
      Test.startTest();
      User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
      insert testUser1; 
      
      // insert account
      Account account = Test_Utils.insertAccount();
      
      // create account plan
      Account_Plan__c accountPlan = Test_Utils.insertAccountPlan(false, account.id);
      accountPlan.Name = 'TestAccountPlan';
      insert accountPlan;
      
      Account_Plan_Contact__c apc = new Account_Plan_Contact__c(Account_Plan__c = accountPlan.id,
                                       CurrencyIsoCode = 'USD');
      insert apc;
      
      //Case #01848189 Account Planning Project
      //This validation is no longer required.
      //String expectedMsg = Label.ACCOUNTPLANNING_VALIDATION_MSG_ON_DELETE;
      //assertErrorOnDelete(apc,expectedMsg);
      
      Test.stopTest();
    }
    
    
    //test the Validate Records Positive Scenario.
    static testMethod void testValidateRecordsToBeDeleted_Positive(){
      //start Test
      Test.startTest();
      User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
      insert testUser1; 
      
      // insert account
      Account account = Test_Utils.insertAccount();
      Id currentUserId = UserInfo.getUserId();
      //insert the accountTeamMember
      AccountTeamMember accTeamMem = Test_Utils.insertAccountTeamMember(true,account.Id,currentUserId,'test');
      
      // create account plan
      Account_Plan__c accountPlan = Test_Utils.insertAccountPlan(false, account.id);
      accountPlan.Name = 'TestAccountPlan';
      insert accountPlan;
      
      Account_Plan_Team__c accPlanTeamMem = Test_Utils.insertAccountPlanTeam(true,accountPlan.Id,currentUserId);
      
      Account_Plan_Contact__c apc = new Account_Plan_Contact__c(Account_Plan__c = accountPlan.id,
                                       CurrencyIsoCode = 'USD');
      insert apc;
      
      delete apc;
      List<Account_Plan_Contact__c> apcDB = [select Id from Account_Plan_Contact__c where Id = :apc.Id];
      //assert
      system.assertEquals(apcDB.size(),0,'Account Plan Competitor should be deleted.');
      Test.stopTest();
    }
    
    //Case #01848189 Account Planning Project
    //This validation is no longer required.
    //
    //assert error message on delete when the current user is not in AccountTeamMember.
    //private static void assertErrorOnDelete(SObject sObj, String expectedMsg) {
    //  try {
    //        delete sObj;
    //        System.assert(false, 'exception expected for SObject: ' + sObj);
    //  } catch (Exception e) {
    //        System.assert(e.getMessage().contains(expectedMsg), 'message=' + e.getMessage());
    //  }
    //}
}