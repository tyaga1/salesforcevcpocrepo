/*=============================================================================
 * Experian
 * Name: NominationHomeExt
 * Description: 
 * Created Date: 3 Nov 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 
 * TODO: Replace sObject with the correct API name
 =============================================================================*/

public with sharing class NominationHomeExt {
  
  ApexPages.StandardController stdCon;
  Nomination__c currentRecord;
  
  //===========================================================================
  // Each Section is referenced here. Create another one, and initialise using
  // a query in the initialisePage() method.
  //===========================================================================
  public SectionPaging myPendingApproval {get;set;}
  public SectionPaging myRewards {get;set;}
  public SectionPaging mySubmissions {get;set;}
  
  public NominationHomeExt(ApexPages.StandardController con) {
    stdCon = con;
    currentRecord = (Nomination__c)stdCon.getRecord();
  }
  
  //===========================================================================
  // Called on apex:page action - loads the queries
  //===========================================================================
  public PageReference initialisePage() {
    myPendingApproval = new SectionPaging(
      ' SELECT Id, Nominee__r.FullPhotoUrl, Nominee__r.SmallPhotoUrl, Nominee__c, Nominee__r.Title, Status__c, '+ 
      ' Nominees_Manager__r.Name, Payroll_Award_Date__c, Nominee__r.Name, Manager_Approved_Date__c, Approver__c, ' +
      ' LastModifiedDate, Type__c, Requestor__c, Team_Name__c ' +
      ' FROM Nomination__c ' +
      ' WHERE OwnerId = \''+UserInfo.getUserId()+'\' ' +
      ' AND Status__c = \''+NominationHelper.NomConstants.get('PendingApproval')+'\' ' +
      ' ORDER BY CreatedDate DESC'
    );
    myRewards = new SectionPaging(
      ' SELECT Id, Nominee__r.FullPhotoUrl, Nominee__r.SmallPhotoUrl, Nominee__c, Nominee__r.Title, ' +
      '        Type__c, Status__c, Requestor__c, Nominees_Manager__r.Name, Payroll_Award_Date__c,  ' +
      '        Nominee__r.Name, Manager_Approved_Date__c, Approver__c, Badge__r.Name, Badge__r.Recognition_Category__c, Badge__r.ImageUrl ' +
      ' FROM Nomination__c ' +
      ' WHERE Nominee__c != null ' +
      ' AND Nominee__c = \''+UserInfo.getUserId()+'\' ' +
      ' AND Nominee_Viewable__c = true ' +
      ' ORDER BY CreatedDate DESC '
      , 3
    );
    mySubmissions = new SectionPaging(
      'SELECT Id, Nominee__r.FullPhotoUrl, Nominee__r.SmallPhotoUrl, Nominee__c, Nominee__r.Title, ' +
      '       Status__c, Nominees_Manager__r.Name, Payroll_Award_Date__c, Nominee__r.Name, Manager_Approved_Date__c, Type__c, ' +
      '       Team_Name__c, Badge__r.ImageUrl ' +
      ' FROM Nomination__c ' +
      ' WHERE Requestor__c != null ' +
      ' AND Requestor__c = \''+UserInfo.getUserId()+'\' ' +
      ' ORDER BY CreatedDate DESC'
    );
    return null;
  }
  
  //===========================================================================
  // Navigate to the 'New' reward page.
  //===========================================================================
  public PageReference navToNewRewardPage() {
    // PageReference pageRef = new PageReference('https://experian--supportdev--c.cs22.visual.force.com/apex/NominationNew');
    return Page.NominationNew;
  }
  
  //===========================================================================
  //TP: Nav Function for Manager Dashboard
  //===========================================================================
  public PageReference navToManagerDashboard() {
    // Never code full urls!
    // https://experian--supportdev.cs22.my.salesforce.com
    // Consider a way we can get this id without hard coding!
    List<Dashboard> dash = [
      SELECT Id 
      FROM Dashboard 
      WHERE DeveloperName = 'Recognition_Award_Committee_Dashboard'
      LIMIT 1
    ];
    if (!dash.isEmpty()) {
      ApexPages.StandardController tmpCon = new ApexPages.StandardController(dash.get(0));
      return tmpCon.view();
    }
    return null;
  }
  
  //===========================================================================
  // TP: Nav Function for Rec Coordinator Dashboard
  // TODO: Fix this to reference the correct dashboard!
  //===========================================================================
  public PageReference navToRecognitionCoordinatorDashboard() {
    //PageReference pageRef = new PageReference('/01Z170000009HAj');
    PageReference pageRef = new PageReference('/00OJ0000000QDK8');
    return pageRef;
  }
  
  //===========================================================================
  // 
  //===========================================================================
  public List<Nomination__c> getRecentNominations() {
    return [
      SELECT Id, Nominee__r.FullPhotoUrl, Nominee__r.SmallPhotoUrl, Nominee__c, Nominee__r.Title, 
             Status__c, Nominees_Manager__r.Name, Payroll_Award_Date__c, Type__C, Requestor__c, Nominee__r.Name, 
             Manager_Approved_Date__c
      FROM Nomination__c
      WHERE LastViewedDate != null 
      ORDER BY LastViewedDate DESC 
      limit 10
    ];
  }
  
  //===========================================================================
  // Inner class to handle multiple sections on the page without recoding every
  // time. 
  // Note: This class return nomination records in getNomRecords().
  // Create new methods if return different sobjects.
  //===========================================================================
  public class SectionPaging {
    
    public String initQuery {get;set;}
    public Integer defaultSize {get{if (defaultSize == null) defaultSize = 5; return defaultSize;}set;}
    public Integer totalSize {get;set;}
    public Integer totalPages {get;set;}
    public Integer gotoPageNum {get;set;}
    
    public SectionPaging(String q) {
      initQuery = q;
    }
    
   public SectionPaging(String q, Integer s) {
      initQuery = q;
      defaultSize = s;
    }
    
    //===========================================================================
    // 
    //===========================================================================
    public ApexPages.StandardSetController setCon {
      get {
        if (setCon == null) {
          setCon = new ApexPages.StandardSetController(
            Database.getQueryLocator(initQuery)
          );
          setCon.setPageSize(defaultSize); 
          totalSize = setCon.getResultSize();
          totalPages = Integer.valueOf(Math.ceil(Decimal.valueOf(totalSize) / Decimal.valueOf(defaultSize)));
        }
        return setCon;
      }
      set;
    }
    
    public List<Nomination__c> getNomRecords() {
      return (List<Nomination__c>)setCon.getRecords();
    }
    
    public List<Integer> getPageList() {
      List<Integer> pages = new List<Integer>();
      for (Integer i = 1; i <= totalPages; i++) {
        pages.add(i);
      }
      return pages;
    }
    
    
    public PageReference gotoPage() {
      if (gotoPageNum != null) {
        setCon.setPageNumber(gotoPageNum);
      }
      return null;
    }
  
    public Integer getCurrentPage() {
      return setCon.getPageNumber();
    }
  
    public Boolean getHasNext() {
      return setCon.getHasNext();
    }
  
    public Boolean getHasPrevious() {
      return setCon.getHasPrevious();
    }
  
    public PageReference getNext() {
      setCon.next();
      return null;
    }
  
    public PageReference getPrevious() {
      setCon.previous();
      return null;
    }
  }  
  
}