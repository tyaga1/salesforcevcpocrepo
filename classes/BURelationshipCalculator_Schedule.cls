/**=====================================================================
 * Appirio, Inc
 * Name: BURelationshipCalculator_Schedule
 * Description: Schedule BURelationshipCalculator_Batch to run daily
 * Created Date: Nov 07th, 2013
 * Created By: Pankaj Mehra (Appirio) 
 * 
 * Date Modified                Modified By                  Description of the update
 * Apr 23, 2014                 Nathalie Le Guay             Commenting class
 * Sep 9th, 2015                Paul Kissick                 Fixing for test coverage clear down. (TODELETE)
 =====================================================================*/
/*global*/public class BURelationshipCalculator_Schedule /*implements Schedulable*/ {
  public BURelationshipCalculator_Schedule () {
    system.debug('Nothing');
  }
   /*global void execute(SchedulableContext SC) {
      BURelationshipCalculator_Batch batch = new BURelationshipCalculator_Batch(); 
      Database.executeBatch(batch, 200);
   }*/
}