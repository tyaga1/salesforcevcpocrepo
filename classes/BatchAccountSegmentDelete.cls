/**=====================================================================
 * Appirio, Inc
 * Name: BatchAccountSegmentDelete
 * Description: Batch job for deleting Account Segment that are stale
 *              Rule: - Last update is over 3 months ago (part of the query)
 *                    - Not rolling up any Opportunity or Order (first portion of 'execute()')
 *                    - No Account Team Member represents the segments. 
 *                      Example: if no Account Team Member where User's Country = 'France', we
 *                      can remove the Account Segments where Value__c='France' and Type__c='Country'
 *                      (second portion of 'execute')
 *              Will send an email to address specified in the Global Settings, if failure
 * Created Date: Aug 31st, 2015
 * Created By: Nathalie Le Guay
 *
 * Date Modified                Modified By                  Description of the update
 * Sept 17th, 2015              Nathalie Le Guay             Fixed initial query (< instead of >)
 * Oct 12th, 2015               Noopur                       Updated query in start() method
 * Nov 9th, 2015                Paul Kissick                 Case 01234035: Adding FailureNotificationUtility
 * Dec 3rd, 2015                Paul Kissick                 Case 01266075: Fixing use of Global_Settings__c for timings
 =======================================================================*/
global class BatchAccountSegmentDelete implements  Database.Batchable<sObject>, Database.Stateful {
  
  global Set<String> failedAccSegmentDeleteRecords;
  
  private Datetime lastStartDate;
  private Datetime holdingRunTime;
  
  //========================================================================================
  // Start
  //========================================================================================
  global Database.QueryLocator start(Database.BatchableContext BC){
    System.debug('\nLimit Check #1: '+ Limits.getCpuTime());
    
    Datetime lastStartDate = BatchHelper.getBatchClassTimestamp('BatchAccountSegmentDeleteLastRun'); // Doesn't appear to be used!
    
    holdingRunTime = Datetime.now();
    
    Datetime threeMonthsAgo = Datetime.now().addDays(-90); 
    system.debug('====threeMonthsAgo>>>' +threeMonthsAgo);
    String queryString = 'SELECT Id, Account__c, Value__c, Type__c, LastModifiedDate, Name'
                    + ' FROM Account_Segment__c WHERE LastModifiedDate < :threeMonthsAgo' ;
                                
    System.debug('\nLimit Check #2: '+ Limits.getCpuTime());
    System.debug('\n\nQuery:\n' + queryString);

    Database.QueryLocator result = Database.getQueryLocator(queryString);
    System.debug('\nLimit Check #3: '+ Limits.getCpuTime() + ' and limit cpu time (getLimitCpuTime): ' + Limits.getLimitCpuTime());
    return result;
  }

  //========================================================================================
  // Execute
  //========================================================================================
  global void execute(Database.BatchableContext BC, List<sObject> scope) {
    system.debug('====scope>>>' +scope);
    List<Account_Segment__c> accountSegments1 = (List<Account_Segment__c>) scope;
    Set<Id> accountIds = new Set<Id>();

    List<Account_Segment__c> deleteCandidateAccountSegmentsList = new List<Account_Segment__c>();
    List<Account_Segment__c> deleteAccountSegmentsList = new List<Account_Segment__c>();
    
    System.debug('\nLimit Check #4: '+ Limits.getCpuTime());
    
    List<Account_Segment__c> accountSegments = new List<Account_Segment__c>();
    for (Account_Segment__c accSeg : [SELECT Id, Account__c, Value__c, Type__c, LastModifiedDate, Name,
                                             (SELECT Id FROM Opportunities_Global_Lines_of_Business__r),
                                             (SELECT Id FROM Opportunities_Business_Lines__r),
                                             (SELECT Id FROM Opportunities_Business_Units__r),
                                             (SELECT Id FROM Opportunities_Regions__r),
                                             (SELECT Id FROM Opportunities_Countries__r),
                                             (SELECT Id FROM Orders_Global_Business_Lines__r),
                                             (SELECT Id FROM Orders_Business_Lines__r),
                                             (SELECT Id FROM Orders_Business_Units__r),
                                             (SELECT Id FROM Orders_Regions__r),
                                             (SELECT Id FROM Orders_Countries__r)
                                     FROM Account_Segment__c WHERE Id IN :accountSegments1 ]) {
        accountSegments.add(accSeg);
    }
    system.debug('===accountSegments>'+accountSegments.size());
    // Because there can't be more than 1 joint sub-query, we are querying the most restrictive list
    // of Account Segments (filter = modified more than 3 months ago) and will then identify the 
    // Account Segments that don't have any Opportunity or Order attached to them.
    for (Account_Segment__c accountSegment : accountSegments) {
      if (
        (accountSegment.Opportunities_Global_Lines_of_Business__r.size() == 0)
        &&
        (accountSegment.Opportunities_Business_Lines__r.size() == 0)
        &&
        (accountSegment.Opportunities_Business_Units__r.size() == 0)
        &&
        (accountSegment.Opportunities_Regions__r.size() == 0)
        &&
        (accountSegment.Opportunities_Countries__r.size() == 0)
        && (accountSegment.Orders_Global_Business_Lines__r.size() == 0)
        &&
        (accountSegment.Orders_Business_Lines__r.size() == 0)
        &&
        (accountSegment.Orders_Business_Units__r.size() == 0)
        &&
        (accountSegment.Orders_Regions__r.size() == 0)
        &&
        (accountSegment.Orders_Countries__r.size() == 0)
        ) {
        deleteCandidateAccountSegmentsList.add(accountSegment);
        accountIds.add(accountSegment.Account__c);
      }
    }
    System.debug('\ndeleteCandidateAccountSegmentsList '+ deleteCandidateAccountSegmentsList.size());
    System.debug('\naccountIds.size() '+ accountIds.size());
    System.debug('\naccountIds '+ accountIds);
    
    System.debug('\nLimit Check #5: '+ Limits.getCpuTime());

    Map<Id, Set<String>> accountToSegmentNames = new Map<Id, Set<String>>();
    // Here we are gathering all the segment values (GBL, BL, BU, Region, Country) of the users 
    // associated with the ATM listed under the Accounts linked to our Accounts Segments identified
    // as candidates for deletion
    for (AccountTeamMember member : [SELECT Id, UserId, User.Global_Business_Line__c, User.Business_Line__c, User.Business_Unit__c, User.Region__c, User.Country__c, AccountId 
                                     FROM AccountTeamMember
                                     WHERE AccountId in: accountIds ]) {
      if (!accountToSegmentNames.containsKey(member.AccountId)) {
        accountToSegmentNames.put(member.AccountId, new Set<String>());
      }
      String gbl = member.User.Global_Business_Line__c;
      String bl = member.User.Business_Line__c;
      String bu = member.User.Business_Unit__c;
      String region = member.User.Region__c;
      String country = member.User.Country__c;
      if (String.isNotBlank(gbl)) {
        accountToSegmentNames.get(member.AccountId).add(gbl);
      }
      if (String.isNotBlank(bl)) {
        accountToSegmentNames.get(member.AccountId).add(bl);
      }
      if (String.isNotBlank(bu)) {
        accountToSegmentNames.get(member.AccountId).add(bu);
      }
      if (String.isNotBlank(region)) {
        accountToSegmentNames.get(member.AccountId).add(region);
      }
      if (String.isNotBlank(country)) {
        accountToSegmentNames.get(member.AccountId).add(country);
      }
    }
    System.debug('\naccountToSegmentNames '+ accountToSegmentNames);
    System.debug('\nLimit Check #6: '+ Limits.getCpuTime());
   
    for (Account_Segment__c accountSegment : deleteCandidateAccountSegmentsList) {
      // If there is no ATM who had this segment value AT ALL on the Account, we definitely don't 
      // care about this Account Segment anymore
      system.debug('==accountToSegmentNames.get(accountSegment.Account__c)>>' +accountToSegmentNames.get(accountSegment.Account__c) + 'accountSegment.Value__c)>>' + accountSegment.Value__c);
      
      if (accountToSegmentNames.get(accountSegment.Account__c) == null ||
            (accountToSegmentNames.get(accountSegment.Account__c) != null && !accountToSegmentNames.get(accountSegment.Account__c).contains(accountSegment.Value__c))) {
        deleteAccountSegmentsList.add(accountSegment);
      }

    }
    System.debug('\ndeleteAccountSegmentsList '+ deleteAccountSegmentsList.size());
    system.debug('\nFinal Values');
    for (Account_Segment__c acct: deleteAccountSegmentsList) {
      system.debug('Account: ' + acct.Account__c + ' Segment Name: ' + acct.Name);
    }
    delete deleteAccountSegmentsList;
    
    System.debug('\nLimit Check #7: '+ Limits.getCpuTime());
    System.debug('~~~~~~'+deleteAccountSegmentsList.size());

  }

  //========================================================================================
  // Finish
  //========================================================================================
  global void finish(Database.BatchableContext BC) {
    
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(BC.getJobId(), 'BatchAccountSegmentDelete', false);
    
    Integer numberOfErrors = 0;
    if (failedAccSegmentDeleteRecords != null && failedAccSegmentDeleteRecords.size() > 0) {
      numberOfErrors = failedAccSegmentDeleteRecords.size();
      bh.batchHasErrors = true;
    }

    String emailBody = '';
    
    if (failedAccSegmentDeleteRecords != null && failedAccSegmentDeleteRecords.size() > 0) {
      
      emailBody += '*** Account Segment record delete failed: \n';  
      // emailBody += '<table><tr ><td width="15%">Account Id</td><td width="15%">Type</td><td width="15%">Value</td><td width="*">Exception</td></tr>';
      for (String currentRow : failedAccSegmentDeleteRecords) {
        emailBody += currentRow + '\n\n';
      }
      emailBody += '\n\n';
    }
    
    bh.emailBody += emailBody;
    
    bh.sendEmail();
        
    BatchHelper.setBatchClassTimestamp('BatchAccountSegmentDeleteLastRun', holdingRunTime);
  }
}