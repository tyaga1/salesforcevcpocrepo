/**=====================================================================
 * Experian
 * Name: ScheduleEDQAddressesDependency
 * Description: Schedules the BatchEDQAddressesDependency class.
                Designed to collate all EDQ Address Dependency records and loads the first batch.
                This will then cycle through all records, until the list is empty.
                
                Please execute this class as an IsDataAdmin user, running daily overnight (uk time).
                
 * Created Date: 14 Mar 2016
 * Created By: Paul Kissick (Experian)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
global class ScheduleEDQAddressesDependency implements Schedulable {
  
  global void execute(SchedulableContext SC) {
    
    List<String> depList = new List<String>();
    
    // Search through all active EDQ Address Dependency custom metadata records.
    List<EDQ_Address_Dependency__mdt> allDeps = [
      SELECT DeveloperName
      FROM EDQ_Address_Dependency__mdt
      WHERE Active__c = true
    ];
    
    // Loads a list of all the entries to perform batch processes against.
    for(EDQ_Address_Dependency__mdt ead : allDeps) {
      depList.add(ead.DeveloperName); 
    }
    
    // Initialise the first batch with a list of all the entries
    BatchEDQAddressesDependency b = new BatchEDQAddressesDependency();
    b.edqAddrDepsList = depList;
    
    Database.executeBatch(b, ScopeSizeUtility.getScopeSizeForClass('BatchEDQAddressesDependency'));
    
  }
    
}