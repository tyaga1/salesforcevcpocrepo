/**=====================================================================
 * Experian
 * Name: ScheduleConfInfoSealOCRExistUpdate 
 * Description: Schedule BatchConfInfoSealOCRExistUpdate to run daily
 * Created Date: 06th Dec 2016
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
 ============================================================================*/
global class ScheduleConfInfoSealOCRExistUpdate implements Schedulable {
  global void execute(SchedulableContext SC) {
    BatchConfInfoSealOCRExistUpdate batch = new BatchConfInfoSealOCRExistUpdate(); 
    Database.executeBatch(batch, 200); 
  }
}
      // Code to run in dev console 
     //  System.schedule('ConfInfoSealOCRExistUpdate', '0 0 18 * * ?', new ScheduleConfInfoSealOCRExistUpdate());