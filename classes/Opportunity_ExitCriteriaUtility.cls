/**===========================================================================================================
 * Appirio, Inc
 * Name: Opportunity_ExitCriteriaUtility.cls
 * Description: T:235320: Exit criteria for Opportunities
 * Created Date: 02/07/2014
 * Created By: Naresh Kr Ojha (Appirio)
 *
 * Date Modified      Modified By                  Description of the update
 * Feb 19th, 2014     Nathalie Le Guay (Appirio)   Created oppStagesAllowedMap & updated all
 *                                                 criteria check methods to simplify logic &
 *                                                 use CurrencyUtility class
 * Feb 22th, 2014     Naresh kr Ojha (Appirio)     T-251145: updated to remove ref Has_Completed_Task__c
 * Feb 25th, 2014     Nathalie Le Guay (Appirio)  Renamed checkPreviousStageNotMet() to isPassingRequiredExitCriteria()
 *                                                 Rewrote isPassingRequiredExitCriteria() + added oppStageNameToNumberMap
 *                                                 and oppStageNumberToNameMap
 *                                                 Removed duplicate calls to check exit criteria
 *                                                 Created isStage6ExitCriteriaMet()
 * Feb 26th, 2014     Nathalie Le Guay (Appirio)   Removing references to Selection_confirmed__c and Signed_Contract__c
 *                                                 and Task_Quote_Delivered__c
 * Feb 27th, 2014     Naresh Kr Ojha (Appirio)     Updated hasRequiredTask() method to use queried opportunities, as it can have Tasks on oppty
 *                                                 after querying them.
 * Feb 28th, 2014     Naresh kr Ojha (Appirio)     Fixed issue: I-103440
 * March 3rd, 2014    Nathalie Le Guay             Updated opp query
 * Mar 04th, 2014     Arpita Bose(Appirio)         T-243282: Added Constants in place of String
 * Mar 27th, 2014     Nathalie Le Guay             Adding debug statements
 * Mar 31st, 2014     Arpita Bose(Appirio)         T-267875: Removed reference to field Total_Annual_Amount__c and the validations for
 *                                                 Stage_4_to_5_Opportunity_Approval, Stage_4_to_5_AmountOrTAA_Over_Approval
 *                                                 and Stage_5_to_6_Opportunity_Approval
 * April 3rd, 2014    Mohit Parnami(Appirio)       T-266502: updated isMeetingExitCriteria method to use the constructor of currencyUtility
                                                   having parameter as set of ISO codes
 * Apr 15th, 2014     Arpita Bose                  T-271695: Removed reference to Below_Review_Thresholds__c field
 * Apr 15th, 2014     Nathalie Le Guay             T-265496: Removed Stage_4_Exit_Criteria_Met  and Stage_5_Exit_Criteria_Met logic
 * Apr 15th, 2014     Nathalie Le Guay             T-265496: Removed Stage_4_Exit_Criteria_Met in isStage4ExitCriteriaMet()
 *                                                 and Stage_5_Exit_Criteria_Met in isStage5ExitCriteriaMet()
 * Apr 16th, 2014     Arpita Bose                  T-271695: Renamed fields Has_Stage_4_Approval__c to Has_Stage_3_Approval__c
 *                                                  and Has_Stage_5_Approval__c to Has_Senior_Approval__c
 * May 21st, 2014     Nathalie Le Guay             Updated getOpportunitiesExitCriteriaNotMet() skipping stage section so that it doesnt apply in testing
 * Jul 28th, 2014     Nathalie Le Guay             S-252919 - Added Outcomes__c to the Opp query (getOpportunitiesExitCriteriaNotMet)
 * Sep 29th, 2014     Naresh Kr Ojha               T-323155: Free trial opportunity wont go through Oppty Exit criteria Validations, added check
 * Nov 11th, 2014     Noopur                       Added the conditions to check of the current user is EDQ and UK&I to bypass some of the criteria.
 * Jun 26th, 2015     Arpita Bose (Appirio)        T-413194: Added Custom Labels
 * Aug 13th, 2015     Naresh Kr Ojha               I-174978: Updated to fix issue Too Many SOQL.
 * Feb 25th, 2016     Paul Kissick                 Case 01872263: Fixed query to return archived tasks
 * Apr 5th, 2016      Paul Kissick                 Case 01028611: Adding checks to stage 4
 * Jul 20th, 2016     Manoj Gopu                   CRM2:W-005438 Commented out the code related to Opp Stages
 * Jul 28th, 2016     Cristian Torres              CRM2:W-005403: If a contact Role is no Longer with Company the Opportunity cannot be moved to Executed
 * Aug 1st, 2016      Paul Kissick                 CRM2: Fixed code formatting, which was somehow completely lost!
 * Aug 5th, 2016      Manoj Gopu                   CRM2:W-005438 Moved the Competetior check from Qualify to Propose And made it required
 * Aug 9th, 2016      Paul Kissick                 CRM2:W-005495: Removing fields no longer used, and adding new BU check on stage 4
 * Aug 9th, 2016      Diego Olarte                 CRM2:W-005496: Adding new error on check on stage 6 for making sure a Primary_Quote_has_been_Approved
 * Aug 10th, 2016     Paul Kissick                 CRM2:W-005495: Adding missing check of amount on exit stage 6 for UK&I
 * Aug 19th, 2016     Paul Kissick                 CRM2:W-005403: Adding label for Contacts not at company notification, and added constant
 * Sep 07th, 2016     Manoj Gopu                   CRM2:W-005438:  Updated the missing criteria error message for Opp Validation.
 * Sep 07th, 2016     Manoj Gopu                   CRM2:W-005421: Added a new error message for Transacational Order
 * Sep 14th, 2016     Diego Olarte                 CRM2:W-005496: Added new field CPQ_User_to_Validate__c to validate users based on the CPQ User Type for the new error on Quote Approval
 * Sep 28th, 2016     Manoj gopu                   CRM2:w-005949: Added Constants to Free trial and transactional sale Record types. 
 * Oct 03rd, 2016     Manoj Gopu                   CRM2:W-005938: Replaced the stage value ith stage label wherever error message is displayed.
 * Oct 14th, 2016     Paul Kissick                 Corrected code for getting the picklist labels, also named properly.
 * Nov 16th, 2016     Diego Olarte                 Case 02144295: Added additional evaluation to check for opp.Quote_Count__c == null, add a rule to include CPQ_User_to_Validate2__c and CPQ_User_to_Validate3__c to add additional CPQ Users to validate for a non BIS CPQ User and because only quote is required
 * Dec 05th, 2016     Manoj Gopu                   Case 02215771: Added a exception for product count and Competitor count for Partner Community Record type 
 ============================================================================================================*/
public with sharing class Opportunity_ExitCriteriaUtility {

  private static Boolean isCriteriaMet;
  public static String errorMessage;
  
  public static Map<String, String> stagePicklistLabels {get{
    if (stagePicklistLabels == null) {
      stagePicklistLabels = new Map<String, String>();
      for (Schema.PicklistEntry a : Opportunity.StageName.getDescribe().getPickListValues()) {
        stagePicklistLabels.put(a.getValue(),a.getLabel());
      }
    }
    return stagePicklistLabels;
  }set;} 
  
  public static Map<String, Id> oppRecordTypeMap {get{
    if (oppRecordTypeMap == null) {
      oppRecordTypeMap = new Map<String, Id>();
      for (RecordType rt : [SELECT Id, Name, DeveloperName FROM RecordType WHERE sObjectType = 'Opportunity']) {
        oppRecordTypeMap.put(rt.Name, rt.Id);  
      }
    }
    return oppRecordTypeMap;
  }set;}
  
  public static User currentUser = [
    SELECT Id, Region__c 
    FROM User 
    WHERE Id = :Userinfo.getUserId()
  ];

  public static Boolean bypassTheCriteria;
    //CT removing the repeating message
  public static Boolean didCriteria6AlreadyRun = false;

  //Queried opportunities
  public static Map<String, Opportunity> opportunityMap;

  private static Map<String, Set<String>> oppStagesAllowedMap = new Map<String, Set<String>>{
    Constants.OPPTY_STAGE_3 => new Set<String>{Constants.OPPTY_STAGE_4},
    Constants.OPPTY_STAGE_4 => new Set<String>{Constants.OPPTY_STAGE_5, Constants.OPPTY_STAGE_3},
    Constants.OPPTY_STAGE_5 => new Set<String>{Constants.OPPTY_STAGE_6, Constants.OPPTY_STAGE_4, Constants.OPPTY_STAGE_3},
    Constants.OPPTY_STAGE_6 => new Set<String>{Constants.OPPTY_STAGE_7, Constants.OPPTY_STAGE_5, Constants.OPPTY_STAGE_4, Constants.OPPTY_STAGE_3}
  };

  public static Map<String, Integer> oppStageNameToNumberMap = new Map<String, Integer>{
    Constants.OPPTY_STAGE_3 => 3,
    Constants.OPPTY_STAGE_4 => 4,
    Constants.OPPTY_STAGE_5 => 5,
    Constants.OPPTY_STAGE_6 => 6,
    Constants.OPPTY_STAGE_7 => 7
  };

  public static Map<Integer, String> oppStageNumberToNameMap = new Map<Integer, String>{
    3 => Constants.OPPTY_STAGE_3,
    4 => Constants.OPPTY_STAGE_4,
    5 => Constants.OPPTY_STAGE_5,
    6 => Constants.OPPTY_STAGE_6,
    7 => Constants.OPPTY_STAGE_7
  };

  //============================================================================================
  // T-251412: Exit Criteria: Display error message if Exit Criteria of previous Stages not met
  //============================================================================================
  private static Boolean isPassingRequiredExitCriteria (Opportunity opp, String stageName, String oldStage) {

    Boolean meetingCriteria = true;

    Integer startApplyingCriteriaStage = 3;

    system.debug('\n[Opportunity_ExitCriteriaUtility : isPassingRequiredExitCritria] : opp.Starting_Stage__c' + opp.Starting_Stage__c + ' oldStage' + oldStage);

    Integer startingStageNumber = oppStageNameToNumberMap.get(opp.Starting_Stage__c);
    Integer stageNumber = oppStageNameToNumberMap.get(stageName);
    Integer oldStageNum = oppStageNameToNumberMap.get(oldStage);

    system.debug('\n[Opportunity_ExitCriteriaUtility : isPassingRequiredExitCritria] : startingStageNumber: '+ startingStageNumber + ' and opps startingstage: ' + opp.Starting_Stage__c);

    if (String.isNotEmpty(opp.Starting_Stage__c) && startingStageNumber != null) {
      startApplyingCriteriaStage = startingStageNumber;
    }

    do {
      system.debug('\n[Opportunity_ExitCriteriaUtility : isPassingRequiredExitCriteria] : \nstartApplyingCriteriaStage:' + startApplyingCriteriaStage + ' \nstageNumber:'+ stageNumber);
      system.debug('\n[Opportunity_ExitCriteriaUtility : isPassingRequiredExitCriteria] :startApplyingCriteriaStage <= stageNumber?' + (startApplyingCriteriaStage <= stageNumber) + '\nstageNumber > oldStageNum?'+(stageNumber > oldStageNum));

      if (startApplyingCriteriaStage <= stageNumber && stageNumber > oldStageNum) { // || (startApplyingCriteriaStage < oldStageNum && stageNumber < oldStageNum)

        system.debug('\nIs tempStage >= oldStageNum ? tempStage='+startApplyingCriteriaStage + ' and oldStageNum='+oldStageNum);

        meetingCriteria = isMeetingExitCriteria(opp, oppStageNumberToNameMap.get(startApplyingCriteriaStage+1), oppStageNumberToNameMap.get(startApplyingCriteriaStage));

        system.debug(meetingCriteria+'\n[Opportunity_ExitCriteriaUtility: isPassingRequiredExitCritria111] : from: '+ oppStageNumberToNameMap.get(startApplyingCriteriaStage+1)+ ' to: '+ oppStageNumberToNameMap.get(startApplyingCriteriaStage)+ '\n:::'+ meetingCriteria);

      }
      // PK: Adding spacing between the errors!
      if (!meetingCriteria) {
        errorMessage = Label.OECS_MSG_CRITERIA_NOT_MET + ' ' + oldStage + ' ' + Label.OECS_MSG_to + ' ' + stagePicklistLabels.get(stageName) + '. '+ errorMessage; //T-413194 : Added Custom Labels
        return false;
      }
      startApplyingCriteriaStage++;

    } while (startApplyingCriteriaStage < stageNumber);

    return meetingCriteria;
  }


  //============================================================================================
  // Returns list of opportunities which do not match exit criteria
  //============================================================================================
  public static Boolean getOpportunitiesExitCriteriaNotMet (Map<Id, Opportunity> newOpps, Map<Id, Opportunity> oldOpps) {

    if (!OpportunityTriggerHandler.hasCheckedExitCriteria) {
      OpportunityTriggerHandler.hasCheckedExitCriteria = true;
    }

    List<Opportunity> notMetOpportunitiesList = new List<Opportunity>();
    opportunityMap = new Map<String, Opportunity>();
    
    
    Global_Settings__c globalSettings = Global_Settings__c.getValues(Constants.GLOBAL_SETTING);

    // PK Case 01872263 - Altered query below
    for (Opportunity oppty : [SELECT ID, RecordTypeId, Competitor_Count__c, Turn_Off_Contact_Role_Criteria_Check__c, StageName,Starting_Stage__c,
                                     CurrencyIsoCode, Budget__c, Has_Stage_3_Approval__c, Has_Senior_Approval__c,Opportunity_Products_Count__c,
                                     Owner.Region__c, Stage_3_Approver__c, Senior_Approver__c,  
                                     /*Is_There_Commercial_Risk__c, Is_There_Delivery_Risk__c,
                                     Is_There_Financial_Risk__c, Is_There_Legal_Risk__c,
                                     Has_There_Been_Significant_Change__c,*/ Type, OwnerId, Owner_s_Business_Unit__c,
                                     Risk_Tool_Output__c, Risk_Tool_Output_Code__c,
                                     //Below_Review_Thresholds__c, T-271695: Removed reference to Below_Review_Thresholds__c
                                     Amount, CloseDate, Tech_Support_Maintenance_Tiers__c,
                                     Primary_Quote_has_been_Approved__c, Quote_Primary__c, Quote_Count__c,CPQ_User_to_Validate__c,CPQ_User_to_Validate2__c,CPQ_User_to_Validate3__c,// CRM2:W-005496
                                     (SELECT Id, Type_of_Sale__c, PricebookEntry.Product2.Global_Business_Line__c, PricebookEntry.Product2.Business_Line__c
                                      FROM OpportunityLineItems WHERE IsDeleted = false),
                                     (SELECT Id, Role, IsPrimary
                                      FROM OpportunityContactRoles
                                      WHERE Role = :Constants.DECIDER AND IsDeleted = false),
                                     (SELECT Id, Type, Status, Outcomes__c FROM Tasks
                                      WHERE Status = :Constants.STATUS_COMPLETED AND IsDeleted = false)
                                     FROM Opportunity WHERE ID IN: newOpps.keySet() ALL ROWS]) {
      opportunityMap.put(oppty.ID, oppty);
    }

    for (Opportunity oppty : newOpps.values()) {
      //As per task : T-323155 Free trial opportunity wont go through Opportunity exit criteria validations
      if (String.isNotBlank(oppty.Type) && oppty.Type.equalsIgnoreCase(Constants.OPPTY_TYPE_FREE_TRIAL)) {
        continue;
      }
      errorMessage = '';
      //Commented by Manoj to go stage from Qualify to Propose without validation rules
      /*if (oppty.StageName == globalSettings.Opp_Stage_4_Name__c && oldOpps.get(oppty.Id).StageName == globalSettings.Opp_Stage_3_Name__c) {
        //If this was an originally loaded Opportunity then we don't require people to fill in the contact role
        if (opportunityMap.get(oppty.ID).OpportunityContactRoles.size() < 1 && !opportunityMap.get(oppty.ID).Turn_Off_Contact_Role_Criteria_Check__c) {
          oppty.StageName.addError(Label.Opp_Stage_3_Stage_Exit_Failure);
          continue;
        }
      }*/     
      
      //Added by manoj to check the opportunity product count 
      if ((oppty.StageName==Constants.OPPTY_STAGE_5 || oppty.StageName==Constants.OPPTY_STAGE_6 || oppty.StageName==Constants.OPPTY_STAGE_7) 
          && oldOpps.get(oppty.Id).StageName!= oppty.StageName && oppty.Opportunity_Products_Count__c==0 && 
          oppty.RecordTypeId != oppRecordTypeMap.get(Constants.RECORDTYPE_Partner_Community)) { //Added by Mj to check Opty Product Count if record type is not Partner Community    
        //Get the RecordTypeId from map to compare Transactional sale
        if (oppty.RecordTypeId != oppRecordTypeMap.get(Constants.RECORDTYPE_Transactional_Sale)) {          
          oppty.StageName.addError(Label.PROPOSE_CRITERIA_NOT_MET + ' ' + stagePicklistLabels.get(oppty.StageName) + '. ');
        }
        else{
          oppty.StageName.addError(Label.TRANS_SALE_CRITERIA_NOT_MET);
        }       
        return false;
      }
         
      if ((oppty.StageName==Constants.OPPTY_STAGE_5 || oppty.StageName==Constants.OPPTY_STAGE_6 || oppty.StageName==Constants.OPPTY_STAGE_7) && 
          (oppty.Competitor_Count__c == null || Integer.valueOf(oppty.Competitor_Count__c) == 0) && 
           oppty.Type != Constants.OPPTY_TYPE_RENEWAL && 
           oppty.RecordTypeId != oppRecordTypeMap.get(Constants.RECORDTYPE_FREE_TRIAL) && 
           oppty.RecordTypeId != oppRecordTypeMap.get(Constants.RECORDTYPE_Transactional_Sale) && 
           oppty.RecordTypeId != oppRecordTypeMap.get(Constants.RECORDTYPE_Partner_Community)) { //Added by Mj to check Opty Competitor Count if record type is not Partner Community
        oppty.StageName.addError(Label.PROPOSE_CRITERIA_NOT_MET + ' ' + stagePicklistLabels.get(oppty.StageName) + '. ');
        return false;
      }

      // Check whether opportunity skipping stage
      if (Test.isRunningTest() &&
          Constants.OPPTY_CHECK_SKIPPING &&
          !oppty.isClosed &&
          !oppty.StageName.equalsIgnoreCase(Constants.OPPTY_STAGE_NO_DECISION) &&
          isSkippingStages(oppty.StageName, oldOpps.get(oppty.ID).StageName)) {
        oppty.addError (Label.OECS_MSG_SKIPSTAGE);
        notMetOpportunitiesList.add(oppty);
        continue;
      }

      system.debug('\nOldStageName: '+oldOpps.get(oppty.ID).StageName + '\nNew StageNAME: '+ oppty.StageName);

      system.debug('\n===isPassingRequiredExitCriteria(opportunityMap.get(oppty.ID), oppty.StageName, oldOpps.get(oppty.ID).StageName)=' + isPassingRequiredExitCriteria(opportunityMap.get(oppty.ID), oppty.StageName, oldOpps.get(oppty.ID).StageName));
     
      //Saving the current state of the error message so that it doesn't get modified after this and we don't get duplicated 
      //error messages since the variable is vissible to the entire class
      String auxStringErrorMessage = errorMessage;
      
        // passing both opp and oppty's StageName because the opp from the opportunityMap is retrieved from DB, not from trigger
      if (oppty.StageName != oldOpps.get(oppty.ID).StageName &&
          !isPassingRequiredExitCriteria(opportunityMap.get(oppty.ID), oppty.StageName, oldOpps.get(oppty.ID).StageName)) {
        //Moving to new stage
        //CT oppty.StageName.addError('HELLO ' + errorMessage + ' GREETINGS ');
        oppty.StageName.addError(auxStringErrorMessage);
      }
    }
    return (notMetOpportunitiesList.size() > 0 ? false : true);
  }

  //============================================================================================
  // Check two stages weather the stage is skpping
  //============================================================================================
  private static Boolean isSkippingStages (String newStage, String oldStage) {
    Boolean isSkipping = false;
    if (oppStagesAllowedMap.get(oldStage) != null &&
        newStage != oldStage &&
        !oppStagesAllowedMap.get(oldStage).contains(newStage)) {
      isSkipping = true;
    }
    return isSkipping;
  }

  //============================================================================================
  //check opportunity weather that is meeting exit criteria
  //============================================================================================
  public static Boolean isMeetingExitCriteria (Opportunity opp, String newStage, String oldStage) {
    Boolean isOpptyMeetingExitCriteria = true;

    Decimal adjustedRate = 0.00;
    Decimal currencyRate = 1.00;

    Set<String> setOppCurrencyISOCode = new Set<String>();
    setOppCurrencyISOCode.add(opp.CurrencyIsoCode);

    CurrencyUtility currencyUtil = new CurrencyUtility(setOppCurrencyISOCode);
    currencyRate = currencyUtil.convertCurrency(opp.CurrencyIsoCode, 1, Constants.CURRENCY_GBP, opp.CloseDate);
    adjustedRate = 1 - currencyRate;

    // Noopur - added to remove to many SOQL queries
    String groupName = (!String.isBlank(OpportunityTriggerHandler.groupname) ? OpportunityTriggerHandler.groupname : BusinessUnitUtility.getBusinessUnit(opp.OwnerId));

    if (String.isNotBlank(groupName) && groupName.equalsIgnoreCase(Constants.EDQ) && opp.Owner.Region__c == Constants.REGION_UKI) {
      bypassTheCriteria = true;
    }
    else {
      bypassTheCriteria = false;
    }

    system.debug('\n[Opportunity_ExitCriteriaUtility : isMeetingExitCriteria]' + bypassTheCriteria);

    //When opportunity moving Stage 3 to Stage 4.
    if (newStage == Constants.OPPTY_STAGE_4 && oldStage == Constants.OPPTY_STAGE_3) {
      isOpptyMeetingExitCriteria = isStage3ExitCriteriaMet(opp, newStage, oldStage, adjustedRate);
    } //When opportunity moving Stage 4 to Stage 5.
    else if (newStage == Constants.OPPTY_STAGE_5 && oldStage == Constants.OPPTY_STAGE_4) {
      isOpptyMeetingExitCriteria = isStage4ExitCriteriaMet(opp, newStage, oldStage, adjustedRate);
    } //When opportunity moving Stage 5 to Stage 6.
    else if (newStage == Constants.OPPTY_STAGE_6 && oldStage == Constants.OPPTY_STAGE_5) {
      isOpptyMeetingExitCriteria = isStage5ExitCriteriaMet(opp, newStage, oldStage, adjustedRate);
    }
    else if (newStage == Constants.OPPTY_STAGE_7 && oldStage == Constants.OPPTY_STAGE_6) {
      //system.debug(opp.Has_There_Been_Significant_Change__c+'[**8opp****]'+opp);
      isOpptyMeetingExitCriteria = isStage6ExitCriteriaMet(opp, newStage, oldStage);
    }
    system.debug('[errorMessageerrorMessageerrorMessage----]'+errorMessage);
    return isOpptyMeetingExitCriteria;
  }

  //============================================================================================
  // Stage 3 exit criteria met ?
  //============================================================================================
  private static Boolean isStage3ExitCriteriaMet(Opportunity opp, String newStage, String oldStage, Decimal adjustedRate) {
    isCriteriaMet = true;

    system.debug('\n[Opportunity_ExitCriteriaUtility: isStage3ExitCriteriaMet] new Stage:' +newStage + ' oldStage: '+oldStage + '; adjustedRate: '+ adjustedRate);
    //Commented by Manoj to go stage from Qualify to Propose without validation rules
    /*
    if (opp != null && (
         (opp.Tasks != null && opp.Tasks.size() < 1) ||
         opp.Budget__c == '' ||
         (opp.Competitor_Count__c == null || Integer.valueOf(opp.Competitor_Count__c) == 0) ||
         (opp.OpportunityContactRoles != null && opp.OpportunityContactRoles.size() < 1 && !opp.Turn_Off_Contact_Role_Criteria_Check__c))) {
      isCriteriaMet = false;
      errorMessage = Label.OECS_MSG_STAGE3_EXIT;
    }
    */
    system.debug('[errorMessageerrorMessageerrorMessage3333]'+errorMessage);
    system.debug('[---debug1.0--]'+opp.Stage_3_Approver__c);
    system.debug('[---debug1.1--]'+opp.Has_Stage_3_Approval__c);
    system.debug('[---debug1.2--]'+opp.StageName);
    system.debug('[---debug1.3--]'+opp);
    system.debug('[---debug1.4--]'+bypassTheCriteria);

    if (opp.Owner.Region__c == Constants.REGION_UKI && !bypassTheCriteria && opp.Amount >= 500000 && oppStageNameToNumberMap.get(newStage) == 4) {
      if (opp.Stage_3_Approver__c == null) {
        errorMessage += ' ' + Label.OECS_MSG_POPULATE_STAGE_3_APPROVER;
        isCriteriaMet = false;
      }
      else if (opp.Has_Stage_3_Approval__c == false) {
        errorMessage += ' ' + Label.OECS_MSG_STAGE_3_TO_4_AMOUNTORTAA_OVER_APPROVAL;
        isCriteriaMet = false;
      }
    }

    system.debug('\n[Opportunity_ExitCriteriaUtility: isStage3ExitCriteriaMet] stage 3 met:' +isCriteriaMet);

    return isCriteriaMet;
  }

  //============================================================================================
  // Stage 4 exit criteria met ?
  //============================================================================================
  private static Boolean isStage4ExitCriteriaMet(Opportunity opp, String newStage, String oldStage, Decimal adjustedRate) {
    isCriteriaMet = true;

    //Boolean hasQuoteDelivered = hasRequiredTask(opp, Constants.ACTIVITY_TYPE_QUOTE_DELIVERED);

    //if (!hasQuoteDelivered || opp.Opportunity_Products_Count__c == 0) {
    //Commented by Manoj to go stage from Propose to Commit without validation rules except products selected
    /*
    if (opp.Opportunity_Products_Count__c == 0) {
      errorMessage += '\n'+Label.OECS_MSG_STAGE_4_EXIT_CRITERIA;
      isCriteriaMet = false;
    }
    */
    // CRM2:W-005438 - Added by Manoj
    /*Id freeTrialRecId = Opportunity.sObjectType.getDescribe().getRecordTypeInfosByName().get(Label.OPPTY_TYPE_FREE_TRIAL).getRecordTypeId();
    Id transaleRecId = Opportunity.sObjectType.getDescribe().getRecordTypeInfosByName().get(Label.Opty_Transactional_Sale).getRecordTypeId();
    if ((opp.Competitor_Count__c == null || Integer.valueOf(opp.Competitor_Count__c) == 0) && 
        opp.Type != Constants.OPPTY_TYPE_RENEWAL && 
        opp.RecordTypeId != freeTrialRecId && 
        opp.RecordTypeId != transaleRecId) {
      errorMessage += '\n'+Label.OECS_MSG_STAGE_4_EXIT_CRITERIA;
      isCriteriaMet = false;
    }*/

    system.debug('\nOpp Region: '+ opp.Owner.Region__c + ' bypass: ' + bypassTheCriteria);

    Boolean foundTechRequirement = hasTechSuppFieldRequired(opp);

    if (foundTechRequirement && String.isBlank(opp.Tech_Support_Maintenance_Tiers__c)) {
      isCriteriaMet = false;
      errorMessage += '\n' + Label.OECS_MSG_POPULATE_Tech_Support_Main_Tiers;
    }

    if (opp.Owner.Region__c == Constants.REGION_UKI && !bypassTheCriteria) {
      /*
      // Removing since fields no longer required.
      if (opp.Is_There_Commercial_Risk__c == null || opp.Is_There_Delivery_Risk__c == null ||
          opp.Is_There_Financial_Risk__c == null|| opp.Is_There_Legal_Risk__c == null) {
        errorMessage += '\n'+Label.OECS_MSG_POPULATE_RISK_FIELDS;
        isCriteriaMet = false;
      }
      */
      // CRM2:W-005495 - New check for GTM
      if (opp.Amount >= 250000 && String.isNotBlank(opp.Owner_s_Business_Unit__c) && 
          (opp.Owner_s_Business_Unit__c.startsWithIgnoreCase('UK&I GTM ') || 
           opp.Owner_s_Business_Unit__c.startsWithIgnoreCase('UK&I CS ') || 
           opp.Owner_s_Business_Unit__c.startsWithIgnoreCase('UK&I DA '))) {
        if (opp.Risk_Tool_Output__c == null || String.isBlank(opp.Risk_Tool_Output_Code__c)) {
          errorMessage = Label.OECS_MSG_RISK_TOOL_OUTPUT_MISSING;
          isCriteriaMet = false;
        }
      }
      if (/*(opp.Has_There_Been_Significant_Change__c != null && opp.Has_There_Been_Significant_Change__c.equalsIgnoreCase('Yes')) || */ opp.Amount >= 500000) {
        if (opp.Senior_Approver__c == null) {
          errorMessage = Label.OECS_MSG_POPULATE_SENIOR_APPROVER;
          isCriteriaMet = false;
        }
        else if (opp.Has_Senior_Approval__c == false) {
          errorMessage += '\n' + Label.OECS_MSG_STAGE_4_TO_5_AMOUNTORTAA_OVER_APPROVAL;
          isCriteriaMet = false;
        }
      }
    }
    system.debug('\n[Opportunity_ExitCriteriaUtility: isStage4ExitCriteriaMet] stage 4 met:' +isCriteriaMet);
    return isCriteriaMet;
  }

  //============================================================================================
  // Stage 5 exit criteria met ?
  //============================================================================================
  private static Boolean isStage5ExitCriteriaMet(Opportunity opp, String newStage, String oldStage, Decimal adjustedRate) {
    isCriteriaMet = true;
    //Commented by Manoj to go stage from Commit to Contract without validation rules
    /*
    Boolean hasSelectionConfirmed = hasRequiredTask(opp, Constants.ACTIVITY_TYPE_SELECTION_CONFIRMED);

    if (!hasSelectionConfirmed) {
      errorMessage += '\n'+Label.OECS_MSG_STAGE_5_EXIT_CRITERIA_MET;
      isCriteriaMet = false;
    }
    */
    system.debug('[errorMessageerrorMessageerrorMessage5555]'+errorMessage);
    if (opp.Owner.Region__c == Constants.REGION_UKI && !bypassTheCriteria) {
      /*
      // PK Removing since these fields are no longer required.
      if (opp.Is_There_Commercial_Risk__c == null || opp.Is_There_Delivery_Risk__c == null || opp.Is_There_Financial_Risk__c == null || opp.Is_There_Legal_Risk__c == null) {
        errorMessage += Label.OECS_MSG_POPULATE_RISK_FIELDS;
        isCriteriaMet = false;
      }
      
      if (opp.Has_There_Been_Significant_Change__c == null) {
        isCriteriaMet = false;
      }
      */      
      if (opp.Amount >= 500000 /*|| (opp.Has_There_Been_Significant_Change__c != null && opp.Has_There_Been_Significant_Change__c.equalsIgnoreCase('Yes'))*/) {
        if (opp.Senior_Approver__c == null) {
          errorMessage += Label.OECS_MSG_POPULATE_SENIOR_APPROVER;
          isCriteriaMet = false;
        }
        else if (opp.Has_Senior_Approval__c == false) {
          errorMessage += Label.OECS_MSG_STAGE_5_TO_6_OPPTY_APPROVAL;
          isCriteriaMet = false;
        }
      }
    }

    system.debug('\n[Opportunity_ExitCriteriaUtility: isStage5ExitCriteriaMet] stage 5 met:' +isCriteriaMet);
    return isCriteriaMet;
  }

  //============================================================================================
  // Stage 6 Exit Criteria not met?
  //============================================================================================
  private static Boolean isStage6ExitCriteriaMet(Opportunity opp, String newStage, String oldStage) {
    /*
    if (didCriteria6AlreadyRun == false) {
        System.debug('TEST Cris 1');
        didCriteria6AlreadyRun = true;
    } 
    else {
        return false;
    }
    */
      
    isCriteriaMet = true;
    //Commented by Manoj to go stage from Contract to Execute without validation rules
    /*
    Boolean hasSignedContract = hasRequiredTask(opp, Constants.ACTIVITY_TYPE_SIGNED_CONTRACT);

    if (hasSignedContract == false) {
      isCriteriaMet = false;
      errorMessage += Label.OECS_MSG_STAGE_6_EXIT_CRITERIA_MET;
    }
    */
    
    //BIS Primary aprove quote requirement
    if (opp.Primary_Quote_has_been_Approved__c == false && (opp.Quote_Count__c >= 0 || opp.Quote_Count__c == null) && opp.CPQ_User_to_Validate__c == true) {
      errorMessage += Label.OECS_MSG_APPROVED_QUOTE_REQUIRED;
      isCriteriaMet = false;
    }
    
    //Non BIS Primary quote requirement
    if (((opp.Quote_Primary__c == false && opp.Quote_Count__c > 0) || (opp.Quote_Count__c == 0 || opp.Quote_Count__c == null)) && (opp.CPQ_User_to_Validate2__c == true || opp.CPQ_User_to_Validate3__c == true)) {
      errorMessage += Label.OECS_MSG_PRIMARY_QUOTE_REQUIRED;
      isCriteriaMet = false;
    }
    
    system.debug('[errorMessageerrorMessageerrorMessage666]'+errorMessage);
    system.debug('[---opp.Owner.Region__c--->]'+opp.Owner.Region__c);

    if (opp.Owner.Region__c == Constants.REGION_UKI && !bypassTheCriteria && opp.Amount >= 500000) {
      /*
      system.debug('[---opp.Has_There_Been_Significant_Change__c--->]'+opp.Has_There_Been_Significant_Change__c);
      if (String.isBlank(opp.Has_There_Been_Significant_Change__c)) {
        system.debug('[---opp.e--->]:'+opp.Has_There_Been_Significant_Change__c);
        errorMessage += Label.OECS_MSG_POPULATE_SIGNIFICANT_CHANGE;
        isCriteriaMet = false;
      }
      else if (opp.Has_There_Been_Significant_Change__c.equalsIgnoreCase('Yes')) {*/
        if (opp.Senior_Approver__c == null) {
          errorMessage += Label.OECS_MSG_POPULATE_SENIOR_APPROVER;
          isCriteriaMet = false;
        }
        else if (opp.Senior_Approver__c != null && opp.Has_Senior_Approval__c == false) {
          errorMessage += Label.OECS_MSG_STAGE_6_TO_7_OPPTY_APPROVAL;
          isCriteriaMet = false;
        }
      // }
    }
    //Check the contact roles if the status is either No longer with Company or Inactive then
    //display the warning in the VF
    List<OpportunityContactRole> contactRoles = [
      SELECT ContactId, Contact.Status__c, Contact.Name
      FROM OpportunityContactRole
      WHERE OpportunityId = :opp.Id
      AND Contact.Status__c != null
    ];
    
    List<String> contactNameList = new List<String>();

    if (!contactRoles.isEmpty()) {

      Set<Id> contactIds = new Set<Id>();

      // Check status of each contact
      for (OpportunityContactRole ocr : contactRoles) {
        if (ocr.Contact.Status__c.equalsIgnoreCase(Constants.CONTACT_STATUS_LEFT)) {
          if (!contactIds.contains(ocr.ContactId)) {
            contactNameList.add(ocr.Contact.Name);
            contactIds.add(ocr.ContactId);
          }
        }
      }
      if (!contactNameList.isEmpty()) {
        errorMessage += system.label.OPPTY_ERR_NO_LONGER_COMPANY_CONTACTS + ' ' + String.join(contactNameList, ', ');
        isCriteriaMet = false;
      }
    }
    system.debug('\n[Opportunity_ExitCriteriaUtility: isStage6ExitCriteriaMet] stage 6 met:' +isCriteriaMet);
    return isCriteriaMet;
  }

  //============================================================================================
  // Utility method to check wether an Opp has a required task or not by specifying the task Type
  //============================================================================================
  public static Boolean hasRequiredTask(Opportunity opp, String taskType) {
    if (opp.Tasks.size() > 0) {
      for (Task t: opp.Tasks) {
        if (String.isNotBlank(t.Outcomes__c) && t.Outcomes__c.containsIgnoreCase(taskType) && String.isNotEmpty(t.Status) && t.Status.equalsIgnoreCase(Constants.STATUS_COMPLETED)) {
          return true;
        }
      }
    }
    return false;
  }

  public static Boolean hasTechSuppFieldRequired(Opportunity opp) {
    Boolean foundTechRequirement = false;
    try {
      if (Constants.REGION_EMEA.equals(opp.Owner.Region__c) && opp.Type.startsWith('New')) {
        for (OpportunityLineItem oli : opp.OpportunityLineItems) {
          if (oli.PriceBookEntry != null && oli.PriceBookEntry.Product2 != null) {
            Product2 p = oli.PriceBookEntry.Product2;
            if (Constants.USER_GBL_DECISION_ANALYTICS.equals(p.Global_Business_Line__c)) {
              if ('Software'.equals(p.Business_Line__c)) {
                if (String.isNotBlank(oli.Type_of_Sale__c) && oli.Type_of_Sale__c.contains('Maintenance')) {
                  foundTechRequirement = true;
                }
              }
            }
          }
        }
      }
    }
    catch (Exception e) {
      system.debug(e.getMessage());
    }
    return foundTechRequirement;
  }

}