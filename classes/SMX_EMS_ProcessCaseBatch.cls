/**=====================================================================
 * Appirio, Inc
 * Name: SMX_EMS_ProcessCaseBatch
 * Description: T-375155
 * Created Date: 7th May, 2015
 * Created By: Arpita Bose (Appirio)
 *
 * Date Modified            Modified By                  Description of the update
 * Jul 22nd, 2015           Paul Kissick                 Case #01040868 - Fixing nested soql query within for loop & adding yesterday to query
 * Sep 18th, 2015           Paul Kissick                 I-179463: Duplicate Management Failures
 * Jul 17th, 2016(QA)       Tyaga Pati                   CRM2-W-005364/01790367 - Function added to Create Activities when Survey Response is sent by Satmatrix
 =====================================================================*/
global class SMX_EMS_ProcessCaseBatch implements Database.Batchable<Case>, Database.AllowsCallouts{
                 
  // start method
  global Iterable<Case> start(database.batchablecontext BC){
 
      return [
        SELECT Id, ContactId, Contact.AccountId, Contact.Email, (SELECT Id FROM Survey__r) 
        FROM Case 
        WHERE isClosed = TRUE 
        AND ClosedDate >= YESTERDAY 
        AND RecordType.Name = 'EMS'
        AND Status IN ('Closed Resolved','Closed Not Resolved') 
        AND ContactId != null 
        AND ContactId != ''
        AND ClosedDate <= :(system.now() - 0.1666667)
      ]; 
   
  }

  //execute method
  global void execute(Database.BatchableContext BC, List<Case> scope){
    String strSurveyName = 'EMEA MS Customer Service'; 
    String strSurveyId = 'EXPERIAN_107228';
        
    List <Feedback__c> feedbackList = new List<Feedback__c>();
    Long lgSeed = System.currentTimeMillis();
    Boolean result = false;
    
    for (Case cs : scope) {
      List<Feedback__c> lstSurvey = cs.Survey__r;
      if (lstSurvey.isEmpty()) { 
        lgSeed = lgSeed + 1;
        Feedback__c feedback = new Feedback__c();
        feedback.Case__c = cs.Id;
        feedback.Name = 'P_' + lgSeed;
        feedback.Contact__c = cs.ContactId; //ContactName 
        feedback.Status__c = 'Nominated';               
        feedback.DataCollectionName__c = strSurveyName;
        feedback.DataCollectionId__c = strSurveyId;
        feedback.Account__c = cs.Contact.AccountId; 
        feedbackList.add(feedback);        
        system.debug('ifLstSurveyEmpty--feedbackList--'+feedbackList);   
      }
    }
    insert feedbackList;
  }
  
  // finish method
  global void finish(Database.BatchableContext info){

  }//global void finish loop
  
}