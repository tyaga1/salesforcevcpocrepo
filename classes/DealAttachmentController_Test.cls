/**=====================================================================
 * Experian
 * Name: DealAttachmentController_Test
 * Description: Tests for Controller to display list of Encrypted attachments on the deal records
 * Created Date: Oct 28th, 2015
 * Created By: Paul Kissick 
 * 
 * Date Modified      Modified By                Description of the update
 * 22nd June 2016     Richard Joseph             Removing the assert and making the methods obsolete
 =====================================================================*/
@isTest
private class DealAttachmentController_Test {

  static testMethod void myUnitTest() {
    
    String textFile = 'Here is some text to test with. It should be safely encrypted and when decrypted, will be the same as this!';
    
    Account acc = Test_Utils.insertAccount();
    
    Deal__c deal = new Deal__c(Project_Name__c = 'Test Project Name');
    insert deal;
    
    Attachment att1 = new Attachment(ParentId = deal.Id, Name = 'Test Att.txt', ContentType = 'text/plain', Body = Blob.valueOf(textFile));
    insert att1;
    
    //RJ - Commented below as part of shield. Not req
    
    /*system.assertNotEquals(Blob.valueOf(textFile),[SELECT Body FROM Attachment WHERE Id = :att1.Id].Body);
    
    DealAttachmentController dac = new DealAttachmentController(new ApexPages.StandardController(deal));
    system.assertEquals(1, dac.getDealAttachments().size());
    
    dac.delAttachId = att1.Id;
    dac.delAtt();
    
    system.assertEquals(0, dac.getDealAttachments().size());*/
    
    
  }
  
}