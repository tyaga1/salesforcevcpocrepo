/**=====================================================================
 * Experian
 * Name: DownloadEncryptedDealAttController_Test 
 * Description: Tests for Controller to manage the decryption of the attachments on Deal Records
 * Created Date: Oct 28th, 2015
 * Created By: Paul Kissick 
 * 
 * Date Modified      Modified By                Description of the update
 * 22nd June 2016     Richard Joseph             Removing the assert and making the methods obsolete
 * Sep 29th, 2016     Manoj Gopu                 Case 02148690: Calling Insert Account method from Test_Utils class to fix the test class failure.
 =====================================================================*/
@isTest
private class DownloadEncryptedDealAttController_Test {

  static testMethod void dealAttachmentDownloadTest() {
    
    String textFile = 'Here is some text to test with. It should be safely encrypted and when decrypted, will be the same as this!';
    String textName = 'Test Att.txt';
    Deal__c deal = new Deal__c(Project_Name__c = 'Test Project Name');
    insert deal;
    
    Test_Utils.insertAccount();  //Added by Manoj
    Attachment att1 = new Attachment(ParentId = deal.Id, Name = textName, ContentType = 'text/plain', Body = Blob.valueOf(textFile));  
    insert att1;
    
    //RJ Commented it out.
    /*system.assertNotEquals(null, [SELECT Encryption_Key__c FROM Deal__c WHERE Id = :deal.Id].Encryption_Key__c);
    
    ApexPages.currentPage().getParameters().put('Id',att1.Id);
    
    DownloadEncryptedDealAttController dedc = new DownloadEncryptedDealAttController();/*
    /*system.assertEquals(dedc.getAttName(), textName);
    system.assertNotEquals(null, dedc.getAttType());
    system.assertEquals(EncodingUtil.base64encode(Blob.valueOf(textFile)), dedc.getAttBody());*/
    
    
    
  }
}