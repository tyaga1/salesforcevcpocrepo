/*=============================================================================
 * Experian
 * Name: EntitlementMilestoneResolutionTime_Test
 * Description: 
 * Created Date: 25 Apr 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/

@isTest
private class EntitlementMilestoneResolutionTime_Test {

  static testMethod void myMilestoneResolutionTest1() {
    
    MilestoneType resTime = [
      SELECT Id 
      FROM MilestoneType
      WHERE Name = 'Resolution Time'
      LIMIT 1
    ];
    List<Case> allCases = new List<Case>();
    List<Account> accs = [SELECT Id, (SELECT Id FROM Entitlements WHERE Type = 'Email Support') FROM Account];
    for (Account a : accs) {
      Case newCase = Test_Utils.insertCase(false, a.Id);
      List<Entitlement> ents = a.Entitlements;
      if (ents != null && !ents.isEmpty()) {
        Entitlement e = ents[0];
        newCase.EntitlementId = e.Id;
      }
      allCases.add(newCase);
    }
    insert allCases;
    
    Test.startTest();
    
    EntitlementMilestoneResolutionTime resTimeCalc = new EntitlementMilestoneResolutionTime();
    
    for (Case c : allCases) {
      Integer checkTime = resTimeCalc.calculateMilestoneTriggerTime(c.Id, resTime.Id);
      if (c.EntitlementId != null) {
        system.assertEquals(50, checkTime, 'The entitlement should be 50 mins.');
      }
      else {
        system.assertEquals(EntitlementMilestoneResolutionTime.defaultTime, checkTime, 'The entitlement should be '+EntitlementMilestoneResolutionTime.defaultTime+' mins.');
      }
    }
    
    Test.stopTest();
    
  }
  
  @testSetup
  private static void setupData() {
    // Initialise data for test here.
    
    User testUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    insert testUser;
    
    system.runAs(testUser) {
      
      Account acc1 = Test_Utils.createAccount();
      acc1.Name = 'Test Account T1';
      Account acc2 = Test_Utils.createAccount();
      acc2.Name = 'Test Account T2';
      Account acc3 = Test_Utils.createAccount();
      acc3.Name = 'Test Account T3';
      insert new List<Account>{acc1, acc2, acc3};
      
      Entitlement ent = new Entitlement(
        AccountId = acc1.Id,
        Escalation_Manager__c = UserInfo.getUserId(),
        Case_Resolution_Time__c = 50,
        Name = acc1.Name + ' Email',
        Type = 'Email Support', 
        StartDate = Date.today().addDays(-40),
        EndDate = Date.today().addDays(40)
      );
      
      Entitlement ent3 = new Entitlement(
        AccountId = acc3.Id,
        Escalation_Manager__c = UserInfo.getUserId(),
        Case_Resolution_Time__c = 50,
        Name = acc3.Name + ' Email',
        Type = 'Email Support', 
        StartDate = Date.today().addDays(-40),
        EndDate = Date.today().addDays(40)
      );
      
      insert new List<Entitlement>{ent, ent3};
      
    }
    
  }
  
}