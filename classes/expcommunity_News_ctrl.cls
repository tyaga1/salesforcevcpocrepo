/******************************************************************************
 * Name: expcommunity_News_ctrl.cls
 * Created Date: 1/26/2017
 * Created By: Hay Win
 * Description : Controller for expcommunity_NewsSlider_cmpt for new Exp Community
 * Change Log- 
 ****************************************************************************/
 
public with sharing class expcommunity_News_ctrl{ 
   
    public List<Employee_Community_News__c> featured {get;set;}
    public List<Employee_Community_News__c> articles {get;set;}
    public Integer articleNum {get;set;}
    public String appName {get;set;}
    public Boolean isFeatured {get;set;}
    public Boolean allActive {get;set;}    
    
    public expcommunity_News_ctrl() {
        
        //featured = [Select ID, Detail__c, Headline__c, Image_Link__c, isActive__c, isFeaturedArticle__c, Synopsis__c from Employee_Community_News__c where Content_Type__c = 'News' AND isFeaturedArticle__c = true and isActive__c = true ORDER BY CreatedDate desc Limit 1];
        //articles = [Select ID, Detail__c, Headline__c, Image_Link__c, isActive__c, isFeaturedArticle__c, Synopsis__c from Employee_Community_News__c where Content_Type__c = 'News' AND isFeaturedArticle__c != true and isActive__c = true ORDER BY CreatedDate desc Limit 4];
        featured = new List<Employee_Community_News__c>();
        articles = new List<Employee_Community_News__c>(); 
        articleNum = 5;
        isFeatured = false;
        allActive = false;
    }
    
  
    
    public boolean getfindFeature(){
    
     String appQuery = '';
        if (appName != null && appName != '') {
            appQuery = ' AND Application_Name__c =: appName ';
        } else {
            appQuery = ' AND Application_Name__c = \'EITSHome\' ';
        }
        
        String queryFeat = 'Select ID, Detail__c, Headline__c, Image_Link__c, isActive__c, isFeaturedArticle__c, Synopsis__c from Employee_Community_News__c where Content_Type__c = \'News\''+ appQuery  + ' AND isFeaturedArticle__c = true and isActive__c = true ORDER BY CreatedDate desc Limit 1';
        String queryArticle = 'Select ID, Detail__c, Headline__c, Image_Link__c, isActive__c, isFeaturedArticle__c, Synopsis__c from Employee_Community_News__c where Content_Type__c = \'News\''+ appQuery  + ' AND isFeaturedArticle__c != true and isActive__c = true ORDER BY CreatedDate desc Limit 4';
        
        featured = Database.query(queryFeat);
        articles = Database.query(queryArticle);
        articleNum = articles.size();
        
        if (featured.size() > 0) {
            isFeatured = true;
        }
        
        if (articleNum > 0) {
            allActive =true;
        }
        
        if ((featured.size() + articleNum) > 0) {
            return true;
        } else {
            return false;
        }
        //return true;
    }
}