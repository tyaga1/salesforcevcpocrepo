/**=====================================================================
 * Experian
 * Name: BatchEDQAddressesDependency_Test
 * Description: Case 01890211 - Tests for BatchEDQAddressesDependency class.
 * Created Date: 16 Mar 2016
 * Created By: Paul Kissick (Experian)
 * 
 * Date Modified      Modified By                Description of the update
 * Apr 7th, 2016      Paul Kissick               Case 01932085: Fixing Test User Email Domain
 =====================================================================*/
@isTest
private class BatchEDQAddressesDependency_Test {

  static testMethod void testBatch() {
    
    system.assertEquals(0,[SELECT COUNT() FROM Address__c WHERE EDQ_Dependency__c = true]);
    
    List<EDQ_Address_Dependency__mdt> testDep = [SELECT DeveloperName FROM EDQ_Address_Dependency__mdt WHERE Active__c = false AND DeveloperName = 'EXAMPLE_ADDRESS'];
    
    system.assertEquals(1, testDep.size(), 'Missing EXAMPLE_ADDRESS metadata.');
    
    Test.startTest();
    
    BatchEDQAddressesDependency b = new BatchEDQAddressesDependency();
    b.edqAddrDepsList = new List<String>{testDep[0].DeveloperName};
    
    Database.executeBatch(b);
    
    Test.stopTest();
    
    system.assertEquals(1,[SELECT COUNT() FROM Address__c WHERE EDQ_Dependency__c = true]);
    
  }
  
  @testSetup
  static void setupData() {
    
    // needs some orders with addresses to work.
    
    Profile p = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN];
    User testUser1 = Test_Utils.createUser(p, 'fasdasdsaf3432235236@experian.com', 'test1');
    
    system.runAs(testUser1) {
    
      Account a1 = Test_Utils.createAccount();
      a1.EDQ_Dependency_Exists__c = true;
      insert a1;
      
      Contact c1 = Test_Utils.insertContact(a1.Id);
      
      Address__c ad1 = Test_Utils.insertAddress(true);
      
      Account_Address__c accAdd = new Account_Address__c(
        Account__c = a1.Id,
        Address__c = ad1.Id,
        Address_Type__c = Constants.ADDRESS_TYPE_REGISTERED
      );
      
      insert accAdd;
      
    }
    
  }
  
}