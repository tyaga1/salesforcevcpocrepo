/*=============================================================================
 * Experian
 * Name: ManyThanksController
 * Description: Controller for VF action to handle thanking/badging many individuals
                in the one process, instead of doing so separately.
 * Created Date: 18 Aug 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * Aug 19, 2016       Paul Kissick          Added support for posting to Group Feeds and added 'with sharing'
 =============================================================================*/
public with sharing class ManyThanksController {

  // Holds the user Ids found in the custom lookups
  public Map<Integer, String> userIdMap {
    get {
      if (userIdMap == null) {
        userIdMap = new Map<Integer,String>{0 => ''};
      }
      return userIdMap;
    }
    set;
  }
  
  // Holds the user names found in the custom lookups (only for the Bootstrap version)
  public Map<Integer, String> userNameMap {
    get {
      if (userNameMap == null) {
        userNameMap = new Map<Integer,String>{0 => ''};
      }
      return userNameMap;
    }
    set;
  }
  
  // Holds the badge Id selected. Preselects the first one.
  public String badgeId {
    get{
      if (badgeId == null) {
        badgeId = allBadges.get(0).Id;
      }
      return badgeId;
    }
    set;
  }
  
  // If a group is selected, hold here.
  private String groupId; 

  public Map<Integer, String> groupIdMap {
    get {
      if (groupIdMap == null) {
        groupIdMap = new Map<Integer, String>{0 => ''};
      }
      return groupIdMap;
    }
    set;
  }
  
  public Map<Integer, String> groupNameMap {
    get {
      if (groupNameMap == null) {
        groupNameMap = new Map<Integer, String>{0 => ''};
      }
      return groupNameMap;
    }
    set;
  }

  // The thank you message we'll post onto the users profile.
  public String thanksMessage {get;set;}

  // Return all active badges, with Company Badge = true, and not a recognition program badge.
  public List<WorkBadgeDefinition> allBadges {
    get {
      if (allBadges == null) {
        allBadges = [
          SELECT Id, Name, IsCompanyWide, Description, ImageUrl, IsActive
          FROM WorkBadgeDefinition
          WHERE IsActive = true 
          AND IsCompanyWide = true
          //AND Recognition_Category__c = null
          ORDER BY Badge_Order__c ASC NULLS LAST, Name
        ];
      }
      return allBadges;
    }
    set;
  }
  
  public Integer getUserIdMapSize() {
    return userIdMap.size()-1;
  }

  public ManyThanksController() {
    try {
      // Pull out the group id, if this is opened from a group page. Preselects the group to post to.
      String parentId = ApexPages.currentPage().getParameters().get('id');
      if (((Id)parentId).getSobjectType() == CollaborationGroup.sobjectType) {
        groupIdMap.put(0, parentId);
        CollaborationGroup cGroup = [SELECT Name FROM CollaborationGroup WHERE Id = :parentId];
        groupNameMap.put(0, cGroup.Name); 
      }
    }
    catch (Exception e) {
      system.debug(e.getMessage());
    }
    
  }

  public PageReference addAnother() {
    // Increase the map, but only if the previous one is completed.
    Integer origSize = userIdMap.size();
    if (String.isNotBlank(userIdMap.get(userIdMap.size()-1))) {
      userIdMap.put(origSize, '');
      userNameMap.put(origSize, '');
    }
    return null;
  }
  
  // Holds any errors found during the saving/validation.
  public List<String> errorsFound {get {if (errorsFound == null) { errorsFound = new List<String>(); } return errorsFound;} set;}

  // Post to profiles.
  public PageReference addBadges() {
    Boolean noUsersFound = true;
        for (String s : userIdMap.values()) {
          if (String.isNotBlank(s)) {
            noUsersFound = false;
          }
        }
        if (noUsersFound) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, system.label.ManyThanks_Person_Missing));
          return null;
        }
        
        if (String.isBlank(badgeId)) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,  system.label.ManyThanks_Badge_Missing));
          return null;
        }
        
        if (String.isBlank(thanksMessage)) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, system.label.ManyThanks_Message_Missing));
          return null;
        }
        
        if (String.isNotBlank(groupIdMap.get(0))) {
          groupId = groupIdMap.get(0);
        }
              
        Map<String, Id> refToUserMap = new Map<String, Id>();
        Map<String, Id> refToGiverMap = new Map<String, Id>();
        Map<String, String> refToMessageMap = new Map<String, String>();
        Map<String, Id> refToBadgeMap = new Map<String, Id>();
        Map<String, Id> refToNomMap = new Map<String, Id>();
        Map<String, Id> refToGroupMap = new Map<String, Id>();
              
        for (String s : userIdMap.values()) {
          // Skip any empty ones.
          if (String.isBlank(s)) {
            continue;
          }
          Id recipId = (Id)s;
          refToUserMap.put(recipId, recipId);
          refToGiverMap.put(recipId, UserInfo.getUserId());
          refToMessageMap.put(recipId, thanksMessage);
          refToBadgeMap.put(recipId, badgeId);
          refToNomMap.put(recipId, null);
          refToGroupMap.put(recipId, (String.isNotBlank(groupId) ? (Id)groupId : null));
          
        }
        // Use the helper class to post to all profiles.
        Boolean res = NominationHelper.postBadgeToProfilesMany(refToUserMap, refToGiverMap, refToMessageMap, refToBadgeMap, refToNomMap, refToGroupMap);
              
        if (res == false) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Problem: '+String.join(NominationHelper.errorsFound, '\n')));
        }
        else {
          userIdMap = null;
      badgeId = '';
      thanksMessage = '';
      groupId = '';
        }
    return null;
  }

}