/**=====================================================================
 * Experian
 * Name: SerasaMainframeWSRequest
 * Description:  Class to do http callout to Serasa mainframe web service 
 * and parse results to verify if a CNPJ or CPF is valid or not
 *
 * Created Date: 13 Oct, 2015
 * Created By: Sadar Yacob
 * 
 * Date Modified      Modified By                Description of the update
 * Oct 21st, 2015     Sadar Yacob                Parse for tag'endereco' instead of 'enderecos' to loop thru one or more records for endereco
                                                 The endereco record with the identifacdor = RECEITA is the IRS Address and correct address to use
 * Oct 22nd, 2015     Paul Kissick               Refactored and replaced credentials with a custom setting.
 * Oct 22nd, 2015     Sadar Yacob                Added CNPJStatus,Address line2,line3 and Postal Code to the return class
 * Oct 29th, 2015     Paul Kissick               Replaced custom setting with new custom object
 * May 30th, 2015     Charlie Park               Add maiden name field.
 =====================================================================*/

public class SerasaMainframeWSRequest{
  
  // Testing endpoint is https://serviceshomologa.serasaexperian.com.br/EBS/Pessoav1
  
  //===========================================================================
  // Wrapper class to hold results from CNPJ lookup
  //===========================================================================
  public class cnpjResult {
    public String AccountName {get;set;} //razaoSocial
    public String AKA {get;set;}  //nomeFantasia
    public String CNPJNumber {get;set;}
    public String DateFounded {get;set;} //dataFundacao
    public String StreetNo {get;set;} //numero
    public String City {get;set;}  //nome
    public String StateCode {get;set;} //uf
    public String StreetAddr1 {get;set;}  //logradouro
    public String StreetAddr2 {get;set;}  //complemento
    public String StreetAddr3 {get;set;}  //bairro
    public String PostalCode {get;set;}
    public String StatusCode {get;set;}
    public String CNPJStatus {get;set;}
    public String ErrorMessage {get;set;}
    public String returnStr() {
      return AccountName +';'+AKA +';'+CNPJNumber +';'+CNPJStatus+';'+StreetNo +';' + StreetAddr1 +';'+ StreetAddr2  +';'+StreetAddr3 +';' +City +';' +StateCode +';' + PostalCode;
    }
  }   
  
  
  //===========================================================================
  // Wrapper class to hold results form the CPF lookup
  //===========================================================================
  public class cpfResult {
    public String ContactName {get;set;} //nome
    public String DateofBirth {get;set;} //dataNascimento
    public Date getDateofBirthDate() {
      return Date.valueOf(DateofBirth);
    }
    public String Sex {get;set;} //sexo
    public String CPFNumber {get;set;} //numero
    public String StreetNo {get;set;} //numero
    public String City {get;set;}  //nome
    public String StateCode {get;set;} //uf
    public String StreetAddr1 {get;set;}  //logradouro
    public String StreetAddr2 {get;set;}  //complemento
    public String StreetAddr3 {get;set;}  //bairro
    public String StatusCode {get;set;}
    public String ErrorMessage {get;set;}
    public String PostalCode {get;set;}
    public String MothersMaidenName {get;set;}
    public String returnStr() {
      return ContactName +';'+CPFNumber +';'+DateofBirth+';'+StreetNo +';' + StreetAddr1 +';'+ StreetAddr2 + ';'+ StreetAddr3 +';'+ City +';' +StateCode;
    }
  }
  
  public class webserviceResponse {
    public Boolean success {get;set;}
    public Boolean offline {get;set;}
    public String errorMsg {get;set;}
    public List<cpfResult> cpfResults {get;set;}
    public List<cnpjResult> cnpjResults {get;set;}
    public webserviceResponse() {
      success = false;
      offline = false;
      errorMsg = '';
      cpfResults = new List<cpfResult>();
      cnpjResults = new List<cnpjResult>();
    }
  }
  
  private static String webserviceName = 'SerasaMainframe';
  
  //===========================================================================
  // Settings holding the webservice details
  //===========================================================================
  private static Webservice_Endpoint__c smfSettings {
    get {
      if (smfSettings == null) {
	      try {
	        smfSettings = WebserviceEndpointUtil.getWebserviceDetails(webserviceName);
	      }
	      catch(Exception ex) {
	        system.debug('Cannot locate SerasaMainframe settings within Webservice_Endpoint__c settings.');
	      }
	      finally {
	        if (smfSettings == null) {
	          smfSettings = new Webservice_Endpoint__c(Name = webserviceName, Username__c = '', Password__c = '', URL__c = '', Timeout__c = 0, Active__c = false);
	        }
	      }
      }
      return smfSettings;
    }
  }
  
    /* 
    This is the Request in the SOAP CALL FOR CNPJ
      <v1:ObterDadosReceitaFederalRequest>
         <v1:documento>
            <v11:numero>62173620000180</v11:numero>
            <v11:tipo>CNPJ</v11:tipo>
         </v1:documento>
      </v1:ObterDadosReceitaFederalRequest>
  
      This is the Request in the SOAP CALL  FOR CPF  
      <v1:ObterDadosReceitaFederalRequest>
         <v1:documento>
            <v11:numero>17113441840</v11:numero>
            <v11:tipo>CPF</v11:tipo>
            </v1:documento>
      </v1:ObterDadosReceitaFederalRequest>

   */
  
  private Static String buildXmlRequest(String rType, String rValue, String userName, String password) {
    
    String requestBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://services.experian.com.br/EBS/Pessoa/v1" xmlns:v11="http://services.experian.com.br/EBO/Pessoa/v1">';
    requestBody += '<soapenv:Header>';
    requestBody += '<wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
    requestBody +=  '<wsse:UsernameToken wsu:Id="UsernameToken-4">'; 
    requestBody +=   '<wsse:Username>' + userName +'</wsse:Username>';
    requestBody +=   '<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+ password + '</wsse:Password>';
    requestBody +=   '<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">' + 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(userName + ':' + password)) +'</wsse:Nonce>';
    requestBody +=   '<wsu:Created>' +Datetime.now().formatGmt('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'') + '</wsu:Created>';
    requestBody +=  '</wsse:UsernameToken>';
    requestBody += '</wsse:Security>';
    requestBody += '</soapenv:Header>';
    requestBody += '<soapenv:Body>';
    
    requestBody += '<v1:ObterDadosReceitaFederalRequest>';
    requestBody +=  '<v1:documento>';
    requestBody +=   '<v11:numero>' + rValue+ '</v11:numero>';
    requestBody +=   '<v11:tipo>'+ rType +'</v11:tipo>';
    requestBody +=  '</v1:documento>';
    requestBody += '</v1:ObterDadosReceitaFederalRequest>';

    requestBody += '</soapenv:Body>';
    requestBody += '</soapenv:Envelope>';
    return requestBody;
  }
    
  private static HttpRequest buildHttpRequest() {
    HttpRequest hr = new HttpRequest();
    hr.setEndpoint(smfSettings.URL__c);  //'callout:SerasaMainframeWS'
    hr.setTimeout(Integer.valueOf(smfSettings.Timeout__c * 1000));
    
    hr.setHeader('Authorization','Basic ' + EncodingUtil.base64Encode(Blob.valueOf(smfSettings.Username__c + ':' + smfSettings.Password__c)));
            
    hr.setHeader('Content-Type', 'text/xml; charset=utf-8');
    hr.setHeader('Connection','keep-alive');
    
    hr.setMethod('POST');
    return hr;
  }
  
  
  public static webserviceResponse CallCNPJCheckWS(String cnpjNumVal) {
    
    webserviceResponse wsresp = new webserviceResponse();
    
    // List <cnpjResult> cnpjResultLst = new List<cnpjResult>();
    
    if (String.isBlank(smfSettings.URL__c)) {
      wsresp.success = false;
      wsresp.errorMsg = webserviceName+' not found.';
      return wsresp;
    }
    
    if (smfSettings.Active__c == false) {
      wsresp.offline = true;
      wsresp.success = false;
      wsresp.errorMsg = '';
      return wsresp;
    }

    // String responseVal;
    String sAccntName;
    String sAKA;
    String sMunicipalRegistration;
    String sStateRegistration;
    String sCNPJNumber;
    String sDateFounded;
    String sStreetAddr1;
    String sStreetAddr2;
    String sStreetAddr3;
    String sCity;
    String sStreetNo;
    String sStateCode;
    String sCNPJStatus;
    String sPostalCode;
    String wsStatusCode;
    cnpjResult rCNPJ = new cnpjResult(); //class containing CNPJ Details
        
    Http http = new Http();
    HttpRequest req = buildHttpRequest();
    try {                
      req.setBody(buildXmlRequest(
        'CNPJ',
        cnpjNumVal,
        smfSettings.Username__c,
        smfSettings.Password__c
      ));
            
      // system.debug('from class SerasaWSMainframe = :'+req.getBody()); // DO NOT SHOW AS PASSWORD IS IN THIS

      HttpResponse resp = http.send(req);
      //system.debug('Profile update Time in Millis :: = '+(System.currentTimeMillis()- startTime));
            
      List <String> cnpjInfo = new List <String>();
       
      wsStatusCode = string.valueof(resp.GetStatusCode());
      system.debug('Auth success check =' + wsStatusCode); //is it 200 (success) or 500 (failed)?
           
      if(resp != null && resp.getStatusCode() == 200) {
        system.debug('update response = '+resp.getBody());
        // responseVal = resp.toString();
        // system.debug('update response2 = '+responseVal );  
        
        //PARSE USING DOM
        XmlDom doc = new XmlDom(resp.getBody());
        XmlDom.Element[] recs = doc.getElementsByTagName('ObterDadosReceitaFederalResponse'); // pessoaJuridica'); //pessoa
        if (recs != null) {
          for (XMlDom.Element rec : recs) {
            sAccntName = rec.getValue('razaoSocial');
            sAKA = rec.getValue('nomeFantasia');
            sCNPJNumber = rec.getValue('numero');
            sDateFounded = rec.getValue('dataFundacao');
                        
            rCNPJ.StatusCode = wsStatusCode;
            rCNPJ.AccountName = sAccntName;
            rCNPJ.AKA = sAKA;
            rCNPJ.CNPJNumber = sCNPJNumber;
            rCNPJ.DateFounded = sDateFounded;
                        
            // responseVal = wsStatusCode +';'+ sAccntName +';'+sAKA+';'+sCNPJNumber;
                        
            //3nd element under enderecos
            for (XmlDom.Element addr : rec.getElementsByTagName('endereco')) {
              //enderecos
              if (addr.getValue('identificador') == 'RECEITA') { 
                //this is the Govt database address for this Account
                sStreetNo = addr.getValue('numero');
                sStreetAddr1 = addr.getValue('logradouro');
                sStreetAddr2 = addr.getValue('complemento');
                sStreetAddr3= addr.getValue('bairro');
                sCity = addr.getValue('nome');
                sStateCode = addr.getValue('uf');
                sPostalCode = addr.getvalue('cep');
                                
                cnpjInfo.add('StreetNo:'+sStreetNo );
                cnpjInfo.add('StreetAddr1:'+sStreetAddr1 );
                cnpjInfo.add('StreetAddr2:'+sStreetAddr2 );
                cnpjInfo.add('District:'+sStreetAddr3);
                cnpjInfo.add('State:'+sCity );
                cnpjInfo.add('StateCode:'+sStateCode  + ' ' + sPostalCode);
                                
                rCNPJ.StreetAddr1 = sStreetAddr1;
                rCNPJ.StreetAddr2 = sStreetAddr2;
                rCNPJ.StreetAddr3 = sStreetAddr3 ;
                rCNPJ.StreetNo = sStreetNo;
                rCNPJ.City = sCity;
                rCNPJ.StateCode =sStateCode;
                rCNPJ.PostalCode = sPostalCode;
              }
            }
            for (XmlDom.Element stat : rec.getElementsByTagName('situacaoReceitaFederal')) {
              sCNPJStatus = stat.getValue('descricao');
              //System.Debug('CNJP Status:'+sCNPJStatus ); 
              if (sCNPJStatus.IndexOf('CNPJ ATIVO' ) > 0 ){ 
                sCNPJStatus = 'Active'; }
                rCNPJ.CNPJStatus = sCNPJStatus;
              }
              cnpjInfo.add('AccountName:' + sAccntName);
              cnpjInfo.add('AKA:' +sAKA);
              cnpjInfo.add('CNPJ:' + sCNPJNumber + ';' + sCNPJStatus);

            } //end for
                    
          // system.Debug('-----------------------------------');
          //for ( string inc :cnpJInfo) {
          //  system.Debug('List values:'+ inc); 
          //} 
          //system.Debug('-----------------------------------');
        } // if
        wsresp.cnpjResults.add(rCNPJ); 
        wsresp.success = true;
      } 
      else if (resp.getStatusCode() != 200) {
        system.debug('update response = '+resp.getBody()); 
        // responseVal = resp.toString();
        // system.debug('update response2 = '+responseVal );  
        wsresp.errorMsg = wsStatusCode+' : '+parseErrors(resp.getBody());
        wsresp.success = false;
      }
      
      //lets check if the cnpjResult has anything in it
      
      system.Debug('Return from Class :'+ rCNPJ.returnStr() );
      
      return wsresp; //responseVal ; //give the http response code 200/500 etc FOR BAD CNJP NEED TO TRAP IT DIFFERENTLY
    } 
    catch (System.CalloutException e) {
      wsresp.errorMsg = '0:'+e.getmessage();
      wsresp.success = false;
      return wsresp;
    }
    return wsresp; 
  }

  
  public static webserviceResponse CallCPFCheckWS(String cpfNumVal) {
    
    webserviceResponse wsresp = new webserviceResponse();
    
    // List <cpfResult> cpfResultLst = new List<cpfResult>();
    if (String.isBlank(smfSettings.URL__c)) {
      wsresp.success = false;
      wsresp.errorMsg = webserviceName+' not found.';
      return wsresp;
    }
    
    if (smfSettings.Active__c == false) {
      wsresp.offline = true;
      wsresp.success = false;
      wsresp.errorMsg = '';
      return wsresp;
    }
    
    // String responseVal;
    String sContactName;
    String sCPFNumber;
    String sDateofBirth;
    String sStreetAddr1;
    String sStreetAddr2;
    String sStreetAddr3;
    String sCity;
    String sStreetNo;
    String sStateCode;
    String sPostalCode;
    String sSex;
    String sMothersMaidenName;

    cpfResult rCPF = new cpfResult(); //class containing CPF Details
    
    Http http = new Http();
    
    HttpRequest req = buildHttpRequest();
    try {                
      
      req.setBody(buildXmlRequest(
        'CPF',
        cpfNumVal,
        smfSettings.Username__c,
        smfSettings.Password__c
      ));
      
      // system.debug('from class SerasaWSMainframe = :'+req.getBody()); // Must not show this as password is contained
      
      //Long startTime = System.currentTimeMillis();
      HttpResponse resp = http.send(req);
      //system.debug('Profile update Time in Millis :: = '+(System.currentTimeMillis()- startTime));
          
      List <String> cpfInfo= new List <String>();
       
      String wsStatusCode = string.valueof(resp.GetStatusCode());
      
      system.debug('Auth success check =' + wsStatusCode); //is it 200 (success) or 500 (failed)?
            
      if (resp != null && resp.getStatusCode() == 200) {
        
        system.debug('update response = '+resp.getBody()); 
        // responseVal = resp.toString();
        //system.debug('update response2 = '+responseVal );  
        //PARSE USING DOM
        XmlDom doc = new XmlDom(resp.getBody());
        XmlDom.Element[] recs = doc.getElementsByTagName('ObterDadosReceitaFederalResponse'); // pessoaJuridica'); //pessoa
        if (recs != null) {
          for (XMlDom.Element rec : recs) {
            sContactName = rec.getValue('nome');
            sCPFNumber = rec.getValue('numero');
            sDateofBirth = rec.getValue('dataNascimento');
            sSex = rec.getValue('sexo');
            sMothersMaidenName = rec.getValue('nomeMae');

            // responseVal = wsStatusCode +';'+ sContactName+';'+sCPFNumber;
            rCPF.ContactName = sContactName;
            rCPF.CPFNumber= sCPFNumber;
            rCPF.DateofBirth= sDateofBirth;
            rCPF.StatusCode = wsStatusCode;
            rCPF.Sex = sSex;
            rCPF.MothersMaidenName = sMothersMaidenName;
                        
            //3nd element under enderecos
            for (XmlDom.Element addr : rec.getElementsByTagName('endereco')) {
              if (addr.getValue('identificador') =='RECEITA' ) { 
                //this is the IRS address
                sStreetNo = addr.getValue('numero');
                sStreetAddr1 = addr.getValue('logradouro');
                sStreetAddr2 = addr.getValue('complemento');
                sStreetAddr3 = addr.getValue('bairro');
                sCity = addr.getValue('nome');
                sStateCode = addr.getValue('uf');
                sPostalCode = addr.getvalue('cep');
                              
                cpfInfo.add('StreetNo:'+sStreetNo );
                cpfInfo.add('StreetAddr1:'+sStreetAddr1 );
                cpfInfo.add('StreetAddr2:'+sStreetAddr2 );
                cpfInfo.add('StreetAddr3:'+sStreetAddr3 );
                cpfInfo.add('City:'+sCity );
                cpfInfo.add('StateCode:'+sStateCode );
                cpfInfo.add('PostalCode:'+sPostalCode );
                               
                rCPF.StreetNo = sStreetNo;
                rCPF.StreetAddr1 = sStreetAddr1;
                rCPF.StreetAddr2 = sStreetAddr2;
                rCPF.StreetAddr3 = sStreetAddr3;
                rCPF.City = sCity;
                rCPF.StateCode = sStateCode;
                rCPF.PostalCode = sPostalCode;
              }
            }  
            cpfInfo.add('ContactName:' + sContactName);
            cpfInfo.add('CPF:' + sCPFNumber);                      
          } //end for
          //system.Debug('Fine till here?');
          //system.Debug('-----------------------------------');
          //for ( string inc :cpfInfo) {
          //  system.Debug('List values:'+ inc); 
          //} 
          //system.Debug('-----------------------------------');                    
        }
        wsresp.success = true;
        wsresp.cpfResults.add(rCPF);
      } 
      else if (resp.getStatusCode() != 200) {
        system.debug('update response = '+resp.getBody()); 
        // responseVal = resp.toString();
        //system.debug('update response2 = '+responseVal );  
        wsresp.errorMsg = wsStatusCode+' : '+parseErrors(resp.getBody());
        wsresp.success = false;
      }
      //lets check if the cpfResult has anything in it
      // system.debug('Return from Class :'+ rCPF.returnStr() );
      return wsresp; //responseVal ; 
    } 
    catch(System.CalloutException e){
      //rCPF.StatusCode = '0';
      //rCPF.ErrorMessage = e.getmessage();
      wsresp.errorMsg = '0:'+e.getmessage();
      wsresp.success = false;
      //cpfResultLst.add(rCPF); 
      return wsresp;            
    }
    return wsresp; //responseVal ;
  }
  
  private static String parseErrors(String respBody) {
    String errorMsg = '';
    XmlDom errdoc = new XmlDom(respBody);
    XmlDom.Element[] errrecs = errdoc.getElementsByTagName('BusinessFault'); 
    String errMessage;
    if (errrecs != null) {
      for (XMlDom.Element rec1 : errrecs) {
        errMessage = rec1.getValue('mensagem');
        system.Debug('Error Message:'+ errMessage); 
        //responseVal = wsStatusCode + ';' + errMessage;
        errorMsg = errMessage;
      }
    }
    XmlDom.Element[] techErrs = errdoc.getElementsByTagName('TechnicalFault'); 
    if (techErrs != null) {
      for (XMlDom.Element rec1 : techErrs) {
        errMessage = rec1.getValue('mensagem');
        system.Debug('Error Message:'+ errMessage); 
        //responseVal = wsStatusCode + ';' + errMessage;
        errorMsg = errMessage + ' - Please contact CRMSupport@experian.com';
      }
    }
    return errorMsg;
  }
  
  public class MockCalloutCPF implements HttpCalloutMock {
    
    String goodResponseBody = '<?xml version="1.0" encoding="utf-8"?>'
      +'<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsa="http://www.w3.org/2005/08/addressing">'
      +'<env:Header><wsa:MessageID>urn:EDB114C07E3511E5BFFE51D9E2086429</wsa:MessageID>'
      +'<wsa:ReplyTo><wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>'
      +'<wsa:ReferenceParameters>'
      +'<instra:tracking.compositeInstanceCreatedTime xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">2015-10-29T10:09:38.466-02:00</instra:tracking.compositeInstanceCreatedTime>'
      +'</wsa:ReferenceParameters>'
      +'</wsa:ReplyTo>'
      +'<wsa:FaultTo><wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>'
      +'<wsa:ReferenceParameters><instra:tracking.compositeInstanceCreatedTime xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">2015-10-29T10:09:38.466-02:00</instra:tracking.compositeInstanceCreatedTime>'
      +'</wsa:ReferenceParameters></wsa:FaultTo></env:Header>'
      +'<env:Body><ObterDadosReceitaFederalResponse xmlns="http://services.experian.com.br/EBS/Pessoa/v1" xmlns:tns="http://services.experian.com.br/EBS/Pessoa/v1">'
      +'<pessoa><ns9:pessoaFisica xmlns:ns9="http://services.experian.com.br/EBO/Pessoa/v1">'
      +'<ns9:nome>ADRIANO DAURIZIO</ns9:nome>'
      +'<ns9:dataNascimento>1971-09-11</ns9:dataNascimento>'
      +'<ns9:nomeMae>M ASSIMINA FERRANTE DAURIZIO</ns9:nomeMae>'
      +'<ns9:sexo>MASCULINO</ns9:sexo>'
      +'<ns9:dadosProfissionais></ns9:dadosProfissionais>'
      +'</ns9:pessoaFisica>'
      +'<ns9:documentos xmlns:ns9="http://services.experian.com.br/EBO/Pessoa/v1">'
      +'<ns9:documento>'
      +'<ns9:numero>17113441840</ns9:numero>'
      +'<ns9:tipo>CPF</ns9:tipo>'
      +'</ns9:documento>'
      +'</ns9:documentos>'
      +'<ns9:enderecos xmlns:ns9="http://services.experian.com.br/EBO/Pessoa/v1">'
      +'<ns8:endereco xmlns:ns8="http://services.experian.com.br/EBO/Endereco/v1">'
      +'<ns8:numero>S/N</ns8:numero>'
      +'<ns8:cep>00000000</ns8:cep>'
      +'<ns8:cidade>'
      +'<ns8:estado></ns8:estado>'
      +'</ns8:cidade>'
      +'<ns8:identificador>RECEITA</ns8:identificador>'
      +'</ns8:endereco>'
      +'</ns9:enderecos>'
      +'<ns9:telefones xmlns:ns9="http://services.experian.com.br/EBO/Pessoa/v1">'
      +'<ns6:telefone xmlns:ns6="http://services.experian.com.br/EBO/Telefone/v1">'
      +'<ns6:numero>0</ns6:numero>'
      +'</ns6:telefone>'
      +'</ns9:telefones>'
      +'<ns9:situacaoReceitaFederal xmlns:ns9="http://services.experian.com.br/EBO/Pessoa/v1">'
      +'<ns9:codigo>2</ns9:codigo>'
      +'<ns9:descricao>(ATIVA) -</ns9:descricao>'
      +'<ns9:data>2015-03-11</ns9:data>'
      +'<ns9:anoReferencia>15</ns9:anoReferencia>'
      +'</ns9:situacaoReceitaFederal>'
      +'</pessoa>'
      +'<dadosAdicionais>'
      +'<origem>00000000 RECEITA</origem>'
      +'<percentualGrafia>0</percentualGrafia>'
      +'</dadosAdicionais>'
      +'</ObterDadosReceitaFederalResponse>'
      +'</env:Body></env:Envelope>';
    
    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse res = new HttpResponse();
      res.setHeader('Content-Type', 'application/xml');
      res.setBody(goodResponseBody);
      res.setStatusCode(200);
      return res;
    }
  }
  
  public class MockCalloutCNPJ implements HttpCalloutMock {
    
    String goodResponseBody = '<?xml version="1.0" encoding="utf-8"?>'
      +'<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsa="http://www.w3.org/2005/08/addressing">'
      +'<env:Header>'
      +'<wsa:MessageID>urn:CE2544A07E3A11E5BFFE51D9E2086429</wsa:MessageID>'
      +'<wsa:ReplyTo><wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>'
      +'<wsa:ReferenceParameters>'
      +'<instra:tracking.compositeInstanceCreatedTime xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">2015-10-29T10:44:32.020-02:00</instra:tracking.compositeInstanceCreatedTime>'
      +'</wsa:ReferenceParameters>'
      +'</wsa:ReplyTo>'
      +'<wsa:FaultTo>'
      +'<wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>'
      +'<wsa:ReferenceParameters>'
      +'<instra:tracking.compositeInstanceCreatedTime xmlns:instra="http://xmlns.oracle.com/sca/tracking/1.0">2015-10-29T10:44:32.020-02:00</instra:tracking.compositeInstanceCreatedTime>'
      +'</wsa:ReferenceParameters>'
      +'</wsa:FaultTo>'
      +'</env:Header>'
      +'<env:Body>'
      +'<ObterDadosReceitaFederalResponse xmlns="http://services.experian.com.br/EBS/Pessoa/v1" xmlns:tns="http://services.experian.com.br/EBS/Pessoa/v1">'
      +'<pessoa>'
      +'<ns9:pessoaJuridica xmlns:ns9="http://services.experian.com.br/EBO/Pessoa/v1">'
      +'<ns9:razaoSocial>DEBRITO PROPAGANDA LTDA</ns9:razaoSocial>'
      +'<ns9:dataFundacao>1994-09-22</ns9:dataFundacao>'
      +'<ns9:nire>0</ns9:nire>'
      +'<ns9:codigoAtividadeSerasa>S030200</ns9:codigoAtividadeSerasa>'
      +'<ns9:codigoCnae>7311400</ns9:codigoCnae>'
      +'<ns9:codigoTipoSociedade>206</ns9:codigoTipoSociedade>'
      +'<ns9:quantidadeFiliais>3</ns9:quantidadeFiliais>'
      +'</ns9:pessoaJuridica>'
      +'<ns9:documentos xmlns:ns9="http://services.experian.com.br/EBO/Pessoa/v1">'
      +'<ns9:documento>'
      +'<ns9:numero>000000424000156</ns9:numero>'
      +'<ns9:tipo>CNPJ</ns9:tipo>'
      +'</ns9:documento>'
      +'</ns9:documentos>'
      +'<ns9:enderecos xmlns:ns9="http://services.experian.com.br/EBO/Pessoa/v1">'
      +'<ns8:endereco xmlns:ns8="http://services.experian.com.br/EBO/Endereco/v1">'
      +'<ns8:logradouro>FIDENCIO RAMOS</ns8:logradouro>'
      +'<ns8:numero>S/N</ns8:numero>'
      +'<ns8:cep>04551010</ns8:cep>'
      +'<ns8:bairro>VL OLIMPIA</ns8:bairro>'
      +'<ns8:cidade>'
      +'<ns8:nome>SAO PAULO</ns8:nome>'
      +'<ns8:estado><ns8:uf>SP</ns8:uf></ns8:estado>'
      +'<ns8:codigoPraca>SPO</ns8:codigoPraca>'
      +'<ns8:codigoMunicipio>3550308</ns8:codigoMunicipio>'
      +'</ns8:cidade>'
      +'<ns8:identificador>SIMPLIFY</ns8:identificador>'
      +'</ns8:endereco>'
      +'<ns8:endereco xmlns:ns8="http://services.experian.com.br/EBO/Endereco/v1">'
      +'<ns8:logradouro>R FIDENCIO RAMOS</ns8:logradouro>'
      +'<ns8:cep>04551010</ns8:cep>'
      +'<ns8:bairro>VL OLIMPIA</ns8:bairro>'
      +'<ns8:cidade>'
      +'<ns8:nome>SAO PAULO</ns8:nome>'
      +'<ns8:estado><ns8:uf>SP</ns8:uf></ns8:estado>'
      +'<ns8:codigoPraca>SPO</ns8:codigoPraca>'
      +'<ns8:codigoMunicipio>3550308</ns8:codigoMunicipio>'
      +'</ns8:cidade>'
      +'<ns8:identificador>CORREIOS</ns8:identificador>'
      +'</ns8:endereco>'
      +'<ns8:endereco xmlns:ns8="http://services.experian.com.br/EBO/Endereco/v1">'
      +'<ns8:logradouro>FIDENCIO RAMOS</ns8:logradouro>'
      +'<ns8:numero>223</ns8:numero>'
      +'<ns8:bairro>VL OLIMPIA</ns8:bairro>'
      +'<ns8:complemento>AND 1 CJ 11</ns8:complemento>'
      +'<ns8:cidade>'
      +'<ns8:nome>SAO PAULO</ns8:nome>'
      +'<ns8:estado><ns8:uf>SP</ns8:uf></ns8:estado>'
      +'<ns8:codigoPraca>SPO</ns8:codigoPraca>'
      +'<ns8:codigoMunicipio>3550308</ns8:codigoMunicipio>'
      +'</ns8:cidade><ns8:identificador>RECEITA</ns8:identificador>'
      +'</ns8:endereco>'
      +'</ns9:enderecos>'
      +'<ns9:situacaoReceitaFederal xmlns:ns9="http://services.experian.com.br/EBO/Pessoa/v1">'
      +'<ns9:codigo>2</ns9:codigo>'
      +'<ns9:descricao>(ATIVA) - CNPJ ATIVO</ns9:descricao>'
      +'<ns9:data>2012-01-08</ns9:data>'
      +'<ns9:anoReferencia>12</ns9:anoReferencia>'
      +'</ns9:situacaoReceitaFederal>'
      +'</pessoa>'
      +'<dadosAdicionais>'
      +'<origem>RECEITA</origem>'
      +'<percentualGrafia>0</percentualGrafia>'
      +'<codigoCnaeAnterior>5573</codigoCnaeAnterior>'
      +'<codigoNaturezaSerasa>S0302</codigoNaturezaSerasa>'
      +'<codigoNaturezaReceita>31</codigoNaturezaReceita>'
      +'</dadosAdicionais>'
      +'</ObterDadosReceitaFederalResponse>'
      +'</env:Body></env:Envelope>';
    
    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse res = new HttpResponse();
      res.setHeader('Content-Type', 'application/xml');
      res.setBody(goodResponseBody);
      res.setStatusCode(200);
      return res;
    }
    
  }
  
  
  public class MockBadCalloutCNPJ implements HttpCalloutMock {
    
    String badResponseBody = '<?xml version="1.0" encoding="utf-8"?>'
      +'<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">'
      +'<env:Body>'
      +'<env:Fault xmlns:ns0="http://services.experian.com.br/EBS/Pessoa/v1">'
      +'<faultcode>ns0:BusinessFault</faultcode>'
      +'<faultstring></faultstring>'
      +'<faultactor></faultactor>'
      +'<detail>'
      +'<BusinessFault xmlns="http://services.experian.com.br/EBO/Fault/v1">'
      +'<codigo>9</codigo>'
      +'<data>2015-10-29T12:26:39.490-02:00</data>'
      +'<descricao></descricao>'
      +'<mensagem>Ocorreu um erro no webservice - (23) FILIAL NAO CADASTRADA</mensagem>'
      +'</BusinessFault>'
      +'</detail>'
      +'</env:Fault>'
      +'</env:Body></env:Envelope>';
    
    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse res = new HttpResponse();
      res.setHeader('Content-Type', 'application/xml');
      res.setBody(badResponseBody);
      res.setStatusCode(500);
      return res;
    }
    
  }
  
  public class MockBadCalloutCPF implements HttpCalloutMock {
    
    String badResponseBody = '<?xml version="1.0" encoding="utf-8"?>'
      +'<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">'
      +'<env:Body>'
      +'<env:Fault xmlns:ns0="http://services.experian.com.br/EBS/Pessoa/v1">'
      +'<faultcode>ns0:BusinessFault</faultcode>'
      +'<faultstring></faultstring>'
      +'<faultactor></faultactor>'
      +'<detail>'
      +'<BusinessFault xmlns="http://services.experian.com.br/EBO/Fault/v1">'
      +'<codigo>9</codigo>'
      +'<data>2015-10-29T12:32:04.629-02:00</data>'
      +'<descricao></descricao>'
      +'<mensagem>Ocorreu um erro no webservice - (23) CPF NAO EXISTE NO CADASTRO DA SRF ATE ESTA DATA</mensagem>'
      +'</BusinessFault>'
      +'</detail>'
      +'</env:Fault>'
      +'</env:Body></env:Envelope>';
    
    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse res = new HttpResponse();
      res.setHeader('Content-Type', 'application/xml');
      res.setBody(badResponseBody);
      res.setStatusCode(500);
      return res;
    }
    
  }
 
}