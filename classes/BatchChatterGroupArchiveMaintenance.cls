/*=============================================================================
 * Experian
 * Name: BatchChatterGroupArchiveMaintenance
 * Description: Generates an email reminder to group owner about group being archieved for more than 60 days
 * Created Date: 2016-05-25
 * Created By: Diego Olarte
 *
 * Date Modified      Modified By           Description of the update
 * Jun 3rd, 2016      Paul Kissick          Made 'public' instead of 'global'. Tidied formatting. 
                                            Fixed bug with sendTo array. Added link to group in email.
 * Oct 19th, 2016     Diego Olarte          Case 02131459: Replace the to from Owner to Admin Members.
 =============================================================================*/

public class BatchChatterGroupArchiveMaintenance implements Database.Batchable<sObject>, Database.Stateful {
    
  public Database.Querylocator start (Database.Batchablecontext bc) {
    
    Global_Settings__c gblSetting = Global_Settings__c.getValues(Constants.GLOBAL_SETTING);
    
    Integer daysParam1 = Integer.valueOf((gblSetting.chatterGrpActy_DaysBefore__c != null) ? gblSetting.chatterGrpActy_DaysBefore__c : 61.0);
    Integer daysParam2 = Integer.valueOf((gblSetting.chatterGrpActy_DaysAfter__c != null) ? gblSetting.chatterGrpActy_DaysAfter__c : 60.0);
    
    // Note, the first days parameter must be greater than the second.
    if (daysParam1 <= daysParam2) {
      daysParam2 = Integer.valueOf((gblSetting.chatterGrpActy_DaysBefore__c != null) ? gblSetting.chatterGrpActy_DaysBefore__c : 61.0);
      daysParam1 = Integer.valueOf((gblSetting.chatterGrpActy_DaysAfter__c != null) ? gblSetting.chatterGrpActy_DaysAfter__c : 60.0);
    }
    
    String query = ' SELECT Id, Name, IsArchived, LastFeedModifiedDate, LastModifiedDate, LastViewedDate, OwnerId, Owner.Email, Owner.Name '+ 
      ' FROM CollaborationGroup '+ 
      ' WHERE IsArchived = false '+ 
      ' AND OwnerID NOT IN ( SELECT Id FROM User WHERE Profile.Name = \'System Administrator\') '+
      ' AND (LastFeedModifiedDate >= LAST_N_DAYS:'+String.valueOf(daysParam1)+' AND LastFeedModifiedDate < LAST_N_DAYS:'+String.valueOf(daysParam2)+') '+ 
      ' LIMIT 200 ';

    system.debug(query);
    return Database.getQueryLocator(query);
  }
  
  public void execute (Database.BatchableContext bc, List<CollaborationGroup> scope) {
    processGroups(scope);
  }
  
  public static void processGroups(List<CollaborationGroup> scope) {
    
    List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
    
    Global_Settings__c gblSetting = Global_Settings__c.getValues(Constants.GLOBAL_SETTING);
   
    List<OrgWideEmailAddress> orgWEA = [
      SELECT Id 
      FROM OrgWideEmailAddress 
      WHERE Address = :gblSetting.chatterGrpActy_FromAddress__c
      LIMIT 1
    ];
    /* DO: Commented out to replace for new loops to sent to Admin Members
    for (CollaborationGroup chatterGroup : scope) {
      
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();   
      mail.setToAddresses(new String[]{chatterGroup.Owner.Email});
      
      if (orgWEA.size() == 1) {
        mail.setOrgWideEmailAddressId(orgWEA[0].Id);
      }
      
      String urlToGroup = URL.getSalesforceBaseUrl().toExternalForm()+ '/'+chatterGroup.Id;
      
      mail.setSubject('Chatter Group Inactive for 60 days');
      String body = '<p>Dear ' + chatterGroup.Owner.Name.escapeHtml4() + ',</p>';
      body += '<p>The Chatter group, "' + chatterGroup.Name.escapeHtml4() + '", owned by you has 60 days with no activity.</p>';
      body += '<p>Please review accordingly, otherwise the system will archive it 30 days after you receive this notification.<p>';
      body += '<p><a href="'+urlToGroup+'">'+urlToGroup+'</a><p>';
      mail.setHtmlBody(body);

      mails.add(mail);
    }*/
    
    List<Messaging.SingleEmailMessage>  myEmails = new List<Messaging.SingleEmailMessage>();    
        
    for (CollaborationGroupMember toAddresses : [SELECT Member.Name, Member.Email, CollaborationGroupId, CollaborationGroup.Name FROM CollaborationGroupMember WHERE CollaborationRole = 'Admin' AND CollaborationGroupId =: scope]) {
      
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          mail.setToAddresses(new String[]{toAddresses.Member.Email});
          
          System.debug('The email is: ' + toAddresses.Member.Email);
      
          if (orgWEA.size() == 1) {
            mail.setOrgWideEmailAddressId(orgWEA[0].Id);
          }
          
          String urlToGroup = URL.getSalesforceBaseUrl().toExternalForm()+ '/'+ toAddresses.CollaborationGroupId;
          
          mail.setSubject('Chatter Group Inactive for 60 days');          
          String body = '<p>Dear ' + toAddresses.Member.Name.escapeHtml4() + ',</p>';
          body += '<p>The Chatter group, "' + toAddresses.CollaborationGroup.Name.escapeHtml4() + '", owned by you has 60 days with no activity.</p>';
          body += '<p>Please review accordingly, otherwise the system will archive it 30 days after you receive this notification.<p>';
          body += '<p><a href="'+urlToGroup+'">'+urlToGroup+'</a><p>';
          mail.setHtmlBody(body);
        
          myEmails.add(mail);
      }
      
    if (!myEmails.isEmpty()) {
      Messaging.sendEmail(myEmails);
    }
  }
  
  public void finish (Database.BatchableContext bc) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchChatterGroupArchiveMaintenance', true);
  }
  
}