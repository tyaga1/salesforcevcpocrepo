/**=====================================================================
 * Appirio, Inc
 * Name: ContactRedirectController_Test
 * Description: Test class for ContactRedirectController
 * Created Date: Oct 08th, 2015
 * Created By: Arpita Bose (Appirio)
 * 
 * Date Modified      Modified By                  Description of the update
 * Apr 7th, 2016      Paul Kissick                 Case 01932085: Fixing Test User Email Domain
 * Mar 15, 2017       Ryan (Weijie) Hu             Added testing method for serasa consumer contact creation
 * Jun 06th, 2017     Tyaga pati                   Inserted Contact
 * =====================================================================*/
@isTest
private class ContactRedirectController_Test {
  
  private static String testUserEmail = 'test1234@experian.com';
  
  static testmethod void testContactRedirect_SerasaUserWithoutCPF(){
    // create test data
    Profile p = [select id from profile where name=: Constants.PROFILE_EXP_SERASA_FINANCE ];
    PermissionSet perSet = [SELECT Id, Name FROM PermissionSet
                              WHERE Name =: Constants.PERMISSIONSET_EDQ_SAAS_DEPLOYMENT_MANAGER];
                                
    User testUser = Test_Utils.createUser(p, testUserEmail, 'test1');
    testUser.EmployeeNumber = 'E1234';
    insert testUser;
    
    PermissionSetAssignment psetAssgn = new PermissionSetAssignment (AssigneeId = testUser.Id, PermissionSetId = perSet.Id);
    
    Serasa_User_Profiles__c custSettings = new Serasa_User_Profiles__c(Name = Constants.SERASA, Profiles__c= Constants.PROFILE_EXP_SERASA_FINANCE) ;
    insert custSettings;
     
    Account account = Test_Utils.insertAccount();
    Contact con = Test_Utils.createContact(account.ID);
    insert con;
    
    system.runAs(testUser) {
    Test.startTest();
    
      ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)con);
      ContactRedirectController controller = new ContactRedirectController(stdController);
      
      controller.accId = account.Id;
      controller.accName = account.Name;
      controller.selectedCPFOption = 'CPFNotNeeded';
      
      // call controller methods
      controller.getNewAccountPermSets();
      controller.getNewAccountProfSets();
      
      controller.redirect();
      //Asserts
      System.assertEquals(null,controller.redirect());
      
      controller.newContact();
      controller.redirectToCPF(); 
      //Asserts
      System.assertNotEquals(null,controller.redirectToCPF());
      controller.newContactAndAddress();
      
      
      Test.stopTest();
    }
  }
  
  //
  static testmethod void testContactRedirect_SerasaUserWithCPF(){
    // create test data
    Profile p = [select id from profile where name=: Constants.PROFILE_EXP_SERASA_FINANCE ];
    PermissionSet perSet = [SELECT Id, Name FROM PermissionSet
                              WHERE Name =: Constants.PERMISSIONSET_EDQ_SAAS_DEPLOYMENT_MANAGER];
                                
    User testUser = Test_Utils.createUser(p, testUserEmail, 'test1');
    testUser.EmployeeNumber = 'E1234';
    insert testUser;
    
    PermissionSetAssignment psetAssgn = new PermissionSetAssignment (AssigneeId = testUser.Id, PermissionSetId = perSet.Id);
    
    Serasa_User_Profiles__c custSettings = new Serasa_User_Profiles__c(Name = Constants.SERASA, Profiles__c= Constants.PROFILE_EXP_SERASA_FINANCE) ;
    insert custSettings;
     
    Account account = Test_Utils.insertAccount();
    Contact con = Test_Utils.createContact(account.ID);
    insert con;
    
    system.runAs(testUser) {
    Test.startTest();
    
      ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)con);
      ContactRedirectController controller = new ContactRedirectController(stdController);
      
      controller.accId = account.Id;
      controller.accName = account.Name;
      controller.selectedCPFOption = 'CPFNeeded';
      
      // call controller methods
      controller.getNewAccountPermSets();
      controller.getNewAccountProfSets();
      
      controller.redirect();
      //Asserts
      System.assertEquals(null,controller.redirect());
      controller.newContact();
      controller.redirectToCPF(); 
      //Asserts
      System.assertNotEquals(null,controller.redirectToCPF());
      controller.newContactAndAddress();
      
      
      Test.stopTest();
    }
  }

  //
  static testmethod void testContactRedirect_SerasaUserConsumerContact() {
    // create test data
    Profile p = [select id from profile where name=: Constants.PROFILE_EXP_SERASA_FINANCE ];
    PermissionSet perSet = [SELECT Id, Name FROM PermissionSet
                              WHERE Name =: Constants.PERMISSIONSET_EDQ_SAAS_DEPLOYMENT_MANAGER];
                                
    User testUser = Test_Utils.createUser(p, testUserEmail, 'test1');
    testUser.EmployeeNumber = 'E1234';
    insert testUser;
    
    PermissionSetAssignment psetAssgn = new PermissionSetAssignment (AssigneeId = testUser.Id, PermissionSetId = perSet.Id);
    
    Serasa_User_Profiles__c custSettings = new Serasa_User_Profiles__c(Name = Constants.SERASA, Profiles__c= Constants.PROFILE_EXP_SERASA_FINANCE) ;
    insert custSettings;
     
    Account account = Test_Utils.insertAccount();
    account.Serasa_Consumer_Active_Account__c = true;
    RecordType consumer_account_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Consumer_Account'];
    account.RecordTypeId = consumer_account_rt.Id;
    update account;

    Serasa_Consumer_Contact_Active_Account__c newRecordOnSetting = new Serasa_Consumer_Contact_Active_Account__c();
    newRecordOnSetting.Active_Account_Id__c = account.Id;
    insert newRecordOnSetting;
    RecordType consumer_con_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Consumer_Contact'];

    Contact con = new Contact(
      MailingCountry = 'United States of America',
      MailingState = 'Texas',
      MailingCity = 'Dallas',
      FirstName = 'firstname'+111,
      Salutation = 'Mr.',
      LastName = 'test'+111,
      Email = 'test@'+111+'test.com',
      AccountId = account.Id,
      RecordTypeId = consumer_con_rt.Id
      
    );
    //Contact con = Test_Utils.createContact(account.ID);
    insert con;
    
    system.runAs(testUser) {
    Test.startTest();
    
      RecordType consumer_contact_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Consumer_Contact'];

      ApexPages.currentPage().getParameters().put('accId',account.id);
      ApexPages.currentPage().getParameters().put('RecordType', consumer_contact_rt.Id);
      ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)con);
      ContactRedirectController controller = new ContactRedirectController(stdController);
      
      controller.accId = account.Id;
      controller.accName = account.Name;
      controller.selectedCPFOption = 'CPFNeeded';
      
      controller.redirect();
      
      Test.stopTest();
    }
  }

  //
  static testmethod void testContactRedirect_SerasaUserConsumerContact2() {
    // create test data
    Profile p = [select id from profile where name=: Constants.PROFILE_EXP_SERASA_FINANCE ];
    PermissionSet perSet = [SELECT Id, Name FROM PermissionSet
                              WHERE Name =: Constants.PERMISSIONSET_EDQ_SAAS_DEPLOYMENT_MANAGER];
                                
    User testUser = Test_Utils.createUser(p, testUserEmail, 'test1');
    testUser.EmployeeNumber = 'E1234';
    insert testUser;
    
    PermissionSetAssignment psetAssgn = new PermissionSetAssignment (AssigneeId = testUser.Id, PermissionSetId = perSet.Id);
    
    Serasa_User_Profiles__c custSettings = new Serasa_User_Profiles__c(Name = Constants.SERASA, Profiles__c= Constants.PROFILE_EXP_SERASA_FINANCE) ;
    insert custSettings;
     
    Account account = Test_Utils.insertAccount();

    Contact con = Test_Utils.createContact(account.ID);
    insert con;
    
    system.runAs(testUser) {
    Test.startTest();
    
      RecordType consumer_contact_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Consumer_Contact'];

      ApexPages.currentPage().getParameters().put('accId',account.id);
      ApexPages.currentPage().getParameters().put('RecordType', consumer_contact_rt.Id);
      ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)con);
      ContactRedirectController controller = new ContactRedirectController(stdController);
      
      controller.accId = account.Id;
      controller.accName = account.Name;
      controller.selectedCPFOption = 'CPFNeeded';
      
      controller.redirect();
      
      Test.stopTest();
    }
  }

  //
  static testmethod void testContactRedirect_SerasaUserConsumerContact3() {
    // create test data
    Profile p = [select id from profile where name=: Constants.PROFILE_EXP_SERASA_FINANCE ];
    PermissionSet perSet = [SELECT Id, Name FROM PermissionSet
                              WHERE Name =: Constants.PERMISSIONSET_EDQ_SAAS_DEPLOYMENT_MANAGER];
                                
    User testUser = Test_Utils.createUser(p, testUserEmail, 'test1');
    testUser.EmployeeNumber = 'E1234';
    insert testUser;
    
    PermissionSetAssignment psetAssgn = new PermissionSetAssignment (AssigneeId = testUser.Id, PermissionSetId = perSet.Id);
    
    Serasa_User_Profiles__c custSettings = new Serasa_User_Profiles__c(Name = Constants.SERASA, Profiles__c= Constants.PROFILE_EXP_SERASA_FINANCE) ;
    insert custSettings;
     
    Account account = Test_Utils.insertAccount();


    Account account2 = Test_Utils.insertAccount();
    Serasa_Consumer_Contact_Active_Account__c newRecordOnSetting = new Serasa_Consumer_Contact_Active_Account__c();
    newRecordOnSetting.Active_Account_Id__c = account2.Id;
    insert newRecordOnSetting;

    delete account2;

    Contact con = Test_Utils.createContact(account.ID);
    insert con;
    
    system.runAs(testUser) {
    Test.startTest();
    
      RecordType consumer_contact_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Consumer_Contact'];

      ApexPages.currentPage().getParameters().put('accId',account.id);
      ApexPages.currentPage().getParameters().put('RecordType', consumer_contact_rt.Id);
      ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)con);
      ContactRedirectController controller = new ContactRedirectController(stdController);
      
      controller.accId = account.Id;
      controller.accName = account.Name;
      controller.selectedCPFOption = 'CPFNeeded';
      
      controller.redirect();
      
      Test.stopTest();
    }
  }
  
  //
  static testmethod void testContactRedirect_otherUser(){
    // create test data
    Profile p = [select id from profile where name=: Constants.PROFILE_EXP_SALES_EXEC ];
      
    User testUser = Test_Utils.createUser(p, testUserEmail, 'test1');
    insert testUser;
     
    Account account = Test_Utils.insertAccount();
    Contact con = Test_Utils.createContact(account.ID);
    insert con;
    
    system.runAs(testUser) {
    Test.startTest();
    
      ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)con);
      ContactRedirectController controller = new ContactRedirectController(stdController);
      
      controller.getNewAccountPermSets();
      controller.getNewAccountProfSets();
      
      controller.redirect();
      //Asserts
      System.assertNotEquals(null,controller.redirect());
      
      controller.newContactAndAddress();
      controller.newContact();
      
      Test.stopTest();
    }
  }

}