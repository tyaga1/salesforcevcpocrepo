/**=====================================================================
 * Experian
 * Name: NominationsLandingPageCntlr_test
 * Description: Test class to cover  NominationsLandingPageCntlr
 * Created Date: March 13th 2016
 * Created By: Richard Joseph
 *
 * Date Modified      Modified By                Description of the update
 * Sep  6th, 2016     Diego Olarte               Replaced Old justification with new RichJustification__c field/ No longer needed
 =====================================================================*/
@isTest
private class NominationsLandingPageCntlr_Test{
  
  static testMethod void test1() {
    NominationsLandingPageCntlr n = new NominationsLandingPageCntlr();
    system.assert(true == true);
  }
  
  
  /*
  private static testMethod void NominationsLandingPageCntlrTest() {
  List<User> usrList = new List<User>();
    User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    system.runAs(thisUser) {
      Profile p = [SELECT Id FROM PROFILE WHERE Name = :Constants.PROFILE_SYS_ADMIN ];
      UserRole copsRole = [SELECT Id FROM UserRole WHERE Name = :Constants.ROLE_NA_COPS];
      User testUser1 = Test_Utils.createEDQUser(p, 'test1234@experian.com', 'test1');
      User testUser2 = Test_Utils.createEDQUser(p, 'test1235@experian.com', 'test2');
      testUser1.UserRoleId = copsRole.Id;
      testUser2.UserRoleId = copsRole.Id;
      
      usrList.add(testUser1);
      usrList.add(testUser2);
      insert usrList;
      testUser1.Manager = testUser2;
      update testUser1;
      
      Document documentRec;
      documentRec = new Document();
      documentRec.Body = Blob.valueOf('Some Text');
      documentRec.ContentType = 'image/jpeg';
      documentRec.DeveloperName = 'my_document';
      documentRec.IsPublic = true;
      documentRec.Name = 'My Document';
      documentRec.FolderId = UserInfo.getUserId();
      insert documentRec;
      
      List<WorkBadgeDefinition>  wrkDefLst=new List<WorkBadgeDefinition>();
      wrkDefLst.add(new WorkBadgeDefinition(
        Name = 'Half Year Nomination', 
        Description = 'Test Description', 
        ImageUrl = documentRec.Id,
        IsActive = true
      ));
      
      Insert wrkDefLst;
      
      Test.starttest();
      Nomination__c nominationRecord = new Nomination__c(
        Nominee__c = TestUser1.Id,         
        Justification__c = 'Test Data'
      );
      
      NominationsLandingPageCntlr cntrlObj = new nominationsLandingPageCntlr();
      cntrlObj.newNomination=nominationRecord ;
      cntrlObj.submitNominationRec();
      
      Test.stopTest();
      system.assertequals(nominationRecord.Id != null ,true );
      
      }
  
  
  }
  */
  
  }