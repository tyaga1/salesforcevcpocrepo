/**=====================================================================
 * Appirio, Inc
 * Name: ScheduledApexOnCase
 * Description: Class to calculate the case Age considering business hours
 *             (installed from Asset)
 * Created Date: 25 Apr 2015
 * Created By: Noopur (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 * May, 12th 2015               Arpita Bose(Appirio)         T-375163: Updated for calulating Record_time__c on Case
 * Nov 17th, 2015               Paul Kissick                 Case 01244121: Fix for reopening cases that were closed during the job.
 * Aug 4th, 2016                Paul Kissick                 Case 02092488: Splitting class into a separate batch since it was timing out
 *========================================================================*/
global class ScheduledApexOnCase implements Schedulable {
  
  global void execute(SchedulableContext sc) {
    Database.executeBatch(new BatchCaseAgeUpdate(), ScopeSizeUtility.getScopeSizeForClass('BatchCaseAgeUpdate'));
  }
  
}