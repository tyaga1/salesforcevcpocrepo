/**=====================================================================
 * Appirio, Inc;
 * Name: GenerateAccountPlanPDFController
 * Description: 
 * Created Date: 
 * Created By: Appirio
 * 
 * Date Modified      Modified By                  Description of the update
 * Sep 8th, 2015      Paul Kissick                 Separating out queries from main query
 * Sep 9th, 2015      Jagjeet Singh(Appirio)       T-432468 : Added 'Additional Information' field in Account Plan Query
 * Sep 9th, 2015      Jagjeet Singh(Appirio)       T-432678 : Added new fields for the Account Plan Competitor section.
 * Sep 14th, 2015     Jagjeet Singh(Appirio)       T-432478 : Retrieving associated Notes On the Account Plan
 * Oct. 3rd, 2016     James Wills                  Case #02154715 : Updated taskList generation to include Account_Plan_Objective__c Tasks
 * Jan. 25th, 2017    James Wills                  Case #01999757 : Added Constructor and initController(), getPDFSections() methods, moved most functionality to new Apex Class AccountPlanPDFComponentsController
 *  =====================================================================*/
global with sharing class GenerateAccountPlanPDFController {
  
  public ID AccountPlanObjId {get;set;}
  
  public Account_Plan__c accountPlanObj {get;set;}
  
  public Boolean showExperianGoals{get;set;}
  public Boolean showExperianStrategy{get;set;}
  public Boolean showClientsCoreBusiness {get;set;}
  public Boolean showCapexInOurDomain {get;set;}
  public Boolean showOpexInOurDomain {get;set;}
  public Boolean showClientsStrategy {get;set;}
  public Boolean showClientsObjectives {get;set;}
  public Boolean showClientsChanges {get;set;}
  public Boolean showClientsCompetitors {get;set;}
  public Boolean showClientsPartners {get;set;}
  public Boolean showExperianAnnualisedRevenue {get;set;}
  public Boolean showSummaryRecentHistory {get;set;}
  public Boolean showRecentRelationshipSuccess {get;set;}
  public Boolean showRelationshipObjNotAchieved {get;set;}
  public Boolean showRelationshipLessonsLearned {get;set;}
  public Boolean showAdditionalInformation {get;set;}
  
  public static Set<String> validFieldsForList = new Set<String>{Label.ACCOUNT_PLAN_PDF_CUST_INFO,
                                                       Label.ACCOUNT_PLAN_PDF_OBJECTIVES, 
                                                       Label.ACCOUNT_PLAN_PDF_CLIENT_INFORMATION,
                                                       Label.ACCOUNT_PLAN_PDF_RECENT_HISTORY,
                                                       Label.ACCOUNT_PLAN_PDF_PENETRATION,
                                                       Label.ACCOUNT_PLAN_PDF_RELATIONSHIP,
                                                       Label.ACCOUNT_PLAN_PDF_RELATIONSHIP_RADAR,
                                                       Label.ACCOUNT_PLAN_PDF_DASHBOARD,
                                                       Label.ACCOUNT_PLAN_PDF_COMPETITORS,
                                                       Label.ACCOUNT_PLAN_PDF_CURRENT_PROVIDERS,
                                                       Label.ACCOUNT_PLAN_PDF_SWOT_ANALYSIS,
                                                       Label.ACCOUNT_PLAN_PDF_OPPORTUNITIES,
                                                       Label.ACCOUNT_PLAN_PDF_CRITICAL_SUCCESS_FACTORS,
                                                       Label.ACCOUNT_PLAN_PDF_NOTES,
                                                       Label.ACCOUNT_PLAN_PDF_ACCOUNT_TEAM,
                                                       Label.ACCOUNT_PLAN_PDF_CONTACTS};
  
  
  
  global class selectedSections_Class implements Comparable{
    public integer order;
    public String paramName;
    public String paramLabel;
  
    global Integer compareTo(Object compareTo) {
       selectedSections_Class compareToSelectedSection = (selectedSections_Class)compareTo;
       // The return value of 0 indicates that both elements are equal.
       Integer returnValue = 0;
       if (order> compareToSelectedSection.order) {
         // Set return value to a positive value.
         returnValue = 1;
       } else if (order < compareToSelectedSection.order) {
         // Set return value to a negative value.
         returnValue = -1;
       }
       return returnValue;
    }
  
  }
  //public String schemaToUseForPDFId{get;set;}
  
  //Note - this parameter is passed to the PDF file when this is generated. The file will not generate properly if the value of this parameter is null.
  public String schemaToUseForPDFId{get{
      if(ApexPages.currentPage().getParameters().get('schemaToUseForPDFId')!=null){
        return ApexPages.currentPage().getParameters().get('schemaToUseForPDFId');
      } else{
        return '';
      }
    }  
  set;}
  
  /*public Account_Plan_PDF_Data__c schemaToUseForPDF{
      get{
         return [SELECT Name, AccountPlanPDFCustInfo__c, AccountPlanPDFClientInformation__c,AccountPlanPDFContacts__c, AccountPlanPDFAccountTeam__c,
                 AccountPlanPDFCompetitors__c, AccountPlanPDFCurrentProviders__c, AccountPlanPDFDashboard__c, AccountPlanPDFNotes__c, AccountPlanPDFObjectives__c,
                 AccountPlanPDFOpportunities__c, AccountPlanPDFPenetration__c, AccountPlanPDFRecentHistory__c, AccountPlanPDFRelationship__c,AccountPlanPDFRelationshipRadar__c,
                 AccountPlanPDFSWOTAnalysis__c,AccountPlanPDFCriticalSuccessFactors__c                 
                FROM Account_Plan_PDF_Data__c WHERE id =:ApexPages.currentPage().getParameters().get('schemaToUseForPDFId') LIMIT 1];
      }
      set;
  }*/
  
                                                   
  public List<String> pdfBuildInstructions_List {
    
    get{                                              
      //Map<Integer,String> pdfBuildInstructions_Map = new Map<Integer, String>();
      
      List<selectedSections_Class> selectedSectionClass_List = new List<selectedSections_Class>();
      
      List<String> pdfBuildInstructions_List = new List<String>();
      
              
      if(schemaToUseForPDFId!=''){
      
        String type='Account_Plan_PDF_Data__c';      
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType pdfDataSchema = schemaMap.get(type);
        Map<String, Schema.SObjectField> fieldMap = pdfDataSchema.getDescribe().fields.getMap();//is this used???required???
      
        Account_Plan_PDF_Data__c schemaToUseForPDF_Local = [SELECT Name, AccountPlanPDFCustInfo__c, AccountPlanPDFClientInformation__c,AccountPlanPDFContacts__c, AccountPlanPDFAccountTeam__c,
                                                           AccountPlanPDFCompetitors__c, AccountPlanPDFCurrentProviders__c, AccountPlanPDFDashboard__c, AccountPlanPDFNotes__c, AccountPlanPDFObjectives__c,
                                                           AccountPlanPDFOpportunities__c, AccountPlanPDFPenetration__c, AccountPlanPDFRecentHistory__c, AccountPlanPDFRelationship__c,AccountPlanPDFRelationshipRadar__c,
                                                           AccountPlanPDFSWOTAnalysis__c,AccountPlanPDFCriticalSuccessFactors__c,
                                                           Show_Additional_Information__c,Show_Capex_in_Our_Domain__c,Show_Clients_Changes__c,
                                                           Show_Clients_Competitors__c,Show_Clients_Core_Business__c,Show_Clients_Objectives__c,
                                                           Show_Clients_Partners__c,Show_Clients_Strategy__c,Show_Experian_Annualised_Revenue__c,
                                                           Show_Open_in_our_Domain__c,Show_Recent_Relationship_Success__c,Show_Relationship_Lessons_Learned__c,
                                                           Show_Relationship_Obj_not_Achieved__c,Show_Summary_Recent_History__c, 
                                                           Show_Experian_Goals__c, Show_Experian_Strategy__c
                                                           FROM Account_Plan_PDF_Data__c WHERE id =:ApexPages.currentPage().getParameters().get('schemaToUseForPDFId') LIMIT 1];
        
        showExperianGoals              = schemaToUseForPDF_Local.Show_Experian_Goals__c;
        showExperianStrategy           = schemaToUseForPDF_Local.Show_Experian_Strategy__c;
        showClientsCoreBusiness        = schemaToUseForPDF_Local.Show_Clients_Core_Business__c;
        showCapexInOurDomain           = schemaToUseForPDF_Local.Show_Capex_in_Our_Domain__c;
        showOpexInOurDomain            = schemaToUseForPDF_Local.Show_Open_in_our_Domain__c;
        showClientsStrategy            = schemaToUseForPDF_Local.Show_Clients_Strategy__c;
        showClientsObjectives          = schemaToUseForPDF_Local.Show_Clients_Objectives__c;
        showClientsChanges             = schemaToUseForPDF_Local.Show_Clients_Changes__c;
        showClientsCompetitors         = schemaToUseForPDF_Local.Show_Clients_Competitors__c;
        showClientsPartners            = schemaToUseForPDF_Local.Show_Clients_Partners__c;
        showExperianAnnualisedRevenue  = schemaToUseForPDF_Local.Show_Experian_Annualised_Revenue__c;
        showSummaryRecentHistory       = schemaToUseForPDF_Local.Show_Summary_Recent_History__c;
        showRecentRelationshipSuccess  = schemaToUseForPDF_Local.Show_Recent_Relationship_Success__c;
        showRelationshipObjNotAchieved = schemaToUseForPDF_Local.Show_Relationship_Obj_not_Achieved__c;
        showRelationshipLessonsLearned = schemaToUseForPDF_Local.Show_Relationship_Lessons_Learned__c;
        showAdditionalInformation      = schemaToUseForPDF_Local.Show_Additional_Information__c;        
        
        
        for(String fieldValue : validFieldsForList){
          Integer displayOrder = (Integer)(Decimal)schemaToUseForPDF_Local.get(fieldValue);
          if(displayOrder > 0.0){       
              selectedSections_Class selectedSection = new selectedSections_Class();
              selectedSection.order                  = displayOrder;
              selectedSection.paramName              = fieldValue; 
              selectedSectionClass_List.add(selectedSection);
          }
        }
      

        Integer n=0;
        selectedSectionClass_List.sort();
        for(n=0;n<selectedSectionClass_List.size();n++){
          String componentForPDF= selectedSectionClass_List[n].paramName;
          pdfBuildInstructions_List.add(componentForPDF);
        }
      

      } else {
        //This replicates the original functionailty and is called when the original button is pressed from the Account Plan tab.
        showExperianGoals              = true;
        showExperianStrategy           = true;
        showClientsCoreBusiness        = true;
        showCapexInOurDomain           = true;
        showOpexInOurDomain            = true;
        showClientsStrategy            = true;
        showClientsObjectives          = true;
        showClientsChanges             = true;
        showClientsCompetitors         = true;
        showClientsPartners            = true;
        showExperianAnnualisedRevenue  = true;
        showSummaryRecentHistory       = true;
        showRecentRelationshipSuccess  = true;
        showRelationshipObjNotAchieved = true;
        showRelationshipLessonsLearned = true;
        showAdditionalInformation      = true;        
      
        //This will display all of the sections in the default order
        //When called from AccountPlanTabPanel, or users who have not saved a schema or for when after a schema is deleted and so none is selected.
        for(String componentForPDF: validFieldsForList){
          pdfBuildInstructions_List.add(componentForPDF);        
        }

      }
       
      return pdfBuildInstructions_List;
    }
  
    set;
  
  }     
  
  
  /*public List<SelectOption> getItems() {
    List<SelectOption> options = new List<SelectOption>();
    
    for(String userPDFfData : pdfBuildInstructions_List){
      options.add(new SelectOption(userPDFfData, userPDFfData));
    }
    return options;
  }
  

  /**
   This Constructor initialize/populate all the required data to generate the pdf.
   */
  global GenerateAccountPlanPDFController(ApexPages.StandardController stdController) {
    accountPlanObj = (Account_Plan__c)stdController.getRecord();
    
  }    
    
  
  //public void initController(Account_Plan__c accountPlanObj){

    //AccountPlanObjId=accountPlanObj.id;
    
  //}
   

  public Component.Apex.outputPanel getPDFSections(){            

    Component.Apex.outputPanel pdfOutputPanel = new Component.Apex.outputPanel();

    for(String componentName : pdfBuildInstructions_List){
    
      if(componentName==Label.ACCOUNT_PLAN_PDF_CUST_INFO){
        Component.c.AccountPlanPDFCustInfo custInfo = new Component.c.AccountPlanPDFCustInfo();
        
        //May need to be commented out in order to deploy
        custInfo.showExperianGoals1    = showExperianGoals;
        custInfo.showExperianStrategy1 = showExperianStrategy;
        //May need to be commented out in order to deploy
        
        pdfOutputPanel.childComponents.add(custInfo);
        
      } else if(componentName==Label.ACCOUNT_PLAN_PDF_OBJECTIVES){      
        Component.c.AccountPlanPDFObjectives objectives = new Component.c.AccountPlanPDFObjectives();
        objectives.accountPlan1=accountPlanObj;
        pdfOutputPanel.childComponents.add(objectives);
        
      } else if(componentName==Label.ACCOUNT_PLAN_PDF_CLIENT_INFORMATION){
        Component.c.AccountPlanPDFClientInformation clientInfo = new Component.c.AccountPlanPDFClientInformation();
        
        //May need to be commented out in order to deploy
        clientInfo.showClientsCoreBusiness1 = showClientsCoreBusiness;
        clientInfo.showCapexInOurDomain1 = showCapexInOurDomain;
        clientInfo.showOpexInOurDomain1 = showOpexInOurDomain;
        clientInfo.showClientsStrategy1 = showClientsStrategy;
        clientInfo.showClientsObjectives1 = showClientsObjectives;
        clientInfo.showClientsChanges1 = showClientsChanges;
        clientInfo.showClientsCompetitors1 = showClientsCompetitors;
        clientInfo.showClientsPartners1 = showClientsPartners;
        clientInfo.showExperianAnnualisedRevenue1 = showExperianAnnualisedRevenue;
        //May need to be commented out in order to deploy
        
        pdfOutputPanel.childComponents.add(clientInfo);
        
      } else if(componentName==Label.ACCOUNT_PLAN_PDF_RECENT_HISTORY){
        Component.c.AccountPlanPDFRecentHistory recentHistory = new Component.c.AccountPlanPDFRecentHistory();
        
        //May need to be commented out in order to deploy
        recentHistory.showSummaryrecentHistory1 = showSummaryRecentHistory;
        recentHistory.showRecentRelationshipSuccess1 = showRecentRelationshipSuccess;
        recentHistory.showRelationshipObjNotAchieved1 = showRelationshipObjNotAchieved;
        recentHistory.showRelationshipLessonsLearned1 = showRelationshipLessonsLearned;
        recentHistory.showAdditionalInformation1 = showAdditionalInformation;
        //May need to be commented out in order to deploy
        
        pdfOutputPanel.childComponents.add(recentHistory);
        
      } else if(componentName==Label.ACCOUNT_PLAN_PDF_PENETRATION){
        Component.c.AccountPlanPDFPenetration penetration = new Component.c.AccountPlanPDFPenetration();
        penetration.accountPlan1=accountPlanObj;
        pdfOutputPanel.childComponents.add(penetration);  
        
      } else if(componentName==Label.ACCOUNT_PLAN_PDF_RELATIONSHIP){
        Component.c.AccountPlanPDFRelationship relationship = new Component.c.AccountPlanPDFRelationship();
        relationship.accountPlan1=accountPlanObj;
        pdfOutputPanel.childComponents.add(relationship);
        
      } else if(componentName==Label.ACCOUNT_PLAN_PDF_RELATIONSHIP_RADAR){
        Component.c.AccountPlanPDFRelationshipRadar relationshipRadar = new Component.c.AccountPlanPDFRelationshipRadar();
        pdfOutputPanel.childComponents.add(relationshipRadar);
        
      } else if(componentName==Label.ACCOUNT_PLAN_PDF_DASHBOARD){
        Component.c.AccountPlanPDFDashboard dashboard = new Component.c.AccountPlanPDFDashboard();
        pdfOutputPanel.childComponents.add(dashboard);  
        
      } else if(componentName==Label.ACCOUNT_PLAN_PDF_COMPETITORS){
        Component.c.AccountPlanPDFCompetitors competitors = new Component.c.AccountPlanPDFCompetitors();
        pdfOutputPanel.childComponents.add(competitors);
        
      } else if(componentName==Label.ACCOUNT_PLAN_PDF_CURRENT_PROVIDERS){
        Component.c.AccountPlanPDFCurrentProviders currentProviders = new Component.c.AccountPlanPDFCurrentProviders();
        currentProviders.accountPlan1=accountPlanObj;
        pdfOutputPanel.childComponents.add(currentProviders);
        
      } else if(componentName==Label.ACCOUNT_PLAN_PDF_SWOT_ANALYSIS){
        Component.c.AccountPlanPDFSWOTAnalysis swotAnalysis = new Component.c.AccountPlanPDFSWOTAnalysis();
        pdfOutputPanel.childComponents.add(swotAnalysis);
        
      } else if(componentName==Label.ACCOUNT_PLAN_PDF_OPPORTUNITIES){
        Component.c.AccountPlanPDFOpportunities opportunities = new Component.c.AccountPlanPDFOpportunities();
        pdfOutputPanel.childComponents.add(opportunities);
        
      } else if(componentName==Label.ACCOUNT_PLAN_PDF_CRITICAL_SUCCESS_FACTORS){
        Component.c.AccountPlanPDFCriticalSuccessFactors criticalSuccessFactors = new Component.c.AccountPlanPDFCriticalSuccessFactors();
        pdfOutputPanel.childComponents.add(criticalSuccessFactors);
        
      } else if(componentName==Label.ACCOUNT_PLAN_PDF_NOTES){
        Component.c.AccountPlanPDFNotes notes = new Component.c.AccountPlanPDFNotes();
        pdfOutputPanel.childComponents.add(notes);
        
      } else if(componentName==Label.ACCOUNT_PLAN_PDF_ACCOUNT_TEAM){        
        Component.c.AccountPlanPDFAccountTeam accountTeam = new Component.c.AccountPlanPDFAccountTeam();
        pdfOutputPanel.childComponents.add(accountTeam);
        
      } else if(componentName==Label.ACCOUNT_PLAN_PDF_CONTACTS){
        Component.c.AccountPlanPDFContacts contacts = new Component.c.AccountPlanPDFContacts();
        pdfOutputPanel.childComponents.add(contacts);
      }
           
    }               

    return pdfOutputPanel;
    
  }
  
  /*Save pdf as an attachment to oppty confidential information*/
  public void saveAttachment(){
    PageReference pdfPage =  Page.GenerateAccountPlanPDF;
    pdfPage.getParameters().put('id',(String)accountPlanObj.id);       
    pdfPage.getParameters().put('mode','pdf'); 
    Attachment attachment = new Attachment();
    Blob body;
    try {
      body = pdfPage.getContentAsPDF();
    } 
    catch (VisualforceException e){
      body = Blob.valueOf('Test Text');
    } 
    attachment.Body = body;
    attachment.Name = String.valueOf('Account Plan.pdf');  //TODO : change the name of file.
    attachment.ParentId = accountPlanObj.id;
    insert attachment; 
  }
  
  
}