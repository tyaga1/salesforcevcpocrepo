/*=============================================================================
 * Experian
 * Name: BatchRegionalScorecardCalculated
 * Description: Case 01916186
                Runs through the calculated field metrics for Regional Scorecard.
                Has to be separate from other batch process.
 * Created Date: 23 May 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 *

 =============================================================================*/

public class BatchRegionalScorecardCalculated implements Database.Batchable<sObject>, Database.Stateful {

  public Id metricId;

  public List<Id> metricIdList;
  public Map<Id, Boolean> metricIdTypeMap;
  
  private List<String> errorsFound;

  public Database.Querylocator start (Database.Batchablecontext bc) {
    
    errorsFound = new List<String>();
    
    return Database.getQueryLocator([
      SELECT Id, Type__c, Metric_Calculation_Field_1__c,
        Metric_Calculation_Field_2__c, Calculation_Field_Operator__c, Master_Grouping__c,
        Aggregate_Type__c
      FROM Regional_Scorecard_Metric__c
      WHERE Id = :metricId
    ]);
  }

  public void execute (Database.BatchableContext bc, List<Regional_Scorecard_Metric__c> scope) {

    // First, retrieve the primary master grouping (which is built from the number of sales users)
    List<Regional_Scorecard_Data__c> primaryDatasetList = [
      SELECT Id, Name__c, Type__c
      FROM Regional_Scorecard_Data__c
      WHERE Regional_Scorecard_Metric__r.Master_Grouping__c = true
    ];

    Map<String, Decimal> rsDatasetMap = new Map<String, Decimal>();
    String key;

    for (Regional_Scorecard_Data__c prsd : primaryDatasetList) {
      key = prsd.Type__c + RegionalScorecardUtils.KEY_SEPARATOR + prsd.Name__c;
      rsDatasetMap.put(key, 0.0);
    }

    for (Regional_Scorecard_Metric__c currentMetric : scope) {
      if (String.isNotBlank(currentMetric.Calculation_Field_Operator__c)) {
        // these should only run after the parent queries have run

        for (Id fieldRef : new Id[]{currentMetric.Metric_Calculation_Field_1__c, currentMetric.Metric_Calculation_Field_2__c}) {

          List<Regional_Scorecard_Data__c> rsDataset1 = [
            SELECT Metric_ID__c, Type__c, Regional_Scorecard_Metric__c,
              Metric_Type__c, Value_Count__c, Value_Currency__c, Value_Percentage__c
            FROM Regional_Scorecard_Data__c
            WHERE Regional_Scorecard_Metric__c = :fieldRef
          ];

          for (Regional_Scorecard_Data__c rsd : rsDataset1) {
            // The dataset name is pulled from each data entry and the metric id used (minus the metric id at the start)
            String dsName = rsd.Metric_ID__c.mid(19, 255);

            // Extracts the value based on the metric type.
            Decimal dsVal = (rsd.Metric_Type__c.equals('Count') ? rsd.Value_Count__c :
                            (rsd.Metric_Type__c.equals('Currency') ? rsd.Value_Currency__c :
                            (rsd.Metric_Type__c.equals('Percentage') ? rsd.Value_Percentage__c :
                            (rsd.Metric_Type__c.equals('Decimal') ? rsd.Value_Decimal__c : 0.0))));

            // Populate the dataset map on the first round...
            if (fieldRef.equals(currentMetric.Metric_Calculation_Field_1__c) && rsDatasetMap.containsKey(dsName)) {
              rsDatasetMap.put(dsName, dsVal);
            }

            if (fieldRef.equals(currentMetric.Metric_Calculation_Field_2__c)) {
              // second round, check the entry is in the map, otherwise we cannot perform the operation required.
              if (rsDatasetMap.containsKey(dsName)) {
                // perform the calc and reset the map key/val
                Decimal tVal = rsDatasetMap.get(dsName);

                if (currentMetric.Calculation_Field_Operator__c.equals('+')) {
                  rsDatasetMap.put(dsName, tVal + dsVal);
                }
                if (currentMetric.Calculation_Field_Operator__c.equals('-')) {
                  rsDatasetMap.put(dsName, tVal - dsVal);
                }
                // Multiply/Divide doesn't work with a zero.
                if (currentMetric.Calculation_Field_Operator__c.equals('*') || currentMetric.Calculation_Field_Operator__c.equals('/')) {
                  if (dsVal != 0.0 && tVal != 0.0) {
                    if (currentMetric.Calculation_Field_Operator__c.equals('*')) {
                      rsDatasetMap.put(dsName, tVal * dsVal);
                    }
                    if (currentMetric.Calculation_Field_Operator__c.equals('/')) {
                      Decimal calcVal = tVal.divide(dsVal, 5);
                      if (currentMetric.Type__c.equals('Percentage')) {
                        calcVal = calcVal * 100; // Percentages must be multiplied by 100 to store correctly (otherwise they are between 0.0 and 1.0)
                      }
                      rsDatasetMap.put(dsName, calcVal);
                    }
                  }
                  else {
                    // If there's a zero, the result is zero, so don't actually try the calculation.
                    rsDatasetMap.put(dsName,0.0);
                  }
                }
              }
              else {
                rsDatasetMap.put(dsName, 0.0);
              }
            }
          }
        }

        List<Regional_Scorecard_Data__c> dataToUpsert = new List<Regional_Scorecard_Data__c> ();

        for (String rsName : rsDatasetMap.keySet()) {
          // here we have the calculated data, so lets start building the upserts....
          dataToUpsert.add(
            RegionalScorecardUtils.buildDataRecord(
              currentMetric, 
              rsDatasetMap.get(rsName), 
              rsName.substringAfter(RegionalScorecardUtils.KEY_SEPARATOR), 
              rsName.substringBefore(RegionalScorecardUtils.KEY_SEPARATOR)
            )
          );
        }
        try {
          // Upsert based on the metric id, helps only keep 1 for each metric/type/name combo
          upsert dataToUpsert Metric_ID__c;

          // Finally mark the last run date.
          Regional_Scorecard_Metric__c rsm = new Regional_Scorecard_Metric__c(
            Id = currentMetric.Id,
            Last_Ran__c = system.now()
          );
          update rsm;
        }
        catch (DMLException ex) {
          apexLogHandler.createLogAndSave('BatchRegionalScorecardCalculated','execute', ex.getStackTraceString(), ex);
          errorsFound.add(ex.getMessage());
        }
        catch (Exception e) {
          errorsFound.add(e.getMessage());
        }
      }
    }
  }

  public void finish (Database.BatchableContext bc) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchRegionalScorecardCalculated', false);
    if (!errorsFound.isEmpty() || Test.isRunningTest()) {
      bh.emailBody += 'The following errors were found while processing.\n\n';
      for (String err : errorsFound) {
        bh.emailBody += err + '\n';
      }
    }
    bh.sendEmail();

    // If any more ids are in the metricIdList, prepare to run the next batch, chaining up each time.
    if (!Test.isRunningTest() && metricIdList != null) {
      RegionalScorecardUtils.runBatch(metricIdList, metricIdTypeMap);
    }
  }

}