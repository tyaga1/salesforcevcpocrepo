/******************************************************************************
 * Name: TestClassforexpUserAssignPermission.cls
 * Created Date: 5/12/2017
 * Created By: Charlie Park, UCInnovation
 * Description : Test class for expcommUserRemovePermissionLicense
 * Change Log- 
 ****************************************************************************/
 
 @isTest
private class TestClassforexpUserAssignPermission {
  
  @isTest static void myUnitTest() {

//Standard Platform User
        
        Profile getProfile = [SELECT Id FROM Profile where name ='Standard Platform User' limit 1]; 
        Profile stdProfile = [SELECT Id FROM Profile where name ='Standard User' limit 1]; 
        User newUser = new User(Alias = 'uniq', Email='sampleuser.testuser@experian.com', FirstName='Unique2 Test',
        EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileID = getProfile.Id, phone = '555555555',
        TimeZoneSidKey='America/Los_Angeles', UserName='sampleUser2.test@experian.Test.com', Department ='Test');
        insert newUser; 
        
        //PermissionSet permissionSet = [SELECT id, Label FROM PermissionSet where Name = 'Global Experian Employee Case Access' LIMIT 1];

        //PermissionSet permissionSet = new PermissionSet(Name='Test', Label='Global Experian Employee Case Access');
        //insert permissionSet;

        //PermissionSetAssignment permSetAssignment = new PermissionSetAssignment(AssigneeId = newUser.Id, PermissionSetId = permissionSet.Id);
        //insert permSetAssignment;

        Test.startTest();
            List<String> newUserList = new List<String>();
            newUserList.add(String.valueOf(newUser.Id));

            expcommUserAssignPermission.assignPermissionLicense(newUserList);

        Test.stopTest();
    }
}