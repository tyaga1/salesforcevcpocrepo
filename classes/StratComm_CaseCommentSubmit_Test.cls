@isTest
private class StratComm_CaseCommentSubmit_Test {
    
    @isTest static void saveCommentTest() {
        Account testAccount1 = Test_Utils.createAccount();
        insert testAccount1;
 
        Case c1 = new Case(
                        AccountId = testAccount1.Id,
                        Subject = 'Test Case 1',
                        Description = 'Test Description 1',
                        status = 'Closed-Complete');

        insert c1;
        
        Attachment testAttachment = new Attachment();
    testAttachment.ParentId = c1.id;
    testAttachment.Name = 'test';
    testAttachment.Body = Blob.valueof('test');
    //insert testAttachment;

        StratComm_CaseCommentSubmit_Controller.saveComment('testcomment', c1.Id);
        StratComm_CaseCommentSubmit_Controller.checkIfCaseIsClosed(c1.Id);
        StratComm_CaseCommentSubmit_Controller.saveAttachment(c1.Id,'test','test','txt');
    }
    
    
    
}