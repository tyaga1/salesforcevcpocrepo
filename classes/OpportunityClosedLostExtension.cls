/**=====================================================================
 * Appirio, Inc
 * Name: OpportunityClosedLostExtension
 * Description: 
 * Created Date: Oct 23rd, 2013
 * Created By: Mohammed Irfan (Appirio)
 * 
 * Date Modified        Modified By                  Description of the update
 * Jan 30th, 2014       Jinesh Goyal(Appirio)        T-232760: Homogenize the comments
 * Feb 9th, 2014        Nathalie Le Guay (Appirio)   Adding Competitor__c fields and making compRec a controller variable
 * Feb 13th, 2014       Jinesh Goyal(Appirio)        T-232763: Added Exception Logging
 * Feb 17th, 2014       Jinesh Goyal(Appirio)        T-248998, T-249059: Added No Decision Logic and updated picklist logic
 *                      Ceri Jones (Appirio)
 * Feb 24th, 2014       Nathalie Le Guay (Appirio)   Moved code to set isNoDecisionType at the beginning of constructor
 * Mar 04th, 2014       Arpita Bose (Appirio)        T-243282: Added Constants in place of String
 * Apr 08th, 2014       Arpita Bose                  T-269372: Added addError()in try-catch block
 * Aug 07th, 2014       Naresh kr Ojha               T-289258: Added try catch to show DMLExceptions for methods createComp() and saveRecord( )
 * Sep 24th, 2015       Noopur                       I-181870 : Replaced the getLabel() method with getValue() before adding to 
                                                     selectoptions in the method getPrimaryReasons()
 * Oct 8th, 2015        Paul Kissick                 I-184191: Display errors when trying to close
 * Nov 17th, 2015       Paul Kissick                 Case 01122944 - Fix for triggers not being fired properly.
 * Apr 1st, 2016        Sarah Barber                 Case 01661353 - Add new field and make win Back info required 
 * Apr 26th, 2016       Sarah Barber                 Case 01951938 - Added Contract_End_Date__c to display on the page
 * Jun 26th, 2017       James Wills                  Case 02255019 - Removed functionality associated with No Decision button and transferred picklist options to Closed Lost button. 
 * Aug 4th, 2017        Tyaga Pati                   Case 13584540  - Added new value "Experian Strategic Decision" to existing no_decission list for exception from validations. also fixed pick list value "Abandon Stalled Project" (Caps P)
 =====================================================================*/

public class OpportunityClosedLostExtension {
    
  public Opportunity opp {set;get;}
  public ApexPages.StandardController sController {get;set;}
  public String selectedPrimaryReason             {get;set;}
  public String selectedCompetitor                {get;set;}
  public Global_Settings__c custSetting           {get;set;}
  public Competitor__c compRec                    {get;set;}
  private String typeOpp;
  public Boolean isNoDecisionType                 {get;set;}
  public Boolean isOtherSelectAsPRC               {get;set;}
  public String selectedNoDecisionReason          {get;set;}
  public Map<id,Competitor__c> mapComp            {get;set;}
  
  public Boolean oppIsClosedForVFPage             {get;set;}

  public Set<String> noDecisionValues_Set = new Set<String>{Constants.OPPTY_PRIMARY_CLOSED_REASON_ASPROJECT, Constants.OPPTY_PRIMARY_CLOSED_REASON_EXPERION_NO_GO_DECISION,
                                                            Constants.OPPTY_PRIMARY_CLOSED_REASON_CUSTOMER_UNDECIDED, Constants.OPPTY_PRIMARY_CLOSED_REASON_NO_BUDGET,
                                                            Constants.OPPTY_PRIMARY_CLOSED_REASON_EXP_STRATEGIC_DECISION};//Case 02255019 


  //Constructor
  public OpportunityClosedLostExtension (ApexPages.StandardController stdController) {
    sController = stdController;
    isNoDecisionType = false;

    custSetting = Global_Settings__c.getValues (Constants.GLOBAL_SETTING);
    //Id oppId = (Id)( (Opportunity)stdController.getRecord ()).id;
    Id oppId = sController.getId();
    this.opp = [select Name,StageName,Primary_Reason_W_L__c,isClosed, isWon, Other_Closed_Reason__c, Lost_To__c, Contract_End_Date__c, Win_Back_Date__c, Win_Back_Date_Unknown__c, Primary_Winning_Competitor__c from Opportunity where id=:oppId];

    //Check if the Opportunity is already Closed/Lost
    if (opp.StageName.equalsIgnoreCase (custSetting.Opp_Closed_Lost_Stagename__c) || opp.isClosed) {
      ApexPages.addMessage (new ApexPages.Message (ApexPages.Severity.ERROR, Label.OCL_Message_Closed_Opportunity));
    }  

    typeOpp = ApexPages.currentPage().getParameters().get('type'); 
    /*if (typeOpp != null && typeOpp.equalsIgnoreCase ('nodecision')) {
      isNoDecisionType = true;
      if (opp != null) opp.StageName = Constants.OPPTY_STAGE_NO_DECISION;
    }*///Case 02255019
    /*
    else if (typeOpp == null){
      if (opp != null) opp.StageName = custSetting.Opp_Closed_Lost_Stagename__c;
    }
    */
    this.compRec = new Competitor__c();
    isOtherSelectAsPRC = false;
    selectedNoDecisionReason = '';      
    

    if(opp.StageName==custSetting.Opp_Closed_Lost_Stagename__c){
      oppIsClosedForVFPage = true;
    }
    
    
  }

  //Retrieve Opportunity Competitors and generate SelectOption list.
  public List<SelectOption> getCompetitors() {
    List<SelectOption> options = new List<SelectOption>();
    options.add (new SelectOption ('','--'+Label.OCL_Option_Select+'--'));
    mapComp = new Map<id, Competitor__c>([
      SELECT Id, Opportunity__c, Account__r.Name 
      FROM Competitor__c 
      WHERE Opportunity__c = :opp.id
    ]);
    for (Competitor__c compRec: mapComp.values()) {
      if (!String.isEmpty(compRec.Account__r.Name)) {
        options.add (new SelectOption(compRec.Id,compRec.Account__r.Name));
      }
    }
        
    if (options.size() < 2) {
      options.clear();
      options.add (new SelectOption ('','--'+Label.OCL_Option_None+'--'));
    }
    return options;
  }

  //Associate new Competitor with Opportunity
  public pagereference createComp(){
    system.Debug('###'+compRec);
    // Add a return if no account has been entered!
    if (compRec.Account__c == null) {
      return null;
    }
    compRec.Opportunity__c = opp.Id;
    try {
      insert compRec;
    }
    catch (DMLException e) {
      ApexLogHandler.createLogAndSave ('OpportunityClosedLostExtension', 'createComp', e.getStackTraceString(), e);
      for (Integer i = 0; i < e.getNumDml(); i++) {
        compRec.addError(e.getDmlMessage(i));
      }
    }
    compRec = new Competitor__c();
    return null;
  }
    
  //Retrieve Opportunity Primary Reason Picklist values and generate SelectOption list.
  public List<SelectOption> getPrimaryReasons() {
    system.debug ('\n[OpportunityClosedLostExtension : getPrimaryReasons]: ' + Opportunity.sObjectType.getDescribe().fields.getMap());
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('','--'+Label.OCL_Option_Select+'--'));   
    
    for (Schema.PicklistEntry pe:
         Opportunity.sObjectType.getDescribe().fields.getMap().get('Primary_Reason_W_L__c').getDescribe().getPicklistValues()) {
      /*if (isNoDecisionType) {
        if (pe.getValue().equalsIgnoreCase (Constants.OPPTY_PRIMARY_CLOSED_REASON_ASPROJECT)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_EXPERION_NO_GO_DECISION)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_CUSTOMER_UNDECIDED)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_NO_BUDGET)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_OTHER)){
          options.add (new SelectOption(pe.getValue(),pe.getLabel()));
        }
      } else {*///Case 02255019
        if (pe.getValue().equalsIgnoreCase (Constants.OPPTY_PRIMARY_CLOSED_REASON_ASPROJECT)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_EXPERION_NO_GO_DECISION)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_CUSTOMER_UNDECIDED)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_NO_BUDGET)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_INADEQUATE_DATA_QUALITY)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_INADEQUATE_DATA_RANGE)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_INADEQUATE_DEL_CAP)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_LEGAL_REST)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_PRICE_TOO_HIGH)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_INADEQUATE_FUNCTIONALITY)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_INADEQUATE_TERMS)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_INADEQUATE_CLI_REL)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_OTHER)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_INADEQUATE_COMP_CAP)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_EXP_STRATEGIC_DECISION)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_INADEQUATE_SERVICE_REC)
            || pe.getValue().equalsIgnoreCase(Constants.OPPTY_PRIMARY_CLOSED_REASON_OTHER)) {
          options.add (new SelectOption (pe.getValue(),pe.getLabel()));
        }
      //}
    }

    if (options.size() < 1) {
      options.clear();
      options.add (new SelectOption ('','--'+Label.OCL_Option_None+'--'));
    }
    return options;
  }

  //PrimaryReason Onchange handler
  public pagereference primaryReasonChanged() {
    opp.Primary_Reason_W_L__c = selectedPrimaryReason;
    isOtherSelectAsPRC = (selectedPrimaryReason != null && selectedPrimaryReason.equalsIgnoreCase (Constants.OPPTY_PRIMARY_CLOSED_REASON_OTHER));
    //  if (selectedPrimaryReason != null && selectedPrimaryReason.equalsIgnoreCase (Constants.OPPTY_PRIMARY_CLOSED_REASON_OTHER)) isOtherSelectAsPRC = true;
    //  else isOtherSelectAsPRC = false;
    return null;
  }
    
  //Save button handler
  public pagereference saveRecord() {
    //Primary Reason is required.
    if (opp.Primary_Reason_W_L__c==null || opp.Primary_Reason_W_L__c.equals('')) {
      ApexPages.addMessage (new ApexPages.Message (ApexPages.Severity.ERROR, Label.OCL_Message_Primary_Reason_required));
      return null;
    }
    
    //Win back info required.
    if ((opp.Primary_Reason_W_L__c==null || !noDecisionValues_Set.contains(opp.Primary_Reason_W_L__c)) && opp.Win_Back_Date_Unknown__c==false && opp.Win_Back_Date__c==null){ //&& isNoDecisionType == False) {//Case 02255019
      ApexPages.addMessage (new ApexPages.Message (ApexPages.Severity.ERROR, Label.OCL_Win_Back_Date_Info_Required));
      return null;
    }
        
    //If Primary Closed Reason is 'Other'
    //Then Other_Closed_Reason is required 
    if (opp.Primary_Reason_W_L__c.equalsIgnoreCase('Other') && (opp.Other_Closed_Reason__c == null || opp.Other_Closed_Reason__c.equals (''))) {
      ApexPages.addMessage (new ApexPages.Message (ApexPages.Severity.ERROR, Label.OCL_Message_Other_Closed_Reason_required));
      return null;
    }
    system.debug ('#######################'+opp.Primary_Winning_Competitor__c);
    system.debug ('#######################'+mapComp);
    
   
    //Competitor is required in order to Save.       
    if ((opp.Primary_Reason_W_L__c==null || !noDecisionValues_Set.contains(opp.Primary_Reason_W_L__c)) && opp.Primary_Winning_Competitor__c == null){ //&& isNoDecisionType == false) {//Case 02255019
      ApexPages.addMessage (new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCL_Message_Competitor_Name_required));
      return null;
    }
    //else if(isNoDecisionType == false) {
      if(opp.Primary_Winning_Competitor__c!=null){//Case 02255019 
        CompRec = mapComp.get(opp.Primary_Winning_Competitor__c);
        if (opp.Win_Back_Date__c != null){
          CompRec.Win_back_date__c = opp.Win_back_Date__c;
        }
        CompRec.lost_to__c = true;
        opp.Primary_Winning_Competitor__c = CompRec.Account__r.Name;
      
        try {
          update CompRec;
          // PK 01122944 - Note, there's a rollup from Competitor to Opportunity, and this is causing the before/after update
          // Triggers to fire, so we need to reset the trigger state variables so we can fire them again properly. 
        } 
        catch (DMLException e) {
          ApexLogHandler.createLogAndSave ('OpportunityClosedLostExtension','saveRecord', e.getStackTraceString(), e);
          for (Integer i = 0; i < e.getNumDml(); i++) {
            CompRec.addError(e.getDmlMessage(i));
          }
          return null; // I-184191 : Don't complete and return to page
        }
      }//Case 02255019 
    //}
    
    // PK: 01122944 - Fix for triggers not being fired properly.
    OpportunityTriggerHandler.isAfterUpdateTriggerExecuted = false;
    OpportunityTriggerHandler.isBeforeUpdateTriggerExecuted = false;
    OpportunityTriggerHelper.oppTriggerHasAlreadyRun = false;

    //Update Opportunity.
    if (isNoDecisionType == true) {
      opp.StageName = Constants.OPPTY_STAGE_NO_DECISION;
    }
    else {
      opp.StageName = custSetting.Opp_Closed_Lost_Stagename__c; //'Closed Lost';
      //opp.Requested_Stage__c = custSetting.Opp_Closed_Lost_Stagename__c;
    }
    try {
      update opp;
    } 
    catch (Dmlexception ex) {
      ApexLogHandler.createLogAndSave ('OpportunityClosedLostExtension','saveRecord', ex.getStackTraceString(), ex);
      for (Integer i = 0; i < ex.getNumDml(); i++) {
        opp.addError(ex.getDmlMessage(i));
      }
      return null; // I-184191 : Don't complete and return to page
    }
        
    //Opp_Closed_Lost_Stagename 
    system.debug ('#######################'+opp); 
    system.debug ('#######################'+CompRec);       
    return new PageReference ('/'+opp.id);
  }

}