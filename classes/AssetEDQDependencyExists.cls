/**=====================================================================
 * Experian
 * Name: AssetEDQDependencyExists
 * Description: The following batch class is designed to be scheduled to run every day.
                    This class will get all Contacts with an EDQ Dependency
 * Created Date: 6/17/2015 
 * Created By: Diego Olarte (Experian)
 *
 * Date Modified                Modified By                  Description of the update
 * Sep 14th, 2015               Paul Kissick                 Adding support for testing
 * Nov 9th, 2015                Paul Kissick                 Case 01266075: Optimisations to improve query load
 =====================================================================*/
global class AssetEDQDependencyExists implements Database.Batchable<sObject> {

  global Database.Querylocator start ( Database.BatchableContext bc ) {
    return Database.getQueryLocator ([
      SELECT Id, AccountId
      FROM Asset
      WHERE Status__c in ('Live','Scheduled')
      AND AccountId != null
      AND (Order_Owner_BU__c LIKE '%Data Quality%' OR Product_Business_Line__c LIKE '%Data Quality%' OR SaaS__c = true)
      AND Account.EDQ_Dependency_Exists__c = false
    ]);
  }

  global void execute (Database.BatchableContext bc, List<Asset> scope) {
    Set<Id> accIdSet = new Set<Id>();
    
    List<Account> accountsList = new List<Account>();
    
    for(Asset ast : scope) {
      accIdSet.add(ast.AccountId);
    }
    
    for(Id accId : accIdSet) {
      accountsList.add(
        new Account(
          Id = accId, 
          EDQ_Dependency_Exists__c = true
        )
      );
    }

    if(accountsList != null && accountsList.size() > 0) {
      try {             
        update accountsList;
      }
      catch (DMLException ex) {
        apexLogHandler.createLogAndSave('AssetEDQDependencyExists','execute', ex.getStackTraceString(), ex);
      }
    }

  }

  //To process things after finishing batch
  global void finish (Database.BatchableContext bc) {
    
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'AssetEDQDependencyExists', true);
    
    if (!Test.isRunningTest()) {
      system.scheduleBatch(new OrderEDQDependencyExists(),'Order - EDQ Dependency Exists '+String.valueOf(Datetime.now().getTime()),2,ScopeSizeUtility.getScopeSizeForClass('OrderEDQDependencyExists'));
    }

  }

}