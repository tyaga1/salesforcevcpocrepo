/*=============================================================================
 * Experian
 * Name: RegionalScorecardExportController
 * Description: Case 01916186
                Produces a csv containing all the data for the regional scorecard.
 * Created Date: 22 May 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 *

 =============================================================================*/

public class RegionalScorecardExportController {

  public RegionalScorecardExportController() {
  }

  public String getCurrentDateTime() {
    return Datetime.now().format('yyyyMMddHHmmss');
  }

  public List<String> metricLabels {
    get {
      if (metricLabels == null) {
        metricLabels = new List<String>();
      }
      return metricLabels;
    } set;
  }

  public List<String> metricValues {
    get {
      if (metricValues == null) {
        metricValues = new List<String>();
      }
      return metricValues;
    } set;
  }

  public Map<String, Map<String, String>> outputData {
    get {
      if (outputData == null) {
        outputData = new Map<String, Map<String, String>>();
      }
      return outputData;
    }
    set;
  }

  public PageReference loadData() {

    // Load all the data
    List<Regional_Scorecard_Data__c> rsDataList = [
      SELECT Value_Count__c, Value_Currency__c, Value_Percentage__c, Value_Decimal__c,
        Metric_Type__c, Type__c, Name__c, Label__c
      FROM Regional_Scorecard_Data__c
      WHERE Regional_Scorecard_Metric__r.Displayed_Field__c = true
      ORDER BY Regional_Scorecard_Metric__c, Type__c, Name__c
    ];

    // Prepare the master grouping data (used later)
    List<Regional_Scorecard_Data__c> rsDataPrimary = [
      SELECT Type__c, Name__c
      FROM Regional_Scorecard_Data__c
      WHERE Regional_Scorecard_Metric__r.Master_Grouping__c = true
      ORDER BY Type__c, Name__c
    ];

    // Prepare the output labels for each metric
    List<Regional_Scorecard_Metric__c> rsMetricList = [
      SELECT Label__c
      FROM Regional_Scorecard_Metric__c
      WHERE Displayed_Field__c = true
      ORDER BY Field_Reference__c
    ];

    metricLabels = new List<String>();
    metricValues = new List<String>();
    outputData = new Map<String, Map<String, String>>();

    Map<String, String> rawDataMap = new Map<String, String>();

    for (Regional_Scorecard_Metric__c rsm : rsMetricList) {
      metricLabels.add(rsm.Label__c);
      rawDataMap.put(rsm.Label__c, '0');
    }

    for (Regional_Scorecard_Data__c rsd : rsDataPrimary) {
      metricValues.add(getTypeNameString(rsd.Type__c, rsd.Name__c));
      outputData.put(getTypeNameString(rsd.Type__c, rsd.Name__c), new Map<String, String>(rawDataMap));
    }

    String metricLabel;
    String metricName;
    Decimal metricValue;
    String metricValStr;

    for (Regional_Scorecard_Data__c rsd : rsDataList) {
      metricLabel = rsd.Label__c;
      metricName = getTypeNameString(rsd.Type__c, rsd.Name__c);
      metricValue = (rsd.Metric_Type__c == 'Count' ? rsd.Value_Count__c :
                    (rsd.Metric_Type__c == 'Currency' ? rsd.Value_Currency__c :
                    (rsd.Metric_Type__c == 'Percentage' ? rsd.Value_Percentage__c :
                    (rsd.Metric_Type__c == 'Decimal' ? rsd.Value_Decimal__c : 0.0))));
      metricValStr = (rsd.Metric_Type__c == 'Count' ? String.valueOf(metricValue.intValue()) :
                    (rsd.Metric_Type__c == 'Currency' ? String.valueOf(metricValue.format()) :
                    (rsd.Metric_Type__c == 'Decimal' ? String.valueOf(metricValue.format()) :
                    (rsd.Metric_Type__c == 'Percentage' ? String.valueOf(metricValue.setScale(4))+'%' : '0'))));
      if (outputData.containsKey(metricName) && outputData.get(metricName).containsKey(metricLabel)) {
        outputData.get(metricName).put(metricLabel, metricValStr);
      }
    }

    return null;
  }

  private String getTypeNameString(String type1, String name1) {
    return '"' + type1 + '","' + name1 + '"';
  }


}