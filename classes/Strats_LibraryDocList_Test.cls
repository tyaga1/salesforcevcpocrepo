@isTest(SeeAllData=true)
private class Strats_LibraryDocList_Test {

     @isTest static void contentCreation() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {

            
            UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];

            Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
            
            User portalAccountOwner1 = new User(
                    UserRoleId = portalRole.Id,
                    ProfileId = profile1.Id,
                    Username = System.now().millisecond() + 'test2@test.com',
                    Alias = 'batman',
                    Email='bruce.wayne@experian.com',
                    EmailEncodingKey='UTF-8',
                    Lastname='Wayne',
                    LanguageLocaleKey='en_US',
                    LocaleSidKey='en_US',
                    TimeZoneSidKey='America/Chicago'
                    );
            Database.insert(portalAccountOwner1);
            
            Account portalAccount1 = new Account(
                      Name = 'TestAccount',                       
                      OwnerId = portalAccountOwner1.Id
                      );
                      Database.DMLOptions dml = new Database.DMLOptions();
dml.DuplicateRuleHeader.AllowSave = true; 
            Database.insert(portalAccount1, dml);
            
            Contact contact1 = new Contact(FirstName = 'Test',
                                    Lastname = 'McTesty',
                                    AccountId = portalAccount1.Id,
                                    Email = System.now().millisecond() + 'test@test.com'
                                    );
            Database.insert(contact1, dml);

            Profile communityProfile = [SELECT Id FROM Profile WHERE Name = 'Xperian Global Community Strategic' LIMIT 1];
            User usr = new User(LastName = 'LIVESTON',
                     FirstName='JASON',
                     Alias = 'jliv',
                     Email = 'jason.liveston@experian.com',
                     Username = 'jason.liveston@asdf.com',
                     ProfileId = communityProfile.id,
                     TimeZoneSidKey = 'GMT',
                     ContactId = contact1.Id,
                     CommunityNickname = 'test12345',
                     LanguageLocaleKey = 'en_US',
                     EmailEncodingKey = 'UTF-8',
                     LocaleSidKey = 'en_US'
                     );
            insert usr;
            
            ContentWorkSpace cws = new ContentWorkSpace( Name='New Library',
                                                       DeveloperName = 'New_Library'    
                                                    );
            insert cws;
            system.debug('The library Id: ' + cws.Id);
            
            
            ContentWorkSpacePermission cwsPerm = new ContentWorkspacePermission(Name = 'New_Library',
                                                                    Description = 'test',
                                                                    PermissionsAddContent = true,
                                                                    PermissionsManageWorkspace = true
                                                                      );
            insert cwsPerm;
                                                                      
        
            ContentWorkSpaceMember cwsMember = new ContentWorkspaceMember(ContentWorkSpaceId = cws.Id,
                                                                        MemberId = usr.Id,
                                                                        ContentWorkspacePermissionId = cwsPerm.Id
                                                                        );
            insert cwsMember;
        
            ContentVersion cv = new ContentVersion(
                                                  Title = 'Penguins',
                                                  PathOnClient = 'Penguins.txt',
                                                  VersionData = Blob.valueOf('Test Content'),
                                                  IsMajorVersion = true
                                                );
            insert cv;
            
            ContentVersion completeCV = [Select ContentDocumentId, FirstPublishLocationId From ContentVersion Where Id = :cv.Id];
            
        
            ContentWorkSpaceDoc cwsDoc = new ContentWorkSpaceDoc(ContentDocumentId = completeCV.ContentDocumentId,
                                                               ContentWorkSpaceId = cws.Id
                                                               
                                                              );
            insert cwsDoc;
            
            ContentVersion cvtest = [Select ContentDocumentId, FirstPublishLocationId From ContentVersion Where Id = :cv.id];
            system.debug('updated content: ' + cvtest.ContentDocumentId + ' ' + cvtest.FirstPublishLocationId );                                                              
                                                              
             ContentVersion cvupdated = new ContentVersion(Title = 'Penguins',
                                                  PathOnClient = 'Penguins.txt',
                                                  VersionData = Blob.valueOf('Test Content updated'),
                                                  ContentDocumentId = completeCV.ContentDocumentId,
                                                  IsMajorVersion = true );
             insert cvupdated;

             Strats_LibraryDocList.getLibraryDocs();
        }
    }
    
}