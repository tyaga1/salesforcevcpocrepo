/**
 * @copyright Topcoder INC
 * Developer : Topcoder
 * Version : 0.1
 * Date : 04-MAY-2014
 * This class contains unit tests for validating the behavior of Apex class RaderChartController
 * 
 * Modified By               Modified On                        Description
 * Jagjeet Singh(Appirio)     Sept 28th,2015                    T-433561 - Added a new test method.
 */
@isTest
private class AccountHealthRadarChartControllerTEST {
    // test method for validating the size of the contacts inserted.
    static testMethod void DataExistAndCreationTest() {
        Account_Plan__c aplan = new Account_Plan__c();
        insert aplan; // insert account plan
        ApexPages.currentPage().getParameters().put('id', aplan.id);
        Account_Plan_Contact__c accPlanContact = new Account_Plan_Contact__c(Account_Plan__c =aplan.Id,Include_in_Overall_Health_Status__c = TRUE );
        insert accPlanContact; // insert account plan contact
        Test.startTest();
        AccountHealthRadarChartController1 con1 = new AccountHealthRadarChartController1();
        System.assertEquals(con1.accPlanContactList.size(), 1); // one record inserted hence size should be 1 
        con1.changeChartSize();
        Test.stopTest();
        
    }
    // test method for validating the average.
    static testMethod void DataExistAndCreationTest2() {
        Account_Plan__c aplan = new Account_Plan__c();
        insert aplan; // insert account plan
        ApexPages.currentPage().getParameters().put('id', aplan.id);
        Account_Plan_Contact__c accPlanContact = new Account_Plan_Contact__c(Account_Plan__c =aplan.Id,Include_in_Overall_Health_Status__c = TRUE );
        accPlanContact.Value_Proposition_Experian_Strength__c = '4';
        accPlanContact.Value_Proposition_Importance_to_Contact__c = '4';
        accPlanContact.Value_Proposition_Overall_Competition_St__c = '4';
        accPlanContact.Business_Understanding_Experian_Strength__c = '4';
        accPlanContact.Business_Understanding_Importance_to_Con__c = '4';
        accPlanContact.Business_Understanding_Overall_Competiti__c = '4';
        accPlanContact.Responsiveness_Experian_Strength__c = '4';
        accPlanContact.Responsiveness_Importance_to_Contact__c = '4';
        accPlanContact.Responsiveness_Overall_Competition_Stren__c = '4';
        accPlanContact.Terms_Conditions_Experian_Strength__c = '4';
        accPlanContact.Terms_Conditions_Importance_to_Contact__c = '4';
        accPlanContact.Terms_Conditions_Overall_Competition_S__c = '4';
        accPlanContact.Quality_Experian_Strength__c = '4';
        accPlanContact.Quality_Importance_to_Contact__c = '4';
        accPlanContact.Quality_Overall_Competition_Strength__c = '4';
        accPlanContact.Customer_Care_Experian_Strength__c = '4';
        accPlanContact.Customer_Care_Importance_to_Contact__c = '4';
        accPlanContact.Customer_Care_Overall_Competition_Streng__c = '4';
        accPlanContact.Strategy_Alignment_Experian_Strength__c = '4';
        accPlanContact.Strategy_Alignment_Importance_to_Contact__c = '4';
        accPlanContact.Strategy_Alignment_Overall_Competition_S__c = '4';
        accPlanContact.Relationship_Experian_Strength__c = '4';
        accPlanContact.Relationship_Importance_to_Contact__c = '4';
        accPlanContact.Relationship_Overall_Competition_Strengt__c = '4';
        
        insert accPlanContact; // insert account plan contact
        Test.startTest();
        
        AccountHealthRadarChartController1 con1 = new AccountHealthRadarChartController1();
        List<AccountHealthRadarChartController1.RadarData> radarDatList;
        radarDatList = con1.data;
        system.assertEquals(radarDatList.Size(),8, 'Radar data should be created for all');
        System.assertEquals(con1.accPlanContactList.size(), 1); // one record inserted hence size should be 1 
        con1.changeChartSize();
        Test.stopTest();
        
    }
}