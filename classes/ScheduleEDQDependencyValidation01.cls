/**=====================================================================
 * Experian
 * Name: ScheduleEDQDependencyValidation01
 * Description: The following batch class is designed to be scheduled to run every day.
                    This class will get all Contacts with an EDQ Dependency
 * Created Date: 6/17/2015
 * Created By: Diego Olarte (Experian)
 *
 * Date Modified                Modified By                  Description of the update
 * Nov 9th, 2015                Paul Kissick                 Adding handling for scope size.
 =====================================================================*/
 
global class ScheduleEDQDependencyValidation01 implements Schedulable {
  
  global void execute(SchedulableContext ctx) {
   /* ContactEDQDependencyExists batchToProcess = new ContactEDQDependencyExists();
    database.executebatch(batchToProcess, ScopeSizeUtility.getScopeSizeForClass('ContactEDQDependencyExists'));*/
  }
  
}