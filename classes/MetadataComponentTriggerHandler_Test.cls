/**=====================================================================
 * Name: MetadataComponentTriggerHandler_Test
 * Trigger Name: MetadataComponentTrigger
 * Description: Test class for MetadataComponentTriggerHandler
 * Created Date: June 29th, 2015
 * Created By: Nur Azlini
 * 
 * Date Modified       Modified By                  Description of the update
 * Apr 7th, 2016       Paul Kissick                 Case 01932085: Fixing Test User Email Domain
 =====================================================================*/
@isTest
private class MetadataComponentTriggerHandler_Test {
  
  static testmethod void testCondition1(){

    // create User
    Profile p = [select id from profile where name=: Constants.PROFILE_SYS_ADMIN ];
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    insert testUser1;
    system.runAs(testUser1){
      IsDataAdmin__c isDateAdmin = Test_Utils.insertIsDataAdmin(false);
      //create metadata components
      Metadata_Component__c metaCom = Test_Utils.insertMetadataComponent();
      upsert metaCom;
		  
		  //Populate Object ApI Name
		  metaCom.Object_API_Name__c = 'Account';
		  update metaCom;
      metaCom = [SELECT Id, Name, Metadata_Member__c, Object_API_Name__c, Folder__c
                 FROM Metadata_Component__c
                 WHERE Id = : metaCom.id];
      system.assertEquals(metaCom.Name, metaCom.Metadata_Member__c, 'validate that Name is set as Metadata Member');
    }
  }

}