@isTest private without sharing class DB_Custom_Case
{
  @isTest (SeeAllData=true)
  private static void testTrigger()
  {
    CRMfusionDBR101.DB_Globals.triggersDisabled = true;
    sObject testData = CRMfusionDBR101.DB_TriggerHandler.createTestData( Case.getSObjectType() );
    
    Test.startTest();
    
    Profile p = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN];
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    insert testUser1;
    insert new IsDataAdmin__c(SetupOwnerId=testUser1.Id, IsDataAdmin__c=true);
    
    system.runAs(testUser1) {       

        insert testData;
        update testData;
        CRMfusionDBR101.DB_Globals.generateCustomTriggerException = true;
        update testData;
        CRMfusionDBR101.DB_Globals.generateCustomTriggerException = false;
        delete testData;
    }
    Test.stopTest();
  }
}