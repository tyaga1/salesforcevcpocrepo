/**=====================================================================
 * @copyright Topcoder INC
 * Developer : Topcoder
 * Version : 0.1
 * Date : 04-MAY-2014
 * This is to draw the chart , controller of the apex component
 * Modified On          Modified By                            Description
 * May 13th,2014        Naresh kr Ojha (Appirio)               Commented height and width members, as they are not used on vf.
 * Jul 16th,2014        Arpita Bose (Appirio)                  T-295436: Added a constructor with StandardController to display
 *                                                             Account_Plan_Contact__c records on the vf page
 * Jul 18th,2014        Arpita Bose (Appirio)                  T-295436: Modified method drawGraph() to include all the contacts
 * Oct 15th,2014        Pratibha Chhimpa (Appirio)             T-325542: Added criteria Include_in_Overall_Health_Status__c = True for task 
 * Jul 8th, 2015        Paul Kissick                           Case #01004274 : Fix for divide by 0 on graph data, and formatting!
 * Sept 7th,2015        Jagjeet Singh(Appirio)                 T-432470, Extracting additional fields in  query.
 * Sept 13th,2015       Jagjeet Singh(Appirio)                 T-433561, Updated the draGraph() method to exclude the NUll Contact Scores.
 * May 23rd, 2016       James Wills                            Case: 01966786 AP - Contacts tab - Diagram Formatting. Removed carriage return from 'Strategy Alignment'*/
public class AccountHealthRadarChartController1 {
  public List<RadarData> data {get;set;} // Inner class list will be used to create chart
  public LIST<Account_Plan_Contact__c> accPlanContactList {get;set;} // will be used to show the contacts
  //public Integer height {get;set;} {height = 400;} // will be used to change the height of the graph based on user input
  //public Integer width {get;set;} {width = 900;} // will be used to change the width of the graph based on user input
  public String importanceToContactFill {get;set;} {importanceToContactFill = 'red';}
  public String experianStrengthFill {get;set;} {experianStrengthFill = 'green';}
  public String overallCompetitionStrengthFill {get;set;} {overallCompetitionStrengthFill = 'blue';}
  public String accountPlanId {get;set;}
  public Account_Plan__c accountPlan{get;set;}
  public Attachment radarChartImage {get;set;}
  public ApexPages.StandardSetController aplanResults {get;set;}
  /**
   * Constructor of the class, will fetch the data and initialise the graph data
   **/
  public AccountHealthRadarChartController1(ApexPages.StandardController controller) {
    initController();
  }
     
  public AccountHealthRadarChartController1() {
    initController();
  }
    
  public void initController() {
    accountPlanId = ApexPages.currentPage().getParameters().get('id') ;
    accountPlan = [Select Name from Account_Plan__c where id=:accountPlanId];
    for(Attachment radarChart : [Select Id From Attachment Where ParentId = :accountPlanId and Name = 'RadarChart']){
      radarChartImage = radarChart;
    }        
    drawGraph();
  }
  public PageReference backToPlan(){
    PageReference pr = new PageReference('/' + accountPlanId);
    pr.setRedirect(true);
    return pr;
  }
        
  /**
   ** This method will draw the graph based on the Account Plan Contact
   **/
  public void drawGraph(){
    accPlanContactList  = [SELECT Value_Proposition_Overall_Competition_St__c, Value_Proposition_Importance_to_Contact__c, 
                                  Value_Proposition_Experian_Strength__c, Terms_Conditions_Overall_Competition_S__c, 
                                  Terms_Conditions_Importance_to_Contact__c, Terms_Conditions_Experian_Strength__c, 
                                  Strategy_Alignment_Overall_Competition_S__c, Strategy_Alignment_Importance_to_Contact__c, 
                                  Strategy_Alignment_Experian_Strength__c, Responsiveness_Overall_Competition_Stren__c, 
                                  Responsiveness_Importance_to_Contact__c, Responsiveness_Experian_Strength__c, 
                                  Relationship_Overall_Competition_Strengt__c, Relationship_Importance_to_Contact__c, 
                                  Relationship_Experian_Strength__c, Quality_Overall_Competition_Strength__c, 
                                  Quality_Importance_to_Contact__c, Quality_Experian_Strength__c, 
                                  Customer_Care_Overall_Competition_Streng__c,Experian_Relationship__c,LastModifiedDate,
                                  Customer_Care_Importance_to_Contact__c, Customer_Care_Experian_Strength__c, 
                                  Contact_Name__c, Business_Understanding_Overall_Competiti__c, Include_in_Overall_Health_Status__c,
                                  Business_Understanding_Importance_to_Con__c, Business_Understanding_Experian_Strength__c,
                                  Scores_Validated_By_Interview__c,Last_Interview_Date__c,Importance_To_Contact_Notes__c,
                                  Experian_Strength_Notes__c,Competitor_Strength_Notes__c
                           FROM Account_Plan_Contact__c 
                           WHERE Account_Plan__c =: accountPlanId
    ];
    if(accPlanContactList <>NULL && accPlanContactList.size()>0){
      data = new List<RadarData>();
      Decimal VPexpStrength          = 0;        
      Decimal VPcontactImportance    = 0;  
      Decimal VPoverAllStrength      = 0;  
         
      Decimal BUexpStrength          = 0;      
      Decimal BUcontactImportance    = 0; 
      Decimal BUoverAllStrength      = 0;  
             
      Decimal RESPexpStrength        = 0;  
      Decimal RESPcontactImportance  = 0;
      Decimal RESPoverAllStrength    = 0;
      
      Decimal TCexpStrength          = 0;     
      Decimal TCcontactImportance    = 0;  
      Decimal TCoverAllStrength      = 0; 
      
      Decimal QexpStrength           = 0;
      Decimal QcontactImportance     = 0;
      Decimal QoverAllStrength       = 0;
      
      Decimal CCexpStrength          = 0;     
      Decimal CCcontactImportance    = 0; 
      Decimal CCoverAllStrength      = 0;  
      
      Decimal SAexpStrength          = 0;    
      Decimal SAcontactImportance    = 0;  
      Decimal SAoverAllStrength      = 0; 
      
      Decimal RELexpStrength         = 0;   
      Decimal RELcontactImportance   = 0;
      Decimal RELoverAllStrength     = 0;
        
      Decimal processedVPexpStrengthCount = 0;
      Decimal processedVPcontactImportanceCount = 0;
      Decimal processedVPoverAllStrengthCount = 0;
        
      Decimal processedBUexpStrengthCount = 0;
      Decimal processedBUcontactImportanceCount = 0;
      Decimal processedBUoverAllStrengthCount = 0;
        
      Decimal processedRESPexpStrengthCount = 0;
      Decimal processedRESPcontactImportanceCount = 0;
      Decimal processedRESPoverAllStrengthCount = 0;
        
      Decimal processedTCexpStrengthCount = 0;
      Decimal processedTCcontactImportanceCount = 0;
      Decimal processedTCoverAllStrengthCount = 0;
        
      Decimal processedQexpStrengthCount = 0;
      Decimal processedQcontactImportanceCount = 0;
      Decimal processedQoverAllStrengthCount = 0;
        
      Decimal processedCCexpStrengthCount = 0;
      Decimal processedCCcontactImportanceCount = 0;
      Decimal processedCCoverAllStrengthCount = 0;
      
      Decimal processedSAexpStrengthCount = 0;
      Decimal processedSAcontactImportanceCount = 0;
      Decimal processedSAoverAllStrengthCount = 0;
        
      Decimal processedRELexpStrengthCount = 0;
      Decimal processedRELcontactImportanceCount = 0;
      Decimal processedRELoverAllStrengthCount = 0;
      
      //getting the data and putting the values accordingly
      for(Account_Plan_Contact__c accPlan : accPlanContactList) {
        if(accPlan.Include_in_Overall_Health_Status__c == true) {
            
          if(accPlan.Value_Proposition_Experian_Strength__c != null){
                VPexpStrength += Decimal.valueOf(accPlan.Value_Proposition_Experian_Strength__c);
                processedVPexpStrengthCount++;
          }
          if(accPlan.Value_Proposition_Importance_to_Contact__c != null){
                VPcontactImportance += Decimal.valueOf(accPlan.Value_Proposition_Importance_to_Contact__c);
                processedVPcontactImportanceCount++;
          }
          if(accPlan.Value_Proposition_Overall_Competition_St__c != null){
                VPoverAllStrength += Decimal.valueOf(accPlan.Value_Proposition_Overall_Competition_St__c);
                processedVPoverAllStrengthCount++;
          }
          
          if(accPlan.Business_Understanding_Experian_Strength__c != null){
                BUexpStrength += Decimal.valueOf(accPlan.Business_Understanding_Experian_Strength__c);
                processedBUexpStrengthCount++;
          }
          if(accPlan.Business_Understanding_Importance_to_Con__c != null){
                BUcontactImportance += Decimal.valueOf(accPlan.Business_Understanding_Importance_to_Con__c);
                processedBUcontactImportanceCount++;
          }
          if(accPlan.Business_Understanding_Overall_Competiti__c != null){
                BUoverAllStrength += Decimal.valueOf(accPlan.Business_Understanding_Overall_Competiti__c);
                processedBUoverAllStrengthCount++;
          }
            
          if(accPlan.Responsiveness_Experian_Strength__c != null){
                RESPexpStrength += Decimal.valueOf(accPlan.Responsiveness_Experian_Strength__c);
                processedRESPexpStrengthCount++;
          }
          if(accPlan.Responsiveness_Importance_to_Contact__c != null){
                RESPcontactImportance += Decimal.valueOf(accPlan.Responsiveness_Importance_to_Contact__c);
                processedRESPcontactImportanceCount++;
          }
          if(accPlan.Responsiveness_Overall_Competition_Stren__c != null){
                RESPoverAllStrength += Decimal.valueOf(accPlan.Responsiveness_Overall_Competition_Stren__c);
                processedRESPoverAllStrengthCount++;
          }
            
          if(accPlan.Terms_Conditions_Experian_Strength__c != null){
                TCexpStrength += Decimal.valueOf(accPlan.Terms_Conditions_Experian_Strength__c);
                processedTCexpStrengthCount++;
          }
          if(accPlan.Terms_Conditions_Importance_to_Contact__c != null){
                TCcontactImportance += Decimal.valueOf(accPlan.Terms_Conditions_Importance_to_Contact__c);
                processedTCcontactImportanceCount++;
          }
          if(accPlan.Terms_Conditions_Overall_Competition_S__c != null){
                TCoverAllStrength += Decimal.valueOf(accPlan.Terms_Conditions_Overall_Competition_S__c);
                processedTCoverAllStrengthCount++;
          }
          
          if(accPlan.Quality_Experian_Strength__c != null){
                QexpStrength += Decimal.valueOf(accPlan.Quality_Experian_Strength__c);
                processedQexpStrengthCount++;
          }
          if(accPlan.Quality_Importance_to_Contact__c != null){
                QcontactImportance += Decimal.valueOf(accPlan.Quality_Importance_to_Contact__c);
                processedQcontactImportanceCount++;
          }
          if(accPlan.Quality_Overall_Competition_Strength__c != null){
                QoverAllStrength += Decimal.valueOf(accPlan.Quality_Overall_Competition_Strength__c);
                processedQoverAllStrengthCount++;
          }
          
          if(accPlan.Customer_Care_Experian_Strength__c != null){
                CCexpStrength += Decimal.valueOf(accPlan.Customer_Care_Experian_Strength__c);
                processedCCexpStrengthCount++;
          }
          if(accPlan.Customer_Care_Importance_to_Contact__c != null){
                CCcontactImportance += Decimal.valueOf(accPlan.Customer_Care_Importance_to_Contact__c);
                processedCCcontactImportanceCount++;
          }
          if(accPlan.Customer_Care_Overall_Competition_Streng__c != null){
                CCoverAllStrength += Decimal.valueOf(accPlan.Customer_Care_Overall_Competition_Streng__c);
                processedCCoverAllStrengthCount++;
          }
          
          if(accPlan.Strategy_Alignment_Experian_Strength__c != null){
                SAexpStrength += Decimal.valueOf(accPlan.Strategy_Alignment_Experian_Strength__c);
                processedSAexpStrengthCount++;
          }
          if(accPlan.Strategy_Alignment_Importance_to_Contact__c != null){
                SAcontactImportance += Decimal.valueOf(accPlan.Strategy_Alignment_Importance_to_Contact__c);
                processedSAcontactImportanceCount++;
          }
          if(accPlan.Strategy_Alignment_Overall_Competition_S__c != null){
                SAoverAllStrength += Decimal.valueOf(accPlan.Strategy_Alignment_Overall_Competition_S__c);
                processedSAoverAllStrengthCount++;
          }
            
          if(accPlan.Relationship_Experian_Strength__c != null){
                RELexpStrength += Decimal.valueOf(accPlan.Relationship_Experian_Strength__c);
                processedRELexpStrengthCount++;
          }
          if(accPlan.Relationship_Importance_to_Contact__c != null){
                RELcontactImportance += Decimal.valueOf(accPlan.Relationship_Importance_to_Contact__c);
                processedRELcontactImportanceCount++;
          }
          if(accPlan.Relationship_Overall_Competition_Strengt__c != null){
                RELoverAllStrength += Decimal.valueOf(accPlan.Relationship_Overall_Competition_Strengt__c);
                processedRELoverAllStrengthCount++;
          }
        }
      }
      // PK: Case #01004274 : Fixed below to check for 0 before dividing!
      data.add(new RadarData(
        'Value\nProposition',
        (processedVPcontactImportanceCount > 0) ? VPcontactImportance/processedVPcontactImportanceCount : 0,
        (processedVPexpStrengthCount > 0) ? VPexpStrength/processedVPexpStrengthCount : 0,
        (processedVPoverAllStrengthCount > 0) ? VPoverAllStrength/processedVPoverAllStrengthCount : 0
      ));
      data.add(new RadarData(
        'Business \n Understanding',
        (processedBUcontactImportanceCount > 0) ? BUcontactImportance/processedBUcontactImportanceCount : 0,
        (processedBUexpStrengthCount > 0) ? BUexpStrength/processedBUexpStrengthCount : 0,
        (processedBUoverAllStrengthCount > 0) ? BUoverAllStrength/processedBUoverAllStrengthCount : 0
      ));
      data.add(new RadarData(
        'Responsiveness',
        (processedRESPcontactImportanceCount > 0) ? RESPcontactImportance/processedRESPcontactImportanceCount : 0,
        (processedRESPexpStrengthCount > 0) ? RESPexpStrength/processedRESPexpStrengthCount : 0,
        (processedRESPoverAllStrengthCount > 0) ? RESPoverAllStrength/processedRESPoverAllStrengthCount : 0
      ));
      data.add(new RadarData(
        'Terms & Conditions',
        (processedTCcontactImportanceCount > 0) ? TCcontactImportance/processedTCcontactImportanceCount : 0,
        (processedTCexpStrengthCount > 0) ? TCexpStrength/processedTCexpStrengthCount : 0,
        (processedTCoverAllStrengthCount > 0) ? TCoverAllStrength/processedTCoverAllStrengthCount : 0
      ));
      data.add(new RadarData(
        'Quality',
        (processedQcontactImportanceCount > 0) ? QcontactImportance/processedQcontactImportanceCount : 0,
        (processedQexpStrengthCount > 0) ? QexpStrength/processedQexpStrengthCount : 0,
        (processedQoverAllStrengthCount > 0) ? QoverAllStrength/processedQoverAllStrengthCount : 0
      ));
      data.add(new RadarData(
        'Client Care',
        (processedCCcontactImportanceCount > 0) ? CCcontactImportance/processedCCcontactImportanceCount : 0,
        (processedCCexpStrengthCount > 0) ? CCexpStrength/processedCCexpStrengthCount : 0,
        (processedCCoverAllStrengthCount > 0) ? CCoverAllStrength/processedCCoverAllStrengthCount : 0
      ));
      data.add(new RadarData(
        'Strategy Alignment',
        (processedSAcontactImportanceCount > 0) ? SAcontactImportance/processedSAcontactImportanceCount : 0,
        (processedSAexpStrengthCount > 0) ? SAexpStrength/processedSAexpStrengthCount : 0,
        (processedSAoverAllStrengthCount > 0) ? SAoverAllStrength/processedSAoverAllStrengthCount : 0
      ));
      data.add(new RadarData(
        'Relationship',
        (processedRELcontactImportanceCount > 0) ? RELcontactImportance/processedRELcontactImportanceCount : 0,
        (processedRELexpStrengthCount > 0) ? RELexpStrength/processedRELexpStrengthCount : 0,
        (processedRELoverAllStrengthCount > 0) ? RELoverAllStrength/processedRELoverAllStrengthCount : 0
      ));
    }
     
  }
    
  /**
    * This method will be used to rerender the graph with new width and height based on user input
    **/
  public void changeChartSize(){      
  }
  /**
   * Inner class to show the chart
   **/ 
  public class RadarData {
    String memName {get;set;}
    List<Decimal> ImportanceToContact {get;set;}{ImportanceToContact = new List<Decimal>();}
    List<Decimal>ExperianStrength {get;set;} {ExperianStrength = new List<Decimal>();}
    List<Decimal> OverallCompetitionStrength {get;set;} {OverallCompetitionStrength = new List<Decimal>();}
    /**
      * Constructor of the inner class
      **/
    public RadarData(String memName, Decimal importanceToContact, Decimal experianStrength , Decimal competitionStrength ) {
      this.memName = memName;
      this.ImportanceToContact.add(importanceToContact);
      this.ExperianStrength.add(experianStrength) ;
      this.OverallCompetitionStrength.add(competitionStrength) ;
    }
  }
  
}