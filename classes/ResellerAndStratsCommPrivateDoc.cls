public class ResellerAndStratsCommPrivateDoc{

    @AuraEnabled
    public static Map<String, List<Attachment>> getPrivateDocs(string docType) {
        List<Attachment> docListForStrat = new List<Attachment>();
        List<Attachment> docList = new List<Attachment>();
        Map<String, List<Attachment>> confInfoAttachmentMap = new Map<String, List<Attachment>>();
        User curUser = [Select id, Contactid, Contact.Accountid from User where id =: UserInfo.getUserId()];

        //User curUser = [Select id, Contactid, Contact.Accountid from User where id = '00521000000vmIXAAY']; // testing
        
        if (curUser.Contactid != null && curUser.Contact.Accountid != null){
        System.debug('acc id not null' + curUser.Contact.Accountid);
            //Account acc = [Select id, (Select Id from Confidential_Information__r where Document_Type__c = 'BIS Community Document' OR Document_Type__c = 'CIS Community Document') from Account where id =: curUser.Contact.Accountid];
            List<Confidential_Information__c> confidList = new List<Confidential_Information__c>();
            //confidList = [Select Id from Confidential_Information__c where Account__c =: curUser.Contact.Accountid AND (Document_Type__c = 'BIS Community Document' OR Document_Type__c = 'CIS Community Document')];
            
            if (docType == 'Reseller')
            {
                confidList = [Select Id, Name, Account__r.Name,(Select Id, Name, LastModifiedDate from Attachments Order by LastModifiedDate desc)  from Confidential_Information__c 
                            where Account__c =: curUser.Contact.Accountid AND (Document_Type__c = 'BIS Community Document' OR Document_Type__c = 'CIS Community Document')];
            }
            else
            {
                confidList = [Select Id, Name, Account__r.Name,(Select Id, Name, LastModifiedDate from Attachments Order by LastModifiedDate desc)  from Confidential_Information__c 
                            where Account__c =: curUser.Contact.Accountid AND Document_Type__c = 'Strat Clients Document'];
            }

            System.debug('conf  null ' + confidList );
            if (confidList.size() > 0) {
                
                //confidList  = [Select id, Name, (Select Id, Name from Attachments Order by LastModifiedDate desc limit 1) from Confidential_Information__c where Id IN: acc.Confidential_Information__r];



                for(Confidential_Information__c doc: confidList) 
                {
                    System.debug('inside doc');               
                    if (doc.Attachments.size() > 0) {
                        docList.addAll(doc.Attachments);
                        confInfoAttachmentMap.put(doc.Name,doc.Attachments);
                        //confInfoAttachmentMap.get(doc).addAll(doc.Attachments);
                    }
                }
            System.debug('docList ' + docList);  
            System.debug('confInfoMap ' + confInfoAttachmentMap);  
            }
        }    //Ends if User
        
        //return docList;
        return confInfoAttachmentMap;
    }
    
    @AuraEnabled
    public static string getAccountName() 
    {
        User curUser = [Select id, Contactid, Contact.Account.Name from User where id =: UserInfo.getUserId()];
        return curUser.Contact.Account.Name;
    }


    /*@AuraEnabled 
    public static List<ContentDocument> getLibraryContent() {
        List<ContentWorkspace> contSpace = [SELECT Id FROM ContentWorkspace WHERE Name = 'BIS Policy Document'];
        List<ContentDocument> contDocList = new List<ContentDocument>();
        
        if (contSpace.size() > 0) {
            
            contDocList = [Select id, parentid, title, LastModifiedDate from ContentDocument where parentId =: contSpace[0].Id];
        }
        
        
        return contDocList;
    }
    
    @AuraEnabled 
    public static List<ContentVersion> getLibraryContent() {
        List<ContentWorkspace> contSpace = [SELECT Id FROM ContentWorkspace WHERE Name = 'BIS Policy Document'];
        List<ContentVersion> contDocList = new List<ContentVersion>();
        
        if (contSpace.size() > 0) {
            
            contDocList = [select id, ContentDocumentId, FileType, FileExtension, title, IsLatest, LastModifiedDate from ContentVersion where isLatest = true AND ContentDocument.parentId =: contSpace[0].Id];
        }
        
        
        return contDocList;
    }
    */
    
}