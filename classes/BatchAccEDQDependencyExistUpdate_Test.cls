/*=============================================================================
 * Experian
 * Name: BatchAccEDQDependencyExistUpdate_Test
 * Description: Case 02224510 : Test class to verify the behaviour of BatchAccEDQDependencyExistUpdate.cls
  * Created Date: 08th Dec 2016
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
 
 ============================================================================*/
@isTest
private class BatchAccEDQDependencyExistUpdate_Test{
    static testMethod void myUnitTest(){
        Test.startTest();
            Account acc = Test_Utils.insertAccount();

            Account acc1 = Test_Utils.insertAccount();
            
            Partner objP=new Partner();
            objP.AccountFromId=acc.Id;
            objP.AccountToId=acc1.Id;
            insert objP;
            
            Product2 product = new Product2(Name = '_test_Prod1'); 
            product.Global_Business_Line__c = Constants.USER_GBL_CREDIT_SERVICES; 
            product.IsActive = true; 
            product.Business_Line__c='Experian Data Quality';
            insert product; 
            
            Asset objAss=new Asset();
            objAss.Name='Test Asset';
            objAss.AccountId=acc.Id;
            objAss.Product2Id=product.Id;
            objAss.Start_Date__c = system.today().addDays(-1);
            insert objAss;
            
            Database.executeBatch(new BatchAccEDQDependencyExistUpdate());
            ScheduleAccEDQDependencyExistUpdate bc = new  ScheduleAccEDQDependencyExistUpdate();                
            String sch = '0 0 21 * * ?'; 
            system.schedule('Test AccEDQDependencyExistUpdate', sch, bc);              
            
        Test.stoptest();
    }
}