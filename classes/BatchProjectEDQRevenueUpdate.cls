/*=============================================================================
 * Experian
 * Name: BatchProjectEDQRevenueUpdate 
 * Description: Case :02187106 : This batch is used for one time data update For EDQ revenue Discrepancy field on the Project Object.
 * Created Date:16th Jan 2017
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
 ============================================================================*/
Public class BatchProjectEDQRevenueUpdate implements Database.Batchable<sObject>, Database.Stateful
{
    
    //start method of batch class
    Public Database.QueryLocator start(Database.BatchableContext BC){            
        String query ='select id,EDQ_Revenue_Discrepancy__c,CurrencyISOCode FROM Project__c';
        return Database.getQueryLocator(query);      
    }
    //execute method of batch class
    Public void execute(Database.BatchableContext BC, List<Project__c> lstProject){    
        List<Project__c> lstProjectUpdate = new List<Project__c>();
        map<Id,list<Delivery_Line__c>> mapProjectDelLines = new map<Id,list<Delivery_Line__c>>();
        for(Delivery_Line__c objDel:[Select Id,convertCurrency(Revenue__c) rev,convertCurrency(OL_EDQ_Margin__c) edq,Order_Line_Item__c,Project__c FROM Delivery_Line__c where Project__c IN:lstProject])
        {
            if(mapProjectDelLines.containsKey(objDel.Project__c)){
                list<Delivery_Line__c> lstExiDel = mapProjectDelLines.get(objDel.Project__c);
                lstExiDel.add(objDel);
                mapProjectDelLines.put(objDel.Project__c,lstExiDel);
            }else{
                list<Delivery_Line__c> lstDel = new list<Delivery_Line__c>();
                lstDel.add(objDel);
                mapProjectDelLines.put(objDel.Project__c,lstDel);
            }
        }
        map<string,Decimal> mapConvRate = new map<string,Decimal>();
        for(currencytype objCurrType:[SELECT conversionrate,isocode  FROM currencytype]){
            mapConvRate.put(objCurrType.isocode,objCurrType.conversionrate);
        }
        for(Project__c objProject : lstProject)
        {
            Decimal sumdeliveryrevenue = 0;
            Decimal oplineitemsum = 0;
            map<string,string> mapOrderLines = new map<string,string>();
            if(mapProjectDelLines.containsKey(objProject.Id)){
                for(Delivery_Line__c objDelivery_Lines : mapProjectDelLines.get(objProject.Id)){
                    if(objDelivery_Lines.get('rev') !=NUll)
                        sumdeliveryrevenue = sumdeliveryrevenue  + (decimal)objDelivery_Lines.get('rev');
                    if(objDelivery_Lines.get('edq') !=NUll && !mapOrderLines.containsKey(objDelivery_Lines.Order_Line_Item__c)){
                        oplineitemsum = oplineitemsum   + (decimal)objDelivery_Lines.get('edq');
                        mapOrderLines.put(objDelivery_Lines.Order_Line_Item__c,objDelivery_Lines.Order_Line_Item__c);                   
                    }
                }
            }
            objProject.EDQ_Revenue_Discrepancy__c = (oplineitemsum - sumdeliveryrevenue) * mapConvRate.get(objProject.CurrencyISOCode) ;
            lstProjectUpdate.add(objProject);
        }
        if(!lstProjectUpdate.isEmpty())
        update lstProjectUpdate;
    }  
    
    Public void finish(Database.BatchableContext BC){
    
    }
}