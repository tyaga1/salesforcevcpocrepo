/**=====================================================================
 * Experian
 * Name: DealNotesController_Test
 * Description: Tests for Controller to display list of notes on the deal records
 * Created Date: Oct 29th, 2015
 * Created By: Paul Kissick 
 * 
 * Date Modified      Modified By                Description of the update
 =====================================================================*/
@isTest
private class DealNotesController_Test {

  static testMethod void testNotes() {
    
    Deal__c deal = new Deal__c(Project_Name__c = 'Test Project Name');
    insert deal;
    
    Note n1 = new Note(ParentId = deal.Id, Title = 'Note Title', Body = 'Note Body');
    insert n1;
    
    DealNotesController dnc = new DealNotesController(new ApexPages.StandardController(deal));
    system.assertEquals(1, dnc.getDealNotes().size());
    
    dnc.delNoteId = n1.Id;
    dnc.delNote();
    
    system.assertEquals(0, dnc.getDealNotes().size());
    
    
  }
  
}