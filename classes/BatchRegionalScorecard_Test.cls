/*=============================================================================
 * Experian
 * Name: BatchRegionalScorecard_Test
 * Description: 
 * Created Date: 24 May 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/

@isTest
private class BatchRegionalScorecard_Test {

  static String[] customRegions = new String[]{'Narnia','Mordor','Asgard'};
  static String[] customBusinessLines = new String[]{'Narnia Decision Analytics','Mordor Marketing Services','Asgard Credit Services'};
  static String[] customBusinessUnits = new String[]{'Narnia Decision Analytics Sales','Mordor Marketing Services Ops','Asgard Credit Services Marketing'};
  static String[] customSalesTeams = new String[]{'Narnia Decision Analytics Sales Team 1','Mordor Marketing Services Ops Team 1','Asgard Credit Services Marketing Team 1'};
  static String[] customSalesSubTeams = new String[]{'Narnia Decision Analytics Sales Sub Team 1','Mordor Marketing Services Ops Sub Team 1','Asgard Credit Services Marketing Sub Team 1'};


  static testMethod void testNoOfUsers() {
    Regional_Scorecard_Metric__c rsm = [SELECT Id FROM Regional_Scorecard_Metric__c WHERE Name = '# of SFDC Sales Users' LIMIT 1];
    
    Test.startTest();
    BatchRegionalScorecardEngine brse = new BatchRegionalScorecardEngine();
    brse.metricId = rsm.Id;
    
    Database.executeBatch(brse);
    
    Test.stopTest();
    
    system.assertEquals(3,[
      SELECT COUNT() 
      FROM Regional_Scorecard_Data__c 
      WHERE Regional_Scorecard_Metric__c = :rsm.Id 
      AND Type__c = 'Region'], 'There should be 3 entries for region');
    
  }
  
  static testMethod void testNoOfOpps() {
    
    loadMasterGrouping();
    
    Regional_Scorecard_Metric__c rsm = [SELECT Id FROM Regional_Scorecard_Metric__c WHERE Name = '# of Open Opps' LIMIT 1];
    
    Test.startTest();
    BatchRegionalScorecardEngine brse = new BatchRegionalScorecardEngine();
    brse.metricId = rsm.Id;
    
    Database.executeBatch(brse);
    
    Test.stopTest();
    
    for(Regional_Scorecard_Data__c rsd : [SELECT Name__c, Type__c, Value_Count__c FROM Regional_Scorecard_Data__c]) {
      system.debug(rsd);
    }
    
    system.assertEquals(3,[
      SELECT COUNT() 
      FROM Regional_Scorecard_Data__c 
      WHERE Regional_Scorecard_Metric__c = :rsm.Id 
      AND Type__c = 'Region' 
      AND Value_Count__c != 0.0], 'There should be 3 entries for region');
    
  }
  
  static testMethod void testOppAmounts() {
    
    loadMasterGrouping();
    
    Regional_Scorecard_Metric__c rsm = [SELECT Id FROM Regional_Scorecard_Metric__c WHERE Name = 'Total Pipeline' LIMIT 1];
    
    Test.startTest();
    BatchRegionalScorecardEngine brse = new BatchRegionalScorecardEngine();
    brse.metricId = rsm.Id;
    
    Database.executeBatch(brse);
    
    Test.stopTest();
    
    system.assertEquals(3,[
      SELECT COUNT() 
      FROM Regional_Scorecard_Data__c 
      WHERE Regional_Scorecard_Metric__c = :rsm.Id 
      AND Type__c = 'Region' 
      AND Value_Currency__c != 0.0], 'There should be 3 entries for region');
    
  }
  
  static testMethod void testCalculations() {
    
    loadMasterGrouping();
    loadOppCounts();
    loadOppAmounts();
    
    Regional_Scorecard_Metric__c rsm = [SELECT Id FROM Regional_Scorecard_Metric__c WHERE Name = 'Avg Opps per User' LIMIT 1];
    
    Test.startTest();
    BatchRegionalScorecardCalculated brse = new BatchRegionalScorecardCalculated();
    brse.metricId = rsm.Id;
    
    Database.executeBatch(brse);
    
    Test.stopTest();
    
    system.assertEquals(3,[
      SELECT COUNT() 
      FROM Regional_Scorecard_Data__c 
      WHERE Regional_Scorecard_Metric__c = :rsm.Id 
      AND Type__c = 'Region' 
      AND Value_Count__c != 0.0], 'There should be 3 entries for region');
    
  }
  
  static testMethod void testStartBatch() {
    
    Test.startTest();
    
    RegionalScorecardUtils.startBatch();
    
    Test.stopTest();
    
    system.assertEquals(3,[
      SELECT COUNT() 
      FROM Regional_Scorecard_Data__c 
      WHERE Type__c = 'Region' 
      AND Value_Count__c != 0.0], 'There should be 3 entries for region');
    
  }
  
  static testMethod void testStartBatchToCalc() {
    
    loadMasterGrouping();
    
    List<Regional_Scorecard_Metric__c> disableRsms = [
      SELECT Id, Active__c
      FROM Regional_Scorecard_Metric__c
      WHERE Calculation_Field_Operator__c = null
    ];
    for (Regional_Scorecard_Metric__c rsm : disableRsms) {
      rsm.Active__c = false;
    }
    update disableRsms;
    
    Test.startTest();
    
    RegionalScorecardUtils.startBatch();
    
    Test.stopTest();
    
  }
  
  static testMethod void testScheduler() {
    
    Test.startTest();
    
    String CRON_EXP = '0 0 0 * * ?';
    String jobId = system.schedule('ScheduleRegionalScorecardProcess - TEST '+Datetime.now().getTime(), CRON_EXP, new ScheduleRegionalScorecardProcess());
            
    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
    
    Test.stopTest();

    // Verify the expressions are the same  
    system.assertEquals(CRON_EXP, ct.CronExpression);

    // Verify the job has not run  
    system.assertEquals(0, ct.TimesTriggered); 
  }
  
  static testMethod void testExporter() {
    loadMasterGrouping();
    loadOppCounts();
    loadOppAmounts();
    
    Test.startTest();

    RegionalScorecardExportController rsec = new RegionalScorecardExportController();
    
    system.assertEquals(0, rsec.outputData.size());
    system.assertEquals(0, rsec.metricLabels.size());
    system.assertEquals(0, rsec.metricValues.size());

    rsec.loadData();
    system.assertNotEquals('', rsec.getCurrentDateTime());
    system.assertNotEquals(0, rsec.outputData.size());
    system.assertNotEquals(0, rsec.metricLabels.size());
    system.assertNotEquals(0, rsec.metricValues.size());

    Test.stopTest();
  }
  
  @testSetup
  private static void setupData() {
    // Initialise data for test here.
    // lets create some metrics...
    
    Regional_Scorecard_Metric__c rsm1 = new Regional_Scorecard_Metric__c(
      Name = '# of SFDC Sales Users',
      Label__c = '# of SFDC Sales Users',
      Description__c = 'The number of users',
      CurrencyISOCode = 'USD',
      Active__c = true, 
      Master_Grouping__c = true,
      Aggregate_Type__c = 'Count',
      Aggregate_Field__c = 'Id',
      Grouping__c = 'Region__c,Business_Line__c,Business_Unit__c,Sales_Team__c,Sales_Sub_Team__c',
      Object_API_Name__c = 'User',
      Where_Clause__c = 'UserType = \'Standard\' AND IsActive = true AND Region__c IN (\''+String.join(customRegions,'\',\'')+'\')',
      Displayed_Field__c = true,
      Grouping_Names__c = 'Region,Business Line,Business Unit,Sales Team/Channel,Sales Sub-Team/Region',
      Type__c = 'Count',
      Calculation_Field_Operator__c = null,
      Metric_Calculation_Field_1__c = null,
      Metric_Calculation_Field_2__c = null,
      Processing_Order__c = '1'
    );
    
    Regional_Scorecard_Metric__c rsm2 = new Regional_Scorecard_Metric__c(
      Name = '# of Open Opps',
      Label__c = '# of Open Opps',
      Description__c = 'The number of open opps',
      CurrencyISOCode = 'USD',
      Active__c = true, 
      Aggregate_Type__c = 'Count',
      Aggregate_Field__c = 'Id',
      Grouping__c = 'Owner_s_Region__c,Owner_s_Business_Line__c,Owner_s_Business_Unit__c,Owner_s_Sales_team__c,Owner_s_Sales_Sub_Team__c',
      Object_API_Name__c = 'Opportunity',
      Where_Clause__c = 'IsClosed = false',
      Displayed_Field__c = true,
      Grouping_Names__c = 'Region,Business Line,Business Unit,Sales Team/Channel,Sales Sub-Team/Region',
      Type__c = 'Count',
      Calculation_Field_Operator__c = null,
      Metric_Calculation_Field_1__c = null,
      Metric_Calculation_Field_2__c = null,
      Processing_Order__c = '2'
    );
    
    Regional_Scorecard_Metric__c rsm3 = new Regional_Scorecard_Metric__c(
      Name = 'Total Pipeline',
      Label__c = 'Total Pipeline',
      Description__c = 'Total Pipeline',
      CurrencyISOCode = 'USD',
      Active__c = true, 
      Aggregate_Type__c = 'Sum',
      Aggregate_Field__c = 'Amount',
      Grouping__c = 'Owner_s_Region__c,Owner_s_Business_Line__c,Owner_s_Business_Unit__c,Owner_s_Sales_team__c,Owner_s_Sales_Sub_Team__c',
      Object_API_Name__c = 'Opportunity',
      Where_Clause__c = 'IsClosed = false',
      Displayed_Field__c = true,
      Grouping_Names__c = 'Region,Business Line,Business Unit,Sales Team/Channel,Sales Sub-Team/Region',
      Type__c = 'Currency',
      Calculation_Field_Operator__c = null,
      Metric_Calculation_Field_1__c = null,
      Metric_Calculation_Field_2__c = null,
      Processing_Order__c = '2'
    );
    
    insert new Regional_Scorecard_Metric__c[]{rsm1, rsm2, rsm3};
    
    Regional_Scorecard_Metric__c rsm4 = new Regional_Scorecard_Metric__c(
      Name = 'Avg Opps per User',
      Label__c = 'Avg Opps per User',
      Description__c = 'Avg Opps per User',
      CurrencyISOCode = 'USD',
      Active__c = true, 
      Aggregate_Type__c = 'Average',
      Aggregate_Field__c = 'n/a',
      Grouping__c = 'n/a',
      Object_API_Name__c = 'n/a',
      Where_Clause__c = 'n/a',
      Displayed_Field__c = true,
      Grouping_Names__c = 'n/a',
      Type__c = 'Decimal',
      Calculation_Field_Operator__c = '/',
      Metric_Calculation_Field_1__c = rsm2.Id,
      Metric_Calculation_Field_2__c = rsm1.Id,
      Processing_Order__c = '3'
    );
    
    Regional_Scorecard_Metric__c rsm5 = new Regional_Scorecard_Metric__c(
      Name = 'Avg Amount per Opp',
      Label__c = 'Avg Amount per Opp',
      Description__c = 'Avg Amount per Opp',
      CurrencyISOCode = 'USD',
      Active__c = true, 
      Aggregate_Type__c = 'Average',
      Aggregate_Field__c = 'n/a',
      Grouping__c = 'n/a',
      Object_API_Name__c = 'n/a',
      Where_Clause__c = 'n/a',
      Displayed_Field__c = true,
      Grouping_Names__c = 'n/a',
      Type__c = 'Decimal',
      Calculation_Field_Operator__c = '/',
      Metric_Calculation_Field_1__c = rsm3.Id,
      Metric_Calculation_Field_2__c = rsm2.Id,
      Processing_Order__c = '3'
    );
    
    insert new Regional_Scorecard_Metric__c[]{rsm4, rsm5};
    
    // Make some users
    Profile p = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN LIMIT 1];
    List<User> testUsers = Test_Utils.createUsers(p, 'tstUser13214@experian.com', 'TST', 3);
    for (Integer i = 0; i < testUsers.size(); i++) {
      testUsers[i].Region__c = customRegions[i];
      testUsers[i].Business_Line__c = customBusinessLines[i];
      testUsers[i].Business_Unit__c = customBusinessUnits[i];
      testUsers[i].Sales_Team__c = customSalesTeams[i];
      testUsers[i].Sales_Sub_Team__c = customSalesSubTeams[i];
    }
    system.runAs(new User(Id = UserInfo.getUserId())) {
      insert testUsers;
    }
    
    Account acc = Test_Utils.insertAccount();
    List<Opportunity> allOpps = new List<Opportunity>();
     
    for (User u : testUsers) {
      Opportunity opp = Test_Utils.createOpportunity(acc.Id);
      opp.OwnerId = u.Id;
      opp.Amount = 100.0;
      allOpps.add(opp);
    }
    
    insert allOpps;
  }
  
  private static void loadMasterGrouping() {
    List<Regional_Scorecard_Data__c> dataList = new List<Regional_Scorecard_Data__c>();
    Regional_Scorecard_Metric__c metric = [SELECT Id, Type__c FROM Regional_Scorecard_Metric__c WHERE Master_Grouping__c = true LIMIT 1];
    for (Integer i = 0; i < customRegions.size(); i++) {
      dataList.add(RegionalScorecardUtils.buildDataRecord (metric, 1, customRegions[i], 'Region'));
      dataList.add(RegionalScorecardUtils.buildDataRecord (metric, 1, customBusinessLines[i], 'Business Line'));
      dataList.add(RegionalScorecardUtils.buildDataRecord (metric, 1, customBusinessUnits[i], 'Business Unit'));
      dataList.add(RegionalScorecardUtils.buildDataRecord (metric, 1, customSalesTeams[i], 'Sales Team/Channel'));
      dataList.add(RegionalScorecardUtils.buildDataRecord (metric, 1, customSalesSubTeams[i], 'Sales Sub-Team/Region'));
    }
    insert dataList;
  }
  
  private static void loadOppCounts() {
    List<Regional_Scorecard_Data__c> dataList = new List<Regional_Scorecard_Data__c>();
    Regional_Scorecard_Metric__c metric = [SELECT Id, Type__c FROM Regional_Scorecard_Metric__c WHERE Name = '# of Open Opps' LIMIT 1];
    for (Integer i = 0; i < customRegions.size(); i++) {
      dataList.add(RegionalScorecardUtils.buildDataRecord (metric, 1, customRegions[i], 'Region'));
      dataList.add(RegionalScorecardUtils.buildDataRecord (metric, 1, customBusinessLines[i], 'Business Line'));
      dataList.add(RegionalScorecardUtils.buildDataRecord (metric, 1, customBusinessUnits[i], 'Business Unit'));
      dataList.add(RegionalScorecardUtils.buildDataRecord (metric, 1, customSalesTeams[i], 'Sales Team/Channel'));
      dataList.add(RegionalScorecardUtils.buildDataRecord (metric, 1, customSalesSubTeams[i], 'Sales Sub-Team/Region'));
    }
    insert dataList;
  }
  
  private static void loadOppAmounts() {
    List<Regional_Scorecard_Data__c> dataList = new List<Regional_Scorecard_Data__c>();
    Regional_Scorecard_Metric__c metric = [SELECT Id, Type__c FROM Regional_Scorecard_Metric__c WHERE Name = 'Total Pipeline' LIMIT 1];
    for (Integer i = 0; i < customRegions.size(); i++) {
      dataList.add(RegionalScorecardUtils.buildDataRecord (metric, 100.0, customRegions[i], 'Region'));
      dataList.add(RegionalScorecardUtils.buildDataRecord (metric, 100.0, customBusinessLines[i], 'Business Line'));
      dataList.add(RegionalScorecardUtils.buildDataRecord (metric, 100.0, customBusinessUnits[i], 'Business Unit'));
      dataList.add(RegionalScorecardUtils.buildDataRecord (metric, 100.0, customSalesTeams[i], 'Sales Team/Channel'));
      dataList.add(RegionalScorecardUtils.buildDataRecord (metric, 100.0, customSalesSubTeams[i], 'Sales Sub-Team/Region'));
    }
    insert dataList;
  }
  
}