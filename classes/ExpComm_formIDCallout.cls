/******************************************************************************
 * Name: ExpComm_formIDCallout.cls
 * Created Date: 2/14/2017
 * Created By: Hay Win, UCInnovation
 * Description : class to call service now and insert or update the custom setting,ServiceNow_FormID__c
 * Change Log- 
 ****************************************************************************/
 
global class ExpComm_formIDCallout {
 
   public void basicAuthCallout(){
   
   Map<String,ServiceNow_FormID__c> catelogNameMap = new Map<String,ServiceNow_FormID__c>();
   Set<ServiceNow_FormID__c> catelogSet = new Set<ServiceNow_FormID__c>();
   List<ServiceNow_FormID__c> catelogList = new List<ServiceNow_FormID__c>();
   List<ServiceNow_FormID__c> updateList = new List<ServiceNow_FormID__c>();
   Set<ServiceNow_FormID__c> updateSet = new Set<ServiceNow_FormID__c>();
   List<String> nameList = new List<String>();
   String newForms = '';
   String updatedForms = '';
   
   HttpRequest req = new HttpRequest();
    req.setEndpoint('callout:Service_Now/api/now/table/sc_cat_item?sysparm_fields=sys_name%2Csys_id&sysparm_limit=1000&active=true');
    req.setMethod('GET');
    Http http = new Http();
    HTTPResponse res = http.send(req);



     JSONParser parser = JSON.createParser(res.getBody());
    while (parser.nextToken() != null) {
        if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
            String label= parser.getText();
            String catelog ='';
            String formID = '';
            parser.nextToken();
            if (label== 'sys_id') {
                formID = parser.getText();       
                // get Catelog name
                parser.nextToken();
                String catelogLabel = parser.getText();
                parser.nextToken();
                if (catelogLabel =='sys_name') {
                    catelog = parser.getText();
                    ServiceNow_FormID__c service_form= new ServiceNow_FormID__c(Name = formID, catelog__c = catelog);
                    
                    //check if it's update
                    if (ServiceNow_FormID__c.getValues(formID) != null) {
                        ServiceNow_FormID__c existing = ServiceNow_FormID__c.getValues(formID);
                        if ((existing.Name != formID.trim() && existing.catelog__c == catelog.trim()) || (existing.Name == formID.trim() && existing.catelog__c != catelog.trim())) {
                            
                            existing.Name = formID.trim();
                            existing.catelog__c = catelog.trim();
                            System.debug('existing ' + existing);
                            updateSet.add(existing);
                        }
                        
                        //updateSet.add(service_form);
                    } else {
                        catelogNameMap.put(catelog, service_form);
                        catelogSet.add(service_form);
                    }
                }            
            } 
        }
       }
       
     for(ServiceNow_FormID__c form: [Select Id, Name, catelog__c from ServiceNow_FormID__c where catelog__c IN: catelogNameMap.keySet()]) {
         ServiceNow_FormID__c current = catelogNameMap.get(form.catelog__c);
         if (current != null) {
             catelogSet.remove(current);
             form.Name = current.Name;
             updateSet.add(form);
         }
     } 
     
     catelogList.addAll(catelogSet);
     
       if (catelogList.size() > 0 ) {
           insert catelogList;
           newForms = 'The following new Form IDs were created: \r\n';
           for(ServiceNow_FormID__c servNowForm: catelogList){
               newForms = newForms + servNowForm.Name + ' : ' + servNowForm.catelog__c + '\r\n';
           }
       }
       
       updateList.addAll(updateSet);
       if (updateList.size() > 0) {
           update updateList;
           updatedForms = 'The following Form IDs were Updated: \r\n'; 
           for(ServiceNow_FormID__c servNowForm: updateList){
               updatedForms = updatedForms + servNowForm.Name + ' : ' + servNowForm.catelog__c + '\r\n';
           }
       }
       
       String finalFormList = '';
       if(newForms != '') finalFormList = finalFormList + newForms + '\r\n';
       if(updatedForms != '') finalFormList = finalFormList + updatedForms;
       
       if(finalFormList != ''){
           List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
           Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
           email.setPlainTextBody(finalFormList );
           
           List<String> toAddressList = new List<String>();
           toAddressList.add(label.ServiceNow_Form_Id_Email);           
           email.setToAddresses(toAddressList);
           email.setSubject('Service Now Form IDs Created or Updated');
           email.setSaveAsActivity(false);
           
           emailList.add(email);
           Messaging.sendEmail(emailList,false);

       }
       
   }
}