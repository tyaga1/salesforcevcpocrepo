/**=====================================================================
 * Experian
 * Name: WebserviceEndpointAddPassCtrlr_Test
 * Description: 
 * Created Date: 11 Nov 2015
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update

 =====================================================================*/
@isTest
private class WebserviceEndpointAddPassCtrlr_Test {

  static testMethod void myUnitTest() {
    
    ApexPages.standardController stdCon = new ApexPages.StandardController([SELECT Id, Username__c, Password__c FROM Webservice_Endpoint__c WHERE Name = 'Test Webservice 1' LIMIT 1]);
    
    WebserviceEndpointAddPassCtrlr wsCntrl = new WebserviceEndpointAddPassCtrlr(stdCon);
    wsCntrl.checkKey();
    
    system.assertEquals(true, wsCntrl.okayToShow);
    
    wsCntrl.wsPassword = 'New Password';
    
    wsCntrl.save();
    
    Webservice_Endpoint__c wsCheck = [SELECT Id, Username__c, Password__c FROM Webservice_Endpoint__c WHERE Name = 'Test Webservice 1' LIMIT 1];
    system.assertNotEquals('New Password', wsCheck.Password__c);
    
  }
  
  @testSetup
  static void setupTestData() {
    Webservice_Endpoint__c ws = new Webservice_Endpoint__c(Username__c = 'TestUser', Active__c = true, Name = 'Test Webservice 1', URL__c = 'https://test.com');
    insert ws;
  }
}