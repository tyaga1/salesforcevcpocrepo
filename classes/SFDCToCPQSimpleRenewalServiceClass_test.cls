/**=====================================================================
 * Experian
 * Name: SFDCToCPQSimpleRenewalServiceClass_test
 * Description: Test class to cover  SFDCToCPQSimpleRenewalServiceClass
 * Created Date: Oct 13th 2014
 * Created By: Richard Joseph
 *
 * Date Modified      Modified By                Description of the update
 * Apr 7th, 2016      Paul Kissick               Case 01932085: Fixing Test User Email Domain
 * May 16th, 2016     Paul Kissick               Case 01220695: Adding proper edq validity checks
 * Oct 20th, 2016     James Wills                Case #02088090 - Resolved issue with test following validation rule change
 * Dic 09th, 2016     Diego Olarte               Case #02137101 - Added extra contact roles to sync with Contact Address Validation
 * Apr 25th, 2017     Sanket Vaidya              Case 02150014: CRM 2.0- Opportunity Competitor Information [Added Is_competitor__c flag to true for account]  
 =====================================================================*/
@isTest
private class SFDCToCPQSimpleRenewalServiceClass_test {

  static Map<String, TriggerSettings__c> triggerSettingMap;
  static Opportunity testOpp;
  static Opportunity testOpp1;
  static OpportunityLineItem oli2;
  static Product2 product;

  /*private static testMethod void SimpleRenewalServiceClass_Test() {

    IsDataAdmin__c ida = new IsDataAdmin__c(SetupOwnerId = Userinfo.getUserId(), IsDataAdmin__c = true);
    insert ida;

    TriggerSettings__c insertTriggerSettings = Test_Utils.insertTriggerSettings(Constants.USER_TRIGGER);
    insertTriggerSettings.IsActive__c = false;
    update insertTriggerSettings;

    Profile p;
    UserRole copsRole;
    User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    List<User> usrList = new List<User>();
    User testUser1;
    User testUser2;
    Record_Type_Ids__c recIds = new Record_Type_Ids__c(
      SetupOwnerId = Userinfo.getOrganizationId(),
      Opportunity_Standard__c = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('Standard').getRecordTypeId()
    );
    insert recIds;

    Global_Settings__c gs = new Global_Settings__c(
      Name = Constants.GLOBAL_SETTING,
      Opp_Closed_Lost_Stagename__c = Constants.OPPTY_CLOSED_LOST,
      Opp_Renewal_Probability__c = 30,
      Opp_Renewal_Name_Format__c = 'Renewal - ####',
      Opp_Renewal_StageName__c = Constants.OPPTY_STAGE_3,
      Opp_Renewal_Type__c = Constants.OPPTY_TYPE_RENEWAL,
      Opp_Stage_3_Name__c = Constants.OPPTY_STAGE_3,
      Opp_Stage_4_Name__c = Constants.OPPTY_STAGE_4
    );
    insert gs;

    system.runAs(thisUser) {
      p = [select id from profile where name=: Constants.PROFILE_SYS_ADMIN ];
      copsRole = [SELECT Id FROM UserRole WHERE Name =: Constants.ROLE_NA_COPS];
      testUser1 = Test_Utils.createEDQUser(p, 'test1234@experian.com', 'test1');
      testUser2 = Test_Utils.createEDQUser(p, 'test1235@experian.com', 'test2');
      testUser1.UserRoleId = copsRole.Id;
      testUser2.UserRoleId = copsRole.Id;
      usrList.add(testUser1);
      usrList.add(testUser2);
      insert usrList;
    }
    system.runAs(testUser1) {
      createTestData();
      delete ida;
    }
    OpportunityTriggerHandler.isAfterUpdateTriggerExecuted = false;
    OpportunityTriggerHandler.isBeforeUpdateTriggerExecuted = false;
    OpportunityTrigger_OrderHelper.isExecuted = false;
    system.runAs(testUser2) {

      testOpp.StageName = Constants.OPPTY_STAGE_7;
      //testOpp.Status__c = Constants.OPPTY_CLOSED_WON;
      testOpp.Primary_Reason_W_L__c = constants.PRIMARY_REASON_WLC_DATA_QUALITY;
      testOpp.Channel_Type__c = Constants.OPPTY_CHANNEL_TYPE_DIRECT;
      testOpp.Amount = 100;
      testOpp.Has_Senior_Approval__c = true;
      testOpp.Starting_Stage__c = Constants.OPPTY_STAGE_6;
      testOpp.CloseDate = date.today();
      testOpp.Contract_Start_Date__c = date.today().addDays(5);
      testOpp.Contract_End_Date__c = date.today().addYears(1);

    }
    system.debug('Richard Test ' + testOpp.Number_of_Follow_Up_Opportunities__c);

    Test.startTest();

    update testOpp;

    Test.stoptest();

    system.debug('Richard Test ' + [Select Id from Opportunity where Previous_Opportunity__c =:testOpp.Id]);
    for(Opportunity renewalOpty : [Select Id from Opportunity where Previous_Opportunity__c =:testOpp.Id limit 1]) {
      ApexPages.currentPage().getParameters().put('Id',renewalOpty.id );

      list<Quote__c> testQuote = new list<Quote__c>();
      testQuote.add(new Quote__c());
      ApexPages.StandardsetController stdCntrl = new ApexPages.StandardsetController(testQuote);
      SimpleQuoteRenewalExtCntlr testSimplQuoteRenewalObj = new SimpleQuoteRenewalExtCntlr(stdCntrl);
      SFDCToCPQSimpleRenewalServiceClass.callCPQSimpleRenewalService(renewalOpty.id,UserInfo.getUserName());
    }

  }*/

  //============================================================================
  // test method for creating test data to be used in various test methods
  //============================================================================
  static void createTestData() {

    Global_Settings__c gs = new Global_Settings__c(
      Name = Constants.GLOBAL_SETTING,
      Opp_Closed_Lost_Stagename__c = Constants.OPPTY_CLOSED_LOST,
      Opp_Renewal_Probability__c = 30,
      Opp_Renewal_Name_Format__c = 'Renewal - ####',
      Opp_Renewal_StageName__c = Constants.OPPTY_STAGE_3,
      Opp_Renewal_Type__c = Constants.OPPTY_TYPE_RENEWAL,
      Opp_Stage_3_Name__c = Constants.OPPTY_STAGE_3,
      Opp_Stage_4_Name__c = Constants.OPPTY_STAGE_4
    );
    insert gs;

    TriggerSettings__c insertTriggerSettings = Test_Utils.insertTriggerSettings(Constants.ACCOUNT_TRIGGER);
    insertTriggerSettings.IsActive__c = false;
    update insertTriggerSettings;

    insertTriggerSettings = Test_Utils.insertTriggerSettings(Constants.CONTACT_TRIGGER);
    insertTriggerSettings.IsActive__c = false;
    update insertTriggerSettings;

    insertTriggerSettings = Test_Utils.insertTriggerSettings(Constants.ACCOUNT_ADDRESS_TRIGGER);
    insertTriggerSettings.IsActive__c = false;
    update insertTriggerSettings;

    insertTriggerSettings = Test_Utils.insertTriggerSettings(Constants.CONTACT_ADDRESS_TRIGGER);
    insertTriggerSettings.IsActive__c = false;
    update insertTriggerSettings;

    insertTriggerSettings = Test_Utils.insertTriggerSettings(Constants.TASK_TRIGGER);
    insertTriggerSettings.IsActive__c = false;
    update insertTriggerSettings;

    insertTriggerSettings = Test_Utils.insertTriggerSettings(Constants.OPPORTUNITY_CONTACT_ADDRESS_TRIGGER);
    insertTriggerSettings.IsActive__c = false;
    update insertTriggerSettings;


     // Create an account
    Account testAccount = Test_Utils.insertAccount();    
    
    Account testAccount1 = Test_Utils.insertAccount();
    
    Account testAccount2 = Test_Utils.insertAccount();
    
    Account testAccount3 = Test_Utils.insertAccount();
    
    Account testAccount4 = Test_Utils.insertAccount();
    
    Account testAccount5 = Test_Utils.insertAccount();
    
    Account testAccount6 = Test_Utils.insertAccount();
    //Test.startTest();
    Address__c addrs1 = Test_Utils.insertAddress(true);
    //insert account address
    Account_Address__c accAddrs = Test_Utils.insertAccountAddress(true, addrs1.Id, testAccount.Id);
    // Create an opportunity
    testOpp = Test_Utils.createOpportunity(testAccount.Id);
    testOpp1 = Test_Utils.createOpportunity(testAccount.Id);
    testOpp.Starting_Stage__c = Constants.OPPTY_STAGE_6;
    testOpp.Type = Constants.OPPTY_TYPE_RENEWAL;

    insert testOpp;

    Test_Utils.createOpptyTasks(testOpp.Id, true);

    Contact newcontact  = new Contact (
      FirstName = 'Larry', LastName = 'Ellison',
      AccountId = testAccount.Id,
      Email = 'larrye@email.com'
    );
    insert newcontact;
    
    Contact newcontact1  = new Contact (
      FirstName = 'Larry1', LastName = 'Ellison1',
      AccountId = testAccount1.Id,
      Email = 'larrye1@email.com'
    );
    insert newcontact1;
    
    Contact newcontact2  = new Contact (
      FirstName = 'Larry2', LastName = 'Ellison2',
      AccountId = testAccount2.Id,
      Email = 'larrye2@email.com'
    );
    insert newcontact2;
    
    Contact newcontact3  = new Contact (
      FirstName = 'Larry3', LastName = 'Ellison3',
      AccountId = testAccount3.Id,
      Email = 'larrye3@email.com'
    );
    insert newcontact3;
    
    Contact newcontact4  = new Contact (
      FirstName = 'Larry4', LastName = 'Ellison4',
      AccountId = testAccount4.Id,
      Email = 'larrye4@email.com'
    );
    insert newcontact4;
    
    Contact newcontact5  = new Contact (
      FirstName = 'Larry5', LastName = 'Ellison5',
      AccountId = testAccount5.Id,
      Email = 'larrye5@email.com'
    );
    insert newcontact5;
    
    Contact newcontact6  = new Contact (
      FirstName = 'Larry6', LastName = 'Ellison6',
      AccountId = testAccount6.Id,
      Email = 'larrye6@email.com'
    );
    insert newcontact6;
    

    Contact_Address__c conAdd = Test_Utils.insertContactAddress(true, Test_Utils.insertAddress(true).Id, newcontact.Id);
    
    Contact_Address__c conAdd1 = Test_Utils.insertContactAddress(true, Test_Utils.insertAddress(true).Id, newcontact1.Id);
    
    Contact_Address__c conAdd2 = Test_Utils.insertContactAddress(true, Test_Utils.insertAddress(true).Id, newcontact2.Id);
    
    Contact_Address__c conAdd3 = Test_Utils.insertContactAddress(true, Test_Utils.insertAddress(true).Id, newcontact3.Id);
    
    Contact_Address__c conAdd4 = Test_Utils.insertContactAddress(true, Test_Utils.insertAddress(true).Id, newcontact4.Id);
    
    Contact_Address__c conAdd5 = Test_Utils.insertContactAddress(true, Test_Utils.insertAddress(true).Id, newcontact5.Id);
    
    Contact_Address__c conAdd6 = Test_Utils.insertContactAddress(true, Test_Utils.insertAddress(true).Id, newcontact6.Id);

    List<Opportunity_Contact_Address__c> oppConAdds = new List<Opportunity_Contact_Address__c>();
    Opportunity_Contact_Address__c oppConAdd = new Opportunity_Contact_Address__c();
    oppConAdd.Opportunity__c= testOpp.Id ;
    oppConAdd.Address__c = conAdd.Address__c;
    oppConAdd.Contact__c = newcontact.Id;
    oppConAdd.Role__c = Constants.OPPTY_CONTACT_ROLE_PURCHASE_LEDGER;
    oppConAdds.add(oppConAdd);

    Opportunity_Contact_Address__c oppConAdd1 = new Opportunity_Contact_Address__c();
    oppConAdd1.Opportunity__c= testOpp.Id ;
    oppConAdd1.Address__c = conAdd1.Address__c;
    oppConAdd1.Contact__c = newcontact1.Id;
    oppConAdd1.Role__c = Constants.OPPTY_CONTACT_ROLE_COMMERCIAL;
    oppConAdds.add(oppConAdd1);

    Opportunity_Contact_Address__c oppConAdd2 = new Opportunity_Contact_Address__c();
    oppConAdd2.Opportunity__c= testOpp.Id ;
    oppConAdd2.Address__c = conAdd2.Address__c;
    oppConAdd2.Contact__c = newcontact2.Id;
    oppConAdd2.Role__c = Constants.OPPTY_CONTACT_ROLE_CONTRACTUAL;
    oppConAdds.add(oppConAdd2);

    Opportunity_Contact_Address__c oppConAdd3 = new Opportunity_Contact_Address__c();
    oppConAdd3.Opportunity__c= testOpp.Id ;
    oppConAdd3.Address__c = conAdd3.Address__c;
    oppConAdd3.Contact__c = newcontact3.Id;
    oppConAdd3.Role__c = Constants.OPPTY_CONTACT_ROLE_RENEWAL;
    oppConAdds.add(oppConAdd3);

    Opportunity_Contact_Address__c oppConAdd4 = new Opportunity_Contact_Address__c();
    oppConAdd4.Opportunity__c= testOpp.Id ;
    oppConAdd4.Address__c = conAdd4.Address__c;
    oppConAdd4.Contact__c = newcontact4.Id;
    oppConAdd4.Role__c = Constants.OPPTY_CONTACT_ROLE_SHIPTO;
    oppConAdds.add(oppConAdd4);

    Opportunity_Contact_Address__c oppConAdd5 = new Opportunity_Contact_Address__c();
    oppConAdd5.Opportunity__c= testOpp.Id ;
    oppConAdd5.Address__c = conAdd5.Address__c;
    oppConAdd5.Contact__c = newcontact5.Id;
    oppConAdd5.Role__c = Constants.OPPTY_CONTACT_ROLE_UPDATE;
    oppConAdds.add(oppConAdd5);
    
    Opportunity_Contact_Address__c oppConAdd6 = new Opportunity_Contact_Address__c();
    oppConAdd6.Opportunity__c= testOpp.Id ;
    oppConAdd6.Address__c = conAdd6.Address__c;
    oppConAdd6.Contact__c = newcontact6.Id;
    oppConAdd6.Role__c = Constants.OPPTY_CONTACT_ROLE_UPDATE;
    oppConAdds.add(oppConAdd6);

    insert oppConAdds;

    //Test_Utils.insertEDQOpportunityContactRoles(testOpp.Id, newContact.Id);
    //OpportunityContactRole oppContactRole = Test_Utils.insertOpportunityContactRole(true, testOpp.Id, newcontact.Id, Constants.DECIDER, true);
    //DO Case #02137101: Added all roles to check sync Contact role address validation
      OpportunityContactRole oppContactRole =  Test_Utils.insertOpportunityContactRole(false, testOpp.Id, newcontact.Id, Constants.DECIDER, true);

      OpportunityContactRole oppConRole1 = Test_Utils.insertOpportunityContactRole(false, testOpp.Id, newcontact6.Id, Constants.OPPTY_CONTACT_ROLE_PURCHASE_LEDGER, false);

      OpportunityContactRole oppConRole2 = Test_Utils.insertOpportunityContactRole(false, testOpp.Id, newcontact1.Id, Constants.OPPTY_CONTACT_ROLE_RENEWAL, false);

      OpportunityContactRole oppConRole3 = Test_Utils.insertOpportunityContactRole(false, testOpp.Id, newcontact2.Id, Constants.OPPTY_CONTACT_ROLE_UPDATE, false);

      OpportunityContactRole oppConRole4 = Test_Utils.insertOpportunityContactRole(false, testOpp.Id, newcontact3.Id, Constants.OPPTY_CONTACT_ROLE_COMMERCIAL, false);

      OpportunityContactRole oppConRole5 = Test_Utils.insertOpportunityContactRole(false, testOpp.Id, newcontact4.Id, Constants.OPPTY_CONTACT_ROLE_SHIPTO, false);

      OpportunityContactRole oppConRole6 = Test_Utils.insertOpportunityContactRole(false, testOpp.Id, newcontact5.Id, Constants.OPPTY_CONTACT_ROLE_CONTRACTUAL, false);

      insert new List<OpportunityContactRole>{oppContactRole,oppConRole1,oppConRole2,oppConRole3,oppConRole4,oppConRole5,oppConRole6};
      

    // Create Opportunity Line Item
    product = Test_Utils.insertProduct();
    product.RevenueScheduleType = Constants.REVENUE_SCHEDULED_TYPE_REPEAT;
    product.RevenueInstallmentPeriod = Constants.INSTALLMENT_PERIOD_DAILY;
    product.NumberOfRevenueInstallments = 2;
    //product.CanUseQuantitySchedule = false;
    product.CanUseRevenueSchedule = true;

    update product;
    PricebookEntry stdPricebookEntry = Test_Utils.insertPricebookEntry(product.Id, Test.getStandardPricebookId(), Constants.CURRENCY_USD);
    //insert OLI
    oli2 = Test_Utils.createOpportunityLineItem(testOpp.Id, stdPricebookEntry.Id, testOpp.Type);
    oli2.Start_Date__c = Date.today().addDays(5);
    oli2.End_Date__c = System.today().addDays(10);
    oli2.CPQ_Quantity__c = 1000;
    oli2.Type__c = Constants.OPPTY_LINE_ITEM_TYPE_RENEWAL;//Case #02088090
    insert oli2;
   

    //insert OLIS
    OpportunityLineItemSchedule olsi1 =  Test_Utils.createOpportunityLineItemSchedule(oli2.id);
    olsi1.ScheduleDate = System.today().addDays(5);
    olsi1.Revenue = oli2.TotalPrice / 3;
    OpportunityLineItemSchedule olsi2 =  Test_Utils.createOpportunityLineItemSchedule(oli2.id);
    olsi2.ScheduleDate = olsi1.ScheduleDate.addDays(5);
    olsi2.Revenue = oli2.TotalPrice / 3;
    OpportunityLineItemSchedule olsi3 =  Test_Utils.createOpportunityLineItemSchedule(oli2.id);
    olsi3.ScheduleDate = olsi2.ScheduleDate.addDays(5);
    olsi3.Revenue = oli2.TotalPrice / 3;

    List<OpportunityLineItemSchedule> opptySchedules = new List<OpportunityLineItemSchedule>();
    opptySchedules.add(olsi1);
    opptySchedules.add(olsi2);
    opptySchedules.add(olsi3);
    insert opptySchedules;

    testAccount.Is_Competitor__c = true;
    update testAccount;
    Competitor__c comp = Test_Utils.createCompetitor(testOpp.Id);
    insert comp;
    CPQ_Settings__c cs = new CPQ_Settings__c();
    cs.Name = 'CPQ';
    cs.CPQ_USERTYPE_BIS_MANAGER__c = 'BIS Manager';
    cs.CPQ_USERTYPE_BIS_STRATEGIC_PRICING__c = 'BIS Strategic Pricing';
    insert cs;
  }
}