public class ResellerCommVFExt{

    public Case newCase {get;set;}
    private Final Case origCase {get;set;}
    public ID CIS {get;set;}
    public ID BIS {get;set;}
    private Case tempCase{get;set;}
    public String selectedType {get;set;}
    public String selectedCode {get;set;}
    public List<String> subcode {get;set;}
    public boolean typeDisable {get;set;}
    
    public List<SelectOption> getCaseTypes() {
        List<SelectOption> caseOptions = new List<SelectOption>();
        caseOptions.add(new SelectOption('CSDA BIS Support','CSDA BIS Support'));
        caseOptions.add(new SelectOption('CSDA CIS Support','CSDA CIS Support'));
        
        return caseOptions;
    }

    
    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public ResellerCommVFExt(ApexPages.StandardController stdController) {
        //get subcodes
        subcode = new List<String>();
        List<User> curUser = [Select id, Contactid, Contact.Accountid from User where id =: UserInfo.getUserId()];
        if (curUser.size() > 0 ) {
            if (curUser[0].Contact.Accountid != null){
            List<Account> userAcc = [Select id, (Select id, Subscriber_Code__c from Sub_Codes__r) from Account where id =: curUser[0].Contact.Accountid];
            if (userAcc.size() > 0) {
                for(Sub_Code__c sc : userAcc[0].Sub_Codes__r) {
                    subcode.add(sc.Subscriber_Code__c);
                }
            }
            }
        }
        
        typeDisable = false;
        
        String caseID = ApexPages.currentPage().getParameters().get('id');
        CIS = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CSDA CIS Support').getRecordTypeId();
        BIS = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CSDA BIS Support').getRecordTypeId();
        //System.debug('path1 ' + Site.getPathPrefix());
        if (caseID != null) {
            this.origCase = (Case)stdController.getRecord();
            List<Case> newCaseList = [Select Id, Type, Priority, Reason, Status, Subject, Description, Subcode__c, Account_Sales_Support_Type__c, Secondary_Case_Reason__c, RecordType.Name from Case where id =: this.origCase.ID];
            this.newCase= newCaseList[0].clone(false);
            System.debug('this.origCase.ID '+ this.origCase.ID);
            newCase.status ='New';
            
            selectedType = newCase.RecordType.Name;
            typeDisable = true;
            selectedCode = newCase.Subcode__c;
            
        } else {
            
             
            this.newCase = (Case)stdController.getRecord();
            //newCase.type = 'GCSS Support';  
            newCase.status ='New';
            newCase.RecordTypeID = BIS;
            
        }
        
        //newCase.User_Requestor__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
        //newCase.Requestor_Email__c = UserInfo.getUserEmail();
        //newCase.Requestor_Work_Phone__c = curUser.Phone;
        newCase.Priority = 'P3';
        newCase.Origin = 'Community';
    }
    
        
    public List<SelectOption> getSubCodes() {
        
        List<SelectOption> codeOptions = new List<SelectOption>();
        codeOptions.add(new SelectOption('','--None--'));
        if (subcode.size() > 0) {
            for(String code: subcode) {
                codeOptions.add(new SelectOption(code,code));
            }
            
        }
        return codeOptions;
    }
    
    public pagereference Save() {    
    System.debug('new Case ' + newCase);
        newCase.Subcode__c = selectedCode;
        insert newCase;
        PageReference pg = new PageReference(Site.getPathPrefix()+'/s/case/' + newCase.id);
        pg.setredirect(true);
        return pg;
    }
    
    public pagereference Cancel() {
        PageReference returnPage = new PageReference(Site.getPathPrefix());
        return returnPage;
    }
    
    public void changeRType() {
       
        if (selectedType == 'CSDA CIS Support') {           
            newCase.RecordTypeID = CIS;
            SYstem.debug('CIS');
        } else if(selectedType == 'CSDA BIS Support'){
            newCase.RecordTypeID = BIS;
            SYstem.debug('BIS');
        }
    }
}