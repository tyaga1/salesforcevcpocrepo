global class ScheduleAccountPlanTeamUpdate implements Schedulable
{
/**=====================================================================
 * Experian, Inc
 * Name: ScheduleAccountPlanTeamUpdate 
 * Description: The following Schedulable Class is used to schedule batch class BatchAccountPlanTeamUpdate.
 * Created Date: 
 * Created By: Tyaga Pati (Experian)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
  
  global void execute(SchedulableContext sc)
  {
    BatchAccountPlanTeamUpdate batchToProcess = new BatchAccountPlanTeamUpdate();
    database.executebatch(batchToProcess);
  }
}