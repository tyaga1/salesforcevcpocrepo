/**=============================================================================
 * Experian plc
 * Name: BatchTrainingEmailReminder_Test
 * Test Class for: BatchTrainingEmailReminderForOnboarding
 * Description: Case #02069865 Custom object for Training management console
 * Created Date: March 23rd, 2017
 * Created By: Sanket Vaidya
 * 
 * Date Modified        Modified By                  Description of the update
 * 
 =============================================================================*/

@isTest
public class BatchTrainingEmailReminder_Test
{
	@isTest (SeeAllData=true) //To make custom settings available in batch file.
    public static void testBatchTrainingEmailRemdinerForOnboarding()
    {
        List<Contact> contacts = new List<Contact>();
        for(Integer i=0; i < 10; i++)
        {
        	contacts.add(new Contact(LastName='Test ' + i));
        }
        INSERT contacts;
        
        List<Training__c> listTraining = new List<Training__c>();
        
        System.Debug('Contacts : ' + contacts);
        
        for(Integer i=0; i < 10; i++)
        {
            Training__c t = new Training__c(Training_Type__c = 'On-boarding', Contact__c = contacts[i].Id, On_Boarding_Comm_Date_Sent__c = (Date.TODAY()-30));
            listTraining.add(t);
        }
        
        INSERT listTraining;
        
        database.executebatch(new BatchTrainingEmailReminderForOnBoarding());
    }
    	
}