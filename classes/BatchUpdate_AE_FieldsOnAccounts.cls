/******************************************************************************
 * Name: BatchUpdate_AE_FieldsOnAccounts
 * Description: Updates the BIS and CIS fields on Accounts
 *              when an isDataAdmin user updates then on the AssignmentTeam, thus circumventing the AssignmentTeamTriggerHandler, which
 *              performs this update for normal users.
 *
 * Created Date: 8th Sept. 2017
 * Created By: James Wills
 *
 * Date Modified                Modified By                  Description of the update
 * 18th Sept. 2017              James Wills                  Update to execute method.
 ******************************************************************************/
global class BatchUpdate_AE_FieldsOnAccounts implements Database.Batchable<sObject>, Database.Stateful, Schedulable {

  private DateTime currentRunTime;
  private DateTime lastRunTime;
  
  global void execute(SchedulableContext sc){
    BatchUpdate_AE_FieldsOnAccounts b = new BatchUpdate_AE_FieldsOnAccounts();
    Database.executebatch(b);
  }
    
  //========================================================================================
  // Start
  //========================================================================================
  global Database.QueryLocator start(Database.BatchableContext BC){
    
    currentRunTime = System.now();
    lastRunTime    = BatchHelper.getBatchClassTimestamp('BatchUpdate_AE_FieldsOnAccountsLastRun');
    
    
    return Database.getQueryLocator([SELECT id, Name, Account_Executive__c,
                                     (SELECT id, Account__c FROM Account_Assignment_Teams__r)
                                     FROM Assignment_Team__c 
                                     WHERE LastModifiedDate > :lastRunTime AND
                                     ((Name LIKE 'BIS - Growth%' OR Name LIKE 'BIS - Inside%') OR
                                      (Name LIKE 'CIS - Growth%' OR Name LIKE 'CIS - Inside%'))]);
  }
  
  //========================================================================================
  // Execute
  //========================================================================================
  global void execute(Database.BatchableContext BC, List<sObject> scope) {
    
    Map<ID,ID> aatBIS_AEs_And_Accounts_Map = new Map<ID,ID>();
    Map<ID,ID> aatCIS_AEs_And_Accounts_Map = new Map<ID,ID>();
          
    for(Assignment_Team__c at :(List<Assignment_Team__c>)scope){
      if(at.Name.LEFT(3)=='BIS'){
        for(Account_Assignment_Team__c aat : at.Account_Assignment_Teams__r){
          aatBIS_AEs_And_Accounts_Map.put(aat.Account__c, at.Account_Executive__c);
        }
      } else if(at.Name.LEFT(3)=='CIS'){
        for(Account_Assignment_Team__c aat : at.Account_Assignment_Teams__r){
          aatCIS_AEs_And_Accounts_Map.put(aat.Account__c, at.Account_Executive__c);
        }
      }
    }
         
    if(aatBIS_AEs_And_Accounts_Map.isEmpty()==false){

      List<Account> accList1                 = [SELECT id, BIS_Account_Executive__c, CIS_Account_Executive__c FROM Account WHERE id IN :aatBIS_AEs_And_Accounts_Map.keySet()];
      List<Account> accountsToUpdateBIS_List = new List<Account>();
      for(Account acc : accList1){
        //Check to see if the BIS AE has changed.
        if(acc.BIS_Account_Executive__c != aatBIS_AEs_And_Accounts_Map.get(acc.id)){
          acc.BIS_Account_Executive__c = aatBIS_AEs_And_Accounts_Map.get(acc.id);
          //Check if the role of the AE has changed from CIS to BIS, if it has then remove previous value.
          //if(acc.CIS_Account_Executive__c == acc.BIS_Account_Executive__c){
          //  acc.CIS_Account_Executive__c=null;
          //}
          accountsToUpdateBIS_List.add(acc);
        }
      }
      
      if(!accountsToUpdateBIS_List.isEmpty()){
        updateAccounts(accountsToUpdateBIS_List);
      }
    }

    if(aatCIS_AEs_And_Accounts_Map.isEmpty()==false){
      List<Account> accList2 = [SELECT id, CIS_Account_Executive__c, BIS_Account_Executive__c FROM Account WHERE id IN :aatCIS_AEs_And_Accounts_Map.keySet()];
      List<Account> accountsToUpdateCIS_List = new List<Account>();
      for(Account acc : accList2){
        if(acc.CIS_Account_Executive__c != aatCIS_AEs_And_Accounts_Map.get(acc.id)){
          acc.CIS_Account_Executive__c = aatCIS_AEs_And_Accounts_Map.get(acc.id);
          //Check if the role of the AE has changed from BIS to CIS, if it has then remove previous value.
          //if(acc.BIS_Account_Executive__c == acc.CIS_Account_Executive__c){
          //  acc.BIS_Account_Executive__c=null;
          //}
          accountsToUpdateCIS_List.add(acc);
        }
      }
      
      if(!accountsToUpdateCIS_List.isEmpty()){
        updateAccounts(accountsToUpdateCIS_List);
      }
    }
   
  }
  
  global void updateAccounts(List<Account> accountsToUpdate_List){
    try{
      update accountsToUpdate_List;
    } catch(Exception e){
      system.debug('\n[AssignmentTeamMemberTriggerHandler: updateAssignmentTeamMemberRoleFieldsOnAccount]: ['+e.getMessage()+']]');
      apexLogHandler.createLogAndSave('AssignmentTeamMemberTriggerHandler','updateAssignmentTeamMemberRoleFieldsOnAccount', e.getStackTraceString(), e);
    } 
  }
  
  //========================================================================================
  // Finish
  //========================================================================================
  global void finish(Database.BatchableContext BC) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchUpdate_AE_FieldsOnAccounts', true);    
    BatchHelper.setBatchClassTimestamp('BatchUpdate_AE_FieldsOnAccountsLastRun', currentRunTime);
  }

}