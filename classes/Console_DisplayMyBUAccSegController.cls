/**=====================================================================
 * Appirio, Inc
 * Name: Console_DisplayMyBUAccSegController
 * Aug 18th, 2015    Parul Gupta    Modified    T-426970 - Controller for Custom console component 
 																													 for Account Segments
 * Aug 25th, 2015    Parul Gupta    Modified    I-176975  Added getQuery() method
 * Aug 26th, 2015    Parul Gupta    Modified    I-177549 Modified Cancel() 
 * Sep 03rd, 2015    Parul Gupta    Modified    I-178954 - Modified constructor for initializing isDataAdmin 
 ========================================================================*/
public with sharing class Console_DisplayMyBUAccSegController {
	public Account acct                         {get;set;}
	public Account_Segment__c accSeg            {get;set;}
	public List<Schema.FieldSetMember> fieldSet {get;set;}
	public String segmentMapEntryName           {get;set;}
	public Boolean isEdit                       {get;set;}
	private User currentUser                    {get;set;}
	public boolean isDataAdmin                  {get;set;}

	// Constructor
	public Console_DisplayMyBUAccSegController (ApexPages.StandardController stdController) {
		isEdit = false;
		isDataAdmin = IsDataAdmin__c.getInstance().IsDataAdmin__c;
 		this.acct = (Account)stdController.getRecord(); 		
      
  	// Get current User
  	currentUser = [SELECT Id, Business_Unit__c, Business_Line__c, Global_Business_Line__c 
                 FROM User 
                 WHERE Id =: UserInfo.getUserId() Limit 1];

		// Get my business units's field set
  	Schema.FieldSet myBUFieldSet = getMyBUFieldSet(currentUser.Global_Business_Line__c, 
      											currentUser.Business_Line__c, currentUser.Business_Unit__c);
      
  	// If fieldset is available, get account segment record 
   	if ( myBUFieldSet != null && this.acct.id != null){
   		this.accSeg = getAccSeg();
    	if (accSeg != null) {
     		fieldSet = myBUFieldSet.getFields();
    	}
  	}
	}

	//===========================================================================
  // Get account segment record for user's business unit
  //===========================================================================
 	private Account_Segment__c getAccSeg() { 	
    List<sObject> sobjList = Database.query(getQuery());
	 	if (sobjList.size() > 0){
	  	return (Account_Segment__c)sobjList[0];
	 	}     
		return null;
	}
	
	//===========================================================================
  // Method returns query string
  //===========================================================================
	private String getQuery(){
		String query = '';
 		query = 'SELECT ';
    Integer i = 0;
    accSeg = new Account_Segment__c();
    for (Schema.SObjectField key : DescribeUtility.getFieldMap(accSeg).values()) {
      if (i == 0) {
        query += key.getDescribe().getName();
      } else {
        query += ', ' + key.getDescribe().getName();
      }
      i++;
    }
		    		    
    query += ', Segment__r.Value__c, Segment__r.Parent__r.Value__c, Segment__r.Parent__r.Parent__r.Value__c ';
    query += ', (SELECT CreatedById, CreatedBy.Name, CreatedDate, Field, OldValue, NewValue From Histories) ';
    query += ' FROM Account_Segment__c WHERE Account__c = \'' + acct.id 
    			+ '\' and Segment_Type__c = \'Business Unit\' and Segment_Value__c = \'' + currentUser.Business_Unit__c + '\'';
    return query;
	}
    
	//===========================================================================
  // Get Field set for User's Business Unit
  //===========================================================================
	private Schema.FieldSet getMyBUFieldSet (String gbl, String bl, String bu) {
  	List<Account_Segmentation_Mapping__c> accSegMapping = [SELECT Id, Name, Field_Set_API_Name__c
                                                       FROM Account_Segmentation_Mapping__c 
                                                       WHERE Business_Unit__c =: bu
                                                       AND Business_Line__c =: bl
                                                       AND Global_Business_Line__c =: gbl
                                                       LIMIT 1];  

	  if (accSegMapping.size() > 0) {
	    String fieldSetAPIName = accSegMapping[0].Field_Set_API_Name__c;
	    Map<String, Schema.FieldSet> fieldSets = Schema.SObjectType.Account_Segment__c.fieldSets.getMap();
	    if (!String.isEmpty(fieldSetAPIName) && fieldSets.containsKey(fieldSetAPIName)) {
	    	segmentMapEntryName = accSegMapping[0].Name;
	      return fieldSets.get(fieldSetAPIName);
	    }
	  } 
   	return null;
	}
    
 	//===========================================================================
  // Sets the isEdit parameter to display the inputFields
  //===========================================================================
  public PageReference edit() {
    isEdit = true;
    return null;
  }

  //===========================================================================
  // Saves the updated record to the database
  //===========================================================================
  public PageReference save() {
    if (accSeg.Segment__c == null) {
       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please fill Segment before saving'));
       return null;
    }
    
    try{
    	update accSeg;
    	isEdit = false;
    	this.accSeg = Database.query(getQuery());
    }catch(Exception ex){
    	String errorMessage = ex.getMessage();
    	Integer occurence;
	    if (ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
        occurence = errorMessage.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,') + 34;
        errorMessage = errorMessage.mid(occurence, errorMessage.length());
        occurence = errorMessage.lastIndexOf(':');
        errorMessage = errorMessage.mid(0, occurence);
	    }
	    else {
	    	errorMessage = ex.getMessage();
	    }	
	    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
    }
    return null;
  }

  //===========================================================================
  // Displays the read only mode
  //===========================================================================
  public PageReference cancel() {
    isEdit = false;
    this.accSeg = Database.query(getQuery());
    return null;
  }
}