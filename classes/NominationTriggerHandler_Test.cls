/**=====================================================================
 * Experian
 * Name: NominationTiggerHandler_Test
 * Description: Test class to cover  NominationTiggerHandler
 * Created Date: March 13th 2016
 * Created By: Richard Joseph
 *
 * Date Modified      Modified By                Description of the update
 * Apr 18th, 2016     Paul Kissick               Updated formatting
 * Sep  2nd, 2016     Diego Olarte               Replaced Old justification with new RichJustification__c field/ No longer needed and Comment out Cost center from test data as this is set by User.Oracle_Cost_Center__c now
 * Apr 20th,2017      Manoj Gopu                 Case:02374799 : Added code to cover the delete functionality
 =====================================================================*/
@isTest
private class NominationTriggerHandler_Test {
  
  private static testMethod void NominationTiggerHandlerTest() {
    List<User> usrList = new List<User>();
    User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    PermissionSet objP = [select id from PermissionSet where Name = 'Recognition_Data_Admin' ];
      PermissionSetAssignment objPer = new PermissionSetAssignment();
      objPer.AssigneeId= thisUser.Id;
      objPer.PermissionSetId = objP.Id;
      insert objPer;

    system.runAs(thisUser) {
      Profile p = [SELECT Id FROM PROFILE WHERE Name = :Constants.PROFILE_SYS_ADMIN ];
      UserRole copsRole = [SELECT Id FROM UserRole WHERE Name = :Constants.ROLE_NA_COPS];
      User testUser1 = Test_Utils.createEDQUser(p, 'test1234@experian.com', 'test1');
      User testUser2 = Test_Utils.createEDQUser(p, 'test1235@experian.com', 'test2');
      testUser1.UserRoleId = copsRole.Id;
      testUser2.UserRoleId = copsRole.Id;
      /*testUser1.Oracle_Cost_Center__c = '11111';
      testUser2.Oracle_Cost_Center__c = '11111';*/ // Commented by RJ for EMERG fix
              
      usrList.add(testUser1);
      usrList.add(testUser2);
      insert usrList;
      testUser1.Manager = testUser2;
      update testUser1;
      
      Document documentRec;
      documentRec = new Document();
      documentRec.Body = Blob.valueOf('Some Text');
      documentRec.ContentType = 'image/jpeg';
      documentRec.DeveloperName = 'my_document';
      documentRec.IsPublic = true;
      documentRec.Name = 'My Document';
      documentRec.FolderId = UserInfo.getUserId();
      insert documentRec;
      
      List<WorkBadgeDefinition>  wrkDefLst=new List<WorkBadgeDefinition>();
      wrkDefLst.add(new WorkBadgeDefinition(
        Name = 'Half Year Winner', 
        Description = 'Test Description', 
        ImageUrl = documentRec.Id, 
        IsActive = true
      ));
      wrkDefLst.add(new WorkBadgeDefinition(
        Name = 'Elite', 
        Description = 'Test Description', 
        ImageUrl = documentRec.Id, 
        IsActive = true
      ));
      wrkDefLst.add(new WorkBadgeDefinition(
        Name = 'Half Year Nomination', 
        Description = 'Test Description', 
        ImageUrl = documentRec.Id,
        IsActive = true
      ));
      wrkDefLst.add(new WorkBadgeDefinition(
        Name = 'Trophy', 
        Description = 'Test Description', 
        ImageUrl = documentRec.Id, 
        IsActive = true
      ));
      insert wrkDefLst;

      List<Nomination__c> nominationLst = new List<Nomination__c>();
      nominationLst.add(new Nomination__c(
        Nominee__c = TestUser1.Id, 
        Requestor__c = TestUser2.Id,
        Justification__c = 'Test Data',
        Type__c = 'H1',
        //Status__c = 'Approved'
        Status__c = 'Submitted'

      ));
      nominationLst.add(new Nomination__c(
        Nominee__c = TestUser1.Id,
        Requestor__c = TestUser2.Id,
        Justification__c = 'Test Data',
        Type__c = 'H1',
        //Status__c = 'Won'
        Status__c = 'Submitted'

      ));
      nominationLst.add(new Nomination__c(
        Nominee__c = TestUser1.Id, 
        Requestor__c = TestUser2.Id,
        Justification__c = 'Test Data',
        Type__c = 'Elite',
        //Status__c = 'Won'
        Status__c = 'Submitted'
      ));
      nominationLst.add(new Nomination__c(
        Nominee__c = TestUser1.Id,
        Requestor__c = TestUser2.Id,
        Justification__c = 'Test Data',
        Type__c = 'Level 2',
        //Status__c = 'Approved',
        Status__c = 'Submitted',
        Spot_Award_Amount__c = '$25'
        
      ));
      
      Nomination__c objN = new Nomination__c(
        Nominee__c = TestUser1.Id,
        Requestor__c = TestUser2.Id,
        Justification__c = 'Test Data',
        Type__c = 'Level 2',
        //Status__c = 'Approved',
        Status__c = 'Submitted',
        Spot_Award_Amount__c = '$25'
        
      );
      nominationLst.add(objN);

      Test.startTest();
      insert nominationLst;
      update nominationLst;
      delete objN;
      Nomination__c nominationtst= [Select id, OwnerId from Nomination__c where Type__c = 'Level 2' and Id in :nominationLst];
      Test.stopTest();
      System.assertEquals(testUser2.Id,nominationtst.ownerid);
      
      
    }
  }
      
}