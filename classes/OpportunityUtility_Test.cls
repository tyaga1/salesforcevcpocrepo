/**=====================================================================
 * Appirio, Inc
 * Name: OpportunityUtility_Test
 * Description: Test class for OpportunityUtility
 * Created Date: Sep 9th, 2015
 * Created By: Parul Gupta (Appirio)
 * 
 * Date Modified      Modified By                Description of the update
 * Apr 7th, 2016      Paul Kissick               Case 01932085: Fixing Test User Email Domain
 =====================================================================*/
@isTest
private class OpportunityUtility_Test {

    static testMethod void testOpportunityUtility() {
    
    // create User
    Profile p = [SELECT Id FROM Profile WHERE Name=: Constants.PROFILE_SYS_ADMIN ];
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    insert testUser1;
  
    System.runAs(testUser1) {
      // Create an account    
      Account testAccount = Test_Utils.insertAccount();
      
      Contact con = Test_Utils.insertContact(testAccount.Id);
      
      // Create an opportunity
      Opportunity testOpp = Test_Utils.createOpportunity(testAccount.Id);
      testOpp.Contract_Start_Date__c = Date.today().addDays(-20);
      testOpp.Contract_End_Date__c = Date.today().addDays(10);
      testOpp.CloseDate = Date.today().addDays(5);
      testOpp.Type = Constants.OPPTY_NEW_FROM_NEW;
      insert testOpp;
        
      //////////////////////
      // Create Opportunity Line Item
      Product2 product = Test_Utils.createProduct();
      product.Simple_or_Complex__c = Constants.PRODUCT_COMPLEX;
      product.CanUseRevenueSchedule = true;
      insert product;
  
      PricebookEntry stdPricebookEntry = Test_Utils.insertPricebookEntry(product.Id, Test.getStandardPricebookId(), Constants.CURRENCY_USD);
      
      //insert OLI
      OpportunityLineItem opptyLineItem = Test_Utils.createOpportunityLineItem(testOpp.Id, stdPricebookEntry.id, testOpp.Type);
      opptyLineItem.Start_Date__c = Date.today().addDays(-20);
      opptyLineItem.End_Date__c = Date.today().addDays(2);
      insert opptyLineItem;
  
      OpportunityLineItemSchedule olis = Test_Utils.createOpportunityLineItemSchedule(opptyLineItem.ID);
      olis.ScheduleDate = Date.today().addDays(-40);
      insert olis;   
      
      List<Account_Segment__c> listAccSegments = createHierarchies(testAccount);
      List<Account_Segment__c> accSegments = [Select id, Segment_Type__c, (Select id from Orders_Business_Units__r), 
																			      (Select id from Orders_Business_Lines__r), 
																			      (Select id from Orders_Countries__r), 
																			      (Select id from Orders_Global_Business_Lines__r), 
																			      (Select id from Orders_Regions__r) 
																			      from Account_Segment__c where id in : listAccSegments];
      
      Order__c testOrdr = Test_Utils.insertOrder (false, testAccount.Id , con.Id, testOpp.Id);
        testOrdr.Segment_Business_Unit__c = listAccSegments[0].Id;
        testOrdr.Segment_Business_Line__c = listAccSegments[1].Id;
        testOrdr.Segment_Country__c = listAccSegments[2].Id;
        testOrdr.Segment_Global_Business_Line__c = listAccSegments[3].Id;
        testOrdr.Segment_Region__c = listAccSegments[4].Id;
        testOrdr.Amount_Corp__c = 10; 
				insert testOrdr;
				
      Test.startTest();
      
  		OpportunityUtility.checkOpportunityDates(testOpp.ID); 
  		
  		Set<Id> orderIds = OpportunityUtility.getAllRelatedOrderIds(accSegments);
  		for(Account_Segment__c accSegment : accSegments){
  			list<Opportunity> oppList = OpportunityUtility.getRelatedOpportunities(accSegment);  
  			list<Order__c> orderList = OpportunityUtility.getRelatedOrders(accSegment);
  		}  
  		
      Test.stopTest();      
    }
		}
		
		private static List<Account_Segment__c> createHierarchies(Account testAccount) {
        List<Hierarchy__c> lstHierarchy = new List<Hierarchy__c>();
     Hierarchy__c hierarchy_BusinessUnit = 
     Test_Utils.insertHierarchy(false, null, 'APAC Corporate Finance', 'Business Unit');
     lstHierarchy.add(hierarchy_BusinessUnit);
     Hierarchy__c hierarchy_BusinessLine = 
     Test_Utils.insertHierarchy(false, null, 'APAC Corporate', 'Business Line');
     lstHierarchy.add(hierarchy_BusinessLine);
     Hierarchy__c hierarchy_Country = 
     Test_Utils.insertHierarchy(false, null, 'India', 'Country');
     lstHierarchy.add(hierarchy_Country);
     Hierarchy__c hierarchy_GlobalBusinessLine = 
     Test_Utils.insertHierarchy(false, null, 'Corporate', 'Global Business Line');
     lstHierarchy.add(hierarchy_GlobalBusinessLine);
     Hierarchy__c hierarchy_Region = 
     Test_Utils.insertHierarchy(false, null, 'Global', 'Region');
     lstHierarchy.add(hierarchy_Region);
     insert lstHierarchy;    //insertion of hierarchy records
     
      // Insert Account Segment    
    List<Account_Segment__c> listAccSegments = new List<Account_Segment__c>();
    Account_Segment__c accSegment_BusinessLine = Test_Utils.insertAccountSegment(false, testAccount.Id, hierarchy_BusinessUnit.Id, null);
    listAccSegments.add(accSegment_BusinessLine);
    
    Account_Segment__c accSegment_BusinessUnit = Test_Utils.insertAccountSegment(false, testAccount.Id, hierarchy_BusinessLine.Id, null);
    listAccSegments.add(accSegment_BusinessUnit);
    
    Account_Segment__c accSegment_Country = Test_Utils.insertAccountSegment(false, testAccount.Id, hierarchy_Country.Id, null);
    listAccSegments.add(accSegment_Country);
    
    Account_Segment__c accSegment_GlobalBusinessLine = Test_Utils.insertAccountSegment(false, testAccount.Id, hierarchy_GlobalBusinessLine.Id, null);
    listAccSegments.add(accSegment_GlobalBusinessLine);
    
    Account_Segment__c accSegment_Region = Test_Utils.insertAccountSegment(false, testAccount.Id, hierarchy_Region.Id, null);
    listAccSegments.add(accSegment_Region);
    
    insert listAccSegments;
    return listAccSegments;
  }
}