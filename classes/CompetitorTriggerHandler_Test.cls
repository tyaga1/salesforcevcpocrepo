/**=====================================================================
 * Appirio, Inc
 * Name        : CompetitorTriggerHandler_Test
 * Description : Handler class for CompetitorTriggerHandler (for T-291592)
 * Created Date: Jul 4th, 2014
 * Created By  : Sonal Shrivastava (Appirio JDC)
 
 Modified On                Modified By                   Description
 * May 25th, 2016           Tyaga Pati                    Case 01996424 - Test Classes for the Savo integration changes on Product Master to Opportunity
 * April 3rd, 2017			Sanket Vaidya	 			  Case 02150014: CRM 2.0- Opportunity Competitor Information - Added updatePrimaryCompetitor()
  =====================================================================*/
@isTest 
public class CompetitorTriggerHandler_Test {
  
  static testMethod void testCompetitorTrigger() {
    // insert accounts
    Account account1 = Test_Utils.insertAccount();
    Account account2 = Test_Utils.insertAccount();
    account2.Is_Competitor__c = true;
    update account2;
    
    // insert opportunity
    Opportunity opportunity = Test_Utils.createOpportunity(account1.Id);        
    insert opportunity;
    List<Opp_Plan_Score_Calc__c> opsc = new List<Opp_Plan_Score_Calc__c>();
       List<String> namelist = new List<String>{'Information Scoring','Qualification Scoring','Buying Centre','Competition Scoring','Summary Position','Solution at a Glance','Joint Action Plan','Value Proposition','Action Plan'};
       Integer i = 0;
       For(String name : namelist) {
       Opp_Plan_Score_Calc__c op1 = Test_Utils.insertOppPlanScoreCalc(name,true);
       Opp_Plan_Score_Sub_Calc__c op2 = Test_Utils.insertOppPlanScoreSubCalc(op1.Id,true);
      
       }
        
    // insert Opp Plan 
    Opportunity_Plan__c oppPlan = Test_Utils.insertOpportunityPlan(true, opportunity.id);
    
    Test.startTest();
    
    //Insert Competitor
    Competitor__c comp = new Competitor__c(Opportunity__c = opportunity.Id, Account__c = account2.Id);
    insert comp;
    
    //Verify that Opportunity Plan Competitor record is created on Opportunity Plan      
    System.assertEquals(1, [SELECT Id FROM Opportunity_Plan_Competitor__c WHERE Opportunity_Plan__c = :oppPlan.Id].size());
    
    //Delete Competitor
    delete comp;
    
    //Verify that Opportunity Plan Competitor record is deleted on Opportunity Plan      
    System.assertEquals(0, [SELECT Id FROM Opportunity_Plan_Competitor__c WHERE Opportunity_Plan__c = :oppPlan.Id].size());
    
    Test.stopTest();
  }
//Test Class to Test the updateCompetitorFuseTagId function in the  CompetitorTriggerHandler
//This class is responsible for updating the FuseTag at Opty Level when Insert or update
//Happens at the class level.
 
  static testMethod void testupdateCompetitorFuseTagId() {
    // insert accounts
    Account account1 = Test_Utils.insertAccount();
    Account account2 = Test_Utils.insertAccount();
    account2.Is_Competitor__c = true;
    account2.FUSE_Competitor_Name__c= '11111111';
    update account2;
    Opportunity oppty = Test_Utils.createOpportunity(account1.Id); 
    insert oppty;
    Test.startTest();
    Competitor__c comp = new Competitor__c(Opportunity__c = oppty.Id, Account__c = account2.Id);
    insert comp;
    
    //Verify that Opportunity Plan Competitor record is deleted on Opportunity Plan      
    System.assertEquals('11111111', [SELECT FuseCompetitorTag__c FROM Opportunity WHERE ID = :oppty.Id].FuseCompetitorTag__c);
    
    Test.stopTest();
  }
  

  @isTest
  public static void Update_Primary_Competitor_Flag()
  {
        Account acc1 = Test_Utils.insertAccount(); //Account 1
        acc1.Is_Competitor__c = true;
        UPDATE acc1;
                
        Opportunity opp1 = Test_Utils.insertOpportunity(acc1.Id);
                
        List<Competitor__c> listCompetitors = new List<Competitor__c>();
      	
      	for(Integer i=0; i < 10; i++)
        {
            Competitor__c comp = new Competitor__c(Account__c = acc1.Id, Opportunity__c = opp1.Id);
            if(Math.mod(i, 2) == 0)
            {
                comp.Primary_Competitor__c = True;
            }
            listCompetitors.add(comp);
        }
      
       INSERT listCompetitors; 
  }
    
}