/**=====================================================================
 * Experian
 * Name: SMXNACSClBISSupProCaseBtchSdlrCntxtTest
 * Description: 
 * Created Date: 22/07/2016
 * Created By: Diego Olarte
 * 
 * Date Modified     Modified By        Description of the update
 =====================================================================*/
@isTest
private class SMXNACSClBISSupProCaseBtchSdlrCntxtTest {
  
  static testmethod void testBatch(){
    
    system.assertEquals(0,[SELECT COUNT() FROM Feedback__c]);
    
    Test.startTest();

    SMXNACSClntBISSupportProcessCaseBatch b = new  SMXNACSClntBISSupportProcessCaseBatch();
    Database.executeBatch(b);

    Test.stopTest();
    
   // system.assertEquals(1,[SELECT COUNT() FROM Feedback__c WHERE DataCollectionId__c = :SMXNACSClntBISSupportProcessCaseBatch.strSurveyId]);

  }
 
  @testSetup
  private static void prepareTestData() {
    Account a = Test_Utils.insertAccount();
    
    Contact c = Test_Utils.insertContact(a.Id);
    
    User testUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    testUser.Global_Business_Line__c = 'Corporate';
    testUser.Business_Line__c = 'Corporate';
    testUser.Business_Unit__c = 'APAC:SE';
    testUser.Region__c = 'North America';
    insert testUser;
    system.runAs(testUser){
        Case cs = new Case(
          Subject = 'CCC1', 
          AccountId = a.Id, 
          Status = 'Closed Resolved', 
          ContactId = c.Id,
          ClosedDate = Datetime.now().addHours(-2),
          CreatedDate = Datetime.now().addDays(-2).addHours(-1),
          Type = 'TEST',
          Reason = 'TEST',
          Origin = 'Email',
          RecordTypeId = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_CASE , 'CSDA BIS Support')
        );
        insert cs;
    }

  }

    
}