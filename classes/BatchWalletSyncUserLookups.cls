/**=====================================================================
 * Experian
 * Name: BatchWalletSyncUserLookups
 * Description: Batch to set the user lookups on the WalletSync records
 *  This should be the start batch for the wallet -> accounts sync process
 * Created Date: 3rd, August, 2015
 * Created By: Paul Kissick
 *
 * Date Modified       Modified By          Description of the update
 * Aug 20th, 2015      Paul Kissick         Adding NULLs to clear employee lookups prior to setting (in case they are changed and the user is not found)
 * Aug 25th, 2015      Paul Kissick         Adding check for Employee Number != 0
 * Aug 28th, 2015      Paul Kissick         Adding Previous Account Supervisor to the process.
 * Sep 10th, 2015      Paul Kissick         Added try/catch around email sending & changed email wording.
 * Oct 1st, 2015       Paul Kissick         Changing lastStartDate to Wallet_Sync_Processing_Last_Run__c
 * Nov 9th, 2015       Paul Kissick         Case 01234035 : Adding failure notification management.
 * Nov 27th, 2015      Paul Kissick         Case 01266075: Replacing Global_Setting__c with new Batch_Class_Timestamp__c
 * May 3rd, 2016       Paul Kissick         Case 01972593: Adding checks for Sales (not chatter) users.
 * Oct 19th, 2016      Manoj Gopu           Case 02172178: Replaced Employee number with the Serasa Employee Number
 * Oct 31st, 2016      Manoj Gopu           Case 01268343: Updated the Code in order to check for Active/Inactive User Emp Code
 =====================================================================*/

global class BatchWalletSyncUserLookups implements  Database.Batchable<sObject>, Database.Stateful {

  global Map<String, Integer> employeeCodeToNumberOfRecords;
  global List<String> updateErrors;
  global Map<String, String> employeeCodeToUserType;
  
  global Database.QueryLocator start(Database.BatchableContext BC) {
    // Initialise the global vars
    employeeCodeToNumberOfRecords = new Map<String, Integer>();
    updateErrors = new List<String>();
    employeeCodeToUserType = new Map<String, String>();
    
    Datetime lastStartDate = WalletSyncUtility.getWalletSyncProcessingLastRun();
    return Database.getQueryLocator([
      SELECT Id, Account_Owner_Employee_Code__c, 
        Account_Supervisor_Emp_Code__c, Previous_Account_Owner_Employee_Code__c,
        Account_Owner__c, Account_Supervisor__c, Previous_Account_Owner__c, 
        Previous_Account_Supervisor_Emp_Code__c, Previous_Account_Supervisor__c
      FROM WalletSync__c
      WHERE Last_Processed_Date__c >= :lastStartDate
    ]);
  }
  
  global void execute(Database.BatchableContext BC, List<WalletSync__c> walletSyncs) {
    // this will be batched in suitable sizes....
    Map<String,Id> employeeCodeToUserIdMap = new Map<String,Id>();
    // First, run through each record and pull out the Employee Codes. This will be used to populate a map of code to id
    for (WalletSync__c ws : walletSyncs) {
      if (String.isNotBlank(ws.Account_Owner_Employee_Code__c) && ws.Account_Owner_Employee_Code__c != '0') {
        employeeCodeToUserIdMap.put(ws.Account_Owner_Employee_Code__c,null);
      }
      if (String.isNotBlank(ws.Account_Supervisor_Emp_Code__c) && ws.Account_Supervisor_Emp_Code__c != '0') {
        employeeCodeToUserIdMap.put(ws.Account_Supervisor_Emp_Code__c,null);
      }
      if (String.isNotBlank(ws.Previous_Account_Owner_Employee_Code__c) && ws.Previous_Account_Owner_Employee_Code__c != '0') {
        employeeCodeToUserIdMap.put(ws.Previous_Account_Owner_Employee_Code__c,null);
      }
      if (String.isNotBlank(ws.Previous_Account_Supervisor_Emp_Code__c) && ws.Previous_Account_Supervisor_Emp_Code__c != '0') {
        employeeCodeToUserIdMap.put(ws.Previous_Account_Supervisor_Emp_Code__c,null);
      }
    }
    
    // Now we have some employee codes, lets get the user ids...
    // Case 02172178 MG - changed EmployeeNumber to Serasa_Employee_Number__c
    Map<Id, User> userMap = new Map<Id, User>([
      SELECT Id, Serasa_Employee_Number__c, UserType
      FROM User 
      WHERE  Serasa_Employee_Number__c IN :employeeCodeToUserIdMap.keySet() order by isActive      
    ]); // Added by Manoj
    for (User u : userMap.values()) {
      employeeCodeToUserIdMap.put(u.Serasa_Employee_Number__c,u.Id);
    }
    
    // Great, now lets get these walletsync records updated....
    for (WalletSync__c ws : walletSyncs) {
      // Account Owner
      setWalletSyncOwnerField ('Account_Owner_Employee_Code__c', 'Account_Owner__c', ws, userMap, employeeCodeToUserIdMap);
      // Account Supervisor
      setWalletSyncOwnerField ('Account_Supervisor_Emp_Code__c', 'Account_Supervisor__c', ws, userMap, employeeCodeToUserIdMap);
      // Previous Account Owner
      setWalletSyncOwnerField ('Previous_Account_Owner_Employee_Code__c', 'Previous_Account_Owner__c', ws, userMap, employeeCodeToUserIdMap);
      // Previous Account Supervisor
      setWalletSyncOwnerField ('Previous_Account_Supervisor_Emp_Code__c', 'Previous_Account_Supervisor__c', ws, userMap, employeeCodeToUserIdMap);
    }
    List<Database.SaveResult> wsUpdRes = Database.update(walletSyncs,false);
    for (Database.SaveResult sr : wsUpdRes) {
      if (!sr.isSuccess()) {
        for (Database.Error err : sr.getErrors()) {
          updateErrors.add(err.getMessage());
        }
      }
    }
  }
  
  global void incrementMissingEmployeeCode(String code) {
    if (String.isNotBlank(code)) {
      if (!employeeCodeToNumberOfRecords.containsKey(code)) {
        employeeCodeToNumberOfRecords.put(code,0);
      }
      Integer holdingNum = employeeCodeToNumberOfRecords.get(code) + 1;
      employeeCodeToNumberOfRecords.put(code,holdingNum);
    }
  }
  
  private void setWalletSyncOwnerField (String empCodeFieldName, String destFieldName, WalletSync__c ws, Map<Id, User> userMap, Map<String,Id> employeeCodeToUserIdMap) {
    String empCode = (String)ws.get(empCodeFieldName);
    if (String.isNotBlank(empCode)) {
      ws.put(destFieldName, null);
      if (employeeCodeToUserIdMap.containsKey(empCode) && 
          employeeCodeToUserIdMap.get(empCode) != null) {
        Id ownerId = employeeCodeToUserIdMap.get(empCode);
        system.debug(userMap.get(ownerId));
        if (userMap.get(ownerId).UserType == 'Standard') {
          ws.put(destFieldName, ownerId);
        }
        else {
          employeeCodeToUserType.put(empCode, userMap.get(ownerId).UserType);
          incrementMissingEmployeeCode(empCode);
        }
      }
      else {
        incrementMissingEmployeeCode(empCode);
      }
    }
    // return ws;
  }
  
  global void finish(Database.BatchableContext BC) {
    try {
      BatchHelper bh = new BatchHelper();
      bh.checkBatch(BC.getJobId(), 'BatchWalletSyncUserLookups', false);
      
      if (!updateErrors.isEmpty() || Test.isRunningTest()) {
        bh.batchHasErrors = true;
        bh.emailBody += '\nThe following errors were observed when updating records:\n';
        bh.emailBody += String.join(updateErrors,'\n');
      }
      
      bh.sendEmail();
      
      // Send other email for any unmatched
      
      if (!employeeCodeToNumberOfRecords.isEmpty() || Test.isRunningTest()) {
        // found problems with the employee codes...
        String emailBodyCodes = 'The BatchWalletSyncUserLookups batch job failed to find ' + employeeCodeToNumberOfRecords.size() + ' employee codes.\n\n';
        emailBodyCodes += 'Batch process cannot find matching Users in Salesforce for the following Employee Codes. Please make sure Serasa Users are set up ' +
          ' in Salesforce with the correct Employee Number. This is used to identify the Account Owner and Account Supervisor in the Wallet System and to update ' +
          ' the Account Team in Salesforce.\n\n';
        emailBodyCodes += 'Emp Code : Records Affected\n---------------------------\n';
        
        // Get picklist labels for UserType on User
        
        Map<String, String> userTypeValueMap = new Map<String, String>();
        
        for (Schema.PicklistEntry pe : User.UserType.getDescribe().getPicklistValues()) {
          userTypeValueMap.put(pe.getValue(), pe.getLabel());
        }
        
        for (String code : employeeCodeToNumberOfRecords.keySet()) {
          emailBodyCodes += code + ' : ' + employeeCodeToNumberOfRecords.get(code) + ' Records' + 
                            (employeeCodeToUserType.containsKey(code) ? ' - Type = ' + userTypeValueMap.get(employeeCodeToUserType.get(code)) : '') + '\n';
        }
        try {
          Messaging.SingleEmailMessage mail2 = new Messaging.SingleEmailMessage();
          mail2.setSaveAsActivity(false);
          mail2.setToAddresses(FailureNotificationUtility.retrieveRecipients('BatchWalletSyncUserLookups-LookupFails'));
          mail2.setSubject('Force.com BatchWalletSyncUserLookups Lookup Failures');
          mail2.setBccSender(false);
          mail2.setUseSignature(false);
          mail2.setHtmlBody(emailBodyCodes.replace('\n','<br>'));
          mail2.setPlainTextBody(emailBodyCodes);
          if (!Test.isRunningTest()) {
            Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mail2});
          }
        }
        catch (System.EmailException e) {
          system.debug(e.getMessage());
        }
      }
    }
    catch (Exception e) {
      system.debug(e.getMessage()); 
    }
    
    if (!Test.isRunningTest()) { // Must be within this check as tests cannot start other batches
      // THIS BATCH, WHEN FINISHED, STARTS THE NEXT BATCH OF ACCOUNT LOOKUPS
      system.scheduleBatch(new BatchWalletSyncAccountLookups(), 'BatchWalletSyncAccountLookup'+String.valueOf(Datetime.now().getTime()), 0, ScopeSizeUtility.getScopeSizeForClass('BatchWalletSyncAccountLookup'));
    }
  }
  
}