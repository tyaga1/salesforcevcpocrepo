@istest(seeAllData=TRUE)
global class SMXNACaseNominationBatchSdlrContextTest{
    
    static testmethod void SmxcaseScheduler(){
        test.starttest();
        SMXNACaseNominationBatchSchedulerContext sc= new SMXNACaseNominationBatchSchedulerContext();
        String schedule = '0 0 4 * * ?';
        system.schedule('Scheduled Process Nomination NA Cases', schedule, sc);
        test.stoptest();
    }
    
}