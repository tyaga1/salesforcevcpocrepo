/**====================================================================================
 * Experian
 * Name: TimecardTriggerHandler
 * Description: TimecardTriggerHandler
 * Created Date: Jun 12th, 2015
 * Created By: Paul Kissick 
 * 
 * Modified Date        Modified By      Description of the update
 * Jan 7th, 2016        Paul Kissick     Case 01268829 : Lock timecard if before the end date
 *====================================================================================*/
public class TimecardTriggerHandler {

  private static User currentUser;

  public static void beforeInsert(List<Timecard__c> newTimecardsList) {
    populateCurrentUser();
    checkUserCanUpdate(newTimecardsList, null);
  }
  
  public static void beforeUpdate(Map<Id,Timecard__c> oldMap, List<Timecard__c> newList) {
    populateCurrentUser();
    checkUserCanUpdate(newList, oldMap);
  }

  public static void afterInsert(List<Timecard__c> newTimecardsList) {
    updateProjectsForDeliveryTypes(newTimecardsList);
  }
  
  public static void afterDelete(List<Timecard__c> oldTimecardsList) {
    updateProjectsForDeliveryTypes(oldTimecardsList);
  }
  
  public static void afterUpdate(Map<Id,Timecard__c> oldTimecardsMap, List<Timecard__c> newTimecardsList) {
    updateProjectsForDeliveryTypes(newTimecardsList);
  }
  
  // Case 01268829
  private static void populateCurrentUser() {
    if (currentUser == null) {
      currentUser = [
        SELECT Id, Business_Unit__c, Profile.Name 
        FROM User 
        WHERE Id = :UserInfo.getUserId()
      ];
    }
  }
  
  //================================================
  // Case 01268829 - Check for and prevent changes to timecards after they are locked.
  //================================================
  public static void checkUserCanUpdate(List<Timecard__c> tcList, Map<Id,Timecard__c> oldMap) {
    // Check if the timecard setting record exists for the user's BU
    Date lockingDate;
    Boolean isManager = false;
    if (currentUser.Business_Unit__c != null &&
        Timecard_Settings__c.getValues(currentUser.Business_Unit__c) != null) {
      if (currentUser.profile.Name == Constants.PROFILE_EXP_PROJ_DELIVERY_MANAGER || 
          currentUser.profile.Name == Constants.PROFILE_SYS_ADMIN) {
        // isManager = true; // PK: 160107 Removing this for now.
      }
      Timecard_Settings__c timecardSetting = Timecard_Settings__c.getValues(currentUser.Business_Unit__c);
      lockingDate = timecardSetting.Locking_End_Date__c;
    }
    if (lockingDate != null && isManager == false) {
      // Load set of fields to check....
      Set<String> timecardFieldSet = Schema.SObjectType.Timecard__c.fields.getMap().keySet();
      Set<String> safeFieldSet = new Set<String>{'lastmodifieddate','lastmodifiedbyid','systemmodstamp','lastvieweddate','lastreferenceddate'};
      timecardFieldSet.removeAll(safeFieldSet);
      for(Timecard__c tc : tcList) {
        if (oldMap == null) {
          // first check for new ones and don't allow...
          if (tc.Date__c != null && tc.Date__c <= lockingDate) {
            tc.addError(Label.Timecard_Locked);
          }
        }
        else {
          if (((oldMap.get(tc.Id).Date__c != null && oldMap.get(tc.Id).Date__c <= lockingDate) ||
              (tc.Date__c != null && tc.Date__c <= lockingDate)) && 
              isChangedAnyField(timecardFieldSet,tc,oldMap.get(tc.Id)) && 
              !(oldMap.get(tc.Id).Hours__c == null && tc.Hours__c != null && tc.Hours__c == 0)
              ) {
            // show error
            tc.addError(Label.Timecard_Locked); // Maybe use this label: TimecardEntry_Timecard_Locked_Warning
          }
        }
      }
    }
  } 
  
  private static Boolean isChangedAnyField (Set<String> fieldNameSet, Timecard__c newRecord, Timecard__c oldRecord) {
    Boolean isChanged = false;
    for (String fieldName : fieldNameSet) {
      if (newRecord.get(fieldName) != oldRecord.get(fieldName)) {
        isChanged = true;
        break;
      }
    }
    return isChanged;
  }
  
  public static void updateProjectsForDeliveryTypes(List<Timecard__c> timecards) {
    List<Project__c> projectsToUpdate = new List<Project__c>();
    Set<Id> projectIds = new Set<Id>();
    
    for(Timecard__c tc : timecards) {
      projectIds.add(tc.Project__c);
    }
    projectsToUpdate = [
      SELECT Id, Update_LOE_From_Delivery_Lines__c FROM Project__c
      WHERE Id IN :projectIds
      AND Type__c IN :DeliveryLineTriggerHandler.deliveryLineServiceTypesLoeSet
    ];
    for(Project__c pro : projectsToUpdate) {
      pro.Update_LOE_From_Delivery_Lines__c = true;
    }
    try {
      update projectsToUpdate;
    }
    catch (DMLException ex) {
      ApexLogHandler.createLogAndSave('DeliveryLineTriggerHandler','checkForProjectDeliveryType', ex.getStackTraceString(), ex);
      for (Integer i = 0; i < ex.getNumDml(); i++) {
        timecards.get(0).addError(ex.getDmlMessage(i)); 
      }
    }
  }
}