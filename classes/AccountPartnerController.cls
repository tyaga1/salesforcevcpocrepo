/*=====================================================================
* Experian
* Name: AccountPartnerController
* Description: This class is used to display the Partners in the acccount record
*
* Created Date: Aug 23rd, 2016
* Created By: Manoj (Experian)
*
* Date Modified          Modified By             Description of the update
* Aug 23rd, 2016         Manoj Gopu              CRM2:W-005614 - Accounts - Opportunity list where account is the partner
* Sep 5th, 2016          Paul Kissick            CRM2:W-005614 - Rewrite to use AccountPartner object. Fixed formatting. Removed unnecessary 'new' code.
* Sep 9th, 2016          Manoj Gopu              CRM2:W-005614 - Changed the sorting field to Stage and order to Descending order. 
* Sep 28th,2016          Manoj gopu              CRM2:W-005954 -  Changed the accid variable from private to public because it's used in page
* Oct 12th,2016          Manoj Gopu              CRM2:w-005953 -  Restricting access to New button to users other than team members and system administrators, DQ admin, Sales effectiveness and CSA profiles. 

======================================================================*/
public with sharing class AccountPartnerController {

  public List<AccountPartner> lstPartners {get;set;}
  public String deletedId {get;set;}

  public String showMore {get;set;}
  public Boolean isShowMore {get;set;}
  public Integer lstDataSize {get;set;}
  public Id accId{get;set;} 
  private Integer inlinePageSize = 5;
  private Integer fullPageSize = 50;
  public boolean isNewButton{get;set;}
  
  private String sortExp = 'Stage';
  private String sortDirection = 'DESC';
 // private Id accId;

  public Integer pageNumber;//used for pagination
  public Integer pageSize;//used for pagination
  public Integer totalPageNumber;//used for pagination
  public Integer PrevPageNumber {get;set;}//used for pagination
  public Integer NxtPageNumber {get;set;}//used for pagination
  public Integer NlistSize {get;set;}//used for pagination
  public Integer totallistsize {get;set;}//total size of list for pagination
  private ApexPages.StandardController stdCon;

  public String sortExpression {
    get {
      return sortExp;
    }
    set {
      //if the column is clicked on then switch between Ascending and Descending modes
      if (value == sortExp) {
        sortDirection = (sortDirection == 'ASC') ? 'DESC' : 'ASC';
      }
      else {
        sortDirection = 'ASC';
      }
      sortExp = value;
    }
  }

  public String getSortDirection() {
    //if not column is selected
    if (String.isBlank(sortExpression)) {
      return 'ASC';
    }
    else {
      return sortDirection;
    }
  }

  public List<PartnerData> lstPartnersData {get;set;} //Used to combine the partner list along with the opportunity details
  public List<PartnerData> lstPartnersDataData {get;set;}

  public AccountPartnerController(ApexPages.StandardController controller) {
    stdCon = controller;
    accId = controller.getId();
    showMore = ApexPages.currentPage().getParameters().get('showmore');
    displayPartnerData();
    
    // Restricting access to All users other than Account team members and these 4 profiles 
    list<AccountTeamMember> lstTeamMem = [select id from AccountTeamMember where AccountId=:accId AND UserId=:Userinfo.getuserId()];
    string strProfileName=[select id,Name from Profile where id=:Userinfo.getProfileId() limit 1].Name;
    if(!lstTeamMem.isEmpty() || strProfileName == Profiles_w_override_access_2_AcctContact__c.getValues('Sys Admin').Profile_Name__c ||
        strProfileName == Profiles_w_override_access_2_AcctContact__c.getValues('CSA').Profile_Name__c ||
        strProfileName == Profiles_w_override_access_2_AcctContact__c.getValues('SE').Profile_Name__c ||
        strProfileName == Profiles_w_override_access_2_AcctContact__c.getValues('DQ').Profile_Name__c){
            isNewButton = true;
    }
  }

  public void displayPartnerData() {

    lstPartners = new List<AccountPartner>();
    List<String> lstOptyIds = new List<String>();
    lstPartnersData = new List<PartnerData>();
      
    //Get the account level partners
    for (AccountPartner ap : [SELECT Id, AccountToId, Role, IsPrimary, OpportunityId, AccountFrom.Name, AccountFromId, AccountTo.Name
                              FROM AccountPartner
                              WHERE AccountToId = :accId AND Opportunity.AccountId != :accId]) {
      lstPartners.add(ap);
      lstOptyIds.add(ap.OpportunityId);
    }

    isShowMore = false;
    //Get the opportunities for the above partners
    Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>([
      SELECT Id, Name, StageName, Contract_Start_Date__c, Contract_End_Date__c, CloseDate, OwnerId, Amount, Owner.Name, Owner.Region__c,
             Owner.Business_Unit__c, AccountId, Account.Name
      FROM Opportunity
      WHERE Id IN :lstOptyIds
    ]);
    for (AccountPartner objPtn : lstPartners) {
      PartnerData objData = new PartnerData(objPtn);
      
      // Holding empty opp to show in place of no related opp for account partner
      Opportunity opp = new Opportunity();
      opp.Name = '-';
      opp.Amount = 0;
      opp.CloseDate = null;
      opp.OwnerId = null;
      opp.StageName = '-';

      objData.objOpportunity = (mapOpp.containsKey(objPtn.OpportunityId)) ? mapOpp.get(objPtn.OpportunityId) : opp;
      objData.sortExpression = sortExpression;
      objData.sortDirection = SortDirection;
      objData.accFromName = (objPtn.AccountFromId != null) ? objPtn.AccountFrom.Name : '-';
      objData.oppOwner = (mapOpp.containsKey(objPtn.OpportunityId)) ? mapOpp.get(objPtn.OpportunityId).Owner.Name : '-';
      objData.region = (mapOpp.containsKey(objPtn.OpportunityId) && String.isNotBlank(mapOpp.get(objPtn.OpportunityId).Owner.Region__c)) ? mapOpp.get(objPtn.OpportunityId).Owner.Region__c : '-';
      objData.bu = (mapOpp.containsKey(objPtn.OpportunityId) && String.isNotBlank(mapOpp.get(objPtn.OpportunityId).Owner.Business_Unit__c)) ? mapOpp.get(objPtn.OpportunityId).Owner.Business_Unit__c : '-';
      objData.partRole = (String.isNotBlank(objPtn.Role)) ? objPtn.Role : '-';
      objData.isprimary = String.valueOf(objPtn.IsPrimary);
      String strcldt = '-';
      if (objData.objOpportunity.CloseDate != null) {
        strcldt = DateTime.newInstance(objData.objOpportunity.CloseDate, Time.newInstance(12,0,0,0)).format('yyyyMMddhhmmss');
      }
      objData.strCloseDate = strcldt;
      String strstartdt = '-';
      if (objData.objOpportunity.Contract_Start_Date__c != null) {
        strstartdt = DateTime.newInstance(objData.objOpportunity.Contract_Start_Date__c, Time.newInstance(12,0,0,0)).format('yyyyMMddhhmmss');
      }
      objData.strStartDate = strstartdt;
      String strenddt = '-';
      if (objData.objOpportunity.Contract_End_Date__c != null) {
        strenddt = DateTime.newInstance(objData.objOpportunity.Contract_End_Date__c, Time.newInstance(12,0,0,0)).format('yyyyMMddhhmmss');
      }
      objData.strEndDate = strenddt;
      lstPartnersData.add(objData);//Combining the partner records and opportunity records
    }

    //Get the list size
    lstDataSize = lstPartnersData.size();
    lstPartnersData.sort();
    //This will execute, if the page displayed in account.
    if (String.isBlank(showMore)) {
      List<PartnerData> tempPartnersData = new List<PartnerData>();
      if (lstPartnersData.size() > inlinePageSize) {
        for (PartnerData obj:lstPartnersData) {
          if (tempPartnersData.size() < inlinePageSize) {
            tempPartnersData.add(obj);
          }
        }
        isShowMore = true;
        lstPartnersData = new List<PartnerData>();
        lstPartnersData.addAll(tempPartnersData);
      }
    }
    totallistsize = lstPartnersData.size(); // this size for pagination
    pageSize = fullPageSize; // default page size will be 50
    if (totallistsize == 0) {
      BindData(0);// this method is for pagination, if no records found then passing zero to the pagination method
      NxtPageNumber = lstPartnersDataData.size();
      PrevPageNumber = 0;
    }
    else {
      BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
      NxtPageNumber = lstPartnersDataData.size();
      PrevPageNumber = 1;
    }
  }

  //This wrapper class is used to combine the partners along with the opportunities
  public class PartnerData implements Comparable {
    public AccountPartner objPartner {get;set;} //used to store the partner information
    public Opportunity objOpportunity {get;set;} //used to store the opportunity information
    public String sortExpression; //used for grid
    public String sortDirection; //used for grid
    public String strCloseDate {get;set;}
    public String strStartDate {get;set;}
    public String strEndDate {get;set;}
    public String accFromName {get;set;}
    public String oppOwner {get;set;}
    public String partRole {get;set;}
    public String isprimary {get;set;}
    public String region {get;set;}
    public String bu {get;set;}
      
    public PartnerData(AccountPartner p) {
      objPartner = p;
    }

    public Integer compareTo(Object ObjToCompare) {
      if (sortDirection == 'ASC') {
        if (sortExpression == 'Opportunity Account')
          return accFromName.CompareTo(((PartnerData)ObjToCompare).accFromName);
        else if (sortExpression == 'Opportunity')
          return objOpportunity.Name.CompareTo(((PartnerData)ObjToCompare).objOpportunity.Name);
        else if (sortExpression == 'Stage')
          return objOpportunity.StageName.CompareTo(((PartnerData)ObjToCompare).objOpportunity.StageName);
        else if (sortExpression == 'CloseDate')
          return strCloseDate.CompareTo(((PartnerData)ObjToCompare).strCloseDate);
        else if (sortExpression == 'StartDate')
          return strStartDate.CompareTo(((PartnerData)ObjToCompare).strStartDate);
        else if (sortExpression == 'EndDate')
          return strEndDate.CompareTo(((PartnerData)ObjToCompare).strEndDate);
        else if (sortExpression == 'Owner')
          return oppOwner.CompareTo(((PartnerData)ObjToCompare).oppOwner);
        else if (sortExpression == 'Region')
          return region.CompareTo(((PartnerData)ObjToCompare).region);
        else if (sortExpression == 'BusinessUnit')
          return bu.CompareTo(((PartnerData)ObjToCompare).bu);
        else if (sortExpression == 'Role')
          return partRole.CompareTo(((PartnerData)ObjToCompare).partRole);
        else if (sortExpression == 'Primary')
          return isprimary.CompareTo(((PartnerData)ObjToCompare).isprimary);
        else if (sortExpression == 'Amount') {
          if (objOpportunity.Amount == ((PartnerData)ObjToCompare).objOpportunity.Amount) return 0;
          if (objOpportunity.Amount > ((PartnerData)ObjToCompare).objOpportunity.Amount) return 1;
          if (objOpportunity.Amount < ((PartnerData)ObjToCompare).objOpportunity.Amount) return -1;
        }
      }
      else {
        if (sortExpression == 'Opportunity Account')
          return ((PartnerData)ObjToCompare).accFromName.CompareTo(accFromName);
        else if (sortExpression == 'Opportunity')
          return ((PartnerData)ObjToCompare).objOpportunity.Name.CompareTo(objOpportunity.Name);
        else if (sortExpression == 'Stage')
          return ((PartnerData)ObjToCompare).objOpportunity.StageName.CompareTo(objOpportunity.StageName);
        else if (sortExpression == 'CloseDate')
          return ((PartnerData)ObjToCompare).strCloseDate.CompareTo(strCloseDate);
        else if (sortExpression == 'StartDate')
          return ((PartnerData)ObjToCompare).strStartDate.CompareTo(strStartDate);
        else if (sortExpression == 'EndDate')
          return ((PartnerData)ObjToCompare).strEndDate.CompareTo(strEndDate);
        else if (sortExpression == 'Owner')
          return ((PartnerData)ObjToCompare).oppOwner.CompareTo(oppOwner);
        else if (sortExpression == 'Region')
          return ((PartnerData)ObjToCompare).region.CompareTo(region);
        else if (sortExpression == 'BusinessUnit')
          return ((PartnerData)ObjToCompare).bu.CompareTo(bu);
        else if (sortExpression == 'Role')
          return ((PartnerData)ObjToCompare).partRole.CompareTo(partRole);
        else if (sortExpression == 'Primary')
          return ((PartnerData)ObjToCompare).isprimary.CompareTo(isprimary);
        else if (sortExpression == 'Amount') {
          if (((PartnerData)ObjToCompare).objOpportunity.Amount == objOpportunity.Amount) return 0;
          if (((PartnerData)ObjToCompare).objOpportunity.Amount > objOpportunity.Amount) return 1;
          if (((PartnerData)ObjToCompare).objOpportunity.Amount < objOpportunity.Amount) return -1;
        }
      }
      return null;
    }
  }
  
  public PageReference backToAccount() {
    return stdCon.view();
  }
  
  //method is used to delete the selected partner
  public PageReference deletePartner() {
    system.debug('deletedId@@@'+deletedId);
    try {
      // Partner fails to delete for normal user because they cannot see the object! 
      delete ([SELECT Id FROM Partner WHERE Id = :deletedId]);
    }
    catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, e.getMessage()));
    }
    displayPartnerData();
    return null;
  }

  // pagination
  public PageReference nextBtnClick() {
    BindData(pageNumber + 1);
    pageData(pageNumber + 1);
    return null;
  }

  // Below method fires when user clicks on previous button of pagination.
  public PageReference previousBtnClick() {
    pageData(pageNumber - 1);
    BindData(pageNumber - 1);
    return null;
  }

  /// Below method fires when user clicks on next button of pagination.
  public Integer getPageNumber() {
    return pageNumber;
  }

  /// Below method is for getting pagesize how many records per page .
  public Integer getPageSize() {
    return pageSize;
  }

  /// Below method is for enabling and disabling the previous button of pagination.
  public Boolean getPreviousButtonEnabled() {
    return !(pageNumber > 1);
  }

  /// Below method is for enabling and disabling the nextbutton of pagination.
  public Boolean getNextButtonDisabled() {
    if (totallistsize == 0) {
      return true;
    }
    else {
      return ((pageNumber * pageSize) >= totallistsize);
    }
  }

  /// Below method gets the total no.of pages.
  public Integer getTotalPageNumber() {
    totalPageNumber = 0;
    if (totalPageNumber == 0 && totallistsize != 0) {
      totalPageNumber = totallistsize / pageSize;
      Integer mod = totallistsize - (totalPageNumber * pageSize);
      if (mod > 0) {
        totalPageNumber++;
      }
    }
    return totalPageNumber;
  }

  /// Below method is the main method of pagination if have atleast one record then we pass 1 to this method otherwise zero.
  public void BindData(Integer newPageIndex) {
    lstPartnersDataData = new List<PartnerData>();
    Integer counter = 0;
    Integer min = 0;
    Integer max = 0;

    if (newPageIndex > pageNumber) {
      min = pageNumber * pageSize;
      max = newPageIndex * pageSize;
    }
    else {
      max = newPageIndex * pageSize;
      min = max - pageSize;
    }
    if (lstPartnersData != null) {
      for (PartnerData b : lstPartnersData) {
        counter++;
        if (counter > min && counter <= max) {
          lstPartnersDataData.add(b);// here adding files list
        }
      }
    }
    pageNumber = newPageIndex;
    NlistSize = lstPartnersDataData.size();
  }

  /// Below method bnds the values for Next and previous buttons.
  public void pageData(Integer newPageIndex) {
    if (newPageIndex > pageNumber) {
      PrevPageNumber = PrevPageNumber+pagesize;
      NxtPageNumber = NxtPageNumber+NlistSize;
    }
    if (newPageIndex < pageNumber) {
      PrevPageNumber = PrevPageNumber-pagesize;
      NxtPageNumber = NxtPageNumber-NlistSize;
    }
  }

  /// Below method binds the data for last button.
  public void LastpageData(Integer newPageIndex) {
    lstPartnersDataData = new List<PartnerData>();
    Integer counter = 0;
    Integer min = 0;
    Integer max = 0;
    min = pageNumber * pageSize;
    max = newPageIndex * pageSize;
    for (PartnerData a : lstPartnersData) {
      counter++;
      if (counter > min && counter <= max) {
        lstPartnersDataData.add(a);// here adding the folders list
      }
    }
    pageNumber = newPageIndex;
    NlistSize = lstPartnersDataData.size();
    //PrevPageNumber = totallistsize - NlistSize +1;
    PrevPageNumber = ((pageNumber - 1)*pageSize) +1;
    NxtPageNumber = totallistsize;
  }

  /// Below method bnds the value for last button.
  public PageReference LastbtnClick() {
    BindData(totalpagenumber - 1);
    LastpageData(totalpagenumber);
    return null;
  }

  /// Below method bnds the value for first button.
  public PageReference FirstbtnClick() {
    BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
    NxtPageNumber = NlistSize;
    PrevPageNumber = 1;
    return null;
  }

}