/**=====================================================================
 * Experian
 * Name: CAUserProfileTriggerHandler_Test
 * Description: Test Class for CAUserProfileTriggerHandler
 *
 * Created Date: September 4th 2017
 * Created By: Alexander McCall for ITCA
 * 
=====================================================================*/
@isTest
private class CAUserProfileTriggerHandler_Test{
        
     @isTest static void testSkillUserTableInsert() {
    
    
     Profile p = [SELECT Id FROM Profile WHERE Name=: Constants.PROFILE_SYS_ADMIN ];
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    insert testUser1;
    
    Career_Architecture_User_Profile__c caup1 = new Career_Architecture_User_Profile__c(//Career_Area__c=,
                                                                                        //Date_Discussed_with_Employee__c=,
                                                                                        //Discussed_with_Employee__c=,
                                                                                        Employee__c= UserInfo.getUserId() ,
                                                                                        //Manager_Comments__c=,
                                                                                        //Role__c=,
                                                                                        State__c='Current',
                                                                                        Status__c='Submitted to Manager');
    insert caup1;
    
    caup1.Manager_Comments__c='test comments';
    update caup1;
    
    
    Career_Architecture_Skills_Plan__c casp1 = new Career_Architecture_Skills_Plan__c(Career_Architecture_User_Profile__c=caup1.id,
                                                                                      Experian_Skill_Set__c=[SELECT id FROM Experian_Skill_Set__c LIMIT 1].id,
                                                                                      Level__c='Level6',
                                                                                      Skill__c=[SELECT id FROM ProfileSkill ORDER BY id DESC LIMIT 1].id  );
    
    insert casp1;
    
    casp1.level__c='Level1';
    update casp1;
                                               
    caup1.Status__c='Discussed with Employee';
    update caup1;
    
    ProfileSkillUser psu1 = new ProfileSkillUser(UserId=UserInfo.getUserId(),
                                                ProfileSkillID=[SELECT id FROM ProfileSkill ORDER BY id ASC LIMIT 1].id  );
                                                
    insert psu1;
    
    delete psu1;
    delete casp1;
    delete caup1;
    }
      
      
  @testSetup
  private static void createData(){
    
    buildProfileSkills();
    buildExperianSkillSet();
    buildSkillSetToSKill();
   
  }

  private static void buildProfileSkills(){
  
    ProfileSkill ps1 = new ProfileSkill(Name='Apex', 
                                        Level5__c='Maintains Code.', 
                                        Level6__c='Writes test classes.', 
                                        Level7__c='Designs Code.',
                                        Sfia_Skill__c=false);
                                        
    ProfileSkill ps2 = new ProfileSkill(Name='Programming & Software Development', 
                                        Level5__c='Maintains Code.', 
                                        Level6__c='Writes test classes.', 
                                        Level7__c='Designs Code.',
                                        Sfia_Skill__c=true);                                  

    List<ProfileSkill> ps_List = new List<ProfileSkill>{ps1,ps2};
    insert ps_List;
    
  }

  private static void buildExperianSkillSet(){
    Experian_Skill_Set__c exp1 = new Experian_Skill_Set__c(Name='Software Development', Career_Area__c='Applications');
    insert exp1;
  
  }

  private static void buildSkillSetToSKill(){
    List<ProfileSkill> psList = [SELECT id, Name FROM ProfileSkill];
    List<Experian_Skill_Set__c> essList = [SELECT id FROM Experian_Skill_Set__c];
   
    Skill_Set_to_Skill__c sts1 = new Skill_Set_to_Skill__c(Name='Software Development - Application Support', 
                                                          Skill__c=psList[0].id, 
                                                          Experian_Role__c='Professional',
                                                          Experian_Skill_Set__c=essList[0].id,
                                                          Level__c='Level4');
    insert sts1;
  
 }

 
  }