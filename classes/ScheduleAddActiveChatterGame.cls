global class ScheduleAddActiveChatterGame implements Schedulable
{
  /*
  * Author:     Diego Olarte (Experian)
  * Description:  The following class is for scheduling the 'AddActiveChatterGame.cls' class to run at specific intervals.
  */  
  
  global void execute(SchedulableContext sc)
  {
    AddActiveChatterGame batchToProcess = new AddActiveChatterGame();
    database.executebatch(batchToProcess);
  }
}