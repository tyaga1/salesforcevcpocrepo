/*=============================================================================
 * Experian
 * Name: BatchOppdatesDataUpdate
 * Description: W-005289 : This batch is used for one time data update For Opp Receivedsignedcontract and Quotedelivereddate fields 
 *                         
 * Created Date: 31st Aug 2016
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
 22nd Feb 2017        Manoj Gopu            Case:02227631 : Updated the Batch to Update the Selection Confirmed date on the Opp from Activity 
 ============================================================================*/
//Created this batch for data Update in opp based on Activity
public class BatchOppdatesDataUpdate implements Database.Batchable<sObject>, Database.Stateful
{   
    //start method of batch class
    public Database.QueryLocator start(Database.BatchableContext BC){    
        string strstatus = Constants.STATUS_COMPLETED;    
        string strselConf=Constants.ACTIVITY_TYPE_SELECTION_CONFIRMED ;
        String query ='select id,Outcomes__c,Status,WhatId,LastModifiedDate from Task where Status =:strstatus AND Outcomes__c includes (:strselConf)';
        return Database.getQueryLocator(query);      
    }
    //execute method of batch class
    public void execute(Database.BatchableContext BC, List<Task> lstTasks){    
        list<Id> lstOppIds=new list<Id>();
        for(Task ts:lstTasks){
            lstOppIds.add(ts.WhatId);
        }
         
        string strselConf=Constants.ACTIVITY_TYPE_SELECTION_CONFIRMED ;
        map<Id,Opportunity> mapOpty=new map<Id,Opportunity>([select id,StageName,Selection_confirmed_date__c from Opportunity where id=:lstOppIds]);
        if(!mapOpty.isEmpty()){            
            map<Id,Opportunity> mapOptysTobeUpdate=new map<Id,Opportunity>();
            for(Task ts:lstTasks){
                Opportunity opty=new Opportunity();                
                if(mapOpty.containsKey(ts.WhatId) && (ts.Outcomes__c.contains(strselConf))){
                    opty=mapOpty.get(ts.WhatId);
                    DateTime dT=ts.LastModifiedDate;                     
                    if(opty.Selection_confirmed_date__c==null && ts.Outcomes__c.contains(strselConf)) {                   
                        opty.Selection_confirmed_date__c=date.newinstance(dT.year(), dT.month(), dT.day());
                    }
                  
                    mapOptysTobeUpdate.put(opty.Id,opty);              
                }
            }
            if(!mapOptysTobeUpdate.isEmpty())
                Database.update(mapOptysTobeUpdate.Values());
        }
    }   
    public void finish(Database.BatchableContext BC){}
    
}