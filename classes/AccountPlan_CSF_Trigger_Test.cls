/**=====================================================================
 * Appirio, Inc
 * Name:         AccountPlan_CSF_Trigger_Test
 * Description:  Test Class for AccountPlan_CSF_Trigger
 * Created Date: 31 July 2014
 * Created By:   Parul Gupta
 * Modified On                  Modified By                      Description
 * Sept 22nd,2015               Jagjeet Singh(Appirio)           Added test method for ValidateRecordsToBeDeleted
 * Sept 29th,2015               Jagjeet Singh(Appirio)           Getting the error message from Custom Label.
 * Apr  29th,2016               James Wills                      Case #01848189 Account Planning Project. Deprecated validation for ACCOUNTPLANNING_VALIDATION_MSG_ON_DELETE
 =====================================================================*/
@isTest
private class AccountPlan_CSF_Trigger_Test {

    // This method tests for functionality of trigger on before insert
    static testMethod void testBeforeInsert() {
        Account account = Test_utils.insertAccount();
        Account_Plan__c aplan = Test_utils.insertAccountPlan(false, account.id);
        aplan.CurrencyIsoCode = 'USD';
        insert aplan;
        // Create test data in util
        Account_Plan_Critical_Success_Factor__c acctPlanCSF = new Account_Plan_Critical_Success_Factor__c();
        acctPlanCSF.Account_Plan__c= aplan.Id;
        acctPlanCSF.Description__c = 'Test Description';
        insert acctPlanCSF ;
        
        //Query currencyisocode
      
    }
    
    static testMethod void testBeforeUpdate() {
        Account account = Test_utils.insertAccount();
        Account_Plan__c aplan = test_utils.insertAccountPlan(true , account.id);
        Account_Plan_Critical_Success_Factor__c apcsf = new Account_Plan_Critical_Success_Factor__c();
        apcsf.Account_Plan__c = aplan.Id;
        apcsf.Description__c = 'hello';
        insert apcsf;
        apcsf.Description__c = 'hi';
        update apcsf;
    }
    
    //test the Validate Records Negative Scenario.
    static testMethod void testValidateRecordsToBeDeleted_Negative(){
      //start test
      Test.startTest();
      User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
      insert testUser1; 
      
      // insert account
      Account account = Test_Utils.insertAccount();
      
      // create account plan
      Account_Plan__c accountPlan = Test_Utils.insertAccountPlan(false, account.id);
      accountPlan.Name = 'TestAccountPlan';
      insert accountPlan;
      
      Account_Plan_Critical_Success_Factor__c acctPlanCSF = new Account_Plan_Critical_Success_Factor__c();
      acctPlanCSF.Account_Plan__c= accountPlan.Id;
      acctPlanCSF.Description__c = 'Test Description';
      insert acctPlanCSF;
      
      //Case #01848189 Account Planning Project
      //This validation is no longer required.
      //String expectedMsg = Label.ACCOUNTPLANNING_VALIDATION_MSG_ON_DELETE;      
      //assertErrorOnDelete(acctPlanCSF,expectedMsg);
      
      Test.stopTest();
    }
     
     
    //test the Validate Records Positive Scenario.
    static testMethod void testValidateRecordsToBeDeleted_Positive(){
      //start Test
      Test.startTest();
      User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
      insert testUser1; 
      
      // insert account
      Account account = Test_Utils.insertAccount();
      Id currentUserId = UserInfo.getUserId();
      //insert the accountTeamMember
      AccountTeamMember accTeamMem = Test_Utils.insertAccountTeamMember(true,account.Id,currentUserId,'test');
      
      // create account plan
      Account_Plan__c accountPlan = Test_Utils.insertAccountPlan(false, account.id);
      accountPlan.Name = 'TestAccountPlan';
      insert accountPlan;
      
      Account_Plan_Team__c accPlanTeamMem = Test_Utils.insertAccountPlanTeam(true,accountPlan.Id,currentUserId);
      
      Account_Plan_Critical_Success_Factor__c acctPlanCSF = new Account_Plan_Critical_Success_Factor__c();
      acctPlanCSF.Account_Plan__c= accountPlan.Id;
      acctPlanCSF.Description__c = 'Test Description';
      insert acctPlanCSF;
      
      delete acctPlanCSF;
      List<Account_Plan_Critical_Success_Factor__c> aCsfDb = [select Id from Account_Plan_Critical_Success_Factor__c where Id = :acctPlanCSF.Id];
      //assert
      system.assertEquals(aCsfDb.size(),0,'Account Plan Competitor should be deleted.');
      Test.stopTest();
    }
    
    
    //Case #01848189 Account Planning Project
    //This validation is no longer required.
    //
    //assert error message on delete when the current user is not in AccountTeamMember.
    //private static void assertErrorOnDelete(SObject sObj, String expectedMsg) {
    //  try {
    //        delete sObj;
    //        System.assert(false, 'exception expected for SObject: ' + sObj);
    //  } catch (Exception e) {
    //        System.assert(e.getMessage().contains(expectedMsg), 'message=' + e.getMessage());
    //  }
    //}

    
}