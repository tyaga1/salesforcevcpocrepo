/**********************************************************************************************
 * Experian, Inc 
 * Name         : SMXRestResourceRelSurvey
 * Created By   : Satmetrix
 * Purpose      : Update responses from Satmetrix with additional responses for Relationship Survey
 * Created Date : 02 August 2016
 *
 * Date Modified                Modified By                Description of the update
 * Jul 17th, 2016(QA)           Tyaga Pati                 CRM 2W-005364/CASE 01790367 - Function added to Create Activities when Survey Response is sent by Satmatrix
 * Aug 17th, 2016(QA)           Satmetrix                  Updating the field (Account__c) instead of new field (AccountID__c)
 * Aug 26th, 2016(QA)           Satmetrix                  Passing the ManagerID value stored in Satmetrix
 * Jul 17th, 2016(QA)           Tyaga Pati                 CRM 2W-005364/CASE 01790367 - Changes to accomodate separate activity creation logic for NON - CSDA units
 * Aug 30th, 2016(QA)           Tyaga Pati                 CRM 2W-005364/CASE 01790367 - Record Type Changes
 * Aug 30th, 2016(QA)           Tyaga Pati                 CRM 2W-005364/CASE 01790367 -Activity Creation logic to see if duplicates are getting created.
 * Sept 7th, 2016(QA)           Tyaga Pati                 CRM 2W-005364/CASE 01790367 -Added Enhancements to have customized subject and Due Date on activity
 * Sept 8th, 2016(QA)           Tyaga Pati                 CRM 2W-005364/CASE 01790367 - Exception handling for null/Invalid or empty string in incoming survey record.
 * Sept 12th, 2016(QA)          Tyaga Pati                 CRM 2W-005364/ISSUE I276- Fixed Missed out subject description.
 * Sept 16th, 2016(QA)          Tyaga Pati                 CRM 2W-005364/ISSUE I276- Added code for parking the failing surveys in Apex log for tracking due to missing contact from merge
 * Sept 16th, 2016(QA)          Tyaga Pati                 CRM 2W-005364/ISSUE Status Code 1500 for contact missing survey load failure
 * Sept 20th, 2016(QA)          Tyaga Pati                 CRM 2W-005364/ISSUE Added Experian Global to catch exceptions in Activity assignment
 * Oct 12th, 2016(QA)           Satmetrix                  Decoding the special character for Comment String (Modified line# - 110, 120 and 133)
                                                           To Change the Field type of Area of improvement (Area_of_improvement__c) field from Text (255) to Long Text Area(32768), Comment line# 120, deploy the modified (or) commented class, redeploy the same by uncommenting the line# 120 after field type is modified.
 * Sept 20th, 2016(QA)          Tyaga Pati                 Case 02166684: Web service changes from Satmatrix for Encode/Decode and Field type changes. and Activity initial status change
 * Sept 20th, 2016(QA)          Tyaga Pati                 Case 02166703: Activate Field Reference again
   
***********************************************************************************************/
@RestResource(urlMapping='/UpdateFeedbackRel/*')
global with sharing class SMXRestResourceRelSurvey {
  
  @HttpPost
  global static String doPost(String strActionCode,Decimal decScore,String strComment,String strStatus,String strSubStatus,String strFeedbackReceivedDate,String strInvitationSentDate,String strFirstReminderDate,String strSecondReminderDate,String strThirdReminderDate, String strPersonId,String strDcname, String strDcid, Decimal OverallSat, Decimal ContPurchase, Decimal ContPurchaseCSDA,String CommImprov, Decimal SatPresalesSales, Decimal SatClientAccMgt, Decimal SatSupport, String strAccountID, Decimal SatDeliveryImpl, String StrManagerId) {
    RestRequest req = RestContext.request;
    String strFbkId= req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    strFbkId = EncodingUtil.urlDecode(strFbkId,'UTF-8');
    return updateFeedback(strFbkId,strActionCode,decScore,strComment,strStatus,strSubStatus,strFeedbackReceivedDate,strInvitationSentDate,strFirstReminderDate,strSecondReminderDate,strThirdReminderDate,strPersonId,strDcname, strDcid, OverallSat, ContPurchase, ContPurchaseCSDA, CommImprov, SatPresalesSales, SatClientAccMgt, SatSupport,strAccountID,SatDeliveryImpl, StrManagerId);         
  }
  
  public static String updateFeedback(String strFbkId,String strActionCode,Decimal decScore,String strComment,String strStatus,String strSubStatus,String strFeedbackReceivedDate,String strInvitationSentDate,String strFirstReminderDate,String strSecondReminderDate,String strThirdReminderDate,String strPersonId,String strDcname, String strDcid, Decimal OverallSat, Decimal ContPurchase, Decimal ContPurchaseCSDA, String CommImprov, Decimal SatPresalesSales, Decimal SatClientAccMgt, Decimal SatSupport, String strAccountID, Decimal SatDeliveryImpl, String StrManagerId)
    {
    String strReturnVal = '';
    String strFeedbackReceivedDateParsed = '';
    String strInvitationSentDateParsed = '';
    String strFirstReminderDateParsed = '';
    String strSecondReminderDateParsed = '';
    String strThirdReminderDateParsed = '';
    List <Feedback__c> lstFeedback= [
      SELECT Id 
      FROM Feedback__c 
      WHERE Name = :strFbkId
    ];
    String strContactRecordStatus = 'Failure';
    if (lstFeedback.isEmpty()) {      
      strContactRecordStatus = createSurveyRecord( strFbkId, strStatus, strPersonId, strSubStatus,strDcname,strDcid, strAccountID);
    }
    else {
      strContactRecordStatus = 'Success';
    }
    
    if (strContactRecordStatus == 'Success') {
      if (strActionCode == 'UpdateESRStatus') {
        Feedback__c Feedback = [
          SELECT Name, Status__c, StatusDescription__c, ResponseReceivedDate__c, FirstReminderDate__c, SecondReminderDate__c, ThirdReminderDate__c 
          FROM Feedback__c 
          WHERE Name = :strFbkId
        ];
        Feedback.Status__c = strStatus;
        Feedback.StatusDescription__c = strSubStatus;
        if(String.isNotBlank(strFeedbackReceivedDate)) {
          Datetime dtme = getConvertDateTime(strFeedbackReceivedDate);         
          Feedback.ResponseReceivedDate__c = dtme;
        }
        if(String.isNotBlank(strInvitationSentDate)) {
          Datetime dtme = getConvertDateTime(strInvitationSentDate);               
          Feedback.Invitation_Sent_Date__c = dtme;
        }
        if(String.isNotBlank(strFirstReminderDate)) {
          Datetime dtme = getConvertDateTime(strFirstReminderDate);               
          Feedback.FirstReminderDate__c = dtme;
        }
        if(String.isNotBlank(strSecondReminderDate)) {
          Datetime dtme = getConvertDateTime(strSecondReminderDate);
          Feedback.SecondReminderDate__c = dtme;
        }
        if (String.isNotBlank(strThirdReminderDate)) {
          Datetime dtme = getConvertDateTime(strThirdReminderDate);
          Feedback.ThirdReminderDate__c = dtme;
        }
        List<Account> lstAccount = new List<Account>();
          lstAccount = [
            SELECT Id
            FROM Account
            WHERE Id = :strAccountID
        ];
        if (!lstAccount.isEmpty())
          Feedback.Account__c = strAccountID;
        update Feedback;
        strReturnVal = Feedback.Status__c;
      }
      else if (strActionCode == 'UpdateFeedbackDetails') {
        String strSFDCServer = '';
        SMXConfiguration__c smx_config = SMXConfiguration__c.getValues('Configuration');
        if(smx_config != null) {
          strSFDCServer = smx_config.URL_SALESFORCE__c;
        }
        Feedback__c Feedback = [
          SELECT Name, Account__C, DataCollectionId__c, PrimaryScore__c, PrimaryComment__c, Status__c, StatusDescription__c, SurveyDetailsURL__c, Contact__c, NPS_Status__c, ResponseReceivedDate__c
          FROM Feedback__c 
          WHERE Name = :strFbkId
        ];
        Feedback.PrimaryScore__c= decScore;
        //Decoding the strComment value
        Feedback.PrimaryComment__c = EncodingUtil.urlDecode(strComment, 'UTF-8');
        Feedback.Status__c = 'Invitation Delivered';     
        Feedback.StatusDescription__c = 'Response Received';
        Feedback.SurveyDetailsURL__c = strSFDCServer + '/apex/SMXSurveyDetails?ProvID='+strFbkId;
        Feedback.Overall_Satisfaction__c = OverallSat;
        if(ContPurchase!=null)
            Feedback.Continue_to_Purchase__c = ContPurchase;
        else if(ContPurchaseCSDA!=null)
            Feedback.Continue_to_Purchase__c = ContPurchaseCSDA;
        //Decoding the CommImprov value
        Feedback.Area_of_improvement__c = EncodingUtil.urlDecode(CommImprov,'UTF-8');
        Feedback.Satisfaction_with_presales_sales__c = SatPresalesSales;
        Feedback.Satisfaction_with_account_management__c = SatClientAccMgt;
        Feedback.Satisfaction_with_ongoing_service_and_su__c = SatSupport;
        Feedback.Satisfaction_with_DeliveryImplementation__c = SatDeliveryImpl;
        update Feedback;

        //Update Contact TableNPS Score and Comment
        if (Feedback.Contact__c != null) 
          {
            Contact contact = [SELECT PrimaryScore__c, PrimaryComment__c FROM Contact WHERE Id = :Feedback.Contact__c ];
            contact.PrimaryScore__c= decScore;
            //Decoding the strComment value
            contact.PrimaryComment__c= EncodingUtil.urlDecode(strComment,'UTF-8');
            contact.Survey_name__c=strDcname;
            update contact;
            strReturnVal = contact.PrimaryComment__c;
            system.debug('**** the Data Collection Id ' + Feedback.DataCollectionId__c );
              // If NA CSDA which can be determined by Data Collection Id : EXPERIAN_26515 then follow this function else just one activity and assign it to the manager sent by satmatrix.
            system.debug('**** The Activity function is being called with survey id :'+ Feedback.Id + 'and Account id : '+ Feedback.Account__C+ 'contat id :' + Feedback.Contact__c);
              //Check if Activities already exist for the survey Record.
              //Requery Needed to get the Latest Values Updated By Satmatrix to be used in Activity creation.
            Feedback__c Feedback1 = [SELECT NPS_Status__c, ResponseReceivedDate__c FROM Feedback__c WHERE Name = :strFbkId ];
            Integer surActivityCount =0; 
              //= database.countQuery('SELECT count(Id) FROM Task where ParentSurvey__c =:Feedback.Name');
            List<AggregateResult> results = [SELECT count(Id) FROM Task where ParentSurvey__r.Name =:Feedback.Name];
            surActivityCount = (Integer) results[0].get('expr0');
            system.debug('*** The Number of Existing Activitis is : ' + surActivityCount );
              //Calculate Due Date for the Activity to be Created
            Date ActivityDueDate ;
            if ((Feedback1.NPS_Status__c == 'Detractor') && (Feedback1.ResponseReceivedDate__c != null))
              {
                DateTime dT = Feedback.ResponseReceivedDate__c;
                ActivityDueDate =  date.newinstance(dT.year(), dT.month(), dT.day()).addDays(2);
              } 
          
            else if (((Feedback1.NPS_Status__c == 'Passive') ||(Feedback1.NPS_Status__c == 'Promoter'))&&(Feedback1.ResponseReceivedDate__c != null))
              {
                DateTime dT = Feedback.ResponseReceivedDate__c;
                ActivityDueDate =  date.newinstance(dT.year(), dT.month(), dT.day()).addDays(5);
              } 
            
            else ActivityDueDate  = (Date)date.today();

            system.debug('>>>>>> the due date is  >>>>>> :' + ActivityDueDate   );   
            system.debug('******** the count of activities for this survey with Provider id :' + Feedback.Name + ': is :' + surActivityCount  ); 

            if(surActivityCount <1)
              {
                if(Feedback.DataCollectionId__c == 'EXPERIAN_26515')
                  {
                    createNewActivityCSDA(Feedback.Id,Feedback.Account__C,Feedback.Contact__c, Feedback1.NPS_Status__c, ActivityDueDate );
                  }
                
                else if((Feedback.DataCollectionId__c != 'EXPERIAN_26515')&& (StrManagerId != '') && (StrManagerId != null))
                  {
                    createNewActivity(Feedback.Id,Feedback.Account__C,Feedback.Contact__c,StrManagerId,Feedback1.NPS_Status__c, ActivityDueDate);
                  }
          
             }//End of Count Check 
      
          }//End of If checking Contact
      }//If checking Update Status
    }
    return strReturnVal; 
  }
  
//**********************************************************************************************
// TP Case 01790367 : This function will Create Activity on the Survey Record for all BUs except CSDA
//********************************************************************************************* 
 global static String createNewActivity(Id SurveyRecId, Id SurveyAcntId, Id SurveyConId, String StrManagerId, string NPS_Status, Date ActivityDueDate ) {
    Id SurveyCon = SurveyConId;
    Id ActivityOwnId;
    Experian_Global__c expGolabl= Experian_Global__c.getInstance();
    Id tempActOwner = expGolabl.OwnerId__c;
    system.debug('***** the Id of the activity owner is ' + tempActOwner );//Experian Global For Activity assignment.
    if(StrManagerId <> null && StrManagerId <> '' )
      {
        system.debug('hi ^^^^^^ the value of string manage is' + StrManagerId );
        List<User> usrLstId = [SELECT Id from User where Id =:StrManagerId];
        if(!usrLstId.isEmpty())
          {
            ActivityOwnId = [SELECT Id from User where Id =:StrManagerId].Id;  
          }
        else 
            //ActivityOwnId = [SELECT Id from User where Id ='005i0000001w7MDAAY'].Id;//Need a default Owner if file does not contain a member.  
            ActivityOwnId = [SELECT Id from User where Id = :tempActOwner].Id;//Need a default Owner if file does not contain a member.  
          
      }
    else 
      ActivityOwnId = [SELECT Id from User where Id = :tempActOwner].Id;//Need a default Owner if file does not contain a member.
    
    Task TaskTemp = new Task();
    if (ActivityOwnId<> null) {   
        TaskTemp.OwnerId = ActivityOwnId;
        TaskTemp.WhatId = SurveyAcntId;
        TaskTemp.Subject = 'Client Survey Response from Satmetrix' + ' - '+ NPS_Status ;
        TaskTemp.Status = 'Not Started';
        TaskTemp.Priority = 'Normal';
        TaskTemp.ActivityDate = ActivityDueDate ;
        TaskTemp.Type = 'Client Survey';
        TaskTemp.WhoId =  SurveyConId;
        TaskTemp.ParentSurvey__c = [select Id from feedback__C where Id =:SurveyRecId].Id;
        TaskTemp.RecordTypeId = [Select Id,SobjectType,Name From RecordType where Name =:'Survey Task' and SobjectType =:'Task' limit 1].Id;
        Insert TaskTemp;
       }
       
       
   system.debug('**** the following activity was created' + TaskTemp);
   return 'Following Activities Were Created '+TaskTemp;
  } 
  
//**********************************************************************************************
// TP Case 01790367 : This function will Create Activity on the Survey Record. 
//********************************************************************************************* 
 global static String createNewActivityCSDA(Id SurveyRecId, Id SurveyAcntId, Id SurveyConId, String NPS_Status, Date ActivityDueDate  ) {
    Id SurveyCon = SurveyConId;
    //String Description = ActivityText;      
    List<Contact_Team__c> BUlistTemp = [
      SELECT Business_Unit__c 
      FROM Contact_Team__c 
      WHERE (Business_Unit__c LIKE '%BIS' OR Business_Unit__c LIKE '%CIS') 
      AND Contact__c = :SurveyConId
    ];
    system.debug('****: The BU List is ' + BUlistTemp );
    List<String> BUlst = new List<String>();
    //return 'The size of BU List is '+BUlst;
    if (BUlistTemp.size() > 0) {
      for(Contact_Team__c BU: BUlistTemp) {
        //return BU.Business_Unit__c;
        BUlst.add(BU.Business_Unit__c);
      }
    }
    system.debug('***** : The list of matching BUs is ' + BUlst);
    //Query AccountTeam to Find all the members with matching Business Unit as in the List Above.
    // PK NOTE 2016-03-23: Should this be list instead of set?
    system.debug('**** the Account is ' +SurveyAcntId + ' and the bu list is '+ BUlst  );
    //Calculate Due Date
    
    Set<AccountTeamMember> ActAsgnLst = new Set<AccountTeamMember>([
      SELECT UserId 
      FROM AccountTeamMember 
      WHERE AccountId = :SurveyAcntId 
      AND User.Business_Unit__c in:BUlst
    ]);
    
    system.debug('**** The list of members is :' + ActAsgnLst );        
    //Iterate through the Set of Assignees above and create Activities for them.   
    List<Task> SatMRespTask = New List<Task>();
    if (ActAsgnLst.size() > 0) {
      for(AccountTeamMember ActAssignee:ActAsgnLst) {
        Task TaskTemp = new Task();
        TaskTemp.OwnerId = ActAssignee.UserId;
        TaskTemp.WhatId = SurveyAcntId;
        TaskTemp.Subject = 'Client Survey Response from Satmetrix' + ' - ' + NPS_Status;
        TaskTemp.ActivityDate = ActivityDueDate ;
        TaskTemp.Status = 'Not Started';
        TaskTemp.Priority = 'Normal';
        TaskTemp.Type = 'Client Survey';
        TaskTemp.WhoId =  SurveyConId;
        TaskTemp.ParentSurvey__c = [select Id from feedback__C where Id =:SurveyRecId].Id;
        TaskTemp.RecordTypeId = [Select Id,SobjectType,Name From RecordType where Name =:'Survey Task' and SobjectType =:'Task' limit 1].Id; 
        SatMRespTask.add(TaskTemp);
      } 
    }   
    insert SatMRespTask;
    system.debug('>>>>>> the following activity was created' + SatMRespTask);
    return 'Following Activities Were Created '+SatMRespTask;
  } 
 
  
  
  
  public static DateTime getConvertDateTime(string strDT) {
    // build the datetime based on the char positions as this broke very easily
    // e.g. 2015-08-11 08:46
    Integer yr = Integer.valueOf(strDT.mid(0,4));
    Integer mo = Integer.valueOf(strDT.mid(5,2));
    Integer dy = Integer.valueOf(strDT.mid(8,2));
    Integer hr = (strDT.length()>12) ? Integer.valueOf(strDT.mid(11,2)) : 0;
    Integer mi = (strDT.length()>14) ? Integer.valueOf(strDT.mid(14,2)) : 0;
    Integer se = (strDT.length()>17) ? Integer.valueOf(strDT.mid(17,2)) : 0;
    return DateTime.newInstanceGMT(yr, mo, dy, hr, mi, se);
  }
  
  public static String createSurveyRecord(String strFbkId, String strStatus, String strPersonId, String strSubStatus, String strDcname, String strDcid, String strAccountID) {
    List<Contact> lstContact = new List<Contact>();
    List<Case> lstcase = new List<Case>();
    List<Account> lstAccount = new List<Account>();
    Set<String> setContactEmail = new Set<String>();
    lstContact = [
      SELECT Id 
      FROM Contact 
      WHERE Id = :strPersonId
    ];
    system.debug('Tyaga the contact returned is ' + lstContact );
    
    lstAccount = [
        SELECT Id
        FROM Account
        WHERE Id = :strAccountID
    ];
    if (!lstContact.isEmpty()) {
      Feedback__c feedbackListSurvey = new Feedback__c();
      feedbackListSurvey.Name = strFbkId;
      feedbackListSurvey.Contact__c =lstContact[0].Id; //ContactName 
      feedbackListSurvey.Status__c = strStatus;               
      feedbackListSurvey.StatusDescription__c=strSubStatus;
      feedbackListSurvey.DataCollectionName__c = strDcname;
      feedbackListSurvey.DataCollectionId__c = strDcid;
      
      if (!lstAccount.isEmpty())
          feedbackListSurvey.Account__c = strAccountID;
      insert feedbackListSurvey; 
      return 'Success';
    }
    else 
      {
       Apex_Log__c FailedSurveyRec = new Apex_Log__c();
       FailedSurveyRec.Message__c= 'The Survey with id : ' + strFbkId + ' : Failed to be created because the contact with id :' + strPersonId + ' :was not found in system' ; 
       FailedSurveyRec.Exception_Line_Number__c = '310';
       FailedSurveyRec.method_name__c = 'SMXRestResourceRelSurvey'  ;
       FailedSurveyRec.Status_Code__c = 1500;
       FailedSurveyRec.Status_Message__c = strFbkId ;
       FailedSurveyRec.start_dtm__c = system.today();
       insert FailedSurveyRec; 
       system.debug(' **** The apex log record inserted is '+ FailedSurveyRec.Id);
       return 'Failure';
    }
  }
}