/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SealLicenseManagerScheduleHelper implements seal.SealLicenseManager.IScheduleDispatched {
    global SealLicenseManagerScheduleHelper() {

    }
    global void execute(System.SchedulableContext SC) {

    }
    global static Boolean isLicenseCurrent() {
        return null;
    }
    global static Boolean updateLicense() {
        return null;
    }
    @Future(callout=true)
    global static void updateLicenseForSchedule() {

    }
}
