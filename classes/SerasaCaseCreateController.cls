/**=====================================================================
  * Experian
  * Name: SerasaCaseCreateController
  * Description: W-007274: Apex class extension controller for page SerasaCaseCreate
  * Created Date: April 10 2017
  * Created By: Ryan (Weijie) Hu, UCInnovation
  *
  * Date Modified      Modified By                  Description of the 

  * April 28th 2017    Ryan (Weijie) Hu             W-007274: (I959, I960, I961) fixes applied
  * May   1st  2017    Ryan (Weijie) Hu             Translation added
  * Jun   08th 2017    Manoj Gopu                   I1182:Disabling dupeblocker on case Creation
  *=====================================================================*/
public with sharing class SerasaCaseCreateController {

    private final Case mysObject;
    private final String CASE_RT_SERASA_CANCELLATION = 'Serasa_Cancellation';

    private final String ERRORMSG_NO_CCR_SELECTED_TRANSLATED = Label.Serasa_Case_Create_ERRMSG_NotSelected; //Translation needed

    public String recordTypeIdStr;
    public String caseType;
    public RecordType currentSObjRecordType;

    public Boolean errorDetected {get; set;}
    public String dynamicTitle {get; set;}
    public Case parentCase {get; set;}
    public Account parentAccount {get; set;}
    public Contact parentContact {get; set;}
    public List<contractCancellationRequestWrapper> ccrWrapperList {get; set;}
    public String parentCaseIdStr {get; set;}

    public Boolean usingAssignmentRules {get; set;}

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public SerasaCaseCreateController(ApexPages.StandardController stdController) {
        this.mysObject = (Case)stdController.getRecord();
        parentCaseIdStr = (ApexPages.currentPage().getParameters().containsKey('pCaseId')) ? ApexPages.currentPage().getParameters().get('pCaseId') : null;

        // Default Title
        dynamicTitle = Label.Serasa_Case_Create_DynamicTitle_Default;

        errorDetected = true;

        usingAssignmentRules = true;

        recordTypeIdStr = (ApexPages.currentPage().getParameters().containsKey('RecordType')) ? ApexPages.currentPage().getParameters().get('RecordType') : null;
        caseType = (ApexPages.currentPage().getParameters().containsKey('cType')) ? ApexPages.currentPage().getParameters().get('cType') : null;

        List<RecordType> recordTypeList = [SELECT Id, Name, DeveloperName FROM RecordType WHERE Id =: recordTypeIdStr LIMIT 1];

        if (recordTypeList != null && recordTypeList.size() > 0) {
            currentSObjRecordType = recordTypeList.get(0);

            this.mysObject.RecordTypeId = currentSObjRecordType.Id;
            this.mysObject.OwnerId = UserInfo.getUserId();

            if (currentSObjRecordType.DeveloperName == CASE_RT_SERASA_CANCELLATION && caseType == 'cccr') {
                errorDetected = false;
                cancelContractCancellationRequestCase();
            }
        }
    }

    // Save Case button calls this method 
    public PageReference save() {
        if (currentSObjRecordType.DeveloperName == CASE_RT_SERASA_CANCELLATION && caseType == 'cccr') {
            return saveCaseForCancelContractCancellation();
        }

        return null;
    }

    // Cancel button calls this method
    public PageReference cancel() {
        if (currentSObjRecordType.DeveloperName == CASE_RT_SERASA_CANCELLATION && caseType == 'cccr') {
            return cancelCaseForCancelContractCancellation();
        }

        return null;
    }

    // Cancel action is defined here for cancelling a case creation for cancel contract cancellation
    public PageReference cancelCaseForCancelContractCancellation() {
        return new pagereference('/' + parentCaseIdStr);
    }

    // Save action is defined here for saving a case for cancel contract cancellation
    public PageReference saveCaseForCancelContractCancellation() {        

        List<Contract_Cancellation_Request__c> newCCRList = new List<Contract_Cancellation_Request__c>();
        List<Contract_Cancellation_Request__c> originalCCRList = new List<Contract_Cancellation_Request__c>();

        Boolean atLeastOne = false;

        // For each current Cancellation, loop them and find the selected ones. Make a copy of important informations
        if (ccrWrapperList != null && ccrWrapperList.size() > 0) {
            for (contractCancellationRequestWrapper wrapper: ccrWrapperList) {
                if (wrapper.selected == true) {
                    Contract_Cancellation_Request__c ccr = wrapper.ccr.clone(false, true, false, false);
                    ccr.Status__c = 'Processed';
                    ccr.Operation_Type__c = 'Cancel the Cancellation';
                    ccr.Integration_ID__c = '';
                    newCCRList.add(ccr);

                    wrapper.ccr.Status__c = 'Cancelled';
                    originalCCRList.add(wrapper.ccr);

                    atLeastOne = true;
                }
            }
        }

        if (atLeastOne == false) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, ERRORMSG_NO_CCR_SELECTED_TRANSLATED);
            ApexPages.addMessage(msg);
            return null;
        }

        Savepoint sp = Database.setSavepoint();

        try {

            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule = true;

            if (usingAssignmentRules == true) {
                mysObject.setOptions(dmo);

            }
            CRMfusionDBR101.DB_Api.disableDupeBlocker(); //Disabling dupeblocker
            insert mysObject;
            
            for (Contract_Cancellation_Request__c ccr: newCCRList) {
                ccr.case__c = mysObject.Id;
            }

            insert newCCRList;
            update originalCCRList;
        }
        catch (Exception ex) {
            mysObject.Id = null;
            Database.rollback(sp);
            ApexPages.addMessages(ex);

            // Recovery of the old status of the Contract Cancellation for users to see
            for (Contract_Cancellation_Request__c oriCCR : originalCCRList) {

                // We know that they must be pending because only pending ones allow to be in the cancellation process
                oriCCR.Status__c = 'Pending';
            }

            return null;
        }

        return new pagereference('/' + mysObject.Id);
    }

    // Get fields from field sets
    public List<Schema.FieldSetMember> getFields() {
        return SObjectType.Case.FieldSets.Serasa_Cancel_Contract_Cancellation_Req.getFields();
    }

    // Helper method to create case for contract cancellation request
    public void cancelContractCancellationRequestCase() {

        try {

            // Override default title
            dynamicTitle = Label.Serasa_Case_Create_DynamicTitle_CCCR;

            // Pre-populate case information

            // User Island field
            // Auto populate case field matching user's Sales_Team__c field
            User currentUser = [SELECT Id, Sales_Team__c, Business_Unit__c FROM USER WHERE Id =: UserInfo.getUserID()];

            if (currentUser.Business_Unit__c == 'LATAM Serasa Customer Care') {
                mysObject.User_Island__c = currentUser.Sales_Team__c;
            }
            else {
                mysObject.User_Island__c = 'Account Manager';
                mysObject.Island__c = 'Sales Executive';
            }

            Id parentCaseId = Id.valueOf(parentCaseIdStr);
            List<Case> caseList = [SELECT Id, CaseNumber, Subject, RecordTypeId, RecordType.Name, OwnerId, AccountId, ContactId, (SELECT Id, Name, Account__c, Cancellation_Date__c, Contract_Number__c, Description__c, Operation_Type__c, Integration_ID__c, CurrencyIsoCode, Source_System__c, Status__c FROM ContractCancellationRequests__r WHERE Status__c = 'Pending' AND (Operation_Type__c = 'Cancel today' OR Operation_Type__c = 'Cancel in 30 days')) FROM Case WHERE Id =: parentCaseId];
            system.debug('Tyaga the query returned the following cancellation request on the case.' + caseList );
            
            
            if(caseList != null && caseList.size() > 0) {
                parentCase = caseList.get(0);

                this.mysObject.ParentId = parentCase.Id;
                this.mysObject.Case_Type__c = 'Cancellation';

                // Get Parent Account on parent case
                if (parentCase.AccountId != null) {
                    this.mysObject.AccountId = parentCase.AccountId;
                    parentAccount = [SELECT Id, Name, CNPJ_Number__c FROM Account WHERE Id =: parentCase.AccountId].get(0);
                }

                // Get Parent Contact on parent case
                if (parentCase.ContactId != null) {
                    this.mysObject.ContactId = parentCase.ContactId;
                    parentContact = [SELECT Id, Name FROM Contact WHERE Id =: parentCase.ContactId LIMIT 1].get(0);
                }

                

                ccrWrapperList = new List<contractCancellationRequestWrapper>();

                if (parentCase.ContractCancellationRequests__r != null && parentCase.ContractCancellationRequests__r.size() > 0) {
                    
                    system.debug('Tyaga this loop did execute' + parentCase.ContractCancellationRequests__r);
                    for (Contract_Cancellation_Request__c ccr : parentCase.ContractCancellationRequests__r) {
                        ccrWrapperList.add(new contractCancellationRequestWrapper(ccr));
                    } 
                }
            }

        }
        catch (Exception e) {
            // Return error back to visualforce page.
        }
    }

    // Helper class for creating case for contract cancellation request
    public class contractCancellationRequestWrapper {
        public Boolean selected {get; set;}
        public Contract_Cancellation_Request__c ccr {get; set;}

        contractCancellationRequestWrapper(Contract_Cancellation_Request__c ccr) {
            this.ccr = ccr;
        }
    }
}