/**
 * This class contains testclass for expcommunity_DocList.cls
 */
@isTest
private class TestClassForExpcommunity_DocList{

    static testMethod void myUnitTest() {
    
    Test.startTest();
    Folder folderName = [select id, Name from folder where Type = 'Document' limit 1];
        
        Document document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = 'application/pdf';
        document.DeveloperName = 'my_document';
        document.IsPublic = true;
        document.Name = 'My Document';
        document.FolderId = folderName.id;
        insert document;         
        
         
             expcommunity_DocList docCall = new expcommunity_DocList();
             docCall.getfindDocs();
             docCall.DocfolderName = folderName.Name;
             docCall.getfindDocs();
             docCall.DocfolderName = 'test';
             docCall.getfindDocs();
         
         Test.stopTest();
        
    }
    
}