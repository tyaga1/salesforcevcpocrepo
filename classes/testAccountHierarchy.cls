/*=====================================================================
 * Date Modified      Modified By                  Description of the update
 * Oct 30th,2014      Pallavi Sharma(Appirio)      Fix Failure
 * Apr 24th,2016      Cristian Torres (UC In)      Added test method to cover truncated hierarchy scenario
 * Oct 19th, 2016     Manoj Gopu                   Added code to cover the Active Contracts count in Account Structure
=====================================================================*/
@isTest 
private class testAccountHierarchy{

    static testMethod void testAccountHierarchy(){
        User testUser = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
        System.runAs(testUser) {
            AccountHierarchyTestData.createTestHierarchy();
    
            Account topAccount      = [ Select id, name from account where name = 'HierarchyTest0' limit 1 ];
            Account middleAccount   = [ Select id, parentID, name from account where name = 'HierarchyTest4' limit 1 ];
            Account bottomAccount   = [ Select id, parentID, name from account where name = 'HierarchyTest9' limit 1 ];
            Account[] accountList   = [ Select id, parentID, name from account where name like 'HierarchyTest%' ];
    
            test.startTest();
            
            PageReference AccountHierarchyPage = Page.AccountHierarchyPage;
            Test.setCurrentPage( AccountHierarchyPage );
            ApexPages.currentPage().getParameters().put( 'id', topAccount.id );
            ApexPages.currentPage().getParameters().put( 'flagFullHierarchy', '1');
        
            // Instanciate Controller
            AccountStructure controller = new AccountStructure();
            
            // Call Methodes for top account
            controller.setcurrentId( null );
            AccountStructure.ObjectStructureMap[] smt1 = new AccountStructure.ObjectStructureMap[]{};
            smt1 = controller.getObjectStructure();
            System.Assert( smt1.size() > 0, 'Test failed at Top account, no Id' );
    
            controller.setcurrentId( String.valueOf( topAccount.id ) );
            controller.showFullStructure = true;
            AccountStructure.ObjectStructureMap[] smt2 = new AccountStructure.ObjectStructureMap[]{};
            smt2 = controller.getObjectStructure();/*
            System.Assert( smt2.size() > 0, 'Test failed at Top account, with Id: '+smt2.size() );
    
            //Call ObjectStructureMap methodes
            smt2[0].setnodeId( '1234567890' );
            smt2[0].setlevelFlag( true );
            smt2[0].setlcloseFlag( false );
            smt2[0].setnodeType( 'parent' );
            smt2[0].setcurrentNode( false );
            smt2[0].setaccount( topAccount );
            
            String nodeId       = smt2[0].getnodeId();
            Boolean[] levelFlag = smt2[0].getlevelFlag();
            Boolean[] closeFlag = smt2[0].getcloseFlag();
            String nodeType     = smt2[0].getnodeType();
            Boolean currentName = smt2[0].getcurrentNode();
            Account smbAccount  = smt2[0].getaccount();
            */
            // Call Methodes for middle account
            controller.setcurrentId( String.valueOf( middleAccount.id ) );
            AccountStructure.ObjectStructureMap[] smm = new AccountStructure.ObjectStructureMap[]{};
            smm = controller.getObjectStructure();
            //System.Assert( smm.size() > 0, 'Test failed at middle account' );
    
            // Call Methodes for bottom account
            controller.setcurrentId( String.valueOf( bottomAccount.id ) );
            AccountStructure.ObjectStructureMap[] smb = new AccountStructure.ObjectStructureMap[]{};
            smb = controller.getObjectStructure();
            //System.Assert( smb.size() > 0, 'Test failed at top account' );
            
            test.stopTest();
        }
    }
    
    
    //Cristian Torres cloned other test method in order to test a new case 
    static testMethod void testAccountHierarchyAbbreviated(){
        User testUser = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
        System.runAs(testUser) {
            AccountHierarchyTestData.createTestHierarchy();
    
            Account topAccount      = [ Select id, name from account where name = 'HierarchyTest0' limit 1 ];
            Account middleAccount   = [ Select id, parentID, name from account where name = 'HierarchyTest4' limit 1 ];
            Account bottomAccount   = [ Select id, parentID, name from account where name = 'HierarchyTest9' limit 1 ];
            Account[] accountList   = [ Select id, parentID, name from account where name like 'HierarchyTest%' ];
            Contract__c testContract = Test_Utils.insertContract(true, topAccount.Id, Constants.AGENCY, Constants.STATUS_ACTIVE);//Added by Manoj
            
    
            test.startTest();
            
            PageReference AccountHierarchyPage = Page.AccountHierarchyPage;
            Test.setCurrentPage( AccountHierarchyPage );
            ApexPages.currentPage().getParameters().put( 'id', topAccount.id );
            ApexPages.currentPage().getParameters().put( 'flagFullHierarchy', '0');
        
            // Instanciate Controller
            AccountStructure controller = new AccountStructure();
            
            
            ApexPages.StandardController cont;
            AccountStructure controller2 = new AccountStructure(cont);
            
            // Call Methodes for top account
            controller.setcurrentId( null );
            AccountStructure.ObjectStructureMap[] smt1 = new AccountStructure.ObjectStructureMap[]{};
            smt1 = controller.getObjectStructure();
    
            controller.setcurrentId( String.valueOf( topAccount.id ) );
            controller.showFullStructure = true;
            AccountStructure.ObjectStructureMap[] smt2 = new AccountStructure.ObjectStructureMap[]{};
            smt2 = controller.getObjectStructure();
            
            // Call Methodes for middle account
            controller.setcurrentId( String.valueOf( middleAccount.id ) );
            AccountStructure.ObjectStructureMap[] smm = new AccountStructure.ObjectStructureMap[]{};
            smm = controller.getObjectStructure();
            //System.Assert( smm.size() > 0, 'Test failed at middle account' );
    
            // Call Methodes for bottom account
            controller.setcurrentId( String.valueOf( bottomAccount.id ) );
            AccountStructure.ObjectStructureMap[] smb = new AccountStructure.ObjectStructureMap[]{};
            smb = controller.getObjectStructure();
            //System.Assert( smb.size() > 0, 'Test failed at top account' );
            
            test.stopTest();
        }
    }
}