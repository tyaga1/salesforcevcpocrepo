/**=====================================================================
 * Appirio, Inc
 * Test Class Name: OpportunityStageIndicatorHelper_Test
 * Class Name: OpportunityStageIndicatorHelper.cls
 * Description: To test functionality of OpportunityStageIndicatorHelper
 * Created Date: Apr 22nd, 2014
 * Created By: Naresh Kr Ojha (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 * Jan 29th, 2015               Noopur                       T-356448: Added code to cover the wrapper class and edqUKIUser
 * 18th Mar, 2015               Paul Kissick                 Added more tests for other circumstances.
 * Apr 5, 2016                  Paul Kissick                 Case 01028611: Adding new requirement to stage 4
 * Jun 27th, 2016               Manoj Gopu                   Case #01947180 - Remove EDQ specific Contact object fields - MG COMMENTED CODE OUT
 * Aug 9th, 2016                Paul Kissick                 CRM2:W-005396:Adding fix for 'Decider'. Wasn't referencing the constant
 * Aug. 1st, 2017               James Wills                  Case 02079142 - Updated class for new validation.
 =====================================================================*/
@isTest
private class OpportunityStageIndicatorHelper_Test {

  //=========================================================================
  // Test funcitonality of helper class - basic constructor test
  //=========================================================================
  static testMethod void testOpportunityStageIndicatorHelper() {
    // Create Account
    Account accnt = Test_Utils.insertAccount();
    // Create Opportunity
    Opportunity opp = Test_Utils.insertOpportunity(accnt.ID);
    opp.Starting_Stage__c = Constants.OPPTY_STAGE_7;
    update opp;

    PageReference pageRef = Page.OpportunityStageIndicatorPage;
    Test.setCurrentPage(pageRef);

    Test.startTest();

    ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(opp);
    OpportunityStageIndicatorHelper controller = new OpportunityStageIndicatorHelper(sc);

    // Check that constructor executed and populated opportunity
    system.assertEquals(controller.opp.ID, opp.ID);
    User currentUser = [SELECT Id,Region__c
                        FROM User
                        WHERE Id = : userinfo.getUserId()];
    String groupName = BusinessUnitUtility.getBusinessUnit(userinfo.getUserId()) ;
    if (String.isNotBlank(groupName) 
        && groupName.equalsIgnoreCase(Constants.EDQ) 
        && currentUser.Region__c == Constants.REGION_UKI) {
      system.assertEquals(controller.edqUKIUser,true);
    }
    else {
      system.assertEquals(controller.edqUKIUser,false);
    }

    Test.stopTest();
  }

  //=========================================================================
  // Test functionality of helper class for Opp closed/lost
  //=========================================================================
  static testMethod void testOpportunityClosedLost() {
    // Creating admin user
    User testUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    insert testUser;

    system.runAs(testUser) {

      // Create Account
      Account accnt = Test_Utils.insertAccount();
      // Create Opportunity
      Opportunity opp = Test_Utils.insertOpportunity(accnt.ID);
      opp.Starting_Stage__c = Constants.OPPTY_STAGE_7;
      opp.Other_Closed_Reason__c = 'The deal could not be completed.';//Case 02079142
      update opp;
      opp.StageName = Constants.OPPTY_CLOSED_LOST;
      update opp;
      Test_Utils.createOpptyTasks(opp.Id, true);
      Contact newcontact  = new Contact (FirstName = 'Larry', LastName = 'Ellison',
                                    AccountId = accnt.Id, Email = 'larrye@email.com');
      insert newcontact;

      OpportunityContactRole oppContactRole = new OpportunityContactRole(ContactId = newcontact.Id,
                                               OpportunityId = opp.Id, IsPrimary = true, Role = Constants.DECIDER);
      insert oppContactRole ;

      PageReference pageRef = Page.OpportunityStageIndicatorPage;
      Test.setCurrentPage(pageRef);

      Test.startTest();

      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(opp);
      OpportunityStageIndicatorHelper controller = new OpportunityStageIndicatorHelper(sc);

      // Check that constructor executed and populated opportunity
      system.assertEquals(controller.opp.ID, opp.ID);

      system.assert(controller.hasSelectionConfirmed);
      system.assert(controller.hasSignedContract);
      system.assert(controller.hasQuoteDelivered);
      system.assert(controller.hasContactRole);
      system.assert(controller.hasCompletedTask);
      system.assert(controller.hasTechSuppFieldRequirement == false);
      Test.stopTest();
    }
  }

  static testMethod void testEDQUser() {
    CPQ_Settings__c cpqSettings1 = new CPQ_Settings__c(
      Name = 'CPQ',
      Company_Code__c = 'Experian',
      CPQ_API_Access_Word__c = 'password',
      CPQ_API_Endpoint__c = 'https://doesntmatter/endpoint.asmx',
      CPQ_API_UserName__c = 'username'
    );
    insert cpqSettings1;
    Profile p = [SELECT Id FROM Profile WHERE Name=: Constants.PROFILE_SYS_ADMIN ];
    User edqUser = Test_Utils.createEDQUser(p, 'test1234@experian.com', 'test1');
    insert edqUser;
    edqUser.Region__c = Constants.REGION_UKI;
    edqUser.CPQ_User_Type__c = 'EDQ Sales';
    edqUser.CPQ_User__c = false;
    update edqUser;

    system.runAs(edqUser) {

      // Create Account
      Account testAccount = Test_Utils.insertEDQAccount(true);

      // Create an Contact
      Contact con1  = new Contact (FirstName = 'Test 1', LastName = 'Ellison', AccountId = testAccount.Id, Email = 'larrye@email.com');
      //con1.EDQ_On_Demand__c = true;
      con1.Title = 'NewTitle';
      con1.EDQ_Integration_Id__c = 'TESTID0011001002';
      con1.Phone = '9799559433';
      insert con1;

      // Create an opportunity
      Opportunity opp = Test_Utils.insertOpportunity(testAccount.ID);
      opp.Starting_Stage__c = Constants.OPPTY_STAGE_7;
      update opp;

      OpportunityContactRole oppContactRole = new OpportunityContactRole(
        ContactId = con1.Id,
        OpportunityId = opp.Id, 
        IsPrimary = true, 
        Role = Constants.DECIDER
      );
      insert oppContactRole ;

      PageReference pageRef = Page.OpportunityStageIndicatorPage;
      Test.setCurrentPage(pageRef);

      Test.startTest();

      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(opp);
      OpportunityStageIndicatorHelper controller = new OpportunityStageIndicatorHelper(sc);

      //Check that constructor executed and populated opportunity
      system.assertEquals(controller.opp.ID, opp.ID);
      system.assertEquals(true,controller.edqUKIUser,'Not EDQ User');

      Test.stopTest();
    }
  }
  
  //===========================================================================
  // Tests the changeStageTo method.
  //===========================================================================
  @isTest static void testChangingStage() {
    
    // Create Account
    Account accnt = Test_Utils.insertAccount();
    // Create Opportunity
    Opportunity opp = Test_Utils.insertOpportunity(accnt.ID);
    opp.Starting_Stage__c = Constants.OPPTY_STAGE_7;
    update opp;

    PageReference pageRef = Page.OpportunityStageIndicatorPage;
    Test.setCurrentPage(pageRef);

    Test.startTest();

    ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(opp);
    OpportunityStageIndicatorHelper osih = new OpportunityStageIndicatorHelper(sc);
    
    osih.newStage = null;
    system.assertEquals(null, osih.changeStageTo());
    
    osih.newStage = Constants.OPPTY_STAGE_6;
    system.assertEquals(null, osih.changeStageTo());
    
    system.assertEquals(Constants.OPPTY_STAGE_6, [SELECT StageName FROM Opportunity WHERE Id = :opp.Id].StageName);
    
    osih.newStage = Constants.OPPTY_STAGE_7;
    // This isn't allowed, so an error should be returned.
    system.assertEquals(null, osih.changeStageTo());
    
    system.assertNotEquals(0, ApexPages.getMessages().size());

    // Check that the error message you are expecting is in pageMessages
    Boolean foundWarning = false;
    for (ApexPages.Message msg : ApexPages.getMessages()) {
      if (msg.getSummary() == Label.OECS_Change_to_Execute_Below && msg.getSeverity() == ApexPages.Severity.Warning) {
        foundWarning = true;
      }
    }
    
    system.assertEquals(true, foundWarning, 'Warning not found on page!');
    
    system.assertEquals(Constants.OPPTY_STAGE_6, [SELECT StageName FROM Opportunity WHERE Id = :opp.Id].StageName);
    
    Test.stopTest();
    
  }
  
  @testSetup
  static void setupData() {
    insert new Opportunity_Sales_Process__c( Name = Constants.OPPTY_STAGE_7, Sales_Process_Name__c = Constants.OPPTY_SALES_PROCESS_STANDARD);
  }
  
}