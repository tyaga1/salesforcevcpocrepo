/******************************************************************************
 * Name: expcomm_ViewCaseExt.cls
 * Created Date: 2/17/2017
 * Created By: Hay Win
 * Description : Controller for expcomm_viewCase for new Exp Community
 *               to view the cases with id passed from URL
 * Change Log- 
 ****************************************************************************/
 
public class expcomm_ViewCaseExt{

    public Case newCase {get;set;}
    public Case origCase {get;set;}
    public List<Attachment> attachmentList {get;set;}
    public boolean isPlatform {get;set;}
    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public expcomm_ViewCaseExt(ApexPages.StandardController stdController) {
    isPlatform = false;
        this.origCase = (Case)stdController.getRecord();
        List<Case> newCaseList = [Select Id, CaseNumber, User_Requestor__c, Recordtype.Name, Requestor_Email__c, Requestor_Work_Phone__c, Implementation_Status__c, Requester_BU__c, Priority, Reason, Secondary_Case_Reason__c, Status, Subject, Description, Business_Impact__c, User_Impact__c, Date_Due_for_Completion__c,License_Type__c, User_First_Name__c, User_Last_Name__c, Organization_BU__c, Region__c, Country__c, Reports_Into_in_CRM_hierarchy__c, User_s_Manager_s_Email__c, Employee_Number__c, CPQ_access_required__c, User_Job_Title__c, Function__c, User_Email_Address__c, User_Work_Phone__c, User_Mobile_Phone__c, CurrencyIsoCode, User_Timezone__c, User_Language__c, Similar_User__c, Similar_User__r.Name from Case where id =: this.origCase.ID];
        //System.debug('case name '+ this.newCase.CaseNumber);
        newCase = new Case();
        newCase = newCaseList[0];
        
        attachmentList = new List<Attachment>();
        attachmentList =[Select id, Name, BodyLength, ContentType, Description, LastModifiedDate from Attachment where parentID =: this.origCase.ID];
        
        if (newCase.Recordtype.Name == 'Platform Support') {
            isPlatform = true;
        } else if (newCase.Recordtype.Name == 'New User'){
            isPlatform = false;
        }
    }
    
    
    public pagereference CloneCase() {
        PageReference returnPage = new PageReference(Site.getPathPrefix() + '/apex/expcomm_caseCreate?id=' + newCase.Id);
        return returnPage;
    }
    
    public pagereference CloseCase() {
        newCase.Status = 'Closed - Complete';
        update newCase;
        PageReference returnPage = new PageReference(Site.getPathPrefix() + '/apex/expcomm_viewCase?id=' + newCase.Id);
        return returnPage;
    }
}