/**=====================================================================
 * Name: WorkTriggerHandler_Test 
 * Description: Test class for WorkTriggerHandler.cls
 * Created Date: Feb 22nd, 2016
 * Created By: Sadar Yacob
 *
 * Date Modified                Modified By                  Description of the update
 * March 14th 2016              Sadar Yacob                  Use a Custom Per set for Agile_Accelerator_User_Custom to runAS to avoid test class failure
 * Apr 7th, 2016                Paul Kissick                 Case 01932085: Fixing Test User Email Domain
 =====================================================================*/
@isTest
private class WorkTriggerHandler_Test {

  //Things to do and test for this test class: 
  //a.  Create a user Story via Work object
  //b.  Create a Case and Relate a Case to the Work record
  //c.  Change the Status of the work record and do Update
  //d.  check if the Case Status also got changed
   
  static testmethod void testCondition1() {
   
    //Create User
    Profile p = [SELECT id FROM profile WHERE name=: Constants.PROFILE_SYS_ADMIN ];
    User testUser1 = Test_Utils.createUser(p, 'test12334@experian.com', 'test1');
    // User testUser2 = Test_Utils.createUser(p, 'test12335@experian.com', 'test2');
    insert testUser1;
    //insert testUser2;
    IsDataAdmin__c isDataAdmin = Test_Utils.insertIsDataAdmin(true);

    User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

   
    system.runAs(currentUser) {
      // Need to Query permission set name 'Agile Accelerator Admin' to let the test class pass.
      PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'Agile_Accelerator_User_custom']; //Agile_Accelerator_Admin'];
    
      // Assign the above inserted user for the Agile Accelerator Admin Permission Set.
      PermissionSetAssignment psa = new PermissionSetAssignment();
      psa.AssigneeId = testUser1.Id;
      psa.PermissionSetId = ps.Id;
      insert psa; 
    }


    system.runAs(testUser1) {
      Test.startTest();
      
      // Create Global settings
      Global_Settings__c custSetting = Test_Utils.insertGlobalSettings();
      custSetting.Case_Access_Request_TeamRole__c = Constants.CASE_TEAM_ROLE_REQUESTOR;
      update custSetting;

      //Create Scrum Team
      agf__ADM_Scrum_Team__c newTeam = new agf__ADM_Scrum_Team__c(
        Name = 'Test Team',
        agf__Active__c = true,
        agf__Cloud__c = 'IT'
      );
    
      insert newTeam;
    
      //Create Agile Product Tag
      agf__ADM_Product_Tag__c newProductTag = new agf__ADM_Product_Tag__c(
        Name = 'GCSS Product Team',
        agf__Team__c = newTeam.Id,
        agf__Team_Tag_Key__c = '123', 
        agf__Active__c = true
      );
    
      insert newProductTag;
  
      Date todaysDate = system.today();
      agf__ADM_Sprint__c newSprint = new agf__ADM_Sprint__c(
        Name = '2016.01a-Test Sprint 1',
        agf__Scrum_Team__c = newTeam.Id, 
        agf__Start_Date__c = todaysDate,
        agf__End_Date__c = todaysDate + 30
      );
      insert newSprint;
    
      //Create User Story
      agf__ADM_Work__c newUserStory = new agf__ADM_Work__c(
        agf__Subject__c = 'Test User Story123',
        agf__Description__c = 'Test Details goes here',
        agf__Status__c = 'Work In Progress',
        agf__Product_Tag__c = newProductTag.Id,
        agf__Assignee__c = testUser1.Id,
        agf__Due_Date__c = todaysDate
      );
    
  
      insert newUserStory;
    
      List<agf__ADM_Work__c> userStory1 = [SELECT id FROM agf__ADM_Work__c WHERE id = :newUserStory.Id and agf__Status__c = 'Work In Progress'];
      system.assert(userStory1.size() > 0); 
    
      //Create Case and associate with User Story;
      String sfdcRecordTypeId = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_CASE, Constants.CASE_REC_TYPE_SFDC_SUPPORT );
      
      Case newCase = new Case();
      newCase.Type = Constants.CASE_TYPE_INTERNAL;
      newCase.RecordTypeId = sfdcRecordTypeId ;
      newCase.Status = 'Open';
      newCase.Implementation_Status__c = 'CCB Approved';
      newCase.Requestor__c = testUser1.Id;
      //newCase.RequestorEmail
      newCase.Reason = 'Other area';
      newCase.Secondary_Case_Reason__c = 'Other';
      newCase.Subject = 'Test Case 123';
      newCase.Description = 'Test Case Description';
      newCase.agf__ADM_Work__c = newUserStory.Id;
      newCase.Target_Release_Date__c = todaysDate;
      
      insert newCase;
      
      isDataAdmin.IsDataAdmin__c = false;
      update isDataAdmin;
    
      newUserStory.agf__Status__c = 'Dev Complete';
      // Update Status of the Work 
      //Update the Story with a Sprint and other details that should then update the case
      
      newUserStory.agf__Sprint__c = newSprint.Id;
      //newUserStory.agf__Assignee__c =testUser2.Id;
      newUserStory.Status_Notes__c = 'TEST Notes';
      newUserStory.agf__Due_Date__c = todaysDate+30;
      update newUserStory;
     
      Test.stopTest();
    
      List<Case> cseList = [SELECT Id FROM Case WHERE id = :newCase.Id AND Implementation_Status__c = 'Dev Complete'];
      system.assert(cseList.size() > 0);  
     
    }
  }
}