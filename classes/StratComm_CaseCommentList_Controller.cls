public with sharing class StratComm_CaseCommentList_Controller {
    
    public static List<caseComment> caseComments = new List<caseComment>();

    @AuraEnabled
    public static Boolean checkIfCaseIsClosed(Id caseID) {
        Case thisCase = [SELECT id, isClosed FROM Case WHERE id=:caseID];
        if (thisCase.isClosed)
        {
            return true;
        }
        return false;
    }

    @AuraEnabled
    public static List<caseCommentWrapper> findCaseComments(Id caseID) {
        TimeZone userTimezone = UserInfo.getTimeZone();
        List<caseCommentWrapper> caseCommentList = new List<caseCommentWrapper>();
        caseComments = [SELECT id, CommentBody, ParentID, CreatedBy.Name, CreatedBy.ID, CreatedDate FROM CaseComment WHERE IsPublished = true AND ParentID =: caseID ORDER BY CreatedDate DESC];

        for(CaseComment c: caseComments)
        {
            caseCommentWrapper ccw = new caseCommentWrapper();
            ccw.CommentBody = c.CommentBody;
            ccw.CreatedDate = c.CreatedDate.format('yyyy-MM-dd HH:mm:ss', userTimezone.getID());
            ccw.CreatorName = c.CreatedBy.Name;
            ccw.UserLink = c.CreatedBy.ID;
            
            caseCommentList.add(ccw);
        }

        return caseCommentList;
    }


    // Create a map of userIds to isPortalUser boolean to determine what icon to use for each case comment.
    // Need to do this because we cannot query any fields that determine the user's portal user type from the case comment query.
    /*
    @AuraEnabled
    public static Map<string,Boolean> getMapOfUserToIsPortalUser() {
        Set<id> setOfUserIds = new Set<id>();
        Map<string,Boolean> mapOfUserToPortalUserBoolean = new Map<string,Boolean>();

        for(caseComment cc: caseComments)
        {
            setOfUserIds.add(cc.CreatedBy.Id);
        }
        
        List<User> listOfUsers = [SELECT id, Name, AccountId FROM User WHERE id IN:setOfUserIds];

        for(User u: listOfUsers)
        {
            if (u.AccountId == null)
            {
                mapOfUserToPortalUserBoolean.put(u.id, false);
            }
            else
            {
                mapOfUserToPortalUserBoolean.put(u.id, true);
            }
        }

        return mapOfUserToPortalUserBoolean;
    }
    */

    /****Wrapper Classes***/
    public class caseCommentWrapper{
        @AuraEnabled
        public String CommentBody {get; set;}
        @AuraEnabled
        public String CreatorName {get; set;}
        @AuraEnabled
        public String CreatedDate {get; set;}
        @AuraEnabled
        public String UserLink {get; set;}

        public caseCommentWrapper(){

        }
    }

}