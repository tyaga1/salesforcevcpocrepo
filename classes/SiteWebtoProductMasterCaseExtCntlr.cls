/**=====================================================================
 *  Name: SiteWebtoProductMasterCaseExtCntlr 
 * Description: Controler for page  SiteWebtoProductMasterCasePage
 * Created Date: Dec 15th 2014
 * Created By: Richard Joseph
 *
 * Date Modified      Modified By                Description of the update
 * Jan 5th, 2016      James Wills                Case 01266714: Updated text to reflect updates to Case Record Types (see comment below).
 * Feb 3rd, 2016      Paul Kissick               Reformatted!
 =====================================================================*/
public class SiteWebtoProductMasterCaseExtCntlr {
  
  public Product_Master__c productMasterUpdate {get; set;}
  public String productMasterStringUpdate {get; set;}
  public boolean showTypeSelection {get; set;}
  public boolean showNewProductReq {get; set;}
  public boolean showProductUpdateReq {get; set;}
  public String productMasterId {get; set;}

  private Id caseRecId {get; set;}

  private Product_Master__c productMasterRecord;
  
  public SiteWebtoProductMasterCaseExtCntlr(ApexPages.StandardController controller) {
    this.productMasterRecord = (Product_Master__c)controller.getRecord();
    showTypeSelection = true;
    showNewProductReq = false;
    showProductUpdateReq = false;
    try {
      caseRecId = Case.sObjectType.getDescribe().getRecordTypeInfosByName().get('New Product').getRecordTypeId(); //Case #01266714 James Wills; Changed from 'Product Request'
    }
    catch(exception ex) {
    }

  }

  public Pagereference ReqNewProduct() {
    showTypeSelection = false;
    showNewProductReq = true;
    showProductUpdateReq = false;
    return null;
  }
    
  public Pagereference ReqUpdateProduct() {
    showTypeSelection = false;
    showNewProductReq = false;
    showProductUpdateReq = true;
    //return pageRef;
    return null;
  }

  public Pagereference CreateCase() {
    String requestString = '';
    SObjectType sobjType = Schema.getGlobalDescribe().get('Product_Master__c');
    DescribeSObjectResult sobjDef = sobjType.getDescribe();
    Map<String, SObjectField> sfields = sobjDef.fields.getMap();
    Set<String> fieldSet = sfields.keySet();
    for(String s : fieldSet) {
      SObjectField fieldToken = sfields.get(s);
      requestString = requestString + fieldToken.getDescribe().getLabel() + ': ';
      requestString = requestString + (productMasterRecord.get(s) != null ? productMasterRecord.get(s) : '') + '\n';          
    }
    Case newCase = new Case();
    // newCase.Description= String.Valueof('Name: '+productMasterRecord.Name+ '\n Product Master Name: '+ productMasterRecord.Product_master_name__c + '\n Product Description: '+ productMasterRecord.Product_Description__c + '\n Type Of Sale: '+ productMasterRecord.Type_Of_Sale__c  );
    newCase.Description = requestString; 
    newCase.Origin = 'Web';
    newCase.Subject = 'New Product Master Request';
    newCase.RecordTypeId = caseRecId;
    newcase.Reason = 'New Product';
    
    //Fetching the assignment rules on case
    AssignmentRule asgnRule = new AssignmentRule();
    asgnRule = [SELECT Id FROM AssignmentRule WHERE SobjectType = 'Case' AND Active = true LIMIT 1];
    
    //Creating the DMLOptions for "Assign using active assignment rules" checkbox
    Database.DMLOptions dmlOpts = new Database.DMLOptions();
    dmlOpts.assignmentRuleHeader.assignmentRuleId = asgnRule.id;

    //Setting the DMLOption on Case instance
    newCase.setOptions(dmlOpts);
    insert newCase; 
    PageReference pageRef = new PageReference('http://zoom'); // TODO: REPLACE THIS WITH LABEL?          
    return pageRef;
  }
    
  public Pagereference CreateUpdateCase() {
    Case newCase = new Case();
    newCase.Description= String.valueOf('Name: '+productMasterRecord.Name+ '\n Update Description: '+ productMasterStringUpdate);
    newCase.Origin ='Web';
    newCase.Subject ='Update Product Master Request';
    newcase.Reason = 'New Product';
    
    //Fetching the assignment rules on case
    AssignmentRule asgnRule = new AssignmentRule();
    asgnRule = [SELECT Id FROM AssignmentRule WHERE SobjectType = 'Case' AND Active = true LIMIT 1];
    
    //Creating the DMLOptions for "Assign using active assignment rules" checkbox
    Database.DMLOptions dmlOpts = new Database.DMLOptions();
    dmlOpts.assignmentRuleHeader.assignmentRuleId= asgnRule.id;
    
    //Setting the DMLOption on Case instance
    newCase.setOptions(dmlOpts);
    insert newCase; 
    
    PageReference pageRef = new PageReference('http://zoom');               
    return pageRef;
  }
}