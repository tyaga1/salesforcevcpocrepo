/**********************************************************************************
 * Experian, Inc
 * Name: BatchContactStatusUpdateOnlyGoLive_Test
 * Description: Test class for BatchContactStatusUpdateOnlyGoLive
 * Created Date: Aug 23rd, 2016
 * Created By: Tyaga Pati (Experian)
 *
 * Summary:
 * - Test class for the Batch class to update the Contact Status field.
 *
 * Date Modified                Modified By                  Description of the update

 **********************************************************************************/
 
 @isTest
private class BatchContactStatusUpdateOnlyGoLive_Test {

  static testMethod void testBatch() {
    
    Test.startTest();
    
    Database.executeBatch(new BatchContactStatusUpdateOnlyGoLive());
    
    Test.stopTest();
    
    system.assertEquals(0, [SELECT COUNT() FROM Contact WHERE Status__c = null]);
    
  }
    
  static testMethod void testSchedule() {
    
    Test.startTest();
    
    system.schedule('Update the New Status Field', '0 0 * * * ?', new BatchConStatusUpdOneTimeSchedule());
    
    Test.stopTest();
    
  }
  
  @testSetup
  private static void setupData() {
    // Initialise data for test here.
    
    Account a1 = Test_Utils.insertAccount();
    Contact c1 = Test_Utils.createContact(a1.Id);
    Contact c2 = Test_Utils.createContact(a1.Id);
    c1.Status__c = null;
    //c1.Status_old__c = Constants.STATUS_ACTIVE;
    c2.Status__c = null;
    //c2.Status_old__c = Constants.STATUS_ACTIVE;   
    insert new List<Contact>{c1,c2};   
    
  }
  
}