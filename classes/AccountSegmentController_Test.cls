/**=====================================================================
 * Appirio, Inc
 * Name: AccountSegmentController_Test
 * Description: Test class for AccountSegmentController
 * Created Date: Apr 8th, 2015
 * Created By: Nathalie Le Guay (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 * 18th Aug 2015                Parul Gupta                  T-426961
 * 10th May 2017                James Wills                  InsideSales:W-008298 - Added CIS and BIS names to segments to test method updateBISAndCISSegmentValues()
 =====================================================================*/
@isTest(seeAllData = false)
public with sharing class AccountSegmentController_Test {
  static testMethod void controllerTest() {
    //createTestData();
    Account_Segment__c mySegment = [SELECT Id, Parent_Account_Segment__c, Account__c, Value__c,
                                           Segment__c
                                    FROM Account_Segment__c
                                    WHERE Segment__r.Value__c = 'Child1'];
    ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(mySegment);

    Test.startTest();
    AccountSegmentController controller = new AccountSegmentController(sc);

    System.assertNotEquals(null, controller.segment);
    System.assertEquals('Grand Parent > Parent > Child1', controller.title);
    System.assertEquals(false, controller.isEdit);

    controller.edit();
    System.assertEquals(true, controller.isEdit);

    controller.cancel();
    System.assertEquals(false, controller.isEdit);

    controller.segment.Total_Won__c = 15;
    controller.save();
    System.assertEquals(false, controller.isEdit);
    
    controller.edit();    
    controller.segment.AHS_Health_Status__c = 'Yellow';
    controller.segment.AHS_Include_in_Status_Report__c = true;
    controller.save();
    
    // Enforce validation rules to fail and assert for page messages
    controller.edit(); 
    controller.segment.AHS_Include_in_Status_Report__c = false;
    controller.save();
    system.assert(ApexPages.getMessages().size() > 0);

    Test.stopTest();
  }


  static testMethod void deleteSegment_Test(){
    Account_Segment__c mySegment = [SELECT Id, Account__c FROM Account_Segment__c WHERE Name = 'CIS - Growth'];
    
    Test.startTest();    
      delete mySegment;      
    Test.stopTest();
  }


  @testSetup 
  static void createTestData() {
    Account testAcc = Test_Utils.createAccount();
    insert testAcc;

    Hierarchy__c grandParentHierarchy = new Hierarchy__c();
    grandParentHierarchy.Type__c = 'Global Business Line';
    grandParentHierarchy.Value__c = 'Grand Parent';
    grandParentHierarchy.Unique_Key__c = 'My BU';
    insert grandParentHierarchy;

    Hierarchy__c parentHierarchy = new Hierarchy__c();
    parentHierarchy.Type__c = 'Business Line';
    parentHierarchy.Value__c = 'Parent';
    parentHierarchy.Unique_Key__c = 'My BU Blah';
    parentHierarchy.Parent__c = grandParentHierarchy.Id;
    insert parentHierarchy;

    Hierarchy__c childHierarchy1 = new Hierarchy__c();
    childHierarchy1.Type__c = 'Business Unit';
    childHierarchy1.Value__c = 'Child1';
    childHierarchy1.Parent__c = parentHierarchy.Id;
    childHierarchy1.Unique_Key__c = 'My BU Blah2';
    insert childHierarchy1;

    Hierarchy__c childHierarchy2 = new Hierarchy__c();
    childHierarchy2.Type__c = 'Business Unit';
    childHierarchy2.Value__c = 'Child2';
    childHierarchy2.Parent__c = parentHierarchy.Id;
    childHierarchy2.Unique_Key__c = 'My BU Blah2';
    insert childHierarchy2;


    Account_Segment__c segment = Test_Utils.insertAccountSegment(false, testAcc.Id, grandParentHierarchy.Id, null);
    segment.Name     = 'BIS - Growth';
    insert segment;

    Account_Segment__c segment2 = Test_Utils.insertAccountSegment(false, testAcc.Id, parentHierarchy.Id, segment.Id);
    insert segment2;

    Account_Segment__c segment3 = Test_Utils.insertAccountSegment(false, testAcc.Id, childHierarchy1.Id, segment2.Id);
    insert segment3;
    
    Account_Segment__c segment4 = Test_Utils.insertAccountSegment(false, testAcc.Id, childHierarchy2.Id, segment2.Id);    
    segment4.Name = 'CIS - Growth';
    insert segment4;
    
    Account_Segment__c segment5 = new Account_Segment__c(Account__c = testAcc.Id);
    segment5.Value__c =  Constants.REGION_GLOBAL;
    segment5.Type__c   = 'Region';
    insert segment5;

    Account_Segmentation_Mapping__c mapping = new Account_Segmentation_Mapping__c();
    mapping.Name = 'Child';
    mapping.Global_Business_Line__c = 'Grand Parent';
    mapping.Business_Line__c = 'Parent';
    mapping.Business_Unit__c = 'Child';
    mapping.Common_View_Name__c = 'CSDA';
    mapping.Field_Set_API_Name__c = 'CSDA';
    insert mapping;
  }

}