/*=============================================================================
 * Experian
 * Name: MembershipOnboardingToolUtils
 * Description: 
 * Created Date: 15 Jun 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/

public class MembershipOnboardingToolUtils {
  
  public static List<SelectOption> getClientIndustry() {
    List<SelectOption> selectList = new List<SelectOption>();
    selectList.add(new SelectOption('','-- '+Membership__c.Client_Industry__c.getDescribe().getLabel()+' --'));
    for (Schema.PicklistEntry pl : Membership__c.Client_Industry__c.getDescribe().getPicklistValues()) {
      selectList.add(new SelectOption(pl.getValue(), pl.getLabel()));
    }
    system.debug('Total Industry Options: '+ String.valueOf(selectList.size()-1));
    return selectList;
  }
  
  public static List<SelectOption> getProductToBeSold(Set<String> allowedProducts, Boolean applyFilter) {
    List<SelectOption> selectList = new List<SelectOption>();
    selectList.add(new SelectOption('','-- '+Membership__c.Product_to_be_Sold__c.getDescribe().getLabel()+' --'));
    for (Schema.PicklistEntry pl : Membership__c.Product_to_be_Sold__c.getDescribe().getPicklistValues()) {
      //system.debug(pl.getValue() + ' : IsDisabled : '+!allowedProducts.contains(pl.getValue()));
      selectList.add(new SelectOption(pl.getValue(), pl.getLabel(), (applyFilter && !allowedProducts.contains(pl.getValue()))));
    }
    system.debug('Total Product Options: '+ String.valueOf(selectList.size()-1));
    return selectList;
  }
  
  public static List<SelectOption> getPermissiblePurpose(Set<String> allowedPermissiblePurposes, Boolean applyFilter) {
    List<SelectOption> selectList = new List<SelectOption>();
    selectList.add(new SelectOption('','-- '+Membership__c.Permissible_Purpose__c.getDescribe().getLabel()+' --'));
    for (Schema.PicklistEntry pl : Membership__c.Permissible_Purpose__c.getDescribe().getPicklistValues()) {
      //system.debug(pl.getValue() + ' : IsDisabled : '+!allowedPermissiblePurposes.contains(pl.getValue()));
      selectList.add(new SelectOption(pl.getValue(), pl.getLabel(), (applyFilter && !allowedPermissiblePurposes.contains(pl.getValue()))));
    }
    system.debug('Total Purpose Options: '+String.valueOf(selectList.size()-1));
    return selectList;
  }
  
  public static List<SelectOption> getThirdParty(Set<String> allowedThirdParty, Boolean applyFilter) {
    List<SelectOption> selectList = new List<SelectOption>();
    selectList.add(new SelectOption('','-- '+Membership__c.Third_Party_Involvement__c.getDescribe().getLabel()+' --'));
    for (Schema.PicklistEntry pl : Membership__c.Third_Party_Involvement__c.getDescribe().getPicklistValues()) {
      //system.debug(pl.getValue() + ' : IsDisabled : '+!allowedThirdParty.contains(pl.getValue()));
      selectList.add(new SelectOption(pl.getValue(), pl.getLabel(), (applyFilter && !allowedThirdParty.contains(pl.getValue()))));
    }
    system.debug('Total Third Party Options: '+String.valueOf(selectList.size()-1));
    return selectList;
  }
  
}