public class OpportunityPlanContactController {
    private final Opportunity_Plan_Contact__c opportunityPlanContact;
    ApexPages.StandardController stdController;
    
    //List<String> clientGoalsList {get; set;}
    public List<Boolean> business_Goals{get;set;} 
    public List<Boolean> decision_Criterias{get;set;} 
    public List<Boolean> personal_Goals{get;set;} 
    public List<Boolean> solution_benefits{get;set;} 
    public List<Boolean> experian_differentiator{get;set;} 

    public String pageTitle{get;set;}

    public boolean isEdit{get;set;}

    Integer cur_business_Goals = 1;
    Integer cur_decision_Criterias = 1;
    Integer cur_personal_Goals = 1;
    Integer cur_solution_benefits = 1;
    Integer cur_experian_differentiator = 1;

    String error_message = 'You have reached the maximum of 5 entries for this section, no more can be added.';
    
    public OpportunityPlanContactController(ApexPages.StandardController stdController) {
        this.stdController = stdController;
        // grab parameters
        String oppPlanId = ApexPages.currentPage().getParameters().get('oppPlanId');
        //String usrId = ApexPages.currentPage().getParameters().get('usrId');
        //String accId = ApexPages.currentPage().getParameters().get('accId');
        this.opportunityPlanContact = (Opportunity_Plan_Contact__c)stdController.getRecord();

        business_Goals  = new List<Boolean>{true, false, false, false, false};
        personal_Goals  = new List<Boolean>{true, false, false, false, false};
        solution_benefits  = new List<Boolean>{true, false, false, false, false};
        decision_Criterias  = new List<Boolean>{true, false, false, false, false};
        experian_differentiator  = new List<Boolean>{true, false, false, false, false};
        isEdit = false;

        if( oppPlanId != null ){
            // query the name of opportunity plan 
            //String oppPlanName = [select Name from Opportunity_Plan__c where Id=:oppPlanId limit 1].Name;
            this.opportunityPlanContact.Opportunity_Plan__c = oppPlanId;
        }
        else{
            if( ApexPages.currentPage().getParameters().get('id') != null ){
                // editing an existing opportunity plan contact
                isEdit = true;
                if(this.opportunityPlanContact.Business_Goal_1__c != null ) business_Goals[0] = true;
                if(this.opportunityPlanContact.Business_Goal_2__c != null ) business_Goals[1] = true;
                if(this.opportunityPlanContact.Business_Goal_3__c != null ) business_Goals[2] = true;
                if(this.opportunityPlanContact.Business_Goal_4__c != null ) business_Goals[3] = true;
                if(this.opportunityPlanContact.Business_Goal_5__c != null ) business_Goals[4] = true;
                adjustDisplay(business_Goals);
                cur_business_Goals = getLastTrue(business_Goals) + 1;

                if(this.opportunityPlanContact.Decision_Criteria_1__c != null ) decision_Criterias[0] = true;
                if(this.opportunityPlanContact.Decision_Criteria_2__c != null ) decision_Criterias[1] = true;
                if(this.opportunityPlanContact.Decision_Criteria_3__c != null ) decision_Criterias[2] = true;
                if(this.opportunityPlanContact.Decision_Criteria_4__c != null ) decision_Criterias[3] = true;
                if(this.opportunityPlanContact.Decision_Criteria_5__c != null ) decision_Criterias[4] = true;
                adjustDisplay(decision_Criterias); 
                cur_decision_Criterias = getLastTrue(decision_Criterias) + 1;

                if(this.opportunityPlanContact.Personal_Goal_1__c != null ) personal_Goals[0] = true;
                if(this.opportunityPlanContact.Personal_Goal_2__c != null ) personal_Goals[1] = true;
                if(this.opportunityPlanContact.Personal_Goal_3__c != null ) personal_Goals[2] = true;
                if(this.opportunityPlanContact.Personal_Goal_4__c != null ) personal_Goals[3] = true;
                if(this.opportunityPlanContact.Personal_Goal_5__c != null ) personal_Goals[4] = true;
                adjustDisplay(personal_Goals);
                cur_personal_Goals = getLastTrue(personal_Goals) + 1;

                if(this.opportunityPlanContact.Solution_Benefits_1__c != null ) solution_benefits[0] = true;
                if(this.opportunityPlanContact.Solution_Benefits_2__c != null ) solution_benefits[1] = true;
                if(this.opportunityPlanContact.Solution_Benefits_3__c != null ) solution_benefits[2] = true;
                if(this.opportunityPlanContact.Solution_Benefits_4__c != null ) solution_benefits[3] = true;
                if(this.opportunityPlanContact.Solution_Benefits_5__c != null ) solution_benefits[4] = true;
                adjustDisplay(solution_benefits);
                cur_solution_benefits = getLastTrue(solution_benefits) + 1;

                if(this.opportunityPlanContact.Exp_Differentiator_1__c != null ) experian_differentiator[0] = true;
                if(this.opportunityPlanContact.Exp_Differentiator_2__c != null ) experian_differentiator[1] = true;
                if(this.opportunityPlanContact.Exp_Differentiator_3__c != null ) experian_differentiator[2] = true;
                if(this.opportunityPlanContact.Exp_Differentiator_4__c != null ) experian_differentiator[3] = true;
                if(this.opportunityPlanContact.Exp_Differentiator_5__c != null ) experian_differentiator[4] = true;
                adjustDisplay(experian_differentiator);
                cur_experian_differentiator = getLastTrue(experian_differentiator) + 1;
            }
            // otherwise, black page
        }

        if( isEdit == false ){
            // this is a new opportunity plan contact
            this.opportunityPlanContact.Link_Manager__c = true;
        }
    }

    private void adjustDisplay(List<boolean> lis){
        Integer lastTrue = getLastTrue(lis);
        for(Integer i = 0; i <= lastTrue; i++){
            lis[i] = true;
        }
    }

    public pageReference add_business_goals() {
        if (cur_business_Goals < 5) {
            business_Goals[cur_business_Goals] = true;
            cur_business_Goals++;
            return null;
        }
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, error_message ));
        return null;
    }

    public pageReference add_decision_Criterias() {
        if (cur_decision_Criterias < 5) {
            decision_Criterias[cur_decision_Criterias] = true;
            cur_decision_Criterias++;
            return null;
        }
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, error_message ));
        return null;
    }

    public pageReference add_personal_Goals() {
        if (cur_personal_Goals < 5) {
            personal_Goals[cur_personal_Goals] = true;
            cur_personal_Goals++;
            return null;
        }
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, error_message ));
        return null;
    }

    public pageReference add_solution_benefits() {
        if (cur_solution_benefits < 5) {
            solution_benefits[cur_solution_benefits] = true;
            cur_solution_benefits++;
            return null;
        }
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, error_message ));
        return null;
    }

    public pageReference add_experian_differentiator() {
        if (cur_experian_differentiator < 5) {
            experian_differentiator[cur_experian_differentiator] = true;
            cur_experian_differentiator++;
            return null;
        }
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, error_message ));
        return null;
    }

    private boolean noGap_businessGoal(){
        List<boolean> bg_has_content = new List<boolean>{false, false, false, false, false};
        if(this.opportunityPlanContact.Business_Goal_1__c != null ) bg_has_content[0] = true;
        if(this.opportunityPlanContact.Business_Goal_2__c != null ) bg_has_content[1] = true;
        if(this.opportunityPlanContact.Business_Goal_3__c != null ) bg_has_content[2] = true;
        if(this.opportunityPlanContact.Business_Goal_4__c != null ) bg_has_content[3] = true;
        if(this.opportunityPlanContact.Business_Goal_5__c != null ) bg_has_content[4] = true;

        Integer lastTrue = getLastTrue(bg_has_content);
        if( bg_has_content[0] == false && lastTrue > 0 ) return false;
        // from the first one to the last one that is true, if there's anything inbetween
        for(Integer i = 1; i <= lastTrue; i++){
            if( bg_has_content[i] == false ){
                return false;   // if anything inbetween is false, there's a gap, return false
            }
        }
        // if anything inbetween is true, there's no gap, return true
        return true;
    }

    private boolean noGap_decisionCriterias(){
        List<boolean> dc_has_content = new List<boolean>{false, false, false, false, false};
        if(this.opportunityPlanContact.Decision_Criteria_1__c != null ) dc_has_content[0] = true;
        if(this.opportunityPlanContact.Decision_Criteria_2__c != null ) dc_has_content[1] = true;
        if(this.opportunityPlanContact.Decision_Criteria_3__c != null ) dc_has_content[2] = true;
        if(this.opportunityPlanContact.Decision_Criteria_4__c != null ) dc_has_content[3] = true;
        if(this.opportunityPlanContact.Decision_Criteria_5__c != null ) dc_has_content[4] = true;

        Integer lastTrue = getLastTrue(dc_has_content);
        if( dc_has_content[0] == false && lastTrue > 0 ) return false;
        // from the first one to the last one that is true, if there's anything inbetween
        for(Integer i = 1; i <= lastTrue; i++){
            if( dc_has_content[i] == false ){
                return false;   // if anything inbetween is false, there's a gap, return false
            }
        }
        // if anything inbetween is true, there's no gap, return true
        return true;
    }

    private boolean noGap_personalGoal(){
        List<boolean> pg_has_content = new List<boolean>{false, false, false, false, false};
        if(this.opportunityPlanContact.Personal_Goal_1__c != null ) pg_has_content[0] = true;
        if(this.opportunityPlanContact.Personal_Goal_2__c != null ) pg_has_content[1] = true;
        if(this.opportunityPlanContact.Personal_Goal_3__c != null ) pg_has_content[2] = true;
        if(this.opportunityPlanContact.Personal_Goal_4__c != null ) pg_has_content[3] = true;
        if(this.opportunityPlanContact.Personal_Goal_5__c != null ) pg_has_content[4] = true;

        Integer lastTrue = getLastTrue(pg_has_content);
        if( pg_has_content[0] == false && lastTrue > 0 ) return false;
        // from the first one to the last one that is true, if there's anything inbetween
        for(Integer i = 1; i <= lastTrue; i++){
            if( pg_has_content[i] == false ){
                return false;   // if anything inbetween is false, there's a gap, return false
            }
        }
        // if anything inbetween is true, there's no gap, return true
        return true;
    }

    private boolean noGap_solutionBenefits(){
        List<boolean> sb_has_content = new List<boolean>{false, false, false, false, false};
        if(this.opportunityPlanContact.Solution_Benefits_1__c != null ) sb_has_content[0] = true;
        if(this.opportunityPlanContact.Solution_Benefits_2__c != null ) sb_has_content[1] = true;
        if(this.opportunityPlanContact.Solution_Benefits_3__c != null ) sb_has_content[2] = true;
        if(this.opportunityPlanContact.Solution_Benefits_4__c != null ) sb_has_content[3] = true;
        if(this.opportunityPlanContact.Solution_Benefits_5__c != null ) sb_has_content[4] = true;

        Integer lastTrue = getLastTrue(sb_has_content);
        if( sb_has_content[0] == false && lastTrue > 0 ) return false;
        // from the first one to the last one that is true, if there's anything inbetween
        for(Integer i = 1; i <= lastTrue; i++){
            if( sb_has_content[i] == false ){
                return false;   // if anything inbetween is false, there's a gap, return false
            }
        }
        // if anything inbetween is true, there's no gap, return true
        return true;
    }

    private boolean noGap_experianDifferentiator(){
        List<boolean> exp_has_content = new List<boolean>{false, false, false, false, false};
        if(this.opportunityPlanContact.Exp_Differentiator_1__c != null ) exp_has_content[0] = true;
        if(this.opportunityPlanContact.Exp_Differentiator_2__c != null ) exp_has_content[1] = true;
        if(this.opportunityPlanContact.Exp_Differentiator_3__c != null ) exp_has_content[2] = true;
        if(this.opportunityPlanContact.Exp_Differentiator_4__c != null ) exp_has_content[3] = true;
        if(this.opportunityPlanContact.Exp_Differentiator_5__c != null ) exp_has_content[4] = true;

        Integer lastTrue = getLastTrue(exp_has_content);
        if( exp_has_content[0] == false && lastTrue > 0 ) return false;
        // from the first one to the last one that is true, if there's anything inbetween
        for(Integer i = 1; i <= lastTrue; i++){
            if( exp_has_content[i] == false ){
                return false;   // if anything inbetween is false, there's a gap, return false
            }
        }
        // if anything inbetween is true, there's no gap, return true
        return true;
    }

    public pageReference save(){
        // pop up error message if there's a gap
        if( noGap_businessGoal() == false ){
            // pop up error message
            String message = 'Please fill in all Business Goals before saving.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, message ));
            return null;
        }

        // pop up error message if there's a gap
        if( noGap_decisionCriterias() == false ){
            // pop up error message
            String message = 'Please fill in all Decision Criterias before saving.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, message ));
            return null;
        }

        // pop up error message if there's a gap
        if( noGap_personalGoal() == false ){
            // pop up error message
            String message = 'Please fill in all Personal Goals before saving.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, message ));
            return null;
        }

        if( noGap_solutionBenefits() == false ){
            // pop up error message
            String message = 'Please fill in all Solution Benefits before saving.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, message ));
            return null;
        }

        if( noGap_experianDifferentiator() == false ){
            // pop up error message
            String message = 'Please fill in all Experian Differentiator before saving.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, message ));
            return null;
        }
		 
		if (!isEdit) {
			// Check the contact and make sure there is no opportunity plan contact with that contact already. 
			Id contactId = opportunityPlanContact.Contact__c;
			List<Opportunity_Plan_Contact__c> existingContacts = [Select Id From Opportunity_Plan_Contact__c Where Opportunity_Plan__c =: this.opportunityPlanContact.Opportunity_Plan__c AND Contact__c =:contactId];
			
			if (!existingContacts.isEmpty()) {
				// pop up error message
	            String message = 'There is already an existing Opportunity Plan Contact with this Contact. Please select another Contact and try again';
	            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, message ));
	            return null;
			}
		}
		

        try {
            if( isEdit == true ){
                update this.opportunityPlanContact;
            }
            else{
                insert this.opportunityPlanContact;
            }
        } catch (Exception e) {
            ApexPages.addMessages(e);  
            return null;
        }
        
        if( this.opportunityPlanContact.Contact__c != null ){
            // if we have a contact as input, we check look up this contact in opportunity contact roles, if contact match, get the role
            // and fill it into the role read only field
            boolean hasException = false;
            // first get Opportunity plan for looking up the record
            Id oppPlanId;
            // second look up the opportunity inside the Opportunity_Plan__c
            Id oppId; 
            // third look up the Role from Opportunity Contact Role where the Opportunity Id match the Opportunity Id we have
            String role_r = ''; 
            
            try{
                oppPlanId  = this.opportunityPlanContact.Opportunity_Plan__c;
                //System.debug('opportunity plan Id is: ' + oppPlanId); // a1De0000001GqtjEAC
                oppId = [select Opportunity_Name__c from Opportunity_Plan__c where Id=:oppPlanId limit 1].Opportunity_Name__c;
                //System.debug('opportunity Id is: ' + oppId); // 006e0000009y002AAA
                List<OpportunityContactRole> ocr = [select ContactId, Role from OpportunityContactRole where OpportunityId=:oppId];
                List<String> roles = new List<String>();
                for(Integer i = 0; i < ocr.size(); i++){
                    if( ocr[i].ContactId ==  this.opportunityPlanContact.Contact__c){
                        roles.add(ocr[i].Role);
                    }
                }
                for(Integer i = 0; i < roles.size(); i++){
                    if( i == (roles.size()-1) ){
                        role_r = role_r + roles[i];
                    }
                    else{
                        role_r = role_r + roles[i]+ ', ';
                    }
                }
            }catch(Exception e){
                System.debug('Could not find a role for the contact__c in the OpportunityPlanContact you just edit or create!');
                System.debug(e.getMessage());
                hasException = true;
            }
            
            if( hasException == false ){
            	/*
            	 Old code to update the role only if it didn't previously have a value.
            	 Reverted back to the original, where it is always updated. 
            	Boolean updateRoleReadOnly = true;
            	
            	if (opportunityPlanContact.Id != null) {
            		List<Opportunity_Plan_Contact__c> oppPlanContactList = [select Id, Role_Read_Only__c From Opportunity_Plan_Contact__c where Id =: opportunityPlanContact.Id];
					
					if (!oppPlanContactList.isEmpty()) {
						Opportunity_Plan_Contact__c oppPlanContact = oppPlanContactList.get(0);
						
						if (oppPlanContact.Role_Read_Only__c != null && oppPlanContact.Role_Read_Only__c != '') {
							updateRoleReadOnly = false;
						}
					}
            	}
            	
            	
            	// Only update the role read only if it is has no value. 
            	if (updateRoleReadOnly == true) {
            		this.OpportunityPlanContact.Role_Read_Only__c = role_r;
            	}
            	*/
            		
            	this.OpportunityPlanContact.Role_Read_Only__c = role_r;	
                update this.opportunityPlanContact;   
            } 
        }
        
        
        PageReference pr = new PageReference('/' + this.opportunityPlanContact.Id);
        // Don't redirect with the viewstate of the current record.
        pr.setRedirect(true);
        return pr;
    }
    
    public pageReference save_and_new(){
        pageReference tem = this.save();
        if(tem == null) return null;
        // Create PageReference for creating a new sObject and add any inbound query string parameters.
        PageReference pr = new PageReference('/apex/OpportunityPlanContact');
        
        // If it is a new page, it will contain the oppPlanId.
        if (ApexPages.currentPage().getParameters().containsKey('oppPlanId')) {
        	pr.getParameters().put('oppPlanId', ApexPages.currentPage().getParameters().get('oppPlanId'));
        } else {
        	
        	// If it does not contain the oppPlanId in the URL, find it. 
        	Id oppPlanId;
        	
        	try {
        		oppPlanId  = this.opportunityPlanContact.Opportunity_Plan__c;
        		pr.getParameters().put('oppPlanId', oppPlanId);
        	} catch (exception e) {
        		
        	}
        	
        }
        
        pr.getParameters().put('retURL', ApexPages.currentPage().getParameters().get('retURL'));
        
        // Don't redirect with the viewstate of the current record.
        pr.setRedirect(true);
        return pr;
    }

    private Integer getLastTrue(List<Boolean> lis){
        for(Integer i = lis.size()-1; i >= 0; i--){
            if(lis[i] == true) return i;
        }
        return 0;
    }
}