/**=====================================================================
 * Name: itcaTeamSummaryController_Test
 * Description: 
 * Created Date: August 30th, 2017
 * Created By: Malcolm Russell
 * 
 * Date Modified         Modified By            Description of the update
 * 
 * =====================================================================*/
@isTest
private class itcaTeamSummaryController_Test {

     static testMethod void myUnitTest1() {
        
   // Career_Architecture_User_Profile__c CAUP = new Career_Architecture_User_Profile__c(Name='Test');
    createprofile(); 
    Career_Architecture_User_Profile__c CAUP = [select id from Career_Architecture_User_Profile__c where Employee__c=:UserInfo.getUserId()  LIMIT 1];
    
    PageReference pageRef = Page.ITCA_Team_Summary;
        Test.setCurrentPage(pageRef);
    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)CAUP);
    
    itcaTeamSummaryController ext = new itcaTeamSummaryController(stdController);
    
    List<itcaTeamSummaryController.itcaTeamUser> teamList =ext.teamList;
    
  }
  
  @testSetup
  private static void createData(){
    
    buildProfileSkills();
    buildExperianSkillSet();
    buildSkillSetToSKill();
   
   
  }

  private static void buildProfileSkills(){
  
    ProfileSkill ps1 = new ProfileSkill(Name='Apex', 
                                        Level5__c='Maintains Code.', 
                                        Level6__c='Writes test classes.', 
                                        Level7__c='Designs Code.',
                                        Sfia_Skill__c=false);
                                        
    ProfileSkill ps2 = new ProfileSkill(Name='Programming & Software Development', 
                                        Level5__c='Maintains Code.', 
                                        Level6__c='Writes test classes.', 
                                        Level7__c='Designs Code.',
                                        Sfia_Skill__c=true);                                  

    List<ProfileSkill> ps_List = new List<ProfileSkill>{ps1,ps2};
    insert ps_List;
    
  }

  private static void buildExperianSkillSet(){
    Experian_Skill_Set__c exp1 = new Experian_Skill_Set__c(Name='Software Development', Career_Area__c='Applications');
    insert exp1;
  
  }

  private static void buildSkillSetToSKill(){
    List<ProfileSkill> psList = [SELECT id, Name FROM ProfileSkill];
    List<Experian_Skill_Set__c> essList = [SELECT id FROM Experian_Skill_Set__c];
   
    Skill_Set_to_Skill__c sts1 = new Skill_Set_to_Skill__c(Name='Software Development - Application Support', 
                                                          Skill__c=[SELECT id FROM ProfileSkill where Sfia_Skill__c=true LIMIT 1].id, 
                                                          Experian_Role__c='Professional',
                                                          Experian_Skill_Set__c=essList[0].id,
                                                          Level__c='Level4');
    insert sts1;
  
 }

  private static void createProfile(){
  
    Profile p = [SELECT Id FROM Profile WHERE Name=: Constants.PROFILE_SYS_ADMIN ];
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    testUser1.ManagerId = UserInfo.getUserId();
    insert testUser1;
    
    Career_Architecture_User_Profile__c caup1 = new Career_Architecture_User_Profile__c(//Career_Area__c=,
                                                                                        //Date_Discussed_with_Employee__c=,
                                                                                        //Discussed_with_Employee__c=,
                                                                                        Employee__c= testUser1.id,
                                                                                        //Manager_Comments__c=,
                                                                                        //Role__c=,
                                                                                        State__c='Current',
                                                                                        Status__c='Submitted to Manager');
    insert caup1;
    
    User testUser2 = Test_Utils.createUser(p, 'test9876@experian.com', 'test2');
    testUser2.ManagerId = UserInfo.getUserId();
    insert testUser2;
    
    Career_Architecture_User_Profile__c caup2 = new Career_Architecture_User_Profile__c(//Career_Area__c=,
                                                                                        //Date_Discussed_with_Employee__c=,
                                                                                        //Discussed_with_Employee__c=,
                                                                                        Employee__c= testUser2.id,
                                                                                        //Manager_Comments__c=,
                                                                                        //Role__c=,
                                                                                        State__c='Future Plan',
                                                                                        Status__c='Submitted to Manager');
    insert caup2;
    
    Career_Architecture_User_Profile__c caup3 = new Career_Architecture_User_Profile__c(//Career_Area__c=,
                                                                                        //Date_Discussed_with_Employee__c=,
                                                                                        //Discussed_with_Employee__c=,
                                                                                        Employee__c= UserInfo.getUserId(),
                                                                                        //Manager_Comments__c=,
                                                                                        //Role__c=,
                                                                                        State__c='Current',
                                                                                        Status__c='Submitted to Manager');
    insert caup3;
    
    System.runAs(testUser1){
     
    Career_Architecture_Skills_Plan__c casp1 = new Career_Architecture_Skills_Plan__c(Career_Architecture_User_Profile__c=caup1.id,
                                                                                      Experian_Skill_Set__c=[SELECT id FROM Experian_Skill_Set__c LIMIT 1].id,
                                                                                      Level__c='Level4',
                                                                                      Skill__c=[SELECT id FROM ProfileSkill LIMIT 1].id  );
    
    insert casp1;
    }
    System.runAs(testUser2){
     
    Career_Architecture_Skills_Plan__c casp2 = new Career_Architecture_Skills_Plan__c(Career_Architecture_User_Profile__c=caup2.id,
                                                                                      Experian_Skill_Set__c=[SELECT id FROM Experian_Skill_Set__c LIMIT 1].id,
                                                                                      Level__c='Level4',
                                                                                      Skill__c=[SELECT id FROM ProfileSkill LIMIT 1].id  );
    
    insert casp2;
    }
  }
}