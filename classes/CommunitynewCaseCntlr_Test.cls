/**=====================================================================
 * Name: CommunitynewCaseCntlr_TestClass
 * Description: T-376603: Test class for CommunitynewCaseCntlr
 *
 * Created Date: June 1st, 2016
 * Created By: Richard Joseph
 *
 * Date Modified            Modified By              Description of the update
 ======================================================================*/
@isTest
private class CommunitynewCaseCntlr_Test {

  private static testMethod void CommunitynewCaseCntlrTestMethod() {

    Account testAcc = Test_Utils.insertAccount();
      
    Contact testCon = Test_Utils.createContact(testAcc.Id);
    insert testCon; 
      
    ID rectypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_REC_TYPE_EDQ_CASE_TECH_SUPPORT).getRecordTypeId();  
      
    CommunitynewCaseCntlr contrlObj = new CommunitynewCaseCntlr();
    
    system.assertEquals(0, contrlObj.accountAssetList.size());
    
    Case testCase = Test_Utils.insertCase(false, testAcc.Id);
    testCase.RecordTypeId = rectypeid; 
    testCase.ContactId = testCon.Id;
    testCase.Subject = 'Test Case';
    testCase.RecordTypeId = rectypeid;
    testCase.Description = 'Test data';   
    contrlObj.newCommunitySupportCase = testCase;
    
    contrlObj.confirmSave();
    system.assert(contrlObj.newCommunitySupportCase.Id != null);
  }

}