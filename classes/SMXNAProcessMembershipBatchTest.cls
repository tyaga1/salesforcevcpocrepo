/**=====================================================================
 * Experian
 * Name: SMXNAProcessMembershipBatchTest
 * Description: 
 * Created Date: 18/01/2016
 * Created By: Diego Olarte
 * 
 * Date Modified     Modified By        Description of the update
 * 26th Jan, 2016    Paul Kissick       Fixed test
 =====================================================================*/

@isTest
private class SMXNAProcessMembershipBatchTest {

  static testMethod void testBatch(){
    User testUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    testUser.Global_Business_Line__c = 'Corporate';
    testUser.Business_Line__c = 'Corporate';
    testUser.Business_Unit__c = 'APAC:SE';
    testUser.Region__c = 'North America';
    insert testUser;
    system.runAs(testUser) {
      
      system.assertEquals(0, [SELECT COUNT() FROM Feedback__c]);

      Test.startTest();

      SMXNAProcessMembershipBatch b = new SMXNAProcessMembershipBatch();
      Database.executeBatch(b);
      
      Test.stopTest();
      
      system.assertEquals(1, [SELECT COUNT() FROM Feedback__c WHERE DataCollectionId__c = :SMXNAProcessMembershipBatch.strSurveyId]);
    }
  }
  
  @testSetup
  private static void prepareTestData() {
    
    Account a = Test_Utils.insertAccount();
    
    Contact c = Test_Utils.insertContact(a.Id);
    
    Membership__c mb1 = new Membership__c(
      Physical_Fee__c = 'Payment Information Provided', 
      Service_Area__c = 'ABC', 
      Account__c = a.Id, 
      Contact_Name__c = c.Id,
      Membership_Onboarding_Welcome_sent__c = true,
      Membership_Welcome_Activity_Sent_date__c = Datetime.now().addHours(-36)
    );
    
    Membership__c mb2 = new Membership__c(
      Physical_Fee__c = 'Payment Information Provided', 
      Service_Area__c = 'ABC', 
      Account__c = a.Id,
      Contact_Name__c = c.Id,
      Membership_Onboarding_Welcome_sent__c = false
    );
               
    insert new List<Membership__c>{mb1,mb2};
     
  }
  
}