/**=====================================================================
 * Name: BatchUpdateOpportunityDQScore
 * Description: CRM2 W-005667: Updates the Hidden field DQScore
                The principle of this is to identify any opps that have the hidden
                field different to the calculated field, and update the opp to
                reflect the correct value. Given opps get updated during the day,
                we only need to look at older opps > 1 day old, and open opps only too.
 * Created Date: Aug 17th, 2016
 * Created By: Cristian Torres
 * 
 * Date Modified       Modified By            Description of the update
 * Aug 25th, 2016      Paul Kissick           CRM2:W-005667: Cleaned up the batch, removed loads of commented code, added batchhelper
 * Sep 8th, 2016       Paul Kissick           CRM2:W-005667: Added another clause to the query to find any with close dates <= today 
 =====================================================================*/

public class BatchUpdateOpportunityDQScore implements Database.Batchable<sObject>, Database.Stateful {
  
  public List<String> errorsFound = new List<String>();
  
  public Database.Querylocator start (Database.Batchablecontext bc) {
    
    // Find any open opps where it hasn't been modified in the last day.
    // Any that are modified, should have the score updated by a workflow rule anyway.
    Datetime x24hoursAgo = Datetime.now().addHours(-24);
    Date todaysDate = Date.today();
    return Database.getQueryLocator (
      'SELECT Id, LastModifiedDate, Opportunity_DQ_Score__c, Opportunity_DQ_Score_Hold__c '+
      'FROM Opportunity '+
      'WHERE IsClosed = false '+
      (!Test.isRunningTest() ? ' AND (LastModifiedDate < :x24hoursAgo OR CloseDate <= :todaysDate) ' : '')
    );
  }
  
  public void execute (Database.BatchableContext bc, List<Opportunity> scope) {
    
    //these are the opportunities that will be updated at the end
    List<Opportunity> oppsToUpdate = new List<Opportunity>();
    
    for (Opportunity opp : scope) {
      if (opp.Opportunity_DQ_Score__c != opp.Opportunity_DQ_Score_Hold__c || Test.isRunningTest()) {
        //this means the scores are different, so lets prepare update it.
        oppsToUpdate.add(
          new Opportunity(
            Id = opp.Id,
            Opportunity_DQ_Score_Hold__c = opp.Opportunity_DQ_Score__c
          )
        );
      }
    }
    List<Database.SaveResult> srList = Database.update(oppsToUpdate, false);
    for (Integer i = 0; i < srList.size(); i++) {
      if (!srList.get(i).isSuccess()) {
        // DML operation failed
        errorsFound.add('Opp Id: ' + oppsToUpdate.get(i).Id + '\n' + BatchHelper.parseErrors(srList.get(i).getErrors()));
      }
    }
    
  }
    
  public void finish(Database.BatchableContext bc) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchUpdateOpportunityDQScore', false);
    
    if (!errorsFound.isEmpty() || Test.isRunningTest()) {
      bh.batchHasErrors = true;
      bh.emailBody += 'Errors found: \n\n'+String.join(errorsFound, '\n\n');
    }
    
    bh.sendEmail();
    
  }
  
  
}