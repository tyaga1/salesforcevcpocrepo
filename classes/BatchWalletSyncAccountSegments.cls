/**=====================================================================
 * Appirio, Inc
 * Name: BatchWalletSyncAccountSegments
 * Description: Batch job for updating Account Segment from Wallet Sync
 * Created Date: Aug 20th, 2015
 * Created By: Noopur Sundriyal
 *
 * Date Modified                Modified By                  Description of the update
 * Sep 10th, 2015               Paul Kissick                 Added try/catch around email sending
 * Sep 22nd, 2015               Arpita Bose                  I-181201: Updated to set Processed__c to true
 * Sep 30th, 2015               Paul Kissick                 Changing search from CNPJ Number to Account ID
 * Oct 1st, 2015                Paul Kissick                 Changing lastStartDate to Wallet_Sync_Processing_Last_Run__c 
 * Nov 9th, 2015                Paul Kissick                 Case 01234035: Adding FailureNotificationUtility
 * Nov 27th, 2015               Paul Kissick                 Case 01266075: Replacing Global_Setting__c with new Batch_Class_Timestamp__c
 =======================================================================*/
global class BatchWalletSyncAccountSegments implements  Database.Batchable<sObject>, Database.Stateful {
    
  global Set<String> failedAccountSegmentRdcrdIDs;
  global map<Id,WalletSync__c> walletSyncRecords;
  global set<String> succeededWalletSyncIds;
    
    //========================================================================================
  // Start
  //========================================================================================
  global Database.QueryLocator start(Database.BatchableContext BC){

    succeededWalletSyncIds = new set<String>();
    Datetime lastStartDate = WalletSyncUtility.getWalletSyncProcessingLastRun();

    return Database.getQueryLocator([
      SELECT Id, Agency_Name__c,Business_Area__c, Account__c, //CNPJ_Number__c,
        Business_Group__c, Channel__c, Region__c,Sector__c,Segment1__c,
        Segment2__c, Segment3__c, Main_Activity__c, Client_Situation__c, Client_Since__c
      FROM WalletSync__c 
      // WHERE Last_Processed_Date__c >= :lastStartDate
      WHERE Processed__c = false 
      AND Account__c != null
    ]);
  }
  
  //========================================================================================
  // Execute
  //========================================================================================
  global void execute(Database.BatchableContext BC, List<sObject> scope) {
    system.debug('====scope===>>' +scope);
    // set<String> CNPJNumbers = new set<String>();
    Set<Id> accountIds = new Set<Id>();
    walletSyncRecords = new map<Id,WalletSync__c>();
    List<Account_Segment__c> accSegmentToUpdate = new List<Account_Segment__c>();
    failedAccountSegmentRdcrdIDs = new Set<String>();
    map<Id,Id> accsegWalletSyncIdMap = new map<Id,Id>();
    // gather the CNPJ Numbers
    for (WalletSync__c wSync : (List<WalletSync__c>) scope) {
        // CNPJNumbers.add(wSync.CNPJ_Number__c);
        accountIds.add(wSync.Account__c);
        walletSyncRecords.put(wSync.Account__c,wSync);
    }
    
    // Fetch the related Account and Account Segments 
    for ( Account acc : [SELECT Id , // CNPJ_Number__c,
                               (SELECT LATAM_Main_Activity__c, LATAM_Agency_Name__c, LATAM_Client_Since__c, 
                                       LATAM_Client_Situation__c, LATAM_Channel__c, LATAM_Segment1__c, 
                                       LATAM_Segment2__c, LATAM_Segment3__c, LATAM_Business_Group__c, LATAM_Region__c,
                                       LATAM_Sector__c, LATAM_Business_Area__c
                                FROM Account_Segments__r) 
                         FROM Account a 
                         WHERE Id IN :accountIds]) {
                            
      // if CNPJ Number is not there, then move to the next record
      //if ( acc.CNPJ_Number__c == null || acc.CNPJ_Number__c == '') {
      //  continue;
      //}
      
      // Map the fields of Account Segment with the WalletSync fields
      WalletSync__c wSync = walletSyncRecords.get(acc.Id);
      if( wSync == null ) {
        continue;
      }
        for ( Account_Segment__c accSeg : acc.Account_Segments__r ) {
            accSeg.LATAM_Agency_Name__c = wSync.Agency_Name__c;
            accSeg.LATAM_Business_Area__c = wSync.Business_Area__c;
            accSeg.LATAM_Business_Group__c = wSync.Business_Group__c;
            accSeg.LATAM_Channel__c = wSync.Channel__c;
            accSeg.LATAM_Region__c = wSync.Region__c;
            accSeg.LATAM_Sector__c = wSync.Sector__c;     // PK: 25/9/15 NOTE: This is replicated on the Account as well, by BatchWalletSyncAccountIndSect. Due to deprecate.
            accSeg.LATAM_Segment1__c = wSync.Segment1__c;
            accSeg.LATAM_Segment2__c = wSync.Segment2__c;
            accSeg.LATAM_Segment3__c = wSync.Segment3__c;
            accSeg.LATAM_Main_Activity__c = wSync.Main_Activity__c;
            accSeg.LATAM_Client_Situation__c = wSync.Client_Situation__c;
            accSeg.LATAM_Client_Since__c = wSync.Client_Since__c;
            accSegmentToUpdate.add(accSeg);
            accsegWalletSyncIdMap.put(accSeg.Id,wSync.Id);
        }
    }
    
    // update the Account Segments.
    if ( !accSegmentToUpdate.isEmpty() ) {
        List<Database.SaveResult> updatedAccountSegments = Database.update(accSegmentToUpdate, false);
        //update accSegmentToUpdate;
        
        for(Integer i = 0; i < updatedAccountSegments.size(); i++){
  
          if (!updatedAccountSegments.get(i).isSuccess()){
            // DML operation failed
            Database.Error error = updatedAccountSegments.get(i).getErrors().get(0);
            String failedDML = error.getMessage();
              String failedStr = 'Id:' + accSegmentToUpdate.get(i).Id + '\nName:' + accSegmentToUpdate.get(i).Name + '\nError' + failedDML;
              failedAccountSegmentRdcrdIDs.add(failedStr);
          }
          else {
            succeededWalletSyncIds.add(accsegWalletSyncIdMap.get(accSegmentToUpdate.get(i).Id));
          }
        }
    }
    
  }
  
  //========================================================================================
  // Finish
  //========================================================================================
  global void finish(Database.BatchableContext BC) {
    try {
	    List<WalletSync__c> walletSyncUpdate = new List<WalletSync__c>();
	    
	    // I-181201 
	    List<WalletSync__c> walletSyncToUpdate = new List<WalletSync__c>();
	    for (WalletSync__c wSync : [SELECT Id, Processed__c
	                                FROM WalletSync__c
	                                WHERE ID IN : succeededWalletSyncIds]) {
	      wSync.Processed__c = true;
	      walletSyncToUpdate.add(wSync);
	    }
	    if (!walletSyncToUpdate.isEmpty()) {
	      update walletSyncToUpdate;
	    }
	    
	    BatchHelper bh = new BatchHelper();
	    bh.checkBatch(BC.getJobId(), 'BatchWalletSyncAccountSegments', false);
	    
	    if (failedAccountSegmentRdcrdIDs != null && failedAccountSegmentRdcrdIDs.size() > 0) {
	      bh.batchHasErrors = true;
	      bh.emailBody += '\n\nAccount Segments failed on update:\n*********';
	      for (String currentRow : failedAccountSegmentRdcrdIDs) {
	        bh.emailBody += '\n'+currentRow+'\n*************\n';
	      }
	    }
	    
	    bh.sendEmail();
    }
    catch (Exception e) {
      system.debug(e.getMessage()); 
    }
    
    // THIS IS THE END OF THE BATCH SO CLOSE OFF
    WalletSyncUtility.processingCompleted();
  }
  

}