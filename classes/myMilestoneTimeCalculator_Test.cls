/**=====================================================================
  * Experian
  * Name: myMilestoneTimeCalculator_Test
  * Description: Test Class for myMilestoneTimeCalculator. (W-008058)
  *        
  * Created Date: April 19th 2017
  * Created By: Manoj Gopu 
  *
  * Date Modified      Modified By                  Description of the update
  =====================================================================*/
@isTest
private class myMilestoneTimeCalculator_Test {
  
  static testmethod void testCondition1(){
    Test.startTest();
     
      BusinessHours bh = [select id from businesshours where IsDefault=true];
      Custom_Fields_Ids__c objCust = new Custom_Fields_Ids__c();
      objCust.Serasa_business_hours_id__c = bh.Id;
      insert objCust;
        //create 1 account
      Account acc1 = Test_Utils.insertAccount();
      //create 3 addresses
      Address__c address1 = Test_Utils.insertAddress(false);
      address1.Address_id__c = 'testExternalId1';
      Address__c address2 = Test_Utils.insertAddress(false);
      address2.Address_id__c = 'testExternalId2';
      List<Address__c> listAddresses = new List<Address__c>();
      listAddresses.add(address1);
      listAddresses.add(address2);
      insert listAddresses;
      //create 1 account address
      Account_Address__c accAddress1 = Test_Utils.insertAccountAddress(true, address1.Id, acc1.Id);
      //create contact
      Contact contact1 = Test_Utils.insertContact(acc1.Id);
      // get the closed statuses of case object
      List<String> closedStatuses = new List<String>();
      for(CaseStatus cStatus : CaseTriggerHandler.getClosedCaseStatuses()){
        closedStatuses.add(cStatus.MasterLabel);
      }
      ESDEL_Delivery_Project__c deliveryProject = Test_Utils.insertDeliveryProject(true, 'project1', acc1.id, contact1.id, 'bord1');
      // insert new case of type spain delivery task     
         
      Case newCase = Test_Utils.insertCase (false, deliveryProject.id, acc1.id, 'bord1');
      newCase.Type = Constants.CASE_TYPE_SPAIN_DELIVERY_TASK;
      newCase.SLA__c = 10;
      newCase.Description = 'Test Descr';
      insert newCase;  
      
      
      myMilestoneTimeCalculator obj = new myMilestoneTimeCalculator();
      obj.calculateMilestoneTriggerTime(newCase.Id, 'MileStone');          
      
      Test.stopTest();
    }
    
    static testmethod void testCondition2(){
        Test.startTest();
        BusinessHours bh = [select id from businesshours where IsDefault=true];
        Custom_Fields_Ids__c objCust = new Custom_Fields_Ids__c();
      objCust.Serasa_business_hours_id__c = bh.Id;
      insert objCust;
        //create 1 account
      Account acc1 = Test_Utils.insertAccount();
      //create 3 addresses
      Address__c address1 = Test_Utils.insertAddress(false);
      address1.Address_id__c = 'testExternalId1';
      Address__c address2 = Test_Utils.insertAddress(false);
      address2.Address_id__c = 'testExternalId2';
      List<Address__c> listAddresses = new List<Address__c>();
      listAddresses.add(address1);
      listAddresses.add(address2);
      insert listAddresses;
      //create 1 account address
      Account_Address__c accAddress1 = Test_Utils.insertAccountAddress(true, address1.Id, acc1.Id);
      //create contact
      Contact contact1 = Test_Utils.insertContact(acc1.Id);
      // get the closed statuses of case object
      List<String> closedStatuses = new List<String>();
      for(CaseStatus cStatus : CaseTriggerHandler.getClosedCaseStatuses()){
        closedStatuses.add(cStatus.MasterLabel);
      }
      ESDEL_Delivery_Project__c deliveryProject = Test_Utils.insertDeliveryProject(true, 'project1', acc1.id, contact1.id, 'bord1');
      // insert new case of type spain delivery task     
         
      Case newCase = Test_Utils.insertCase (false, deliveryProject.id, acc1.id, 'bord1');
      newCase.Type = Constants.CASE_TYPE_SPAIN_DELIVERY_TASK;      
      newCase.Description = 'Test Descr';
      insert newCase;       
      
      myMilestoneTimeCalculator obj = new myMilestoneTimeCalculator();
      obj.calculateMilestoneTriggerTime(newCase.Id, 'MileStone');          
      
      Test.stopTest();
    }
}