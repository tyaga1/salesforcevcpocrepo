/**=====================================================================
 * Appirio, Inc
 * Name: MembershipHelper
 * Description: T-364083: Membership: Custom List Button on Account
 * Created Date: Feb 23th, 2015
 * Created By: Noopur (Appirio)
 *
 * Date Modified            Modified By           Description of the update
 * Feb 24th, 2015           Noopur                T-364091 : Modified the method
 *                                                createMembership to create the URL
 *                                                when button on Opportunity page is clicked.
 * Feb 25th, 2015           Naresh                T-365688: Membership: Submit button.
 * Mar 24th, 2015           Noopur                uncommented the saveURL portion in the redirectUrl,which was commented earlier.
 * May 5th, 2015            Nathalie              Recomment saveURL - per Angela, user should not be redirected
 * Jun 5th, 2015            Tyaga Pati            Added Function to check for presence of attachment or confidential record before record is submitted.
 * Jun 21st, 2016           Paul Kissick          Case 01222694 - Adding support for the onboarding tool on Membership records.
 =====================================================================*/
global class MembershipHelper {
  
  //==========================================================================
  // Webservice method to create Membership record, from opportunity
  //==========================================================================
  webservice static String createMembership (String recordTypeName, Id accId, String accName, Id oppId) {
    String oppName = '';
    String recordTypeId = DescribeUtility.getRecordTypeIdByName('Membership__c', recordTypeName);
    // String memberShipObjPrefix = Custom_Object_Prefixes__c.getInstance().Membership__c;
    Map<String, String> urlParams = new Map<String, String>();
    
    String accIndustry = null;
    
    String redirectUrl = '/'+DescribeUtility.getPrefix('Membership__c')+'/e';
    
    if (String.isNotBlank(recordTypeId)) {
      urlParams.put('RecordType', recordTypeId);
    }

    if (String.isNotBlank(oppId)) {
      system.debug('Setting oppId : '+oppId);
      urlParams.put('retURL', '/'+oppId);
    }
    else if (String.isNotBlank(accId)) {
      system.debug('Setting accId : '+accId);
      urlParams.put('retURL', '/'+accId);
    }

    if (oppId != null) {
      for (Opportunity opp: [SELECT Id, Name, AccountId, Account.Name
                             FROM Opportunity
                             WHERE Id = :oppId]) {
        if (opp.AccountId != null) {
          accId = opp.AccountId;
          accName = opp.Account.Name;
        }
        oppName = opp.Name;
      }
    }

    Contact con = new Contact();
    if (accId != null) {
      // Account acc = [SELECT Id, Industry FROM Account WHERE Id = :accId LIMIT 1];
      // accIndustry = acc.Industry;
      
      for (Contact conObj : [SELECT Id, Name
                             FROM Contact
                             WHERE AccountId = :accId
                             AND Contact_Role__c = 'Primary']) {
        con = conObj;
      }
    }
    
    if (con != null && con.Id != null) {
      urlParams.put('CF'+Custom_Fields_Ids__c.getInstance().Membership_Contact__c, con.Name);
      urlParams.put('CF'+Custom_Fields_Ids__c.getInstance().Membership_Contact__c+'_lkid', con.Id);
    }
    if (accId != null) {
      urlParams.put('CF'+Custom_Fields_Ids__c.getInstance().Membership_Account__c, accName);
      urlParams.put('CF'+Custom_Fields_Ids__c.getInstance().Membership_Account__c+'_lkid', accId);
      //if (accIndustry != null) {
      //  urlParams.put('00N56000000eOUT', accIndustry);
      //}
    }
    if (oppId != null) {
      urlParams.put('CF'+Custom_Fields_Ids__c.getInstance().Membership_Opportunity__c, oppName);
      urlParams.put('CF'+Custom_Fields_Ids__c.getInstance().Membership_Opportunity__c+'_lkid', oppId);
    }
    
    return buildSafeUrl(redirectUrl, urlParams);
  }
  
  public static String buildSafeUrl(String urlStart, Map<String, String> urlParams) {
    List<String> urlVals = new List<String>();
    for(String urlKey : urlParams.keySet()) {
      urlVals.add(urlKey + '=' + EncodingUtil.urlEncode(urlParams.get(urlKey),'UTF-8'));
    }
    return urlStart + '?'+String.join(urlVals,'&');
  }

  //==========================================================================
  // Webservice method to submit membership
  //==========================================================================
  webservice static String submitMembership (String membershipId) {
    
    Membership__c membership = null;
     
    for (Membership__c mShip : [SELECT Id, Submit__c, Bill_Code__c, Onboarding_Documents_Required__c, 
                                       Onboarding_Documents_Complete__c, Onboarding_Bypass__c
                                FROM Membership__c
                                WHERE Id = :membershipId]) {
      membership = mShip;
    }
    
    system.debug('********membership::::::'+membership);
    //If no membership record.
    if (membership == null) {
      return system.Label.MEMBERSHIP_ERR_NORECORD;
    } 
    else if (membership.Submit__c == true) {
      return system.Label.MEMBERSHIP_ERR_ALREADY_SUBMITTED;
    }
    else if (membership.Onboarding_Bypass__c == false && 
             membership.Onboarding_Documents_Required__c == true && 
             membership.Onboarding_Documents_Complete__c == false) {
      return system.label.MEMBERSHIP_ERR_ONBOARDING_REQUIRED;
    }
    try {
      membership.Submit__c = true;
      update membership;
    }
    catch (DMLException ex) {
      apexLogHandler.createLogAndSave('MembershipHelper', 'submitMembership', ex.getStackTraceString(), ex);
      String msg = '';
      for (Integer i = 0; i < ex.getNumDml(); i++) {
        msg += ex.getDmlMessage(i); 
      }
      return 'Error: ' + msg;
    }
    return system.Label.MEMBERSHIP_MSG_SUCCESS;
  }

  //==========================================================================
  // Method to check the Count of Attachment or Confidentiality informaiton 
  //   before submitting mem req.
  // PK : Improved to support chatter attachments too.
  //==========================================================================

  webservice static String CheckAttachment(String membershipId) {
    Boolean foundAttachment = false;
    
    List<Confidential_Information__c> membershipConfInfoList = [
      SELECT Id, (SELECT Id FROM CombinedAttachments), (SELECT Id FROM Attachments)
      FROM Confidential_Information__c
      WHERE Membership__c = :membershipId
    ];
    for (Confidential_Information__c confInfo : membershipConfInfoList) {
      if (confInfo.CombinedAttachments != null || (Test.isRunningTest() && confInfo.Attachments != null)) {
        if (!confInfo.CombinedAttachments.isEmpty() || (Test.isRunningTest() && !confInfo.Attachments.isEmpty())) {
          foundAttachment = true;
          break;
        }
      }
    }
    return (foundAttachment) ? 'success' : 'failure';
  }

}