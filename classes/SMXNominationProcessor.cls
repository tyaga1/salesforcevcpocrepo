/**=====================================================================
 * Experian
 * Name: SMXNominationProcessor
 * Description: class acts a WS client for Satmetrix WS and pushes nomination data into Satmetrix.
 * Created Date: Unknown
 * Created By: Unknown
 *
 * Date Modified     Modified By        Description of the update
 * Jul 22nd, 2015    Paul Kissick       Case #01040868 - Fixing error for Cases owned by Group/Queue & Formatting
 * Dec 08th, 2015    Diego Olarte       Case #01257554 - Added the EDQ NA Support Experience survey data
 * Jan 15th, 2016    Diego Olarte       Case #01259582 - Added the NA CS Client Support survey data and Onboarding Membership survey
 * Feb 9th, 2016     Paul Kissick       Case #01847726 - Adding extra xml for new fields, also refactored again.
 * Mar 1st, 2016     Paul Kissick       Case #01881161 - Fix for CaseID being ID instead of Number for edqNaSupportExperience
 * Mar 8th, 2016     Paul Kissick       Case #01893640 - Fix incorrect Hierarchy 2 node code value for CS surveys
 * Mar 14th, 2016    Paul Kissick       Case #01899325 - 2 new tags required for naOnboardingMembership
 * Jul 28th, 2016    Diego Olarte       Case #02071405 - Added the NA CS BIS Support survey data
 * Jan 10th, 2017    Diego Olarte       Case #02259314 - Added the mapping for survey 'EXPERIAN_106614' in the following fields region, follow-up owner 1, account manager, country
 * Jan 31st, 2017    Diego Olarte       Case #02138677 - Added new mapping for survey 'EXPERIAN_108224' by adding new field from Satmetrix Business unit
 * Apr 20th, 2017    Diego Olarte       Case #02362591 - Added new mapping for survey 'EXPERIAN_113966' by adding new field from Satmetrix Business unit
 * May 10th, 2017    James Wills        COMMENTED OUT Case #02207626 - Added clause to prevent error in batch when no Opp is associated with Survey
 * Mar 17th, 2017    Diego Olarte       Case #02399105 - Fixed for CaseID being Number for edqGPDSupport
 * Jul 12th, 2017    Diego Olarte       Case #13378726 - Fixed address mapping for 'EXPERIAN_106615'
 * Sep 22nd, 2017    Diego Olarte       Case #14010834 - Modified Region and Hierarchy 1 node code to change UK&I to UKI
 * Sep 22nd, 2017    Diego Olarte       Case #13905278 - Remove Case ID and sent only case number for Satmetrix CaseID
 * Sep 25th, 2017    Diego Olarte       Case #14010834 - Added new Survey 'EXPERIAN_114851'
 * Sep 26th, 2017    Diego Olarte       Case #13676563 - Added new mapping for Primary BL and Business Unit
 * Sep 27th, 2017    Diego Olarte       Case #13905288 - Set the Date event format to date only for EDQSupportExperience
 =====================================================================*/
global class SMXNominationProcessor {

  global static Integer testHttpStatusCode = 200;
  global static String testHttpResponseXML = '<webserviceresponse><status><code>0</code><message>Success</message></status>'
    +'<list><ContactInfo><contactRecordId></contactRecordId><personId></personId><surveyURL>https://nexeosolutions.staging.satmetrix.com</surveyURL>'
    +'</ContactInfo></list></webserviceresponse>';
  
  // Adding static variables to reference the survey tags
  public static String edqSupportExperience       = 'EXPERIAN_106615';
  public static String edqSupportExperienceEMEA   = 'EXPERIAN_114851';
  public static String edqNaSupportExperience     = 'EXPERIAN_109584';
  public static String naCsClientSupport          = 'EXPERIAN_108224';
  public static String naOnboardingMembership     = 'EXPERIAN_108225';
  public static String edqPurchaseExperience      = 'EXPERIAN_106613';
  public static String edqContractRenewal         = 'EXPERIAN_106614';
  public static String edqGPDSupport              = 'EXPERIAN_113966';

  /*
  Process the survey record.
  Reads contact mapping information from various mapped Salesforce entities and forms the xml and invokes Satmetrix WS to create a participant
  While creating participant syncs account info as well with Satmetrix
   strFbkId - Name of the survey record to be processed
  */
  global static void processNomination(String strFbkId) {
    // read survey record
    // Contact is master/detail link from Feedback to Contact
    Feedback__c fbk = [
      SELECT Contact__c, Name, DataCollectionId__c, Case__c, Status__c, StatusDescription__c, Opportunity__c, Order__c, 
        Contact__r.Name, Contact__r.AccountId, Contact__r.Department, Contact__r.Email, Contact__r.FirstName, Contact__r.LastName, 
        Contact__r.Title, Contact__r.HasOptedOutOfEmail, Contact__r.Phone, Contact__r.Salutation, Contact__r.Account.Region__c, Id,
        Contact__r.Account.Country_of_Registration__c, Case__r.Subject, Contact__r.Language__c,
        Case__r.ClosedDate, Case__r.RecordType.DeveloperName, Case__r.OwnerId, Case__r.Description, Case__r.Support_Team__c,Case__r.CaseNumber,
        Opportunity__r.Id, Opportunity__r.CloseDate, Opportunity__r.OwnerId, Opportunity__r.Description,
        Order__r.Id, Order__r.Renewal_Survey_Date__c, Order__r.OwnerId, Order__r.Status__c, Opportunity__r.Owner_s_Country__c,
        Order__r.Billing_Country__c, Order__r.Owner_Region_on_Order_Create_Date__c, 
        Membership__r.OwnerId, Membership__r.Name, Membership__r.Membership_Welcome_Activity_Sent_date__c, Membership__r.Contact_Name__c
      FROM Feedback__c
      WHERE Name = :strFbkId
    ];

    String strStatus = '';
    String strXPEnterpriseID = (Test.isRunningTest()) ? '' : SMXConfiguration__c.getValues('Configuration').ID_XP_ENTERPRISE__c;
    String strCreateParticipantURL = (Test.isRunningTest()) ? '' : SMXConfiguration__c.getValues('Configuration').URL_NOMINATE_CONTACTS__c;
    String strXPServer = (Test.isRunningTest()) ? '' : SMXConfiguration__c.getValues('Configuration').XP_SERVER__c;
    String strXPSecurityToken = (Test.isRunningTest()) ? '' : SMXConfiguration__c.getValues('Configuration').XP_SECURITYTOKEN__c;

    // read account and contact, check contact opt outs etc and sync account to XP
    Contact c = fbk.Contact__r;
    List<Account> lstAccount = [
      SELECT Id, Name, OwnerId, NumberOfEmployees, AnnualRevenue, Industry
      FROM Account
      WHERE Id = :c.AccountId
    ];
    
    List<String> lstValidationErrors = validateForScrubRules(fbk, c, lstAccount);

    // process survey record if contact is active and not opted out of email communication
    if (lstValidationErrors.isEmpty()) {
      Account a = lstAccount.get(0);

      // sync the account with Xperience by calling party WS
      SMXAccountProcessor.syncAccount(a.Id);

      // prepare participant xml
      String strParticipantXML = prepareParticipantXML(fbk,c,a,strXPEnterpriseID,strXPSecurityToken);

      if (strParticipantXML != '') {
        // WS invocation to create participant in Xperience
        HttpRequest req = createHttpRequest(strXPServer,strCreateParticipantURL);
        req.setBody(strParticipantXML);
        Http http = new Http();
        try {
          HTTPResponse httpResponse;
          if (!Test.isRunningTest()) {
            httpResponse = http.send(req);
          }
          else {
            httpResponse = new HttpResponse();
            httpResponse.setBody(testHttpResponseXML);
            httpResponse.setStatusCode(testHttpStatusCode);
          }
          if ((httpResponse.getStatusCode()) == 200) {
            String xmlString = httpResponse.getBody();
            system.debug('WS response: ' + xmlString);
            XMLDom doc= new XMLDom(xmlString);
            XMLDom.Element compDataXML = (XMLDom.Element)doc.getElementByTagName('webserviceresponse');
            if (compDataXML != null) {

              String strResultCode, strResultDescription, resultValue;

              XMLDom.Element code = (XMLDom.Element)compDataXML.getElementByTagName('code');
              if(code != null) {
                strResultCode = code.nodeValue;
              }
              XMLDom.Element contactRecordId = (XMLDom.Element)compDataXML.getElementByTagName('contactRecordId');
              XMLDom.Element description = (XMLDom.Element)compDataXML.getElementByTagName('surveyURL');
              if (description != null) {
                strResultDescription= description.nodeValue;
                Feedback__c feedback = new Feedback__c(Id = fbk.Id);
                /*
                Feedback__c feedback = [
                  SELECT Name, Status__c, StatusDescription__c
                  FROM Feedback__c
                  WHERE Id = :fbk.Id
                ];
                */
                feedback.Status__c = 'Success';
                if(contactRecordId.nodeValue !=null) {
                  feedback.Name = contactRecordId.nodeValue;
                }
                if(strResultDescription.contains(strXPServer)) {
                  feedback.StatusDescription__c = 'Participant Created';
                }
                else {
                  feedback.StatusDescription__c = strResultDescription;
                }
                update feedback;
                strStatus = 'Success';
              }
              else {
                if (strResultCode == '0') {
                  for(XMLdom.Element ee:compDataXML.getElementsByTagName('message')) {
                    resultValue = ee.nodeValue;
                  }
                  if (resultValue == 'No Send Rule is applied for the provider') {
                    Feedback__c feedback = new Feedback__c(Id = fbk.Id);
                    //[select Name, Status__c,StatusDescription__c from Feedback__c where Name = :fbk.Name];
                    feedback.Status__c = 'Skipped';
                    if(contactRecordId.nodeValue !=null) {
                      feedback.Name = contactRecordId.nodeValue;
                    }
                    feedback.StatusDescription__c = 'No Send Rule Applied';
                    update feedback;
                    strStatus = 'Failure';
                  }
                  else {
                    if (!Test.isRunningTest()) {
                      resultValue=resultValue.replace('[','');
                      resultValue=resultValue.replace(']','');
                    }
                    Feedback__c feedback = [SELECT Name, Status__c, StatusDescription__c FROM Feedback__c WHERE Id = :fbk.Id];
                    feedback.Status__c = 'Skipped';
                    feedback.StatusDescription__c = resultValue;
                    update feedback;
                    strStatus = 'Failure';
                  }
                  /*
                  if (resultValue != 'No Send Rule is applied for the provider') {
                    Feedback__c feedback = [select Name, Status__c,StatusDescription__c from Feedback__c where Name = :fbk.Name];
                    feedback.Status__c = 'Success';
                    feedback.StatusDescription__c = 'Participant Created';
                    XMLDom.Element elemRow = (XMLDom.Element)doc.getElementByTagName('row');
                    String strURL = elemRow.getAttribute('value');
                    feedback.SurveyLink__c = strURL;
                    update feedback;
                    strStatus = 'Success';
                  }
                  */
                }
                if (strResultCode <> '0') {
                  description = (XMLDom.Element)compDataXML.getElementByTagName('description');
                  strResultDescription= description.nodeValue;
                  Feedback__c feedback = [
                    SELECT Name, Status__c, StatusDescription__c
                    FROM Feedback__c
                    WHERE Id = :fbk.Id
                  ];
                  feedback.Status__c = 'Failure';
                  feedback.StatusDescription__c = strResultDescription;
                  update feedback;
                  strStatus = 'Failure';
                }
              }
            }
          }
          if ((httpResponse.getStatusCode()) <> 200) {
            // WS invocation failure
            String strMessage = httpResponse.getStatus();
            Feedback__c feedback = new Feedback__c(Id = fbk.Id);
            // [select Name, Status__c, StatusDescription__c from Feedback__c where Id = :fbk.Id];
            feedback.Status__c = 'Failure';
            feedback.StatusDescription__c = strMessage;
            update feedback;
            strStatus = 'Failure';
          }
        }
        catch(System.CalloutException e) {
          // callout exception
          String strMessage = e.getMessage();
          Feedback__c feedback = new Feedback__c(Id = fbk.Id);
          // [select Name, Status__c, StatusDescription__c from Feedback__c where Name = :fbk.Id];
          feedback.Status__c = 'Failure';
          feedback.StatusDescription__c = strMessage;
          update feedback;
          strStatus = 'Failure';
        }
      }
      else {
        fbk.Status__c = 'Failure';
        //fbk.StatusDescription__c = 'Survey cannot be sent. Unrecognised Survey (not support / relation)';
        fbk.StatusDescription__c = 'There is a mapping error occured. Please check the contact mapping.';
        update fbk;
      }
    }
    else {
      fbk.Status__c = 'Failure';
      if (!lstValidationErrors.isEmpty()) {
        fbk.StatusDescription__c = String.join(lstValidationErrors,'');
      }
      update fbk;
    }
  }

  // utility method to create a http request
  global static HttpRequest createHttpRequest(String strXPServer, String strURL) {
    HttpRequest req = new HttpRequest();
    req.setTimeout(120000);
    req.setMethod('POST');

    req.setHeader('Host',strXPServer);
    req.setHeader('Connection','keep-alive');
    req.setHeader('Content-Type','text/xml');
    req.setHeader('DataEncoding','UTF-8');

    req.setEndpoint(strURL);
    return req;
  }

  /*
  creates participant xml by reading mapped objects/ attributes
  */
  global static String prepareParticipantXML(Feedback__c fbk, Contact c, Account a, String strXPEnterpriseID, String strXPSecurityToken) {
    
    Map<String,String> xmlKeyValues = new Map<String,String>();
    
    String strLocaleCode = 'en_US';
    Datetime currentDate = system.now();
    String strCurrentDate = currentDate.format('yyyy-MM-dd') + 'T' + currentDate.format('HH:mm:ss') + '.00';
    String strPurchaseLevel = '';
    String strXML = '';
    String testMode = 'N';
    if (!Test.isRunningTest() && SMXConfiguration__c.getValues('Configuration').TEST_MODE__c) {
      testMode = 'Y';
    }
    strXML = '<?xml version="1.0" encoding="utf-8"?>' +
      '<utilService><enterpriseId>' + strXPEnterpriseID + '</enterpriseId>'+
      '<securityToken>' + strXPSecurityToken + '</securityToken>' +
      '<surveyId>'+ fbk.DataCollectionId__c + '</surveyId>'+
      '<sendMail>Y</sendMail>'+
      '<isTestUpload>'+testMode+'</isTestUpload>'+
      '<contactMap-list>'+
        '<contactMap>' +
          '<contactMapEntry-list>'+
            '<contactMapEntry key ="Person Identifier" value = "'+fbk.Contact__c+'"/>'+
            '<contactMapEntry key ="First name" value = "'+escapeXML(c.FirstName)+'"/>'+
            '<contactMapEntry key ="Person last name" value = "'+escapeXML(c.LastName)+'"/>'+
            '<contactMapEntry key ="Email"  value = "'+c.Email+'"/>'+
            '<contactMapEntry key ="Phone"  value = "'+escapeXML(c.Phone)+'"/>'+
            '<contactMapEntry key ="Job Title"  value = "'+escapeXML(c.Title)+'"/>'+
            '<contactMapEntry key ="Person Salutation"  value = "'+escapeXML(c.Salutation)+'"/>'+
            '<contactMapEntry key ="Contact ID"  value = "'+fbk.Contact__c+'"/>'+
            '<contactMapEntry key ="Product"  value = ""/>'+
             //'<contactMapEntry key ="Project Manager"  value = ""/>'+
            '<contactMapEntry key ="Reference Number"  value = ""/>';
    String evtDesc;
    Datetime evtDatetime; // Will convert to ISO format later...
    User evtOwner;
    Id evtOwnerId;
    
    if (fbk.DataCollectionId__c == edqGPDSupport) {// EDQ GPD Support(EXPERIAN_113966) {
        strXML += '<contactMapEntry key ="Company ID"  value = "ExperianEDQ"/>'+
                  '<contactMapEntry key ="Company name"  value = "Experian EDQ GPD"/>';
    }
    else {
        strXML += '<contactMapEntry key ="Company ID"  value = "'+a.Id+'"/>'+
                  '<contactMapEntry key ="Company name"  value = "'+escapeXML(a.Name)+'"/>';
    }
    
    if (fbk.DataCollectionId__c == edqSupportExperienceEMEA && fbk.Contact__r.Language__c == 'French') {
        strXML += '<contactMapEntry key ="Language"  value = "fr_FR"/>';
    }
    else if(fbk.DataCollectionId__c == edqSupportExperienceEMEA && fbk.Contact__r.Language__c == 'German'){
        strXML += '<contactMapEntry key ="Language"  value = "de_DE"/>';
    }
    else if(fbk.DataCollectionId__c == edqSupportExperienceEMEA && fbk.Contact__r.Language__c == 'Dutch'){
        strXML += '<contactMapEntry key ="Language"  value = "nl_NL"/>';
    }
    else if(fbk.DataCollectionId__c == edqSupportExperienceEMEA && fbk.Contact__r.Language__c == 'Spanish'){
        strXML += '<contactMapEntry key ="Language"  value = "es_ES"/>';
    }
    else {
        strXML += '<contactMapEntry key ="Language"  value = "en_US"/>';
    }
    
    if (fbk.DataCollectionId__c == edqSupportExperience || // EDQ Support Experience
        fbk.DataCollectionId__c == edqSupportExperienceEMEA || // EDQ Support Experience (EMEA)
        fbk.DataCollectionId__c == edqNaSupportExperience || // EDQ NA Support Experience (EXPERIAN_109584),
        fbk.DataCollectionId__c == naCsClientSupport || // NA CS Client Support(EXPERIAN_108224)
        fbk.DataCollectionId__c == edqGPDSupport) {// EDQ GPD Support(EXPERIAN_113966)
      Case cs = fbk.Case__r;
      // PK: Case 01881161 - Fixed edqNaSupportExperience caseID field
      //if (cs.OwnerId != null && cs.OwnerId.getSobjectType() == User.sObjectType &&  fbk.DataCollectionId__c == edqSupportExperience){
      if (cs.OwnerId != null && (cs.OwnerId.getSobjectType() == User.sObjectType)){
        evtOwnerId = cs.OwnerId;
        /*strXML += '<contactMapEntry key ="CaseID"  value = "'+escapeXML(cs.id)+'"/>';
      }
      
      //Case #02399105
      else if (cs.OwnerId != null && (cs.OwnerId.getSobjectType() == User.sObjectType) && fbk.DataCollectionId__c == edqGPDSupport){
        evtOwnerId = cs.OwnerId;
        strXML += '<contactMapEntry key ="CaseID"  value = "'+escapeXML(cs.CaseNumber)+'"/>';
      }//Case #02399105
      
      else if (cs.OwnerId != null && (cs.OwnerId.getSobjectType() == User.sObjectType)){
        evtOwnerId = cs.OwnerId; DO: Case 13905278: Comment out to use Case number as Case ID for Satmetrix */
        strXML += '<contactMapEntry key ="CaseID"  value = "'+escapeXML(cs.CaseNumber)+'"/>';
      }
      else {
        evtOwnerId = null;
        strXML += '<contactMapEntry key ="CaseID"  value = ""/>';
      }
      
      evtDesc = cs.Description;
      if (String.isNotBlank(evtDesc)) {
        if (evtDesc.length() > 1650) {
          evtDesc = evtDesc.subString(0,1650) +'\n <br> ---Case Description truncated to limit the size---</br>\n ';
        }
      }
      if (cs.ClosedDate != null) {
        evtDatetime = (Datetime)cs.ClosedDate;
      }
      if (fbk.DataCollectionId__c == naCsClientSupport) {
        String teamName = '';
        if (String.isNotBlank(cs.Support_Team__c)) {
          if (cs.Support_Team__c == 'Client Billing Questions' || cs.Support_Team__c == 'Client Support Online') {
            teamName = 'Client Support';
          }
          if (cs.Support_Team__c == 'Partnership Support' || cs.Support_Team__c == 'Reseller Support') {
            teamName = 'Indirect Support';
          }
          if (cs.Support_Team__c == 'Public Sector Support' || cs.Support_Team__c == 'Sales Support') {
            teamName = 'Sales Support';
          }
        }
        strXML += '<contactMapEntry key ="Team"  value = "'+teamName+'"/>';
        // Adding extra keys for firstname, lastname as they are different for this survey - DO NOT REMOVE
        strXML += '<contactMapEntry key ="firstname" value = "'+escapeXML(c.FirstName)+'"/>'+
                  '<contactMapEntry key ="lastname" value = "'+escapeXML(c.LastName)+'"/>';
      }
      
    }
           
    else if (fbk.DataCollectionId__c == edqPurchaseExperience) { // EDQ Purchase Experience
      if(fbk.Opportunity__c != null){//Case 02207626
        Opportunity opp = fbk.Opportunity__r;
        if (opp.OwnerId != null && opp.OwnerId.getSobjectType() == User.sObjectType) {
          evtOwnerId = opp.OwnerId;
          strXML += '<contactMapEntry key ="CaseID"  value = "'+escapeXML(opp.id)+'"/>';
        }
        if (opp.CloseDate != null) {
          evtDatetime = (Datetime)opp.CloseDate;
        }
        evtDesc = 'Closed Won';
      //} else {
      //  strXML += '<contactMapEntry key ="CaseID"  value = />';
      }//Case 02207626
    }
    
    else if (fbk.DataCollectionId__c == edqContractRenewal) { // EDQ Contract Renewal 
      Order__c ord = fbk.Order__r;
      if (ord.OwnerId != null && ord.OwnerId.getSobjectType() == User.sObjectType) {
        evtOwnerId = ord.OwnerId;
        strXML += '<contactMapEntry key ="CaseID"  value = "'+escapeXML(ord.id)+'"/>';
      }
      if (ord.Renewal_Survey_Date__c != null) {
        evtDatetime = (Datetime)ord.Renewal_Survey_Date__c;
      }
      evtDesc = ord.Status__c;
    }
    
    else if (fbk.DataCollectionId__c == naOnboardingMembership) { // NA Onboarding Membership
      Membership__c mb = fbk.Membership__r;
      if (mb.OwnerId != null && mb.OwnerId.getSobjectType() == User.sObjectType) {
        evtOwnerId = mb.OwnerId;
        strXML += '<contactMapEntry key ="CaseID"  value = "'+escapeXML(mb.id)+'"/>';
        strXML += '<contactMapEntry key ="Membership number"  value = "'+escapeXML(mb.Name)+'"/>';
      }
      if (mb.Membership_Welcome_Activity_Sent_date__c != null) {
        evtDatetime = (Datetime)mb.Membership_Welcome_Activity_Sent_date__c;
      }
      evtDesc = 'Membership letter sent';
      strXML += '<contactMapEntry key ="Team"  value = "Membership"/>';
      // Adding extra keys for firstname, lastname as they are different for this survey - DO NOT REMOVE
      strXML += '<contactMapEntry key ="firstname" value = "'+escapeXML(c.FirstName)+'"/>'+
                '<contactMapEntry key ="lastname" value = "'+escapeXML(c.LastName)+'"/>';
    }
    //Added by DO new field mapping
    if (fbk.DataCollectionId__c == edqContractRenewal || 
        fbk.DataCollectionId__c == edqSupportExperience || 
        fbk.DataCollectionId__c == edqPurchaseExperience ) {
        
        strXML += '<contactMapEntry key ="Primary BL" value = "MS"/>'+
                '<contactMapEntry key ="Business Unit" value = "Data Quality"/>';
    }
    if (evtOwnerId != null) {
      evtOwner = [
        SELECT Id, Name, Email, Country__c, Region__c, 
               ManagerId, Manager.Name, Manager.Email, 
               Manager.ManagerId, Manager.Manager.Name, Manager.Manager.Email 
        FROM User
        WHERE Id = :evtOwnerId
      ];
    }
    if (evtDatetime != null){
        if(fbk.DataCollectionId__c == edqSupportExperience) {//DO: Remove time for EDQSupportExperience Survey
          strXML += '<contactMapEntry key ="Initiating event description"  value = "'+escapeXML(evtDesc)+'"/>'+
                    '<contactMapEntry key ="Initiating event date"  value = "'+evtDatetime.format('yyyy-MM-dd')+'"/>';                      
        }
        else {
          strXML += '<contactMapEntry key ="Initiating event description"  value = "'+escapeXML(evtDesc)+'"/>'+
                    '<contactMapEntry key ="Initiating event date"  value = "'+evtDatetime.format('yyyy-MM-dd hh:mm:ss')+'"/>';                    
        }
    }
    if (evtDatetime != null && fbk.DataCollectionId__c == naCsClientSupport && fbk.Case__r.RecordType.DeveloperName == 'CSDA_CIS_Support') {
      strXML += '<contactMapEntry key ="Hierarchy 2 node code"  value = "CIS"/>'+
                '<contactMapEntry key ="Business unit"  value = "CIS"/>';//Added by DO Case #02138677
    }
    else if (evtDatetime != null && fbk.DataCollectionId__c == naCsClientSupport && fbk.Case__r.RecordType.DeveloperName == 'CSDA_BIS_Support') {
      strXML += '<contactMapEntry key ="Hierarchy 2 node code"  value = "BIS"/>'+
                '<contactMapEntry key ="Business unit"  value = "BIS"/>';//Added by DO Case #02138677
    }
    else if(fbk.DataCollectionId__c == naOnboardingMembership) {
      strXML += '<contactMapEntry key ="Hierarchy 2 node code"  value = "CIS"/>';
    }
    else {
      strXML += '<contactMapEntry key ="Hierarchy 2 node code"  value = "EDQ"/>';
    }
        
    if (evtOwnerId != null) {
      Order__c ord = fbk.Order__r;
      
      //Case #02399105
      Contact con = fbk.Contact__r;
      
      List<Contact_Address__c> conAddress = [
        SELECT Id, Contact__c, Address_Country__c
        FROM Contact_Address__c
        WHERE Id = : con.Id ORDER BY CreatedDate DESC LIMIT 1
      ];
      
      String conAdd = '';
      
      if ( !conAddress.isEmpty() ){
          Contact_Address__c acct = [SELECT Id FROM Contact_Address__c];
          conAdd = [SELECT Address_Country__c FROM Contact_Address__c].Address_Country__c;
      }//Case #02399105
      
          strXML += '<contactMapEntry key ="Follow-up Owner 1"  value = "'+evtOwner.Email+'"/>';
          strXML += '<contactMapEntry key ="Follow-up Owner 1 name"  value = "'+evtOwner.Name+'"/>';
          
      if (fbk.DataCollectionId__c == naOnboardingMembership || fbk.DataCollectionId__c == naCsClientSupport) {
        // strXML += '<contactMapEntry key ="Project Manager"  value = "'+evtOwner.Name+'"/>';
        strXML += '<contactMapEntry key ="Follow-up Owner 2"  value = "'+((evtOwner.ManagerId != null) ? evtOwner.Manager.Email : '')+'"/>';
        strXML += '<contactMapEntry key ="Follow-up Owner 2 name"  value = "'+((evtOwner.ManagerId != null) ? escapeXML(evtOwner.Manager.Name) : '')+'"/>';
        strXML += '<contactMapEntry key ="Follow-up Escalation Owner 1"  value = "'+((evtOwner.Manager.ManagerId != null) ? evtOwner.Manager.Manager.Email : '')+'"/>';
        strXML += '<contactMapEntry key ="Follow-up Escalation Owner 1 name"  value = "'+((evtOwner.Manager.ManagerId != null) ? escapeXML(evtOwner.Manager.Manager.Name) : '')+'"/>';
      }
      if (evtOwnerId != null && fbk.DataCollectionId__c == naOnboardingMembership) {
        strXML += '<contactMapEntry key ="Account Manager"  value = "'+escapeXML(evtOwner.Name)+'"/>';
        // Case 01899325
        strXML += '<contactMapEntry key ="Membership analyst"  value = "'+escapeXML(evtOwner.Name)+'"/>';
        strXML += '<contactMapEntry key ="Team manager"  value = "'+((evtOwner.ManagerId != null) ? escapeXML(evtOwner.Manager.Name) : '')+'"/>';
        
      }
      else {
        strXML += '<contactMapEntry key ="Account Manager"  value = "'+escapeXML(evtOwner.Name)+'"/>';
      }    
                  
      if (fbk.DataCollectionId__c == edqContractRenewal) {
        strXML += '<contactMapEntry key ="Country"  value = "'+escapeXML(ord.Billing_Country__c)+'"/>';
      }
      
      //Case #02399105
      else if (con.Account.Country_of_Registration__c != '' && (fbk.DataCollectionId__c == edqSupportExperience || fbk.DataCollectionId__c == edqSupportExperienceEMEA)) {
        strXML += '<contactMapEntry key ="Country"  value = "'+escapeXML(con.Account.Country_of_Registration__c)+'"/>';
      }
      //Case #13378726: Added by DO
      else if (fbk.DataCollectionId__c == edqSupportExperience || fbk.DataCollectionId__c == edqSupportExperienceEMEA) {
        strXML += '<contactMapEntry key ="Country"  value = "-"/>';
      }
      
      else if (evtOwner.Country__c == 'United Kingdom') {
        strXML += '<contactMapEntry key ="Country"  value = "UK"/>';
      }
      else if (evtOwner.Country__c == 'United States of America'){
        strXML += '<contactMapEntry key ="Country"  value = "USA"/>';
      }
      else {
        strXML += '<contactMapEntry key ="Country"  value = "'+escapeXML(evtOwner.Country__c)+'"/>';
      }
          
      if (fbk.DataCollectionId__c == edqContractRenewal) {
        strXML += '<contactMapEntry key ="Region"  value = "'+escapeXML(ord.Owner_Region_on_Order_Create_Date__c)+'"/>';
      }
      
      else if (fbk.DataCollectionId__c == edqSupportExperienceEMEA){
        strXML += '<contactMapEntry key ="Region"  value = "EMEA"/>'+
                  '<contactMapEntry key ="Hierarchy 1 node code"  value = "EMEA"/>'+
                  '<contactMapEntry key ="Communication from name"  value = "Experian"/>';
      }
      
      //Case #02399105
      else if (fbk.DataCollectionId__c == edqSupportExperience){
        if (con.Account.Region__c == 'UK&I'){ //DO: Added if to change format of UK&I to UKI
            strXML += '<contactMapEntry key ="Region"  value = "UKI"/>'+
                  '<contactMapEntry key ="Hierarchy 1 node code"  value = "UKI"/>';
        }
        else {
            strXML += '<contactMapEntry key ="Region"  value = "'+escapeXML(con.Account.Region__c)+'"/>'+
                  '<contactMapEntry key ="Hierarchy 1 node code"  value = "'+escapeXML(con.Account.Region__c)+'"/>';
        }       
        strXML += '<contactMapEntry key ="Region"  value = "'+escapeXML(con.Account.Region__c)+'"/>'+
                  '<contactMapEntry key ="Hierarchy 1 node code"  value = "'+escapeXML(con.Account.Region__c)+'"/>';
        if (con.Account.Region__c == 'APAC'){
            strXML += '<contactMapEntry key ="Communication from name"  value = "Tristan Taylor"/>';
        }
        else {
            strXML += '<contactMapEntry key ="Communication from name"  value = ""/>';
        }
      }//Case #02399105
      
      else if (evtOwner.Region__c == 'UK&I') {
        strXML += '<contactMapEntry key ="Region"  value = "UKI"/>'+
                  '<contactMapEntry key ="Hierarchy 1 node code"  value = "UKI"/>';
      }
      else if (evtOwner.Region__c == 'North America'||fbk.DataCollectionId__c == naCsClientSupport){
        strXML += '<contactMapEntry key ="Region"  value = "NA"/>'+
                  '<contactMapEntry key ="Hierarchy 1 node code"  value = "NA"/>';
      }
      else {
        strXML += '<contactMapEntry key ="Region"  value = "'+escapeXML(evtOwner.Region__c)+'"/>'+
                  '<contactMapEntry key ="Hierarchy 1 node code"  value = "'+escapeXML(evtOwner.Region__c)+'"/>';
      }              
    }
    strXML += '</contactMapEntry-list>'+
            '</contactMap>'+
          '</contactMap-list>'+
        '</utilService>';
    system.debug('>> Result >>'+strXML);
    return strXML;
  }

  global static String escapeXML(String strXML){
    return (strXML == null) ? '' : strXML.escapeXML();
  }

  global static List<String> validateForScrubRules(Feedback__c fbk, Contact c, List<Account> lstAccount) {
    List<String> lstErrors = new List<String> ();
    if (lstAccount.isEmpty()) {
      lstErrors.add('The contact is not associated with any account\n');
    }
    if (c.HasOptedOutOfEmail) {
      lstErrors.add('The contact has opted out of emails\n');
    }
    if (String.isBlank(c.Email)) {
      lstErrors.add('There is no email address for the contact\n');
    }
    if (String.isBlank(c.FirstName)) {
      lstErrors.add('Missing first name for the contact\n');
    }
    if(String.isBlank(c.LastName)){
      lstErrors.add('Missing last name for the contact\n');
    }
    return lstErrors;
  }

}