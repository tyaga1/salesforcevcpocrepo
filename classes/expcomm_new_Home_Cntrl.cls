/******************************************************************************
 * Name: expcomm_new_Home_Cntrl 
 * Created Date: Jan 31, 2017
 * Created By: Hay Win
 * Description : Controller for new employee community for User Name and Photo URL
 * Change Log- 
 ******************/
public with sharing class expcomm_new_Home_Cntrl {

    public List<User> curUser {get;set;}
    
   
    public expcomm_new_Home_Cntrl () {
        curUser = new List<User>();
        string userID = UserInfo.getUserId();
        curUser = [Select ID, fullphotourl,smallphotourl, FirstName, LastName, LastLoginDate from user where id =: userID ];
        
    }

}