/*=============================================================================
 * Experian
 * Name: DeploymentRequestManualStepsExt_Test
 * Description: Case 01946043 - Tests for DeploymentRequestManualStepsExt
 * Created Date: 12 Apr 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/

@isTest
private class DeploymentRequestManualStepsExt_Test {

  static testMethod void myUnitTest1() {
    
    Deployment_Request__c dr = [SELECT Id FROM Deployment_Request__c WHERE Release__c = 'Support Release 1'];
    
    ApexPages.StandardController stdCon = new ApexPages.StandardController(dr);
    DeploymentRequestManualStepsExt drmse = new DeploymentRequestManualStepsExt(stdCon);
    
    system.assertEquals(2, drmse.getDeploymentComponentsManual().size(), 'There should be 2 components.');
    
  }
  
  @testSetup
  private static void setupData() {

    Account a1 = Test_Utils.insertAccount();
    Case c1 = Test_Utils.insertCase(true,a1.Id);
    
    Metadata_Component__c md1 = new Metadata_Component__c(
      Component_API_Name__c = 'Test_Component_Field1__c',
      Component_Type__c = 'CustomField',
      Object_API_Name__c = 'CustomObject__c',
      Name = 'Test Component'
    );
    Metadata_Component__c md2 = new Metadata_Component__c(
      Component_API_Name__c = 'Test_Component_Field2__c',
      Component_Type__c = 'CustomField',
      Object_API_Name__c = 'CustomObject__c',
      Name = 'Test Component'
    );
    insert new Metadata_Component__c[]{md1,md2};
    
    Case_Component__c cc1 = new Case_Component__c(
      Action__c = 'Create',
      Case_Number__c = c1.Id,
      Component_Name__c = md1.Id,
      Deployment_Type__c = 'Manual Step',
      Manual_Step_Details__c = 'Do stuff', 
      Configuration_Type__c = 'Pre - Configuration'
    );
    Case_Component__c cc2 = new Case_Component__c(
      Action__c = 'Create',
      Case_Number__c = c1.Id,
      Component_Name__c = md2.Id,
      Deployment_Type__c = 'Manual Step',
      Manual_Step_Details__c = 'Do other stuff',
       Configuration_Type__c = 'Post - Configuration'
    );

    insert new Case_Component__c[]{cc1,cc2};
    
    Salesforce_Environment__c src = new Salesforce_Environment__c(Name = 'Source');
    Salesforce_Environment__c trg = new Salesforce_Environment__c(Name = 'Target');
    insert new Salesforce_Environment__c[]{src,trg};
    
    Deployment_Request__c dr1 = new Deployment_Request__c(
      Deployment_Date__c = Date.today().addDays(1),
      Deployment_Lead__c = UserInfo.getUserId(),
      Release__c = 'Support Release 1',
      Status__c = 'Not Started',
      Source__c = src.Id,
      Target__c = trg.Id
    );
    insert dr1;

    
    Deployment_Component__c dco1 = new Deployment_Component__c(
      Case_Component__c = cc1.Id,
      Slot__c = dr1.Id
    );
    Deployment_Component__c dco2 = new Deployment_Component__c(
      Case_Component__c = cc2.Id,
      Slot__c = dr1.Id
    );

    insert new Deployment_Component__c[]{dco1,dco2};
    
    Deployment_Cases__c dca1 = new Deployment_Cases__c(
      Case__c = c1.Id,
      Deployment_Request_Slot__c = dr1.Id
    );
    
    insert dca1;
    

  }
  
}