/*=============================================================================
 * Experian
 * Name: BatchContactStatusUpdate
 * Description: CRM2:005405
                Updates the Contact Status Picklist based on lack of activity on
                the contact.
    * No Activities in Last 365 Days (1 year)
    * No Open Opps (Contact Roles)
    * No Active Campaigns (Campaign Members)
    * At least 1 year old
 * NOTE: This class schedules itself multiple times, if other processes have been
   set to run after being scheduled.
   
 * Created Date: 16 Aug 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 30 Aug, 2016       Paul Kissick          CRM2:005405: Replaced strings for processes with ENUM - Much better!
 * 15 Sep, 2016       Paul Kissick          CRM2:005405: Concerns over Case Contacts being made inactive, so adding a check for that reference
 * 16 Sep, 2016       Paul Kissick          CRM2:005405: Added extra checks for making a contact active for case and memberships.
 * 18 Jul, 2017       James Wills           Case 02265655: Updated  getInactivesRecentCampaigns() method.
 =============================================================================*/

public class BatchContactStatusUpdate implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
  
  // All available processes to run
  public Enum RunProcess {
    Actives,
    InactivesRecentTasks,
    InactivesRecentOpenOpps,
    InactivesRecentCampaigns,
    InactivesRecentCases,
    InactivesRecentMemberships
  }

  private Datetime startTime;

  public List<String> errorsFound = new List<String>();

  public Boolean makeInactive = true; // Used to identify if we are setting contacts inactive or not (active!)
  
  // Gets populated in the execute method for the schedulablecontext
  public List<RunProcess> processesToDoList = new List<RunProcess>();
  
  public BatchContactStatusUpdate () {
  }
  
  // Init the class with a list of all process to run.
  public BatchContactStatusUpdate (List<RunProcess> p) {
    processesToDoList = p;
  }
  
  // Added the schedulable context to this class to keep things cleaner.
  public void execute(SchedulableContext sc) {
    Database.executeBatch(
      new BatchContactStatusUpdate(
        new List<RunProcess>{
          RunProcess.Actives,
          RunProcess.InactivesRecentTasks,
          RunProcess.InactivesRecentOpenOpps,
          RunProcess.InactivesRecentCampaigns,
          RunProcess.InactivesRecentCases,
          RunProcess.InactivesRecentMemberships
        }
      ), 
      ScopeSizeUtility.getScopeSizeForClass('BatchContactStatusUpdate')
    );
  }
  
  // Standard start method, initialises the class.
  public Database.Querylocator start (Database.Batchablecontext bc) {
    startTime = Datetime.now();
    errorsFound = new List<String>();
    RunProcess currentProcess;
    // If this is empty, then just run the 'Active' search (at the end)
    if (!processesToDoList.isEmpty()) {
      // Extract the process to perform at the moment - removes from the list, list gets used later for the remaining processes
      currentProcess = processesToDoList.remove(0);
      if (currentProcess == RunProcess.Actives) {
        return getActives();
      }
      if (currentProcess == RunProcess.InactivesRecentTasks) {
        return getInactivesRecentTasks();
      }
      if (currentProcess == RunProcess.InactivesRecentOpenOpps) {
        return getInactivesRecentOpenOpps();
      }
      if (currentProcess == RunProcess.InactivesRecentCampaigns) {
        return getInactivesRecentCampaigns();
      }
      if (currentProcess == RunProcess.InactivesRecentCases) {
        return getInactivesRecentCases();
      }
      if (currentProcess == RunProcess.InactivesRecentMemberships) {
        return getInactivesRecentMemberships();
      }
    }
    return getActives();
  }
  
  //===========================================================================
  // Find all Contacts that are Active, and have had no activity (tasks/events) 
  // in the last year, are at least a year old, are not tied to any active 
  // campaigns, not tied to any open opportunities, not had the Status field 
  // changed in the last month (this is a buffer to prevent auto-switching back
  // to inactive if someone does update them) and not an SFDC user (experian 
  // employee).
  //===========================================================================
  private Database.Querylocator getActives() {
    // Note about this query. We cannot add more subqueries (max 2 allowed) if there are more checks required.
    makeInactive = true;
    return Database.getQueryLocator([
      SELECT Id, Status__c
      FROM Contact 
      WHERE Status__c = :Constants.STATUS_ACTIVE
      AND (LastActivityDate < LAST_N_DAYS:365 OR (LastActivityDate = null AND CreatedDate < LAST_N_DAYS:365)) 
      AND Id NOT IN (SELECT ContactId FROM CampaignMember WHERE Campaign.IsActive = true) 
      AND Id NOT IN (SELECT ContactId FROM OpportunityContactRole WHERE Opportunity.IsClosed = false)
      AND (Status_Last_Updated_Date__c < LAST_N_DAYS:31 OR Status_Last_Updated_Date__c = null)
      AND SFDC_User__c = null
    ]);
  }
  
  //===========================================================================
  // Find all contacts that are currently inactive, but have had an activity 
  // in the last day, and not had the status updated in the last day. This 
  // helps clean up any that might have been recently made inactive, but a 
  // recent activity will make them active again automatically.
  //===========================================================================
  private Database.Querylocator getInactivesRecentTasks() {
    makeInactive = false;
    return Database.getQueryLocator([
      SELECT Id, Status__c
      FROM Contact 
      WHERE Status__c = :Constants.CONTACT_STATUS_INACTIVE
      AND LastActivityDate >= LAST_N_DAYS:1
      AND Status_Last_Updated_Date__c < LAST_N_DAYS:1
      AND SFDC_User__c = null 
    ]);
  }
  
  //===========================================================================
  // Find all contacts that are currently inactive, but are now on an open 
  // opportunity, where the status hasn't been updated in the last day
  //===========================================================================
  private Database.Querylocator getInactivesRecentOpenOpps() {
    makeInactive = false;
    return Database.getQueryLocator([
      SELECT Id, Status__c
      FROM Contact 
      WHERE Status__c = :Constants.CONTACT_STATUS_INACTIVE
      AND Status_Last_Updated_Date__c < LAST_N_DAYS:1
      AND Id IN (SELECT ContactId FROM OpportunityContactRole WHERE Opportunity.IsClosed = false)
      AND SFDC_User__c = null 
    ]);
  }
  
  
  //===========================================================================
  // Find all contacts that are currently inactive, but are now on an active 
  // campaign, where the status hasn't been updated in the last day
  //===========================================================================
  private Database.Querylocator getInactivesRecentCampaigns() {
    makeInactive = false;
    return Database.getQueryLocator([
      SELECT Id, Status__c
      FROM Contact 
      WHERE Status__c = :Constants.CONTACT_STATUS_INACTIVE
      AND Status_Last_Updated_Date__c < LAST_N_DAYS:1
      AND Id IN (SELECT ContactId FROM CampaignMember         
                 WHERE HasResponded    = true//Case 02265655
                 AND Campaign.IsActive = true)
      AND SFDC_User__c = null 
    ]);
  }
  
  //===========================================================================
  // Find all contacts that are currently inactive, but are now on a case
  // created in the last day
  //===========================================================================
  private Database.Querylocator getInactivesRecentCases() {
    makeInactive = false;
    return Database.getQueryLocator([
      SELECT Id, Status__c
      FROM Contact 
      WHERE Status__c = :Constants.CONTACT_STATUS_INACTIVE
      AND Status_Last_Updated_Date__c < LAST_N_DAYS:1
      AND Id IN (SELECT ContactId FROM Case WHERE CreatedDate >= LAST_N_DAYS:1)
      AND SFDC_User__c = null 
    ]);
  }
  
  //===========================================================================
  // Find all contacts that are currently inactive, but are now on a membership
  // created in the last day
  //===========================================================================
  private Database.Querylocator getInactivesRecentMemberships() {
    makeInactive = false;
    return Database.getQueryLocator([
      SELECT Id, Status__c
      FROM Contact 
      WHERE Status__c = :Constants.CONTACT_STATUS_INACTIVE
      AND Status_Last_Updated_Date__c < LAST_N_DAYS:1
      AND Id IN (SELECT Contact_Name__c FROM Membership__c WHERE CreatedDate >= LAST_N_DAYS:1)
      AND SFDC_User__c = null 
    ]);
  }
  
  //===========================================================================
  // 
  //===========================================================================
  public void execute (Database.BatchableContext bc, List<Contact> scope) {
    List<Contact> updConts = new List<Contact>();
    // Moved a check for contacts with cases into this part since there is no room in the original query
    Map<Id, Contact> contactsWithCases = new Map<Id, Contact>();
    Map<Id, Contact> contactsWithMemberships = new Map<Id, Contact>();
    if (makeInactive == true) {
      Set<Id> contactIdSet = new Set<Id>((new Map<Id, Contact>(scope)).keySet());
      contactsWithCases = new Map<Id, Contact>([
        SELECT Id
        FROM Contact
        WHERE Id IN (
          SELECT ContactId
          FROM Case
          WHERE CreatedDate >= LAST_N_DAYS:365
          AND ContactId IN :contactIdSet
        )
      ]);
      contactsWithMemberships = new Map<Id, Contact>([
        SELECT Id
        FROM Contact
        WHERE Id IN (
          SELECT Contact_Name__c
          FROM Membership__c
          WHERE CreatedDate >= LAST_N_DAYS:365
          AND Contact_Name__c IN :contactIdSet
        )
      ]);
    }
    for (Contact c : scope) {
      // Adding a check to not change to inactive if the map contains the contact id
      if (!(contactsWithCases.containsKey(c.Id) || contactsWithMemberships.containsKey(c.Id))) {
        updConts.add(
          new Contact(
            Id = c.Id, 
            Status__c = (makeInactive) ? Constants.CONTACT_STATUS_INACTIVE : Constants.STATUS_ACTIVE, 
            Status_Last_Updated_Date__c = Datetime.now()
          )
        );
      }
    }
    
    List<Database.SaveResult> srList = Database.update(updConts, false);
    for (Integer i = 0; i < srList.size(); i++) {
      if (!srList.get(i).isSuccess()) {
        // DML operation failed
        system.debug(srList.get(i).getErrors());
        errorsFound.add('Contact Id: ' + updConts.get(i).Id + '\n' + BatchHelper.parseErrors(srList.get(i).getErrors()));
      }
    }
  }
  
  public void finish (Database.BatchableContext bc) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchContactStatusUpdate', false);
    
    if (!errorsFound.isEmpty() || Test.isRunningTest()) {
      bh.batchHasErrors = true;
      bh.emailBody += 'Errors found: \n\n'+String.join(errorsFound, '\n\n');
    }
    
    bh.sendEmail();
    
    // Check if we have processes/queries still left to process and then create a temporary scheduled job to process the batch.
    if (!Test.isRunningTest() && !processesToDoList.isEmpty()) {
      system.scheduleBatch(new BatchContactStatusUpdate(processesToDoList), 'Contact - BatchContactStatusUpdate - '+processesToDoList.get(0).name()+String.valueOf(Datetime.now().getTime()), 0, ScopeSizeUtility.getScopeSizeForClass('BatchContactStatusUpdate'));
    }
    
  }
  
}