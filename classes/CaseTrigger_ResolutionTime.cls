/**=====================================================================
 * Appirio, Inc
 * Name: CaseTrigger_ResolutionTime
 * Description: T-310733: Helper class for Case Resolution Time Tracking
 * Created Date: Sep 08th, 2014
 * Created By: Arpita Bose(Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 * Sep 12th, 2014               Arpita Bose                  S-252787: Modified class to fix the failure on Case record creation
 * Feb 04th, 2015               Naresh Kumar Ojha            T-358487: Added record type CSDA Contract Request to track case history.
 * Feb 24th, 2015               Naresh Kr Ojha               T-365485: Updated code to adopt History__c object instead Case_History__c as per task.
 * Jun 18th, 2015               Naresh Kr Ojha               T-411739: Updated method createCaseHistoryRec() as per task.
 * Jan 5th, 2016                James Wills                  Case #01266714 - Updated reference to constant to reflect change in name (see comment below).
 * Apr 6th, 2016                Paul Kissick                 Case #01864389 - Adding history tracking to include Queues
 * Apr 21st, 2016               Paul Kissick                 Adding optimisations to support multiple executions
 * Jun 14th, 2016               Diego Olarte                 Case #01964765 - Update to add Case_Owner__c to be populated to get more user details
  =====================================================================*/

public class CaseTrigger_ResolutionTime {

  public static Set<Id> userIdSet = new Set<Id> ();
  public static Set<Id> queueIdSet = new Set<Id> ();

  public static Map<Id, User> userMap = new Map<Id, User> ();
  public static Map<Id, Group> queueMap = new Map<Id, Group> ();
  
  public static Set<String> fieldChangeKeySet = new Set<String> (); // Holds the ids of an owner change to prevent logging twice!

  public static Set<Id> recordTypeIdSet { 
    get {
      if (recordTypeIdSet == null) {
        recordTypeIdSet = new Set<Id> ();
        List<String> caseRecType = new List<String> {
          Constants.CASE_REC_TYPE_NA_CCM_Support, //Case #01266714 James Wills; Changed from Constants.CASE_REC_TYPE_CCM_NA_CASE
          Constants.CASE_REC_TYPE_EDQ_CASE_TECH_SUPPORT, //Case #01266714 James Wills; Changed from Constants.CASE_REC_TYPE_EDQ_CASE
          Constants.CASE_REC_TYPE_CSDA_CONT_REQ,
          Constants.RECORDTYPE_CASE_CSDA_BIS_SUPPORT,
          Constants.RECORDTYPE_CASE_CSDA_CIS_BA_REQUEST,
          Constants.RECORDTYPE_CASE_CSDA_CIS_SUPPORT
        };
        for (String caseRecTypeName : caseRecType) {
          recordTypeIdSet.add(CaseTriggerHandler.recordTypeNameToIdMap.get(caseRecTypeName));
        }
      }
      return recordTypeIdSet;
    } 
    set; 
  }

  //===========================================================================
  // T-310733:After Insert method for creating initial Case history Record
  //===========================================================================
  public static void createCaseHistoryRec(List<Case> lstNew) {
    Set<Id> caseIdSet = new Set<Id>();
    for (Case caseRec : lstNew) {
      if (recordTypeIdSet.contains(caseRec.RecordTypeId)) {
        caseIdSet.add(caseRec.Id);
      }
    }
    
    // Leave now since no cases match the record types.
    if (caseIdSet.isEmpty()) {
      return;
    }
    
    List<History__c> historyList = new List<History__c> ();

    //check ownerId contains user or queue
    for (Case caseRec : lstNew) {
      if (caseRec.OwnerId.getSobjectType() == User.sObjectType) {
        userIdSet.add(caseRec.OwnerId);
      }
      else {
        queueIdSet.add(caseRec.OwnerId);
      }
    }

    // Fetching Owners details
    if (!userIdSet.isEmpty()) {
      userMap = getMapOfUser(userIdSet);
    }

    if (!queueIdSet.isEmpty()) {
      queueMap = getMapOfQueue(queueIdSet);
    }

    //Creating Case History
    for (Case caseRec : lstNew) {
      if (recordTypeIdSet.contains(caseRec.RecordTypeId)) {
        User myUser = null;
        if (userMap.containsKey(caseRec.OwnerId)) {
          myUser = userMap.get(caseRec.OwnerId);
        }
        History__c caseHistory = getCaseHistoryRec(caseRec, myUser);
        caseHistory.Change_type__c = Constants.CASE_CHANGE_TYPE_STATUS;
        
        historyList.add(caseHistory);
      }
    }
    try {
      //Inserting Case History
      insert historyList;
    }
    catch(DmlException ex) {
      apexLogHandler.createLogAndSave('CaseTrigger_ResolutionTime', 'afterInsert', ex.getStackTraceString(), ex);
      for (Integer indx = 0; indx < ex.getNumDml(); indx++) {
        lstNew.get(0).addError(ex.getDMLMessage(indx));
      }
    }
  }

  //===========================================================================
  // Called from After Update on CaseTriggerHandler
  //===========================================================================
  public static void caseResolutionTimeTrackingProcess(List<Case> lstOld, List<Case> lstNew, Map<Id, Case> newMap, Map<Id, Case> oldMap) {
    
    Set<Id> caseIdSet = new Set<Id>();
    for (Case caseRec : lstNew) {
      if (recordTypeIdSet.contains(caseRec.RecordTypeId)) {
        caseIdSet.add(caseRec.Id);
      }
    }
    
    // Leave now since no cases match the record types.
    if (caseIdSet.isEmpty()) {
      return;
    }

    Set<Id> caseIdChangedSet = new Set<Id> ();
    List<Case> updateCaseList = new List<Case> ();
    
    Set<Id> parentCaseIdSet = new Set<Id> ();
    List<History__c> historyList = new List<History__c> ();

    // collecting sub case ids
    for (Case caseRec : lstNew) {
      if (recordTypeIdSet.contains(caseRec.RecordTypeId) &&
          (caseRec.OwnerId != oldMap.get(caseRec.Id).OwnerId || caseRec.Status != oldMap.get(caseRec.Id).Status)) {
        caseIdChangedSet.add(caseRec.Id);
      }

      if (caseRec.OwnerId.getSobjectType() == User.sObjectType) {
        userIdSet.add(caseRec.OwnerId);
      }
      else {
        queueIdSet.add(caseRec.OwnerId);
      }
    }

    system.debug('~~~~caseIdChangedSet~~~' + caseIdChangedSet);

    // Fetching Owners details
    if (!userIdSet.isEmpty()) {
      userMap = getMapOfUser(userIdSet);
    }

    if (!queueIdSet.isEmpty()) {
      queueMap = getMapOfQueue(queueIdSet);
    }

    // Updating previous Case History for End time
    if (!caseIdChangedSet.isEmpty()) {
      for (Case caseRec : [SELECT Id, RecordTypeId, IsParent__c, Service_Desk_Parent_Case__c, Status, IsClosed,
                            (SELECT Id, End_time__c
                             FROM Case_History__r
                             WHERE End_time__c = null
                             ORDER BY CreatedDate DESC LIMIT 1)
                           FROM Case
                           WHERE Id IN :caseIdChangedSet]) {
        system.debug('***caseRec.Case_History__r**' + caseRec.Case_History__r);
        if (caseRec.Case_History__r != null && caseRec.Case_History__r.size() > 0) {
          History__c caseHistory = new History__c(
            Id = caseRec.Case_History__r.get(0).Id,
            End_time__c = DateTime.now()
          );
          historyList.add(caseHistory);

          system.debug('~~~caseRec.Case_History__r~~~' + historyList.size());
          system.debug('~~~recordTypeIdSet.contains(caseRec.RecordTypeId)~~~' + recordTypeIdSet.contains(caseRec.RecordTypeId));
          system.debug('~~~caseRec.IsParent__c~~~' + caseRec.IsParent__c);
          system.debug('~~~caseRec.Service_Desk_Parent_Case__c~~~' + caseRec.Service_Desk_Parent_Case__c);
          system.debug('~~~caseRec.Status != oldMap.get(caseRec.Id).Status~~~' + (caseRec.Status != oldMap.get(caseRec.Id).Status));

          // Filter Parent Case for updating child cases
          if (recordTypeIdSet.contains(caseRec.RecordTypeId) &&
              caseRec.IsParent__c &&
              caseRec.Service_Desk_Parent_Case__c == null &&
              caseRec.Status != oldMap.get(caseRec.Id).Status) {
            parentCaseIdSet.add(caseRec.Id);
          }
        }
      }
    }

    system.debug('~~~~~~~~lstNew~~~~' + lstNew);

    //create case history records
    for (Case caseRec : lstNew) {
      if (!userMap.containsKey(caseRec.OwnerId)) {
        // continue; // Case 01864389
      }

      User myUser = userMap.get(caseRec.OwnerId);

      if (recordTypeIdSet.contains(caseRec.RecordTypeId)) {

        // create case history for change type = 'Owner'
        if (caseRec.OwnerId != oldMap.get(caseRec.Id).OwnerId) {
          String ownerChangeKey = caseRec.Id + '~' + oldMap.get(caseRec.Id).OwnerId + '~' + caseRec.OwnerId;
          if (fieldChangeKeySet.contains(ownerChangeKey)) {
            system.debug('Already caught owner change, not adding another one. '+ownerChangeKey);
            continue;
          }
          system.debug('~~~ Owner Change from ' + oldMap.get(caseRec.Id).OwnerId + ' to ' + caseRec.OwnerId);
          
          fieldChangeKeySet.add(ownerChangeKey);
          History__c caseHistory = new History__c();
          caseHistory = getCaseHistoryRec(caseRec, myUser);
          caseHistory.Change_type__c = Constants.CASE_CHANGE_TYPE_OWNER;
          historyList.add(caseHistory);
        }
        // create case history for change type='Status'
        else if (caseRec.Status != oldMap.get(caseRec.Id).Status) {
          String statusChangeKey = caseRec.Id + '~' + oldMap.get(caseRec.Id).Status + '~' + caseRec.Status;
          if (fieldChangeKeySet.contains(statusChangeKey)) {
            system.debug('Already caught status change, not adding another one. '+statusChangeKey);
            continue;
          }
          fieldChangeKeySet.add(statusChangeKey);
          system.debug('~~~ Status Change from ' + oldMap.get(caseRec.Id).Status + ' to ' + caseRec.Status);
          History__c caseHistory = getCaseHistoryRec(caseRec, myUser);
          caseHistory.Change_type__c = Constants.CASE_CHANGE_TYPE_STATUS;
          historyList.add(caseHistory);
        }
      }

      system.debug('===parentCaseIdSet==' + parentCaseIdSet);

      // Updating child case status
      if (!parentCaseIdSet.isEmpty()) {
        for (Case caseRecord : [SELECT Id, Status, Case_Owned_by_Queue__c, Service_Desk_Parent_Case__r.Status, 
                                       IsClosed, Service_Desk_Parent_Case__c
                                       FROM Case
                                       WHERE Service_Desk_Parent_Case__c IN :parentCaseIdSet
                                       AND RecordTypeId IN :recordTypeIdSet]) {
          system.debug('===caseRecord==' + caseRecord);
          if (!caseRecord.IsClosed && 
              caseRecord.Case_Owned_by_Queue__c &&
              !newMap.get(caseRecord.Service_Desk_Parent_Case__c).IsClosed) {
            caseRecord.Status = newMap.get(caseRecord.Service_Desk_Parent_Case__c).Status;
            updateCaseList.add(caseRecord);
          }

          if (newMap.get(caseRecord.Service_Desk_Parent_Case__c).IsClosed) {
            caseRecord.Status = newMap.get(caseRecord.Service_Desk_Parent_Case__c).Status;
            updateCaseList.add(caseRecord);
          }
        }
        try {
          // Updating Cases
          update updateCaseList;
        }
        catch(DmlException ex) {
          apexLogHandler.createLogAndSave('CaseTrigger_ResolutionTime', 'caseResolutionTimeTrackingProcess', ex.getStackTraceString(), ex);
          for (Integer indx = 0; indx < ex.getNumDml(); indx++) {
            lstNew.get(0).addError(ex.getDMLMessage(indx));
          }
        }
      }

      try {
        // Upserting Case History
        upsert historyList;
      }
      catch(DmlException ex) {
        apexLogHandler.createLogAndSave('CaseTrigger_ResolutionTime', 'CaseTrigger_ResolutionTime', ex.getStackTraceString(), ex);
        for (Integer indx = 0; indx < ex.getNumDml(); indx++) {
          lstNew.get(0).addError(ex.getDMLMessage(indx));
        }
      }
    }
  }

  // generic method to create Case history
  public static History__c getCaseHistoryRec(Case c, User u) {
    History__c h = new History__c();
    h.Case__c = c.Id;
    if (c.OwnerId.getSobjectType() == User.sObjectType) {
      h.Owner_Type__c = Constants.CASE_HISTORY_OWNER_TYPE_USER;
      h.Owner_name__c = u.Name;
      h.Case_Owner__c = u.Id;
      h.Users_Primary_Queue__c = u.Primary_queue_name__c;
    }
    else {
      h.Owner_Type__c = Constants.CASE_HISTORY_OWNER_TYPE_QUEUE;
      h.Owner_name__c = queueMap.get(c.OwnerId).Name;
    }
    h.Owner_id__c = c.OwnerId;
    h.Start_time__c = DateTime.now();
    h.New_status__c = c.Status;
    h.Case_Severity__c = c.Severity__c;
    return h;
  }
  
  // generic method to get map of User
  public static Map<Id, User> getMapOfUser(Set<Id> userIds) {
    // retrieve map of User Records
    return new Map<Id, User> ([
      SELECT Id, Primary_queue_name__c, Email, Name
      FROM User
      WHERE Id IN :userIds
    ]);
  }

  // generic method to get map of Queue
  public static Map<Id, Group> getMapOfQueue(Set<Id> queueIds) {
    // retrieve map of Group Records
    return new Map<Id, Group> ([
      SELECT Id, Name
      FROM Group
      WHERE Id IN :queueIds
    ]);
  }
  
}