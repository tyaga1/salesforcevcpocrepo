/**=====================================================================
 * Experian
 * Name: ScheduleWalletSyncStart
 * Description: This is the Master Schedule class for processing updates from the Wallet Sync process.
 *              This class will automatically reschedule itself if the upload process is still running.
 *              It will also bail out if it ends up running more than 1 job simultaneously.
 * 
 * Created Date: 11th August, 2015
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 * Jan 11th, 2016               Paul Kissick                 Case 01266075 - Fix to remove upload check & process running
 =====================================================================*/
global class ScheduleWalletSyncStart implements Schedulable{
    
  global void execute(SchedulableContext sc) {
    // Check for existing re-schedules and purge...
    /*
    //  NO LONGER REQUIRED - PK : Jan 11th, 2016
    for(CronTrigger aJob : [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name LIKE 'WalletSyncAuto%' AND State = 'DELETED']) {
      system.abortJob(aJob.Id);
    }
    
    // Check that the wallet sync isn't already in progress....
    if (WalletSyncUtility.isUploadingRunning()) {
      // still uploading, can we reschedule for 1 hour later...
      Datetime currentTime = Datetime.now().addHours(1);
      system.debug(LoggingLevel.INFO,'WalletSync Upload is still running. Rescheduling processing for '+currentTime);
      String CRON_EXP = '0 0 '+String.valueOf(currentTime.hour())+' '+String.valueOf(currentTime.day())+' '+String.valueOf(currentTime.month())+' ? '+String.valueOf(currentTime.year())+'';
      String jobId = System.schedule('WalletSyncAuto'+String.valueOf(currentTime.getTime()), CRON_EXP, new ScheduleWalletSyncStart());      
    }
    else {
	    if (!WalletSyncUtility.isProcessingRunning()) { */
	      WalletSyncUtility.processingStarted();
	      system.scheduleBatch(
	        new BatchWalletSyncUserLookups(),
	        'BatchWalletSyncUserLookups'+String.valueOf(Datetime.now().getTime()),
	        0,
	        ScopeSizeUtility.getScopeSizeForClass('BatchWalletSyncUserLookups')
	      );
	    //}
    //}
    
  }
  

}