/**=====================================================================
  * Experian
  * Name: CaseCreateRedirectController_test
  * Description:    W-007516 :Test Class for CaseCreateRedirectController. Cover multiple combination of situation
  * Created Date: March 24 2017
  * Created By: Ryan (Weijie) Hu, UCInnovation
  *
  * Date Modified      Modified By                  Description of the update
  * April 19th 2017    Ryan (Weijie) Hu             W-007516: Changes made to get url instead of page reference for live agent bugs
  * July  17th 2017    Manoj Gopu                   Improved Test coverage
  =====================================================================*/

@isTest
private class CaseCreateRedirectController_Test {
  
  @isTest static void test_method_one() {
    // create User
    Profile p = [select id from profile where name=: Constants.PROFILE_SYS_ADMIN ];
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    insert testUser1;

    testUser1.Sales_Team__c = 'Sales Executive';
    testUser1.Business_Unit__c = 'other';
    update testUser1;

    system.runAs(testUser1){

      Test.startTest();
        RecordType normal_case_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'SFDC_Support'];
        ApexPages.currentPage().getParameters().put('RecordType', normal_case_rt.Id);

        Case testCase = new Case();
        testCase.RecordTypeId = normal_case_rt.Id;

        ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)testCase);
        CaseCreateRedirectController ctrl = new CaseCreateRedirectController(stdController);

        ctrl.getURL();


      Test.stopTest();
    }
  }

  @isTest static void test_method_two() {
    // create User
    Profile p = [select id from profile where name=: Constants.PROFILE_SYS_ADMIN ];
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    insert testUser1;

    testUser1.Sales_Team__c = 'Sales Executive';
    testUser1.Business_Unit__c = 'LATAM Serasa Customer Care2';
    update testUser1;

    system.runAs(testUser1){
      
      Test.startTest();
        RecordType normal_case_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Serasa_Support_Request'];
        ApexPages.currentPage().getParameters().put('RecordType', normal_case_rt.Id);

        Case testCase = new Case();
        testCase.RecordTypeId = normal_case_rt.Id;

        ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)testCase);
        CaseCreateRedirectController ctrl = new CaseCreateRedirectController(stdController);

        ctrl.getURL();


      Test.stopTest();
    }
  }

  @isTest static void test_method_three() {
    // create User
    Profile p = [select id from profile where name=: Constants.PROFILE_SYS_ADMIN ];
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    insert testUser1;

    testUser1.Sales_Team__c = 'Sales Executive';
    testUser1.Business_Unit__c = 'other';
    update testUser1;

    system.runAs(testUser1){
      
      Test.startTest();
        RecordType normal_case_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Serasa_Support_Request'];
        ApexPages.currentPage().getParameters().put('RecordType', normal_case_rt.Id);

        Case testCase = new Case();
        testCase.RecordTypeId = normal_case_rt.Id;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)testCase);
        CaseCreateRedirectController ctrl = new CaseCreateRedirectController(stdController);

        ctrl.getURL();


      Test.stopTest();
    }
  }

  @isTest static void test_method_four() {
    // create User
    Profile p = [select id from profile where name=: Constants.PROFILE_SYS_ADMIN ];
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    insert testUser1;

    testUser1.Sales_Team__c = 'Sales Executive';
    testUser1.Business_Unit__c = 'other';
    update testUser1;
    
    Serasa_User_Profiles__c objSerasa = new Serasa_User_Profiles__c();
    objSerasa.Name = Constants.SERASA_CUSTOMER_CARE_PROFILES;
    objSerasa.Profiles__c = 'System Administrator; Test';
    insert objSerasa;

    system.runAs(testUser1){
      
      Test.startTest();
        RecordType normal_case_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Serasa_Support_Request'];
        ApexPages.currentPage().getParameters().put('RecordType', normal_case_rt.Id);

        Case testCase = new Case();
        
        ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)testCase);
        CaseCreateRedirectController ctrl = new CaseCreateRedirectController(stdController);

        ctrl.getURL();


      Test.stopTest();
    }
  }
  
}