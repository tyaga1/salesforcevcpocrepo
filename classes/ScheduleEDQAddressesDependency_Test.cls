/**=====================================================================
 * Experian
 * Name: ScheduleEDQAddressesDependency_Test
 * Description: Test for ScheduleEDQAddressesDependency class
 * Created Date: 14 Mar 2016
 * Created By: Paul Kissick (Experian)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class ScheduleEDQAddressesDependency_Test {
  
  @isTest 
  static void testScheduleEDQAddressesDependency() {
    Global_Settings__c gs = new Global_Settings__c();
    gs.Name = 'Global';
    gs.Batch_Failures_Email__c = 'test@here.com';
    insert gs;
        
    Test.startTest();
    
    // Schedule the test job
    String CRON_EXP = '0 0 0 * * ?';
    String jobId = system.schedule('ScheduleEDQAddressesDependency TEST '+String.valueOf(Datetime.now().getTime()), CRON_EXP, new ScheduleEDQAddressesDependency());
    
    // Get the information from the CronTrigger API object  
    CronTrigger ct = [
      SELECT Id, CronExpression, TimesTriggered, NextFireTime
      FROM CronTrigger 
      WHERE Id = :jobId
    ];     
    Test.stopTest();
    
    // Verify the Schedule has been scheduled with the same time specified  
    system.assertEquals(CRON_EXP, ct.CronExpression);

    // Verify the job has not run, but scheduled  
    system.assertEquals(0, ct.TimesTriggered);
  } 
}