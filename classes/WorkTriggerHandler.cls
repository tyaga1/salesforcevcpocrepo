/**=====================================================================

* Name : WorkTriggerHandler
* Description : Update Case Status when Work (User Story) Status is updated
* Created Date: 02/15/2016
* Created By  : Sadar Yacob

* Date Modified              Modified By                 Description
* March 10th 2016            Sadar Yacob                  When Due Date is changed, set the Target Release Date and Target Deployment Date to it
* March 11th 2016            Sadar Yacob                  Modified to prevent firing recursively by adding alreadyUpdated flag
=====================================================================*/
public class WorkTriggerHandler
{
   private static boolean alreadyUpdated = false;
   
   //=========================================================================
   //get the state of the private boolean flag to prevent trigger from firing recursively
   //=========================================================================
   public static boolean isAlreadyUpdated()
   {
     return alreadyUpdated;
   }
   //=========================================================================
   // After Update Call
   //=========================================================================
   public static void afterUpdate(List<agf__ADM_Work__c> Oldlst, LIST<agf__ADM_Work__c> NewLst, Map<Id, agf__ADM_Work__c> newMap, MAP<Id,agf__ADM_Work__c> oldMap) 
   {
    List <agf__ADM_Work__c>  impactedStoryList = new List <agf__ADM_Work__c>();
    set<Id> workIds = new Set<Id>();
    
    try{
         if (!WorkTriggerHandler.isAlreadyUpdated() )
         {
             //get a list of all Work records that have their Status's modified
             for ( agf__ADM_Work__c ustory : NewLst)
             {
                 agf__ADM_Work__c  oldUserStory = oldMap.get(ustory.id);
                 
                 //compare the status values
                 String oldStoryStatus  = oldUserStory.agf__Status__c;
                 String newStoryStatus = ustory.agf__Status__c;
                 //String  CaseId = ustory.Case.id;
                 
                 if (newStoryStatus != oldStoryStatus || ustory.agf__Due_Date__c != oldUserStory.agf__Due_Date__c 
                     || ustory.agf__Assignee__c != oldUserStory.agf__Assignee__c )
                 { 
                     impactedStoryList.add(ustory);
                     workIds.add(ustory.Id);
                     System.debug('Story with changed Status: ' + ustory.Id + '; Story Status Old:'+  oldStoryStatus   + '; Story Status new: '+ newStoryStatus  );
                     System.debug('Due Date changes : old date:' + oldUserStory.agf__Due_Date__c + ' ; New Due Date :' + ustory.agf__Due_Date__c);
                 }
                 
             }
         
             // Check if they have a Case associated with them
             List <Case> affectedCases = [SELECT id,OwnerId,agf__ADM_Work__c,Status,Implementation_Status__c,Status_Quick_Note__c,Target_Deployment_Date__c ,
                                          Target_Release_Date__c,Reason_For_Reschedule__c from Case WHERE agf__ADM_Work__c in :workIds ];
             
             //get the list of related Case and check if Status is not the same and then sync from Work to Case Record
             for ( Case inc  :affectedCases )
             {
                agf__ADM_Work__c workRec = NewMap.get(inc.agf__ADM_Work__c);
                System.debug('Impacted Case Id : '+ inc.id + ' ; Case status :' + inc.Status + '; Implemntation Status : ' + inc.Implementation_Status__c );
                System.debug ('Related Work Rec: ' + workRec.Id + '; work status :' + workRec.agf__Status__c);  
                //Check to make sure the Case Status and User Story Status are not in Sync   
                if ( workRec.agf__Status__c !=null && workRec.agf__Status__c != inc.Implementation_Status__c )  {
                   inc.Implementation_Status__c = workRec.agf__Status__c;
                }
                date  storyDueDate =system.today() ;
                if (workRec.agf__Due_Date__c != null)
                {   
                  storyDueDate =date.newInstance(workRec.agf__Due_Date__c.year(),workRec.agf__Due_Date__c.month(),workRec.agf__Due_Date__c.day()) ;
                }
                
                date targetReleaseDate =inc.Target_Release_Date__c;
                
                system.debug('Story Due Date:'+ storyDueDate + '; Case Target Releaes Date:' + targetReleaseDate);
                
                if (workRec.agf__Due_Date__c != null && inc.Target_Release_Date__c !=  storyDueDate) 
                {
                  system.debug('story due date <> Case Release date so reset it');
                  inc.Target_Release_Date__c = storyDueDate; // workRec.agf__Due_Date__c;  
                  inc.Target_Deployment_Date__c = storyDueDate;     
                  if ( inc.Reason_For_Reschedule__c == null)
                  {
                   inc.Reason_For_Reschedule__c ='Developer not Available';
                  }
                }
                string workownerid =  workRec.agf__Assignee__c; 
                 if (workRec.agf__Assignee__c != null && inc.OwnerId !=  workRec.agf__Assignee__c && workownerid.startsWith('005') == true )   
                {
                  system.debug('Case owner different from Story Assignee so reset it: Assignee is:' + workownerid );
                  inc.OwnerId = workRec.agf__Assignee__c ;       
                }
                
                if ( workRec.Status_Notes__c != null && inc.Status_Quick_Note__c != workRec.Status_Notes__c )
                {
                  inc.Status_Quick_Note__c = workRec.Status_Notes__c;
                }
             }
             
             if (affectedCases.size() > 0 ) 
             {
              update affectedCases;
             }
             WorkTriggerHandler.alreadyUpdated = true;
        }
     }
     catch (DMLException ex) {
        system.debug('[WorkTriggerHandler: afterUpdate] Exception: ' + ex.getMessage());
        //ApexLogHandler.createLogAndSave('WorkTriggerHandler','afterUpdate', ex.getStackTraceString(), ex);
      }
     
   }
  
}