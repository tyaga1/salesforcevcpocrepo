/**=============================================================================
 * Experian plc
 * Name: BatchTrainingEmailReminderForOnboarding - used in a scheduler [SchedulerTrainingEmailReminder]
 * Description: Case #02069865 Custom object for Training management console
 * Created Date: March 15th, 2017
 * Created By: Sanket Vaidya
 * 
 * Date Modified        Modified By                  Description of the update
 * 
 =============================================================================*/

public class BatchTrainingEmailReminderForOnboarding implements Database.Batchable<sObject>
{
	public Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator([SELECT Id, 
                                                Name,
                                                Contact__r.Name, 
                                                Contact__r.Email, 
                                                Trainer__R.Name, 
                                                Trainer__R.Email, 
                                                On_boarding_Comm_Date_sent__c, 
                                                Training_Completed_Date__c,
                                                Training_type__c,
                                                Check_in_session_Date__c
                                        FROM Training__c 
                                        where
                                            Training_Completed_Date__c = null 
                                            AND Training_type__c = 'On-boarding' 
                                            AND On_boarding_Comm_Date_sent__c != null                                        
                                        ]);        
    }
    
    public void execute(Database.BatchableContext bc, List<Training__c> trainingList)
    {        
        Decimal days = Custom_Fields_Ids__c.getInstance().BatchTrainingReminderNumberOfDays__c;
        System.Debug('Reminder to send before how many days : ' + days);

        String teamEmail = Custom_Fields_Ids__c.getInstance().BatchTrainingReminder_TrainingTeamEmail__c;
        System.Debug('Team Email : ' + teamEmail);

        List<Training__c> emailTrainingList = new List<Training__c>();

        for(Training__c tr : trainingList)
        {   
            if(tr.Training_type__c == 'On-boarding' && tr.On_boarding_Comm_Date_sent__c != null)
            {
                Integer diff = Date.today().daysBetween(date.valueof(tr.On_boarding_Comm_Date_sent__c));

                System.Debug('Training Name : ' + tr.Name + '; Difference : ' + diff );

                if(diff < days) 
                {
                   emailTrainingList.add(tr);
                }
            }
        }

        if(emailTrainingList.size() > 0)
        {
            SendEmail(emailTrainingList, teamEmail, days);
        }
    }
    
    public void finish(Database.BatchableContext bc)
    {          
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();        
        mail.setToAddresses(new String[] {UserInfo.getUserEmail()});                
        mail.setSubject('Batch Process Completed');
        mail.setPlainTextBody('Batch Process : \"BatchTrainingEmailReminderForOnboarding\" has completed');        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

    public void SendEmail(List<Training__c> trList, string teamEmail, Decimal daysForReminder)
    {
        if(String.isEmpty(teamEmail))
        {         
            System.debug('No team email address found.');
            return;
        }
        
        // First, reserve email capacity for the current Apex transaction to ensure
        // that we won't exceed our daily email limits when sending email after
        // the current transaction is committed.
        Messaging.reserveSingleEmailCapacity(2);
       
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        mail.setToAddresses(new String[]{teamEmail});
       
        mail.setSubject('This is a training reminder for On-boarding records.');        
        mail.setBccSender(false);        
        mail.setUseSignature(true);

        String emailBodyText = '';

        Integer counter = 1;
        emailBodyText = '<table><tr>' 
                                + '<th>.</th>'
                                + '<th>Training Name</th>'
                                + '<th>On boarding Comm. date sent</th>'
                                + '<th>Contact Name</th>' 
                                + '<th>Contact Email</th>'
                               + '</tr>';
        for(Training__c tr : trList)
        {
            emailBodyText += '<tr>' +
                                '<td><strong>' + counter++ + '.</strong></td>' + 
                                '<td>' + 
                                    '<a href=\"' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + tr.Id + '\"> ' + tr.Name + '</a>' +
                                '</td>' + 
                                '<td>' + tr.On_boarding_Comm_Date_sent__c + '</td>' +
                                '<td>' + tr.Contact__r.Name + '</td>' +
                                '<td>' + tr.Contact__r.Email + '</td>' +
                            '</tr>';
        }

        emailBodyText += '</table>';

        // Specify the text content of the email.
        mail.setHTMLBody('Reminder for On-boarding training records. <br/><br/>' +
                          '<strong>Their \"On-boarding Comm sent date\" is ' + Integer.valueOf(daysForReminder) + ' days ago and the Date training completed is blank.</strong><br/>' +
                          '<br/><br/>' + 
                                emailBodyText );


        // Send the email you have created.
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}