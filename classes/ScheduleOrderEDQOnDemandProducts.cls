global class ScheduleOrderEDQOnDemandProducts implements Schedulable
{
  /*
  * Author:     Diego Olarte (Experian)
  * Description:  The following class is for scheduling the 'OrderEDQOnDemandProducts.cls' class to run at specific intervals
  */  
  
  global void execute(SchedulableContext sc)
  {
    OrderEDQOnDemandProducts batchToProcess = new OrderEDQOnDemandProducts();
    database.executebatch(batchToProcess);
  }
}