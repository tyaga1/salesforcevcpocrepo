/********************************************************************************
* Appirio Inc.
* Name : AccountPlanUtility
* Description:
* Created : Naresh Kr Ojha (Sep 22nd, 2015).
*
* Modified On           Modified By                 Description
* 23rd Sept, 2015       Jagjeet Singhh(Appirio)     Avoid Validation for the Certain profiles.
* 29th, Apr, 2016       James Wills                 Case #01848189 Account Planning Project. Deprecated validation for ACCOUNTPLANNING_VALIDATION_MSG_ON_DELETE
*********************************************************************************/
public with sharing class AccountPlanUtility {
  
  //============================================================================
  // Method to validate the user before deleting records.
  //============================================================================
  public static void validateDeletingRecords (List<sObject> newList) {
    Set<ID> accountPlanIDs = new Set<ID>();
    Map<ID, Boolean> hasAccess = new Map<ID, Boolean>();
    ID currentUserId = UserInfo.getUserId();  
    ID currentUserProfId = UserInfo.getProfileId();
    
    //retrieve the profile name
    String profileName = [SELECT Name FROM Profile WHERE Id = :currentUserProfId].Name;
    // Check if the User belongs to certain profiles and avoid the validations - Certified Sales Admin or Sales Effectiveness
    if(profileName == 'Experian Certified Sales Administrators' || profileName == Constants.PROFILE_EXP_SALES_EXEC){
       return ;      
    }
    //Getting Account Plan ID from deleted record(s).
    //also populating hasAccess by default false.    
    for (sObject sObj : newList) {
      String accPlanID = (ID)sObj.get('Account_Plan__c');
      accountPlanIDs.add(accPlanID );
      hasAccess.put(accPlanID, false);
    }
    
    //Getting team members to check access for account plans with current user
    for (Account_Plan_Team__c accPlanTeam : [SELECT ID, User__c, Account_Plan__c FROM Account_Plan_Team__c 
                                             WHERE Account_Plan__c IN: accountPlanIDs AND User__c =:currentUserId]) {
      if (accPlanTeam.User__c == currentUserId) {
        hasAccess.put(accPlanTeam.Account_Plan__c, true);
      }
    }
    
    //Case #01848189 Account Planning Project
    //This validation is no longer required.
    //Adding error on non accessed/non permitted records.
    /*for (sObject sObj : newList) {
      ID accPlanID = (ID)sObj.get('Account_Plan__c');
      if (hasAccess.containsKey(accPlanID ) && !hasAccess.get(accPlanID)) {
        String errorMsg = Label.ACCOUNTPLANNING_VALIDATION_MSG_ON_DELETE;
        sObj.addError(errorMsg);
      }
    }*/
  }
}