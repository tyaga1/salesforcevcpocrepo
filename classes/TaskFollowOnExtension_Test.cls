/**=====================================================================
 * Name: TaskFollowOnExtension_Test
 * Description: CRM2:W-005428: There are issues around being able to track a lead
 * This class tests the operation of the TaskFollowOnExtension Apex Class, which opens a Task screen in edit-mode when the custom 'Create Follow-On Task' button on the Task standard screen is pressed. 
 *
 * Created Date: Aug. 9th, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By           Description of the update
 * Aug. 9th 2016         James Wills           CRM2:W-005428: There are issues around being able to track a lead
 * Sept. 7th 2016        James Wills           CRM2:W-005599: Tidying up and resolving test class failure.
 * Dec,  14th 2016       Manoj Gopu            Case:02108066: Added decision Parameter to improve the code coverage 
  =====================================================================*/
  
@isTest
private class TaskFollowOnExtension_Test{
  
  private static testmethod void test_Create_TaskFollowOn_With_Campaign(){
    
    Campaign testCampaign = [SELECT id, Name FROM Campaign WHERE Name = 'TaskFollowOnTest'];
    
    Task testTask = [SELECT id, Subject FROM Task WHERE Campaign__r.Name = 'TaskFollowOnTest'];
    
    Test.startTest();
      
      ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)testTask);
      ApexPages.CurrentPage().getParameters().put('Decision', 'yes');
      TaskFollowOnExtension taskExtension = new TaskFollowOnExtension(stdController);
    
      System.assert(taskExtension.parentID != null);

      PageReference followUpTask = taskExtension.autoRun();   
      
      System.assert(followUpTask.getParameters().KeySet().size()>0, 'TaskFollowOnExtension_Test: Follow-Up Task was not generated correctly.');
      
    Test.stopTest();
    
  }
 
   
  @testSetup
  private static void setUpTestData(){
    
    User thisUser = [SELECT Id FROM User WHERE Id=:UserInfo.getUserId()];
            
    //Create data
    Account testAccount = Test_Utils.createAccount(); 
    testAccount.Name = 'TaskFollowOnTest';
    //Profile profile1 = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN]; 
    //User user1 = Test_Utils.createUser(profile1, 'TestUserForFollowOnTest@experian.com', 'TestUser');
         
    //System.runAs(thisUser){ 
      insert testAccount;   
      Contact testContact = Test_Utils.insertContact(testAccount.id);    
    //}    

    Campaign testCampaign = new Campaign(isActive=true,
                                         Name='TaskFollowOnTest',
                                         Audience__c='Test');    
    insert testCampaign;
    
    Account_Campaign__c testAccountCampaign = new Account_Campaign__c(Account__c  = testAccount.id,Campaign__c = testCampaign.id);
    insert testAccountCampaign;                                                  
  
    Task testTask = new Task(WhoId     = testContact.id,
                            Campaign__c = testCampaign.id,
                            Subject = 'Test');
    insert testTask;  
    
    Custom_Fields_Ids__c customFields = new Custom_Fields_Ids__c(
      ParentTaskIdField__c    = '00N55000000h7lv',
      Task_Seresa_Expense__c  = '00Ni000000H2s2h',
      Task_Outcomes__c        = '00Ni000000F8D8Q',
      Task_Phone_Ext__c       = '00Ni000000GbXkT',
      IsLeadTaskField__c      = '00N55000000h6Zy'
    );
    insert customFields;        
  }  
}