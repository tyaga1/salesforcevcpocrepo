/**=====================================================================
 * UC Innovation
 * Name: TestClassForSerasaMainframeContractWSReq
 * Description:  
 *
 * Created Date: 15 May, 2017
 * Created By: Charlie Park
 =====================================================================*/

@isTest
private class TestClassForSerasaMainframeContractWSReq {
	
	private static testMethod void testSerasaWSCpf() {
    
    Test.setMock(HttpCalloutMock.class, new SerasaMainframeContractWSRequest.MockCalloutContract());
    
    Test.startTest();
    
    SerasaMainframeContractWSRequest.webserviceResponse res = SerasaMainframeContractWSRequest.CallGetContractsWS('000000166');
    
    
    system.assertEquals(true, res.success);
    system.assertEquals(3, res.contractResults.size());
    
    system.assertEquals('200',res.contractResults[0].StatusCode);
    

    Test.stopTest();
  }
  
  private static testMethod void testSerasaWSCpfFail() {
    
    Test.setMock(HttpCalloutMock.class, new SerasaMainframeContractWSRequest.MockBadCalloutContract());
    
    Test.startTest();
    
    SerasaMainframeContractWSRequest.webserviceResponse res = SerasaMainframeContractWSRequest.CallGetContractsWS('00000166');
    
    system.assertEquals(false, res.success);
    system.assertEquals(0, res.contractResults.size());
    
    Test.stopTest();
  }
 
  @testSetup
  private static void setupData() {
    Webservice_Endpoint__c wsEnd = new Webservice_Endpoint__c(Name = 'SerasaMainframe-Contract',Username__c = 'ausername', URL__c = 'https://somewhere.com', Timeout__c = 10, Active__c = true);
    insert wsEnd;
    
    WebserviceEndpointUtil.createWebserviceKey(wsEnd);
    WebserviceEndpointUtil.setWebservicePassword(wsEnd,'apassword');
    
  }
 
}