/*=============================================================================
 * Experian
 * Name: AutoCaseSalesPriceChangeRequest
 * Description: Creates a case and set it on edit mode for the VF page CISPriceChangeform to be used
 * Created Date: 11/29/2016
 * Created By: Diego Olarte
 *
 * Date Modified      Modified By           Description of the update
 * 
 
 * TODO: Replace sObject with the correct API name
 =============================================================================*/
global with sharing class AutoCaseSalesPriceChangeRequest {
   
   webservice static String autoCaseSPCR(String accID) {

      Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(label.CSDA_Online_Price_Change_Request).getRecordTypeId();
      
      String returnResult = '';
      
      Account originAccount = [Select id, name FROM ACCOUNT WHERE id = :accID];
                                 
      //Save point
      Savepoint sp = Database.setSavepoint();
    
      List<Case> caseList = new List<Case>();
            
      try {
      
          Case newAutoCaseSPCR = new Case (
          
          AccountId=originAccount.id,
          RecordTypeId= devRecordTypeId,
          Origin='Internal',
          Status='New',
          Support_Team__c='--None--',
          Subject='Price Change Request');
          caseList.add(newAutoCaseSPCR);
          
          insert caseList;
                
          returnResult = 'Click OK to continue with a new price change request form.';          
      }
      
      catch (Exception e) {
      Database.rollback(sp);
      returnResult = 'Cannot add case because of error: ' + e.getMessage();
      system.debug('[AutoCaseSalesPriceChangeRequest: AutoCaseSalesPriceChangeRequest] Exception: ' + e.getMessage());
      ApexLogHandler.createLogAndSave('AutoCaseSalesPriceChangeRequest','AutoCaseSalesPriceChangeRequest', e.getStackTraceString(), e);
    }
    return returnResult;
      
   }
}