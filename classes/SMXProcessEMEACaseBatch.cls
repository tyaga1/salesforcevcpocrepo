/**=====================================================================
 * Experian
 * Name: SMXProcessEMEACaseBatch 
 * Description: Implements the SMXProcessCaseBatch survey but for EMEA cases only
 * Created Date: Sep 25th, 2017
 * Created By: Diego Olarte
 * 
 * Date Modified     Modified By        Description of the update
 *                                         
 =====================================================================*/

global class SMXProcessEMEACaseBatch implements Database.Batchable<Case>, Database.AllowsCallouts{

  global Iterable<Case> start(database.batchablecontext BC){
    if (Test.isRunningTest()) {
      case cs = SMXProcessEMEACaseBatchTest.prepareTestData();
      List<Case> casesToUpdate = new List<Case>();
      casesToUpdate.add(cs);
      return casesToUpdate; 
    }
    else{
      // Query includes the Email and related survey/feedback records. 
      return [
        SELECT Id, ContactId,Contact.AccountId, Contact.Email, (SELECT Id FROM Survey__r)
        FROM Case 
        WHERE IsClosed = true 
        AND ClosedDate >= YESTERDAY 
        AND Status != 'Closed - Duplicate'
        AND Case_Owned_by_Queue__c = false
        AND EDQ_Support_Region__c = 'EMEA'
        AND RecordTypeId IN (
          SELECT Id 
          FROM RecordType 
          WHERE Name IN ('EDQ Technical Support', 'EDQ Commercial Support', 'EDQ GPD Support')
        ) 
        AND ContactId != null 
        AND ContactId != ''
      ];
    }
    //return AssetsToUpdate;
  }

  global void execute(Database.BatchableContext BC, List<Case> scope){
 
    String strSurveyName = 'EDQ Support Experience (EMEA)';
    String strSurveyId = 'EXPERIAN_114851';
    
    List <Feedback__c> feedbackList = new List<Feedback__c>();
    Long lgSeed = System.currentTimeMillis();
    Boolean result = false;
    for(Case cs : scope){
      
      List<Feedback__c> lstSurvey = cs.Survey__r;
      
      //Check for email address not blank
      if(lstSurvey.isEmpty() && (String.isNotBlank(cs.Contact.Email) && !cs.Contact.Email.endsWithIgnoreCase('experian.com'))) {
        lgSeed = lgSeed + 1;
        Feedback__c feedback = new Feedback__c();
        feedback.Case__c = cs.Id;
        feedback.Name = 'P_' + lgSeed;
        feedback.Contact__c = cs.ContactId; //ContactName 
        feedback.Account__c = cs.Contact.AccountId; 
        feedback.Status__c = 'Nominated';               
        feedback.DataCollectionName__c = strSurveyName;
        feedback.DataCollectionId__c = strSurveyId;
        feedbackList.add(feedback);        
      }
    }
    insert feedbackList;
  }

  global void finish(Database.BatchableContext info){
    if(Test.isRunningTest()){
      SMXProcessCaseBatchTest.ClearTestData();
    }
  }//global void finish loop
    
  
}