/**=====================================================================
 * Appirio, Inc
 * Name: ContactEDQDependencyExists
 * Description: The following batch class is designed to be scheduled to run every day.
                    This class will get all Contacts with an EDQ Dependency
 * Created Date: 6/17/2015
 * Created By: Diego Olarte (Experian)
 *
 * Date Modified                Modified By                  Description of the update
 * Sep 14th, 2015               Paul Kissick                 Adding support for testing
 * Nov 9th, 2015                Paul Kissick                 Case 01266075: Adding improvements to prevent multiple querying
 =====================================================================*/
public class ContactEDQDependencyExists{
/*global class ContactEDQDependencyExists implements Database.Batchable<sObject> {

  global Database.Querylocator start ( Database.BatchableContext bc ) {
    return Database.getQueryLocator ([
      SELECT Id, AccountId 
      FROM Contact 
      WHERE EDQ_On_Demand__c = true
      AND Account.EDQ_Dependency_Exists__c = false
    ]);
  }

  global void execute (Database.BatchableContext bc, List<Contact> scope) {
    Set<Id> accIdSet = new Set<Id>();
    
    List<Account> accountsList = new List<Account>();

    for(Contact con : scope) {
      accIdSet.add(con.AccountId);
    }

    for(Id accId : accIdSet) {
      accountsList.add(new Account(Id = accId, EDQ_Dependency_Exists__c = true));
    }

    if(accountsList != null && accountsList.size() > 0) {
      try {             
        update accountsList;
      }
      catch (DMLException ex) {
        apexLogHandler.createLogAndSave('ContactEDQDependencyExists','execute', ex.getStackTraceString(), ex);
      }
    }

  }

  //To process things after finishing batch
  global void finish (Database.BatchableContext bc) {
    
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'ContactEDQDependencyExists', true);
    
    if (!Test.isRunningTest()) {
      system.scheduleBatch(new OpportunityEDQDependencyExists(),'Opportunity - EDQ Dependency Exists '+String.valueOf(Datetime.now().getTime()),2,ScopeSizeUtility.getScopeSizeForClass('OpportunityEDQDependencyExists'));
    }
    
  }*/

}