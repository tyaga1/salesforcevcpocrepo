/**=====================================================================
 * Appirio, Inc
 * Name: SegmentMaintenanceQueue
 * Description: T-376294: This class is created to avoid future calling exception
 *
 * Created Date: April 08th, 2015
 * Created By: Arpita Bose (Appirio)
 *
 * Date Modified            Modified By                 Description of the update
 * Aug 1st, 2016            Paul Kissick                CRM2:W-005332: Adding support for new Product GBL/BL fields on Acc Segs
 ======================================================================*/
public class SegmentMaintenanceQueue implements Queueable { 

  public Set<Id> recordIds;
  public Set<String> segments;
  
  public SegmentMaintenanceQueue(Set<Id> segmentIds, Set<String> segmentNames) {
    recordIds = segmentIds;
    segments = segmentNames;
  }
  
  public void execute(QueueableContext context) {
    try {
      Map<Id,Account_Segment__c> accountSegmentMap = new Map<Id,Account_Segment__c>();
      // create a map of Account Segment Id and record
      if (segments != null || recordIds != null) {
        if (recordIds != null) {
          accountSegmentMap = AccountSegmentationUtility.getAccountSegmentsByIds(recordIds);
        }
        else {
          accountSegmentMap = AccountSegmentationUtility.getAccountSegmentsByNames(segments);
        }
        if (!accountSegmentMap.isEmpty()) {
          List<Account_Segment__c> updatedSegments = AccountSegmentationUtility.segmentRecalculation(accountSegmentMap);
          if (updatedSegments != null && !updatedSegments.isEmpty()) {
            update updatedSegments;
            system.debug('====updatedSegments>>>' +updatedSegments);
          }
        }
      }
    }
    catch (Exception e) {
      system.debug('\n[AccountSegmentationUtility: segmentationMaintenance]: ['+e.getMessage()+']]');
      apexLogHandler.createLogAndSave('AccountSegmentationUtility','segmentationMaintenance', e.getStackTraceString(), e);
    }
  }
}