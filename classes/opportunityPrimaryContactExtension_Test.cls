/**=====================================================================
 * Experian
 * Name: opportunityPrimaryContactExtension_Test
 * Description: Test Class for Controller for opportunityPrimaryContactExtension. 
 * 
 * Created Date: Sep 14th, 2017
 * Created By: Malcolm Russell
 * 
 * Date Modified        Modified By                  Description of the update
 *
 =====================================================================*/
 @isTest
public class opportunityPrimaryContactExtension_Test{

  public static testMethod void opportunityPrimaryContactExtensionTest(){

    Account testAccount = Test_Utils.insertAccount();
    Contact newcontact  = Test_Utils.insertContact(testAccount.id);

   Opportunity testOpp = Test_Utils.createOpportunity(testAccount.Id);
   insert testOpp;
   
   OpportunityContactRole oppContactRole = Test_Utils.insertOpportunityContactRole(true, testOpp.Id, newcontact.Id, Constants.DECIDER, true);
   
   PageReference pageRef = Page.opportunityPrimaryContact;
    Test.setCurrentPageReference(pageRef);
    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)testOpp);
    
     Test.startTest();
    ApexPages.currentPage().getParameters().put('Id', testOpp.id);
   opportunityPrimaryContactExtension controller = new opportunityPrimaryContactExtension(stdController);
    OpportunityContactRole OCR=controller.primaryContact;
    Test.stopTest();
   
    
    
  }


}