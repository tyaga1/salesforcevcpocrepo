/**=====================================================================
 * Experian
 * Name: BatchOpportunityLIScheduleRevenue
 * Description: Schedule class for BatchOpportunityLIScheduleRevenue
 * Created Date: 19th Jan, 2016
 * Created By: Diego Olarte
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
global class ScheduleBatchOpportunityLIScheduleRevenu implements Schedulable {

  global void execute(SchedulableContext ctx) {
    Database.executeBatch(new BatchOpportunityLIScheduleRevenue(), ScopeSizeUtility.getScopeSizeForClass('BatchOpportunityLIScheduleRevenue'));
  }
  
}