/**=====================================================================
 * Appirio, Inc
 * Name        : CompetitorTriggerHandler
 * Description : Handler class for CompetitorTrigger (for T-291592)
 * Created Date: Jul 4th, 2014
 * Created By  : Sonal Shrivastava (Appirio JDC)
 * 
 * Date Modified                Modified By                  Description of the update
 * Jul 11th, 2014               Arpita Bose(Appirio)         I-120496: Added method updateOppCompeName() to update Competitor Name
 *                                                           with relevant Account Name
 * Jul 11th, 2014               Arpita Bose(Appirio)         I-120496: Commented the code as the requirement is changed as per the chatter
 * Oct 06th, 2014               Diego olarte                 Added deleteOppCompetitorName method to remove comp name from opps
 * Jul 13th, 2015               Nur Azlini                   Case #00617277 Update m2 from ', ,' to ', '+comp.Competitor_name__c
 * Oct 27th, 2015               Paul Kissick                 Case #00617277: Replaced deleteOppCompetitorName with improved code
 * Mar 23rd, 2016               UC Innovation                Case #01861351: Sort the Competitor Names on the Opty field in asc sorted order instead of 
                                                             doing this in the workflow using the method deleteOppCompetitorName()
 * Mar 24th, 2016               Sadar Yacob                  Renamed deleteOppCompetitorName to pdateOppCompetitorName since it is used in both insert/update/delete scenarios
 * Apr 27th, 2016               Tyaga Pati                   Project: New Productised version of Savo: Integration to Salesforce. - Added new method updateFuseTagId to Update Tag on Opportunity for the first instance of
                                                             tag Id available at product level.: Added UpdateCompetitorFuseTagId method
 * Oct 25th, 2016               Paul Kissick                 Case 02181863: Adding bypass when adding a competitor.
 * April 3rd, 2017				Sanket Vaidya				 Case 02150014: CRM 2.0- Opportunity Competitor Information - Added updatePrimaryCompetitor()
 =====================================================================*/
public class CompetitorTriggerHandler {
  
  // I-120496: Commented this code as the requirement is changed
  /*
  //========================================================================================
  // Before Insert Call
  //========================================================================================
  public static void beforeInsert(List<Competitor__c> newList) {
    updateOppCompeName(newList, null); 
  }
  //========================================================================================
  // Before Update Call
  //========================================================================================
  public static void beforeUpdate(List<Competitor__c> newList, Map<Id, Competitor__c> oldMap) {
    updateOppCompeName(newList, oldMap); 
  } */
  
  //============================================================================
  // Method called on After Insert
  //============================================================================
  public static void afterInsert(List<Competitor__c> newList,Map<Id, Competitor__c> newMap) {
    createOppPlanComp(newList);
    updateOppCompetitorName(newMap);
    updateCompetitorFuseTagId(newMap);
    updatePrimaryCompetitor(newMap);
  }

  //============================================================================
  // Method called on After Update
  //============================================================================
  public static void afterUpdate(Map<Id, Competitor__c> newMap) {
    system.debug('++++++++++++++++++++++++++++++++++   After Update Trigger');
    updateOppCompetitorName(newMap);
    updateCompetitorFuseTagId(newMap);
    updatePrimaryCompetitor(newMap);
  }

  //============================================================================
  // Method called on Before Detete
  //============================================================================
  public static void beforeDelete(Map<Id, Competitor__c> oldMap) {
    deleteOppPlanComp(oldMap);
  }
  
  public static void afterDelete(Map<Id,Competitor__c> oldMap) {
    updateOppCompetitorName(oldMap);
  }
  
  //============================================================================
  // Method to create Opportunity Plans Competitors
  //============================================================================
  private static void createOppPlanComp(List<Competitor__c> newList) {
    Map<String, List<Opportunity_Plan__c>> mapOppId_OppPlanList = findOpportunityPlans(newList);
    List<Opportunity_Plan_Competitor__c> lstOppPlanComp = new List<Opportunity_Plan_Competitor__c>();
    
    //Iterate throgh Competitors and create Opportunity Plan Competitor records
    for (Competitor__c comp : newList) {
      if (mapOppId_OppPlanList.containsKey(comp.Opportunity__c)) {
        //Create Opportunity Plan Competitor for each Opportunity Plan
        for (Opportunity_Plan__c oppPlan : mapOppId_OppPlanList.get(comp.Opportunity__c)) {
          lstOppPlanComp.add(new Opportunity_Plan_Competitor__c(
            Opportunity_Plan__c = oppPlan.Id,
            Competitor__c = comp.Id
          ));
        }
      }
    }
    if (!lstOppPlanComp.isEmpty()) {
      insert lstOppPlanComp;
    }
  }
  
  //============================================================================
  // Method to return Opportunity Plans related to Competitor's Opportunities
  //============================================================================
  private static Map<String, List<Opportunity_Plan__c>> findOpportunityPlans(List<Competitor__c> compList) {
    Set<String> oppIdsSet = new Set<String>();
    Map<String, List<Opportunity_Plan__c>> mapOppId_OppPlanList = new Map<String, List<Opportunity_Plan__c>>();
    
    //Find opportunity Ids related to competitors
    for (Competitor__c comp : compList) {
      oppIdsSet.add(comp.Opportunity__c);
    }
    //Find Opportunity Plans related to Opportunities
    for (Opportunity_Plan__c oppPlan : [SELECT Id, Opportunity_Name__c 
                                       FROM Opportunity_Plan__c 
                                       WHERE Opportunity_Name__c IN :oppIdsSet]) {
      if (!mapOppId_OppPlanList.containsKey(oppPlan.Opportunity_Name__c)) {
        mapOppId_OppPlanList.put(oppPlan.Opportunity_Name__c, new List<Opportunity_Plan__c>());
      }
      mapOppId_OppPlanList.get(oppPlan.Opportunity_Name__c).add(oppPlan);
    }
    return mapOppId_OppPlanList;
  }
  
  //============================================================================
  // Method to delete Opportunity Plan Competitors
  //============================================================================
  private static void deleteOppPlanComp(Map<Id, Competitor__c> oldMap) {
    List<Opportunity_Plan_Competitor__c> oppPlanCompList = new List<Opportunity_Plan_Competitor__c>();
    
    for (Opportunity_Plan_Competitor__c opm : [SELECT Id 
                                               FROM Opportunity_Plan_Competitor__c
                                               WHERE Competitor__c IN :oldMap.keySet()]) {
      oppPlanCompList.add(opm);
    }
    if (!oppPlanCompList.isEmpty()) {
      delete oppPlanCompList;
    }
  }
  
  // I-120496: Commented this code as the requirement is changed as per the chatter
  /*
  //============================================================================
  // I-120496: Method to update Competitor Name to the related Account Name
  //============================================================================
  private static void updateOppCompeNameOLD( List<Competitor__c> newList, Map<Id, Competitor__c> oldMap) {
    Set<String> accIDs = new Set<String>();
    List<Competitor__c> lstCompetitor = new List<Competitor__c>();
    Map<String, Account> mapAccId_Acc = new Map<String, Account>();
    
    //Find account Ids related to competitors
    for (Competitor__c comp : newList) {
        if (oldMap == null || (oldMap.get(comp.Id).Account__c != comp.Account__c)) {
            lstCompetitor.add(comp);
            accIDs.add(comp.Account__c);
        }
    }
    
    if (!accIDs.isEmpty()) {
        for (Account acc : [SELECT Id, Name FROM Account WHERE Id IN :accIDs]) {
            mapAccId_Acc.put(acc.Id, acc);
        }
        // Update Competitor's Name from account name
        for (Competitor__c comp : lstCompetitor) {
            comp.Name = mapAccId_Acc.get(comp.Account__c).Name;
        }
    }
  } */

  //============================================================================
  // Method to update competitor name(s) in an oppty as part of insert/update/delete
  //============================================================================
  //Add by DO to remove competitor name in an opp after competitor has been deleted
  // PK: Improved code
  private static void updateOppCompetitorName(Map<Id,Competitor__c> oldCompetitors) {
    // Opps to fix
    Set<Id> oppIds = new Set<Id>();
    for (Competitor__c comp : oldCompetitors.values()) {
      if (comp.Opportunity__c != null) {
        oppIds.add(comp.Opportunity__c);
      }
    }
    
    if (oppIds.isEmpty()) {
      return;
    }
    
    OpportunityTriggerHandler.hasCheckedExitCriteria = true; // Case 02181863 - Bypass exit criteria checks
    
    List<Opportunity> oppsToUpdate = [
      SELECT Id, Competitor_name_s__c , Competitor_Count__c,
        (SELECT Competitor_name__c FROM Competitors__r) 
      FROM Opportunity 
      WHERE Id IN :oppIds
    ];
    
    for (Opportunity opp : oppsToUpdate) {
      List<Competitor__c> oppComps = opp.Competitors__r;
      if (oppComps == null || (oppComps != null && oppComps.isEmpty())) {
        opp.Competitor_name_s__c = null;
      }
      if (oppComps != null && !oppComps.isEmpty()) {
        Set<String> oppNames = new Set<String>();
        for (Competitor__c com : oppComps) {
          oppNames.add(com.Competitor_name__c);
        }
        List<String> compNameList = new List<String>(oppNames);
        compNameList.sort();
        opp.Competitor_name_s__c = String.join(compNameList,', ');
      }
    }
    
    update oppsToUpdate;
    
  }
  
  
  //TP: Call function to update the FirstProductFuseTagId__c on the Opportunity if its Blank. else Dont do anything.  
  private static void updateCompetitorFuseTagId (Map<Id, Competitor__c> newMap) {
    
    //Create List of OptyProd Lines for which Opty does not have FuseCompetitorTag__c
    Map<Competitor__c, Opportunity> CompetitorToOptyMap = new Map<Competitor__c, Opportunity>();       
    
    for (Competitor__c CompeRec : [SELECT Id, Account__r.FUSE_Competitor_Name__c, Opportunity__r.FuseCompetitorTag__c
                                   FROM Competitor__c 
                                   WHERE Opportunity__r.FuseCompetitorTag__c = null
                                   AND Id IN :newMap.keySet()]) {
      CompetitorToOptyMap.put(CompeRec,CompeRec.Opportunity__r);            
    }

    List<Opportunity> OptyListToBeUpdated = new List<Opportunity>();

    for (Competitor__c CompeRectmp : CompetitorToOptyMap.keySet()) {
      if (CompetitorToOptyMap.get(CompeRectmp) != null) {
        OptyListToBeUpdated.add(CompetitorToOptyMap.get(CompeRectmp));
        CompetitorToOptyMap.get(CompeRectmp).FuseCompetitorTag__c= CompeRectmp.Account__r.FUSE_Competitor_Name__c;     
      }
    }
      
    if (!OptyListToBeUpdated.isEmpty()) {
      OpportunityTriggerHandler.hasCheckedExitCriteria = true; // Case 02181863 - Bypass exit criteria checks
      update OptyListToBeUpdated;
    }
    
  }
    
  private static void updatePrimaryCompetitor(Map<Id, Competitor__c> newMap)
  {
      Competitor__c competitorToUpdate;
      
      for(ID cId : newMap.keySet())
      {          
          if(newMap.get(cId).Primary_Competitor__c)
          {
              competitorToUpdate = newMap.get(cID);             
          }
      }
      
      if(competitorToUpdate != null)
      {
       		List<Competitor__c> listOfComps = [SELECT ID, Primary_Competitor__c FROM COMPETITOR__C 
                                               			WHERE ID != :competitorToUpdate.Id AND 
                                               				Opportunity__c =:competitorToUpdate.Opportunity__c AND
                                              				Primary_Competitor__c = true];
			
			for(Competitor__c cmp : listOfComps)
            {
                cmp.Primary_Competitor__c = false;
            }
          
          UPDATE listOfComps;      
      }            
  }
}