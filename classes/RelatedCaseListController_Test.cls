/**=====================================================================
  * Experian
  * Name: RelatedCaseListController
  * Description: Test class for RelateCaseListController
  * Created Date: March 08 2017
  * Created By: Ryan (Weijie) Hu, UCInnovation
  *
  * Date Modified      Modified By                  Description of the update
  *
  =====================================================================*/
@isTest
private class RelatedCaseListController_Test {
  
  @isTest static void testAccountPage() {
    Account account = Test_Utils.insertAccount();
    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)account);
    RelatedCaseListController controller = new RelatedCaseListController(stdController);

    Test.startTest();

    List<Case> filteredCaseList = controller.filteredCaseList;
    String selectedStatus = controller.selectedStatus;
    String selectedReason = controller.selectedReason;
    String selectedSecondaryReason = controller.selectedSecondaryReason;
    List<SelectOption> caseStatus = controller.caseStatus;
    List<SelectOption> caseReasons = controller.caseReasons;
    List<SelectOption> caseSecondaryReasons = controller.caseSecondaryReasons;

    controller.selectedReason = caseReasons.get(0).getValue();
    controller.selectedSecondaryReason = caseSecondaryReasons.get(0).getValue();
    controller.updateFilterCaseList();

    controller.selectedStatus = 'Open';
    controller.updateFilterCaseList();

    if (caseReasons.size() > 1) {
      controller.selectedReason = caseReasons.get(1).getValue();
      controller.updateFilterCaseList();
    }

    if (caseSecondaryReasons.size() > 1) {
      controller.selectedReason = caseReasons.get(0).getValue();
      controller.selectedSecondaryReason = caseSecondaryReasons.get(1).getValue();
      controller.updateFilterCaseList();
    }

    if (caseReasons.size() > 1 && caseSecondaryReasons.size() > 1) {
      controller.selectedReason = caseReasons.get(1).getValue();
      controller.selectedSecondaryReason = caseSecondaryReasons.get(1).getValue();
      controller.updateFilterCaseList();
    }

    Test.stopTest();
  }

  @isTest static void testContactPage() {
    Account account = Test_Utils.insertAccount();
    Contact contact = Test_Utils.createContact(account.Id);
    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)contact);
    RelatedCaseListController controller = new RelatedCaseListController(stdController);
  
    Test.startTest();

    List<Case> filteredCaseList = controller.filteredCaseList;
    String selectedStatus = controller.selectedStatus;
    String selectedReason = controller.selectedReason;
    String selectedSecondaryReason = controller.selectedSecondaryReason;
    List<SelectOption> caseStatus = controller.caseStatus;
    List<SelectOption> caseReasons = controller.caseReasons;
    List<SelectOption> caseSecondaryReasons = controller.caseSecondaryReasons;

    controller.selectedReason = caseReasons.get(0).getValue();
    controller.selectedSecondaryReason = caseSecondaryReasons.get(0).getValue();
    controller.updateFilterCaseList();

    controller.selectedStatus = 'Open';
    controller.updateFilterCaseList();

    if (caseReasons.size() > 1) {
      controller.selectedReason = caseReasons.get(1).getValue();
      controller.updateFilterCaseList();
    }

    if (caseSecondaryReasons.size() > 1) {
      controller.selectedReason = caseReasons.get(0).getValue();
      controller.selectedSecondaryReason = caseSecondaryReasons.get(1).getValue();
      controller.updateFilterCaseList();
    }

    if (caseReasons.size() > 1 && caseSecondaryReasons.size() > 1) {
      controller.selectedReason = caseReasons.get(1).getValue();
      controller.selectedSecondaryReason = caseSecondaryReasons.get(1).getValue();
      controller.updateFilterCaseList();
    }

    Test.stopTest();
  }

  @isTest static void testNoSObjectPage() {
    Opportunity sobj = new Opportunity(Name = 'test');
    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)sobj);
    RelatedCaseListController controller = new RelatedCaseListController(stdController);
    
    Test.startTest();
    controller.updateFilterCaseList();
    Test.stopTest();
  }

  @isTest static void testControlPage() {
    Account account = Test_Utils.insertAccount();
    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)account);
    RelatedCaseListController controller = new RelatedCaseListController(stdController);

    Test.startTest();
    controller.updateFilterCaseList();
    Boolean testingBoolean;
    testingBoolean = controller.hasNext;
    testingBoolean = controller.hasPrevious;
    Integer testingInt;
    testingInt = controller.pageNumber;
    testingInt = controller.totalNumber;

    controller.first();
    controller.last();
    controller.previous();
    controller.next();
    Test.stopTest();
  }
}