/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/contracts/export/*')
global class SalesforceExportService {
    global SalesforceExportService() {

    }
    @HttpGet
    global static List<seal.SalesforceExportService.SalesforceExportServiceBaseElement> httpGet_GetFiles() {
        return null;
    }
    @HttpPost
    global static List<seal.SalesforceExportService.SealImportResultResponse> httpPost_UpdateExportStatus() {
        return null;
    }
global interface SalesforceExportServiceBaseElement {
}
global class SealImportResultResponse {
    global SealImportResultResponse() {

    }
}
}
