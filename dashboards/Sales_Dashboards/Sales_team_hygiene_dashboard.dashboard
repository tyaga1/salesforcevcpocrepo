<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>APAC</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>EMEA</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Latin America</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>North America</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I</values>
        </dashboardFilterOptions>
        <name>Opportunity owner region</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Credit Services,Decision Analytics</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Credit Services</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Decision Analytics</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Consumer Services</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LATAM Serasa Sales</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Marketing Services</values>
        </dashboardFilterOptions>
        <name>User GBL</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>User.Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Opportunity_Count__c</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the Sales teams with the most overdue opportunities.</footer>
            <header>Overdue leaderboard</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/HD01_Overdue_opportunities_Sales_team</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Sales teams with most overdue opps</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>User.Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Opportunity_DQ_Score__c</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueAscending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Opportunity_Count__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the Sales teams with the lowest average Opportunity DQ score.  The RAG indicator benchmarks are RED for an average DQ score under 75%, AMBER for an average DQ score between 75% and 90%, GREEN for an average DQ score of over 90%.</footer>
            <header>Opportunity DQ score</header>
            <indicatorBreakpoint1>75.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>90.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C28B54</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/HTD13_Opportunity_DQ_Sales_team</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Sales teams with lowest average Op DQ</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Account.Contacts.Contact_Teams__r$Relationship_Owner__c.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Account.Contacts.Contact_Teams__r$Relationship_Owner__c.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>Account.Contacts.Contact_Teams__r$Relationship_Owner__c.Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Account.Contacts$Contact_DQ_Score__c</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueAscending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component shows the Sales teams with the lowest average contact DQ score.  The RAG indicator benchmarks are RED for an average DQ score under 75%, AMBER for an average DQ score between 75% and 90%, GREEN for an average DQ score of over 90%.</footer>
            <header>Contact DQ score</header>
            <indicatorBreakpoint1>75.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>90.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/Contact_DQ_score_Sales_team</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Teams with lowest average contact DQ</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>User.Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Opportunity_Count__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the Sales teams with the most stalled new business opportunities. These are identified to be closing in the next 3 months but have not moved sales stage in the last 60 days. Only opportunities created since 18th May 2015 are included.</footer>
            <header>Stalled new business opps closing within 3 months</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/HD04_Stalled_opportunities</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Sales teams with most stalled opps</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>User.Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Opportunity_Count__c</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the Sales teams with the most opportunities that have revenue scheduled for a date that is before the opportunity&apos;s close date. These opportunities need to have the revenue schedules amended.</footer>
            <header>Open opps with misaligned revenue schedules</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/HTD11_Misaligned_SR</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Sales teams with most misaligned opps</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>User.Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Opportunity_Count__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the Sales teams with the most TCV in overdue opportunities.</footer>
            <header>Overdue leaderboard</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/HD01_Overdue_opportunities_Sales_team</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Sales teams with most overdue revenue</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>User.Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Opportunity_Count__c</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the Sales teams with the most New Business opportunities closing in at least 3 financial years&apos; time.</footer>
            <header>New business opportunities closing in more than 3 FYs time</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/HD06_Future_opportunities</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Sales teams with most opps in the future</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>User.Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Opportunity_Count__c</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the Sales teams with the most late stage opportunities that have products but are valued at under US$100.</footer>
            <header>Late stage opportunities worth less than US $100</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/HD08_LS_opps_w_prods_TCV_USD100</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Sales teams with most opps</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>User.Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Opportunity_Count__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA1</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the Sales teams with the most products in at least the Propose stage with no revenue schedule established. A revenue schedule needs to be established for all of these products and opportunities.</footer>
            <header>Stage 4+ opps with products missing revenue schedules</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/HTD12_Products_w_o_revenue_schedule</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Sales teams with most products</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>User.Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Opps owned by inactive users</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Dashboard_reports/HTD14_Opps_owned_by_inactive_users</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>ST w/ most Opps owned by inactive users</title>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>User.Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Opportunity_Count__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Current_FY_revenue_impact__c.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the Sales teams with the greatest FY revenue for their overdue opportunities.</footer>
            <header>Overdue leaderboard</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/HD07_Overdue_opportunities_Sales_team</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Sales teams with most FY revenue impact</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>User.Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Opportunity_Count__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Number_of_Times_Close_Date_Moved_Back__c</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the Sales teams with the most open opportunities with close date pushbacks.</footer>
            <header>Open opportunities with most pushbacks</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/HD05_Opps_w_most_pushbacks</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Sales teams with most pushbacks</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>User.Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Opportunity_Count__c</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the Sales teams with the greatest number of new business opportunities that were created at least 3 financial years ago.</footer>
            <header>Oldest new business opportunities created at least 3 FYs ago</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/HD08_Oldest_opportunities</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Sales teams with most opps</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>User.Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Opportunity_Count__c</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the Sales teams with the most open opportunities owned by dummy Data users. These opportunities need to be moved to an actual salespersons to be actively managed, when possible.</footer>
            <header>Opportunities owned by dummy Data users</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/HD10_Opps_owned_by_Data_user</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Sales teams with most opps</title>
        </components>
    </rightSection>
    <runningUser>gareth.lee@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>Sales team hygiene dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
