<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New,New Business,New From Existing,New From New</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Existing Business,Renewal</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New From Existing</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New From New</values>
        </dashboardFilterOptions>
        <name>Opportunity type</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>APAC</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>EMEA</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>North America</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I</values>
        </dashboardFilterOptions>
        <name>Region</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_FISCAL_YEAR</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LAST_N_MONTHS:12,THIS_MONTH</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_MONTH,LAST_N_MONTHS:11</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_FISCAL_QUARTER</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LAST_FISCAL_QUARTER</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_MONTH</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LAST_MONTH</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LAST_FISCAL_YEAR</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_MONTH,NEXT_N_MONTHS:12</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_MONTH,NEXT_N_MONTHS:11</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NEXT_MONTH</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NEXT_QUARTER</values>
        </dashboardFilterOptions>
        <name>Close date range</name>
    </dashboardFilters>
    <dashboardType>MyTeamUser</dashboardType>
    <description>Dashboard is designed to give a high level global overview of quoting data for the Experian Data Quality business unit.</description>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Opportunity$Type</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$CloseDate</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the number of quotes and the number of opportunities won that are created each month. # of quotes takes in to account both open and closed opportunities.</footer>
            <groupingColumn>Opportunity.Quotes__r$CreatedDate</groupingColumn>
            <header># of quotes created and opportunities won</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ73_of_Quotes_Created_V2</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by quote created date</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y2</axisBinding>
                <column>FORMULA3</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <dashboardFilterColumns>
                <column>Opportunity$Type</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$CloseDate</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the number of quotes and average number of quotes per a won opportunity by deal size. The deal size bracket is based on the EDQ margin field.</footer>
            <groupingColumn>BucketField_89571187</groupingColumn>
            <header># of quotes and average # of quotes per won opportunity</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ76_of_Quotes_Deal_Size</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by deal size</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.EDQ_Margin__c.CONVERT</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows top 10 product master names by region based on margin. Data is based on open opportunities.</footer>
            <groupingColumn>Product2.DE_Product_Name__c</groupingColumn>
            <groupingColumn>Opportunity.Owner_s_Region__c</groupingColumn>
            <header>Top 10 pipeline product master names by region</header>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/CPQ87_Top_10_p_l_by_region</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>based on margin</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>ColumnGrouped</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average deal size of won opportunities by opportunity close date. New business and renewal business is separated in the chart. Deal size is based on the EDQ margin field.</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <groupingColumn>BucketField_94440427</groupingColumn>
            <header>Average deal size</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ39_Average_Deal_Size_by_Close_Date</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by close date</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA3</column>
            </chartSummary>
            <componentType>ColumnGrouped</componentType>
            <dashboardFilterColumns>
                <column>Opportunity$Type</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$CloseDate</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component is designed to show the number of quotes created in each region by business type.</footer>
            <groupingColumn>Opportunity$Owner_s_Region__c</groupingColumn>
            <groupingColumn>BucketField_96159455</groupingColumn>
            <header># of quotes</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ75_of_Quotes_Region</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by region and type</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Opportunity$Type</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$CloseDate</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average number of quotes created per a won opportunity by opportunity close date.</footer>
            <groupingColumn>Opportunity$CloseDate</groupingColumn>
            <header>Average # of quotes per won opportunity</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ104_Avg_quotes_deal_size_C_Date</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by close date</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.EDQ_Margin__c.CONVERT</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows top 10 product master names by region based on margin. Data is based on won opportunities.</footer>
            <groupingColumn>Product2.DE_Product_Name__c</groupingColumn>
            <groupingColumn>Opportunity.Owner_s_Region__c</groupingColumn>
            <header>Top 10 won product master names by region</header>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/CPQ88_Top_10_performing_prods_by_region</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>based on margin</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the win rate for closed opportunities by opportunity close date.</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <header>Win rate</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ54_Win_Loss_Rates</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by close date</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_3__c</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_4__c</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_5__c</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_6__c</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average amount of days spent in each of the sales stages by region. Data is based on won opportunities.</footer>
            <groupingColumn>Opportunity.Owner_s_Region__c</groupingColumn>
            <header>Average days spent in each of the sales stages by region</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ43_Sales_Stage_Velocity_V2</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>for won opportunities</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Opportunity$Type</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$CloseDate</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average number of quotes created per an opportunity by opportunity status. Data is based on closed opportunities only.</footer>
            <groupingColumn>BucketField_18704863</groupingColumn>
            <header>Average # of quotes per opportunity</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ74_Avg_quotes_deal_size_C_Date</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by opportunity status</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>alexander.lethbridge@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>EDQ CPQ Dashboard - Global - US$</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
