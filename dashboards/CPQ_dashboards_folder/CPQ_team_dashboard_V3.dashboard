<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New,New Business,New From Existing,New From New</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Existing Business,Renewal</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New From Existing</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New From New</values>
        </dashboardFilterOptions>
        <name>Opportunity type</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>APAC</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>EMEA</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>North America</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I</values>
        </dashboardFilterOptions>
        <name>Region</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>lessThan</operator>
            <values>GBP 10000</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>between</operator>
            <values>GBP 10000</values>
            <values>GBP 25000</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>between</operator>
            <values>GBP 25000</values>
            <values>GBP 50000</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>greaterThan</operator>
            <values>GBP 50000</values>
        </dashboardFilterOptions>
        <name>Margin Tiers</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.EDQ_Margin__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the number of quotes created each month. Record count refers to the number of quotes created.</footer>
            <groupingColumn>Opportunity.Quote_Created_Date__c</groupingColumn>
            <header>Quotes created</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ37_Number_of_Quotes_AL</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Number of quotes created.</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.EDQ_Margin__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average deal size of won opportunities for opportunities closing from the 1st of July to the current date.</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <header>Average Deal Size</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ39_Average_Deal_Size_by_Close_Date</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by Close Date</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.CPQ_Discount__c</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.EDQ_Margin__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average sub-sales team discount percentage.</footer>
            <groupingColumn>Opportunity.Owner_Sales_Sub_Team_on_Opp_Close_Date__c</groupingColumn>
            <header>Average Team Discount %</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ40_Average_Discount_by_Team</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Based on Sub-Sales Team</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_4__c</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.EDQ_Margin__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows average time spent in the proposed stage by sub-sales team.</footer>
            <groupingColumn>User.Sales_Sub_Team__c</groupingColumn>
            <header>Time Spent in Proposed Stage</header>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>25</maxValuesDisplayed>
            <report>Dashboard_reports/CPQ43_Sales_Stage_Velocity_V2</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>by Sub-Sales Team</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.CPQ_Discount__c</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.EDQ_Margin__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average discount by industry.</footer>
            <groupingColumn>INDUSTRY</groupingColumn>
            <header>Average Discount by Industry</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ44_Discounting_by_Industry</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Ranked by Discount</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.EDQ_Margin__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the range of discount %&apos;s given for open opportunity products, split by Sales stage. Record count refers to the number of open products.</footer>
            <groupingColumn>BucketField_74227530</groupingColumn>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Discounting</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ38_Discounting_of_open_opps</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Discount % for open products</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.EDQ_Margin__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows top 10 most discounted products for won opps</footer>
            <header>Discount %</header>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/CPQ36_Top_10_Discounted_Prods</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Top 10 discounted prods for won opps</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.EDQ_Margin__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component shows the range of discount %&apos;s given for closed won opportunity products, split by Sales stage. Record count refers to the number of closed won opportunities</footer>
            <header>Discounting</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Dashboard_reports/CPQ38a_Discounting_of_open_opps</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowLabelDescending</sortBy>
            <title>Discount % count for won products</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.EDQ_Margin__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows top 10 best performing products based on margin YTD for won opps</footer>
            <header>Top 10 products for won opps</header>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/CPQ32_Top_10_Prods_for_won_opps</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Current FY</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.Discount_Amount__c</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.EDQ_Margin__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the total sub-sales team discount amount..</footer>
            <groupingColumn>Opportunity.Owner_Sales_Sub_Team_on_Opp_Close_Date__c</groupingColumn>
            <header>Total Team Discount Amount</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ40_Average_Discount_by_Team</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Based on Sub-Sales Team</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.EDQ_Margin__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows top 10 best performing products based on margin for open opps</footer>
            <header>Top 10 products for open opps</header>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/CPQ33_Top_10_Prods_for_open_opps</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Current FY</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.Discount_Amount__c</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.EDQ_Margin__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows top 10 best performing products based on margin for open opps</footer>
            <groupingColumn>PRODUCT_NAME</groupingColumn>
            <header>Discount amount</header>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/CPQ36_Top_10_Discounted_Prods</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Top 10 discounted prods for won opps</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.EDQ_Margin__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the range of discount %&apos;s given for closed won opportunity products, split by Sales stage. Record count refers to the number of closed won opportunities</footer>
            <groupingColumn>BucketField_74227530</groupingColumn>
            <header>Discounting</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ38a_Discounting_of_open_opps</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Discount % count for won products</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.EDQ_Margin__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>PRODUCT_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.EDQ_Margin__c.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.EDQ_Margin__c.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component shows top 10 best performing products based on margin YTD for won opps</footer>
            <header>Top 10 products for won opps</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/CPQ32_Top_10_Prods_for_won_opps</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Current FY</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.CPQ_Discount__c</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.EDQ_Margin__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average discount for the top 25 discounting sales people.</footer>
            <groupingColumn>FULL_NAME</groupingColumn>
            <header>Average Salesperson Discount%</header>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>20</maxValuesDisplayed>
            <report>Dashboard_reports/CPQ41_Average_Discount_by_Salesperson</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Top 25 Discounting Sales People</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>alexander.lethbridge@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>CPQ team dashboard V3</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
