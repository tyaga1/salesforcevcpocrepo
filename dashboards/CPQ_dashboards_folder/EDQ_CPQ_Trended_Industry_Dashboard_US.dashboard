<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New,New Business,New From Existing,New From New</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Existing Business,Renewal</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New From Existing</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New From New</values>
        </dashboardFilterOptions>
        <name>Opportunity type</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>APAC</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>EMEA</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>North America</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I</values>
        </dashboardFilterOptions>
        <name>Region</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component is designed to see if the average unit price is changing over time.</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <groupingColumn>INDUSTRY</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ60_Trended_Unit_Price_by_Industry</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Average Unit Price</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>INDUSTRY</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Count_of_Products__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA2</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component shows the unit price by industry for won opportunities.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Dashboard_reports/CPQ50_Avg_Unit_Price_by_Industry</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Average Unit Price</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component is designed to show the change in discount amount over time.</footer>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ62_Trended_Discount_by_Industry</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Discount Amount</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.CPQ_Discount__c</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average discount by industry for won opportunities.</footer>
            <groupingColumn>INDUSTRY</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ44_Discounting_by_Industry</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Average Discount</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.EDQ_Margin__c.CONVERT</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Region__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component is designed to show the change in margin over time.</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <groupingColumn>INDUSTRY</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CPQ61_Trended_Margin_by_Industry</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Margin</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>alexander.lethbridge@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>EDQ CPQ Trended Industry Dashboard - US$</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
