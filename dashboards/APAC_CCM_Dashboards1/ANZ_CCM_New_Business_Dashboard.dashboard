<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Brad Moult,Mark Ferrar,Richard Hartshorn,Patrick Johnson,David Mason,Shoji Takayasu,Lyall Sundel,Trevor Taylor</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Lee Coats,Fleur Ryder,Ian Weide,Ryan Jenkins,Anthony Lansdown,Vic Hodge,Gunkaran Singh</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Sue Winny,Dean Andrews</values>
        </dashboardFilterOptions>
        <name>Opportunity Owner</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Funnel</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <header>120 day Rolling Pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_CCM_reports/ANZ_CCM_NB_90_day_rolling_Weighted_P_L</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>120 Day Pipeline</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <header>Total 120 Day Rolling Pipeline</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total 120 Day Rolling Pipeline</metricLabel>
            <report>APAC_CCM_reports/ANZ_CCM_NB_90_day_rolling_Weighted_P_L</report>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <gaugeMax>400000.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>New Business Won this Month</header>
            <indicatorBreakpoint1>1.0</indicatorBreakpoint1>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C25454</indicatorMiddleColor>
            <report>APAC_CCM_reports/APAC_CCM_Closed_Won_this_Month</report>
            <showPercentage>true</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>NB Won This Month</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <header>Renewals - Forecast this Month (Contract or Execute)</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_CCM_reports/ANZ_CCM_Accounts_Renewals_Monthly</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Renewals FC this Month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Top 10 Highest TCV value Deals this FQ</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>APAC_CCM_reports/ANZ_CCM_NB_Top_Revenue_Opps_this_FQ</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
            <title>Top 10 opps this FQ</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>FULL_NAME</groupingColumn>
            <header>Opportunities Created this Month</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_CCM_reports/ANZ_CCM_Opps_Created_per_Month</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Opps created this Month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>BucketField_92950800</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Opportunities which form broader EMS or Key Account deals</footer>
            <header>ANZ - OneEMS and KeyAccount Opportunities</header>
            <indicatorBreakpoint1>500000.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>1000000.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>User_created_reports/ANZ_KeyAccounts_or_OneEMS_deals_New</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>OneEMS and KeyAccount Opps</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>EXP_AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>FULL_NAME</groupingColumn>
            <header>120 day Expected TCV pipeline by Salesperson</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_CCM_reports/APAC_CCM_90_Day_Expected_TCV_by_Owner</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>120 day expected TCV pipeline</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>EXP_AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <header>Total 120 day expected TCV pipeline</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total 120 Day Expected TCV</metricLabel>
            <report>APAC_CCM_reports/APAC_CCM_90_Day_Expected_TCV_by_Owner</report>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <gaugeMax>1200000.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>New Business  Won this FQ</header>
            <indicatorBreakpoint2>1.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C25454</indicatorMiddleColor>
            <report>APAC_CCM_reports/APAC_CCM_NB_Closed_Won_this_FQ</report>
            <showPercentage>true</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>NB Won this FQ</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>CLOSE_MONTH</groupingColumn>
            <groupingColumn>ACCOUNT_NAME</groupingColumn>
            <header>Renewals - Due this FQ</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_CCM_reports/ANZ_CCM_Accounts_RNLs_due_this_FQ</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Renewals due this FQ</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Top 10 Highest TCV value Deals Next FQ</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>APAC_CCM_reports/ANZ_CCM_NB_Top_Revenue_opps_Next_FQ</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
            <title>Top 10 opps next FQ</title>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>ACCOUNT_NAME</groupingColumn>
            <header>New Business Deals Forecast this Month</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_CCM_reports/ANZ_CCM_NB_Deals_left_in_forecast</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>New Business Forecast this Month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <header>Total Deals Forecast this Month</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total Deals Forecast this Month</metricLabel>
            <report>APAC_CCM_reports/ANZ_CCM_NB_Deals_left_in_forecast</report>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <gaugeMax>4800000.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>New Business Won this FY</header>
            <indicatorBreakpoint2>1.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C25454</indicatorMiddleColor>
            <report>APAC_CCM_reports/APAC_CCM_NB_Closed_Won_this_FY</report>
            <showPercentage>true</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>NB Won this FY</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>CLOSE_MONTH</groupingColumn>
            <groupingColumn>ACCOUNT_NAME</groupingColumn>
            <header>Renewals - Due this FY</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_CCM_reports/ANZ_CCM_Accounts_Renewals_FY1617</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Renewals due this FY</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Top 10 Highest TCV value Deals this FY</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>APAC_CCM_reports/ANZ_CCM_NB_Top_Revenue_opps_this_FY</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
            <title>Top 10 opps this FY</title>
        </components>
    </rightSection>
    <runningUser>jason.chan@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>APAC ANZ - CCM Sales Dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
