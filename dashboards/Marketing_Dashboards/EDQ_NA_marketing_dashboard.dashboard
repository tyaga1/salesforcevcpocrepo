<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Funnel</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component shows the open pipeline for opportunities from marketing campaigns, split by Sales Stage. Only opportunities closing in the current month are included.</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>EDQ_NA_marketing_reports/EDQ170_NA_open_opps_f_campaigns_CM</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Opps closing in the current month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Order__c.Total__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y2</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows won revenue from orders linked to campaigns. Only orders closed in the current month are included.</footer>
            <groupingColumn>Order__c.Owner_BU_on_Order_Create_Date__c</groupingColumn>
            <header>Won orders - Marketing influence</header>
            <legendPosition>Bottom</legendPosition>
            <report>EDQ_NA_marketing_reports/EDQ223_NA_orders_f_campaigns_CM</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Orders closed in the current month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Order__c.Total__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y2</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>his component shows won revenue from orders linked to campaigns. Only orders closed in the current month are included.</footer>
            <groupingColumn>Order__c.Owner_BU_on_Order_Create_Date__c</groupingColumn>
            <header>Won orders</header>
            <legendPosition>Bottom</legendPosition>
            <report>EDQ_NA_marketing_reports/EDQ226_All_NA_orders_f_campaigns_CM</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>All orders closed in the current month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the number of marketing tasks created by each user in the current month. Record count refers to the number of tasks created.</footer>
            <groupingColumn>Activity$CreatedBy</groupingColumn>
            <groupingColumn>Activity$Status</groupingColumn>
            <header>Marketing tasks created</header>
            <legendPosition>Bottom</legendPosition>
            <report>EDQ_NA_marketing_reports/EDQ229_NA_EDQ_marketing_tasks_CM</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current month</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Funnel</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component shows the open pipeline for opportunities from marketing campaigns, split by Sales Stage. Only opportunities closing in the current financial quarter are included.</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>EDQ_NA_marketing_reports/EDQ221_NA_open_opps_f_campaigns_CM</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Opps closing in the current FQ</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Order__c.Total__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y2</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows won revenue from orders linked to campaigns. Only orders closed in the current financial quarter are included.</footer>
            <groupingColumn>Order__c.Close_Date__c</groupingColumn>
            <groupingColumn>Order__c.Owner_BU_on_Order_Create_Date__c</groupingColumn>
            <header>Won orders - Marketing influence</header>
            <legendPosition>Bottom</legendPosition>
            <report>EDQ_NA_marketing_reports/EDQ224_NA_orders_f_campaigns_FQ</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Orders closed in the current FQ</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Order__c.Total__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y2</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows won revenue from orders linked to campaigns. Only orders closed in the current financial quarter are included.</footer>
            <groupingColumn>Order__c.Close_Date__c</groupingColumn>
            <groupingColumn>Order__c.Owner_BU_on_Order_Create_Date__c</groupingColumn>
            <header>Won orders</header>
            <legendPosition>Bottom</legendPosition>
            <report>EDQ_NA_marketing_reports/EDQ227_All_NA_orders_f_campaigns_FQ</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>All orders closed in the current FQ</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the number of marketing tasks created by each user in the current financial quarter. Record count refers to the number of tasks created.</footer>
            <groupingColumn>Activity$CreatedBy</groupingColumn>
            <groupingColumn>Activity$Status</groupingColumn>
            <header>Marketing tasks created</header>
            <legendPosition>Bottom</legendPosition>
            <report>EDQ_NA_marketing_reports/EDQ230_NA_EDQ_marketing_tasks_FQ1</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current FQ</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Funnel</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component shows the open pipeline for opportunities from marketing campaigns, split by Sales Stage. Only opportunities closing in the current financial year are included.</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>EDQ_NA_marketing_reports/EDQ222_NA_open_opps_f_campaigns_FY</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Opps closing in the current FY</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Order__c.Total__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y2</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows won revenue from orders linked to campaigns. Only orders closed in the current financial year are included.</footer>
            <groupingColumn>Order__c.Close_Date__c</groupingColumn>
            <groupingColumn>Order__c.Owner_BU_on_Order_Create_Date__c</groupingColumn>
            <header>Won orders - Marketing influence</header>
            <legendPosition>Bottom</legendPosition>
            <report>EDQ_NA_marketing_reports/EDQ225_NA_orders_f_campaigns_FY</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Orders closed in the current FY</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Order__c.Total__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y2</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows won revenue from orders linked to campaigns. Only orders closed in the current financial year are included.</footer>
            <groupingColumn>Order__c.Close_Date__c</groupingColumn>
            <groupingColumn>Order__c.Owner_BU_on_Order_Create_Date__c</groupingColumn>
            <header>Won orders</header>
            <legendPosition>Bottom</legendPosition>
            <report>EDQ_NA_marketing_reports/EDQ228_All_NA_orders_f_campaigns_FY</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>All orders closed in the current FY</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the number of marketing tasks created by each user in the current financial year. Record count refers to the number of tasks created.</footer>
            <groupingColumn>Activity$CreatedBy</groupingColumn>
            <groupingColumn>Activity$Status</groupingColumn>
            <header>Marketing tasks created</header>
            <legendPosition>Bottom</legendPosition>
            <report>EDQ_NA_marketing_reports/EDQ231_NA_EDQ_marketing_tasks_FY1</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current FY</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>gareth.lee@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>EDQ NA marketing dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
