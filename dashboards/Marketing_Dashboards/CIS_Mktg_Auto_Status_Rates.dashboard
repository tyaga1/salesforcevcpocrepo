<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <description>Lead Status rates for Leads generated via the Marketing Automation tool created during the previous and current Fiscal Year.</description>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Manual</chartAxisRange>
            <chartAxisRangeMax>100.0</chartAxisRangeMax>
            <chartAxisRangeMin>1.0</chartAxisRangeMin>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Leads created with the Marketing Automation tool and the status Rate per Month</footer>
            <header>Prev&amp;Current FY Leads and Status Rate</header>
            <legendPosition>Bottom</legendPosition>
            <report>Web_Analytics_Mktg_Automation_Staging/Prev_Currnt_FY_CIS_Mkt_Aut_StatsRate_Mon</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Disqualified Status Rate per month for Leads created with the Marketing Automation tool</footer>
            <header>Prev&amp;Current FY Leads and Disqualified Status Rate per Month</header>
            <legendPosition>Bottom</legendPosition>
            <report>Web_Analytics_Mktg_Automation_Staging/Prev_Cur_FY_CIS_Mkt_Auto_Lead_Disq_Rate</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Sales Accepted Status Rate per month for Leads created with the Marketing Automation tool</footer>
            <header>Prev&amp;Current FY Leads and Sales Accepted Status Rate per Month</header>
            <legendPosition>Bottom</legendPosition>
            <report>Web_Analytics_Mktg_Automation_Staging/Prev_Cur_FY_CIS_Mkt_Auto_Lead_S_A_Rate</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Converted Status Rate per month for Leads created with the Marketing Automation tool</footer>
            <header>Prev&amp;Current FY Leads and Converted Status Rate per Month</header>
            <legendPosition>Bottom</legendPosition>
            <report>Web_Analytics_Mktg_Automation_Staging/Prev_Cur_FY_CIS_Mkt_Auto_Lead_Conv_Rate</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Qualifying Status Rate per month for Leads created with the Marketing Automation tool</footer>
            <header>Prev&amp;Current FY Leads and Qualifying Status Rate per Month</header>
            <legendPosition>Bottom</legendPosition>
            <report>Web_Analytics_Mktg_Automation_Staging/Prev_Cur_FY_CIS_Mkt_Auto_Lead_Qual_Rate</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Open Status Rate per month for Leads created with the Marketing Automation tool</footer>
            <header>Prev&amp;Current FY Leads and Open Status Rate per Month</header>
            <legendPosition>Bottom</legendPosition>
            <report>Web_Analytics_Mktg_Automation_Staging/Prev_Cur_FY_CIS_Mkt_Auto_Lead_Open_Rate</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Rejected Status Rate per month for Leads created with the Marketing Automation tool</footer>
            <header>Prev&amp;Current FY Leads and Rejected Status Rate per Month</header>
            <legendPosition>Bottom</legendPosition>
            <report>Web_Analytics_Mktg_Automation_Staging/Prev_Cur_FY_CIS_Mkt_Auto_Lead_Rej_Rate</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
    </rightSection>
    <runningUser>nicholas.hopkins@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>CIS Mktg-Auto Status Rates</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
