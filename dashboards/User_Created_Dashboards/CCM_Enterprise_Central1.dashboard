<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>TopToBottom</backgroundFadeDirection>
    <backgroundStartColor>#9999FF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>STAGE_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Thousands</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows a sales team&apos;s top 5 open opportunities that are expected to close in the next month.</footer>
            <header>Opportunities expected to close</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>5</maxValuesDisplayed>
            <report>Sales/Sales_team_Open_pipeline_Next_month</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Next month - Top 5</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>REVENUE_AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Funnel</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component show the amount of scheduled revenue of open opportunities for the current month, split by the current sales stage.</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Scheduled revenue for current month</header>
            <legendPosition>Bottom</legendPosition>
            <report>Finance/X4_47_Finance_Team_MTD_open_and_won_SR</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Open opportunities</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>ColumnStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows a sales team&apos;s pipeline for opportunities expected to close in the rolling next 90 days, split by sales stage.</footer>
            <header>Pipeline - TCV of open opportunities</header>
            <legendPosition>Bottom</legendPosition>
            <report>Sales/Sales_Team_Pipeline_90_days_SS1</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Rolling next 90 days</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>STAGE_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Thousands</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows a sales team&apos;s top 5 open opportunities that are expected to close in the current month.</footer>
            <header>Opportunities expected to close</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>5</maxValuesDisplayed>
            <report>Sales/Sales_team_Open_pipeline_This_month</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Current month - Top 5</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>Order__c$Owner</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Order__c.Order_Line_Items__r.Order_Revenue_Schedules__r$Revenue__c.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows a salesperson&apos;s won scheduled revenue for the current financial year. The total amount shows the amount of a sales team&apos;s scheduled revenue for the current financial year.</footer>
            <header>Salesperson won scheduled revenue leaderboard</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Finance/X4_50</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Current financial year</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Order__c$Total__c.CONVERT</column>
            </chartSummary>
            <componentType>BarStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the current year&apos;s salesperson leaderboard for a sales team. The leaderboard is ranked by won TCV.</footer>
            <groupingColumn>Order__c$Owner</groupingColumn>
            <groupingColumn>Order__c$Type__c</groupingColumn>
            <header>Salesperson TCV leaderboard</header>
            <legendPosition>Bottom</legendPosition>
            <report>Sales/Sales_Team_YTD_Person_won_orders</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>YTD - Closed revenue leaderboard</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>Order__c$Owner</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Order__c$Total__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the current month&apos;s salesperson &apos;new business&apos; leaderboard for a sales team. The &apos;new business&apos; leaderboard is ranked by won TCV. In this component, record count refers to the number of won &apos;new business&apos; orders.</footer>
            <header>Won &apos;New business&apos; TCV leaderboard</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Sales/Sales_Team_MTD_NB_orders_leaderboard</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Current month</title>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>Order__c$Opportunity__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>Order__c$Account__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Order__c$Total__c.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows a sales team&apos;s top 5 won orders for the current month, ranked by TCV.</footer>
            <header>Closed won orders</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>5</maxValuesDisplayed>
            <report>Sales/Sales_Team_MTD_won_orders</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Current month - Top 5</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Line</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows a sales team&apos;s won scheduled revenue for the current financial year.</footer>
            <header>Closed won scheduled revenue</header>
            <legendPosition>Bottom</legendPosition>
            <report>Finance/Finance_Team_Order_schedules</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Won opportunities and transactionals</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>FULL_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>OPPORTUNITY_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows a sales team&apos;s top 5 overdue opportunities. These opportunities need to either be closed or have their expected close date adjusted.</footer>
            <header>Overdue opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>5</maxValuesDisplayed>
            <report>Sales/X1_22</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Close dates in need of attention - Top 5</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>Order__c$Owner</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Order__c$Total__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the current month&apos;s salesperson leaderboard for a sales team. The leaderboard is ranked by won TCV. In this component, record count refers to the number of won orders. Total amount shows the amount of a sales team&apos;s TCV.</footer>
            <header>Salesperson won TCV leaderboard</header>
            <indicatorBreakpoint1>50000.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>75000.0</indicatorBreakpoint2>
            <indicatorHighColor>#FFFFFF</indicatorHighColor>
            <indicatorLowColor>#FFFFFF</indicatorLowColor>
            <indicatorMiddleColor>#FFFFFF</indicatorMiddleColor>
            <report>Sales/Sales_Team_MTD_orders_leaderboard</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Current month</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>FULL_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>OPPORTUNITY_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Thousands</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows a sales team&apos;s top 5 new opportunities created in the current month.</footer>
            <header>New opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>5</maxValuesDisplayed>
            <report>Sales/Top_new_opps_This_month</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Current month - Top 5</title>
        </components>
    </rightSection>
    <runningUser>doreen.janikowski@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>CCM Enterprise Central</title>
    <titleColor>#666666</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
