<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Pieter van Tienen</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Philippe Mazurier</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Chris Thomas</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>Frank Papso</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Marcel Verbraak</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Jonathan Smith</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Richard Topham</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Rianne van Velzen</values>
        </dashboardFilterOptions>
        <name>Team Member Name</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>Vertical</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>Fraudnet</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values></values>
        </dashboardFilterOptions>
        <name>Team Member Sales Team</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>MEMBER_NAME</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityTeamMember.Team_Member_Sales_Team__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>MEMBER_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>OPPORTUNITY.NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.NBQ__c.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This element shows all opportunities that are set to close this quarter, with the respective NBQ value and the opportunity team member</footer>
            <header>Funnel</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>EMEA_Product_reports/EMEA_team_Close</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Top 10 to close this quarter</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.NBQ__c.CONVERT</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>MEMBER_NAME</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityTeamMember.Team_Member_Sales_Team__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>total NBQ contribution per BDM, split by forecast catagory</footer>
            <groupingColumn>MEMBER_NAME</groupingColumn>
            <groupingColumn>Opportunity.Forecast_Category__c</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>EMEA_Product_reports/EMEA_team_close_full_FY</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>NBQ contribution this year by BDM</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.NBQ__c.CONVERT</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>MEMBER_NAME</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityTeamMember.Team_Member_Sales_Team__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>total of NBQ expected in the coming months, split by forecast catagory</footer>
            <groupingColumn>CLOSE_MONTH</groupingColumn>
            <groupingColumn>Opportunity.Forecast_Category__c</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>EMEA_Product_reports/EMEA_team_close_NBQ_monthly</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>NBQ contribution this year by month</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>MEMBER_NAME</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityTeamMember.Team_Member_Sales_Team__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>MEMBER_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>OPPORTUNITY.NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.NBQ__c.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This element shows all opportunities that are set to close this the rest of this financial year, with the respective NBQ value and the opportunity team member</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>EMEA_Product_reports/EMEA_team_Close_rest_of_year</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Top to close rest of year</title>
        </components>
    </rightSection>
    <runningUser>vincent.edelkoort@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>Solution center and fraud team dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
