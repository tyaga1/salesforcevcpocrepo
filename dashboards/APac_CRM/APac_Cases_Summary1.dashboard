<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Australia</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>China</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Hong Kong</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>India</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Japan</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Malaysia</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New Zealand</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Singapore</values>
        </dashboardFilterOptions>
        <name>APAC</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Case.Requestor_Country__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Current open cases in the Sales Effectiveness APAC queue</footer>
            <header>Number of Cases Opened</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APac_CRM/Cases_Opened_Trending_Report</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowLabelAscending</sortBy>
            <title>Stacked by Requester Country</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Donut</componentType>
            <dashboardFilterColumns>
                <column>Case.Requestor_Country__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <legendPosition>Bottom</legendPosition>
            <report>APac_CRM/Cases_Opened_Trending_Report</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>Case.Requestor_Country__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <header>Sales Operations Team Assignment</header>
            <legendPosition>Bottom</legendPosition>
            <report>APac_CRM/CRM_Team_Assignment1</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current and Previous FY</title>
            <useReportChart>true</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Case.Requestor_Country__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>Case.Requestor_Country__c</column>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Closed cases - July 2014 to present</footer>
            <header>Number of Cases Closed - All-time records</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APac_CRM/APAC_Close_Cases</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Stacked by Requester Country</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Donut</componentType>
            <dashboardFilterColumns>
                <column>Case.Requestor_Country__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Case.Requestor_Country__c</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>APac_CRM/APAC_Close_Cases</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>Case$Requestor_Country__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <groupingColumn>Case$Reason</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>APac_CRM/Case_Type</report>
            <showPercentage>true</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Case Type</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Case.Requestor_Country__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>OWNER</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>SUBJECT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <header>Number of Cases Closed</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APac_CRM/Closed_Case_7_days</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Last 30 Days</title>
        </components>
    </rightSection>
    <runningUser>jackie.ooi@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>APac Cases Summary</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
