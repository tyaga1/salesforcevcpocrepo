<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Account_Name__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Order__r.Account__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>HYPERLINK( &quot;/&quot; &amp;  Order__r.Account__r.Id  ,  Order__r.Account__r.Name )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Additional_Information__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Additional Information</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Credit_Note_Owner__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Case 984094</description>
        <externalId>false</externalId>
        <label>Credit Note Owner</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Credit_Note_Request_Forms</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Credit_Type__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Choose ‘Internal’ if the client should not be re-invoiced.
Choose ‘External’ if the client should be re-invoiced.</inlineHelpText>
        <label>Credit Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Internal</fullName>
                    <default>false</default>
                    <label>Internal</label>
                </value>
                <value>
                    <fullName>External</fullName>
                    <default>false</default>
                    <label>External</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EDQ_Integration_ID__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Order__r.Account__r.EDQ_Integration_Id__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>EDQ Integration ID</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EDQ_On_Demand_Product__c</fullName>
        <externalId>false</externalId>
        <formula>Order__r.Has_EDQ_On_Demand_Products__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>EDQ On Demand Product</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Follow_On_Opportunity_Owner__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Case 984094</description>
        <externalId>false</externalId>
        <label>Follow On Opportunity Owner</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Credit_Note_Request_Forms1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Margin_Impact_Amount__c</fullName>
        <description>Used by EDQ. Rebill Margin Amount - Original Margin Amount. Case 13545993</description>
        <externalId>false</externalId>
        <formula>Rebill_Margin_Amount__c - Original_Margin_Amount__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Margin Impact Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Opportunity_ID__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Order__r.Opportunity__r.Id</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Opportunity ID</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Name__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Order__r.Opportunity__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Opportunity Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>HYPERLINK( &quot;/&quot; &amp; Order__r.Opportunity__r.Id ,  Order__r.Opportunity__r.Name )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Opportunity</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Order_Owner_Name_on_Order_Created_Date__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Order__r.Owner_Name_on_Order_Create_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Order Owner Name on Order Created Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Order_Value__c</fullName>
        <externalId>false</externalId>
        <formula>Order__r.Total__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Order Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Order__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Order</label>
        <referenceTo>Order__c</referenceTo>
        <relationshipLabel>Credit Note Request Forms</relationshipLabel>
        <relationshipName>Credit_Form</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Order_contains_On_Demand_products__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>IF(Order__r.Has_EDQ_On_Demand_Products__c =true, &quot;Yes&quot;,&quot;No&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Order contains On Demand products</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Original_Margin_Amount__c</fullName>
        <description>Used by EDQ. Looks up to Order&apos;s EDQ margin. Case 13545993</description>
        <externalId>false</externalId>
        <formula>Order__r.EDQ_Margin__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Original Margin Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Other__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Other</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Owner_Sales_SubTeam_on_Order_Create_Date__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Order__r.Owner_Sales_SubTeam_on_Order_Create_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Owner Sales SubTeam on Order Create Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Partner__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>HYPERLINK( &quot;/&quot; &amp; Order__r.Partner__r.Id ,  Order__r.Partner__r.Name )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Partner</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Reason_for_requesting_the_Credit_Note__c</fullName>
        <externalId>false</externalId>
        <label>Reason for requesting the Credit Note</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>Region__c</controllingField>
            <valueSetDefinition>
                <sorted>true</sorted>
                <value>
                    <fullName>Account Name Error</fullName>
                    <default>false</default>
                    <label>Account Name Error</label>
                </value>
                <value>
                    <fullName>Accounts Receivable</fullName>
                    <default>false</default>
                    <label>Accounts Receivable</label>
                </value>
                <value>
                    <fullName>Address Discrepancy</fullName>
                    <default>false</default>
                    <label>Address Discrepancy</label>
                </value>
                <value>
                    <fullName>Auto-Invoice</fullName>
                    <default>false</default>
                    <label>Auto-Invoice</label>
                </value>
                <value>
                    <fullName>Change Order</fullName>
                    <default>false</default>
                    <label>Change Order</label>
                </value>
                <value>
                    <fullName>Corrupted</fullName>
                    <default>false</default>
                    <label>Corrupted</label>
                </value>
                <value>
                    <fullName>Currency Error</fullName>
                    <default>false</default>
                    <label>Currency Error</label>
                </value>
                <value>
                    <fullName>Duplication</fullName>
                    <default>false</default>
                    <label>Duplication</label>
                </value>
                <value>
                    <fullName>Goodwill</fullName>
                    <default>false</default>
                    <label>Goodwill</label>
                </value>
                <value>
                    <fullName>Internal Process Error</fullName>
                    <default>false</default>
                    <label>Internal Process Error</label>
                </value>
                <value>
                    <fullName>Internal System Errors</fullName>
                    <default>false</default>
                    <label>Internal System Errors</label>
                </value>
                <value>
                    <fullName>Invoice Address</fullName>
                    <default>false</default>
                    <label>Invoice Address</label>
                </value>
                <value>
                    <fullName>Invoiced End User in Error</fullName>
                    <default>false</default>
                    <label>Invoiced End User in Error</label>
                </value>
                <value>
                    <fullName>Licence Dates</fullName>
                    <default>false</default>
                    <label>Licence Dates</label>
                </value>
                <value>
                    <fullName>Net Value</fullName>
                    <default>false</default>
                    <label>Net Value</label>
                </value>
                <value>
                    <fullName>Other (please specify)</fullName>
                    <default>false</default>
                    <label>Other (please specify)</label>
                </value>
                <value>
                    <fullName>Owner Details</fullName>
                    <default>false</default>
                    <label>Owner Details</label>
                </value>
                <value>
                    <fullName>Partner Details/Commission</fullName>
                    <default>false</default>
                    <label>Partner Details/Commission</label>
                </value>
                <value>
                    <fullName>Partner Fees and Royalties</fullName>
                    <default>false</default>
                    <label>Partner Fees and Royalties</label>
                </value>
                <value>
                    <fullName>PO Issues</fullName>
                    <default>false</default>
                    <label>PO Issues</label>
                </value>
                <value>
                    <fullName>Product Incompatible</fullName>
                    <default>false</default>
                    <label>Product Incompatible</label>
                </value>
                <value>
                    <fullName>Product Miss Selling</fullName>
                    <default>false</default>
                    <label>Product Miss Selling</label>
                </value>
                <value>
                    <fullName>Professional Services</fullName>
                    <default>false</default>
                    <label>Professional Services</label>
                </value>
                <value>
                    <fullName>Royalties</fullName>
                    <default>false</default>
                    <label>Royalties</label>
                </value>
                <value>
                    <fullName>Tax Exempt</fullName>
                    <default>false</default>
                    <label>Tax Exempt</label>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <valueName>Account Name Error</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>EMEA</controllingFieldValue>
                <controllingFieldValue>UK</controllingFieldValue>
                <valueName>Auto-Invoice</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>EMEA</controllingFieldValue>
                <controllingFieldValue>UK</controllingFieldValue>
                <valueName>Corrupted</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <valueName>Currency Error</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>EMEA</controllingFieldValue>
                <controllingFieldValue>UK</controllingFieldValue>
                <valueName>Duplication</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>EMEA</controllingFieldValue>
                <controllingFieldValue>UK</controllingFieldValue>
                <valueName>Goodwill</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>NA</controllingFieldValue>
                <valueName>Internal Process Error</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>EMEA</controllingFieldValue>
                <controllingFieldValue>UK</controllingFieldValue>
                <valueName>Invoice Address</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>EMEA</controllingFieldValue>
                <controllingFieldValue>UK</controllingFieldValue>
                <valueName>Invoiced End User in Error</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>EMEA</controllingFieldValue>
                <controllingFieldValue>UK</controllingFieldValue>
                <valueName>Licence Dates</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>EMEA</controllingFieldValue>
                <controllingFieldValue>UK</controllingFieldValue>
                <valueName>Net Value</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>EMEA</controllingFieldValue>
                <controllingFieldValue>NA</controllingFieldValue>
                <controllingFieldValue>UK</controllingFieldValue>
                <valueName>Other (please specify)</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>EMEA</controllingFieldValue>
                <controllingFieldValue>UK</controllingFieldValue>
                <valueName>Owner Details</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>EMEA</controllingFieldValue>
                <controllingFieldValue>UK</controllingFieldValue>
                <valueName>Partner Details/Commission</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>EMEA</controllingFieldValue>
                <controllingFieldValue>UK</controllingFieldValue>
                <valueName>PO Issues</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>EMEA</controllingFieldValue>
                <controllingFieldValue>UK</controllingFieldValue>
                <valueName>Product Incompatible</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>EMEA</controllingFieldValue>
                <controllingFieldValue>UK</controllingFieldValue>
                <valueName>Product Miss Selling</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>EMEA</controllingFieldValue>
                <controllingFieldValue>NA</controllingFieldValue>
                <controllingFieldValue>UK</controllingFieldValue>
                <valueName>Professional Services</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>APAC</controllingFieldValue>
                <controllingFieldValue>EMEA</controllingFieldValue>
                <controllingFieldValue>UK</controllingFieldValue>
                <valueName>Royalties</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>NA</controllingFieldValue>
                <valueName>Accounts Receivable</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>NA</controllingFieldValue>
                <valueName>Address Discrepancy</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>NA</controllingFieldValue>
                <valueName>Change Order</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>NA</controllingFieldValue>
                <valueName>Internal System Errors</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>NA</controllingFieldValue>
                <valueName>Partner Fees and Royalties</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>NA</controllingFieldValue>
                <valueName>Tax Exempt</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <fields>
        <fullName>Rebill_Margin_Amount__c</fullName>
        <description>Used by EDQ. User input. Case 13545993</description>
        <externalId>false</externalId>
        <inlineHelpText>Input the expected rebill amount</inlineHelpText>
        <label>Rebill Margin Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Rebill__c</fullName>
        <externalId>false</externalId>
        <label>Rebill?</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Yes</fullName>
                    <default>false</default>
                    <label>Yes</label>
                </value>
                <value>
                    <fullName>No</fullName>
                    <default>false</default>
                    <label>No</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Region__c</fullName>
        <externalId>false</externalId>
        <label>Region</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>APAC</fullName>
                    <default>false</default>
                    <label>APAC</label>
                </value>
                <value>
                    <fullName>EMEA</fullName>
                    <default>false</default>
                    <label>EMEA</label>
                </value>
                <value>
                    <fullName>NA</fullName>
                    <default>false</default>
                    <label>NA</label>
                </value>
                <value>
                    <fullName>UK</fullName>
                    <default>false</default>
                    <label>UK</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Pending</fullName>
                    <default>true</default>
                    <label>Pending</label>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                    <label>Rejected</label>
                </value>
                <value>
                    <fullName>Approved</fullName>
                    <default>false</default>
                    <label>Approved</label>
                </value>
                <value>
                    <fullName>Stage1 Approved</fullName>
                    <default>false</default>
                    <label>Stage1 Approved</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Credit Note Request Form</label>
    <listViews>
        <fullName>All_Credit_Note</fullName>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <label>All Credit Note</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Credit_Note_Name_and_Order</fullName>
        <columns>NAME</columns>
        <columns>Account_Name__c</columns>
        <columns>Order__c</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Region__c</field>
            <operation>notEqual</operation>
            <value>UK</value>
        </filters>
        <label>EDQ NA: Credit Note Name and Order</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>EDQ_NA_Credit_Note_Name_and_Order</fullName>
        <columns>NAME</columns>
        <columns>Account_Name__c</columns>
        <columns>Order__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Region__c</field>
            <operation>notEqual</operation>
            <value>UK</value>
        </filters>
        <label>EDQ NA: Credit Note Name and Order</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>NA_Credit_Note_Approvers_Credit_Note_Request_Form</fullName>
        <filterScope>Queue</filterScope>
        <label>NA Credit Note Approvers</label>
        <queue>NA_Credit_Note_Approvers</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>UK_Credit_Note_Business_Approver_Team_Credit_Note_Request_Form</fullName>
        <filterScope>Queue</filterScope>
        <label>UK Credit Note Business Approver Team</label>
        <queue>UK_Credit_Note_Business_Approver_Team</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>UK_Credit_Note_Distribution_List_Credit_Note_Request_Form</fullName>
        <filterScope>Queue</filterScope>
        <label>UK Credit Note Distribution List</label>
        <queue>UK_Credit_Note_Distribution_List</queue>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <nameField>
        <displayFormat>CNRFN-{00000000}</displayFormat>
        <label>Credit Note Request Form Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Credit Note Request Forms</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Other_is_required_when_selected</fullName>
        <active>true</active>
        <errorConditionFormula>AND(ISPICKVAL( Reason_for_requesting_the_Credit_Note__c ,&quot;Other (please specify)&quot;),
     LEN( Other__c )=0)</errorConditionFormula>
        <errorDisplayField>Other__c</errorDisplayField>
        <errorMessage>Please provide request details when selecting Other as Reason</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Rebill_Margin_Amount_required_if_Rebill</fullName>
        <active>true</active>
        <description>Rebill Margin Amount is required if Rebill?=Yes</description>
        <errorConditionFormula>ISPICKVAL(Rebill__c, &quot;Yes&quot;) &amp;&amp; ISBLANK(Rebill_Margin_Amount__c)</errorConditionFormula>
        <errorDisplayField>Rebill_Margin_Amount__c</errorDisplayField>
        <errorMessage>Please add Rebill Margin Amount</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Request_owner_BU_equal_to_order_owner_BU</fullName>
        <active>true</active>
        <description>The Credit Note owner should have the same BU as the order BU stamp.</description>
        <errorConditionFormula>TEXT( Credit_Note_Owner__r.Business_Unit__c  )&lt;&gt;  Order__r.Owner_BU_on_Order_Create_Date__c</errorConditionFormula>
        <errorDisplayField>Credit_Note_Owner__c</errorDisplayField>
        <errorMessage>The credit note owner must be in the same BU as the original order &apos;Owner BU on order create date&apos; value.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>New</fullName>
        <availability>online</availability>
        <description>Case 984094</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/a2f/e?CF{!$Setup.Custom_Fields_Ids__c.CNRF_Order__c}={!Order__c.Name}&amp;CF{!$Setup.Custom_Fields_Ids__c.CNRF_Order__c}_lkid={!Order__c.Id}&amp;retURL=%2F{!Order__c.Id}&amp;CF{!$Setup.Custom_Fields_Ids__c.CNRF_Credit_Note_Owner__c}={!Order__c.OwnerFullName}&amp;CF{!$Setup.Custom_Fields_Ids__c.CNRF_Credit_Note_Owner__c}_lkid={!Order__c.OwnerId}
&amp;CF{!$Setup.Custom_Fields_Ids__c.CNRF_Follow_On_Opp_Owner__c}={!Order__c.OwnerFullName}&amp;CF{!$Setup.Custom_Fields_Ids__c.CNRF_Follow_On_Opp_Owner__c}_lkid={!Order__c.OwnerId}</url>
    </webLinks>
</CustomObject>
