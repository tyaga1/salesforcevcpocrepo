<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object holds the testing team for a test case</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Actual_Results_Feedback__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Actual Results/Feedback</label>
        <length>1000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Completion_Date__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Completion Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Is_user_listed_as_Tester__c</fullName>
        <externalId>false</externalId>
        <formula>Tester__r.Id ==  $User.Id</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Is user listed as Tester</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Status_Original__c</fullName>
        <externalId>false</externalId>
        <label>Status Original</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Not Started</fullName>
                    <default>false</default>
                    <label>Not Started</label>
                </value>
                <value>
                    <fullName>In Progress</fullName>
                    <default>false</default>
                    <label>In Progress</label>
                </value>
                <value>
                    <fullName>Failed</fullName>
                    <default>false</default>
                    <label>Failed</label>
                </value>
                <value>
                    <fullName>Passed</fullName>
                    <default>false</default>
                    <label>Passed</label>
                </value>
                <value>
                    <fullName>Blocked</fullName>
                    <default>false</default>
                    <label>Blocked</label>
                </value>
                <value>
                    <fullName>Waiting for Response</fullName>
                    <default>false</default>
                    <label>Waiting for Response</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>Test_Status</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>Test_Case__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Test Case</label>
        <referenceTo>Test_Case__c</referenceTo>
        <relationshipLabel>Test Case Teams</relationshipLabel>
        <relationshipName>Test_Case_Teams</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Tester_BU__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Text(Tester__r.Business_Unit__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Tester BU</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Tester_Region__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Text( Tester__r.Region__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Tester Region</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Tester_User_Function__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Text(Tester__r.Function__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Tester User Function</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Tester_User_Profile__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Tester__r.Profile.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Tester User Profile</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Tester__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Select the UAT Tester Name</description>
        <externalId>false</externalId>
        <inlineHelpText>Select the UAT Tester Name</inlineHelpText>
        <label>Tester</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Test_Case_Teams</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>UAT_Due_Date__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Test_Case__r.Work__r.agf__Due_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>UAT Due Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>Test Case Team</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Tester__c</columns>
        <columns>Status__c</columns>
        <columns>Test_Case__c</columns>
        <columns>UAT_Due_Date__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All_Test_Cases</fullName>
        <columns>NAME</columns>
        <columns>Test_Case__c</columns>
        <columns>Status__c</columns>
        <columns>Tester__c</columns>
        <columns>UAT_Due_Date__c</columns>
        <columns>OWNER.FIRST_NAME</columns>
        <columns>OWNER.LAST_NAME</columns>
        <filterScope>Everything</filterScope>
        <label>All Test Cases</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>My_Test_Cases</fullName>
        <columns>NAME</columns>
        <columns>Test_Case__c</columns>
        <columns>Status__c</columns>
        <columns>Actual_Results_Feedback__c</columns>
        <columns>UAT_Due_Date__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Is_user_listed_as_Tester__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>My Test Cases</label>
        <language>en_US</language>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <nameField>
        <displayFormat>TM-{00000000}</displayFormat>
        <label>Test Case Team Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Test Case Teams</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>Create_Issue</fullName>
        <availability>online</availability>
        <description>Create an issue associated with the Story(Work).</description>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Create Issue</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/35.0/connection.js&quot;)} 

var returnURL = &apos;retURL=/{!Test_Case_Team__c.Id}&apos;;
var retStr = &apos;/{!$ObjectType.Issue__c}/e?&apos; + returnURL;

var result;

var query = &quot;SELECT Id,Actual_Results_Feedback__c, Test_Case__r.Work__c,Test_Case__r.Work__r.Name,Test_Case__r.Test_Steps__c,Test_Case__r.Short_Description__c FROM Test_Case_Team__c WHERE ID=&apos;{!Test_Case_Team__c.Id}&apos; limit 1&quot;;

try
{
   result = sforce.connection.query(query); 
}
catch(e)
{
   console.log(e);
}

records = result.getArray(&quot;records&quot;); 

if(records.length &gt; 0)
{
   var rec = records[0];
   
   if(rec.Test_Case__r != null)
   {
	if(rec.Test_Case__r.Work__r != null)
	     retStr+= &quot;&amp;{!$Setup.Custom_Fields_Ids__c.Issue_Work__c}=&quot; + rec.Test_Case__r.Work__r.Name;
	
	if(rec.Test_Case__r.Work__c != null)
	     retStr+= &quot;&amp;{!$Setup.Custom_Fields_Ids__c.Issue_Work__c}_lkid=&quot; + rec.Test_Case__r.Work__c;
	     
	if(rec.Test_Case__r.Short_Description__c != null)
	     retStr+= &quot;&amp;{!$Setup.Custom_Fields_Ids__c.Issue_Issue_Name__c}=&quot; + rec.Test_Case__r.Short_Description__c;
	
	if(rec.Test_Case__r.Test_Steps__c != null)
	     retStr+= &quot;&amp;{!$Setup.Custom_Fields_Ids__c.Issue_Steps_to_reproduce__c}=&quot; + rec.Test_Case__r.Test_Steps__c;
	
   }

   if(rec.Actual_Results_Feedback__c != null)
     retStr+= &quot;&amp;{!$Setup.Custom_Fields_Ids__c.Issue_Actual_Result__c}=&quot; + records[0].Actual_Results_Feedback__c;

   retStr+= &quot;&amp;{!$Setup.Custom_Fields_Ids__c.Issue_Status__c}=&quot; + &apos;In Review&apos;;
            
}
else
{
   console.log(&apos;Custom button \&apos;Create Issue\&apos; returned no result for query : &apos; + query);
   alert(&apos;No record found. Contact your administrator.&apos;);
}


window.open(encodeURI(retStr), &quot;_self&quot;);</url>
    </webLinks>
</CustomObject>
