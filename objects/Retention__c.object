<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Collect information needed to support Retention Percentage calculations and reporting</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Relate Retention to Account</description>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Retentions</relationshipLabel>
        <relationshipName>Retentions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Actual_Margin__c</fullName>
        <externalId>false</externalId>
        <formula>IF ( Total_Actual_From_Line_Items__c &gt; Available_Margin__c, Available_Margin__c, Total_Actual_From_Line_Items__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Actual Margin</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Available_Margin__c</fullName>
        <description>Available amount summed from all Standard Retention Line Items</description>
        <externalId>false</externalId>
        <inlineHelpText>Available amount summed from all Standard Retention Line Items</inlineHelpText>
        <label>Available Margin</label>
        <summarizedField>Retention_Line_Item__c.Available_Margin__c</summarizedField>
        <summaryFilterItems>
            <field>Retention_Line_Item__c.Exclude_from_Retention_Calculations__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>Retention_Line_Item__c.Type__c</field>
            <operation>equals</operation>
            <value>Standard</value>
        </summaryFilterItems>
        <summaryForeignKey>Retention_Line_Item__c.Retention__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Extra_Margin__c</fullName>
        <externalId>false</externalId>
        <formula>IF (
  Total_Actual_From_Line_Items__c -  Available_Margin__c &gt; 0,
  Total_Actual_From_Line_Items__c -  Available_Margin__c,
  0
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Extra Margin</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Lock__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Lock</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Lost_Amount__c</fullName>
        <externalId>false</externalId>
        <label>Lost Amount</label>
        <summarizedField>Retention_Line_Item__c.Lost_Margin__c</summarizedField>
        <summaryFilterItems>
            <field>Retention_Line_Item__c.Lost_Margin__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </summaryFilterItems>
        <summaryForeignKey>Retention_Line_Item__c.Retention__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Lost_Margin__c</fullName>
        <externalId>false</externalId>
        <formula>Available_Margin__c -  Total_Actual_From_Line_Items__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Lost Margin</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Month__c</fullName>
        <externalId>false</externalId>
        <label>Month</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Reporting_Date__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>DATE(Year__c,Month__c,15)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Reporting Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Retention_Percent__c</fullName>
        <externalId>false</externalId>
        <formula>IF (
  AND(
    Available_Margin__c != 0, 
    Total_Actual_From_Line_Items__c != 0
  ),
  Actual_Margin__c /  Available_Margin__c,
  0
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Retention %</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Total_Actual_From_Line_Items__c</fullName>
        <description>Actual amount summed from all Standard Retention Line Items</description>
        <externalId>false</externalId>
        <inlineHelpText>Actual amount summed from all Standard Retention Line Items</inlineHelpText>
        <label>Total Actual From Line Items</label>
        <summarizedField>Retention_Line_Item__c.Actual_Margin__c</summarizedField>
        <summaryFilterItems>
            <field>Retention_Line_Item__c.Exclude_from_Retention_Calculations__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>Retention_Line_Item__c.Type__c</field>
            <operation>equals</operation>
            <value>Standard</value>
        </summaryFilterItems>
        <summaryForeignKey>Retention_Line_Item__c.Retention__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Actual_From_Long_Line_Items__c</fullName>
        <description>Actual amount summed from all Retention Line Items where Type = &apos;Long&apos;</description>
        <externalId>false</externalId>
        <inlineHelpText>Actual amount summed from all Retention Line Items where Type = &apos;Long&apos;</inlineHelpText>
        <label>Total Actual From Long Line Items</label>
        <summarizedField>Retention_Line_Item__c.Actual_Margin__c</summarizedField>
        <summaryFilterItems>
            <field>Retention_Line_Item__c.Exclude_from_Retention_Calculations__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>Retention_Line_Item__c.Type__c</field>
            <operation>equals</operation>
            <value>Long</value>
        </summaryFilterItems>
        <summaryForeignKey>Retention_Line_Item__c.Retention__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Available_From_Long_Line_Items__c</fullName>
        <description>Available summed from all Retention Line Items where Type = &apos;Long&apos;</description>
        <externalId>false</externalId>
        <inlineHelpText>Available summed from all Retention Line Items where Type = &apos;Long&apos;</inlineHelpText>
        <label>Total Available From Long Line Items</label>
        <summarizedField>Retention_Line_Item__c.Available_Margin__c</summarizedField>
        <summaryFilterItems>
            <field>Retention_Line_Item__c.Exclude_from_Retention_Calculations__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>Retention_Line_Item__c.Type__c</field>
            <operation>equals</operation>
            <value>Long</value>
        </summaryFilterItems>
        <summaryForeignKey>Retention_Line_Item__c.Retention__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Year__c</fullName>
        <externalId>false</externalId>
        <label>Year</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Retention</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Month__c</columns>
        <columns>Year__c</columns>
        <columns>Account__c</columns>
        <columns>Total_Actual_From_Line_Items__c</columns>
        <columns>Available_Margin__c</columns>
        <columns>Lock__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>RET-{000000000}</displayFormat>
        <label>Retention Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Retentions</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
