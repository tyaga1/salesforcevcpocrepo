<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Object used to create and update Risks and Issues related to Projects
S-252396</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>true</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Contingency_Plan__c</fullName>
        <description>Story # S-252396 - Used to create and update Risks and Issues related to Projects</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Contingency Plan</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Date_Reported__c</fullName>
        <description>Story # S-252396 - Used to create and update Risks and Issues related to Projects</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Date Reported</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Date_for_Resolution__c</fullName>
        <description>Story # S-252396 - Used to create and update Risks and Issues related to Projects</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Date for Resolution</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Impact_Description__c</fullName>
        <description>Story # S-252396 - Used to create and update Risks and Issues related to Projects</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Impact Description</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Impact__c</fullName>
        <description>Story # S-252396 - Used to create and update Risks and Issues related to Projects</description>
        <externalId>false</externalId>
        <label>Impact</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Low</fullName>
                    <default>false</default>
                    <label>Low</label>
                </value>
                <value>
                    <fullName>Medium</fullName>
                    <default>false</default>
                    <label>Medium</label>
                </value>
                <value>
                    <fullName>High</fullName>
                    <default>false</default>
                    <label>High</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Issue_Resolution__c</fullName>
        <description>Story # S-252396 - Used to create and update Risks and Issues related to Projects</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Issue Resolution</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Issue_Risk_Description__c</fullName>
        <description>Story # S-252396 - Used to create and update Risks and Issues related to Projects</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Issue / Risk Description</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Issue_Risk_Owner__c</fullName>
        <description>Story # S-252396 - Used to create and update Risks and Issues related to Projects</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Issue / Risk Owner</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Migration_Strategy__c</fullName>
        <description>Story # S-252396 - Used to create and update Risks and Issues related to Projects</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Migration Strategy</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Originator__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Story # S-252396 - Used to create and update Risks and Issues related to Projects</description>
        <externalId>false</externalId>
        <label>Originator</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Project_Risks_Issues</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Priority__c</fullName>
        <description>Story # S-252396 - Used to create and update Risks and Issues related to Projects</description>
        <externalId>false</externalId>
        <label>Priority</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Low</fullName>
                    <default>false</default>
                    <label>Low</label>
                </value>
                <value>
                    <fullName>Medium</fullName>
                    <default>false</default>
                    <label>Medium</label>
                </value>
                <value>
                    <fullName>High</fullName>
                    <default>false</default>
                    <label>High</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Probability__c</fullName>
        <description>Story # S-252396 - Used to create and update Risks and Issues related to Projects</description>
        <externalId>false</externalId>
        <label>Probability</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Low</fullName>
                    <default>false</default>
                    <label>Low</label>
                </value>
                <value>
                    <fullName>Medium</fullName>
                    <default>false</default>
                    <label>Medium</label>
                </value>
                <value>
                    <fullName>High</fullName>
                    <default>false</default>
                    <label>High</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Project__c</fullName>
        <description>Story # S-252396 - Used to create and update Risks and Issues related to Projects</description>
        <externalId>false</externalId>
        <label>Project</label>
        <referenceTo>Project__c</referenceTo>
        <relationshipLabel>Project Risks / Issues</relationshipLabel>
        <relationshipName>Project_Risks_Issues</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Risk_Reason__c</fullName>
        <description>Story # S-252396 - Used to create and update Risks and Issues related to Projects</description>
        <externalId>false</externalId>
        <label>Risk Reason</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Client Satisfaction</fullName>
                    <default>false</default>
                    <label>Client Satisfaction</label>
                </value>
                <value>
                    <fullName>Competative Situation</fullName>
                    <default>false</default>
                    <label>Competative Situation</label>
                </value>
                <value>
                    <fullName>Delay in Out Date</fullName>
                    <default>false</default>
                    <label>Delay in Out Date</label>
                </value>
                <value>
                    <fullName>Increased Costs</fullName>
                    <default>false</default>
                    <label>Increased Costs</label>
                </value>
                <value>
                    <fullName>Limited Resources</fullName>
                    <default>false</default>
                    <label>Limited Resources</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>Story # S-252396 - Used to create and update Risks and Issues related to Projects</description>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>New</fullName>
                    <default>false</default>
                    <label>New</label>
                </value>
                <value>
                    <fullName>Open</fullName>
                    <default>false</default>
                    <label>Open</label>
                </value>
                <value>
                    <fullName>Closed</fullName>
                    <default>false</default>
                    <label>Closed</label>
                </value>
                <value>
                    <fullName>Moved to Issue (Risk RT only)</fullName>
                    <default>false</default>
                    <label>Moved to Issue (Risk RT only)</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Project Risk / Issue</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Risk / Issue Name</label>
        <trackFeedHistory>false</trackFeedHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Project Risks / Issues</pluralLabel>
    <recordTypeTrackFeedHistory>false</recordTypeTrackFeedHistory>
    <recordTypes>
        <fullName>Issue</fullName>
        <active>true</active>
        <label>Issue</label>
        <picklistValues>
            <picklist>Impact__c</picklist>
            <values>
                <fullName>High</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Low</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Medium</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Priority__c</picklist>
            <values>
                <fullName>High</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Low</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Medium</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Probability__c</picklist>
            <values>
                <fullName>High</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Low</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Medium</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Risk_Reason__c</picklist>
            <values>
                <fullName>Client Satisfaction</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Competative Situation</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Delay in Out Date</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Increased Costs</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Limited Resources</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Closed</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Moved to Issue %28Risk RT only%29</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>New</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Open</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Risk</fullName>
        <active>true</active>
        <label>Risk</label>
        <picklistValues>
            <picklist>Impact__c</picklist>
            <values>
                <fullName>High</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Low</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Medium</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Priority__c</picklist>
            <values>
                <fullName>High</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Low</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Medium</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Probability__c</picklist>
            <values>
                <fullName>High</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Low</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Medium</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Risk_Reason__c</picklist>
            <values>
                <fullName>Client Satisfaction</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Competative Situation</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Delay in Out Date</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Increased Costs</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Limited Resources</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Closed</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Moved to Issue %28Risk RT only%29</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>New</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Open</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
