<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Counter_Id__c</fullName>
        <description>The counter id is provided by the customer and is used by tech support to generate the counter code</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <inlineHelpText>The counter id is provided by the customer and is used by tech support to generate the counter code</inlineHelpText>
        <label>Counter Id</label>
        <length>50</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Data_Usage__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>TEXT(Registration__r.Asset__r.Data_Usage__c)</formula>
        <label>Data Usage</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>End_Date__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Registration__r.Asset__r.UsageEndDate</formula>
        <label>End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Number_of_Clicks__c</fullName>
        <externalId>false</externalId>
        <label>Number of Clicks</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Outbound_Message_Toggle__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Checkbox triggering an outbound message to be sent to Boomi via a WF Rule when toggled</description>
        <externalId>false</externalId>
        <label>Outbound Message Toggle</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Price_Category__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Registration__r.Asset__r.Price_Category__c</formula>
        <label>Price Category</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product_Data__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Registration__r.Asset__r.Product_Data__c</formula>
        <label>DataSet</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <externalId>false</externalId>
        <formula>Registration__r.Asset__r.Order_Line__r.CPQ_Quantity__c 

/* CPQ Quantity from the Order Line becomes the Number of Clicks  */</formula>
        <label>Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Registration__c</fullName>
        <externalId>false</externalId>
        <label>Registration</label>
        <referenceTo>Registration__c</referenceTo>
        <relationshipLabel>Counter Update Codes</relationshipLabel>
        <relationshipName>Counter_Update_Codes</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Registration__r.Asset__r.Start_Date__c</formula>
        <label>Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Update_Code__c</fullName>
        <description>The Update Code is generated by tech support using the counter id provided by the customer</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <inlineHelpText>The Update Code is generated by tech support using the counter id provided by the customer</inlineHelpText>
        <label>Update Code</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Counter Update Code</label>
    <listViews>
        <fullName>All</fullName>
        <columns>OBJECT_ID</columns>
        <columns>NAME</columns>
        <columns>Counter_Id__c</columns>
        <columns>Start_Date__c</columns>
        <columns>Update_Code__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>CUC-{0000000000}</displayFormat>
        <label>Counter Update Code Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Counter Update Codes</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>Generate_Counter_Id</fullName>
        <availability>online</availability>
        <description>Will toggle the Outbound_Message_Toggle__c field to send an outbound message to Boomi</description>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Generate Counter Id</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/25.0/connection.js&quot;)} 
{!REQUIRESCRIPT(&quot;/soap/ajax/10.0/apex.js&quot;)}

var counterUpdateCodeObj = new sforce.SObject(&quot;Counter_Update_Code__c&quot;); 
counterUpdateCodeObj.Id = &apos;{!Counter_Update_Code__c.Id}&apos;; 

var toggleValue = &apos;{!Counter_Update_Code__c.Outbound_Message_Toggle__c}&apos;; 

if (toggleValue == true) {
  counterUpdateCodeObj.Outbound_Message_Toggle__c = false;
} else {
  counterUpdateCodeObj.Outbound_Message_Toggle__c = true;
}
sforce.connection.update([counterUpdateCodeObj]);

window.location.reload();</url>
    </webLinks>
</CustomObject>
