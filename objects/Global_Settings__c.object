<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>List</customSettingsType>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>ARIA_Order_Standard_RecordTypeId__c</fullName>
        <description>Record Type &apos;Standard&apos; on the ARIA Order object. Used for the Clone Order function.</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>ARIA Order Standard RecordTypeId</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AccSegmentCreationViaATM_Job_Last_Run__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>AccSegmentCreationViaATM Job Last Run</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Account_Assignment_Team_Job_Last_Run__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Account Assignment Team Job Last Run</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Account_Hierarchy_Max_Levels__c</fullName>
        <description>This is the max assumed level up to which Account Hierarchy can extend. This is to make sure how many times the recursive query will fire and max limit is 10 times. It will exit before if no children found but if found this is how deep it will go. DQ team has mentioned our accounts go 9 level deep.</description>
        <externalId>false</externalId>
        <label>Account Hierarchy Max Levels</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account_Hierarchy_Size__c</fullName>
        <externalId>false</externalId>
        <label>Account Hierarchy Size</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account_Team_Member_CI_Job_Last_Run__c</fullName>
        <description>Account Team Member CI Job Last Run</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Account Team Member CI Job Last Run</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Account_Team_Member_Default_Role__c</fullName>
        <defaultValue>&apos;Sales Rep&apos;</defaultValue>
        <description>S-154585
Topulate from trigger when A/c is created.</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Account Team Member Default Role</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account_Team_Member_Job_Last_Run__c</fullName>
        <description>The last run time of the scheduled APEX job to propagate Account Team Members down a hierarchy of Accounts. For first run, or starting the job up again after having stopped it, this value can be set to an appropriate time by the user.</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Account Team Member Job Last Run</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Assignment_Team_Member_Job_Last_Run__c</fullName>
        <description>Last time the BatchAssignmentTeam job was run</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Assignment Team Member Job Last Run</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>AttachmentBodyReplace_Ended__c</fullName>
        <defaultValue>true</defaultValue>
        <externalId>false</externalId>
        <label>AttachmentBodyReplace Ended</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>BUHierarchy_Job_Last_Run__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>BUHierarchy Job Last Run</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>BU_Default_Currency__c</fullName>
        <defaultValue>&apos;USD&apos;</defaultValue>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <inlineHelpText>Populate BU.CurrencyISOCode</inlineHelpText>
        <label>BU Default Currency</label>
        <length>225</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BatchAccountSegmentDelete_Last_Run__c</fullName>
        <description>Last date/time that the BatchAccountSegmentDelete ran</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <inlineHelpText>Last date/time that the BatchAccountSegmentDelete ran</inlineHelpText>
        <label>BatchAccountSegmentDelete Last Run</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Batch_Failures_Email__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Batch Failures Email</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CPQ_Admin_User__c</fullName>
        <description>Objective: Allow CPQ to edit OLI</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>CPQ Admin User</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Campaign_Internal_Contacts_Employees__c</fullName>
        <description>Case#02131111
All Experian Cross BU Contacts records to be linked to the Active Campaign - Internal Contacts (Employees)</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <inlineHelpText>When the Contact record is created for each new employee, they need to automatically be associated to the Internal Contacts (Employees) Campaign.</inlineHelpText>
        <label>Campaign - Internal Contacts (Employees)</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case_Access_Request_RecordtypeId__c</fullName>
        <description>T-199996</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <inlineHelpText>Case &quot;Access Request&quot; Record TypeId.</inlineHelpText>
        <label>Case Access Request RecordtypeId</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case_Access_Request_TeamRole__c</fullName>
        <defaultValue>&apos;Requestor&apos;</defaultValue>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <inlineHelpText>CaseTeamRole.Name</inlineHelpText>
        <label>Case Access Request TeamRole</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case_Active_Assignment_Id__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Case Active Assignment Id</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case_DQ_Team_Queue__c</fullName>
        <defaultValue>&apos;DQ Team Queue&apos;</defaultValue>
        <description>T-208210</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <inlineHelpText>Case &apos;DQ Team Queue&apos;</inlineHelpText>
        <label>Case DQ Team Queue</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contract_Renewal_Job_Last_Run__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Contract Renewal Job Last Run</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Default_Copado_Project__c</fullName>
        <description>Used to assigned a default value to the Copado case automation, y default set to the id for the GCSS CRM BAU Project</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Default Copado Project</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ExpDQAdminProfileId__c</fullName>
        <description>This custom setting will hold the row Id of the Experian DQ Administrator profile Record Id.</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <inlineHelpText>Experian DQ Admin Record ID.</inlineHelpText>
        <label>ExpDQAdminProfileId</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Experian_Cross_BU_Acc_Id__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Experian Cross BU Acc Id</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Last_Wallet_Sync_Upload_Started__c</fullName>
        <description>Will hold the last &apos;start time&apos; for the upload process</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Last Wallet Sync Upload Started</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Lastest_Batch_Number__c</fullName>
        <externalId>false</externalId>
        <label>Lastest Batch Number</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opp_Closed_Lost_Stagename__c</fullName>
        <defaultValue>&apos;Closed Lost&apos;</defaultValue>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Opp Closed Lost Stagename</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opp_Renewal_Name_Format__c</fullName>
        <defaultValue>&apos;Renewal - ####&apos;</defaultValue>
        <description>Accessed from Apex Class OpportunityTriggerHandler</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <inlineHelpText>User 4 Hash &quot;#&quot; in the Format, which will be replaced by code with current Opportunity.Name. (eg. &apos;Renewal - ####&apos; )</inlineHelpText>
        <label>Opp Renewal Name Format</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opp_Renewal_Probability__c</fullName>
        <description>Accessed from Apex Class OpportunityTriggerHandler</description>
        <externalId>false</externalId>
        <label>Opp Renewal Probability</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opp_Renewal_StageName__c</fullName>
        <description>Accessed from Apex Class OpportunityTriggerHandler</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Opp Renewal StageName</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opp_Renewal_Type__c</fullName>
        <defaultValue>&apos;Renewal&apos;</defaultValue>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Opp Renewal Type</label>
        <length>225</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opp_Stage_3_Name__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Opp Stage 3 Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opp_Stage_4_Name__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Opp Stage 4 Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Profile_IDs_allowed_to_edit_Addresses__c</fullName>
        <description>Any profiles that should be allowed to edit an Address record needs to have its ID entered into this box in order for the &quot;Edit&quot; button to be displayed on the Address VisualForce page</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <inlineHelpText>Please enter the profile ID of the profile you wish to have access to Edit Address records. Multiple IDs can be entered, separate each with a comma.</inlineHelpText>
        <label>Profile IDs allowed to edit Addresses</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Seal_API_User__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Seal API User</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Smart_Search_Query_Limit__c</fullName>
        <defaultValue>250</defaultValue>
        <externalId>false</externalId>
        <label>Smart Search Query Limit</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>WalletIntegn_CompletionDate__c</fullName>
        <description>This field is updated by the wallet Integration when the process is completed</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>WalletIntegn CompletionDate</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>WalletIntegn_StartDate__c</fullName>
        <description>This field is updated by the Wallet Integration for Serasa</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>WalletIntegn StartDate</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>WalletIntegn_Status__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>WalletIntegn Status</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Wallet_Sync_Processing_Completed__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Wallet Sync Processing Completed</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Wallet_Sync_Processing_Last_Run__c</fullName>
        <description>The last time the Wallet Sync Process started, to find entries uploaded since the last run.</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Wallet Sync Processing Last Run</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Wallet_Sync_Processing_Start__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Wallet Sync Processing Start</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>chatterGrpActy_DaysAfter__c</fullName>
        <description>Provides the after range for the BatchChatterGroupArchiveMaintenance class that will email chatter group owners</description>
        <externalId>false</externalId>
        <label>chatterGrpActy DaysAfter</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>chatterGrpActy_DaysBefore__c</fullName>
        <description>Provides the before range for the BatchChatterGroupArchiveMaintenance class that will email chatter group owners</description>
        <externalId>false</externalId>
        <label>chatterGrpActy DaysBefore</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>chatterGrpActy_FromAddress__c</fullName>
        <description>From Address for email notification in BatchChatterGroupArchiveMaintenance class</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>chatterGrpActy FromAddress</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>salesforceSupportMailbox__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>salesforceSupportMailbox</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <label>Global Settings</label>
    <visibility>Protected</visibility>
</CustomObject>
