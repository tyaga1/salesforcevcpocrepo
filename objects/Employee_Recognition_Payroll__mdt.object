<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <description>Used to hold configuration for Nomination Payroll reports, e.g. email, name, send day</description>
    <fields>
        <fullName>Active__c</fullName>
        <defaultValue>true</defaultValue>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Untick to not send emails to this payroll mailbox</inlineHelpText>
        <label>Active</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Auto_Gross_Percentage__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Automatically apply a gross up percentage to the reward amount - only for certain territories.</inlineHelpText>
        <label>Auto Gross Percentage</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Currency__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Currency</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>ARS</fullName>
                    <default>false</default>
                    <label>ARS</label>
                </value>
                <value>
                    <fullName>AUD</fullName>
                    <default>false</default>
                    <label>AUD</label>
                </value>
                <value>
                    <fullName>BGN</fullName>
                    <default>false</default>
                    <label>BGN</label>
                </value>
                <value>
                    <fullName>BRL</fullName>
                    <default>false</default>
                    <label>BRL</label>
                </value>
                <value>
                    <fullName>CAD</fullName>
                    <default>false</default>
                    <label>CAD</label>
                </value>
                <value>
                    <fullName>CHF</fullName>
                    <default>false</default>
                    <label>CHF</label>
                </value>
                <value>
                    <fullName>CLP</fullName>
                    <default>false</default>
                    <label>CLP</label>
                </value>
                <value>
                    <fullName>CNY</fullName>
                    <default>false</default>
                    <label>CNY</label>
                </value>
                <value>
                    <fullName>COP</fullName>
                    <default>false</default>
                    <label>COP</label>
                </value>
                <value>
                    <fullName>CRC</fullName>
                    <default>false</default>
                    <label>CRC</label>
                </value>
                <value>
                    <fullName>DKK</fullName>
                    <default>false</default>
                    <label>DKK</label>
                </value>
                <value>
                    <fullName>EEK</fullName>
                    <default>false</default>
                    <label>EEK</label>
                </value>
                <value>
                    <fullName>EUR</fullName>
                    <default>false</default>
                    <label>EUR</label>
                </value>
                <value>
                    <fullName>GBP</fullName>
                    <default>false</default>
                    <label>GBP</label>
                </value>
                <value>
                    <fullName>HKD</fullName>
                    <default>false</default>
                    <label>HKD</label>
                </value>
                <value>
                    <fullName>IDR</fullName>
                    <default>false</default>
                    <label>IDR</label>
                </value>
                <value>
                    <fullName>INR</fullName>
                    <default>false</default>
                    <label>INR</label>
                </value>
                <value>
                    <fullName>JPY</fullName>
                    <default>false</default>
                    <label>JPY</label>
                </value>
                <value>
                    <fullName>KRW</fullName>
                    <default>false</default>
                    <label>KRW</label>
                </value>
                <value>
                    <fullName>MAD</fullName>
                    <default>false</default>
                    <label>MAD</label>
                </value>
                <value>
                    <fullName>MXN</fullName>
                    <default>false</default>
                    <label>MXN</label>
                </value>
                <value>
                    <fullName>MYR</fullName>
                    <default>false</default>
                    <label>MYR</label>
                </value>
                <value>
                    <fullName>NOK</fullName>
                    <default>false</default>
                    <label>NOK</label>
                </value>
                <value>
                    <fullName>NZD</fullName>
                    <default>false</default>
                    <label>NZD</label>
                </value>
                <value>
                    <fullName>PEN</fullName>
                    <default>false</default>
                    <label>PEN</label>
                </value>
                <value>
                    <fullName>PHP</fullName>
                    <default>false</default>
                    <label>PHP</label>
                </value>
                <value>
                    <fullName>PLN</fullName>
                    <default>false</default>
                    <label>PLN</label>
                </value>
                <value>
                    <fullName>RUB</fullName>
                    <default>false</default>
                    <label>RUB</label>
                </value>
                <value>
                    <fullName>SEK</fullName>
                    <default>false</default>
                    <label>SEK</label>
                </value>
                <value>
                    <fullName>SGD</fullName>
                    <default>false</default>
                    <label>SGD</label>
                </value>
                <value>
                    <fullName>THB</fullName>
                    <default>false</default>
                    <label>THB</label>
                </value>
                <value>
                    <fullName>TRY</fullName>
                    <default>false</default>
                    <label>TRY</label>
                </value>
                <value>
                    <fullName>TWD</fullName>
                    <default>false</default>
                    <label>TWD</label>
                </value>
                <value>
                    <fullName>USD</fullName>
                    <default>true</default>
                    <label>USD</label>
                </value>
                <value>
                    <fullName>VEF</fullName>
                    <default>false</default>
                    <label>VEF</label>
                </value>
                <value>
                    <fullName>ZAR</fullName>
                    <default>false</default>
                    <label>ZAR</label>
                </value>
                <value>
                    <fullName>AED</fullName>
                    <default>false</default>
                    <label>AED</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Day_of_the_Month__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Choose a specific day to send the report. Note: 29-31 are not available due to not all months containing these dates.</inlineHelpText>
        <label>Day of the Month</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1</fullName>
                    <default>false</default>
                    <label>1</label>
                </value>
                <value>
                    <fullName>2</fullName>
                    <default>false</default>
                    <label>2</label>
                </value>
                <value>
                    <fullName>3</fullName>
                    <default>false</default>
                    <label>3</label>
                </value>
                <value>
                    <fullName>4</fullName>
                    <default>false</default>
                    <label>4</label>
                </value>
                <value>
                    <fullName>5</fullName>
                    <default>false</default>
                    <label>5</label>
                </value>
                <value>
                    <fullName>6</fullName>
                    <default>false</default>
                    <label>6</label>
                </value>
                <value>
                    <fullName>7</fullName>
                    <default>false</default>
                    <label>7</label>
                </value>
                <value>
                    <fullName>8</fullName>
                    <default>false</default>
                    <label>8</label>
                </value>
                <value>
                    <fullName>9</fullName>
                    <default>false</default>
                    <label>9</label>
                </value>
                <value>
                    <fullName>10</fullName>
                    <default>false</default>
                    <label>10</label>
                </value>
                <value>
                    <fullName>11</fullName>
                    <default>false</default>
                    <label>11</label>
                </value>
                <value>
                    <fullName>12</fullName>
                    <default>false</default>
                    <label>12</label>
                </value>
                <value>
                    <fullName>13</fullName>
                    <default>false</default>
                    <label>13</label>
                </value>
                <value>
                    <fullName>14</fullName>
                    <default>false</default>
                    <label>14</label>
                </value>
                <value>
                    <fullName>15</fullName>
                    <default>false</default>
                    <label>15</label>
                </value>
                <value>
                    <fullName>16</fullName>
                    <default>false</default>
                    <label>16</label>
                </value>
                <value>
                    <fullName>17</fullName>
                    <default>false</default>
                    <label>17</label>
                </value>
                <value>
                    <fullName>18</fullName>
                    <default>false</default>
                    <label>18</label>
                </value>
                <value>
                    <fullName>19</fullName>
                    <default>false</default>
                    <label>19</label>
                </value>
                <value>
                    <fullName>20</fullName>
                    <default>false</default>
                    <label>20</label>
                </value>
                <value>
                    <fullName>21</fullName>
                    <default>false</default>
                    <label>21</label>
                </value>
                <value>
                    <fullName>22</fullName>
                    <default>false</default>
                    <label>22</label>
                </value>
                <value>
                    <fullName>23</fullName>
                    <default>false</default>
                    <label>23</label>
                </value>
                <value>
                    <fullName>24</fullName>
                    <default>false</default>
                    <label>24</label>
                </value>
                <value>
                    <fullName>25</fullName>
                    <default>false</default>
                    <label>25</label>
                </value>
                <value>
                    <fullName>26</fullName>
                    <default>false</default>
                    <label>26</label>
                </value>
                <value>
                    <fullName>27</fullName>
                    <default>false</default>
                    <label>27</label>
                </value>
                <value>
                    <fullName>28</fullName>
                    <default>false</default>
                    <label>28</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Email_Template_for_Reminder__c</fullName>
        <defaultValue>&quot;General_Payroll_Reminder&quot;</defaultValue>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Developer name of the email template to use for sending the payroll reminders, of incomplete gross values.</inlineHelpText>
        <label>Email Template for Reminder</label>
        <length>255</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Email_Template_for_Report__c</fullName>
        <defaultValue>&quot;General_Payroll_Notification&quot;</defaultValue>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Developer name for email template to send with the report.</inlineHelpText>
        <label>Email Template for Report</label>
        <length>255</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>No_Gross_Up_Possible__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>No Gross Up Possible</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Notes__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Some notes about this, for future reference or why something is the way it is</inlineHelpText>
        <label>Notes</label>
        <required>false</required>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Payroll_Reference__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>All payroll references from oracle</inlineHelpText>
        <label>Payroll Reference</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>AR Monthly Payroll</fullName>
                    <default>false</default>
                    <label>AR Monthly Payroll</label>
                </value>
                <value>
                    <fullName>AT Monthly Payroll</fullName>
                    <default>false</default>
                    <label>AT Monthly Payroll</label>
                </value>
                <value>
                    <fullName>AU Monthly Payroll</fullName>
                    <default>false</default>
                    <label>AU Monthly Payroll</label>
                </value>
                <value>
                    <fullName>BG Monthly Payroll</fullName>
                    <default>false</default>
                    <label>BG Monthly Payroll</label>
                </value>
                <value>
                    <fullName>BR Monthly Payroll</fullName>
                    <default>false</default>
                    <label>BR Monthly Payroll</label>
                </value>
                <value>
                    <fullName>CA Bi-Weekly</fullName>
                    <default>false</default>
                    <label>CA Bi-Weekly</label>
                </value>
                <value>
                    <fullName>CL Monthly</fullName>
                    <default>false</default>
                    <label>CL Monthly</label>
                </value>
                <value>
                    <fullName>CN Monthly Payroll</fullName>
                    <default>false</default>
                    <label>CN Monthly Payroll</label>
                </value>
                <value>
                    <fullName>CO Bi-weekly</fullName>
                    <default>false</default>
                    <label>CO Bi-weekly</label>
                </value>
                <value>
                    <fullName>CR Semi-Monthly</fullName>
                    <default>false</default>
                    <label>CR Semi-Monthly</label>
                </value>
                <value>
                    <fullName>DE Monthly Payroll</fullName>
                    <default>false</default>
                    <label>DE Monthly Payroll</label>
                </value>
                <value>
                    <fullName>DK Monthly Payroll</fullName>
                    <default>false</default>
                    <label>DK Monthly Payroll</label>
                </value>
                <value>
                    <fullName>ES Monthly Payroll</fullName>
                    <default>false</default>
                    <label>ES Monthly Payroll</label>
                </value>
                <value>
                    <fullName>FR Monthly Payroll</fullName>
                    <default>false</default>
                    <label>FR Monthly Payroll</label>
                </value>
                <value>
                    <fullName>GR Monthly Payroll</fullName>
                    <default>false</default>
                    <label>GR Monthly Payroll</label>
                </value>
                <value>
                    <fullName>HK Monthly Payroll</fullName>
                    <default>false</default>
                    <label>HK Monthly Payroll</label>
                </value>
                <value>
                    <fullName>ID Monthly Payroll</fullName>
                    <default>false</default>
                    <label>ID Monthly Payroll</label>
                </value>
                <value>
                    <fullName>IE Monthly Payroll</fullName>
                    <default>false</default>
                    <label>IE Monthly Payroll</label>
                </value>
                <value>
                    <fullName>IN Monthly Payroll</fullName>
                    <default>false</default>
                    <label>IN Monthly Payroll</label>
                </value>
                <value>
                    <fullName>IT Monthly Payroll (14 Payments)</fullName>
                    <default>false</default>
                    <label>IT Monthly Payroll (14 Payments)</label>
                </value>
                <value>
                    <fullName>JP Monthly Payroll</fullName>
                    <default>false</default>
                    <label>JP Monthly Payroll</label>
                </value>
                <value>
                    <fullName>MC Monthly Payroll</fullName>
                    <default>false</default>
                    <label>MC Monthly Payroll</label>
                </value>
                <value>
                    <fullName>MX Monthly Payroll</fullName>
                    <default>false</default>
                    <label>MX Monthly Payroll</label>
                </value>
                <value>
                    <fullName>MY Monthly Payroll</fullName>
                    <default>false</default>
                    <label>MY Monthly Payroll</label>
                </value>
                <value>
                    <fullName>NL Monthly Payroll</fullName>
                    <default>false</default>
                    <label>NL Monthly Payroll</label>
                </value>
                <value>
                    <fullName>NO Monthly Payroll</fullName>
                    <default>false</default>
                    <label>NO Monthly Payroll</label>
                </value>
                <value>
                    <fullName>NZ Monthly Payroll</fullName>
                    <default>false</default>
                    <label>NZ Monthly Payroll</label>
                </value>
                <value>
                    <fullName>PE Bi-weekly</fullName>
                    <default>false</default>
                    <label>PE Bi-weekly</label>
                </value>
                <value>
                    <fullName>RU Monthly Payroll</fullName>
                    <default>false</default>
                    <label>RU Monthly Payroll</label>
                </value>
                <value>
                    <fullName>SE Monthly Payroll</fullName>
                    <default>false</default>
                    <label>SE Monthly Payroll</label>
                </value>
                <value>
                    <fullName>SG Monthly Payroll</fullName>
                    <default>false</default>
                    <label>SG Monthly Payroll</label>
                </value>
                <value>
                    <fullName>TH Monthly Payroll</fullName>
                    <default>false</default>
                    <label>TH Monthly Payroll</label>
                </value>
                <value>
                    <fullName>TR Monthly Payroll</fullName>
                    <default>false</default>
                    <label>TR Monthly Payroll</label>
                </value>
                <value>
                    <fullName>TW Monthly Payroll</fullName>
                    <default>false</default>
                    <label>TW Monthly Payroll</label>
                </value>
                <value>
                    <fullName>UAE Monthly Payroll</fullName>
                    <default>false</default>
                    <label>UAE Monthly Payroll</label>
                </value>
                <value>
                    <fullName>UK Expat Monthly</fullName>
                    <default>false</default>
                    <label>UK Expat Monthly</label>
                </value>
                <value>
                    <fullName>UK Monthly</fullName>
                    <default>false</default>
                    <label>UK Monthly</label>
                </value>
                <value>
                    <fullName>US BI-weekly</fullName>
                    <default>false</default>
                    <label>US BI-weekly</label>
                </value>
                <value>
                    <fullName>VE Bi-weekly</fullName>
                    <default>false</default>
                    <label>VE Bi-weekly</label>
                </value>
                <value>
                    <fullName>ZA Monthly Payroll</fullName>
                    <default>false</default>
                    <label>ZA Monthly Payroll</label>
                </value>
                <value>
                    <fullName>ZZZ Test Payroll</fullName>
                    <default>false</default>
                    <label>ZZZ Test Payroll</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Recipients__c</fullName>
        <defaultValue>&quot;please.change@example.com&quot;</defaultValue>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>To add multiple email recipients, separate each email address with a comma, e.g. pk@test.com,me@here.com</inlineHelpText>
        <label>Recipients</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Report_Layout__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Report Layout</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Payroll_Report</fullName>
                    <default>true</default>
                    <label>Payroll_Report</label>
                </value>
                <value>
                    <fullName>Payroll_Report_US</fullName>
                    <default>false</default>
                    <label>Payroll_Report_US</label>
                </value>
                <value>
                    <fullName>Payroll_Report_Gross</fullName>
                    <default>false</default>
                    <label>Payroll_Report_Gross</label>
                </value>
                <value>
                    <fullName>Payroll_Report_US_Gross</fullName>
                    <default>false</default>
                    <label>Payroll_Report_US_Gross</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Report_Type__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Report Type</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>XLS</fullName>
                    <default>true</default>
                    <label>XLS</label>
                </value>
                <value>
                    <fullName>CSV</fullName>
                    <default>false</default>
                    <label>CSV</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Sandbox_Recipients__c</fullName>
        <defaultValue>&quot;please.change@example.com&quot;</defaultValue>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>** For testing in Sandboxes **
To add multiple email recipients, separate each email address with a comma, e.g. pk@test.com,me@here.com</inlineHelpText>
        <label>Sandbox Recipients</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Test_Case__c</fullName>
        <caseSensitive>false</caseSensitive>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>Will only be used to create test case entries.</inlineHelpText>
        <label>Test Case</label>
        <length>20</length>
        <required>false</required>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>Employee Recognition Payroll</label>
    <pluralLabel>Employee Recognition Payrolls</pluralLabel>
    <visibility>Public</visibility>
</CustomObject>
