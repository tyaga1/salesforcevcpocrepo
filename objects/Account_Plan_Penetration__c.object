<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Object is used to store which BUs have managed to penetrate the Account based from the Account plan
S-210186</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Account_Business_Unit__c</fullName>
        <description>Story # S-210186- Used for Account Penetration flow</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Account Business Unit</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Account_Plan__c</fullName>
        <description>Story # S-210186- Used for Account Penetration flow</description>
        <externalId>false</externalId>
        <label>Account Plan</label>
        <referenceTo>Account_Plan__c</referenceTo>
        <relationshipLabel>Account Plan Penetrations</relationshipLabel>
        <relationshipName>Account_Plan_Penetrations</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Annual_Revenue__c</fullName>
        <description>INTEG - N || APEX - N || WF - N || VAL - N ||
Desc -  || 
Story# - {S-210186} ||</description>
        <externalId>false</externalId>
        <label>Annual Revenue</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Capability__c</fullName>
        <description>INTEG - N || APEX - N || WF - N || VAL - N ||
Desc -  || 
Story# - {S-210186} ||</description>
        <externalId>false</externalId>
        <label>Capability</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Application Processing</fullName>
                    <default>false</default>
                    <label>Application Processing</label>
                </value>
                <value>
                    <fullName>Consumer Insight</fullName>
                    <default>false</default>
                    <label>Consumer Insight</label>
                </value>
                <value>
                    <fullName>Consumer Services</fullName>
                    <default>false</default>
                    <label>Consumer Services</label>
                </value>
                <value>
                    <fullName>Credit Risk</fullName>
                    <default>false</default>
                    <label>Credit Risk</label>
                </value>
                <value>
                    <fullName>Customer insight &amp; engagement</fullName>
                    <default>false</default>
                    <label>Customer insight &amp; engagement</label>
                </value>
                <value>
                    <fullName>Data Quality and Management</fullName>
                    <default>false</default>
                    <label>Data Quality and Management</label>
                </value>
                <value>
                    <fullName>Fraud &amp; ID</fullName>
                    <default>false</default>
                    <label>Fraud &amp; ID</label>
                </value>
                <value>
                    <fullName>Market intelligence</fullName>
                    <default>false</default>
                    <label>Market intelligence</label>
                </value>
                <value>
                    <fullName>Portfolios and Regulation</fullName>
                    <default>false</default>
                    <label>Portfolios and Regulation</label>
                </value>
                <value>
                    <fullName>Vehicle insight</fullName>
                    <default>false</default>
                    <label>Vehicle insight</label>
                </value>
                <value>
                    <fullName>Cross Channel Marketing</fullName>
                    <default>false</default>
                    <label>Cross Channel Marketing</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Penetration__c</fullName>
        <description>Story # S-210186- Used for Account Penetration flow</description>
        <externalId>false</externalId>
        <label>Penetration</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>H</fullName>
                    <default>false</default>
                    <label>H</label>
                </value>
                <value>
                    <fullName>M</fullName>
                    <default>false</default>
                    <label>M</label>
                </value>
                <value>
                    <fullName>L</fullName>
                    <default>false</default>
                    <label>L</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Account Plan Penetration</label>
    <nameField>
        <displayFormat>APP-{0000}</displayFormat>
        <label>Account Plan Penetration</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Account Plan Penetrations</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
