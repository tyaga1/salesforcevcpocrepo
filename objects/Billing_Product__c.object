<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>BP_External_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <encrypted>false</encrypted>
        <externalId>true</externalId>
        <label>BP External Id</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Drmid__c</fullName>
        <encrypted>false</encrypted>
        <externalId>true</externalId>
        <label>Drmid</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Prod_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>For data migration purposes. Case 02346885</description>
        <encrypted>false</encrypted>
        <externalId>true</externalId>
        <label>Prod_Id</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Product_Master__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>New lookup created as part of product master application.</description>
        <externalId>false</externalId>
        <label>Product Master</label>
        <referenceTo>Product_Master__c</referenceTo>
        <relationshipLabel>Billing Products</relationshipLabel>
        <relationshipName>Billing_Products</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Billing Products</relationshipLabel>
        <relationshipName>Billing_Products</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Revenue_Schedule_Rule__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Revenue Schedule Rule</label>
        <referenceTo>Revenue_Schedule_Rule__c</referenceTo>
        <relationshipLabel>Billing Products</relationshipLabel>
        <relationshipName>Billing_Products</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Simple_or_Complex__c</fullName>
        <externalId>false</externalId>
        <label>Simple or Complex</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Simple</fullName>
                    <default>false</default>
                    <label>Simple</label>
                </value>
                <value>
                    <fullName>Complex</fullName>
                    <default>false</default>
                    <label>Complex</label>
                </value>
                <value>
                    <fullName>Simple / Complex</fullName>
                    <default>false</default>
                    <label>Simple / Complex</label>
                </value>
                <value>
                    <fullName>No longer available</fullName>
                    <default>false</default>
                    <label>No longer available</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Type_of_Sale__c</fullName>
        <description>Please keep in sync with &apos;Product - Types of Sale&apos; field</description>
        <externalId>false</externalId>
        <label>Type of Sale</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>true</sorted>
                <value>
                    <fullName>1.5 Year License</fullName>
                    <default>false</default>
                    <label>1.5 Year License</label>
                </value>
                <value>
                    <fullName>2 Year License</fullName>
                    <default>false</default>
                    <label>2 Year License</label>
                </value>
                <value>
                    <fullName>3 Year License</fullName>
                    <default>false</default>
                    <label>3 Year License</label>
                </value>
                <value>
                    <fullName>Ad-Hoc</fullName>
                    <default>false</default>
                    <label>Ad-Hoc</label>
                </value>
                <value>
                    <fullName>Annual Licence</fullName>
                    <default>false</default>
                    <label>Annual Licence</label>
                </value>
                <value>
                    <fullName>Annual Licence &lt; $100K</fullName>
                    <default>false</default>
                    <label>Annual Licence &lt; $100K</label>
                </value>
                <value>
                    <fullName>Annual Support &amp; Maintenance</fullName>
                    <default>false</default>
                    <label>Annual Support &amp; Maintenance</label>
                </value>
                <value>
                    <fullName>Batch</fullName>
                    <default>false</default>
                    <label>Batch</label>
                </value>
                <value>
                    <fullName>Batch - Annually</fullName>
                    <default>false</default>
                    <label>Batch - Annually</label>
                </value>
                <value>
                    <fullName>Batch - Monthly</fullName>
                    <default>false</default>
                    <label>Batch - Monthly</label>
                </value>
                <value>
                    <fullName>Batch - One Time</fullName>
                    <default>false</default>
                    <label>Batch - One Time</label>
                </value>
                <value>
                    <fullName>Batch - Quarterly</fullName>
                    <default>false</default>
                    <label>Batch - Quarterly</label>
                </value>
                <value>
                    <fullName>Bulk Processing</fullName>
                    <default>false</default>
                    <label>Bulk Processing</label>
                </value>
                <value>
                    <fullName>Bureau</fullName>
                    <default>false</default>
                    <label>Bureau</label>
                </value>
                <value>
                    <fullName>Commission</fullName>
                    <default>false</default>
                    <label>Commission</label>
                </value>
                <value>
                    <fullName>Consultancy</fullName>
                    <default>false</default>
                    <label>Consultancy</label>
                </value>
                <value>
                    <fullName>Consultancy &lt; $100K</fullName>
                    <default>false</default>
                    <label>Consultancy &lt; $100K</label>
                </value>
                <value>
                    <fullName>Consultancy &lt; $60K</fullName>
                    <default>false</default>
                    <label>Consultancy &lt; $60K</label>
                </value>
                <value>
                    <fullName>Consultancy &gt;= $60K</fullName>
                    <default>false</default>
                    <label>Consultancy &gt;= $60K</label>
                </value>
                <value>
                    <fullName>Cost Per Acquisition</fullName>
                    <default>false</default>
                    <label>Cost Per Acquisition</label>
                </value>
                <value>
                    <fullName>Customisation</fullName>
                    <default>false</default>
                    <label>Customisation</label>
                </value>
                <value>
                    <fullName>Customisation &lt; $100K</fullName>
                    <default>false</default>
                    <label>Customisation &lt; $100K</label>
                </value>
                <value>
                    <fullName>Data</fullName>
                    <default>false</default>
                    <label>Data</label>
                </value>
                <value>
                    <fullName>Data Conversion</fullName>
                    <default>false</default>
                    <label>Data Conversion</label>
                </value>
                <value>
                    <fullName>Data Licence</fullName>
                    <default>false</default>
                    <label>Data Licence</label>
                </value>
                <value>
                    <fullName>Deferred Licence</fullName>
                    <default>false</default>
                    <label>Deferred Licence</label>
                </value>
                <value>
                    <fullName>Digital Annual Licence</fullName>
                    <default>false</default>
                    <label>Digital Annual Licence</label>
                </value>
                <value>
                    <fullName>Digital Enhancement</fullName>
                    <default>false</default>
                    <label>Digital Enhancement</label>
                </value>
                <value>
                    <fullName>Digital Licence</fullName>
                    <default>false</default>
                    <label>Digital Licence</label>
                </value>
                <value>
                    <fullName>Digital Licence (Usage Based)</fullName>
                    <default>false</default>
                    <label>Digital Licence (Usage Based)</label>
                </value>
                <value>
                    <fullName>Digital List</fullName>
                    <default>false</default>
                    <label>Digital List</label>
                </value>
                <value>
                    <fullName>Enhancement</fullName>
                    <default>false</default>
                    <label>Enhancement</label>
                </value>
                <value>
                    <fullName>External Clicks</fullName>
                    <default>false</default>
                    <label>External Clicks</label>
                </value>
                <value>
                    <fullName>External Clicks - Post Pay</fullName>
                    <default>false</default>
                    <label>External Clicks - Post Pay</label>
                </value>
                <value>
                    <fullName>Hardware</fullName>
                    <default>false</default>
                    <label>Hardware</label>
                </value>
                <value>
                    <fullName>Hosting</fullName>
                    <default>false</default>
                    <label>Hosting</label>
                </value>
                <value>
                    <fullName>Implementation Fee</fullName>
                    <default>false</default>
                    <label>Implementation Fee</label>
                </value>
                <value>
                    <fullName>Initial Software Fee</fullName>
                    <default>false</default>
                    <label>Initial Software Fee</label>
                </value>
                <value>
                    <fullName>Internal Clicks</fullName>
                    <default>false</default>
                    <label>Internal Clicks</label>
                </value>
                <value>
                    <fullName>Licence</fullName>
                    <default>false</default>
                    <label>Licence</label>
                </value>
                <value>
                    <fullName>Licence (Usage based)</fullName>
                    <default>false</default>
                    <label>Licence (Usage based)</label>
                </value>
                <value>
                    <fullName>List</fullName>
                    <default>false</default>
                    <label>List</label>
                </value>
                <value>
                    <fullName>List Rentals</fullName>
                    <default>false</default>
                    <label>List Rentals</label>
                </value>
                <value>
                    <fullName>Maintenance</fullName>
                    <default>false</default>
                    <label>Maintenance</label>
                </value>
                <value>
                    <fullName>Maintenance &lt; $100K</fullName>
                    <default>false</default>
                    <label>Maintenance &lt; $100K</label>
                </value>
                <value>
                    <fullName>Managed Service</fullName>
                    <default>false</default>
                    <label>Managed Service</label>
                </value>
                <value>
                    <fullName>Minimum Spend</fullName>
                    <default>false</default>
                    <label>Minimum Spend</label>
                </value>
                <value>
                    <fullName>Minimum Spend / Licence</fullName>
                    <default>false</default>
                    <label>Minimum Spend / Licence</label>
                </value>
                <value>
                    <fullName>Monthly Fee</fullName>
                    <default>false</default>
                    <label>Monthly Fee</label>
                </value>
                <value>
                    <fullName>Multi - year Licence</fullName>
                    <default>false</default>
                    <label>Multi - year Licence</label>
                </value>
                <value>
                    <fullName>One-off Project</fullName>
                    <default>false</default>
                    <label>One-off Project</label>
                </value>
                <value>
                    <fullName>Online / PAYG</fullName>
                    <default>false</default>
                    <label>Online / PAYG</label>
                </value>
                <value>
                    <fullName>Per Click</fullName>
                    <default>false</default>
                    <label>Per Click</label>
                </value>
                <value>
                    <fullName>Per Day</fullName>
                    <default>false</default>
                    <label>Per Day</label>
                </value>
                <value>
                    <fullName>Per Match Fee</fullName>
                    <default>false</default>
                    <label>Per Match Fee</label>
                </value>
                <value>
                    <fullName>Perpetual Licence</fullName>
                    <default>false</default>
                    <label>Perpetual Licence</label>
                </value>
                <value>
                    <fullName>Per Project</fullName>
                    <default>false</default>
                    <label>Per Project</label>
                </value>
                <value>
                    <fullName>Per Record</fullName>
                    <default>false</default>
                    <label>Per Record</label>
                </value>
                <value>
                    <fullName>Per Seat</fullName>
                    <default>false</default>
                    <label>Per Seat</label>
                </value>
                <value>
                    <fullName>Per Session</fullName>
                    <default>false</default>
                    <label>Per Session</label>
                </value>
                <value>
                    <fullName>Professional Services</fullName>
                    <default>false</default>
                    <label>Professional Services</label>
                </value>
                <value>
                    <fullName>Professional Services &lt; 5 days</fullName>
                    <default>false</default>
                    <label>Professional Services &lt; 5 days</label>
                </value>
                <value>
                    <fullName>Professional Services &gt; $100k</fullName>
                    <default>false</default>
                    <label>Professional Services &gt; $100k</label>
                </value>
                <value>
                    <fullName>Professional Services &gt; 20 days</fullName>
                    <default>false</default>
                    <label>Professional Services &gt; 20 days</label>
                </value>
                <value>
                    <fullName>Professional Services - 11-20 days</fullName>
                    <default>false</default>
                    <label>Professional Services - 11-20 days</label>
                </value>
                <value>
                    <fullName>Professional Services 5-10 days</fullName>
                    <default>false</default>
                    <label>Professional Services 5-10 days</label>
                </value>
                <value>
                    <fullName>Proof of concept</fullName>
                    <default>false</default>
                    <label>Proof of concept</label>
                </value>
                <value>
                    <fullName>Reports</fullName>
                    <default>false</default>
                    <label>Reports</label>
                </value>
                <value>
                    <fullName>Retainer / Min Commitment # Days</fullName>
                    <default>false</default>
                    <label>Retainer / Min Commitment # Days</label>
                </value>
                <value>
                    <fullName>Royalties</fullName>
                    <default>false</default>
                    <label>Royalties</label>
                </value>
                <value>
                    <fullName>Server Charge</fullName>
                    <default>false</default>
                    <label>Server Charge</label>
                </value>
                <value>
                    <fullName>Service Charge</fullName>
                    <default>false</default>
                    <label>Service Charge</label>
                </value>
                <value>
                    <fullName>Service Charge - Day</fullName>
                    <default>false</default>
                    <label>Service Charge - Day</label>
                </value>
                <value>
                    <fullName>Set up Fee - Day</fullName>
                    <default>false</default>
                    <label>Set up Fee - Day</label>
                </value>
                <value>
                    <fullName>Software Sale</fullName>
                    <default>false</default>
                    <label>Software Sale</label>
                </value>
                <value>
                    <fullName>Subscription</fullName>
                    <default>false</default>
                    <label>Subscription</label>
                </value>
                <value>
                    <fullName>Subscription &lt;= $20K</fullName>
                    <default>false</default>
                    <label>Subscription &lt;= $20K</label>
                </value>
                <value>
                    <fullName>Subscription &gt;= $20K</fullName>
                    <default>false</default>
                    <label>Subscription &gt;= $20K</label>
                </value>
                <value>
                    <fullName>Training</fullName>
                    <default>false</default>
                    <label>Training</label>
                </value>
                <value>
                    <fullName>Transactional</fullName>
                    <default>false</default>
                    <label>Transactional</label>
                </value>
                <value>
                    <fullName>Transactional - Fixed</fullName>
                    <default>false</default>
                    <label>Transactional - Fixed</label>
                </value>
                <value>
                    <fullName>Transactional - Usage</fullName>
                    <default>false</default>
                    <label>Transactional - Usage</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Billing Product</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Product__c</columns>
        <columns>Simple_or_Complex__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Billing Product Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Billing Products</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Product__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Simple_or_Complex__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>LAST_UPDATE</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Product__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Simple_or_Complex__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>LAST_UPDATE</lookupDialogsAdditionalFields>
        <searchResultsAdditionalFields>Product__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Simple_or_Complex__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>LAST_UPDATE</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
