<!--
/**=====================================================================
 * Experian
 * Name: CommunityCaseDetailPage
 * Description: Custom page to show Case Detail
 * Created Date: April 2016
 * Created By: Richard Jospeh
 *
 * Date Modified		Modified By			Description of the update
 * 2017-04-14			Will Hemmerick		Case 02143993: Removal of "Error Type/Root Cause" and "Area" Fields
 * 2017-07-10			Will Hemmerick		Case 13362149: Replacement of legacy Logo
 * =====================================================================*/
 -->

<apex:page Sidebar="false" standardController="Case" extensions="CommunityCaseDetailCntrl">
    <apex:stylesheet value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <apex:includeScript Value="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"/>
    <apex:includeScript value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"/>   
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'/>
    
    <Style>
nav {
    font-size: 12px;
    font-family: Arial,Helvetica,sans-serif;
}           
.customheader {
    background-image: url("{!URLFOR($Resource.CommunityResource,'dotted-background-small.png')}") , url("{!URLFOR($Resource.CommunityResource,'hp-heroblue2.jpg')}");
    background-position: left top, right bottom;
    background-size: 100%;
    background-repeat: no-repeat, repeat;

}
.brandZeronaryBgr{
    background-image: url("{!URLFOR($Resource.CommunityResource,'dotted-background-small.png')}") , url("{!URLFOR($Resource.CommunityResource,'hp-heroblue2.jpg')}");
    background-position: left top, right bottom;
    background-size: 100%;
    background-repeat: no-repeat, repeat;
    opacity: 0.9;
}
.addscroll {
      overflow:scroll;
}
#wrapping
 {
white-space: pre-wrap; /* css-3 */
white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
white-space: -pre-wrap; /* Opera 4-6 */
white-space: -o-pre-wrap; /* Opera 7 */
word-wrap: break-word; /* Internet Explorer 5.5+ */

} 

#newcomments {
    display: none;
}        
        
        
    </Style>
    <script type="text/javascript">
            $j = jQuery.noConflict();

            $j(document).ready(function() {
                $j('#closeBtn').click(function(e){
                    doCloseCase();
                });
            });
            function doCloseCase() {
                $j('#closeBtn').attr("disabled", true);
                var helpfulArticles = $j("input[name=chkboxArticle]:checked").map(function () {return this.value;}).get().join(",");
                closeThisJS(
                    helpfulArticles
                );
            }
            function enableSubmit(){
                $j('#closeBtn').removeAttr("disabled");
            }
            function controlCommentBody(){
                $j('[id$="commentBtn"]').attr('disabled',true);
                $j('[id$="commentBody"]').on('keyup', function(i,e){
                    if ($j(this).val().length == 0){
                        $j('[id$="commentBtn"]').attr('disabled',true);
                    }else{
                        $j('[id$="commentBtn"]').removeAttr('disabled');
                    }
                });
            }
            $j(document).ready(function() {
                // limit case reason to 1000 characters
                $j('textarea.form-control').attr('maxlength',1000);
                // show more/less if content is > 1000 characters
                var showChar = 1000;
                var ellipsestext = "...";
                var moretext = "<br />more";
                var lesstext = "<br />less";
                $j('.more').each(function() {
                    var content = $j(this).children();
                    if(!content.is('script')){
                        content = $j(this).children().html();
                        if(content == null){
                           content = $j(this).html();
                            showChar = 250
                        }
                        if(content.length > showChar) {
                            var offset = content.substr(showChar, content.length - showChar).indexOf("</a>") + 4;
                            var c = content.substr(0, showChar + offset);
                            var h = content.substr(showChar + offset, content.length - (showChar + offset));
                            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span>
    <span class="morecontent">
        <span>' + h + '</span><a href="" class="morelink">' + moretext + '</a>
    </span>';
                            $j(this).html(html);
                        }
                    }
                });
                $j(".morelink").click(function(){
                    if($j(this).hasClass("less")) {
                        $j(this).removeClass("less");
                        $j(this).html(moretext);
                    } else {
                        $j(this).addClass("less");
                        $j(this).html(lesstext);
                    }
                    $j(this).parent().prev().toggle();
                    $j(this).prev().toggle();
                    return false;
                });
                controlCommentBody();
            });
</script>
<script type="text/javascript">
function unhideComments() {
    document.getElementById("newcomments").style.display = "inherit";
}
</script>    
    
<div style="font-family: 'Open Sans', sans-serif; font-size: 12px;" class="panel panel-default customheader ">

    <div class="panel-body">


        <div class="container">
            <div class="well">
                <apex:messages />
                <apex:message />
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-2" >
                            <img class="img-responsive" style=" margin: 0 auto;" src="{!URLFOR($Resource.CommunityResource,'EDQ-NewLogo-60h.png')}" />
                        </div>
                        <div class="col-sm-8" >
                            <h4>
                                <p class="text-center">Case {!case.CaseNumber}</p>
                            </h4>
                            <br/>
                            <br/>
                        </div>
                        <div class="col-sm-2" />
                    </div>
                    <div class="row">
                        <div class="col-sm-1" />
                        <div class="col-sm-10" >
                            <div class="container-fluid">
        
                                <div class="row">
                                    <div class="col-sm-3" >
                                        <p style="color:#aaaaaa; font-weight: bold;text-align:right; " for="usr">Status:</p>
                                    </div>
                                    <div class="col-sm-3" >
                                        <apex:outputfield value="{!case.Case_Status__c}" />
                                    </div>
                                    <div class="col-sm-3" >
                                        <p style="color:#aaaaaa; font-weight: bold;text-align:right; " for="usr">Created:</p>
                                    </div>
                                    <div class="col-sm-3" >    
                                        <apex:outputfield value="{!case.Createddate}" />                                            
                                    </div>
                                </div>

                                <apex:outputPanel rendered="{!Case.IsClosed}" layout="block" styleClass="row">
                                    <div class="col-sm-3" >
                                        <p style="color:#aaaaaa; font-weight: bold;text-align:right; " for="usr">Reason:</p>
                                    </div>
                                    <div class="col-sm-3" >
                                        <apex:outputfield value="{!case.Reason}" />
                                    </div>
                                    <div class="col-sm-3" >
                                        <p style="color:#aaaaaa; font-weight: bold;text-align:right; " for="usr">Secondary Reason:</p>
                                    </div>
                                    <div class="col-sm-3" >    
                                        <apex:outputfield value="{!case.Secondary_Case_Reason__c}" />                                          
                                    </div>
                                </apex:outputPanel>

                                <apex:outputPanel rendered="{!Case.IsClosed}" layout="block" styleClass="row">
                                    <div class="col-sm-3" >
                                        <p style="color:#aaaaaa; font-weight: bold;text-align:right; " for="usr">Solutions:</p>
                                    </div>
                                    <div class="col-sm-9" >
                                        <apex:outputfield value="{!case.Solutions__c}" />
                                    </div>
                                </apex:outputPanel>
                                <div class="row" >
                                    <p/>
                                </div>
                                    
                                <div class="row" >
                                    <div class="col-sm-3" >
                                        <p style="color:#aaaaaa; font-weight: bold;text-align:right; " for="usr">Subject:</p>
                                    </div>
                                    <div class="col-sm-9" >
                                        <apex:outputfield value="{!case.Subject}" />
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-sm-3" >
                                        <p style="color:#aaaaaa; font-weight: bold;text-align:right; " for="usr">Product/Service:</p>
                                    </div>
                                    <div class="col-sm-9" >
                                        <apex:outputfield value="{!case.Asset.Name}" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3" >
                                        <p style="color:#aaaaaa; font-weight: bold; text-align:right; " for="usr">Description:</p>
                                    </div>
                                    <div class="col-sm-9" >
                                        <apex:outputfield value="{!case.Description}" />
                                    </div>
                                </div>

                            </div>


                        </div>
                        <div class="col-sm-1" />
                    </div>


                    <div class="hrhd">

                        <div class="panel-body detail-body">

                            <div class="row">
                                <div class="col-md-1"/>
                                <div class="col-md-10">
                                    <apex:outputPanel id="commentsList" styleClass="panel panel-default" layout="block">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">
                                                <b>Case Comments</b>
                                            </h3>
                                        </div>

                                        <div class="case-comment">
                      
                                            <apex:outputPanel layout="block" styleClass="row hdcasefield" rendered="{!(case.IsClosed) == False}">
                                                    <apex:outputPanel layout="block" rendered="{!(case.IsClosed) == False}" styleClass="divider text-center">
                                                        <button class="btn btn-primary" onclick="unhideComments()">Add Comment</button>
                                                    </apex:outputPanel>                                                                      
                                                <div id="newcomments">
                                                <apex:form styleClass="form-horizontal" rendered="{!(case.IsClosed) == False}">
                                         
                                                    <div class="col-sm-12 text-center">
                                                        <br/>
                                                        <apex:inputField id="commentBody" styleClass="form-control caseCommentShort" value="{!aCComment.CommentBody}" />
                                                    
                                                        <br/>
                                                        <apex:commandButton onclick="this.disabled='disabled'" status="fetchStatus2" action="{!saveNewComment}" value="Save Comment" style="font-family: 'Open Sans', sans-serif; font-size: 10px;" styleClass="btn btn-primary" rerender="commentsList"  id="commentBtn" oncomplete="controlCommentBody()" /> 
                                                    
                                                        <apex:actionStatus id="fetchStatus2" startText="Saving" stopText="" styleClass="label label-info"  />
                                                    </div>
                                                        
                                                </apex:form>
                                                </div>
                                               
                                            </apex:outputPanel>
                                          
                                            <apex:outputPanel id="commentErrorContainer" layout="block" styleClass="row" >
                                                <div class="col-xs-12  ">
                                                    <apex:messages styleClass="errorContainer"/>
                                                </div>
                                            </apex:outputPanel>
                                            <apex:outputpanel id="commentsListDetail">           
                                                <apex:pageBlock id="CaseCommentBlock" html-class="addscroll" >

                                                    <apex:pageBlockTable columnsWidth="70%,30%" title="Comments" value="{!Comments}" var="c" style="table-layout:fixed;width:100%;">
                                                        <apex:column headervalue="Comments" value="{!c.commentbody}" style="word-wrap:break-word;">            
                                                        </apex:column>
                                                        <apex:column headervalue="Created" value="{!c.createdby.alias} ({!c.createddate})"/>
                                                    </apex:pageBlockTable>

                                                    <!-- input value=" Add New Comment " class="btn" name="newComment" onclick="navigateToUrl('/business/apex/CommunityNewComment?parent_id={!id}&retURL=/business/Apex/CommunityCaseDetailPage?id={!id}',null,'newComment');" title="New Case Comments" type="button"/-->             
                                                </apex:pageBlock>              
                                            </apex:outputpanel>                
                                        </div>        
                                    </apex:outputPanel>


                                </div>
                                <div class="col-md-1"/>
                            </div>

                            <div class="row">
                                <div class="col-md-1"/>
                                <div class="col-md-10">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                        <apex:outputPanel rendered="{!(case.IsClosed) == False}">
                                   
                                            <b>Add Attachment</b>
                                            </apex:outputPanel>
                                        </h3>
                                    </div>
                                    <apex:outputPanel rendered="{!(case.IsClosed) == False}">
                                    <c:DragnDropFileUploader onCommunity="true" parentId="{!Case.Id}" multiple="true" retUrl="{!$Site.Prefix}/apex/CommunityCaseDetailPage?id={!Case.Id}"/> 
                                    <!--<apex:iframe scrolling="true" height="250px" src="{!$Site.Prefix}/apex/CommunityDragDropFileUploadCase?core.apexpages.devmode.url=1&Id={!Case.Id}" />   -->
                                    </apex:outputPanel>
                                </div>
                                <div class="col-md-1"/>
                            </div>

                            <div class="row">
                                <div class="col-md-1"/>
                                <div class="col-md-10">

                                    <apex:outputPanel id="attachments" styleClass="panel panel-default" layout="block">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">
                                                <b>Attachments</b>
                                            </h3>
                                        </div>    
                                        <apex:pageBlock > 
                                            <apex:pageBlockTable title="Attachments" value="{!case.CombinedAttachments}" var="att">
                                                <apex:column styleClass="text-center" headerValue="Del" rendered="{!Case.IsClosed == FALSE}">
                                                    <apex:outputLink value="{!URLFOR($Action.Attachment.Delete,att.Id,[retURL=$Site.Prefix&'/'&$CurrentPage.name&'?id='&Case.Id])}" rendered="{!$User.Id == att.createdby.id}">
                                                        <span class="glyphicon glyphicon-remove text-danger text-center" title="Click to Delete"/> 
                                                    </apex:outputLink>
                                                    <apex:outputtext rendered="{!$User.Id <> att.createdby.id}">
                                                                <span class="glyphicon glyphicon-ban-circle text-center" title="Unable to Delete"/> 
                                                            </apex:outputtext>
                                                        </apex:column>

                                                        <apex:column headervalue="File Name">
                                                            <a href="{!$Site.Prefix}/servlet/servlet.FileDownload?file={!att.Id}" target="_blank">{!att.Title}</a>
                                                        </apex:column>
                                                        <apex:column headervalue="Created" value="{!att.createdby.CommunityNickname} ({!att.createddate})"/>

                                                    </apex:pageBlockTable>
                                                </apex:pageBlock>
                                            </apex:outputPanel>     
                                        </div>
                                        <div class="col-md-1"/>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-1"/>
                                        <div class="col-md-10">

                                            <apex:outputPanel id="Emails" styleClass="panel panel-default" layout="block">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">
                                                        <b>Email Messages</b>
                                                    </h3>
                                                </div>    
                                                <apex:pageBlock > 
                                                    <apex:pageBlockTable columnsWidth="75%,25%" title="Emails" value="{!case.EmailMessages}" var="eml">
                                                        <apex:column headervalue="Subject" value="{!eml.Subject}" /> 
                                                        <apex:column headervalue="Sent" value="{!eml.createddate}"/>

                                                    </apex:pageBlockTable>
                                                </apex:pageBlock>
                                            </apex:outputPanel>     
                                        </div>
                                        <div class="col-md-1"/>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="text-center">
                                                <a href="{!$Site.Prefix}/Communitycaselistpage" >
                                                    <input type="submit" class="btn-danger btn-default" value="Close"/>
                                                </a>
                                            </p>
                                        </div>
                                    </div>  

                                </div>
                            </div>     


                        </div>
                    </div>
                </div>
            </div>


        </div>


    </apex:page>