<!-- 
/**=====================================================================
 * Appirio, Inc
 * Name: AccountSegment
 * Description: T-372669 - Page used to override the "View" standard behavior
 * 
 * Created Date: March 22nd, 2015
 * Created By: Nathalie Le Guay (Appirio)
 *
 * Date Modified            Modified By           Description of the update
 * Apr 28th, 2015           Nathalie Le Guay      Added Histories related list
 * Aug 19th, 2015           Parul Gupta           T-427373 - Added required attribute 
 * Aug 26th, 2015           Parul Gupta           I-177549 - Added rerender attribute in Cancel button
 * Sep 03rd, 2015           Parul Gupta           I-178954 - FI Legacy DI Standalone field should be read only for NON Data Admin
 * Sep 11th, 2015           Arpita Bose           T-433360: updated to make Edit button disable for LATAM Serasa Sales
 * Apr 24th, 2016           Paul Kissick          Case 01912570: Fixing javascript onload function
 * Aug 2nd, 2016            Paul Kissick          CRM2:W-005332: Adding Product GBL/BL fields
 * Jun 1st, 2017            Manoj Gopu            W-008338 Checking for the Object access and displaying the Opp and Order Relatedlist 
 ======================================================================*/
 -->
<apex:page standardController="Account_Segment__c" extensions="AccountSegmentController" title="{!title}">
  <apex:includeScript value="/support/console/28.0/integration.js"/>
  <apex:pageBlock id="thePage" title="{!title}" mode="maindetail">
    <apex:detail subject="{!segment.Id}" relatedList="false" title="true" inlineEdit="true" showChatter="true"/>
    <apex:pageBlockSection title="Product Business Line Splits">
      <apex:pageBlockSection title="Pipeline Global Business Line Splits" columns="1" collapsible="false">
        <apex:outputField value="{!Account_Segment__c.Pipeline_GBL_Credit_Services__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_GBL_Credit_Services__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_GBL_Decision_Analytics__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_GBL_Decision_Analytics__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_GBL_Experian_Consumer_Services__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_GBL_Experian_Consumer_Services__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_GBL_Marketing_Services__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_GBL_Marketing_Services__c))}" />
      </apex:pageBlockSection>
      <apex:pageBlockSection title="Won Global Business Line Splits" columns="1" collapsible="false">
        <apex:outputField value="{!Account_Segment__c.Won_GBL_Credit_Services__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_GBL_Credit_Services__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_GBL_Decision_Analytics__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_GBL_Decision_Analytics__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_GBL_Experian_Consumer_Services__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_GBL_Experian_Consumer_Services__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_GBL_Marketing_Services__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_GBL_Marketing_Services__c))}" />
      </apex:pageBlockSection>
      <apex:pageBlockSection title="Pipeline Business Line Splits" columns="1" collapsible="false">
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Automotive__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Automotive__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Business_information__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Business_information__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Consumer_Information__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Consumer_Information__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Healthcare__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Healthcare__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Serasa_Authenticate__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Serasa_Authenticate__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Software__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Software__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Identity_and_Fraud__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Identity_and_Fraud__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Analytics__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Analytics__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Professional_and_Consulting__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Professional_and_Consulting__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Other__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Other__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_MicroAnalytics__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_MicroAnalytics__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Cross_Channel_Marketing__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Cross_Channel_Marketing__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Experian_Data_Quality__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Experian_Data_Quality__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Consumer_Insights__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Consumer_Insights__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Targeting__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Targeting__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_FootFall__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_FootFall__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Consumer_Direct__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Consumer_Direct__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Affinity__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Affinity__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_BL_Decision_Analytics__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_BL_Decision_Analytics__c))}" />
      </apex:pageBlockSection>
      <apex:pageBlockSection title="Won Business Line Splits" columns="1" collapsible="false">
        <apex:outputField value="{!Account_Segment__c.Won_BL_Automotive__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Automotive__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_Business_information__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Business_information__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_Consumer_Information__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Consumer_Information__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_Healthcare__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Healthcare__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_Serasa_Authenticate__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Serasa_Authenticate__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_Software__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Software__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_Identity_and_Fraud__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Identity_and_Fraud__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_Analytics__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Analytics__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_Professional_and_Consulting__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Professional_and_Consulting__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_Other__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Other__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_MicroAnalytics__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_MicroAnalytics__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_Cross_Channel_Marketing__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Cross_Channel_Marketing__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_Experian_Data_Quality__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Experian_Data_Quality__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_Consumer_Insights__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Consumer_Insights__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_Targeting__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Targeting__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_FootFall__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_FootFall__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_Consumer_Direct__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Consumer_Direct__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_Affinity__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Affinity__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_BL_Decision_Analytics__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_BL_Decision_Analytics__c))}" />
      </apex:pageBlockSection>
      <apex:pageBlockSection title="Pipeline Serasa Splits" columns="1" collapsible="false">
        <apex:outputField value="{!Account_Segment__c.Pipeline_Serasa_BI__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_Serasa_BI__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_Serasa_CI__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_Serasa_CI__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_Serasa_DA__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_Serasa_DA__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_Serasa_ECS__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_Serasa_ECS__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_Serasa_eID__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_Serasa_eID__c))}" />
        <apex:outputField value="{!Account_Segment__c.Pipeline_Serasa_MS__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Pipeline_Serasa_MS__c))}" />
      </apex:pageBlockSection>
      <apex:pageBlockSection title="Won Serasa Splits" columns="1" collapsible="false">
        <apex:outputField value="{!Account_Segment__c.Won_Serasa_BI__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_Serasa_BI__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_Serasa_CI__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_Serasa_CI__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_Serasa_DA__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_Serasa_DA__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_Serasa_ECS__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_Serasa_ECS__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_Serasa_eID__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_Serasa_eID__c))}" />
        <apex:outputField value="{!Account_Segment__c.Won_Serasa_MS__c}" rendered="{!NOT(ISNULL(Account_Segment__c.Won_Serasa_MS__c))}" />
      </apex:pageBlockSection>
    </apex:pageBlockSection>
  </apex:pageBlock>
  <apex:form >
    <apex:pageBlock mode="maindetail" title="{!segmentMapEntryName}">
      <apex:pageMessages id="pgMsgs"/>
      <apex:pageBlockButtons location="top">
        <apex:outputPanel id="buttons">
          <apex:commandButton value="Edit" action="{!edit}" rerender="segmentSpecific, buttons" id="editButton"  disabled="{!IF(segmentMapEntryName == 'LATAM Serasa Sales', true, false)}" rendered="{!NOT(isEdit)}"/>
          <apex:commandButton value="Save" action="{!save}" rerender="segmentSpecific, buttons, pgMsgs" id="saveButton"  rendered="{!isEdit}"/>
          <apex:commandButton value="Cancel" action="{!cancel}" rerender="segmentSpecific, buttons, pgMsgs" id="cancelButton" rendered="{!isEdit}"/>
        </apex:outputPanel>
      </apex:pageBlockButtons>
      <apex:outputPanel id="segmentSpecific">
        <apex:pageBlockSection rendered="{!NOT(isEdit)}">
          <apex:outputLabel value="There are no data specific to this segment" rendered="{!fieldSet == null}"/>
          <apex:pageBlockSectionItem dataStyle="text-align: right;" rendered="{!fieldSet != null}"> 
            <apex:outputPanel >
            </apex:outputPanel>
          </apex:pageBlockSectionItem>
          <apex:pageBlockSectionItem />
          <apex:repeat value="{!fieldSet}" var="field">
            <apex:outputField value="{!segment[field.fieldPath]}">
              <!-- <apex:inlineEditSupport event="ondblClick"  showOnEdit="saveButton,cancelButton" hideOnEdit="editButton" /> -->
            </apex:outputField>
          </apex:repeat>
        </apex:pageBlockSection>
        <apex:pageBlockSection title="Segment Specific Section {!IF(segmentMapEntryName==null, '', segmentMapEntryName)}" rendered="{!isEdit}">
          <apex:pageBlockSectionItem dataStyle="text-align: right;" rendered="{!fieldSet != null}" />
          <apex:pageBlockSectionItem />
          <apex:repeat value="{!fieldSet}" var="field">
            <apex:inputField value="{!segment[field.fieldPath]}" required="{!OR(field.required, field.dbrequired)}" rendered="{!field.Label != 'FI Legacy DI Standalone'}"/>
            <apex:inputField value="{!segment[field.fieldPath]}" required="{!OR(field.required, field.dbrequired)}" rendered="{!isDataAdmin && field.Label == 'FI Legacy DI Standalone'}"/>
            <apex:outputField value="{!segment[field.fieldPath]}" rendered="{!!isDataAdmin && field.Label == 'FI Legacy DI Standalone'}"/>
             <!-- <apex:inputField value="{!segment[field.fieldPath]}"/> -->
          </apex:repeat>
        </apex:pageBlockSection>
      </apex:outputPanel>
    </apex:pageBlock>
  </apex:form>
  <apex:relatedList list="CombinedAttachments" />
  <apex:relatedList list="Opportunities_Global_Lines_of_Business__r" rendered="{!segment.Segment_Type__c == 'Global Business Line'}"/>
  <apex:relatedList list="Opportunities_Business_Lines__r" rendered="{!segment.Segment_Type__c == 'Business Line'}"/>
  <apex:relatedList list="Opportunities_Business_Units__r" rendered="{!segment.Segment_Type__c == 'Business Unit'}"/>
  <apex:relatedList list="Opportunities_Regions__r" rendered="{!segment.Segment_Type__c == 'Region'}"/>
  <apex:relatedList list="Opportunities_Countries__r" rendered="{!IF(segment.Segment_Type__c == 'Country' && $ObjectType.Opportunity.accessible,TRUE,FALSE)}"/>

  <apex:relatedList list="Orders_Global_Business_Lines__r" rendered="{!segment.Segment_Type__c == 'Global Business Line'}"/>
  <apex:relatedList list="Orders_Business_Lines__r" rendered="{!segment.Segment_Type__c == 'Business Line'}"/>
  <apex:relatedList list="Orders_Business_Units__r" rendered="{!segment.Segment_Type__c == 'Business Unit'}"/>
  <apex:relatedList list="Orders_Regions__r" rendered="{!segment.Segment_Type__c == 'Region'}"/>
  <apex:relatedList list="Orders_Countries__r" rendered="{!IF(segment.Segment_Type__c == 'Country' && $ObjectType.Order__c.accessible,TRUE,FALSE)}"/>

  <apex:relatedList list="Child_Account_Segments__r"/>
  
  <apex:pageBlock title="Account Segment History">
    <apex:pageblocktable value="{!segment.Histories}" var="hist">
      <apex:column value="{!hist.CreatedDate}"/>
      <apex:column value="{!hist.Createdby.Name}"/>
      <apex:column value="{!hist.Field}"/>
      <apex:column value="{!hist.OldValue}"/>
      <apex:column value="{!hist.NewValue}"/>
    </apex:pageblocktable>
  </apex:pageBlock> 
  <!-- T-427203 - Set tab icon and tab title for Fraud and Identity CEM Console -->
  <script type="text/javascript">
  var setTabIcon = function() {       
    sforce.console.setTabIcon('/img/icon/custom51_100/measuringTape16.png', null);    
  };
  var loadPageFunction = function() {
    if (sforce.console.isInConsole()) {
      sforce.console.setTabTitle('{!JSENCODE(title)}'); 
      setTimeout('setTabIcon()', '500');
    }  
  }
  if (window.addEventListener) {
    window.addEventListener('load', loadPageFunction)
  }
  else {
    window.attachEvent('onload', loadPageFunction)
  }
  </script>
</apex:page>