<!--
/**=====================================================================
 * Experian
 * Name: ManyThanksActionBS
 * Description: VF Action to allow saying thanks to multiple people in the 
                process. Saves a lot of time if posting for a group of people.
                Bootstrap version with better layout for mobile.
 * Created Date: 2 Nov 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 * =====================================================================*/
 -->
<apex:page controller="ManyThanksController" docType="html-5.0" title="Thanks" >
  <apex:remoteObjects jsNamespace="RemoteObjectModel">
    <apex:remoteObjectModel name="User" fields="Id,Name,IsActive,Title,SmallPhotoUrl,LastViewedDate">
      <apex:remoteObjectField name="Active_Oracle_Manager__c" jsShorthand="OracleManager" />
    </apex:remoteObjectModel>
    <apex:remoteObjectModel name="CollaborationGroup" fields="Id,Name,IsArchived">
    </apex:remoteObjectModel>
  </apex:remoteObjects>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <apex:stylesheet value="//fonts.googleapis.com/css?family=Roboto" />
  <apex:stylesheet value="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
  <apex:stylesheet value="{!URLFOR($Resource.Bootstrap337, 'css/bootstrap-ns.min.css')}" />
  <apex:includeScript value="{!$Resource.jquery_1_12_4}" />
  <script>
    var $j = jQuery.noConflict();
  </script>
  <apex:includeScript value="{!URLFOR($Resource.Bootstrap337, 'js/bootstrap.min.js')}" />
  <apex:includeScript value="{!$Resource.BootstrapTypeahead}"/>
  <apex:includeScript value="/canvas/sdk/js/publisher.js" />
  <style>
    .egc {
      font-family: "Roboto", Helvetica, Arial, sans-serif !important;
    }
    .thumbnail {
      cursor: pointer;
    }
    .badgeSelected {
      border-width: 2px !important;
      border-color: #26478d !important;
    }
    .searchingImage {
      background-image: url('data:image/gif;base64,R0lGODlhEAAQAPUkAPLy8tvb2+fn5/j4+Pr6+vn5+eDg4P8gEM/Pz//Dv/Hx8fz8/NbW1v8+MP+ln/9NQP/h3/+If+Pj4+rq6uzs7P8RAOXl5dnZ2czMzPX19fVhVtTU1OKJgvZwZvb29vnq6d+GgP3g3dHR0d7e3v/Sz+fY19jY2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFCAAkACwAAAAAEAAQAAAGgUCSkJShWI4WSmY4XEyQUMtkMSQYotEORGi4jLBHTaVBUlzOIwFgMAAIxJWKQ3K+BArM0CFeedQvFExCEXwVf0uCCYWHgiSKfH+BgoR8dGcBHkwfe3EPZmciCBYAABYIIHwOJF0iGK6vrqhkJAQbsLcYHFtCBQy4rgx4ghQXsIBMQQAh+QQFCAAaACwAAAAAEAAQAAAGXkCNUKOQXC7D5JBgOB6VyoDzaKlWBYvk9GK1TpSXACUz7JKTgyRBYKVAlYDuG93F2DGA+fCOseiFF3cIZ0IkShR8QwkRBxVKDHx2FZOTSQUIkZSTB3+aDRCdFQ8OSUEAIfkEBQgAJAAsAAAAABAAEAAABmRAknAovFwkCqJyaDQaCMPCRtl0DhkYEbE6GlIwYK0wQwl0mWAMwkMsKNMYy/KdBszv84FlL8eTAHx9Qx9EIQJ8FEQgBxEJQxp8GUMgFZUVRJATQxyWlUodC0MQDZ1+Qg4PnnNBACH5BAUIABoALAAAAAAQABAAAAZgQI1QQ7lgMMPksMA4HpVKhPN4qVYDhOQUY7UalBiEBTDsKpSZ5CBglUCVRes7melW7pXEfIivRPZCD3gHJElZSQ59QwMAAhZKDX13FpSUSRAHkpWUAoCbEwuAJRYUaUNBACH5BAUIACQALAAAAAAQABAAAAZmQJJwKMRgLhSicmg0MgpDCEfZNG6GjQqIWBUNHZXwVgiwILzDR7hy+BA9ynUlsoyvE/V8PXPpX/RCFH5/RCFEBQF+EkQaAgADQyN+CkMaFpcWRJIGQx2Yl0oGBEMLE5+AQhkUmHVBACH5BAUIABoALAAAAAAQABAAAAZeQI1Q43hUKsPkENI4HpXKg/OIqVYRheS0YrUylJVDJDHsUpSkZAZhvUCVlu47Ceha7pbBfIi3APZCFHgCBEl6an1DGRQBbkkTfXcXk5NJCwKRlJMBgJoGhXuTEgpJQQAh+QQFCAAkACwAAAAAEAAQAAAGZkCScCisVB4OonJoNDYgw0VH2TRyhhOLhlgFDTOW8FaYiBy8Q0rYIggRP8q1BbCMrwf1fB2A6WP0QhZ+f0QFRB4IfhdEIwEUGUMifhRDIxeXi5EYDEMGmJdKG4ZCBJ6YgEIKEqB1QQAh+QQFCAAaACwAAAAAEAAQAAAGX0CNUJOhWCzD5HAxOR6VSoHzWKlWD5Dk1GK1NpQWAWAw7DqUhCTpYH1AlZHuO5noXu6XzHyIv1D2QhJ4AWRDekkKfUMAFggYSgZ9FyYYlZVJBAGSlpUIgJwMBZ8YfklBADs=') !important;
      background-position: right;
      background-repeat: no-repeat; 
    }
  </style>
  <div class="egc container-fluid" id="manyThanksCanvas">
    <apex:form >
      <apex:inputHidden value="{!badgeId}" id="selectedBadgeId" />
      <apex:actionFunction action="{!addBadges}" name="submitToSf" />
      <div class="row">
        <div class="col-xs-12">
          <div class="form-group well well-sm" id="whoGroup">
            <apex:outputPanel id="checkStatus" layout="block" styleClass="row">
              <apex:repeat value="{!userIdMap}" var="k">
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class=" ">
                    <apex:input type="text" 
                          styleClass="form-control who-field" 
                          value="{!userNameMap[k]}" 
                          html-placeholder="{!$Label.ManyThanks_Person_Placeholder}" 
                          html-data-provide="typeahead" 
                          html-autocomplete="off" 
                          html-readonly="readonly" /><!-- TODO: Replace placeholder with label -->
                    <apex:inputHidden id="userIdStore" value="{!userIdMap[k]}" />
                  </div>
                </div>
              </apex:repeat>
              <div class="col-md-4 col-sm-6 col-xs-12">
                <div>
                  <apex:actionStatus id="addAnotherStatus" >
                    <apex:facet name="start"><button class="btn btn-default btn-sm" disabled="disabled">&nbsp; &nbsp;<img src="/img/loading.gif" alt="Loading" />&nbsp; &nbsp;</button></apex:facet>
                    <apex:facet name="stop">
                      <apex:commandButton id="addAnotherButton" 
                                    styleClass="btn-primary btn-sm experianDarkBlue"
                                    status="addAnotherStatus" 
                                    value=" {!$Label.ManyThanks_Add} + " 
                                    action="{!addAnother}"
                                    rerender="checkStatus"
                                    oncomplete="initTypeaheads();"
                      />
                    </apex:facet>
                  </apex:actionStatus>
                </div>
              </div>
            </apex:outputPanel>
            <div class="help-block alert alert-danger hide" role="alert">Please choose someone to thank..</div><!-- TODO: Change to label -->
          </div>
          <div class="panel panel-default panel-sm">
            <div class="panel-heading">
              <h3 class="panel-title">{!$ObjectType.WorkBadge.label}</h3>
            </div>
            <div class="panel-body">
              <apex:repeat value="{!allBadges}" var="b">
                <apex:outputPanel layout="block" styleClass="media" rendered="{!b.Id == badgeId}">
  	              <div class="pull-left">
	                  <img id="selectedBadgeImage" class="media-object" src="{!b.ImageUrl}" alt="{!$ObjectType.Nomination__c.label}" style="width: 64px; height:64px;" />
                  </div>
                  <div class="media-body" >
                    <p id="selectedBadgeDesc">{!b.Name}: {!b.Description}</p>
                  </div>
    	          </apex:outputPanel>
	            </apex:repeat>
            </div>
            <div class="panel-footer"><button class="btn btn-default" onclick="showBadgeSelector(); return false;" >Change Badge</button></div>
          </div>
          <div class="well well-sm" style="display: none;" id="badgeSelector">
            <div>
              <apex:repeat value="{!allBadges}" var="b">
                <apex:outputPanel layout="block" styleClass="media thumbnail {!IF(b.Id == badgeId,'badgeSelected','')}" onclick="selectBadge(this);" html-data-badgeid="{!b.Id}" html-data-badgedesc="{!b.Name}: {!b.Description}" html-data-badgeurl="{!b.ImageUrl}">
                  <div class="pull-left">
                    <img class="media-object" src="{!b.ImageUrl}" alt="{!$ObjectType.Nomination__c.label}" style="width: 64px; height:64px;" />
                  </div>
                  <div class="media-body">
                    <p>{!b.Name}: {!b.Description}</p>
                  </div>
                </apex:outputPanel>
              </apex:repeat>
            </div>
          </div>
          <div class="form-group well well-sm" id="collabGroupGroup">
            <apex:outputPanel layout="block" styleClass="row">
              <div class="col-xs-12">
                <div class=" ">
                  <apex:input type="text" 
                          styleClass="form-control group-field" 
                          value="{!groupNameMap[0]}" 
                          html-placeholder="{!$Label.ManyThanks_Post_To_Group_Placeholder}" 
                          html-data-provide="typeahead" 
                          html-autocomplete="off" 
                          html-readonly="readonly" /><!-- TODO: Replace placeholder with label -->
                  <apex:inputHidden id="groupIdStore" value="{!groupIdMap[0]}" />
                </div>
              </div>
            </apex:outputPanel>
          </div>
          <div class="form-group well well-sm" id="justifSection">
            <apex:inputTextarea html-placeholder="Type your thanks message here..." id="justifText" value="{!thanksMessage}" styleClass="form-control" rows="5" />
            <div class="help-block alert alert-danger hide" role="alert">Please add a message.</div><!-- TODO: Change to label -->
          </div>
        </div>
      <script>
        
var resultMap = [];
var groupMap = [];

var bdgFld = document.getElementById('{!$Component.selectedBadgeId}');
var justTxtId = document.getElementById('{!$Component.justifText}');

var selectBadge = function(bField) {
  $j('#badgeSelector .thumbnail').removeClass('badgeSelected');
  $j(bField).addClass('badgeSelected');
  $j(bdgFld).val($j(bField).data('badgeid'));
  $j('#selectedBadgeImage').attr('src',$j(bField).data('badgeurl'));
  $j('#selectedBadgeDesc').html($j(bField).data('badgedesc'));
  $j('#badgeSelector').animate({ height: 'hide', opacity: 'hide' }, 'fast');
}; 

var showBadgeSelector = function() {
  $j('#badgeSelector').animate({ height: 'show', opacity: 'show' }, 'fast');
};
        
$j().ready(function(){
  initTypeaheads();
  if ((typeof sforce != 'undefined') && sforce && (!!sforce.one)) {
    Sfdc.canvas.publisher.subscribe({name: "publisher.showPanel", onData:function(e) {
      Sfdc.canvas.publisher.publish({name:"publisher.setValidForSubmit", payload:"true"});
    }});
    Sfdc.canvas.publisher.subscribe({ name: "publisher.post",
      onData: function(e) {
      if (submitForm()) {
        alert('Thanks posted.');
        Sfdc.canvas.publisher.publish({ name: "publisher.close", payload:{ refresh:"true" }});
      }
    }}); 
  }
});

var submitForm = function() {
  // before we can submit to the server, check all the required fields...

  var errorsFound = false;
  $j('.has-error').removeClass('has-error');
  $j('div.help-block').addClass('hide');
  
  if ($j(justTxtId).val() === "") {
    $j('#justifSection').addClass('has-error').find('div.help-block').removeClass('hide');
    errorsFound = true;
  }
  
  if (whoCount() == 0) {
    $j('#whoGroup').addClass('has-error').find('div.help-block').removeClass('hide');
    errorsFound = true;
  }

  if (errorsFound === false) {
    submitToSf();
  }
  return !errorsFound;
};

var whoCount = function() {
  var checkCount = 0;
  $j('#whoGroup').find('input[id$="userIdStore"]').each(function (i,v){
    if ($j(v).val() !== "") {
      checkCount++;
    }
  });
  return checkCount;
};


var initTypeaheads = function() {
  $j('.who-field').typeahead({
     highlighter: function(item) {
       return item;
     },
     updater: function(i) {
       $j(this.$element.context).parent().find('[id$=userIdStore]').val(i.id);
       $j(this.$element.context).attr('readonly','readonly');
       return i.name;
     },
     sorter: function(items) { return items; },
     fitToElement: true,
     delay: 100,
     source: function(q, p) {
       var u = new RemoteObjectModel.User();
       $j(this.$element.context).addClass('searchingImage');
       u.retrieve({
           where: {
             and: {
               Name: {like: '%'+q+'%'},
               IsActive: {eq: true}
             }
           },
           limit: 100,
           orderby: [{LastViewedDate: 'DESC NULLS LAST'},{Name : 'ASC'}]
         },
         function (err, records) {
           if (err !== null) {
             alert(err);
             return;
           }
           var list = [];
           $j.each(records, function() {
             var item = {
               id: this.get('Id'),
               name: this.get('Name'),
               title: this.get('Title'),
               image: this.get('SmallPhotoUrl')
             };
             resultMap[item.id] = item;
             list.push(item);
           });
           $j('.searchingImage').removeClass('searchingImage');
           return p(list);
         }
       );
     },
     minLength: 3
   }).removeAttr('readonly');
   
   $j('.group-field').typeahead({
     highlighter: function(item) {
       return item;
     },
     updater: function(i) {
       $j(this.$element.context).parent().find('[id$=groupIdStore]').val(i.id);
       $j(this.$element.context).attr('readonly','readonly');
       return i.name;
     },
     sorter: function(items) { return items; },
     fitToElement: true,
     delay: 100,
     source: function(q, p) {
       var u = new RemoteObjectModel.CollaborationGroup();
       $j(this.$element.context).addClass('searchingImage');
       u.retrieve({
           where: {
             and: {
               Name: {like: '%'+q+'%'},
               IsArchived: {eq: false}
             }
           },
           limit: 100,
           orderby: [{Name : 'ASC'}]
         },
         function (err, records) {
           if (err !== null) {
             alert(err);
             return;
           }
           var list = [];
           $j.each(records, function() {
             var item = {
               id: this.get('Id'),
               name: this.get('Name')
             };
             groupMap[item.id] = item;
             list.push(item);
           });
           $j('.searchingImage').removeClass('searchingImage');
           return p(list);
         }
       );
     },
     minLength: 3
   }).removeAttr('readonly');
 };
        </script>
      </div>
    </apex:form>
  </div>
</apex:page>