<!--
/**=====================================================================
 * Name: FetchOppsListFromAllChildAcnts
 * Description: This page is for fetching all the opportunities from an account and children below it
 * Created Date: July 12th, 2016
 * Created By: Tyaga pati
 * 
 * Date Modified          Modified By           Description of the update
 * 27th July 2016         Cristian Torres       CRM2:W-005383: Added support for filters
 * Sep 6th, 2016          Paul Kissick          CRM2:W-005383: Replaced ambiguous 'FieldKey' reference, and tidied up page.
 * Oct 24th, 2016         Manoj Gopu             CRM2:W-005616: Added export button to the page.
 * =====================================================================*/
-->
<apex:page standardController="Account" extensions="FetchOppsListFromAllChildAcnts" title="Opportunities from Child Accounts">
  <script>function setFocusOnLoad() {}</script> <!--  HACK TO PREVENT SHOWING DATE PICKER ON LOAD -->
  <apex:includeScript value="/xdomain/xdomain.js" />
  <apex:includeScript value="/soap/ajax/26.0/connection.js" />
  <apex:includeScript value="/support/console/26.0/integration.js" />
  <apex:form >
    <apex:sectionHeader title="{!$ObjectType.Account.Label}" subtitle="{!Account.Name}" />
    <apex:pageBlock id="wholePage" Title="{!$ObjectType.Opportunity.labelPlural} from Child Accounts">
      <apex:pageBlockButtons location="top">
        <apex:commandButton action="{!backToAccount}" value="Back to Account" />
      </apex:pageBlockButtons>
      <apex:pageBlockSection >
        <apex:pageBlockSectionItem >
          <apex:outputLabel >Search by Date Range</apex:outputLabel>
          <apex:inputField id="startDate1" value="{!opp1.Mash_Approval_Date__c}" />
        </apex:pageBlockSectionItem>
        <apex:pageBlockSectionItem >
          <apex:outputLabel >Search by Type</apex:outputLabel>
          <apex:inputField id="type1" value="{!opp1.Type}"/>
        </apex:pageBlockSectionItem>
        <apex:pageBlockSectionItem >
          <apex:outputLabel />
          <apex:inputField id="endDate1" value="{!opp1.Contract_End_Date__c}" />
        </apex:pageBlockSectionItem>
        <apex:pageBlockSectionItem >
          <apex:outputLabel >Search by Stage</apex:outputLabel>
          <apex:inputField id="stage1" value="{!opp1.StageName}" required="false" />
        </apex:pageBlockSectionItem>
        <apex:pageBlockSectionItem >
          <apex:outputPanel layout="block" style="text-align: right;">
            <apex:commandButton value="Search" action="{!queryChildOpps}" rerender="wholePage" status="fetchStatus" />
            <apex:commandButton value="Export" action="{!exportToExcel}" status="fetchStatus" />
          </apex:outputPanel>
        </apex:pageBlockSectionItem>
      </apex:pageBlockSection>
      <apex:pageBlockSection columns="1">
        <apex:pageBlockTable value="{!opps}" var="o" >
          <!-- ACCOUNT NAME -->
          <apex:column >
            <apex:facet name="header">   
              <apex:commandLink action="{!queryChildOpps}" value="{!$ObjectType.Account.fields.Name.label}{!IF(sortExpression=='account.name',IF(sortDirection='ASC','▼','▲'),'')}" id="accSort" status="fetchStatus" rerender="wholePage">
                <apex:param value="account.name" name="column" assignTo="{!sortExpression}" ></apex:param>
              </apex:commandLink>
            </apex:facet>
            <apex:outputField value="{!o.Account.Name}" />
          </apex:column>
          <!-- OPP NAME -->
          <apex:column >
            <apex:facet name="header">   
              <apex:commandLink action="{!queryChildOpps}" value="{!$ObjectType.Opportunity.fields.Name.label}{!IF(sortExpression=='name',IF(sortDirection='ASC','▼','▲'),'')}" id="nameSort" status="fetchStatus" rerender="wholePage">
                <apex:param value="name" name="column" assignTo="{!sortExpression}" ></apex:param>
              </apex:commandLink>
            </apex:facet>
            <apex:outputLink value="/{!o.ID}"><apex:outputField value="{!o.Name}" /></apex:outputLink>
          </apex:column>
          <!-- OPP TYPE -->
          <apex:column >
            <apex:facet name="header">   
              <apex:commandLink action="{!queryChildOpps}" value="{!$ObjectType.Opportunity.fields.Type.label}{!IF(sortExpression=='type',IF(sortDirection='ASC','▼','▲'),'')}" id="typeSort" status="fetchStatus" rerender="wholePage">
                <apex:param value="type" name="column" assignTo="{!sortExpression}" ></apex:param>
              </apex:commandLink>
            </apex:facet>
            <apex:outputField value="{!o.Type}"/>
          </apex:column>
          <!-- OPP STAGE -->
          <apex:column >
            <apex:facet name="header">   
              <apex:commandLink action="{!queryChildOpps}" value="{!$ObjectType.Opportunity.fields.StageName.label}{!IF(sortExpression=='StageName',IF(sortDirection='ASC','▼','▲'),'')}" id="stageSort" status="fetchStatus" rerender="wholePage">
                <apex:param value="StageName" name="column" assignTo="{!sortExpression}" ></apex:param>
              </apex:commandLink>
            </apex:facet>
            <apex:outputField value="{!o.stagename}"/>
          </apex:column>
          <!-- OPP AMOUNT -->
          <apex:column style="white-space: nowrap;" >
            <apex:facet name="header">   
              <apex:commandLink action="{!queryChildOpps}" value="{!$ObjectType.Opportunity.fields.Amount.label}{!IF(sortExpression=='Amount_Corp__c',IF(sortDirection='ASC','▼','▲'),'')}" id="amountSort" status="fetchStatus" rerender="wholePage">
                <apex:param value="Amount_Corp__c" name="column" assignTo="{!sortExpression}" ></apex:param>
              </apex:commandLink>
            </apex:facet>
            <apex:outputText value="{0} {1, number, ###,###,##0.00}">
              <apex:param value="{!o.CurrencyIsoCode}" /> 
              <apex:param value="{!o.Amount}" /> 
            </apex:outputText>  
          </apex:column>
          <!-- OPP CLOSE DATE -->
          <apex:column >
            <apex:facet name="header">   
              <apex:commandLink action="{!queryChildOpps}" value="{!$ObjectType.Opportunity.fields.Closedate.label}{!IF(sortExpression=='Closedate',IF(sortDirection='ASC','▼','▲'),'')}" id="dateSort" status="fetchStatus" rerender="wholePage">
                <apex:param value="Closedate" name="column" assignTo="{!sortExpression}" ></apex:param>
              </apex:commandLink>
            </apex:facet>
            <apex:outputField value="{!o.CloseDate}" />
          </apex:column>
          <!-- OPP CURRENT FY REV IMPACT -->
          <apex:column style="white-space: nowrap;" >
            <apex:facet name="header">   
              <apex:commandLink action="{!queryChildOpps}" value="{!$ObjectType.Opportunity.fields.Current_FY_revenue_impact__c.label}{!IF(sortExpression=='Current_FY_revenue_impact__c',IF(sortDirection='ASC','▼','▲'),'')}" id="revSort" status="fetchStatus" rerender="wholePage">
                <apex:param value="Current_FY_revenue_impact__c" name="column" assignTo="{!sortExpression}" ></apex:param>
              </apex:commandLink>
            </apex:facet>
            <apex:outputText rendered="{!o.Current_FY_revenue_impact__c != 0}" value="{0} {1, number, ###,###,##0.00}">
              <apex:param value="{!o.CurrencyIsoCode}" /> 
              <apex:param value="{!o.Current_FY_revenue_impact__c}" /> 
            </apex:outputText>  
          </apex:column>
          <!-- OPP OWNER -->
          <apex:column >
            <apex:facet name="header">   
              <apex:commandLink action="{!queryChildOpps}" value="{!$Label.COR_Owner}{!IF(sortExpression=='owner.name',IF(sortDirection='ASC','▼','▲'),'')}" id="ownerSort" status="fetchStatus" rerender="wholePage">
                <apex:param value="owner.name" name="column" assignTo="{!sortExpression}" ></apex:param>
              </apex:commandLink>
            </apex:facet>
            <apex:outputField value="{!o.Owner.Name}"/>
          </apex:column>
          <!-- OPP OWNER BU -->
          <apex:column >
            <apex:facet name="header">   
              <apex:commandLink action="{!queryChildOpps}" value="{!$ObjectType.Opportunity.fields.Owner_s_Business_Unit__c.label}{!IF(sortExpression=='Owner_s_Business_Unit__c',IF(sortDirection='ASC','▼','▲'),'')}" id="bunitSort" status="fetchStatus" rerender="wholePage">
                <apex:param value="Owner_s_Business_Unit__c" name="column" assignTo="{!sortExpression}" ></apex:param>
              </apex:commandLink>
            </apex:facet>
            <apex:outputField value="{!o.Owner_s_Business_Unit__c}" />
          </apex:column>
          <!-- OPP OWNER SALES TEAM -->
          <apex:column >
            <apex:facet name="header">   
              <apex:commandLink action="{!queryChildOpps}" value="{!$ObjectType.Opportunity.fields.Owner_s_Sales_team__c.label}{!IF(sortExpression=='Owner_s_Sales_team__c',IF(sortDirection='ASC','▼','▲'),'')}" id="steamSort" status="fetchStatus" rerender="wholePage">
                <apex:param value="Owner_s_Sales_team__c" name="column" assignTo="{!sortExpression}" ></apex:param>
              </apex:commandLink>
            </apex:facet>
            <apex:outputField value="{!o.Owner_s_Sales_team__c}" />
          </apex:column>
          <!-- OPP OWNER SALES SUB TEAM -->
          <apex:column >
            <apex:facet name="header">   
              <apex:commandLink action="{!queryChildOpps}" value="{!$ObjectType.Opportunity.fields.Owner_s_Sales_Sub_Team__c.label}{!IF(sortExpression=='Owner_s_Sales_Sub_Team__c',IF(sortDirection='ASC','▼','▲'),'')}" id="ssteamSort" status="fetchStatus" rerender="wholePage">
                <apex:param value="Owner_s_Sales_Sub_Team__c" name="column" assignTo="{!sortExpression}" ></apex:param>
              </apex:commandLink>
            </apex:facet>
            <apex:outputField value="{!o.Owner_s_Sales_Sub_Team__c}" />
          </apex:column>
        </apex:pageBlockTable>        
      </apex:pageBlockSection>
      <div align="center" draggable="false" >
        <apex:panelGrid columns="5">
          <apex:commandButton status="fetchStatus" reRender="wholePage" value="|< {!$Label.First}" action="{!setCon.first}" disabled="{!!setCon.hasPrevious}" title="First Page"/>
          <apex:commandButton status="fetchStatus" reRender="wholePage" value="< {!$Label.Previous}" action="{!setCon.previous}" disabled="{!!setCon.hasPrevious}" title="Previous Page"/>
          <apex:commandButton status="fetchStatus" reRender="wholePage" value="{!$Label.Next} >" action="{!setCon.next}" disabled="{!!setCon.hasNext}" title="Next Page"/>
          <apex:commandButton status="fetchStatus" reRender="wholePage" value="{!$Label.Last} >|" action="{!setCon.last}" disabled="{!!setCon.hasNext}" title="Last Page"/>
          <apex:outputPanel style="color:#4AA02C;font-weight:bold">
            <apex:actionStatus id="fetchStatus" >
              <apex:facet name="start"><apex:image value="/img/loading.gif" /></apex:facet>
              <apex:facet name="stop"><apex:image value="/img/s.gif" width="16" height="16" /></apex:facet>
            </apex:actionStatus>
          </apex:outputPanel>
        </apex:panelGrid>
      </div>
    </apex:pageBlock>
  </apex:form>
</apex:page>