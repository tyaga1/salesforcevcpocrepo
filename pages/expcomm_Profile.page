<apex:page controller="ExpCommunityProfile" showHeader="false" sidebar="false" docType="html-5.0">
<!-- 
Page -        EmployeeCommunity Profile page
Author -      Hay Mun Win
Description - profile view for  Employee Community-->

<html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" lang="en">
<head>
  <meta charset="utf-8" />
  <title>Employee Community</title>
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"/> 


  <apex:stylesheet value="{!URLFOR($Resource.SLDS214, 'assets/styles/salesforce-lightning-design-system.min.css')}" />
   <apex:stylesheet value="{!URLFOR($Resource.EmployeeCommunity_MainRes_2017, 'css/community_mainstyles.css')}" />
  
        
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="{!URLFOR($Resource.EmployeeCommunity_MainRes_2017, 'script/community_script.js')}"></script>
  <script src="{!URLFOR($Resource.EmployeeCommunity_MainRes_2017, 'script/svg4everybody.js')}"></script>  
  <script>svg4everybody();</script>
  
</head>
<body class="slds-m-top--none">

  <!-- REQUIRED SLDS WRAPPER -->
  <div class="experianSLDS">

    <!-- Wrapper -->
<div class="wrapper slds-size--1-of-1">
    <!--Template-->
    <apex:composition template="expcomm_newTemplate">
      
    </apex:composition>
    <!--Template-->
    
    
    <!-- Content Wrapper -->
    <div id="top" class="right-wrapper  slds-small-size--2-of-3 slds-medium-size--3-of-5 slds-large-size--5-of-6 slds-float--right is-shrink">
    
        
        <div class="content-block">
        
            
                        <div class="slds-hide"><chatter:newsfeed /></div>
            <!--Content-->
              <apex:form >     
              <apex:actionFunction name="expprofile_badge_next" status="expprofile_load_status" action="{!badgeNext}" rerender="theBadge"/>
              <apex:actionFunction name="expprofile_badge_prev" status="expprofile_load_status" action="{!badgePrevious}" rerender="theBadge"/>
              <apex:actionFunction name="expprofile_load_following" status="expprofile_load_status" action="{!followingStart}" rerender="allSubscribers"/>
              
              <apex:outputpanel >
                    <apex:actionstatus id="expprofile_load_status">
                        <apex:facet name="start">
                            <div class="expprofile_loading_div"> 
                                <img class="expprofile-loading-logo" src="{!$Resource.exp_loading_logo}"/>
                            </div>
                        </apex:facet>
                    </apex:actionstatus>
                </apex:outputpanel>
                
    <!-- User Profile-->
            <div class="slds-m-vertical--medium" id="expprofile_wrapper">
                <div class="expprofile_container" >
                    
                    <div class="slds-size--1-of-1 slds-small-size--1-of-1 slds-medium-size--8-of-12 slds-large-size--8-of-12 slds-float--left slds-p-horizontal--medium">
                    <div class="expcarding slds-card slds-size--1-of-1 slds-p-around--medium slds-m-right--large">
                        <div class="slds-size--1-of-1 slds-small-size--1-of-1 slds-medium-size--1-of-3 slds-large-size--1-of-3 slds-float--left slds-p-right--medium slds-m-bottom--medium">
                            <div style="display:{!showEditPhoto}"><chatter:userPhotoUpload ></chatter:userPhotoUpload></div>
                            <div style="display:{!showNormalPhoto}"><img id="expprofile_img" class="expprofile_img" src="{!curUser.FullPhotoUrl}"/></div>
                            <div class="expprofile_follow slds-p-vertical--medium slds-p-left--medium"><chatter:follow entityId="{!curUser.Id}"/></div>
                            <div class="slds-p-left--medium" style="display:{!showEditPhoto}"><a class="expprofile_editBtn" href="javascript:;">Edit Profile</a></div>
                        </div>
                            <div class ="slds-size--1-of-1 slds-small-size--1-of-1 slds-medium-size--2-of-3 slds-large-size--2-of-3 slds-float--left"> 
                            <apex:outputPanel id="profileWrapper">   
                                <div class="slds-size--1-of-1 slds-small-size--1-of-1 slds-medium-size--1-of-1 slds-large-size--1-of-2 slds-float--left">
                                    <div class="expprofile_label">First Name</div>
                                    <apex:outputField id="fname" value="{!curUser.FirstName}" styleClass="expprofile_info"/><br/>
                                    
                                    <div class="expprofile_label">Last Name</div>
                                    <apex:outputField value="{!curUser.LastName}" styleClass="expprofile_info"/>
                                    <br/>
                                    
                                    <div class="expprofile_label">Email</div>
                                    <div class="expprofile_info slds-truncate slds-p-right--medium"><a title="{!curUser.Email}" href="{!'mailto:' + curUser.Email}">{!curUser.Email}</a></div><br/>
                                    
                                    <div class="expprofile_label">Mobile Phone</div>
                                    
                                    <apex:outputField value="{!curUser.MobilePhone}" styleClass="expprofile_info"/><br/>
                                    
                                    <div class="expprofile_label">Work Phone</div>
                                        <apex:outputField value="{!curUser.Phone}" styleClass="expprofile_info"/><br/>
                                    
                                  </div> 
                                <div class="slds-size--1-of-1 slds-small-size--1-of-1 slds-medium-size--1-of-1 slds-large-size--1-of-2 slds-float--left">
                                    <div class="expprofile_label">Title</div> 
                                        <apex:outputField value="{!curUser.Title}" styleClass="expprofile_info"/><br/>
                                    
                                    <div class="expprofile_label">Manager</div>
                                    <div class="expprofile_info"><a href="{!$Site.Prefix}/apex/expcomm_Profile?userID={!curUser.ManagerId}">{!curUser.Manager.Name}</a></div><br/>
                                    
                                    <div class="expprofile_label">Address</div> 
                                    <div class="expprofile_info">{!curUser.Street}<br/>{!curUser.City}<apex:outputText rendered="{!NOT(ISBLANK(curUser.City) && ISBLANK(curUser.State))}" value=","/> {!curUser.State}<br/> {!curUser.Country}</div><br/>
                                </div>
                                <div class="slds-size--1-of-1 slds-clearfix"></div>
                                <div class="slds-size--1-of-1 slds-clearfix">
                                    <div class="expprofile_label">About Me </div>
                                        <apex:outputField value="{!curUser.AboutMe}" styleClass="expprofile_info"/><br/>
                                        
                                </div>
                            
                            </apex:outputPanel>
                            </div>   
                        </div>
                        
                        
                    
                    <div class="slds-clearfix slds-card slds-size--1-of-1 slds-p-around--medium slds-m-right--large slds-m-bottom--large" style="clear:both;">
                    
                        <div class="expprofile_label slds-m-vertical--medium">Recognition Badges</div> 
                        <hr/>
                        <apex:outputPanel id="theBadge">
                            <table class="exp_recognition" cellpadding="10">
                            <tr>
                                <th class="slds-p-around--medium">Badge</th>
                                <th class="slds-p-vertical--medium">Message</th>
                                <th class="slds-p-vertical--medium">Given by</th>
                            </tr>
                            
                                <apex:repeat value="{!badgeToShow}" var="badge">
                                    <tr>
                                        <td><img class="badge_icon slds-size--2-of-3" src="{!badge.ImageUrl}" style="max-width:100px; min-width:60px;"/></td>
                                        <td class="slds-p-right--small"><apex:outputText styleclass="slds-m-around--small" value="{!badge.Message}"/></td>
                                        <td><a href="{!$Site.Prefix}/apex/expcomm_Profile?userID={!badge.GiverID}"><apex:outputText value="{!badge.Giver.Name}"/></a></td>
                                    </tr>
                                </apex:repeat>
                                
                                <tr>
                                    <td colspan="3" class="slds-text-align--center">
                                    <apex:commandButton action="{!badgePrevious}" value="<" disabled="{!DisableBadgePrevious}" onclick="callbadge_prev(); return false;"/>  
                                     <apex:commandButton action="{!badgeNext}" value=">" disabled="{!DisableBadgeNext}" onclick="callbadge_next(); return false;" />
                                     <!--<button class="kafs-control-button" id="kafs-prevPageArticles" onclick="callprofile_load(); return false;">Previous</button>
                                     <button class="kafs-control-button" id="kafs-prevPageArticles" onclick="callprofile_load(); return false;">Next</button>-->
                                    </td>
                                </tr>
                                                      
                        </table>
                         
                                 </apex:outputPanel>      
                     
                           
                    </div>
                    </div>
                    
                    <div class="slds-size--1-of-1 slds-small-size--1-of-1 slds-medium-size--4-of-12 slds-large-size--4-of-12  slds-float--left slds-p-horizontal--medium">
                        <div class="slds-card slds-p-around--medium slds-m-top--none slds-m-bottom--medium">
                            <div class="expprofile_label">Followers</div>
                            <a class="expprof_showAll followers" href="javascript:;">Show All</a>
                            <hr class="slds-m-vertical--x-small"/>
                            <apex:outputPanel id="theFollowers">
                            <apex:repeat value="{!followers}" var="follow">
                                <a title="{!follow.subscriber.name}" class="slds-m-right--medium slds-m-bottom--medium" href="{!$Site.Prefix}/apex/expcomm_Profile?userID={!follow.subscriberid}"><img src="{!follow.subscriber.SmallPhotoUrl}" width="45" height="45"/></a>
                            </apex:repeat>
                            
                            
                            </apex:outputPanel>  
                        </div>
                        
                        
                        <div class="slds-card slds-p-around--medium slds-m-top--none slds-m-bottom--medium">
                            
                            <div class="expprofile_label">Following</div> 
                            <apex:commandLink action="{!followingStart}" value="Show All" styleClass="expprof_showAll followings" rerender="allSubscribers" onclick="callFollowing(); return false;" />
                            <hr class="slds-m-vertical--x-small"/>
                            <apex:outputPanel id="theSubscribers">
                            <apex:repeat value="{!displaySubsc}" var="subscribe">
                        
                                <a title="{!subscribe.parent.name}" href="{!$Site.Prefix}/apex/expcomm_Profile?userID={!subscribe.parentID}" class="slds-m-right--medium slds-m-bottom--medium">
                                <img src="{!userWithPhotoUrl[subscribe.parentID].SmallPhotoUrl}" width="45" height="45"/>
                                </a>
                        
                            </apex:repeat>
                            
                            </apex:outputPanel>  
                        </div>
                        
                        <div class="slds-card slds-p-around--medium slds-m-top--none slds-m-bottom--medium">
                            
                            <div class="expprofile_label">Contributions</div> 
                            <hr class="slds-m-vertical--x-small"/>
                            
                            <div class="expContribution">
                                <table cellpadding="10">
                                    <tr>
                                        <td class="expcontribution_cell exp_contribution_border">
                                            <div class="expcontribution_num">{!postCmt}</div>
                                            <div class="expcontribution_text slds-p-vertical--small">posts &amp; comments</div>
                                        </td>
                                        
                                        <td class="expcontribution_cell exp_contribution_border">
                                            <div class="expcontribution_num">{!CommentReceivedCount}</div>
                                            <div class="expcontribution_text slds-p-vertical--small">comments received</div>
                                        </td>
                                        
                                        <td class="expcontribution_cell">
                                            <div class="expcontribution_num">{!LikeReceivedCount}</div>
                                            <div class="expcontribution_text slds-p-vertical--small">likes received</div>
                                        </td>
                                    </tr>
                                </table>
                                
                                <style>
                                    .expcontribution_num {
                                        font-size:1.24em;
                                        line-height:15px;
                                        font-weight:400;
                                        color:#777777;
                                    }
                                    
                                    .expcontribution_text {
                                        font-size:0.92em;
                                        line-height:11px;
                                        color:#777777;
                                        overflow:hidden;
                                        text-overflow:ellipsis;
                                    }
                                    
                                    .expcontribution_cell{
                                        width: 33.333%;
                                        padding: 0 3px 2px;
                                        vertical-align: top;
                                    }
                                    .exp_contribution_border {
                                        border-right-width: 1px;
                                        border-color: #d7dbde;
                                        border-right-style: solid;
                                    }
                                </style>
                            </div>
                            
                        </div>
                        
                        <div class="slds-card slds-p-around--medium slds-m-top--none">
                            
                            <div class="expprofile_label">Groups</div> 
                            <a class="expprof_showAll groups" href="javascript:;">Show All</a>
                            <hr/>
                            <table>
                            
                            <apex:repeat value="{!chatterMember}" var="groups">
                            <tr>
                            <td><a href="/{!groups.CollaborationGroupId}"><img src="{!groups.CollaborationGroup.SmallPhotoUrl}" class="slds-m-bottom--medium" width="45" height="45"/></a></td>
                                <td>
                                    <a href="/{!groups.CollaborationGroupId}"><apex:outputText value="{!groups.CollaborationGroup.name}"/></a><br/>
                                    <apex:outputText value="{!groups.CollaborationGroup.MemberCount} Members"/>
                                </td>
                                <td><chatter:follow entityId="{!groups.id}"/></td>
                            </tr>
                            </apex:repeat>
                            </table>
                        </div>
                        
                    </div>
                    
                </div>
                
                

            </div>
            
                    <div id="expprof_screen" class="">
        <div class="slds-p-bottom--x-large slds-p-horizontal--x-large" id="expprof_paper">
            <span class="xclose slds-p-top--medium slds-p-around--small">x</span>
            <div style="clear:both;"></div>
            <div class="expprof_section expprof_follower_section slds-hide">
                <apex:outputPanel id="allFollowers">
                    <div class="expprof_pop_header">Followers</div>
                    <hr/>
                    <table cellpadding="10px">
                    
                    <apex:repeat value="{!followers}" var="follow">
                    <tr>
                    <td class="expprof_screen_photo expprof_screen_cell slds-p-vertical--x-small">
                        <a class="slds-m-around--small slds-m-left--none" href="{!$Site.Prefix}/apex/expcomm_Profile?userID={!follow.subscriberid}"><img src="{!follow.subscriber.SmallPhotoUrl}" width="30" height="30"/></a>
                    </td>
                    
                    <td class="expprof_screen_cell slds-p-vertical--x-small  slds-p-left--x-small">
                        <a class="slds-m-around--small slds-m-left--none" href="{!$Site.Prefix}/apex/expcomm_Profile?userID={!follow.subscriberid}">
                            {!follow.subscriber.Name}
                        </a><br/>
                        <span class="slds-text-body--small">{!follow.subscriber.title}</span>
                        
                    </td>
                    <td class="expprof_screen_cell exprof_followbtn slds-p-vertical--x-small slds-text-align--right">
                    <chatter:follow entityId="{!follow.subscriberid}"/>
                    </td>
                    </tr>
                    </apex:repeat>
                    
                    </table>
                    <div class="expprof_btn_wrapper">
                    <apex:commandButton action="{!followerPrevious}" value="<" disabled="{!DisableFollowerPrevious}" rerender="allFollowers"/> 
                    <apex:commandButton action="{!followerNext}" value=">" disabled="{!DisableFollowerNext}" rerender="allFollowers"/> 
                    </div>
                </apex:outputPanel>  
                </div>
                
            <!--followings-->
    
            <div class="expprof_section expprof_following_section slds-hide">
                <apex:outputPanel id="allSubscribers">
                    <div class="expprof_pop_header">Following</div>
                    <table cellpadding="10px">
                    
                    <apex:repeat value="{!screenSubsc}" var="subscribe">
                    <tr>
                    <td class="expprof_screen_photo expprof_screen_cell slds-p-vertical--x-small">
                        <a class="slds-m-around--small slds-m-left--none" href="{!$Site.Prefix}/apex/expcomm_Profile?userID={!subscribe.parentID}"><img src="{!userWithPhotoUrl[subscribe.parentID].SmallPhotoUrl}" width="30" height="30"/></a>
                    </td>
                    
                    <td class="expprof_screen_cell slds-p-vertical--x-small  slds-p-left--x-small">
                        <a class="slds-m-around--small slds-m-left--none" href="{!$Site.Prefix}/apex/expcomm_Profile?userID={!subscribe.parentID}">
                            {!subscribe.parent.Name}
                        </a><br/>
                        <span class="slds-text-body--small">{!subscribe.parent.title}</span>
                        
                    </td>
                    <td class="expprof_screen_cell exprof_followbtn slds-p-vertical--x-small slds-text-align--right">
                    <chatter:follow entityId="{!subscribe.parentId}"/>
                    </td>
                    </tr>
                    </apex:repeat>
                    <tr>
                        <td colspan="3" class="slds-text-align--center"><apex:commandButton action="{!followingPrevious}" value="<" disabled="{!DisableFollowingPrevious}" rerender="allSubscribers"/> 
                        <apex:commandButton action="{!followingNext}" value=">" disabled="{!DisableFollowingNext}" rerender="allSubscribers"/> 
                        </td>
                    </tr>
                    </table>
                    
                </apex:outputPanel>  
            </div>
            
        <!-- group -->
        
        <div class="expprof_section expprof_group_section slds-hide">
                <apex:outputPanel id="allGroups">
                    <div class="expprof_pop_header">Groups</div>
                    <table cellpadding="10px">
                    
                    <apex:repeat value="{!chatterMember}" var="groups">
                    <tr>
                    <td class="expprof_screen_photo expprof_screen_cell slds-p-vertical--x-small">
                        <a class="slds-m-around--small slds-m-left--none" href="/{!groups.CollaborationGroupId}"><img src="{!groups.CollaborationGroup.SmallPhotoUrl}" width="30" height="30"/></a>
                    </td>
                    
                    <td class="expprof_screen_cell slds-p-vertical--x-small  slds-p-left--x-small">
                        <a class="slds-m-around--small slds-m-left--none" href="/{!groups.CollaborationGroupId}">
                            {!groups.CollaborationGroup.name}
                        </a><br/>
                        <span class="slds-text-body--small">{!groups.CollaborationGroup.MemberCount} Members</span>
                        
                    </td>
                    <td class="expprof_screen_cell exprof_followbtn slds-p-vertical--x-small slds-text-align--right">
                    </td>
                    </tr>
                    </apex:repeat>
                    
                    </table>
                    <div class="expprof_btn_wrapper">
                    <apex:commandButton action="{!groupPrevious}" value="<" disabled="{!DisableGroupPrevious}" rerender="allGroups"/> 
                    <apex:commandButton action="{!groupNext}" value=">" disabled="{!DisableGroupNext}" rerender="allGroups"/> 
                    </div>
                </apex:outputPanel>  
                </div>
                
            </div>
        </div>
        
            <apex:actionFunction name="followingEnd" action="{!followingEnd}" rerender="allSubscribers"/>
            
            </apex:form>
            <!--Content-->
            
            
            <!-- Footer -->
            
            <!--Footer - Template-->
            <apex:composition template="Community_template_footer"></apex:composition>
            
        
        </div>
        
<div id="expprofile_screen" class="">
        <div class="slds-p-bottom--x-large slds-p-horizontal--x-large" id="expprofile_paper">
            <span class="xclose slds-p-top--medium slds-p-around--small">x</span>
            <div style="clear:both;"></div>
            <div class="expprofile_edit">
            <apex:form >
                <div class="slds-size--1-of-2 slds-float--left">
                    <div class="expprofile_label">First Name</div>
                    <apex:inputField value="{!curUser.FirstName}" styleClass="expprofile_info slds-size--11-of-12"/><br/>
                    
                    <!--<div class="expprofile_label">Mobile Phone</div>
                    
                    <apex:inputField value="{!curUser.MobilePhone}" styleClass="expprofile_info slds-size--11-of-12"/><br/>-->
                    
                    
                  </div> 
                <div class="slds-size--1-of-2 slds-float--left">
                    
                    <div class="expprofile_label">Last Name</div>
                    <apex:inputField value="{!curUser.LastName}" styleClass="expprofile_info slds-size--12-of-12"/>
                    <br/>
                    
                </div>
                
                
                <div class="slds-size--1-of-1 slds-clearfix"></div>
                <div class="slds-size--1-of-1 slds-clearfix">
                    <div class="expprofile_label">About Me </div>
                        <apex:inputField value="{!curUser.AboutMe}" styleClass="expprofile_info slds-size--12-of-12"/><br/>
                        
                </div>
                
                <div class="expprofile_btn_container slds-p-top--small slds-text-align--center">
                    <apex:commandLink action="{!Save}" value="Save All" styleClass="expprofile_save slds-p-horizontal--large slds-p-vertical--medium slds-m-around--large"/>
                    <apex:commandLink action="{!Cancel}" value="Cancel" id="expprofile_cancel" styleClass="expprofile_cancel slds-p-horizontal--large slds-p-vertical--medium slds-m-around--large"/>
                </div>
                                
                </apex:form>
            </div>
        </div>
</div>      
    </div>
</div>


    </div>      <!--Experian Scope-->



</body>
</html>
</apex:page>