<!--
/**=====================================================================
 * Name: AllAccountAttachments
 * Description: This page is used  to display all the attachments related to Account
 * Created Date: Jul 13th,2016
 * Created By: Manoj Gopu
 *
 * Date Modified         Modified By            Description of the update
 * July 13th 2016(QA)    Manoj Gopu             CRM2:W-005491 - Consolidated view of attachments on the account record
 * Sep  06th 2016        Manoj Gopu             CRM2:W-005491 - Added New Labels for Region and Business Unit (I-209)
 * Sep  07th 2016        Manoj Gopu             CRM2:W-005491 - Added view and download options to attachments
 * Sep  21st 2016        Manoj Gopu             CRM2:W-005491 - Added labels for translations
 * Sep  23rd 2016        James Wills            CRM2:W-005491 - Translation Updates
 * Jan  26th 2017		 Sanket Vaidya			Case#02165224 - Order by create date [Added sorting for Type, Record associated to, Created Date, Created By, Creator's Region]
 * =====================================================================*/
-->
<apex:page standardcontroller="Account" extensions="AllAttachments" sidebar="false">
    <apex:form id="PbId">
        <apex:pageBlock title="Attachments">
            <apex:outputPanel rendered="{!lstAttachments.size>0}">
            <div align="left" style="display:{!IF(NOT(ISNull(totallistsize)),'block','none')}">
                <b><font size="1pt" >Page &nbsp;<apex:outputLabel value="{!pageNumber}"/>&nbsp;of&nbsp;<apex:outputLabel value="{!totalPageNumber}"/>&nbsp;&nbsp;</font></b>
                <font size="1pt">Results&nbsp;<apex:outputLabel value="{!PrevPageNumber}"/>&nbsp;to&nbsp;<apex:outputLabel value="{!NxtPageNumber}"/>&nbsp;of&nbsp;<apex:outputLabel value="{!totallistsize}"/>&nbsp;&nbsp;</font>
                <apex:commandButton value="|< {!$Label.First}" action="{!FirstbtnClick}" disabled="{!previousButtonEnabled}" reRender="PbId" status="sts1"></apex:commandButton>
                <apex:commandButton value="< {!$Label.Previous}" action="{!previousBtnClick}" disabled="{!previousButtonEnabled}" reRender="PbId" status="sts1"></apex:commandButton>
                <apex:commandButton value="{!$Label.Next} >" action="{!nextBtnClick}"  disabled="{!nextButtonDisabled}" reRender="PbId" status="sts1"></apex:commandButton>
                <apex:commandButton value="{!$Label.Last} >|"  action="{!LastbtnClick}"  disabled="{!nextButtonDisabled}" reRender="PbId" status="sts1"></apex:commandButton>&nbsp;&nbsp;
            <apex:actionStatus id="sts1" startText="Fetching..." stopText="">
                <apex:facet name="start">
                   <img class="waitingImage" src="/img/loading.gif" title="Please Wait..." />
                </apex:facet>
            </apex:actionStatus>
            </div>

                <apex:pageblockTable value="{!lstAttachmentsData}" var="att">
                     <apex:column headerValue="{!$Label.Attachments_Action}" width="10%">
                        <apex:outputLink value="/{!att.attach.Id}" target="_blank">View</apex:outputLink>
                        <apex:outputPanel rendered="{!att.attach.RecordType == 'Attachment'}">
                            &nbsp;|&nbsp;
                            <apex:outputLink value="/servlet/servlet.FileDownload?file={!att.attach.Id}" target="_blank">Download</apex:outputLink>
                        </apex:outputPanel>
                    </apex:column>
                    <apex:column value="{!att.objectName}">
                        <apex:facet name="header">
                           <apex:commandLink action="{!displayAttachments}" value="{!$Label.Attachments_Object_associated_to}{!IF(sortExpression=='Object Name',IF(sortDirection='ASC','▼','▲'),'')}">
                             <apex:param value="Object Name" name="column" assignTo="{!sortExpression}" ></apex:param>
                           </apex:commandLink>
                         </apex:facet>
                    </apex:column>
                    <!-- ************** -->
                    <apex:column value="{!att.attach.RecordType}">
                        <apex:facet name="header">
                           <apex:commandLink action="{!displayAttachments}" value="{!$ObjectType.CombinedAttachment.fields.RecordType.label}{!IF(sortExpression=='Type',IF(sortDirection='ASC','▼','▲'),'')}">
                             <apex:param value="Type" name="column" assignTo="{!sortExpression}" ></apex:param>
                           </apex:commandLink>
                         </apex:facet>
                    </apex:column>
                     <apex:column >
                        <apex:facet name="header">
                           <apex:commandLink action="{!displayAttachments}" value="{!$ObjectType.CombinedAttachment.fields.Title.label}{!IF(sortExpression=='Title',IF(sortDirection='ASC','▼','▲'),'')}">
                             <apex:param value="Title" name="column" assignTo="{!sortExpression}" ></apex:param>
                           </apex:commandLink>
                         </apex:facet>
                        <apex:outputLink value="/{!att.attach.Id}" target="_blank" rendered="{!att.attach.RecordType != 'File'}">{!att.attach.Title}</apex:outputLink>
                        <apex:outputLink value="/{!att.attach.Id}" target="_blank" rendered="{!att.attach.RecordType == 'File'}">{!att.attach.Title}.{!att.attach.FileExtension}</apex:outputLink>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">
                           <apex:commandLink action="{!displayAttachments}" value="{!$Label.Attachments_Record_Associated_to}{!IF(sortExpression=='RecordAssociatedTo',IF(sortDirection='ASC','▼','▲'),'')}">
                             <apex:param value="RecordAssociatedTo" name="column" assignTo="{!sortExpression}" ></apex:param>
                           </apex:commandLink>
                         </apex:facet>
						<apex:outputLink value="/{!att.attach.ParentId}" target="_blank">{!att.recordName}</apex:outputLink>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">
                           <apex:commandLink action="{!displayAttachments}" value="{!$ObjectType.Account.fields.CreatedDate.label}{!IF(sortExpression=='CreatedDate',IF(sortDirection='ASC','▼','▲'),'')}">
                             <apex:param value="CreatedDate" name="column" assignTo="{!sortExpression}" ></apex:param>
                           </apex:commandLink>
                         </apex:facet>
						<apex:outputText value="{0,date,MM/dd/yyyy}"> <apex:param value="{!att.attach.CreatedDate}" /> </apex:outputText>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">
                           <apex:commandLink action="{!displayAttachments}" value="{!$Label.Attachments_Created_by}{!IF(sortExpression=='CreatedBy',IF(sortDirection='ASC','▼','▲'),'')}">
                             <apex:param value="CreatedBy" name="column" assignTo="{!sortExpression}" ></apex:param>
                           </apex:commandLink>
                         </apex:facet>
						 <apex:outputLink value="/{!att.attach.CreatedById}" target="_blank">{!att.attach.CreatedBy.Name}</apex:outputLink>
                    </apex:column>
                    <apex:column value="{!att.userRegion}" >
                        <apex:facet name="header">
                           <apex:commandLink action="{!displayAttachments}" value="{!$Label.Attachments_Creator_Region}{!IF(sortExpression=='CreatorRegion',IF(sortDirection='ASC','▼','▲'),'')}">
                             <apex:param value="CreatorRegion" name="column" assignTo="{!sortExpression}" ></apex:param>
                           </apex:commandLink>
                         </apex:facet>						 
                    </apex:column>
                     <apex:column value="{!att.userBusiness}">
                        <apex:facet name="header">
                           <apex:commandLink action="{!displayAttachments}" value="{!$Label.Attachments_Creators_BU}{!IF(sortExpression=='Business Unit',IF(sortDirection='ASC','▼','▲'),'')}">
                             <apex:param value="Business Unit" name="column" assignTo="{!sortExpression}" ></apex:param>
                           </apex:commandLink>
                         </apex:facet>
                    </apex:column>
                    <!-- ************** -->
                    <!-- <apex:column value="{!att.attach.RecordType}" headerValue="{!$ObjectType.CombinedAttachment.fields.RecordType.label}" /> 
                    <apex:column >
                        <apex:facet name="header">
                           <apex:commandLink action="{!displayAttachments}" value="{!$ObjectType.CombinedAttachment.fields.Title.label}{!IF(sortExpression=='Title',IF(sortDirection='ASC','▼','▲'),'')}">
                             <apex:param value="Title" name="column" assignTo="{!sortExpression}" ></apex:param>
                           </apex:commandLink>
                         </apex:facet>

                        <apex:outputLink value="/{!att.attach.Id}" target="_blank" rendered="{!att.attach.RecordType != 'File'}">{!att.attach.Title}</apex:outputLink>
                        <apex:outputLink value="/{!att.attach.Id}" target="_blank" rendered="{!att.attach.RecordType == 'File'}">{!att.attach.Title}.{!att.attach.FileExtension}</apex:outputLink>
                    </apex:column>
                    <apex:column headerValue="{!$Label.Attachments_Record_Associated_to}">
                        <apex:outputLink value="/{!att.attach.ParentId}" target="_blank">{!att.recordName}</apex:outputLink>
                    </apex:column>
                    <apex:column headervalue="{!$ObjectType.Account.fields.CreatedDate.label}">
                        <apex:outputText value="{0,date,MM/dd/yyyy}"> <apex:param value="{!att.attach.CreatedDate}" /> </apex:outputText>
                    </apex:column>
                    <apex:column headerValue="{!$Label.Attachments_Created_by}">
                        <apex:outputLink value="/{!att.attach.CreatedById}" target="_blank">{!att.attach.CreatedBy.Name}</apex:outputLink>
                    </apex:column>
                    <apex:column headerValue="{!$Label.Attachments_Creator_Region}" value="{!att.userRegion}"/>
                    <apex:column value="{!att.userBusiness}">
                        <apex:facet name="header">
                           <apex:commandLink action="{!displayAttachments}" value="{!$Label.Attachments_Creators_BU}{!IF(sortExpression=='Business Unit',IF(sortDirection='ASC','▼','▲'),'')}">
                             <apex:param value="Business Unit" name="column" assignTo="{!sortExpression}" ></apex:param>
                           </apex:commandLink>
                         </apex:facet>
                    </apex:column>-->
                </apex:pageblockTable>
            </apex:outputPanel>
            <apex:outputPanel rendered="{!NOT(lstAttachments.size>0)}">
                <center><font color="red"><b>No Records to Display</b></font></center>
            </apex:outputPanel>
        </apex:pageBlock>
    </apex:form>
</apex:page>