<!--
/**=====================================================================
 * Name: AccountPlanTabPanel
 * Description: This is the main page for Account Planning
 * Created Date: March 3rd, 2016
 * Created By: James Wills
 * 
 * Date Modified         Modified By            Description of the update
 * Mar. 3rd,2016         James Wills            Created.
 * May  9th, 2016        James Wills            Case 01959211: AP - Saving Child Objects - Return to correct tab.  Labels also added for tab headings.
 * June 9th, 2016        James Wills            Case 02019045: Information not saving in Account plans (on Client Background Tab)
 * Oct. 21st 2016        James Wills            Case 01983126: Updated titles with Object labels
 * Jan. 30th 2017        James Wills            Case 01999757: Updated PDF button to call build page.
 * =====================================================================*/
-->
<apex:page standardController="Account_Plan__c" extensions="AccountPlanTabPanel" showHeader="true" showChat="true" sidebar="false" tabStyle="Account_Plan__c">

  <apex:stylesheet value="/sCSS/39.0/sprites/Theme3/default/gc/versioning.css"/>   
  <apex:stylesheet value="/sCSS/39.0/sprites/Theme3/default/gc/extended.css"/>

  <!--Account Planning: This style-sheet reference moved from SWOTAnalysis VF Page as it affects other tabs. -->
  <!--apex:stylesheet value="{!URLFOR($Resource.SWOT_Analysis_UI, 'SWOT_Analysis_UI/css/APstyle.css')}"/-->
  
  <apex:form id="frm">
    <!--apex:includeScript value="/support/console/22.0/integration.js"/--> 

    <apex:actionFunction name="setActiveTab_AF" action="{!setAccountPlanCookies}" reRender="">
      <apex:param id="tabName" name="tabName" assignTo="{!currentUserAccountPlanTab}" value=""/>
    </apex:actionFunction> 
     
    <apex:sectionHeader title="{!$ObjectType.Account_Plan__c.label}" subtitle="{!Account_Plan__c.Name}"/> 
    <apex:commandLink action="{!backToAccount}">{!$ObjectType.Account.label}: {!accName}</apex:commandlink>
    <!--Chatter not working properly so this is disabled for the moment-->
    <!--chatter:feedWithFollowers entityId="{!$User.Id}"/-->
    <!--chatter:follow entityId="{!Account_Plan__c.Id}"/-->

    <apex:pageBlock >
      <apex:outputField id="Id" value="{!Account_Plan__c.Id}" rendered="false"/>
      <apex:outputField id="Account__c" value="{!Account_Plan__c.Account__c}" rendered="false"/>
       <apex:outputField id="Account__r" value="{!Account_Plan__c.Account__r.Name}" rendered="false"/>      
       <apex:pageBlockButtons id="detailButtons" location="top">
        <!--Two buttons below disabled for now as more time required for requirements-->
        <!--apex:commandButton action="{!save}" value="Generate Exec Summary" disabled="true" reRender="frm"/-->        
        <apex:commandButton onclick="window.open('/apex/GenerateAccountPlanPDF?id={!Account_Plan__c.Id}','','width=900,height=600');" value="{!$Label.ACCOUNTPLANNING_JOINTACTPLANT_GENERATEPDF}" reRender="frm"/>
        <!--apex:commandButton onclick="window.open('/apex/AccountPlanPDFGenerator?id={!Account_Plan__c.Id}','','width=1200,height=900');" value="{!$Label.ACCOUNT_PLAN_PDF_BUILD}" reRender="frm"/-->
        <apex:commandButton action="{!openAccountPlanPDFGenerator}" value="{!$Label.ACCOUNTPLANNING_PDF_BTN_Custom_PDF}"/>
        <!--apex:commandButton action="{!save}" value="Generate PPT" disabled="true" reRender="frm"/-->
      </apex:pageBlockButtons>
    </apex:pageBlock>
  </apex:form>
  <!--If the switchType is set to client then this is quicker but the Health Status only works on the first tab - 20160329 James Wills -->
  <!--Use the value setting to determine the current tab e.g. value="{!selectedTabValue}-->
  <apex:tabPanel id="accountPlanTabPanel" switchType="server" value="{!currentUserAccountPlanTab}" tabClass="activeTab" inactiveTabClass="inactiveTab" contentStyle="font-size: 12px;">
     <apex:tab label="{!$Label.ACCOUNTPLANNING_ClientOverviewTab}" name="{!$Label.ACCOUNTPLANNING_ClientOverviewTab}" onClick="setTabForPlan('{!$Label.ACCOUNTPLANNING_ClientOverviewTab}')" id="tab1">
       <apex:include pageName="AccountPlanClientOverviewTab"/> 
     </apex:tab>
     <apex:tab label="{!$Label.ACCOUNTPLANNING_ClientBackgroundTab}" name="{!$Label.ACCOUNTPLANNING_ClientBackgroundTab}" onClick="setTabForPlan('{!$Label.ACCOUNTPLANNING_ClientBackgroundTab}')" id="tab2">
       <apex:include pageName="AccountPlanClientBackgroundTab"/> 
     </apex:tab>
     <apex:tab label="{!$Label.ACCOUNTPLANNING_ContactsTab}" name="{!$Label.ACCOUNTPLANNING_ContactsTab}" id="tab3">
       <apex:include id="contactsTab" pageName="AccountPlanContactsTab"/> 
     </apex:tab>
     <apex:tab label="{!$Label.ACCOUNTPLANNING_CompetitionTab}" name="{!$Label.ACCOUNTPLANNING_CompetitionTab}" id="tab4">
       <apex:include pageName="AccountPlanCompetitionTab"/> 
     </apex:tab>
     <apex:tab label="{!$Label.ACCOUNTPLANNING_RevenueAndProductsTab}" name="{!$Label.ACCOUNTPLANNING_RevenueAndProductsTab}" onClick="setTabForPlan('{!$Label.ACCOUNTPLANNING_RevenueAndProductsTab}')" id="tab5">
       <apex:include pageName="AccountPlanRevenueAndProductsTab"/>                                                                     
     </apex:tab>
     <apex:tab label="{!$Label.ACCOUNTPLANNING_SWOTTab}" name="{!$Label.ACCOUNTPLANNING_SWOTTab}" onClick="setTabForPlan('{!$Label.ACCOUNTPLANNING_SWOTTab}')" id="tab6">
       <apex:include pageName="SwotAnalysis"/> 
     </apex:tab>
     <apex:tab label="{!$Label.ACCOUNTPLANNING_StrategyAndActionTab}" name="{!$Label.ACCOUNTPLANNING_StrategyAndActionTab}" onClick="setTabForPlan('{!$Label.ACCOUNTPLANNING_StrategyAndActionTab}')" id="tab7">
       <apex:include pageName="AccountPlanStrategyActionTab"/> 
     </apex:tab>
     <apex:tab label="{!$Label.ACCOUNTPLANNING_OtherResourcesTab}" name="{!$Label.ACCOUNTPLANNING_OtherResourcesTab}" onClick="setTabForPlan('{!$Label.ACCOUNTPLANNING_OtherResourcesTab}')" id="tab8">
       <apex:include pageName="AccountPlanPenetration"/> 
     </apex:tab>
     <apex:tab label="{!$Label.ACCOUNTPLANNING_NotesAndAttachmentsTab}" name="{!$Label.ACCOUNTPLANNING_NotesAndAttachmentsTab}" id="tab9">
       <apex:include pageName="AccountPlanAttachmentsTab"/> 
     </apex:tab>
  </apex:tabPanel>
  <script>
    function setTabForPlan(tab){
      //confirm(tab);
      setActiveTab_AF(tab);      
      return false;
    }
  </script>
</apex:page>