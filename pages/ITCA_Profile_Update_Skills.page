<!--
/**=====================================================================
 * Experian
 * Name: ITCA Profile Update Skills
 * Description: Profile Update skills page for the IT Career Architecture Tool
 * Created Date: July 2017
 * Created By: Alexander McCall
 *
 * Date Modified      Modified By           Description of the update
 * 9th Aug. 2017      James Wills           ITCA:W-009246 Updated to allow editing technical skills.
 * 22nd Aug 2017      Alexander McCall      ITCA Issues I1427 & I1435
 * 22/08/17           Alexander McCall      ITCA Issue I1425
 * 25/08/17           Alexander McCall      ITCA Issues I1426 & I1429
 * 29/08/17           James Wills           ITCA:I1439 - Added explanation for technical specialisms.
 * 29/08/17           James Wills           ITCA:I1426 - Added delete functionality for Career_Architecture_Skills_Plan__c records.
 * 29/08/17           Malcolm Russell       ITCA         
 * 31/08/17           James Wills           ITCA:I1428 - Updated Technical Skills Modal. 
 * 31/08/17           Alexander McCall      ITCA:Issues I1447 & I1452 
 * 31/08/17           Alexander McCall      ITCA:W-009303 Checkbox for public skills/endorsements 
 * 31/08/17           Malcolm Russell       Style fix for radio buttons , Submit Profile to Save skils, remove current and proposed skill level
 * 01/09/17           Alexander McCall      ITCA Issue I1430
 * =====================================================================*/
 -->
<apex:page controller="ITCA_Profile_Builder_controller" showHeader="false" standardStylesheets="true" sidebar="false" docType="html-5.0" action="{!ITCA_Profile_Skill_Update_Load}">

  <title> ITCA </title>
  
  <apex:stylesheet value="{!URLFOR($Resource.SLDS214, 'assets/styles/salesforce-lightning-design-system.min.css')}" />
  <apex:stylesheet value="{!URLFOR($Resource.EmployeeCommunity_MainRes_2017, 'css/community_mainstyles.css')}" />
  <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/bootstrap.exp.min.css')}"/>
  <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/main.css')}"/>
  <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/exp.rebrand.proto.css')}"/>
  <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/itca-styles.css')}"/>
  
   <style>
     .ITCAFooter ul li,ol li{margin-left:1.5em;padding-left:0;}
     .ITCAFooter {font-size: 15px;    font-family: 'Roboto', sans-serif;}
     .header h2 {font-size: 70px;}
     .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {position:absolute;margin-left:-5px;margin-top:4px\9}
   </style> 

  <head>
    <title> ITCA - Sub Skills </title>
    <link rel="stylesheet" type="text/css" href="http://stg1.experian.com/global-styles/content/bootstrap.exp.min.css"/>
    <link rel="stylesheet" type="text/css" href="http://stg1.experian.com/global-styles/content/main.css"/>
    <link rel="stylesheet" type="text/css" href="http://stg1.experian.com/global-styles/content/exp.rebrand.proto.css"/>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="http://stg1.experian.com/site-styles/poc/itca-styles.css"/>
  </head>



  <body class="home">

    <apex:messages id="messages"/>
    

    <!-- Navigation -->
        
    <!-- Navigation -->
    <c:ITCA_Navigation_Banner currentActiveTab1="{!bannerInfoLocal}"/>
    <!-- ../Navigation-->
        

    <!-- Hero Image -->
   <!-- AM Commented Out <div class="banner_container small_marquee" style="background:url(/global-images/hero/line-art-marquee-2000x250.png);filter: progid:DXImageTransform.Microsoft.AlphaImageLoader( src='/global-images/hero/line-art-marquee-2000x250.png', sizingMethod='scale');"> -->
           <div>
              <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-xs-12 header">
                        <div style="margin-top:25px">
                        <h2>Edit your profile</h2><br/><br/>
                        </div>                     
                        <!-- AM Commented Out for I1447 <h3>Current Overall Skill Level: {!userProfileCurrentLevel}</h3> <br/><br/> -->
                    </div>
                </div>
              </div>
           </div>
      <!-- AM Commented out  </div> -->
    <!-- ../Hero Image -->

    <!--Skill Sets Buckets-->
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="row">
            <div class="col-sm-12">
              <h4>Please take some time to update each skill.<br/></h4>
            </div>
            <!--<div class="col-sm-12 selected-skills">
              <p class="text"> Selected Skill Sets: </p>
              <div class="skill-sets">
                <div class="selected-skill" id="software-development">
                  Software Development
                  <i class="fa fa-times" aria-hidden="true"></i>
                </div>
              </div>
            </div>-->
          </div>
        </div>
        <!--   
        <apex:repeat value="{!displayRecords}" var="displayRec"> 
          <apex:outputPanel rendered="{!IF(skillLevel=displayRec.skillName,true,false)}">    
            <apex:selectRadio value="{!displayRec.selectedLevel}">
              <apex:selectOptions value="{!displayRec.skillDescLevel}"/>
            </apex:selectRadio><br/>            
          </apex:outputPanel>                       
         </apex:repeat>
         -->     
         <!-- skill -->
      
        <apex:form >
        
          <!--div class="col-sm-12">          
            <div class="row is-flex">
              <div class="col-sm-8 sub-skills">
                <div class="row">
                  <div class="col-sm-12">
                    <apex:repeat value="{!displayRecords}" var="displayRec">
                      <div class="panel-group" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default system-analysis">
                          <div class="panel-heading" role="tab" id="sub-one">
                            <h4 class="panel-title">
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#sub-collapse-{!displayRec.skillID}" aria-expanded="false" aria-controls="sub-collapse-{!displayRec.skillID}">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                {!displayRec.skillName}
                              </a>
                              <i class="fa fa-times" aria-hidden="true"></i>
                              <div class="skill-acronym"> </div>
                            </h4>
                          </div>
                          <div id="sub-collapse-{!displayRec.skillID}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="sub-collapse-{!displayRec.skillID}">
                            <div class="panel-body">
                              <div class="skill-level">
                                <div class="beginner">
                                  <apex:selectRadio value="{!displayRec.selectedLevel}" styleClass="radio" layout="pageDirection">
                                    <apex:selectOptions value="{!displayRec.skillDescLevel}"/>
                                  </apex:selectRadio>                                  
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </apex:repeat>    
                  </div>
                </div>
              </div>
            </div>
          </div-->  
                    
          <!-- RJ Added  
          <apex:repeat value="{!displayRecords}" var="displayRec">
            <apex:pageBlock title="{!displayRec.skillName}">        
             <apex:selectRadio value="{!displayRec.selectedLevel}">
               <apex:selectOptions value="{!displayRec.skillDescLevel}"/>
             </apex:selectRadio><br/>    
           </apex:pageBlock>
          </apex:repeat>    
          <!-- RJ ended -->  
          
          <div class="col-sm-12">
            <div class="row">
             <!-- <div class="col-sm-12">-->
             <!--   <h4> -->
                  <!-- I1452 We have taken the liberty of providing some predetermined skills based on -->
             <!--     We have added some predetermined skills based on the # Skill Sets selected. Please take some time to add, remove, and rate each skill. -->
             <!--   </h4> -->
             <!-- </div> -->
             <div class="col-sm-12 selected-skills">
               <p class="text"> Your Skills : </p> <!-- AM I1430 -->
                 <apex:repeat value="{!experianSkillSetNameForUser_Set}" var="skillSetToSkill">
                   <div class="skill-sets">
                     <div class="selected-skill" id="system-analysis">
                       {!skillSetToSkill}
                       <!--i class="fa fa-times" aria-hidden="true"--><!--/i-->
                     </div>
                   </div>
                 </apex:repeat>
               </div>
             </div>
           </div>

           <div class="col-sm-12">
             <div class="row is-flex">
               <div class="col-sm-8 sub-skills">
                 <div class="row">
                   <div class="col-sm-12">
                     <div class="panel-group" role="tablist" aria-multiselectable="true">                            
        
                       <apex:actionFunction name="deleteUserSkill_AF" action="{!deleteUserSkill}" reRender="sfiaSkillsListSectionPanel, techSkillsListSectionPanel">
                         <apex:param name="delSkillParam" assignTo="{!userSkillToDelete}" value=""/>
                       </apex:actionFunction>
                       
                       <apex:outputPanel id="sfiaSkillsListSectionPanel">
                       
                       <apex:repeat id="sfiaSkillsListSection" value="{!sfiaSkillsList}" var="displayRec2">

                         <div class="panel-group" role="tablist" aria-multiselectable="true">
                           <div class="panel panel-default">
                             <div class="panel-heading" role="tab" id="technical-one">
                               <h4 class="panel-title">
                                 <a role="button" data-toggle="collapse" data-parent="#accordion" href="#sub-collapse-{!displayRec2.index}" aria-expanded="true" aria-controls="technical-collapse-one">                  
                                   <i class="fa fa-angle-right" aria-hidden="true"/><apex:outputText value=" {!displayRec2.skillName}"/>                  
                                 </a>                            
                                 <!--<apex:outputPanel ><a onclick="deleteUserSkill_JS('{!displayRec2.caspID}');"><i class="fa fa-times" aria-hidden="true"></i></a></apex:outputPanel>-->
                                 <apex:commandlink type="text/html" action="{!deleteUserSkill}"  reRender="sfiaSkillsListSectionPanel, techSkillsListSectionPanel, technicalskillsmodal" >
                                    <i class="fa fa-times" aria-hidden="true"></i> 
                                    <apex:param name="userSkillToDelete" value="{!displayRec2.caspID}" assignTo="{!userSkillToDelete}" />
                                    <apex:param name="userSkillToDeleteSkillID" value="{!displayRec2.skillID}" assignTo="{!userSkillToDeleteSkillID}" />
                                 </apex:commandlink>
                                 <apex:outputPanel ><div class="skill-acronym">{!displayRec2.selectedLevel}</div></apex:outputPanel>
                               </h4>
                              
                             </div>
                             <div id="sub-collapse-{!displayRec2.index}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="sub-collapse-{!displayRec2.skillID}">
                               <div class="panel-body">
                                 <div class="skill-level">
                                   <div class="beginner">
                                     <apex:selectRadio id="radioList" value="{!displayRec2.selectedLevel}" styleClass="radio" layout="pageDirection">
                                       <apex:selectOptions value="{!displayRec2.skillDescLevel}"/>
                                       <apex:actionSupport event="onclick" reRender="submitOutputPanel"/>
                                     </apex:selectRadio>
                                   </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>                   
                      </apex:repeat>
                      </apex:outputPanel>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    
          <!-- Modal Trigger Additional Skills -->
          <!--div class="sfia-skills-button" data-toggle="modal" data-target="#sfia-skills-modal">
            <i class="fa fa-plus-square" aria-hidden="true"></i>Add SFIA Skills
          </div-->
          <!-- ../Modal Trigger Additional Skills -->

          
          <!-- Modal Trigger Additional Skills -->
          <div class="technical-skills-button" data-toggle="modal" data-target="#technical-skills-modal">
            <i class="fa fa-plus-square" aria-hidden="true"></i><a><h4> Add Technical Specialisms</h4></a>
           </div>
          <div class="row">
            <div class="col-sm-12">
              <h4>Technical Skills are about what you DO.  Technical Specialisms are what you KNOW.  Pick your technical specialisms from the list of technical specialisms you know about, and rate your level of responsibility as 4, 5 or 6.</h4>
            </div>
            <div class="col-sm-12 selected-skills">
              <p class="text"> Your Specialisms : </p>     <!-- AM I1430 -->
            </div>
          </div>  

          <!-- ../Modal Trigger Additional Skills -->
          <!-- Extra Skills -->
          <div class="panel-group" role="tablist" aria-multiselectable="true">    
             <apex:outputPanel id="techSkillsListSectionPanel">     
               <apex:repeat id="techSkillsListSection" value="{!techSkillsList}" var="displayRec3">
                 <div class="panel-group" role="tablist" aria-multiselectable="true">
                   <div class="panel panel-default">
                     <div class="panel-heading" role="tab" id="technical-one">
                       <h4 class="panel-title">
                         <a role="button" data-toggle="collapse" data-parent="#accordion" href="#sub-collapse-{!displayRec3.index}" aria-expanded="true" aria-controls="technical-collapse-one">                  
                           <i class="fa fa-angle-right" aria-hidden="true"></i><apex:outputText value=" {!displayRec3.skillName}"/>                  
                         </a>
                         <!-- AM Commented Out <i class="fa fa-times" aria-hidden="true"></i> -->
                         <!--div class="skill-acronym"><apex:outputText value="{!displayRec3.selectedLevel}"/></div>-->
                         <!--<apex:outputPanel ><a onclick="deleteUserSkill_JS('{!displayRec3.caspID}');"><i class="fa fa-times" aria-hidden="true"></i></a></apex:outputPanel>-->
                         <apex:commandlink type="text/html" action="{!deleteUserSkill}"  reRender="sfiaSkillsListSectionPanel, techSkillsListSectionPanel, technicalskillsmodal" >
                                    <i class="fa fa-times" aria-hidden="true"></i> 
                                    <apex:param name="userSkillToDelete" value="{!displayRec3.caspID}" assignTo="{!userSkillToDelete}" />
                                    <apex:param name="userSkillToDeleteSkillID" value="{!displayRec3.skillID}" assignTo="{!userSkillToDeleteSkillID}" />
                         </apex:commandlink>
                         <apex:outputPanel ><div class="skill-acronym">{!displayRec3.selectedLevel}</div></apex:outputPanel>
                       </h4>
                     </div>

                     <div id="sub-collapse-{!displayRec3.index}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="sub-collapse-{!displayRec3.skillID}">
                       <div class="panel-body">
                         <div class="skill-level">
                           <div class="beginner">
                             <apex:selectRadio id="radioList" value="{!displayRec3.selectedLevel}" styleClass="radio" layout="pageDirection">
                               <apex:selectOptions value="{!displayRec3.skillDescLevel}"/>
                               <apex:actionSupport event="onclick" reRender="submitOutputPanel"/>
                             </apex:selectRadio>
                           </div>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>                   
               </apex:repeat>
             </apex:outputPanel>
           </div>
    
           <!-- Submit -->
           <apex:outputPanel id="submitOutputPanel">
            <apex:pageMessages id="pagemessages" />
             <div class="col-sm-12 submit">              
               <!--apex:commandButton id="submitSkillsButton" value="{!$Label.Submit}" action="{!saveToPlan}" styleClass="btn btn-experian-dark-blue"/-->              
               <div>
                 <div class="container">
                   <div class="row">
                     <div class="hero-text col-sm-10 col-xs-12">
                       <!--<h3>Proposed Overall Skill Level: {!userProfileProposedLevel} Change: {!userProfileChange}</h3><br/>-->
                       <h3>Save your Profile.</h3><br/><br/><apex:commandButton id="submitSkillsButton2" value="Save Skills" action="{!saveSkills}" styleClass="btn btn-experian-dark-blue" reRender="pagemessages"/>
                       <br/><br/>
                       <h3>Need to add more skills?</h3><br/>
                      <p> Add new skills to your profile.</p><apex:commandbutton value="Add Skills" onclick="window.location='/apex/ITCA_buildProfile_page'; return false;" styleClass="btn btn-experian-dark-blue"/>
                      <br/><br/>
                      <!-- W-009303 -->   <h3>Allow Endorsements?</h3>
                       <p> Selecting the checkbox will make your skills public and endorseable on your chatter profile. De-selecting this checkbox will make your skills private and result in the deletion of any existing endorsements.
                       &nbsp;                   
                       <apex:repeat id="endorsementSelection" value="{!currentEndorsementPreference}" var="cep">
                            <apex:inputCheckbox value="{!cep.Make_Skills_Public__c}"> 
                                <apex:actionSupport event="onclick" action="{!updateEndorsementChoice}"/>    
                            </apex:inputCheckbox>                      
                       </apex:repeat></p>  
                     </div>
                   </div>
                 </div>
               </div>
             </div>          
           </apex:outputPanel>
           <!-- ../Submit -->       
         
        </apex:form> 
       
        <!-- /skill -->
        
      </div>
    </div>


    <!--Footer-->
    <!--<c:ITCA_Page_Footer />-->
    
    <div class="ITCAFooter">
      <apex:composition template="Community_template_footer"></apex:composition>
    </div>
    <!--../Footer-->

    <!-- Scripts -->
    <!-- Scripts -->
    
     <!-- Modal sfia skills-->
        <!--div class="modal fade" id="sfia-skills-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <apex:form >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Add SFIA skills to your profile</h4>
                    </div>
                    
                    <div class="modal-body">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <input type="text" class="form-control" id="">
                                <div class="input-group-btn">
                                  
                                    <button class="btn btn-default" id="">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </div>
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-12">
                          <apex:repeat id="addTechSkillsListSection" value="{!addSFIASkillsList}" var="addSFIASkill">
                            <div class="sub-skill">
                                <apex:inputCheckbox value="{!addSFIASkill.selectSkill}"/>
                                <label><apex:outputText value="{!addSFIASkill.Name}"/></label>
                            </div>
                          </apex:repeat>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <apex:commandButton value="{!$Label.Submit}" action="{!saveSFIASkills}"/>
                    </div>
                </div>
            </div>
          </apex:form>
        </div-->
    <!-- Modal sfia skills -->
    
    
    <!-- Modal Technical Specialisms-->
        <div class="modal fade" id="technical-skills-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <apex:form id="technicalskillsmodal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Add skills to your profile</h4>
                    </div>
                    <!--apex:pageBlockButtons-->
                    
                    <div class="modal-body">
                        
                        <!--TO DO: Add functionality for searching Technical Specialisms-->
                        <!--div class="col-sm-12">
                            <div class="input-group">
                              <input type="text" class="form-control" id="">
                                <div class="input-group-btn">
                                  <button class="btn btn-default" id="">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                  </button>
                                </div>
                              </input>
                            </div>
                        </div-->
                        <!---->
                        
                        <div class="col-sm-12">
                          <apex:repeat id="addTechSkillsListSection" value="{!addTechSkillsList}" var="addTech">
                            <div class="sub-skill">
                                <apex:inputCheckbox value="{!addTech.selectSkill}"/>
                                <label><apex:outputText value="{!addTech.Name}"/></label>
                            </div>
                          </apex:repeat>
                        </div>
                    </div>
                    <!--/apex:pageBlockButtons-->
                    <div class="modal-footer">
                      <apex:commandButton value="{!$Label.Submit}" action="{!saveTechSkills_ProfileUpdate}"/>
                      
                      <!--button type="button" class="btn btn-primary"/-->
                    </div>
                </div>
            </div>
          </apex:form>
        </div>
    <!-- Modal Technical Specialisms -->

    
    
    
    <!--<script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
    <script type="text/javascript" src="js/jquery.bootstrap-responsive-tabs.min.js"></script>
    <script type="text/javascript" src="js/exp.navbar.js"></script>
    <script type="text/javascript" src="js/floatl.js"></script>
    <script type="text/javascript" src="js/script.js"></script>-->
    
        
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/bootstrap.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.touchSwipe.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.bootstrap-responsive-tabs.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/exp.navbar.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/floatl.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/script.js')}"/>
    <!--.. /Scripts -->
        
    <!-- ITCA Script -->
    <script type="text/javascript">
        $('.panel-title a').click(function(){
            $(this).children('i').toggleClass('fa-angle-right fa-angle-down');
        });
        /*
        $('#software-development i.fa-times').click(function(){
            $('#software-development').addClass('hidden');
            $('.software-development').addClass('hidden');    
        });
        */
        

        
    </script>
    <!-- ../ITCA Script -->
    <!--<script>
        function deleteUserSkill_JS ( skillId) {
          //if( confirm('Are you sure that you want to delete this Profile Skill?') ) {
            deleteUserSkill_AF(skillId);
          //}
          //return false;
        }
                
    </script>-->
    <!--.. /Scripts -->
    
  </body>

</apex:page>