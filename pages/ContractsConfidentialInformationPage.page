<!--
/**=====================================================================
 * Appirio, Inc
 * Name: ContractsConfidentialInformationPage
 * Description: Displays the Opportunity related Confidential Information
 *              on the Contracts detail page
 * Created Date: Feb 3rd, 2015 (Appirio)
 *
 * Date Modified        Modified By                 Description of the update
 * Feb 12th, 2015       Arpita Bose(Appirio)        T-361211 : Commented Account and Membership coulmns and added button for New CI records
 * Feb 13th, 2015       Arpita Bose                 S-277736 : Added CreatedDate in column and commented Add Document button as per chatter
 * Jun 6th, 2016        Paul Kissick                Case 01912505: Adding alert if no account is linked, since CI records fail if there isn't one.
 * Feb 7th,  2017       Sanket Vaidya               Case#02029822 Added membership number and opportunity name.
  =====================================================================*/
-->
<apex:page standardController="Case" extensions="ContractsConfidentialInformationExt" sidebar="{!fullView}" showHeader="{!fullView}" showChat="false">
  <script type="text/javascript">
    function goToConInfo(){
      //alert('URL: {!urlString}');
      
        // If in console view for firefox/chrome, the try block will throw an exception. 
        // The reason why we don't use sforce.console.isInConsole() is because after a new primary tab is opened, the view is no longer recognized as console.
        // Using openConsoleUrl still does not recognize the view as console after execution. 
        try
        {
            window.parent.location.href = '{!urlString}';
        } catch (err)
        {
            sforce.console.openPrimaryTab(null, '{!urlString}', true);
        }
      
        //window.parent.location.href = '{!urlString}';
    }
  </script>
  <apex:sectionHeader title="{!$Label.Confidential_Information_on_Contract_Title}"
    subtitle="{!$Label.Confidential_Information_on_Contract_SubTitle} {!mycase.CaseNumber}" 
    rendered="{!fullView}" />
  <apex:form >
    <apex:outputPanel >
      <apex:commandButton onclick="goToConInfo();" value="{!$Label.Confidential_Information_Add_New_CI}" rendered="{!Case.AccountId != null}"/>
      <apex:pageMessage rendered="{!Case.AccountId == null}" summary="Please add an Account to this case to create a Confidential Information record." strength="1" severity="warning" />
    </apex:outputPanel>
    <apex:pageBlock >
      <div style="height:200px;">
        <apex:pageBlockTable value="{!confInfoList}" var="confInfo">
          <apex:column headerValue="{!$ObjectType.Confidential_Information__c.fields.Name.label}" >
            <apex:outputLink target="_blank" value="/{!confInfo.id}">{!confInfo.Name}</apex:outputLink>
          </apex:column>
          <apex:column value="{!confInfo.Contract_Document__c}" headerValue="{!$ObjectType.Confidential_Information__c.fields.Contract_Document__c.label}" />
          <apex:column value="{!confInfo.Membership__r.Name}"  headerValue="{!$ObjectType.Membership__c.fields.Name.label}" />
          <apex:column value="{!confInfo.Opportunity__r.Name}" headerValue="{!$ObjectType.Confidential_Information__c.fields.Opportunity__c.label}" />
          <apex:column value="{!confInfo.CreatedDate}" headerValue="{!$ObjectType.Confidential_Information__c.fields.CreatedDate.label}" />
        </apex:pageBlockTable>
      </div>
    </apex:pageBlock>
  </apex:form>
</apex:page>