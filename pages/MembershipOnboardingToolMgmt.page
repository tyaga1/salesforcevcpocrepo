<!--
/**=====================================================================
 * Experian
 * Name: MembershipOnboardingToolMgmt
 * Description: Case 01222694 - Management page for the Membership Onboarding Tool
 * Created Date: 17 Jun 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 22nd Sep 2017      Malcolm Russell       Added Document URL
 * =====================================================================*/
 -->
<apex:page controller="MembershipOnboardingToolMgmt" title="Membership Onboarding Tool Management" tabStyle="Membership__c">
  <apex:stylesheet value="/sCSS/sprites/Theme3/default/gc/versioning.css" />
  <apex:stylesheet value="/sCSS/sprites/Theme3/default/gc/extended.css" />
  <style>
  .toolOptions {
    width: 100%;
  }
  </style>
  <apex:sectionHeader title="Membership Onboarding Tool Management" />
  <apex:form >
    <apex:pageMessages id="errorMsgs" />
    <apex:tabPanel switchType="client" selectedTab="optionsTab">
      <apex:tab label="Allowed Options" name="optionsTab" id="optionsTab">
        <apex:pageBlock id="optionsTabBlock" >
          <apex:pageBlockSection columns="2" title="Add New Allowed Option" collapsible="false">
            <apex:pageBlockSectionItem >
              <apex:outputLabel value="{!$ObjectType.Membership__c.fields.Client_Industry__c.label}" />
              <apex:selectList size="5" multiselect="true" value="{!selectedIndustries}" styleClass="toolOptions">
                <apex:selectOptions value="{!ClientIndustry}" />
              </apex:selectList>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
              <apex:outputLabel value="{!$ObjectType.Membership__c.fields.Product_to_be_Sold__c.label}" />
              <apex:selectList size="5" multiselect="true" value="{!selectedProducts}" styleClass="toolOptions">
                <apex:selectOptions value="{!ProductToBeSold}" />
              </apex:selectList>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
              <apex:outputLabel value="{!$ObjectType.Membership__c.fields.Permissible_Purpose__c.label}"/>
              <apex:selectList size="5" multiselect="true" value="{!selectedPurposes}" styleClass="toolOptions">
                <apex:selectOptions value="{!PermissiblePurpose}" />
              </apex:selectList>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
              <apex:outputLabel value="{!$ObjectType.Membership__c.fields.Third_Party_Involvement__c.label}" />
              <apex:selectList size="5" multiselect="true" value="{!selectedThirdParties}" styleClass="toolOptions">
                <apex:selectOptions value="{!ThirdParty}" />
              </apex:selectList>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
              <apex:outputLabel />
              <apex:outputPanel >
                <apex:commandButton value=" Add " action="{!addNewOptions}" rerender="optionsTabBlock,errorMsgs" status="savingOptions" />
                <apex:actionStatus id="savingOptions" startText="Saving..." />
              </apex:outputPanel>
            </apex:pageBlockSectionItem>
          </apex:pageBlockSection>
          <apex:pageBlockSection columns="1" title="Current Allowed Options" collapsible="false">
            <apex:pageBlockTable value="{!AllOptions}" var="o">
              <apex:column headerValue="Actions">
                <apex:commandLink action="{!editOptions}" value="Edit">
                  <apex:param assignTo="{!optionId}" value="{!o.AnyId}" name="option" />
                </apex:commandLink>
              </apex:column>
              <apex:column headerValue="{!$ObjectType.Membership__c.fields.Client_Industry__c.label}" value="{!o.industry}" />
              <apex:column headerValue="{!$ObjectType.Membership__c.fields.Product_to_be_Sold__c.label}" value="{!o.productName}" />
              <apex:column headerValue="{!$ObjectType.Membership__c.fields.Permissible_Purpose__c.label}" value="{!o.purpose}" />
              <apex:column headerValue="{!$ObjectType.Membership__c.fields.Third_Party_Involvement__c.label}">
                <apex:repeat value="{!o.thirdParties}" var="tp">
                  (<apex:commandLink value="X" action="{!deleteOption}"><apex:param value="{!o.thirdPartyMap[tp]}" assignTo="{!optionId}" name="optId" /></apex:commandLink>) {!tp} <br /> 
                </apex:repeat>
               </apex:column>
            </apex:pageBlockTable>
          </apex:pageBlockSection>
          <apex:pageBlockSection columns="1" title="Admin">
            <apex:outputPanel >
              <a href="/{!$ObjectType.Membership_Onboarding_Allowed_Option__c.keyPrefix}">Go</a>
            </apex:outputPanel>
          </apex:pageBlockSection>
        </apex:pageBlock>
      </apex:tab>
      <apex:tab label="Documents" name="docsTab" id="docsTab">
        <apex:pageBlock id="docsTabBlock">
          <apex:pageBlockSection columns="1" title="Create New Document Requirement" collapsible="false">
            <apex:inputField value="{!newDoc.Type_of_Document__c}" />
            <apex:inputField value="{!newDoc.Lookup_Type__c}" />
            <apex:pageBlockSectionItem >
              <apex:outputLabel value="{!$ObjectType.Membership_Onboarding_Document__c.fields.Lookup_Value__c.label}" />
              <apex:selectList value="{!newDoc.Lookup_Value__c}" size="1" multiselect="false">
                <apex:selectOptions value="{!AllLookupOptions}" />
              </apex:selectList>
            </apex:pageBlockSectionItem>
            <apex:inputfield value="{!newDoc.Document_URL__c}"/>
            <apex:inputTextarea value="{!newDoc.Full_Detail__c}" rows="3" cols="80" />
            <apex:inputCheckbox value="{!newDoc.Not_Applicable_Document__c}" />
            <apex:pageBlockSectionItem >
              <apex:outputLabel />
              <apex:outputPanel >
                <apex:commandButton value=" Add/Save " action="{!addNewDocument}" rerender="docsTabBlock,errorMsgs" status="savingDoc" />
                <apex:actionStatus id="savingDoc" startText="Saving..." />
                <apex:actionStatus id="editingDoc" startText=" loading... " />
              </apex:outputPanel>
            </apex:pageBlockSectionItem>
          </apex:pageBlockSection>
          <apex:pageBlockSection columns="1" title="Current Document Requirements" collapsible="false">
            <apex:pageBlockTable value="{!AllDocuments}" var="d">
              <apex:column headerValue="Actions" width="10%">
                <apex:commandLink action="{!editDocument}" value="Edit" status="editingDoc" rerender="docsTabBlock,errorMsgs">
                  <apex:param assignTo="{!docId}" value="{!d.Id}" name="doc" />
                </apex:commandLink> |&nbsp;<apex:commandLink action="{!cloneDocument}" value="Clone" status="editingDoc" rerender="docsTabBlock,errorMsgs">
                  <apex:param assignTo="{!docId}" value="{!d.Id}" name="doc" />
                </apex:commandLink> |&nbsp;<apex:commandLink action="{!deleteDocument}" value="Del" status="editingDoc" rerender="docsTabBlock,errorMsgs">
                  <apex:param assignTo="{!docId}" value="{!d.Id}" name="doc" />
                </apex:commandLink>
              </apex:column>
              <apex:column value="{!d.Lookup_Type__c}" />
              <apex:column value="{!d.Lookup_Value__c}" />
              <apex:column value="{!d.Type_of_Document__c}" />
              <apex:column value="{!d.Full_Detail__c}"/>
              <apex:column headerValue="{!$ObjectType.Membership_Onboarding_Document__c.fields.Not_Applicable_Document__c.label}">
                <apex:inputCheckbox disabled="true" value="{!d.Not_Applicable_Document__c}" />
              </apex:column>
              <apex:column headerValue="Document Link">
                <apex:outputlink value="{!d.Document_URL__c}" target="_blank" rendered="{!IF(d.Document_URL__c =null,false,true)}">
                   <apex:image id="theImage" value="/img/icon/documents24.png"/>   
                </apex:outputlink>
              </apex:column>
            </apex:pageBlockTable>
          </apex:pageBlockSection>
          <apex:pageBlockSection columns="1" title="Admin">
            <apex:outputPanel >
              <a href="/{!$ObjectType.Membership_Onboarding_Document__c.keyPrefix}">Go</a>
            </apex:outputPanel>
          </apex:pageBlockSection>
        </apex:pageBlock>
      </apex:tab>
    </apex:tabPanel>
  </apex:form>
</apex:page>