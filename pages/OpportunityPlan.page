<apex:page standardController="Opportunity_Plan__c" extensions="OpportunityPlanController">
  <apex:outputpanel >
        <apex:actionstatus id="entryStatus">
            <apex:facet name="start">
                <div class="waitingSearchDiv" id="el_loading" style="background-color: #fbfbfb;
                       height: 100%;opacity:0.65;width:100%;">
                    <div class="waitingHolder" style="top: 74.2px; width: 91px;">
                        <img class="waitingImage" src="/img/loading.gif" title="Please Wait..." />
                        <span class="waitingDescription">Please Wait...</span>
                    </div>
                </div>
            </apex:facet>
        </apex:actionstatus>
    </apex:outputpanel>

 <apex:sectionHeader title="Opportunity Plan Edit" subtitle="{!pageTitle}"/>
    <apex:form >

      <apex:pageBlock mode="Edit">
      
      <apex:pageMessages id="pmessage" ></apex:pageMessages>

      <apex:pageBlockButtons id="detailButtons" >
                  <apex:commandButton action="{!save}" value="Save" disabled="{!showSaveButton}"/>
                  <apex:commandButton action="{!cancel}" value="Cancel"/>
              </apex:pageBlockButtons>
          <apex:pageblockSection columns="1" title="Opportunity Plan Edit">
              
              
              <!-- Begin Information Section -->
              <apex:pageBlockSection title="Information">
                  <apex:inputField value="{!Opportunity_Plan__c.Name}" />
                  <apex:inputField id="ownerId" value="{!Opportunity_Plan__c.OwnerId}" rendered="{!showOwner}" />
                  <apex:outputField value="{!Opportunity_Plan__c.OwnerId}" rendered="{!NOT(showOwner)}" />

                  <apex:repeat value="{!$ObjectType.Opportunity_Plan__c.fieldSets.Information_Top_Section}" var="field">
                      <apex:inputField value="{!Opportunity_Plan__c[field]}" />
                  </apex:repeat>
                  
                  <apex:inputField value="{!Opportunity_Plan__c.Opportunity_Name__c}" />
                  <apex:outputText value="" />

                  <apex:inputField value="{!Opportunity_Plan__c.Opportunity_ID_Unique__c}" />
                  <apex:outputText value="" />

                  <apex:inputField value="{!Opportunity_Plan__c.Opportunity_Expected_Close_Date__c}" />
                  <apex:outputText value="" />

                  <apex:repeat value="{!$ObjectType.Opportunity_Plan__c.fieldSets.Information_Bottom_Section}" var="field">
                      <apex:inputField value="{!Opportunity_Plan__c[field]}" />
                  </apex:repeat>

              </apex:pageBlockSection>     
              <!-- End Information Section -->
              
              <!-- Begin Opportunity History Section -->
              <apex:pageBlockSection title="Opportunity History" id="opp_history" >
                      
                      
                      <apex:pageBlocksection >
                          <apex:inputfield value="{!Opportunity_Plan__c.Opportunity_History__c}" style="width: 385px; height: 70px"/>
                      </apex:pageBlocksection>
                      
                      <apex:pageblocksection >
                          <apex:outputText value="" rendered="true" />
                      </apex:pageblocksection>
                        
                        <apex:pageblockSection columns="1">
                            <apex:inputField value="{!Opportunity_Plan__c.Client_Goal_1__c}" rendered="{!clientGoal[0]}" style="width: 360px; height: 70px"/>
                            <apex:inputField value="{!Opportunity_Plan__c.Client_Goal_2__c}" rendered="{!clientGoal[1]}" style="width: 360px; height: 70px"/>
                            <apex:inputField value="{!Opportunity_Plan__c.Client_Goal_3__c}" rendered="{!clientGoal[2]}" style="width: 360px; height: 70px"/>
                            <apex:inputField value="{!Opportunity_Plan__c.Client_Goal_4__c}" rendered="{!clientGoal[3]}" style="width: 360px; height: 70px"/>
                            <apex:inputField value="{!Opportunity_Plan__c.Client_Goal_5__c}" rendered="{!clientGoal[4]}" style="width: 360px; height: 70px"/>
                            <apex:commandButton action="{!addClientGoal}" style="margin-left:21.5%" value="Add Another Client Goal" rerender="opp_history, ownerId, pmessage" status="entryStatus"/>
                        </apex:pageblockSection>
                        
                        <apex:pageBlockSection columns="1">
                            <apex:inputField value="{!Opportunity_Plan__c.Sales_Objective_1__c}" rendered="{!clientObjc[0]}" style="width: 360px; height: 70px"/>
                            <apex:inputField value="{!Opportunity_Plan__c.Sales_Objective_2__c}" rendered="{!clientObjc[1]}" style="width: 360px; height: 70px"/>
                            <apex:inputField value="{!Opportunity_Plan__c.Sales_Objective_3__c}" rendered="{!clientObjc[2]}" style="width: 360px; height: 70px"/>   
                            <apex:inputField value="{!Opportunity_Plan__c.Sales_Objective_4__c}" rendered="{!clientObjc[3]}" style="width: 360px; height: 70px"/>
                            <apex:inputField value="{!Opportunity_Plan__c.Sales_Objective_5__c}" rendered="{!clientObjc[4]}" style="width: 360px; height: 70px"/>
                            
                            <apex:outputpanel id="output" > 
                                <apex:commandButton action="{!addSalesObjective}" style="margin-left:18.5%" value="Add Another Sales Objective" rerender="opp_history, ownerId, pmessage" status="entryStatus" />
                            </apex:outputpanel>
                            
                        </apex:pageBlockSection>
                        
                      
                      
                                  
              </apex:pageBlockSection>
              <!-- End Opportunity History Section --> 
              
              
              <!-- Begin Qualification Criteria --> 
              <apex:pageBlockSection Title="Qualification Criteria">
                  <apex:repeat value="{!$ObjectType.Opportunity_Plan__c.fieldSets.QualificationCriteriaSection}" var="field">
                      <apex:inputField value="{!Opportunity_Plan__c[field]}"/>
                  </apex:repeat>

                  <apex:inputField value="{!Opportunity_Plan__c.Project_High_Priority__c}" />
                  <apex:outputText value="" />

                  <apex:inputField value="{!Opportunity_Plan__c.Known_Buying_Process__c}" />
                  <apex:outputText value="" />

                  <apex:inputField value="{!Opportunity_Plan__c.Justifiable_Investment__c}" />
                  <apex:outputText value="" />

                  <apex:inputField value="{!Opportunity_Plan__c.Known_Competition__c}" />
                  <apex:outputText value="" />

                  <apex:inputField value="{!Opportunity_Plan__c.Resources_Available__c}" />
                  <apex:outputText value="" />

                  <apex:inputField value="{!Opportunity_Plan__c.Reliable_Coach__c}" />
                  <apex:outputText value="" />

              </apex:pageBlockSection>
              <!-- End Qualification Criteria --> 
              
              <!-- Begin Summary Position Today-->
              <apex:pageBlockSection Title="Summary Position Today - Strengths and Risks (1 = Low, 10 = High)">
                        <apex:repeat value="{!$ObjectType.Opportunity_Plan__c.fieldSets.Strengths_and_Risks}" var="field">
                            <apex:inputField value="{!Opportunity_Plan__c[field]}" styleClass="type-{!$ObjectType.Opportunity_Plan__c.Fields[field].type}" />
                        </apex:repeat>
                        <style type="text/css">
                             .type-textarea{ width: 400px; height: 35px; }
                        </style>
              </apex:pageBlockSection>
              <!-- End Summary Position Today-->
              
              <!--Begin Scoring Dashboard -->
              <apex:pageblockSection Title="Scoring Dashboard">
                  <apex:repeat value="{!$ObjectType.Opportunity_Plan__c.fieldSets.ScoringDashSection}" var="field">
                      <apex:inputField value="{!Opportunity_Plan__c[field]}"/>
                  </apex:repeat>
              </apex:pageblocksection>
              <!-- End Scoring Dashboard -->
              
              
          </apex:pageblockSection>
      
      </apex:pageBlock>

  </apex:form>
  
</apex:page>