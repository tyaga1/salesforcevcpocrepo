<!--
/**=====================================================================
  * Experian
  * Name: SerasaCaseCreate
  * Description: W-007274: Apex visualforce page SerasaCaseCreate that helps user to create different
  *                        type of cases.
  *                        Types implemented:
  *                        1. Case for cancel contract cancellation request
  * Created Date: April 10 2017
  * Created By: Ryan (Weijie) Hu, UCInnovation
  *
  * Date Modified      Modified By                  Description of the update
  * May   1st  2017    Ryan (Weijie) Hu             Translation added
  * May   10th 2017    Ryan (Weijie) Hu             Add to show "Unsupported RecordType to user"
  *=====================================================================*/
-->
<apex:page standardController="Case" extensions="SerasaCaseCreateController" showHeader="true" sidebar="true">

	<apex:includeScript value="/support/console/39.0/integration.js"/>
	<script>
		var closeSubtab = function closeSubtab(result) {
            //Now that we have the tab ID, we can close it
            var tabId = result.id;
            sforce.console.closeTab(tabId);
        };

		function serasaBackToCase() {
			if (sforce.console.isInConsole()) {
				sforce.console.getFocusedPrimaryTabId(showTabId)
				
			}
			else {
				window.location = '/{!parentCaseIdStr}';
			}
			
		}

        var showTabId = function showTabId(result) {
        	sforce.console.openPrimaryTab(result.id, '/{!parentCaseIdStr}?isdtp=vw', true);
        };
        
        var openSuccess = function openSuccess(result) {
            //Report whether opening the new tab was successful
            if (result.success == true) {
                sforce.console.getEnclosingTabId(closeSubtab);
            }
        };
	</script>

	<style>
		.serasa-scc-header-title {
			font-size: 1.8em;
			font-weight: normal;
			line-height: 1.1em;
			margin-top: 16px;
		}

		.serasa-scc-form {
			margin-top: 16px;
		}
	</style>

	<apex:pageMessages escape="false"/>

	<div class="serasa-scc-header-title">{!dynamicTitle}</div>

	<apex:form styleClass="serasa-scc-form" rendered="{!NOT(errorDetected)}">
		<apex:pageBlock title="{!dynamicTitle}" mode="edit">
			<apex:pageBlockButtons >
				<apex:commandButton action="{!save}" value="{!$Label.Serasa_Case_Create_Page_SaveBtn}"/>
				<apex:commandButton action="{!cancel}" value="{!$Label.Serasa_Case_Create_Page_CancelBtn}"/>
			</apex:pageBlockButtons>

			<apex:pageBlockSection title="{!$Label.Serasa_Case_Create_Page_pbsection_ccrs}" columns="1">
				<apex:pageBlockTable value="{!ccrWrapperList}" var="wrapper">
					<apex:column headerValue="{!$Label.Serasa_Case_Create_Page_CancelBtn}" width="50px">
						<apex:inputCheckbox value="{!wrapper.selected}"></apex:inputCheckbox>
					</apex:column>
					<apex:column value="{!wrapper.ccr.Name}"/>
					<apex:column value="{!wrapper.ccr.Contract_Number__c}"/>
					<apex:column value="{!wrapper.ccr.Cancellation_Date__c}"/>
					<apex:column value="{!wrapper.ccr.Operation_Type__c}"/>
					<apex:column value="{!wrapper.ccr.Status__c}"/>
				</apex:pageBlockTable>
			</apex:pageBlockSection>

			<apex:pageBlockSection title="{!$Label.Serasa_Case_Create_Page_pbsection_prepopulated}" columns="2">
				<apex:outputField value="{!parentCase.CaseNumber}"/>
				<apex:outputField value="{!parentCase.Subject}"/>
				<apex:outputField value="{!parentCase.RecordTypeId}"/>
				<apex:outputField value="{!parentCase.OwnerId}"/>
				<apex:outputField value="{!Case.AccountId}"/>
				<apex:outputField value="{!parentAccount.CNPJ_Number__c}"/>
				<apex:outputField value="{!Case.ContactId}"/>
			</apex:pageBlockSection>

			<apex:pageBlockSection title="{!$Label.Serasa_Case_Create_Page_pbsection_required}" columns="2">
				<apex:repeat value="{!fields}" var="f">
					<apex:inputField value="{!Case[f.fieldPath]}" required="{!OR(f.required, f.dbrequired)}" rendered="{!IF(f.fieldPath != 'SLA__c', true, false)}"/>
					<apex:outputField value="{!Case[f.fieldPath]}" rendered="{!IF(f.fieldPath == 'SLA__c', true, false)}"/>
					<apex:pageBlockSectionItem rendered="{!IF(f.fieldPath == 'Serasa_Secondary_Case_Reason__c',true, false)}"/>
				</apex:repeat>
			</apex:pageBlockSection>

			<apex:pageBlockSection title="{!$Label.Serasa_Case_Create_Page_pbsection_description}" columns="2">
				<apex:inputField value="{!Case.Subject}"/>
				<apex:pageBlockSectionItem />
				<apex:inputField value="{!Case.Description}" style="width: 100%; height: 64px"/>
				<apex:pageBlockSectionItem />
				<apex:inputField value="{!Case.Resolution__c}" style="width: 100%; height: 64px"/>
			</apex:pageBlockSection>

			<!--apex:pageBlockSection title="{!$Label.Serasa_Case_Create_Page_pbsection_caseinfo}" columns="2">
				<apex:inputField value="{!Case.SLA__c}" />
				<apex:outputField value="{!Case.OwnerId}" />
				<apex:inputField value="{!Case.Status}" />
				<apex:inputField value="{!Case.Priority}" />
			</apex:pageBlockSection-->

			<apex:pageBlockSection title="{!$Label.Serasa_Case_Create_Page_pbsection_descDetial}" columns="2">
				<apex:inputField value="{!Case.Competition__c}" />
				<apex:inputField value="{!Case.Product_Serasa__c}" />
				<apex:inputField value="{!Case.Justification__c}" style="width: 100%; height: 64px"/>
			</apex:pageBlockSection>

			<apex:pageBlockSection title="{!$Label.Serasa_Case_Create_Page_pbsection_additional}" columns="2">
				<apex:inputField value="{!Case.Incomplete_Information__c}" />
				<apex:inputField value="{!Case.Relationship_Channel__c}" />
				<apex:inputField value="{!Case.Incorrect_Classification__c}" />
				<apex:inputField value="{!Case.Customer_Involved__c}" />
				<apex:inputField value="{!Case.Missing_Request__c}" />
			</apex:pageBlockSection>

			<apex:pageBlockSection title="{!$Label.Serasa_Case_Create_Page_pbsection_system}" columns="2">
				<apex:outputField value="{!Case.RecordTypeId}" />
				<apex:outputField value="{!Case.OwnerId}" />
				<apex:inputField value="{!case.User_Island__c}" styleClass="serasa_scc_disabled"/>
				<apex:inputField value="{!Case.First_Call_Resolution__c}" />
				<apex:inputTextarea value="{!Case.Sub_Origin__c}" style="width: 100%; height: 64px"/>
			</apex:pageBlockSection>

			<apex:pageBlockSection title="{!$Label.Serasa_Case_Create_Page_pbsection_optional}" columns="2">
				<apex:inputCheckbox value="{!usingAssignmentRules}" label="Assign using active assignment rules"/>
			</apex:pageBlockSection>

		</apex:pageBlock>
	</apex:form>

	<apex:outputPanel rendered="{!errorDetected}">
		<apex:pageMessage summary="{!$Label.Serasa_Case_Create_Page_NotSupportType}" severity="warning" strength="3" />
		<button onclick="serasaBackToCase();" type="button">{!$Label.Serasa_Case_Create_Page_CancelBtn}</button>
	</apex:outputPanel>

	<script>
		var needToDisableElements = document.getElementsByClassName('serasa_scc_disabled');

		for (var elemt in needToDisableElements) {
			needToDisableElements[elemt].disabled = true;
		}
	</script>
</apex:page>