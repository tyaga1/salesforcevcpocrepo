<apex:page controller="seal.FederatedSearchController" expires="0" title="Federated Search">
	
	<!-- jQuery Files -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>
		var s$ = jQuery.noConflict();
	</script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="{!URLFOR($Resource.bootstrap, 'css/bootstrap-force.css')}" />
	
	<!-- Optional theme -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css" />
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="{!URLFOR($Resource.bootstrap, 'js/bootstrap.min.js')}"></script>
	
	<script>
	
		var j$ = jQuery.noConflict();
	
		j$('document').ready(function() {
			isEnableSalesforceFields();
			j$('.force a').tooltip();
		});
		
		function doSearch() {
			j$('.search-progress').fadeIn();
			doSearchAF();
		}
		
		function setSearchType(type) {
			if(type == 1) {
				j$('#search-type #search-label').text('Searching Salesforce');
				j$('.search-salesforce-option').prop('checked', true);
				j$('.search-seal-option').prop('checked', false);
				isEnableSalesforceFields();
			}
			else if(type == 2) {
				j$('#search-type #search-label').text('Searching Seal');
				j$('.search-salesforce-option').prop('checked', false);
				j$('.search-seal-option').prop('checked', true);
				isEnableSalesforceFields();
			}
			else if(type == 3) {
				j$('#search-type #search-label').text('Searching Both');
				j$('.search-salesforce-option').prop('checked', true);
				j$('.search-seal-option').prop('checked', true);
				isEnableSalesforceFields();
			}
		}
		
		function isEnableSalesforceFields() {
			if(j$('input[id$=searchSalesforce]').is(':checked')) {
				enableSalesforceFields();
			} else {
				disableSalesforceFields();
			}
		}
		
		function enableSalesforceFields() {
			j$('.search-term').attr('disabled', 'disabled');
			j$('.search-term').val('Use the fieldset below to search.');
			j$('.salesforceSearch').show();
			j$('#full-search-action').show();
			j$('.seal-search-action').hide();
		}
		
		function disableSalesforceFields() {
			j$('.search-term').removeAttr('disabled');
			j$('.search-term').val('');
			j$('.salesforceSearch').hide();
			j$('#full-search-action').hide();
			j$('.seal-search-action').show();
		}

		function noenter(ev)  {
      if (window.event && window.event.keyCode == 13 || ev.which == 13) {
      	doSearch();
        doSearchAF();
        return false;
      } else {
        return true;
      }
    }
		
	</script>
	<style>
		ul li,
		ol li {
			margin: 0;
		}
	</style>
	<br/>
	<div class="force">
	<div class="alert alert-info alert-dismissable" style="visibility: visible;">
		<span class="glyphicon glyphicon-lock"></span>
		<strong>Page Restricted:</strong> Access to this page has been restricted. Please contact your System Administrator.
	</div>
	</div>
	<!-- Content hidden, to be revisited in v4
	<apex:sectionHeader title="Federated Search" subtitle="Search for Contracts" />
	<div class="force">
		<apex:outputPanel rendered="{!NOT(sealSettingsDefined)}" styleClass="notLicensedError">
			<apex:pageMessage summary="The admin for this site has not defined the required Seal settings. Please contact Seal." severity="error" strength="3" />
		</apex:outputPanel>
		<apex:form id="theForm" rendered="{!sealSettingsDefined}">
			<apex:actionFunction name="doSearchAF" action="{!doSearch}" />
			<apex:pageMessages />
			<div class="alert alert-info alert-dismissable" style="{!IF(securitySearch, 'visibility: visible;', 'display: none;')}">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <span class="glyphicon glyphicon-lock"></span> <strong>Security Enabled:</strong> Your administrator has enabled a security trimmed search. Your results will be filtered based on your Salesforce permissions.
			</div>
			<div class="panel panel-default panel-body">
				<span>View&nbsp;<apex:commandLink value="active contract mappings." action="{!displayMetadataMappings}"></apex:commandLink></span>
			</div>
			<div id="search-parameters" class="panel panel-primary">
			  <div class="panel-heading">
			    <h3 class="panel-title">Search Options</h3>
			  </div>
			  <div class="panel-body">
			  	<div id="seal-field" class="row">
				  <div class="col-lg-6">
				    <div class="input-group">
				      <div class="input-group-btn">
				      	<apex:outputPanel rendered="{!AND(searchSeal, searchSalesforce)}">
				        	<button id="search-type" type="button" class="btn1 btn1-info dropdown-toggle" data-toggle="dropdown"><span id="search-label">Searching Both</span> <span class="caret"></span></button>
					        <ul class="dropdown-menu">
					          <li><a href="javascript:setSearchType(2);">Search Only Seal</a></li>
					          <li><a href="javascript:setSearchType(1);">Search Only Salesforce</a></li>
					          <li><a href="javascript:setSearchType(3);">Search Both</a></li>
					        </ul>
				        </apex:outputPanel>
				      	<apex:outputPanel rendered="{!AND(searchSeal, NOT(searchSalesforce))}">
				        	<button id="search-type" type="button" class="btn1 btn1-info dropdown-toggle" data-toggle="dropdown"><span id="search-label">Searching Seal</span> <span class="caret"></span></button>
					        <ul class="dropdown-menu">
					          <li><a href="javascript:setSearchType(2);">Search Only Seal</a></li>
					          <li><a href="javascript:setSearchType(1);">Search Only Salesforce</a></li>
					          <li><a href="javascript:setSearchType(3);">Search Both</a></li>
					        </ul>
				        </apex:outputPanel>
				        <apex:outputPanel rendered="{!AND(searchSalesforce, NOT(searchSeal))}">
				        	<button id="search-type" type="button" class="btn1 btn1-info dropdown-toggle" data-toggle="dropdown"><span id="search-label">Searching Salesforce</span> <span class="caret"></span></button>
					        <ul class="dropdown-menu">
					          <li><a href="javascript:setSearchType(2);">Search Only Seal</a></li>
					          <li><a href="javascript:setSearchType(1);">Search Only Salesforce</a></li>
					          <li><a href="javascript:setSearchType(3);">Search Both</a></li>
					        </ul>
				        </apex:outputPanel>
				      </div><
					  <apex:inputText html-placeholder="Enter Seal search string..." styleClass="search-term form-control" value="{!searchTerm}" onkeypress="return noenter(event);" />
					  <span class="input-group-btn">
					      <apex:commandLink styleClass="btn1 btn1-success seal-search-action" rerender="display-results" onclick="doSearch();" oncomplete="j$('.force a').tooltip();">&nbsp;<span class="glyphicon glyphicon-search"></span>&nbsp;</apex:commandLink>
					  </span>
				    </div>
				  </div>
				  <div class="hidden">
				    <apex:inputCheckbox value="{!searchSalesforce}" id="searchSalesforce" styleclass="search-salesforce-option" onclick="isEnableSalesforceFields()" />
					<apex:inputCheckbox value="{!searchSeal}" id="searchSeal" styleclass="search-seal-option" onclick="isEnableSalesforceFields()" />
				  </div>
				</div>
				<div class="salesforceSearch form-horizontal">
					<br />
					<div class="row form-group">
						<div class="col-md-2 control-label">
							<strong class="searchItem">File Name</strong>
						</div>
						<div class="col-md-6">
							<apex:inputText styleClass="form-control form-control-sm" value="{!salesforceFilename}" onkeypress="return noenter(event);" />
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-2 control-label">
							<strong class="searchItem">Account (Contracting Party)</strong>
						</div>
						<div class="col-md-6">
							<apex:inputText styleClass="form-control form-control-sm" value="{!salesforceAccountName}" onkeypress="return noenter(event);" />
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-2 control-label">
							<strong class="searchItem">Containing Text</strong>
						</div>
						<div class="col-md-6">
							<apex:inputText styleClass="form-control form-control-sm" value="{!salesforceContainingText}" onkeypress="return noenter(event);" />
						</div>
					</div>
					<apex:outputPanel id="search-error" rendered="{!isSearchError}">
						<apex:pageMessage summary="Please enter a search term in one of the three fields above." severity="error" strength="3" />
						<br />
					</apex:outputPanel>
					<apex:outputPanel id="search-term_error" rendered="{!isSearchTermError}">
						<apex:pageMessage summary="Please enter a search term with length more than 1." severity="error" strength="3" />
						<br />
					</apex:outputPanel>
					<div>
						<div class="panel panel-default panel-body searchHeading">
							<span><strong>Search Active Contract Mappings</strong></span>
						</div>
						<apex:outputPanel rendered="{!NOT(displayMappings)}">
							<span>No active mappings or none of the mappings support this feature.</span>
						</apex:outputPanel>
						<apex:outputPanel rendered="{!displayMappings}">
							<apex:repeat value="{!searchItems}" var="item">
								<div class="row form-group">
									<div class="col-md-2 control-label">
										<strong class="searchItem">{!item.sealField} / {!item.salesforceField}</strong>
									</div>
									<div class="col-md-6">
										<apex:inputText html-placeholder="Search mapping..." styleClass="form-control form-control-sm" value="{!item.searchValue}" onkeypress="return noenter(event);" />
									</div>
								</div>
							</apex:repeat>
						</apex:outputPanel>
					</div>
				</div>
				<br />
				<div class="row form-group" id="full-search-action">
					<div class="col-md-2 control-label col-md-offset-2">
						<apex:commandLink styleClass="btn1 btn1-success" rerender="theForm" onclick="doSearch();" oncomplete="j$('.force a').tooltip();"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Search</apex:commandLink>
					</div>
				</div>
			  </div>
			</div>
			<apex:outputPanel layout="block" id="display-results" styleClass="panel panel-primary">
				<div class="panel-heading">
				    <h3 class="panel-title">Results</h3>
				  </div>
				  <div class="panel-body">
					<div class="search-progress">
						<div class="progress progress-striped active">
						  <div class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						    <span class="sr-only">Searching...</span>
						  </div>
						</div>
					</div>
					<apex:outputPanel layout="none" rendered="{!AND(ISBLANK(searchTerm), salesforceResultList.size == 0, sealResultList.size == 0)}">
						<em>Enter search criteria.</em>
					</apex:outputPanel>
					<apex:outputPanel layout="none" rendered="{!AND(NOT(ISBLANK(searchTerm)), salesforceResultList.size == 0, sealResultList.size == 0)}">
						<em>No results.</em>
					</apex:outputPanel>
					<apex:outputPanel layout="none" rendered="{!OR(salesforceResultList.size != 0, sealResultList.size != 0)}">
						<ul class="nav nav-tabs">
						  <apex:outputPanel layout="none" rendered="{!searchSeal}">
						  	<li class="active"><a href="#seal-results" data-toggle="tab">Seal Results&nbsp;&nbsp;<span class="badge">{!sealResultList.size} results</span></a></li>
						  </apex:outputPanel>
						  <apex:outputPanel layout="none" rendered="{!searchSalesforce}">
						  	<li class="{!IF(searchSeal, '', 'active')}"><a href="#salesforce-results" data-toggle="tab">Salesforce Results&nbsp;&nbsp;<span class="badge">{!salesforceResultList.size} results</span></a></li>
						  </apex:outputPanel>
						</ul>
						<div class="tab-content">
						  <apex:outputPanel layout="none" rendered="{!searchSeal}">
							  <div class="tab-pane fade in active" id="seal-results">
							  	<table class="table table-striped table-hover">
							  		<thead>
							  			<th>File Name</th>
							  			<th>Contracting Parties</th>
							  			<th>Action</th>
							  		</thead>
							  		<tbody>
							  			<apex:repeat value="{!sealResultList}" var="result">
											<tr>
												<td>{!result.filename}</td>
												<td>{!result.text}</td>
												<td>
													<a href="{!result.link}" target="_blank" title="Download file from Seal.">Download File</a>
												</td>
											</tr>
										</apex:repeat>
							  		</tbody>
							  	</table>
							  </div>
						  </apex:outputPanel>
						  <apex:outputPanel layout="none" rendered="{!searchSalesforce}">
							  <div class="tab-pane fade {!IF(searchSeal, '', 'in active')}" id="salesforce-results">
							  	<table class="table table-striped table-hover">
							  		<thead>
							  			<th>Contract Number</th>
							  			<th>File Name</th>
							  			<th>Account</th>
							  			<th>Action</th>
							  		</thead>
							  		<tbody>
							  			<apex:repeat value="{!salesforceResultList}" var="result">
											<tr>
												<td>{!result.contractNumber}</td>
												<td><a href="/{!result.Id}" target="_blank" title="View File Details">{!result.name}</a></td>
												<td><a href="/{!result.AccountId}" target="_blank" title="View Linked Account">{!result.accountId}</a></td>
												<td>
													<a href="{!result.link}" target="_blank" title="View Contract in Salesforce">View Contract</a>
												</td>
											</tr>
										</apex:repeat>
							  		</tbody>
							  	</table>
							  </div>
						  </apex:outputPanel>
						</div>
					</apex:outputPanel>
				</div>
			</apex:outputPanel>
		</apex:form>
	</div>
	-->
	<style>
		.force .form-control-sm {
			height: 28px;
		}
		.search-progress {
			display: none;
		}
		.input-group-btn {
			display: table-cell !important;
		}
	
		.searchBar {
			margin-left: 34px;
		}
		
		.salesforceFilename {
			margin-left: 38px;
		}
		
		.salesforceAccountName {
			margin-left: 10px;
		}
		
		.salesforceContainingText {
			margin-left: 5px;
		}
		
		.searchSection {
			display: inline;
		}
		
		.searchItem {
			float: right;
		}
		
		.searchHeading {
			width: 240px;
		}
	
	</style>
	
</apex:page>