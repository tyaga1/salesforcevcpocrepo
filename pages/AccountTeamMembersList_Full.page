<!--
/**=====================================================================
 * Name: AccountTeamMembersList_Full.page
 * Description:  Case #01192655: Show the Business Unit alongside each AE under Account Team
 * This the page was written to allow for the customisation of the list of AccountTeamMember records. 
 * Created Date: 2nd March, 2016
 * Created By: James Wills
 * Mar. 2nd, 2016        James Wills           Case #1192655 Show the Business Unit alongside each AE under Account Team: Created
 * Jun. 7th, 2016        James Wills           Case #1192655: Further updates
 * Aug. 30th, 2016       James Wills           CRM2:W-005718: Account Team View - Job Title
 * Oct. 12th, 2016       James Wills           Case #2144309: Uncommented include Scripts
*  =====================================================================*/
-->
<apex:page standardController="Account" extensions="AccountTeamMembersList,AccountTeamMembersList_Full" showheader="true" sidebar="true">
  <apex:sectionHeader title="{!$Label.AccountTeamMembersList_Account_Team}" subtitle="{!$Label.AccountTeamMembersList_Account_Team}"/>
  <apex:includeScript value="/xdomain/xdomain.js"/>
  <apex:includeScript value="/soap/ajax/26.0/connection.js"/>
  <apex:includeScript value="/support/console/26.0/integration.js"/>
  
  
  <apex:form id="frm">
    <apex:outputPanel id="fullPage">
    <apex:commandLink action="{!backToAccount}">{!$Label.ACCOUNTPLANNING_Account} {!accountName}</apex:commandlink>
    
    <apex:pageMessages id="msg"/>
    
    <apex:pageBlock id="accountTeamMembersListPageBlock">
        
      <apex:actionFunction name="atmListReorder_AF" action="{!atmListReorder}" rerender="fullPage">
        <apex:param name="atmListColumn" assignTo="{!atmListOrderParam}" value=""/>
      </apex:actionFunction>
    
      <apex:actionFunction name="deleteATM_AF" action="{!doDelete}" rerender="fullPage">
        <apex:param name="delParam" assignTo="{!selectedId}" value=""/>
      </apex:actionFunction>
    
      <apex:pageBlockSection columns="1">
      <apex:outputPanel id="showMembersPanel">
        <apex:pageBlockTable id="AccountTeamMembersForDisplay" value="{!accountTeamMembersToShowComplete}" var="teamMember">
          <apex:column headerValue="{!$Label.AccountTeamMembersList_Action}">
            <apex:outputPanel layout="inline" rendered="{!userhasEditAccess}"><a onClick="editMember('{!acc.Id}','{!teamMember.member.Id}');" style="cursor: pointer;text-decoration: underline;color: #015ba7;">{!$Label.AccountTeamMembersList_Edit}</a>&nbsp;|&nbsp;</apex:outputPanel>
            <apex:outputPanel layout="inline" rendered="{!userhasDeleteAccess}"><a onClick="deleteATM('{!teamMember.member.Id}');" style="cursor: pointer;text-decoration: underline;color: #015ba7;">{!$Label.AccountTeamMembersList_Del}</a></apex:outputPanel>
          </apex:column>
          <apex:column headerValue="{!$Label.AccountTeamMembersList_Team_Member}" title="{!$Label.Sort_By} {!$Label.AccountTeamMembersList_Team_Member}" onClick="atmListReorder('{!teamMember.member.user.Name}')">
            <apex:outputLink value="/{!teamMember.member.userId}">
              <apex:outputText value="{!teamMember.member.user.Name}" />
            </apex:outputLink>
          </apex:column>
          
          <apex:column value="{!teamMember.member.User.Title}" title="{!$Label.Sort_By} {!$Label.AccountTeamMembersList_Job_Title}" headerValue="{!$Label.AccountTeamMembersList_Job_Title}" onClick="atmListReorder('{!$Label.AccountTeamMembersList_Job_Title}')"/><!--CRM2:W-005718-->
          
          <apex:column value="{!teamMember.member.TeamMemberRole}"         title="{!$Label.Sort_By} {!$Label.AccountTeamMembersList_Team_Role}"                 headerValue="{!$Label.AccountTeamMembersList_Team_Role}"                 onClick="atmListReorder('{!$Label.AccountTeamMembersList_Team_Role}')"/>
          <apex:column headerValue="{!$Label.AccountTeamMembersList_Assignment_Team_Lead}">
            <apex:inputCheckbox value="{!teamMember.assignmentTeamLead}" disabled="true"/>
          </apex:column>
          <apex:column value="{!teamMember.member.User.Business_Unit__c}"  title="{!$Label.Sort_By} {!$Label.AccountTeamMembersList_Team_Member_Business_Unit}" headerValue="{!$Label.AccountTeamMembersList_Team_Member_Business_Unit}" onClick="atmListReorder('{!$Label.AccountTeamMembersList_Team_Member_Business_Unit}')"/>
          <apex:column value="{!teamMember.member.User.Sales_Team__c}"     title="{!$Label.Sort_By} {!$Label.AccountTeamMembersList_Sales_Team}"                headerValue="{!$Label.AccountTeamMembersList_Sales_Team}"                onClick="atmListReorder('{!$Label.AccountTeamMembersList_Sales_Team}')"/>
          <apex:column value="{!teamMember.member.User.Sales_Sub_Team__c}" title="{!$Label.Sort_By} {!$Label.AccountTeamMembersList_Sales_Sub_Team}"            headerValue="{!$Label.AccountTeamMembersList_Sales_Sub_Team}"            onClick="atmListReorder('{!$Label.AccountTeamMembersList_Sales_Sub_Team}')"/>

          <apex:column headerValue="{!$Label.AccountTeamMembersList_Date_Added_By}">
            <apex:outputText value="{!teamMember.createDate & ' / ' & teamMember.member.CreatedBy.Alias}"/>
          </apex:column>
        </apex:pageBlockTable>
        <br/>

      </apex:outputPanel>
     </apex:pageBlockSection>
           <apex:panelGrid id="atmListNavigation" columns="5"> 
        <apex:outputText id="startEndATMe" value="Records ({!startATMNumber}-{!endATMNumber})"/>
        <apex:commandLink action="{!seeMoreATMs}" rendered="{!IF(endATMNumber == totalATMs,false,true)}" reRender="accountTeamMembersListPageBlock" value="See {!additionalATMsToView} more"/>
        <apex:outputText id="totalATM" value="({!totalATMs})"/> 
      </apex:panelGrid>
     
    </apex:pageBlock>
    </apex:outputPanel>

  </apex:form>
  
  <script>
  
  function atmListReorder(atmListColumn){
    //alert(atmListColumn);
    atmListReorder_AF(atmListColumn);
    return false;
  }
  
  function editMember ( accntId, memId) {  
    var editUrl = "/acc/salesteamedit.jsp?retURL=/"+accntId+"&id="+memId;

    if (sforce.console.isInConsole()) {     
      sforce.console.getEnclosingTabId(function(enclosingResult) {
            sforce.console.getEnclosingPrimaryTabId(function(primaryResult) {
                sforce.console.openSubtab(primaryResult.id, editUrl, true, '', null);
            });
        });
    } else {
      window.parent.location.href = editUrl;
    }
    return false;
  }
  
  
  function deleteATM ( atmId) {
    if( confirm('{!$Label.AccountTeamMembersList_Record_Delete_Message}') ) {
      deleteATM_AF(atmId);
    }
    return false;
  }

  </script>

</apex:page>