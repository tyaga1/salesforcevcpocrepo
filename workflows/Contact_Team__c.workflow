<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>No_Longer_a_BU_Lead</fullName>
        <description>No Longer a BU Lead</description>
        <protected>false</protected>
        <recipients>
            <field>Relationship_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CRM_Templates/Contact_Relationship_No_Longer_BU_Lead</template>
    </alerts>
    <rules>
        <fullName>No Longer the BU Lead</fullName>
        <actions>
            <name>No_Longer_a_BU_Lead</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>W-005406 sends a notification to the User that is no loner a BU Lead for a contact</description>
        <formula>AND(     ISCHANGED(Primary_User__c),     Primary_User__c = false    )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
