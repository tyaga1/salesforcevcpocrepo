<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Sales_planning_process_Sales_to_Delivery_handover_creation</fullName>
        <ccEmails>EDQProjectCoordinatorsUK@experian.com</ccEmails>
        <description>Sales planning process Sales to Delivery handover creation</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_to_Delivery_handover_notification</template>
    </alerts>
    <rules>
        <fullName>Sales Planning Process Sales to Delivery creation notification</fullName>
        <actions>
            <name>Sales_planning_process_Sales_to_Delivery_handover_creation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 3) and 2</booleanFilter>
        <criteriaItems>
            <field>Sales_Planning_Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales to Delivery Handover</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sales_Planning_Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sales_Planning_Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales to Customer Success Handover</value>
        </criteriaItems>
        <description>Sales Planning Process Sales to Delivery or Sales to Customer Success creation notification sent to EDQ project team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
