<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Max_Skill_Level</fullName>
        <field>Max_Skill_Level__c</field>
        <formula>IF(Level7__c==null,IF(Level6__c==null,IF(Level5__c==null,IF(Level4__c==null,IF(Level3__c==null,IF(Level2__c==null,IF(Level1__c==null,0,1),2),3),4),5),6),7)</formula>
        <name>Update Max Skill Level</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Max Skill Level</fullName>
        <actions>
            <name>Update_Max_Skill_Level</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
