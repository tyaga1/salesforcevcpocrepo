<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Royal_Mail_Email_for_Auditing</fullName>
        <ccEmails>gemma.shallard@experian.com</ccEmails>
        <ccEmails>sadar.yacob@experian.com</ccEmails>
        <description>Royal Mail Email for Auditing</description>
        <protected>false</protected>
        <recipients>
            <field>To_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>charlene.marecheau@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Royal_Mail_Audit_Template</template>
    </alerts>
    <rules>
        <fullName>Royal Mail Email Alert</fullName>
        <actions>
            <name>Royal_Mail_Email_for_Auditing</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Mail_Merge_Adhoc__c.Trigger__c</field>
            <operation>equals</operation>
            <value>Start Process</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
