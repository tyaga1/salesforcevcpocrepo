<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Unique_Field_from_Formula</fullName>
        <field>Unique__c</field>
        <formula>IF( 
OR(ISNULL(Object_API_Name__c),ISBLANK(Object_API_Name__c)) , 
TEXT(Component_Type__c)&amp;&quot; - &quot;&amp; Metadata_Member__c , 
TEXT(Component_Type__c)&amp;&quot; - &quot;&amp; Metadata_Member__c )</formula>
        <name>Set Unique Field from Formula</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Unique Field from Formula</fullName>
        <actions>
            <name>Set_Unique_Field_from_Formula</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
