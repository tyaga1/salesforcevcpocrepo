<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EMEA_CSDA_Customisation_Deal_Alert_Central_Europe</fullName>
        <ccEmails>veronika.peycheva@experian.com</ccEmails>
        <description>Opportunity with customisation: Sales to Delivery handover notification EMEA Central Europe</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexey.butychin@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lilia.pencheva@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lyubomir.ganev@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>martin.slifka@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>serter.baltaci@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Opportunity_with_customisation_Sales_to_Delivery_handover_notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_CSDA_Customisation_Deal_Alert_Denmark_And_Norway</fullName>
        <ccEmails>veronika.peycheva@experian.com</ccEmails>
        <description>Opportunity with customisation: Sales to Delivery handover notification EMEA DE NO</description>
        <protected>false</protected>
        <recipients>
            <recipient>jeroen.bunck@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>karin.brandt@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lars.tottrup@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lone.lisberg@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Opportunity_with_customisation_Sales_to_Delivery_handover_notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_CSDA_Customisation_Deal_Alert_France</fullName>
        <ccEmails>veronika.peycheva@experian.com</ccEmails>
        <description>Opportunity with customisation: Sales to Delivery handover notification EMEA France</description>
        <protected>false</protected>
        <recipients>
            <recipient>jeroen.zuijdendorp@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>michel.theodose@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vincent.edelkoort@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Opportunity_with_customisation_Sales_to_Delivery_handover_notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_CSDA_Customisation_Deal_Alert_Italy</fullName>
        <ccEmails>veronika.peycheva@experian.com</ccEmails>
        <description>Opportunity with customisation: Sales to Delivery handover notification EMEA Italy</description>
        <protected>false</protected>
        <recipients>
            <recipient>pietro.cordella@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>serter.baltaci@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stefania.maggi@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Opportunity_with_customisation_Sales_to_Delivery_handover_notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_CSDA_Customisation_Deal_Alert_Netherlands</fullName>
        <ccEmails>veronika.peycheva@experian.com</ccEmails>
        <description>Opportunity with customisation: Sales to Delivery handover notification EMEA Netherlands</description>
        <protected>false</protected>
        <recipients>
            <recipient>ger.dooms@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jeroen.zuijdendorp@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vincent.edelkoort@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Opportunity_with_customisation_Sales_to_Delivery_handover_notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_CSDA_Customisation_Deal_Alert_Russia</fullName>
        <ccEmails>veronika.peycheva@experian.com</ccEmails>
        <description>Opportunity with customisation: Sales to Delivery handover notification EMEA Russia</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexey.butychin@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>alexey.grishakov@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lilia.pencheva@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Opportunity_with_customisation_Sales_to_Delivery_handover_notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_CSDA_Customisation_Deal_Alert_South_Africa</fullName>
        <ccEmails>veronika.peycheva@experian.com</ccEmails>
        <description>Opportunity with customisation: Sales to Delivery handover notification EMEA South Africa</description>
        <protected>false</protected>
        <recipients>
            <recipient>gwalt@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hanlie.wessels@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lisa.goncalves@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mark.demaine@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>walter.masekoameng@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Opportunity_with_customisation_Sales_to_Delivery_handover_notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_CSDA_Customisation_Deal_Alert_Spain</fullName>
        <ccEmails>veronika.peycheva@experian.com</ccEmails>
        <description>Opportunity with customisation: Sales to Delivery handover notification EMEA Spain</description>
        <protected>false</protected>
        <recipients>
            <recipient>alejandra.bermejo@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>alfredo.lopez@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hugues.harlez@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>isabel.sanz@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mariadelangel.duran@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pablo.ramos@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Opportunity_with_customisation_Sales_to_Delivery_handover_notification</template>
    </alerts>
    <alerts>
        <fullName>EMEA_CSDA_Customisation_Deal_Alert_Turkey_and_Middle_East</fullName>
        <ccEmails>veronika.peycheva@experian.com</ccEmails>
        <description>Opportunity with customisation: Sales to Delivery handover notification EMEA Turkey and Middle East</description>
        <protected>false</protected>
        <recipients>
            <recipient>belin.aktas@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>fatih.bozdogan@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>harun.dogan@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>serter.baltaci@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Opportunity_with_customisation_Sales_to_Delivery_handover_notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>EDQ_Integration_Id</fullName>
        <field>EDQ_Integration_Id__c</field>
        <formula>Id</formula>
        <name>EDQ Integration Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_LI_Set_Send_to_On_Demand_to_true</fullName>
        <description>Will set the Order Line Item&apos;s Order__c.EDQ_Send_to_On_Demand__c to true</description>
        <field>EDQ_Send_to_On_Demand__c</field>
        <literalValue>1</literalValue>
        <name>Order LI Set Send to On Demand to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Order__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serasa_Order_BI_Split</fullName>
        <field>Serasa_BI_Split__c</field>
        <formula>Product__r.Serasa_BI_Split__c</formula>
        <name>Serasa Order BI Split</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serasa_Order_CI_Split</fullName>
        <field>Serasa_CI_Split__c</field>
        <formula>Product__r.Serasa_CI_Split__c</formula>
        <name>Serasa Order CI Split</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serasa_Order_DA_Split</fullName>
        <field>Serasa_DA_Split__c</field>
        <formula>Product__r.Serasa_DA_Split__c</formula>
        <name>Serasa Order DA Split</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serasa_Order_ECS_Split</fullName>
        <field>Serasa_ECS_Split__c</field>
        <formula>Product__r.Serasa_ECS_Split__c</formula>
        <name>Serasa Order ECS Split</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serasa_Order_ID_Split</fullName>
        <field>Serasa_ID_Split__c</field>
        <formula>Product__r.Serasa_ID_Split__c</formula>
        <name>Serasa Order ID Split</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serasa_Order_MS_Split</fullName>
        <field>Serasa_MS_Split__c</field>
        <formula>Product__r.Serasa_MS_Split__c</formula>
        <name>Serasa Order MS Split</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contract_End_Dt_to_Order_Line_End_Dt</fullName>
        <description>Set Contract End Date to Order Lines End Date</description>
        <field>Contract_End_Date__c</field>
        <formula>End_Date__c</formula>
        <name>Set Contract End Dt to Order Line End Dt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Order__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contract_St_Dt_to_OrderLine_Start_Dt</fullName>
        <description>Set Contract St Date to OrderLine Start Date</description>
        <field>Contract_Start_Date__c</field>
        <formula>Start_Date__c</formula>
        <name>Set Contract St Dt to OrderLine Start Dt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Order__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Recalculate_flag_to_false</fullName>
        <field>Recalculate_Sales_Price__c</field>
        <literalValue>0</literalValue>
        <name>Set Recalculate flag to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Product_BL_Field</fullName>
        <field>Product_BL__c</field>
        <formula>Text(Product__r.Business_Line__c)</formula>
        <name>Update Product BL Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Order Line Item On Demand Updates</fullName>
        <actions>
            <name>Order_LI_Set_Send_to_On_Demand_to_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checks if the Order Line Item has an update requiring its Order to be synched by other systems (On Demand)</description>
        <formula>AND (  EDQ_On_Demand_Product__c ,        
OR (           
ISCHANGED( CPQ_Quantity__c ),            
ISCHANGED( Start_Date__c ),             
ISCHANGED( End_Date__c ),             
ISCHANGED( Status__c ),             
ISCHANGED( EDQ_On_Demand_Product__c )        
),        
Order__r.Locked__c = true )     
||      
AND (    
ISNEW(),               
EDQ_On_Demand_Product__c ,                         Order__r.Locked__c = true  ,          
LastModifiedBy.Alias &lt;&gt; &apos;dmigr&apos;     
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Order Serasa BU Percentage Update</fullName>
        <actions>
            <name>Serasa_Order_BI_Split</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Serasa_Order_CI_Split</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Serasa_Order_DA_Split</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Serasa_Order_ECS_Split</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Serasa_Order_ID_Split</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Serasa_Order_MS_Split</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Transactional_Sale__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Contract End Date on Order Line Item if %3C Product End Date</fullName>
        <actions>
            <name>Set_Contract_End_Dt_to_Order_Line_End_Dt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>if the product end date is greater the the contract end date update the contract end date to the product end date</description>
        <formula>OR (  End_Date__c  &gt;  Order__r.Contract_End_Date__c , ISBLANK(Order__r.Contract_End_Date__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Contract Start Date on Order Line Item if %3E Product Start Date</fullName>
        <actions>
            <name>Set_Contract_St_Dt_to_OrderLine_Start_Dt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>if the product start date is less than the contract start date update the contract start date to the product start date</description>
        <formula>OR (  Start_Date__c &lt;  Order__r.Contract_Start_Date__c , ISBLANK(Order__r.Contract_Start_Date__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Recalculate flag to false</fullName>
        <actions>
            <name>Set_Recalculate_flag_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order_Line_Item__c.Recalculate_Sales_Price__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update EDQ Integration Id on new Order Line Items created in SFDC</fullName>
        <actions>
            <name>EDQ_Integration_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order_Line_Item__c.EDQ_Integration_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Product BL</fullName>
        <actions>
            <name>Update_Product_BL_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to pull in Product BL to the order line</description>
        <formula>Product__c != null</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
