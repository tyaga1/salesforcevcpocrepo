<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SaaS_Deployment_Timestamp</fullName>
        <description>Update the SaaS Timestamp on the deployment object for integration with edq.com</description>
        <field>SaaS_Timestamp__c</field>
        <formula>NOW()</formula>
        <name>SaaS Deployment Timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SaaS Deployment Integration</fullName>
        <actions>
            <name>SaaS_Deployment_Timestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rules triggers the deployment integration with edq.com</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)   &amp;&amp;   (   (IsChanged(Active__c) &amp;&amp; Active__c = true &amp;&amp; Account__r.SaaS__c = true)     ||    (IsChanged(Active__c) &amp;&amp; Active__c = false)     ||    (Active__c = true &amp;&amp; Account__r.SaaS__c = true &amp;&amp; IsChanged(Asset_Count__c))     ||    (Active__c = true &amp;&amp; Account__r.SaaS__c = true &amp;&amp;         (IsChanged(Account__c)          ||         IsChanged(Name)          ||       IsChanged(Description__c)      )    ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
