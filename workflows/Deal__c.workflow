<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>M_A_IsSkipValidation_Flag_Clear_Update</fullName>
        <description>M&amp;A</description>
        <field>IsSkipValidation__c</field>
        <literalValue>0</literalValue>
        <name>M&amp;A IsSkipValidation Flag Clear Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Action_Trim_Text</fullName>
        <field>Trim_Last_Action_Text__c</field>
        <formula>TRIM( LEFT(Last_Action__c,10 ))</formula>
        <name>Update Last Action Trim Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Next_Action_Trim_Text</fullName>
        <field>Trim_Next_Action_Text__c</field>
        <formula>TRIM(LEFT(Next_Action__c,10) )</formula>
        <name>Update Next Action Trim Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Deal To Update Text Fields</fullName>
        <actions>
            <name>Update_Last_Action_Trim_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Next_Action_Trim_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( LEN(TRIM( Last_Action__c ))  &gt; 0 , LEN(TRIM( Next_Action__c ))  &gt; 0 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>M%26A IsSkipValidation Flag Clear Update Rule</fullName>
        <actions>
            <name>M_A_IsSkipValidation_Flag_Clear_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Deal__c.IsSkipValidation__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>M&amp;A</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
