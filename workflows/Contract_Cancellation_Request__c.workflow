<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Cancellation_date</fullName>
        <field>Cancellation_Date__c</field>
        <formula>IF(ISPICKVAL(Operation_Type__c ,&apos;Cancel today&apos;),  TODAY() ,  IF(ISPICKVAL(Operation_Type__c ,&apos;Cancel in 30 days&apos;),TODAY() + 30,null))</formula>
        <name>Update Cancellation date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_id</fullName>
        <field>Integration_ID__c</field>
        <formula>Id</formula>
        <name>Update Integration id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Cancellation Date based on Operation Type</fullName>
        <actions>
            <name>Update_Cancellation_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Cancellation Date on Contract Cancellation request Object.</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), ISCHANGED(Operation_Type__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Integration id on Contract Cancellation</fullName>
        <actions>
            <name>Update_Integration_id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Profile.Name != &apos;Experian DQ Administration&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
