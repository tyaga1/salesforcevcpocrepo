<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Disable_Chatter_game</fullName>
        <field>Active__c</field>
        <literalValue>0</literalValue>
        <name>Disable Chatter game</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Inactive_Chatter_Game</fullName>
        <field>Unique_Active_Game_name__c</field>
        <formula>&quot;Inactive/&quot;&amp; TEXT(Game_Region__c) &amp;
TEXT(Business_Line__c) &amp; TEXT(Function__c )&amp;&quot;/&quot;&amp;
TEXT(CreatedDate)</formula>
        <name>Inactive Chatter Game</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Only_one_Active_Chatter_Game</fullName>
        <field>Unique_Active_Game_name__c</field>
        <formula>TEXT(Function__c )&amp;TEXT(Game_Region__c)&amp;
TEXT(Business_Line__c)</formula>
        <name>Only one Active Chatter Game</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Inactive Chatter Game</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Chatter_Game__c.Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Auto-fills the Unique Active Game name to remove the only one active game with the following criteria is set:
Region
Function
Business Line
Region/ Business Line/ Function</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Disable_Chatter_game</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Inactive_Chatter_Game</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Chatter_Game__c.End_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Only one Active Chatter Game</fullName>
        <actions>
            <name>Only_one_Active_Chatter_Game</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Chatter_Game__c.Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Auto-fills the Unique Active Game with the following criteria is set for uniqueness:
1) Function and Region and Business line
2) Function and Region
3) Region and Business line
4) Function and Business Line
5) Function
6) Region
7) Business Line</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
