<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Issue_Creation_Notification</fullName>
        <description>Issue Creation Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>chennour.wright@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Issue_Raised_Notification</template>
    </alerts>
    <rules>
        <fullName>Issue Raised on Work Record</fullName>
        <actions>
            <name>Issue_Creation_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Case: 02072126 when an Issue is raised on the User Story this Rule gets Fired</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
