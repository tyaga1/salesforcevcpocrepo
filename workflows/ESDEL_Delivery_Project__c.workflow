<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ESDEL_Spain_Project_Owner</fullName>
        <description>Spain Delivery Workstream - update project owner to queue</description>
        <field>OwnerId</field>
        <lookupValue>ESDEL_Spain_Delivery_Project_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Spain Project Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ESDEL - Assign new project to Spain Project Queue</fullName>
        <actions>
            <name>ESDEL_Spain_Project_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ESDEL_Delivery_Project__c.ESDEL_Billing_Order_Id__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Spain Delivery Workstream - assign new projects with Order ID to Spain Project Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
