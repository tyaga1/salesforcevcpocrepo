<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>X30_Day_Notification</fullName>
        <description>30 Day Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/X30_Day_Notification</template>
    </alerts>
    <alerts>
        <fullName>X45_Day_Deactivation_Notice</fullName>
        <description>45 Day Deactivation Notice</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/X45_Day_Notification</template>
    </alerts>
    <alerts>
        <fullName>X45_Day_Deactivation_Notice_Manager</fullName>
        <description>45 Day Deactivation Notice - Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_s_Email_for_Notification__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/X45_Day_Notification_Manager</template>
    </alerts>
    <fieldUpdates>
        <fullName>Current_Manager_Changed</fullName>
        <field>Previous_Manager__c</field>
        <formula>PRIORVALUE( Managers_Name__c )</formula>
        <name>Current Manager Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Current_Sales_Team_Changed</fullName>
        <field>Previous_Sales_Team__c</field>
        <formula>PRIORVALUE(Current_Sales_Team__c)</formula>
        <name>Current Sales Team Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Manager_Changed</fullName>
        <field>Date_Manager_Changed__c</field>
        <formula>TODAY()</formula>
        <name>Date Manager Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Sales_Team_Changed</fullName>
        <field>Date_Sales_Team_Changed__c</field>
        <formula>TODAY()</formula>
        <name>Date Sales Team Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deactivation_Date_Stamp</fullName>
        <field>Date_Deactivated__c</field>
        <formula>Today()</formula>
        <name>Deactivation Date Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Downgrade_Flag_Set</fullName>
        <field>Downgraded__c</field>
        <literalValue>1</literalValue>
        <name>Downgrade Flag Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EDQ_Integration_Id</fullName>
        <field>EDQ_Integration_Id__c</field>
        <formula>Id</formula>
        <name>EDQ Integration Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leaver_Additional_Status_update</fullName>
        <field>Additional_Status__c</field>
        <literalValue>Leaver</literalValue>
        <name>Leaver Additional Status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Manager_s_Email_Update</fullName>
        <field>Manager_s_Email_for_Notification__c</field>
        <formula>Managers_Email__c</formula>
        <name>Manager&apos;s Email Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Modified_By</fullName>
        <field>Last_Modified_By_Not_System__c</field>
        <formula>$User.FirstName &amp; &apos; &apos; &amp; $User.LastName</formula>
        <name>Set Last Modified By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Modified_Date</fullName>
        <description>Case 01199320</description>
        <field>Last_Modified_Date_Not_System__c</field>
        <formula>NOW()</formula>
        <name>Set Last Modified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_Business_Unit_Value</fullName>
        <field>Business_Unit_Value__c</field>
        <formula>TEXT(Business_Unit__c)</formula>
        <name>Stamp Business Unit Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Additional_Status</fullName>
        <description>Set the Additional Status to Leaver when a User is deactivated by Okta Provisioning</description>
        <field>Additional_Status__c</field>
        <literalValue>Leaver</literalValue>
        <name>Update Additional Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_On_License_Change</fullName>
        <description>Rule will update the License Change Date whenever license changes on the User</description>
        <field>License_changed__c</field>
        <formula>TODAY()</formula>
        <name>Update Date On License Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Leaver_Date_Okta</fullName>
        <field>Date_Left_Business__c</field>
        <formula>Today()</formula>
        <name>Update Leaver Date (Okta)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_LinkedIn_license_date_added_field</fullName>
        <description>Time stamp TODAY() on Linkedin_Sales_Navigator_date_added__c</description>
        <field>Linkedin_Sales_Navigator_date_added__c</field>
        <formula>TODAY()</formula>
        <name>Update LinkedIn license date added field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_OOO_Status</fullName>
        <description>Updates the OOO status based on return date.</description>
        <field>ChatOOO__Chat_OOO_Enabled__c</field>
        <literalValue>0</literalValue>
        <name>Update OOO Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_User_Name_On_License_Change</fullName>
        <description>Stamp the logged in Users Name when the License change happens on User record.</description>
        <field>License_Changed_By__c</field>
        <formula>$User.FirstName + &quot; &quot;+ &quot; &quot; + $User.LastName</formula>
        <name>Update User Name On License Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X30_Day_Notification_Count</fullName>
        <field>Number_of_30_Day_Warnings__c</field>
        <formula>Number_of_30_Day_Warnings__c + 1</formula>
        <name>30 Day Notification Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X30_Day_Notification_Sent_Date</fullName>
        <field>X30_Day_Warning_Message_Sent__c</field>
        <formula>Today()</formula>
        <name>30 Day Notification Sent Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X45_Day_Deactivation_Notice</fullName>
        <field>X45_Day_Deactivation_Notice__c</field>
        <formula>Today()</formula>
        <name>45 Day Deactivation Notice</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X46_Day_Additional_Status_update</fullName>
        <field>Additional_Status__c</field>
        <literalValue>Disabled due to none usage</literalValue>
        <name>46 Day Additional Status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X46_Day_Deactivation_CheckBox</fullName>
        <description>An Update of the Checkbox on User Object to &quot;False&quot;</description>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>46 Day Deactivation CheckBox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>30 Day Notification</fullName>
        <actions>
            <name>X30_Day_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>X30_Day_Notification_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Days_since_Activity__c</field>
            <operation>greaterOrEqual</operation>
            <value>30</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Days_since_Activity__c</field>
            <operation>lessThan</operation>
            <value>31</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Standard</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.License__c</field>
            <operation>equals</operation>
            <value>Salesforce</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>30 Day Notification Count</fullName>
        <actions>
            <name>X30_Day_Notification_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( X30_Day_Warning_Message_Sent__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>45 Day Deactivation</fullName>
        <actions>
            <name>X45_Day_Deactivation_Notice</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>X45_Day_Deactivation_Notice_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>X45_Day_Deactivation_Notice</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Days_since_Activity__c</field>
            <operation>greaterOrEqual</operation>
            <value>45</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Days_since_Activity__c</field>
            <operation>lessOrEqual</operation>
            <value>46</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Standard</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.License__c</field>
            <operation>equals</operation>
            <value>Salesforce</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>46 Day Deactivation Non Sales User</fullName>
        <actions>
            <name>Downgrade_Flag_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>X46_Day_Additional_Status_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Days_since_Activity__c</field>
            <operation>greaterOrEqual</operation>
            <value>46</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notContain</operation>
            <value>Chatter,API,Aria,Director,Sales Manager,Sales Executive,System Administrator,Read Only,Experian Certified Sales Administrators,Experian DQ Administration</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.License__c</field>
            <operation>equals</operation>
            <value>Salesforce</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Date_Reactivated__c</field>
            <operation>lessThan</operation>
            <value>LAST 7 DAYS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OOO Chat Auto Disable</fullName>
        <actions>
            <name>Update_OOO_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ChatOOO__Chat_OOO_Enabled__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ChatOOO__Chat_OOO_Until__c</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Uncheck OOO box when return date is met.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Stamp Business Unit Value</fullName>
        <actions>
            <name>Stamp_Business_Unit_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Stamp the BU picklist value when it is modified</description>
        <formula>ISCHANGED(Business_Unit__c) || ISBLANK(Business_Unit_Value__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Track User License Change</fullName>
        <actions>
            <name>Update_Date_On_License_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_User_Name_On_License_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update License Tracking field with last used license and time when License changes</description>
        <formula>ISCHANGED(License__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Additional Status to Leaver on User</fullName>
        <actions>
            <name>Update_Additional_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Leaver_Date_Okta</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the Additional Status to Leaver when a User is deactivated by Okta Provisioning</description>
        <formula>AND(  (ISCHANGED(IsActive)),  PRIORVALUE(IsActive) == true, ( $User.Id == $Label.Okta_Provisioning_User )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Date Deactivated</fullName>
        <actions>
            <name>Deactivation_Date_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.IsActive</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update EDQ Integration Id on new Users created in SFDC</fullName>
        <actions>
            <name>EDQ_Integration_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.EDQ_Integration_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Last Modified Custom Functionality</fullName>
        <actions>
            <name>Set_Last_Modified_By</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Last_Modified_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Case 01199320 - Run only if the user doesn&apos;t have a custom permission</description>
        <formula>NOT($Permission.Bypass_Last_Modified_By)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update LinkedIn Sales Navigator license %28date added%29</fullName>
        <actions>
            <name>Update_LinkedIn_license_date_added_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Linkedin_Sales_Navigator_license__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Linkedin_Sales_Navigator_date_added__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>LinkedIn Sales Navigator license date update</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Manager%27s Email</fullName>
        <actions>
            <name>Manager_s_Email_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Managers_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Updating the second manager&apos;s email field so that it can be used for email alerts</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Previous Manager field</fullName>
        <actions>
            <name>Current_Manager_Changed</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Date_Manager_Changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Current Manager is changed, update Previous Manager field and date changed.</description>
        <formula>ISCHANGED( ManagerId )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Previous Sales Team field</fullName>
        <actions>
            <name>Current_Sales_Team_Changed</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Date_Sales_Team_Changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Sales Team is changed, update Previous Sales Team field and date changed.</description>
        <formula>ISCHANGED( Sales_Team__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>User deactivation on exact leaving date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>User.Date_Left_Business__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Downgrade_Flag_Set</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Leaver_Additional_Status_update</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
