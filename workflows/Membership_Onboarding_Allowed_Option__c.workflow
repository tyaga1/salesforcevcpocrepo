<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Unique_Field_For_Options</fullName>
        <description>Case 01222694 - Append all options to create uniqueness</description>
        <field>Unique__c</field>
        <formula>TEXT(Client_Industry__c) &amp;
TEXT(Product_to_be_Sold__c) &amp; 
TEXT(Permissible_Purpose__c) &amp; 
TEXT(Third_Party_Involvement__c)</formula>
        <name>Set Unique Field For Options</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Unique Option</fullName>
        <actions>
            <name>Set_Unique_Field_For_Options</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Case 01222694 - Sets the unique field to prevent duplication</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
