<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Failure_Email_Alert_to_Deployment_Request_Users</fullName>
        <description>Send Failure Email Alert to Deployment Request Users</description>
        <protected>false</protected>
        <recipients>
            <field>Deployment_Lead__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Release_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Deployment_Request_Failed</template>
    </alerts>
    <alerts>
        <fullName>Send_Success_Email_Alert_to_Deployment_Request_Users</fullName>
        <description>Send Success Email Alert to Deployment Request Users</description>
        <protected>false</protected>
        <recipients>
            <field>Deployment_Lead__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Release_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Deployment_Request_Success</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved_for_Deployment</fullName>
        <field>Status__c</field>
        <literalValue>Approved for Deployment</literalValue>
        <name>Approved for Deployment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Deployment_Request_Status</fullName>
        <field>Status__c</field>
        <literalValue>Locked for Deployment</literalValue>
        <name>Update Deployment Request Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Deployment Failed Email Alert</fullName>
        <actions>
            <name>Send_Failure_Email_Alert_to_Deployment_Request_Users</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>W-005535</description>
        <formula>AND(  ISPICKVAL(Status__c,&apos;Failed&apos;),  ISPICKVAL(PRIORVALUE(Status__c),&apos;Picked Up&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Deployment Success Email Alert</fullName>
        <actions>
            <name>Send_Success_Email_Alert_to_Deployment_Request_Users</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>W-005535</description>
        <formula>AND(  ISPICKVAL(PRIORVALUE(Status__c),&apos;Picked Up&apos;),  CONTAINS(TEXT(Status__c),&apos;Deployed to&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
