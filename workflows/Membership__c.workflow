<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CSDA_AE_MEM_Complete_Notification</fullName>
        <description>CSDA AE MEM Complete Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Membership_Internal_Email_Notification/CSDA_Membership_Request_Completed</template>
    </alerts>
    <alerts>
        <fullName>CSDA_AE_PI_Notification</fullName>
        <description>CSDA AE PI Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Membership_Internal_Email_Notification/Membership_PI_Notification</template>
    </alerts>
    <alerts>
        <fullName>Membership_Approval_Rejected</fullName>
        <description>Membership Approval Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Membership_Internal_Email_Notification/CSDA_Membership_Request_Approval_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Membership_Record_Approved</fullName>
        <description>Membership Record Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Membership_Internal_Email_Notification/CSDA_Membership_Request_Approved</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_Status_to_Approved</fullName>
        <description>Approval status to approved</description>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approval Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_to_Not_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Not Approved</literalValue>
        <name>Approval Status to Not Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_to_Pending</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Approval Status to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSDA_Membership_Date_Time_Opened</fullName>
        <description>Stamp created date on Membership.Date_Time_Opened__c to Now()</description>
        <field>Date_Time_Opened__c</field>
        <formula>Now()</formula>
        <name>CSDA Membership Date/Time Opened</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSDA_Queue_Contract_Attachment</fullName>
        <description>MEM Type equals Contract Attachment, update owner to queue Contract Attachment</description>
        <field>OwnerId</field>
        <lookupValue>CSDA_Contract_Attachment_Request_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>CSDA Queue Contract Attachment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSDA_Queue_Membership_Consultation</fullName>
        <field>OwnerId</field>
        <lookupValue>CSDA_Membership_Consultant_Request</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>CSDA Queue Membership Consultation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSDA_Queue_Oppty_Assessment</fullName>
        <description>CSDA Oppty Assessment RT, update owner to Oppty Assessment queue</description>
        <field>OwnerId</field>
        <lookupValue>CSDA_Opportunity_Assessment_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>CSDA Queue Oppty Assessment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSDA_TMS_Request_Queue</fullName>
        <description>CSDA MEM Type equals New TMS# or Update/Maintenance of existing TMS#, update owner to TMS Request Queue.</description>
        <field>OwnerId</field>
        <lookupValue>CSDA_TMS_Request_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>CSDA TMS Request Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leadership_Approved</fullName>
        <description>Update Status field - Leadership Approved</description>
        <field>Status__c</field>
        <literalValue>Leadership Reviewed</literalValue>
        <name>Leadership Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Membership_Status_equals_Not_Started</fullName>
        <description>All Membership Request - Update Date Assigned if status is set to Not Started</description>
        <field>Date_Time_Submitted__c</field>
        <formula>NOW()</formula>
        <name>Membership Status equals Not Started</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_In_Progress</fullName>
        <description>Update Status field - In Progress</description>
        <field>Status__c</field>
        <literalValue>In Progress</literalValue>
        <name>Status to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Document_Request_Queue_Owner</fullName>
        <description>Update Document Request Owner to Queue</description>
        <field>OwnerId</field>
        <lookupValue>CSDA_Document_Requests</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Document Request Queue Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Membership_Date_Time_Closed</fullName>
        <description>Set Date/Time Closed when Status is set to Complete</description>
        <field>Date_Time_Closed__c</field>
        <formula>NOW()</formula>
        <name>Update Membership Date/Time Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Membership_Owner_to_Membership_Qu</fullName>
        <description>Set Membership Owner to “Membership Queue” when Submit is set to True</description>
        <field>OwnerId</field>
        <lookupValue>CSDA_Opportunity_Membership_Request_Queu</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Membership Owner to Membership Qu</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Membership_Status</fullName>
        <description>Update Membership Status Field to Not Started</description>
        <field>Status__c</field>
        <literalValue>Not Started</literalValue>
        <name>Update Membership Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Packet_Request_Queue_Owner</fullName>
        <description>Update Packet Request Queue Owner</description>
        <field>OwnerId</field>
        <lookupValue>CSDA_Packet_Requests</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Packet Request Queue Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Queue_Owner_Non_Revenue</fullName>
        <description>Update Queue Owner Non-Revenue</description>
        <field>OwnerId</field>
        <lookupValue>CSDA_Membership_Non_Revenue_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Queue Owner Non-Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status</fullName>
        <field>Status__c</field>
        <literalValue>Not Started</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Third_Party_Oaysis_Owner</fullName>
        <description>Update Third Party Oaysis owner to queue</description>
        <field>OwnerId</field>
        <lookupValue>CSDA_Third_Party_Oaysis</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Third Party Oaysis Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CSDA Document Request</fullName>
        <actions>
            <name>Update_Document_Request_Queue_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Membership_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Packet Requests - Update Status and Owner if Submit is checked</description>
        <formula>AND( RecordType.DeveloperName = &apos;Document_Request&apos; ,  Submit__c = true ,  ISCHANGED( Submit__c ) ,  (NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSDA MEM Completed Notification</fullName>
        <actions>
            <name>CSDA_AE_MEM_Complete_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email notification to AE when request is completed</description>
        <formula>ISPICKVAL(Status__c, &apos;Complete&apos;) &amp;&amp;  NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CSDA Membership Date Assigned</fullName>
        <actions>
            <name>Membership_Status_equals_Not_Started</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Membership__c.Status__c</field>
            <operation>equals</operation>
            <value>Not Started</value>
        </criteriaItems>
        <criteriaItems>
            <field>Membership__c.Submit__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update Date Assigned when assigned to Membership</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CSDA Membership PI Notification</fullName>
        <actions>
            <name>CSDA_AE_PI_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify AE when PI equals &quot;Charge My Cost Center&quot;</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), ISPICKVAL( Physical_Fee__c , &apos;Charge My Cost Center&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CSDA Membership Record Created</fullName>
        <actions>
            <name>CSDA_Membership_Date_Time_Opened</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CSDA Membership - Update Date/Time Opened to Created Date</description>
        <formula>AND(DATEVALUE(CreatedDate) =  TODAY() ,         (NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CSDA Membership Request</fullName>
        <actions>
            <name>Update_Membership_Owner_to_Membership_Qu</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Membership_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Membership Requests  - Update Status and Owner if Submit is checked</description>
        <formula>AND(  RecordType.DeveloperName = &apos;Membership_Request&apos; ,       Submit__c  = true ,       ISCHANGED( Submit__c ) ,       (NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSDA Membership Status set to Complete</fullName>
        <actions>
            <name>Update_Membership_Date_Time_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CSDA Membership - Update Date/Time Closed to NOW</description>
        <formula>AND( ISPICKVAL( Status__c , &apos;Complete&apos; ) &amp;&amp;      (NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CSDA Non-Revenue Queue</fullName>
        <actions>
            <name>Update_Membership_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Queue_Owner_Non_Revenue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CSDA Non-Revenue Queue</description>
        <formula>AND ( RecordType.Name = &apos;Membership Non-Revenue&apos; ,       Submit__c = true ,       ISCHANGED( Submit__c ) ,       (NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSDA Packet Request</fullName>
        <actions>
            <name>Update_Membership_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Packet_Request_Queue_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Packet Requests - Update Status and Owner if Submit is checked</description>
        <formula>AND(  RecordType.DeveloperName = &apos;Packet_Requests&apos; ,       Submit__c  = true ,       ISCHANGED( Submit__c ) ,       (NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSDA Queue Contract Attachment</fullName>
        <actions>
            <name>CSDA_Queue_Contract_Attachment</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Membership_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CSDA RT equals Contract Attachment Request, update owner to Contract Attachment queue</description>
        <formula>AND ( RecordType.Name = &apos;Contract Attachment Request&apos; ,       Submit__c  = true ,       ISCHANGED(Submit__c) ,       (NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSDA Queue Membership Consultation</fullName>
        <actions>
            <name>CSDA_Queue_Membership_Consultation</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Consultant Requests - Update Status and Owner if Submit is checked</description>
        <formula>AND(  RecordType.DeveloperName = &apos;Membership_Consultant_Request&apos; ,       Submit__c  = true ,       ISCHANGED( Submit__c ) ,       (NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSDA Queue Oppty Assessment</fullName>
        <actions>
            <name>CSDA_Queue_Oppty_Assessment</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Membership_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CSDA Oppty Assessment RT, update owner to Oppty Assessment queue</description>
        <formula>AND( RecordType.Name = &apos;Membership Opportunity Assessment&apos; ,      Submit__c  = true ,      ISCHANGED( Submit__c ) ,      (NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSDA Queue TMS Requests</fullName>
        <actions>
            <name>CSDA_TMS_Request_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Membership_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CSDA MEM RT equals TMS Number Request, update owner to TMS Request Queue.</description>
        <formula>AND( RecordType.Name  = &apos;TMS Number Request&apos; ,      Submit__c = true ,      ISCHANGED( Submit__c ) ,      (NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Third Party Oaysis Requests</fullName>
        <actions>
            <name>Update_Membership_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Third_Party_Oaysis_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Third Party Processor Oaysis - Update status and owner if submit is checked</description>
        <formula>AND( RecordType.DeveloperName = &apos;Third_Party_Processor_Oaysis_Update&apos; ,  Submit__c = true ,  ISCHANGED( Submit__c ) ,  (NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
