<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_to_Activity_Owner_After_2_Working_days_of_Activity_Creation</fullName>
        <description>Email Alert to Activity Owner After 2 Working days of Activity Creation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/X2_Day_Pending_Marketing_Activity_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Assigned_to_when_the_Marketing_Activity_meets_the_Criteria</fullName>
        <description>Email Notification to Assigned to when the  Marketing Activity meets the Criteria.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CBU_Task_New_assignment_notification</template>
    </alerts>
    <alerts>
        <fullName>Marketing_Activity_Activity_to_be_sent_back_to_Marketing_in_3_days</fullName>
        <description>Marketing Activity: Activity to be sent back to Marketing in 3 days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Marketing_Activity_Activity_to_be_sent_back_to_Marketing_in_3_days</template>
    </alerts>
    <alerts>
        <fullName>Marketing_Activity_Rejected_Notification</fullName>
        <description>Marketing Activity: Rejected Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>marketingautomation@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Marketing_Activity_Activity_Rejected_notification</template>
    </alerts>
    <alerts>
        <fullName>Marketing_Activity_Reminder_to_Accept_or_Reject_New_Activity</fullName>
        <description>Marketing Activity: Reminder to Accept or Reject New Activity</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Marketing_Activity_Reminder_to_Accept_or_Reject_New_Activity</template>
    </alerts>
    <alerts>
        <fullName>Marketing_Activity_Reminder_to_Convert_or_Disqualify</fullName>
        <description>Marketing Activity: Reminder to Convert or Disqualify</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Marketing_Activity_Reminder_to_Convert_or_Disqualify_Marketing_Activity</template>
    </alerts>
    <alerts>
        <fullName>NA_MS_Targeting_Marketing_Activity_Notification</fullName>
        <description>NA MS Targeting Marketing Activity Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Marketing_Activity_New_assignment_notification</template>
    </alerts>
    <alerts>
        <fullName>New_Marketing_Activity_Assignment_Notification</fullName>
        <description>New Marketing Activity Assignment Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Marketing_Activity_New_assignment_notification</template>
    </alerts>
    <alerts>
        <fullName>Task_Assignment_Notification</fullName>
        <description>Task Assignment Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CBU_Task_New_assignment_notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Activity_Reassigned_back_to_Marketing</fullName>
        <field>OwnerId</field>
        <lookupValue>marketingautomation@experian.global</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Activity:Reassigned back to Marketing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Overdue_Opp_Alert_Task_Type_Update</fullName>
        <field>Type</field>
        <literalValue>Overdue opportunity alert</literalValue>
        <name>Overdue Opp Alert Task Type Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Completed_Date_stamped</fullName>
        <description>Stamps the closing date in the Completed Date field</description>
        <field>Completed_Date__c</field>
        <formula>NOW()</formula>
        <name>Task Completed Date stamped</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_activity_type</fullName>
        <field>Activity_Type__c</field>
        <formula>TEXT( Type )</formula>
        <name>Update activity type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updates_the_Assigned_To_Field</fullName>
        <field>OwnerId</field>
        <lookupValue>marta.griffin@experian.global</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Updates the Assigned To Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>InsideSales_Quality_Conversations</fullName>
        <apiVersion>39.0</apiVersion>
        <endpointUrl>https://powerstandings.insidesales.com/kpi/ndsanly</endpointUrl>
        <fields>CreatedById</fields>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>insidesales.api@experian.global</integrationUser>
        <name>InsideSales Quality Conversations</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Activity Owner Transfer Notification</fullName>
        <actions>
            <name>New_Marketing_Activity_Assignment_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(RecordType.Name =&apos;Marketing Activity&apos;,ISCHANGED( OwnerId ),  NOT(ISNEW() ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>InsideSales PowerStandings Quality Conversations</fullName>
        <actions>
            <name>InsideSales_Quality_Conversations</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.CallDurationInSeconds</field>
            <operation>greaterThan</operation>
            <value>300</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.qbdialer__is_subdomain__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Marketing Activity Activity to be sent back to Marketing in 3 days Convert or Disqualify</fullName>
        <active>true</active>
        <formula>AND(RecordType.Name =&apos;Marketing Activity&apos;,Owner:User.Profile.Name&lt;&gt;&quot;Experian Marketing Administrator&quot;, NOT(CONTAINS(Owner:User.Profile.Name, &quot;Inside Sales&quot;)), OR( ISPICKVAL(Status, &quot;Not Started&quot;), ISPICKVAL(Status, &quot;Qualifying&quot;),ISPICKVAL(Status, &quot;Sales Accepted&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Marketing_Activity_Activity_to_be_sent_back_to_Marketing_in_3_days</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Marketing_Activity_Reminder_to_Convert_or_Disqualify</name>
                <type>Alert</type>
            </actions>
            <timeLength>28</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Marketing Activity Rejected Activities Needs reassignment back to Marketing</fullName>
        <actions>
            <name>Marketing_Activity_Rejected_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Activity_Reassigned_back_to_Marketing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>Marketing Activity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.CreatedDate</field>
            <operation>equals</operation>
            <value>LAST 30 DAYS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Marketing Activity Reminder to Accept or Reject New Activity</fullName>
        <active>true</active>
        <formula>AND(RecordType.Name =&apos;Marketing Activity&apos;,Owner:User.Profile.Name&lt;&gt;&quot;Experian Marketing Administrator&quot;, NOT(CONTAINS(Owner:User.Profile.Name, &quot;Inside Sales&quot;)), OR( ISPICKVAL(Status, &quot;Not Started&quot;), ISPICKVAL(Status, &quot;Qualifying&quot;) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Marketing_Activity_Reminder_to_Accept_or_Reject_New_Activity</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Overdue Opportunity Alert</fullName>
        <actions>
            <name>Overdue_Opp_Alert_Task_Type_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Action Required: Opportunity 1 Day Overdue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Action Required: Opportunity 3 Days Overdue</value>
        </criteriaItems>
        <description>Set the Task Type field to Overdue opp alert as unavailable through native functionality</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Rule to Send Email 2 Days Of Marketing Activity Creation</fullName>
        <active>true</active>
        <description>This is the workflow rule to fire emails to remind users of assigned Marketing Activity after 2 business days</description>
        <formula>AND( NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) ,  RecordType.Name =&apos;Marketing Activity&apos;, ISPICKVAL(Status, &apos;Not Started&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Alert_to_Activity_Owner_After_2_Working_days_of_Activity_Creation</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Task Assignment for Marketing Activity</fullName>
        <actions>
            <name>Email_Notification_to_Assigned_to_when_the_Marketing_Activity_meets_the_Criteria</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Updates_the_Assigned_To_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CRM2.0:W-005424 This Workflow is used updated the Assigned to field. and sends an email alert</description>
        <formula>AND( OwnerId =$Setup.Activity_Assignment__c.Assignment_User_13__c,  RecordType.Name =&apos;Marketing Activity&apos;, ContactCountry__c !=&apos;USA&apos;,ContactCountry__c !=&apos;Canada&apos;, CreatedById = $Setup.Activity_Assignment__c.Assignment_User_16__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task Completion Date</fullName>
        <actions>
            <name>Task_Completed_Date_stamped</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Stamps the Completed Date field on Task with date of task closure.</description>
        <formula>IsClosed =True</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update activity type</fullName>
        <actions>
            <name>Update_activity_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
