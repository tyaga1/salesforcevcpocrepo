<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Entitlement_Name_for_New_Entitlement</fullName>
        <description>Case 01794099 - Sets the entitlement name to something meaningful, using the account name and the type.</description>
        <field>Name</field>
        <formula>MID(Account.Name, 0, 252-LEN(TEXT(Type))) &amp; &apos; - &apos; &amp; TEXT(Type)</formula>
        <name>Set Entitlement Name for New Entitlement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Entitlement Name for New Entitlements</fullName>
        <actions>
            <name>Set_Entitlement_Name_for_New_Entitlement</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Case 01794099</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
