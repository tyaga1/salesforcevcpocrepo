<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Assignment_Team_Member_Is_Active_Updated</fullName>
        <field>IsActive_Updated__c</field>
        <literalValue>1</literalValue>
        <name>Assignment Team Member Is Active Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assignment_Team_Members_Role_Updated</fullName>
        <field>Assignment_Team_Role_Updated__c</field>
        <literalValue>1</literalValue>
        <name>Assignment Team Members Role Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assignment_Team_Members_User_Updated</fullName>
        <field>User_Updated__c</field>
        <literalValue>1</literalValue>
        <name>Assignment Team Members User Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Assignment Team Member has IsActive updated</fullName>
        <actions>
            <name>Assignment_Team_Member_Is_Active_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates IsActive_Updated field to true, whenever isActive field get updated.</description>
        <formula>ISCHANGED( IsActive__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assignment Team Member has Team Role updated</fullName>
        <actions>
            <name>Assignment_Team_Members_Role_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Assignment_Team_Role_Updated__c to true</description>
        <formula>ISCHANGED( Assignment_Team_Role__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assignment Team Member has User updated</fullName>
        <actions>
            <name>Assignment_Team_Members_User_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set User Updated true on Assignment Team Member</description>
        <formula>ISCHANGED( User__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
