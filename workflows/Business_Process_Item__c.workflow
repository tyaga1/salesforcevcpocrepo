<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Clear_BPI_Completed_Date</fullName>
        <description>Clears the BPI Completed Date</description>
        <field>Completed_Date__c</field>
        <name>Clear BPI Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BPI_Completed_Date_to_Today</fullName>
        <description>Set the Business Process Item Completed Date to today</description>
        <field>Completed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update BPI Completed Date to Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BPI_ready_date_to_today</fullName>
        <description>Update Business Process Item Ready Date to today</description>
        <field>Ready_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update BPI ready date to today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update BPI Completed Date</fullName>
        <actions>
            <name>Update_BPI_Completed_Date_to_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Business Process Item Completed Date when the status is changed to Completed or Cancelled.</description>
        <formula>OR (    AND(      NOT(       ISPICKVAL(PRIORVALUE(Status__c), &quot;Completed&quot;)     ),      ISPICKVAL(Status__c , &quot;Completed&quot;)   ),    AND(      NOT(       ISPICKVAL(PRIORVALUE(Status__c) , &quot;Cancelled&quot;)     ),      ISPICKVAL(Status__c , &quot;Cancelled&quot;)   ) ) &amp;&amp;  !$Setup.IsDataAdmin__c.IsDataAdmin__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update BPI Ready Date</fullName>
        <actions>
            <name>Clear_BPI_Completed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_BPI_ready_date_to_today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the project work item Ready Date when the status is changed to Ready, or if the record is created. Also clear the Active Date and Completed Date.</description>
        <formula>AND(    OR(      ISNEW(),      NOT(       ISPICKVAL(PRIORVALUE(Status__c) , &quot;Ready&quot;)     )   ),   ISPICKVAL(Status__c , &quot;Ready&quot;),   !$Setup.IsDataAdmin__c.IsDataAdmin__c  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
