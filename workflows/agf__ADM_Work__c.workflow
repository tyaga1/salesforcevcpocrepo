<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Business_Analyst_alerted_when_User_Story_is_Ready_for_Demo_QA</fullName>
        <description>Business Analyst alerted when User Story is Ready for Demo QA</description>
        <protected>false</protected>
        <recipients>
            <field>Business_Analyst__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/User_Story_is_Ready_for_Demo_QA</template>
    </alerts>
    <alerts>
        <fullName>Email_GSO_team_when_Solution_field_is_changed_and_Training_Required_is_checked</fullName>
        <ccEmails>GSOtraining@experian.com</ccEmails>
        <description>Email GSO team when Solution field is changed and Training Required is checked.</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/GSO_Team_When_Solution_field_is_updated_on_Work_record</template>
    </alerts>
    <alerts>
        <fullName>UAT_Lead_alerted_when_User_Story_is_Ready_for_UAT</fullName>
        <description>UAT Lead alerted when User Story is Ready for UAT</description>
        <protected>false</protected>
        <recipients>
            <field>UAT_Lead__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/User_Story_is_Ready_for_Demo_QA</template>
    </alerts>
    <alerts>
        <fullName>User_Story_Requirements_Notification</fullName>
        <description>User Story - Requirements Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>chennour.wright@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Requirement_Complete_Notification</template>
    </alerts>
    <alerts>
        <fullName>User_Story_UAT_Signed_Off_Notification</fullName>
        <ccEmails>gsotraining@experian.com</ccEmails>
        <description>User Story - UAT Signed Off Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>chennour.wright@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>geoff.gordon@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lee.glenn@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/UAT_Signed_Off_Story_Ready_for_Training_Mtrl_Prep</template>
    </alerts>
    <fieldUpdates>
        <fullName>Agile_Accel_Remove_Prioritization_flag</fullName>
        <description>When a Story is added to a Sprint Remove from Prioritization View</description>
        <field>agf__Use_Prioritizer__c</field>
        <literalValue>0</literalValue>
        <name>Agile Accel Remove Prioritization flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Training_Status_when_UAT_Signed_off</fullName>
        <description>When Story Status =  UAT Signed Off, set the Training Status to &apos;Training -In Build&apos;</description>
        <field>Training_Status__c</field>
        <literalValue>Training -In Build</literalValue>
        <name>Set Training Status when UAT Signed off</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>agf__Check_Returned_To_Support_Check_Box</fullName>
        <field>agf__Was_Ever_Returned_to_Support__c</field>
        <literalValue>1</literalValue>
        <name>Check Returned To Support Check Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>agf__P4_status</fullName>
        <field>agf__Perforce_Status__c</field>
        <name>P4 status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Business Analyst notification when Ready for QA</fullName>
        <actions>
            <name>Business_Analyst_alerted_when_User_Story_is_Ready_for_Demo_QA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>agf__ADM_Work__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>User Story</value>
        </criteriaItems>
        <criteriaItems>
            <field>agf__ADM_Work__c.agf__Status__c</field>
            <operation>equals</operation>
            <value>Ready for Demo/QA</value>
        </criteriaItems>
        <description>Email alert to notify the work item&apos;s Business Analyst when the item status is changed to Ready for Demo QA.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email GSO Training when Solution field is updated</fullName>
        <actions>
            <name>Email_GSO_team_when_Solution_field_is_changed_and_Training_Required_is_checked</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( Training_Required__c,  ISCHANGED( Solution__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Requirements are complete</fullName>
        <actions>
            <name>User_Story_Requirements_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>agf__ADM_Work__c.Requirements_Complete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Case : 02072126 When the requirements are marked as complete  for User Story</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reset Prioritization Flag when added to Sprint</fullName>
        <actions>
            <name>Agile_Accel_Remove_Prioritization_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reset Include in Prioritization Flag and set Story Status to Scheduled when Story is added to Sprint</description>
        <formula>AND ( ISCHANGED( agf__Sprint__c ), NOT ISBLANK(agf__Sprint__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Training Status when Story Status is UAT SignedOff</fullName>
        <actions>
            <name>User_Story_UAT_Signed_Off_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Training_Status_when_UAT_Signed_off</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>agf__ADM_Work__c.agf__Status__c</field>
            <operation>equals</operation>
            <value>UAT Signed Off</value>
        </criteriaItems>
        <criteriaItems>
            <field>agf__ADM_Work__c.agf__Product_Tag_Name__c</field>
            <operation>equals</operation>
            <value>CRM2.0</value>
        </criteriaItems>
        <description>Set Training Status to &apos;Training In Build&apos; when Story Status is UAT SignedOff</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UAT Lead notification when Ready for UAT</fullName>
        <actions>
            <name>UAT_Lead_alerted_when_User_Story_is_Ready_for_UAT</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>agf__ADM_Work__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>User Story</value>
        </criteriaItems>
        <criteriaItems>
            <field>agf__ADM_Work__c.agf__Status__c</field>
            <operation>equals</operation>
            <value>Ready for UAT</value>
        </criteriaItems>
        <description>Email alert to notify the work item&apos;s UAT Lead when the item status is changed to Ready for Demo QA.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>agf__Clear P4 Status</fullName>
        <actions>
            <name>agf__P4_status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>agf__ADM_Work__c.agf__Resolved__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <description>Clear P4 status when the work record resolved value becomes 1</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
