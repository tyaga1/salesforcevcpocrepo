<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_to_Project_Completed</fullName>
        <description>Alert to Project Completed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Project_Sponsor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/GCS_Project_Completed</template>
    </alerts>
    <alerts>
        <fullName>Email_Serasa_Sales_Support_Team</fullName>
        <ccEmails>Triagemsuporteavendas@br.experian.com</ccEmails>
        <description>Email Serasa Sales Support Team</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Approval_Request_for_Project</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_Project_Completed_to_Opty_Owner</fullName>
        <description>Send Email Project Completed to Opty Owner</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Project_Completed_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_Project_On_Hold_to_Opty_Owner</fullName>
        <description>Send Email Project On Hold to Opty Owner</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Project_On_Hold_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_to_APAC_MS_EDQ_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>APAC_MS_EDQ</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to APAC MS EDQ Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_EMEA_MS_Spain_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>EMEA_MS_Spain</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to EMEA MS Spain Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_NA_MS_EDQ_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>NA_MS_EDQ</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to NA MS EDQ Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Serasa_MS_Production_Delivery</fullName>
        <field>OwnerId</field>
        <lookupValue>Serasa_MS_Production_Delivery</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Serasa MS Production Delivery</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_UK_I_MS_EDQ_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>UK_I_MS_EDQ</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to UK&amp;I MS EDQ Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Project_Estimated_LOE_Days_Recalculation</fullName>
        <description>Will recalculate the Estimated LOE Days value</description>
        <field>Estimated_LOE_Days__c</field>
        <formula>IF( OR(BEGINS( TEXT( Business_Unit__c ) , &apos;APAC&apos;) ,BEGINS( TEXT( Business_Unit__c ) , &apos;UK&amp;I&apos;)),
         Estimated_LOE_Hours__c / 7.5, 
 Estimated_LOE_Hours__c / 8)</formula>
        <name>Project Estimated LOE Days Recalculation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Project_Estimated_LOE_Hour_Recalculation</fullName>
        <description>Will recalculate the Estimated LOE Hours value</description>
        <field>Estimated_LOE_Hours__c</field>
        <formula>IF( OR(BEGINS( TEXT( Business_Unit__c ) , &apos;APAC&apos;) ,BEGINS( TEXT( Business_Unit__c ) , &apos;UK&amp;I&apos;) ),
 Estimated_LOE_Days__c * 7.5, Estimated_LOE_Days__c *8)</formula>
        <name>Project Estimated LOE Hour Recalculation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Project_Update_Finish_Date</fullName>
        <field>Actual_End_Date__c</field>
        <formula>TODAY()</formula>
        <name>Project - Update Finish Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Project_Update_Start_Date</fullName>
        <description>GCS only</description>
        <field>Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>Project - Update Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Original_End_Date</fullName>
        <field>Original_End_Date__c</field>
        <formula>Project_End_Date__c</formula>
        <name>Update Original End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Awaiting_Approval</fullName>
        <field>Status__c</field>
        <literalValue>Awaiting Approval</literalValue>
        <name>Update Status to Awaiting Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_In_Progress</fullName>
        <field>Status__c</field>
        <literalValue>In Progress</literalValue>
        <name>Update Status to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>When_assigned_to_a_User_from_Queue</fullName>
        <description>When assigned to a User from Queue &apos;Serasa MS Production Delivery&apos;, set the status to &apos;Assigned&apos;.</description>
        <field>Status__c</field>
        <literalValue>Assigned</literalValue>
        <name>When assigned to a User from Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>APAC Email Project Completed to Opty Owner</fullName>
        <actions>
            <name>Send_Email_Project_Completed_to_Opty_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>APAC MS CCM A/NZ,APAC MS EDQ,APAC MS Targeting</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project__c.Opportunity_Owner_Email__c</field>
            <operation>notEqual</operation>
            <value>NULL</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>APAC Email Project On Hold to Opty Owner</fullName>
        <actions>
            <name>Send_Email_Project_On_Hold_to_Opty_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>APAC MS Targeting</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project__c.Status__c</field>
            <operation>equals</operation>
            <value>Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project__c.Opportunity_Owner_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Send an email to the opportunity owner when the associated project is put on hold - APAC MS Targeting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign to APAC MS EDQ Queue for Project</fullName>
        <actions>
            <name>Assign_to_APAC_MS_EDQ_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>APAC MS EDQ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project__c.Assign_to_Queue__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>T-311803: Assign queue to same named business unit value, when Assign to Queue = TRUE</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to EMEA MS Spain Queue for Project</fullName>
        <actions>
            <name>Assign_to_EMEA_MS_Spain_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>EMEA MS Spain</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project__c.Assign_to_Queue__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>T-311803: Assign queue to same named business unit value, when Assign to Queue = TRUE</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to NA MS EDQ Queue for Project</fullName>
        <actions>
            <name>Assign_to_NA_MS_EDQ_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>NA MS EDQ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project__c.Assign_to_Queue__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>T-311803: Assign queue to same named business unit value, when Assign to Queue = TRUE</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to Serasa MS Production Delivery</fullName>
        <actions>
            <name>Assign_to_Serasa_MS_Production_Delivery</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Serasa MS</value>
        </criteriaItems>
        <description>Assign to &apos;Serasa MS Production Delivery&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to UK%26I MS EDQ Queue for Project</fullName>
        <actions>
            <name>Assign_to_UK_I_MS_EDQ_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>UK&amp;I MS EDQ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project__c.Assign_to_Queue__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>T-311803: Assign queue to same named business unit value, when Assign to Queue = TRUE</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Finished GCS Project</fullName>
        <actions>
            <name>Alert_to_Project_Completed</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Project_Update_Finish_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Global SFDC Project</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project__c.Actual_End_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Project Sync Estimated LOE Days with Hours</fullName>
        <actions>
            <name>Project_Estimated_LOE_Days_Recalculation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Will update the Estimated LOE Days field given the new value in the Estimated LOE Hours value.</description>
        <formula>ISCHANGED( Estimated_LOE_Hours__c ) ||  (ISNEW() &amp;&amp;  ISBLANK( Estimated_LOE_Days__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Project Sync Estimated LOE Hours with Days</fullName>
        <actions>
            <name>Project_Estimated_LOE_Hour_Recalculation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Will update the Estimated LOE Hours field given the new value in the Estimated LOE Days value.</description>
        <formula>ISCHANGED( Estimated_LOE_Days__c ) ||  (ISNEW()  &amp;&amp;  ISBLANK( Estimated_LOE_Hours__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Start GCS Project</fullName>
        <actions>
            <name>Project_Update_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Global SFDC Project</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project__c.Status__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Project__c.Start_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Original End Date</fullName>
        <actions>
            <name>Update_Original_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>The Original End Date should be copied from Project End Date when project is created.</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>When assigned to a User from Queue</fullName>
        <actions>
            <name>When_assigned_to_a_User_from_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When assigned to a User from Queue &apos;Serasa MS Production Delivery&apos;</description>
        <formula>IF(       AND(            ISCHANGED(OwnerId),            PRIORVALUE(OwnerId) =  $Setup.Custom_Fields_Ids__c.Queue_Serasa_MS_Production_Delivery__c,                NOT(ISNULL(OwnerId)),            BEGINS(OwnerId, &apos;005&apos;)          )      ,true      , false    )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
