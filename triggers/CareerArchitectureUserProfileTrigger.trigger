/**=====================================================================
 * Experian
 * Name: CareerArchitectureUserProfileTrigger
 * Description: Trigger on Career_Architecture_User_Profile__c
 * Created Date: Jul. 19th 2017
 * Created By: James Wills
 * 
=====================================================================*/
trigger CareerArchitectureUserProfileTrigger on Career_Architecture_User_Profile__c (after insert, after update) {
  
  if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false) {

      //After Insert
      if (Trigger.isAfter && Trigger.isInsert) {
        CAUserProfileTriggerHandler.afterInsert(Trigger.new);
      }

      //After Update
      if (Trigger.isAfter && Trigger.isUpdate) {
        CAUserProfileTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
      }

  }
  
}