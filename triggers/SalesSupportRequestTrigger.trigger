/**=====================================================================
 * Experian plc
 * Name: SalesSupportRequestTrigger
 * Description: Trigger on Sales_Support_Request__c
 * Created Date: Dec 5th, 2014
 * Created By: James Weatherall
 * 
 * Date Modified      Modified By                Description of the update
 * Mar 5th, 2015      James Weatherall           Added beforeInsert and beforeUpdate methods
 * Jan 31st, 2017     Sanket Vaidya             Case #02265406: Added afterUpdate support
 =====================================================================*/

trigger SalesSupportRequestTrigger on Sales_Support_Request__c (before insert, before update, after insert, after update) {
    
  //Ensure a data admin is not loading the data, so the triggers will not fire
  if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false &&
                              TriggerState.isActive(Constants.SALES_SUPPORT_REQUEST_TRIGGER)) {
    if (Trigger.isBefore && Trigger.isInsert) {
      SalesSupportRequestTriggerHandler.beforeInsert(Trigger.new);
    }
                                  
    if (Trigger.isBefore && Trigger.isUpdate) {
      SalesSupportRequestTriggerHandler.beforeUpdate(Trigger.newMap, Trigger.oldMap);
    }
                                  
    if (Trigger.isAfter && Trigger.isInsert) {
      SalesSupportRequestTriggerHandler.afterInsert(Trigger.newMap);
    }

    if (Trigger.isAfter && Trigger.isUpdate) {        
      SalesSupportRequestTriggerHandler.afterUpdate(Trigger.newMap);
    }
    
  }
}