/*=============================================================================
 * Experian
 * Name: QuoteTrigger
 * Description: Trigger class for Quote__c
 * Created Date: 14th Jan 2016
 * Created By: James Wills
 *
 * Date Modified      Modified By           Description of the update
 * 14th Jan 2016      James Wills           Case 01059650: Added afterInsert, afterDelete & beforeUpdate
 * 28th Jul 2016      Diego Olarte          CRM2:W-005496: Added afterUpdate
 * 1st Aug, 2016      Paul Kissick          CRM2: Fixed comment header to match dev standards and tidied class
 =============================================================================*/

trigger QuoteTrigger on Quote__c (after insert, before update, after update, after delete) {

  if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false) {

    if (Trigger.isAfter && Trigger.isInsert) {
       QuoteTriggerHandler.afterInsert(Trigger.new);
    }
    if (Trigger.isBefore && Trigger.isUpdate) {
      QuoteTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
    }
    if (Trigger.isAfter && Trigger.isUpdate) {
      QuoteTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
    }
    if (Trigger.isAfter && Trigger.isDelete) {
      QuoteTriggerHandler.afterDelete(Trigger.old);
    }
  }

}