/**=====================================================================
 * Name: FeedCommentTrigger
 * Description: Trigger for FeedComment object                 
 * Created Date: Aug 23th, 2017
 * Created By: Mauricio Murillo
 * 
 * Date Modified                Modified By                  Description of the update
**=====================================================================*/

trigger FeedCommentTrigger on FeedComment (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

     if (TriggerState.isActive(Constants.FEED_COMMENT_TRIGGER)) {
         
        if (Trigger.isBefore && Trigger.isDelete) {
            FeedCommentTriggerHandler.beforeDelete(Trigger.old, IsDataAdmin__c.getInstance().IsDataAdmin__c);
        }
         
     }  
    
}