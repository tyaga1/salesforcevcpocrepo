/**=====================================================================
 * Name: WorkTriggerCustom
 * Description: Update Case Implementation Status based on Work status changes
 * Created Date: Jan 31st, 2016
 * Created By: Sadar Yacob
 * 
 * Date Modified                Modified By                  Description of the update

  =====================================================================*/

trigger WorkTriggerCustom on agf__ADM_Work__c (after update) 
{

    //Ensure a data admin is not loading the data, so the triggers will not fire
    if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false ) 
    { 

        if (Trigger.isAfter)
        {
            if(Trigger.isUpdate &&  !WorkTriggerHandler.isAlreadyUpdated() ) 
            {
                WorkTriggerHandler.afterUpdate(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap);
            }
            //if(Trigger.isInsert) {
               // WorkTriggerHandler.afterInsert(Trigger.new);    
            //} 
        }
    } //END:IF DataAdmin bypass        
}