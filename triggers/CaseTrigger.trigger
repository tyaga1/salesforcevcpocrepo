/**=====================================================================
 * Appirio, Inc
 * Name: CaseTrigger
 * Description: T-208210                 
 * Created Date: Nov 08th, 2013
 * Created By: Mohammed Irfan (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * Dec 16th, 2013               Sadar Yacob                  provide support for After Insert to the trigger
                                                             to add the Case Requestor to the case Team Member 
 * Dec 19th, 2013               MIrfan                       afterInsert handler
 * Jan 30th, 2014               Naresh Kr Ojha(Appirio)      Code alignment and conventions.
 * Jan 30th, 2014               Jinesh Goyal(Appirio)        T-232760: Homogenize the comments
 * Mar 04th, 2014               Arpita Bose(Appiiro)         T-243282: Added Constants in place of String
 * Apr 22nd, 2014               Rahul Jain                   Added beforeUpdate and beforeInsert operation for T-270394
 * Jun 20th, 2014               Richard Joseph               Made is before Insert and Update . 
                                                             To populate the Requestor__c field from Requestor email.Case#1939
 * Aug 28th, 2014               Arpita Bose                  T-309768: Added code from CaseTrigger of the installed package 
 * Sep 04th, 2014               Arpita Bose                  T-310733: Added afterInsert and updated afterUpdate for adding
 *                                                           caseResolutionTimeTrackingProcess()
 * Nov 6th, 2014                Nathalie Le Guay             Fix to call to the future method: onAfterUpdateAsync not to be called during tests
 * Jan 18th, 2016               Paul Kissick                 Case 01663835: Tidied up and removed async call
 * Feb. 29th, 2016              James Wills                  Case 01663835: Tidied up to improve improve code-coverage for CaseTriggerHandler.
 * Jun 9th, 2016                Paul Kissick                 Case 02012558: Optimising the trigger to support isdataadmin = true
 * Nov 18th, 2016               Manoj Gopu                   ITSM: W-006264: Added before delete functionality 
  =====================================================================*/

trigger CaseTrigger on Case (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
  if (TriggerState.isActive(Constants.CASE_TRIGGER)) {
    
    if (Trigger.isBefore && Trigger.isInsert) {
      CaseTriggerHandler.beforeInsert(Trigger.new, IsDataAdmin__c.getInstance().IsDataAdmin__c);
    }
    if (Trigger.isBefore && Trigger.isUpdate) {
      CaseTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap, IsDataAdmin__c.getInstance().IsDataAdmin__c);
    } 
    if (Trigger.isBefore && Trigger.isDelete) {
        CaseTriggerHandler.beforeDelete(Trigger.old, IsDataAdmin__c.getInstance().IsDataAdmin__c);
    }  
    //if (Trigger.isBefore && Trigger.isDelete) {
    //  
    //}
    if (Trigger.isAfter && Trigger.isInsert) {
      CaseTriggerHandler.afterInsert(Trigger.new, IsDataAdmin__c.getInstance().IsDataAdmin__c);
    }
    if (Trigger.isAfter && Trigger.isUpdate) {
      CaseTriggerHandler.afterUpdate(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap, IsDataAdmin__c.getInstance().IsDataAdmin__c);
    }
    //if (Trigger.isAfter && Trigger.isDelete) {
    //  
    //}
    //if (Trigger.isAfter && Trigger.isUndelete) {
    //  
    //}
  }
  
}