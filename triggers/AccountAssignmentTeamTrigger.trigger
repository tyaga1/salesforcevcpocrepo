/**=====================================================================
 * Appirio, Inc
 * Name: AccountAssignmentTeamTrigger
 * Description: T-354803
 * Created Date: Jan 21st, 2015
 * Created By: Arpita Bose (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * 21-Jan-2015                  Noopur                       Added logic for After Delete
 * Aug 17th, 2015               Paul Kissick                 Case #01098427 - Adding handler for before insert
 * May 10th, 2017               James Wills                  InsideSales:W-007994 - Added after update
  =====================================================================*/
trigger AccountAssignmentTeamTrigger on Account_Assignment_Team__c (before insert, after insert, after update, after delete) {
    
  if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false 
                                  && TriggerState.isActive(Constants.ACCOUNT_ASSIGNMENT_TEAM_TRIGGER)) {
      // Before Insert
      if (Trigger.isBefore && Trigger.isInsert) {
        AccountAssignmentTeamTriggerHandler.beforeInsert(Trigger.new);
      }
      // After insert
      if (Trigger.isAfter && Trigger.isInsert) {
        AccountAssignmentTeamTriggerHandler.afterInsert(Trigger.new, Trigger.newMap);
      }
      
      //After Update call
      if (trigger.isAfter && trigger.isUpdate) {
        AccountAssignmentTeamTriggerHandler.afterUpdate (Trigger.newMap);
      }   

      // After delete
      if (trigger.isAfter && trigger.isDelete) {
        AccountAssignmentTeamTriggerHandler.afterDelete(Trigger.oldMap);
      }
      
  }
  
  
}