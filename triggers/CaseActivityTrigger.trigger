/**=====================================================================
 * Experian, Inc
 * Name: CaseActivityTrigger
 * Description: W-007318
 * Created Date: Apr 26th, 2017
 * Created By: Manoj Gopu
 * 
 * Date Modified                Modified By                  Description of the update
   =====================================================================*/

trigger CaseActivityTrigger on Case_Activity__c (before insert, before Update, after insert, after update, after delete, after undelete) {

  // Ensure a data admin is not loading the data, so the triggers will not fire
   if (TriggerState.isActive(Constants.CASE_Activity_TRIGGER)) {
    // Before Insert Call
    if (trigger.isBefore && trigger.isInsert) {
      CaseActivityTriggerHandler.beforeInsert(Trigger.new,IsDataAdmin__c.getInstance().IsDataAdmin__c);
    }
    
    if (Trigger.isBefore && Trigger.isUpdate) {
      CaseActivityTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap, IsDataAdmin__c.getInstance().IsDataAdmin__c);
    }
    // After Insert Call
   /* if (trigger.isAfter && trigger.isInsert) {    
      
    }
    // After Update Call
    if (trigger.isAfter && trigger.isUpdate) {     
   
    }
    // After Delete Call
    if (Trigger.isAfter && Trigger.isDelete) {  
        
    }
    // After UnDelete Call
    if (Trigger.isAfter && Trigger.isUnDelete){
          
    }*/
  }
}