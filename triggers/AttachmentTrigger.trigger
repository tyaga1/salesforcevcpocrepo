/**=====================================================================
 * Experian
 * Name: AttachmentTrigger
 * Description: Attachment Trigger, specifically created for Deal attachments
 * Created Date: Oct 28th, 2015
 * Created By: Paul Kissick 
 * 
 * Date Modified      Modified By                Description of the update
 =====================================================================*/
 
trigger AttachmentTrigger on Attachment (before insert, after delete, after insert, after undelete) {
  
  if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false) {
    if (Trigger.isBefore && Trigger.isInsert) {
      AttachmentTriggerHandler.beforeInsert(Trigger.new);
    }
    if (Trigger.isAfter && Trigger.isInsert) {
      AttachmentTriggerHandler.afterInsert(Trigger.newMap);
    }
    if (Trigger.isAfter && Trigger.isDelete) {
      AttachmentTriggerHandler.afterDelete(Trigger.oldMap);
    }
    if (Trigger.isAfter && Trigger.isUndelete) {
      AttachmentTriggerHandler.afterUndelete(Trigger.newMap);
    }
  } else  {
    if (Trigger.isBefore && Trigger.isInsert) {
      AttachmentTriggerHandler.beforeInsertIsDataAdmin(Trigger.new);
    }
    if (Trigger.isAfter && Trigger.isInsert) {
      AttachmentTriggerHandler.afterInsertIsDataAdmin(Trigger.newMap);
    }
    if (Trigger.isAfter && Trigger.isDelete) {
      AttachmentTriggerHandler.afterDeleteIsDataAdmin(Trigger.oldMap);
    }
    if (Trigger.isAfter && Trigger.isUndelete) {
      AttachmentTriggerHandler.afterUndeleteIsDataAdmin(Trigger.newMap);
    }
  }
}