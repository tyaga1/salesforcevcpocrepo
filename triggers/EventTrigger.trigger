/**=====================================================================
 * Appirio, Inc
 * Name: EventTrigger
 * Description: Trigger on Event - Created for case #1010967
 * Created Date: July 15th, 2015
 * Created By: Paul Kissick
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/ 
trigger EventTrigger on Event (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
  if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false // && 
                                       // TriggerState.isActive(Constants.EVENT_TRIGGER)
                                       ) {
    if (Trigger.isBefore && Trigger.isInsert) {
      EventTriggerHandler.beforeInsert(Trigger.new);
    }
    if (Trigger.isBefore && Trigger.isUpdate) {
      EventTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
    }
  }

}