/**=====================================================================
 * Experian
 * Name: TimecardTrigger
 * Description: Trigger on Timecard__c
 * Created Date: Jun 12th, 2015
 * Created By: Paul Kissick
 * 
 * Date Modified                Modified By                  Description of the update
 * Jan 7th, 2016				Paul Kissick                 Case 01268829 : Adding extra methods for trigger
 =======================================================================*/
 
trigger TimecardTrigger on Timecard__c (before insert, before update, after insert, after delete, after update) {
  if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false) {
    if (Trigger.isBefore && Trigger.isInsert) {
      TimecardTriggerHandler.beforeInsert(Trigger.new);
    }
    if (Trigger.isBefore && Trigger.isUpdate) {
      TimecardTriggerHandler.beforeUpdate(Trigger.oldMap, Trigger.new);
    }
    if (Trigger.isAfter && Trigger.isInsert) {
      TimecardTriggerHandler.afterInsert(Trigger.new);
    }
    if (Trigger.isAfter && Trigger.isUpdate) {
      TimecardTriggerHandler.afterUpdate(Trigger.oldMap, Trigger.new);
    }
    if (Trigger.isAfter && Trigger.isDelete) {
      TimecardTriggerHandler.afterDelete(Trigger.old);
    }
  }
}