/**=====================================================================
 * Name: MetadataComponentTrigger
 * Handler Class: MetadataComponentTriggerHandler  
 * Description: Trigger on Metadata Component object
 * Created Date: June 25th, 2015
 * Created By: Nur Azlini
 * 
 * Date Modified       Modified By                  Description of the update
 * 
 =====================================================================*/

trigger MetadataComponentTrigger on Metadata_Component__c (before insert, before update) {
    // Before insert call
    if(Trigger.isBefore && Trigger.isInsert){
        MetadataComponentTriggerHandler.beforeInsert(trigger.new);
    }
    // Before insert call
    if(Trigger.isBefore && Trigger.isUpdate){
        MetadataComponentTriggerHandler.beforeUpdate(trigger.newMap, Trigger.oldMap);
    }
    
}