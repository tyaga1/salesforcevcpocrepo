/**=====================================================================
 * Experian
 * Name: AccountSegmentTrigger
 * Description: Trigger on Account_Segment__c 
 * This Trigger has been created for the DRM project.  Its initial purpose is to keep six regional Strategic Client fields updated on the Account Object so that they can be passed to the
 * Account_Staging__c table for consumption by the DRM system. The AccountTriggerHandler class will check for inserts, updates and deletes to Account_Segment__c records
 * with a Type__c of 'Region' since these are the only types of records that are relevant to what the Trigger is doing.
 *
 * The AccountSegmentTriggerHandler methods will not be called when the user is an isDataAdmin()
 *
   After Insert :
   After Update :                                 
   After Delete :  
   
 * Created Date: Dec. 8th 2016
 * Created By: James Wills for DRM Project
 * 
=====================================================================*/

trigger AccountSegmentTrigger on Account_Segment__c ( after  insert, after  update, after  delete ) {
                                    
  if (TriggerState.isActive(Constants.ACCOUNT_SEGMENT_TRIGGER)) {
    if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false) {

      //After Insert
      if (Trigger.isAfter && Trigger.isInsert) {
        AccountSegmentTriggerHandler.afterInsert(Trigger.new);
      }

      //After Update
      if (Trigger.isAfter && Trigger.isUpdate) {
        AccountSegmentTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
      }

      // After Delete
      if (Trigger.isAfter && Trigger.isDelete) {
        AccountSegmentTriggerHandler.afterDelete(Trigger.old);
      }
    } else {
      //After Insert
      if (Trigger.isAfter && Trigger.isInsert) {
        AccountSegmentTriggerHandler.afterInsertIsDataAdmin(Trigger.new);
      }

      //After Update
      if (Trigger.isAfter && Trigger.isUpdate) {
        AccountSegmentTriggerHandler.afterUpdateIsDataAdmin(Trigger.new, Trigger.oldMap);
      }
      
      // After Delete
      if (Trigger.isAfter && Trigger.isDelete) {
        AccountSegmentTriggerHandler.afterDeleteIsDataAdmin(Trigger.old);
      }
    
    }
  }
}