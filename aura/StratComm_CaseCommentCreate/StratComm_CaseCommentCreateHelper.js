({
	toggleSpinner: function(cmp) {
        var spinner = cmp.find('spinner');


        if(!$A.util.hasClass(spinner, 'hide-div')){
            $A.util.addClass(spinner, 'hide-div');
         }      
        else {
            $A.util.removeClass(spinner, 'hide-div');
        }
    },

    creatingComment: function(component, helper) {
        var newComment = component.get("v.newComment");
        var action = component.get("c.saveComment");

        action.setParams({ 
            "newComment": newComment,
            "caseId" : component.get("v.recordId")
        });

        $A.enqueueAction(action)

        action.setCallback(this, function(actionResult){
            var returnedInteger = actionResult.getReturnValue();
            
            if(returnedInteger == 1){
                var myEvent = $A.get("e.c:StratComm_CaseCommentCreation_Event");
                //var myEvent = cmp.getEvent("commentListUpdateEvent");
                myEvent.setParams({"newComment": "True"});
                myEvent.fire();
            }
            
            helper.toggleSpinner(component);
        });

        helper.toggleSpinner(component);
        component.set("v.newComment", "");
    },

/*
    addFiles: function(component. helper, files) {
        if (!files) return;

        for (var file of files)
        {

        }
    },
*/
    addFiles: function(component, helper, files) {
        if (!files) return;
//alert('addfiles');
        for (var file of files)
        {
            var reader = new FileReader();
            helper.setupReader(component, helper, file, reader);
        }
    },

    setupReader: function(component, helper, file, reader) {
        //alert('setupreader');
        //var reader = new FileReader();
        reader.onloadend = $A.getCallback(function() {
            var dataURL = reader.result;
            var toConvertToJSON = {
                "parentId": component.get("v.recordId"),
                "name": file.name,
                "body": dataURL.match(/,(.*)$/)[1],
                "contentType": file.type
            }
            var toConvertToJSON = JSON.stringify(toConvertToJSON);
            var appender = component.get("v.attachmentArrayJSON");
            appender += toConvertToJSON;
            component.set("v.attachmentArrayJSON", appender);
            helper.upload(component, helper, file, dataURL.match(/,(.*)$/)[1]);
        });

        reader.readAsDataURL(file);
    },
    
    upload: function(component, helper, file, base64Data) {
//alert('upload');
        var action = component.get("c.saveAttachment"); 
        action.setParams({
            caseId: component.get("v.recordId"),
            fileName: file.name,
            base64Data: base64Data, 
            contentType: file.type
        });

        component.set("v.message", "Uploading...");
        
        $A.enqueueAction(action); 

        action.setCallback(this, function(a) {
            var myEvent = $A.get("e.c:StratComm_CaseAttachmentCreation_Event");
            //var myEvent = cmp.getEvent("commentListUpdateEvent");
            myEvent.fire();

            component.set("v.message", "File uploaded!");
            helper.toggleSpinner(component);
        });

        helper.toggleSpinner(component);
    }

})