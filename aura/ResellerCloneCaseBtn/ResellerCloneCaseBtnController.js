({
	goToURL : function(component, event, helper) {
        var caseID = component.get("v.recordId");
        /*var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": '/casevf?id=' + caseID
                });
                urlEvent.fire();*/

        var action = component.get("c.findRecordType");
        var recordTypeName;

        action.setParams({ caseID : caseID });

        action.setCallback(this, function(response) {
            var state = response.getState();          
            if (state === "SUCCESS") {   
                var recordType = response.getReturnValue();

                if(recordType.includes('BIS'))
                {
                	recordTypeName = 'BIS';
                }
                else if (recordType.includes('CIS'))
                {
                	recordTypeName = 'CIS';
                }

                window.location.href = '../newcase?id=' + caseID + '&type=' + recordTypeName;

            } else {
                console.log("action error");
            }
        });

        $A.enqueueAction(action);
        
        
    }
})