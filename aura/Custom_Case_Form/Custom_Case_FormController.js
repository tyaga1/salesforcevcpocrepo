({
	doInit : function(component, event, helper) {
        
        //component.set("v.RT", 'CSDA CIS Support');
        // the function that reads the url parameters
            var getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };

        var caseType = getUrlParameter('type');
        console.log('casetype: ' + caseType);        
        var caseAPI ='';
        
        if (caseType === 'BIS') {
            caseAPI = 'CSDA BIS Support';
        } else if(caseType === 'CIS'){
            caseAPI = 'CSDA CIS Support';
        } else {
            caseType = 'BIS';
            caseAPI = 'CSDA BIS Support';
        }
        component.set("v.RT", caseAPI);
        console.log('caseAPI 123' + caseAPI);

        var priority =[];
        priority.push({ class: "optionClass", label: 'Service P1 - Critical', value: 'Service P1 - Critical'});
        priority.push({ class: "optionClass", label: 'Service P2 - High', value: 'Service P2 - High'});
        priority.push({ class: "optionClass", label: 'Service P3 - Medium', value: 'Service P3 - Medium'});
        priority.push({ class: "optionClass", label: 'Service P4 - Low', value: 'Service P4 - Low'});

        if (component.find("inputPriority") != null)
        {
            component.find("inputPriority").set("v.options", priority);
        }
         // Type picklist values
        var caseTypeList = [];
        if(caseType === 'BIS'){ 
            caseTypeList.push({ class: "optionClass", label: 'Batch Client Services Request', value: 'Batch Client Services Request'});
            caseTypeList.push({ class: "optionClass", label: 'Billing Request', value: 'Billing Request'});
            caseTypeList.push({ class: "optionClass", label: 'BIQ Online Application Only', value: 'BIQ Online Application Only'});
            caseTypeList.push({ class: "optionClass", label: 'BIQ Request', value: 'BIQ Request'});
            caseTypeList.push({ class: "optionClass", label: 'Cancellation', value: 'Cancellation'});
            caseTypeList.push({ class: "optionClass", label: 'Contract Request', value: 'Contract Request'});
            caseTypeList.push({ class: "optionClass", label: 'Data Request', value: 'Data Request'});
            caseTypeList.push({ class: "optionClass", label: 'DIQ Request', value: 'DIQ Request'});
            caseTypeList.push({ class: "optionClass", label: 'File Research', value: 'File Research'});
            caseTypeList.push({ class: "optionClass", label: 'General Assistance', value: 'General Assistance'});
            caseTypeList.push({ class: "optionClass", label: 'Internet Access', value: 'Internet Access'});
            caseTypeList.push({ class: "optionClass", label: 'NetConnect Implementation', value: 'NetConnect Implementation'});
            caseTypeList.push({ class: "optionClass", label: 'Non-Renewal', value: 'Non-Renewal'});
            caseTypeList.push({ class: "optionClass", label: 'Product Set up', value: 'Product Set up'});
            caseTypeList.push({ class: "optionClass", label: 'Report Request', value: 'Report Request'});
            caseTypeList.push({ class: "optionClass", label: 'Resellers Only', value: 'Resellers Only'});
            caseTypeList.push({ class: "optionClass", label: 'Subcode Maintenance', value: 'Subcode Maintenance'});
            caseTypeList.push({ class: "optionClass", label: 'Technical Issue', value: 'Technical Issue'});
            caseTypeList.push({ class: "optionClass", label: 'Web Services Implementation', value: 'Web Services Implementation'});
            caseTypeList.push({ class: "optionClass", label: 'New User', value: 'New User'});

            if (component.find("inputType") != null)
            {
                component.find("inputType").set("v.options", caseTypeList);
            }
        } else if(caseType === 'CIS'){
            caseTypeList.push({ class: "optionClass", label: 'Account Management', value: 'Account Management'});
            caseTypeList.push({ class: "optionClass", label: 'Acquisition', value: 'Acquisition'});
            caseTypeList.push({ class: "optionClass", label: 'Billing/Invoices', value: 'Billing/Invoices'});
            caseTypeList.push({ class: "optionClass", label: 'Consumer', value: 'Consumer'});
            caseTypeList.push({ class: "optionClass", label: 'Contract Question', value: 'Contract Question'});
            caseTypeList.push({ class: "optionClass", label: 'Data Issues', value: 'Data Issues'});
            caseTypeList.push({ class: "optionClass", label: 'File Research', value: 'File Research'});
            caseTypeList.push({ class: "optionClass", label: 'Fraud Request', value: 'Fraud Request'});
            caseTypeList.push({ class: "optionClass", label: 'Full Cancellation', value: 'Full Cancellation'});
            caseTypeList.push({ class: "optionClass", label: 'Internet Access', value: 'Internet Access'});
            caseTypeList.push({ class: "optionClass", label: 'Item Cancellation', value: 'Item Cancellation'});
            caseTypeList.push({ class: "optionClass", label: 'Name Change', value: 'Name Change'});
            caseTypeList.push({ class: "optionClass", label: 'Ownership Change', value: 'Ownership Change'});
            caseTypeList.push({ class: "optionClass", label: 'Pricing', value: 'Pricing'});
            caseTypeList.push({ class: "optionClass", label: 'Product Compliance', value: 'Product Compliance'});
            caseTypeList.push({ class: "optionClass", label: 'Product Request', value: 'Product Request'});
            caseTypeList.push({ class: "optionClass", label: 'Project', value: 'Project'});
            caseTypeList.push({ class: "optionClass", label: 'Question', value: 'Question'});
            caseTypeList.push({ class: "optionClass", label: 'Reporting', value: 'Reporting'});
            caseTypeList.push({ class: "optionClass", label: 'Sales Support Internal', value: 'Sales Support Internal'});
            caseTypeList.push({ class: "optionClass", label: 'SSO Request', value: 'SSO Request'});
            caseTypeList.push({ class: "optionClass", label: 'Subcode Maintenance', value: 'Subcode Maintenance'});
            caseTypeList.push({ class: "optionClass", label: 'Technical Issue', value: 'Technical Issue'});
            caseTypeList.push({ class: "optionClass", label: 'New User', value: 'New User'});

            if (component.find("inputType") != null)
            {
                component.find("inputType").set("v.options", caseTypeList);
            }
        }


        var caseReasonList = [];
        if(caseType === 'BIS'){
            caseReasonList.push({ class: "optionClass", label: 'Additional Batch Request', value: 'Additional Batch Request'});
            caseReasonList.push({ class: "optionClass", label: 'Additional Service Added', value: 'Additional Service Added'});
            caseReasonList.push({ class: "optionClass", label: 'Address Change', value: 'Address Change'});
            caseReasonList.push({ class: "optionClass", label: 'Address update', value: 'Address update'});
            caseReasonList.push({ class: "optionClass", label: 'Alerts', value: 'Alerts'});
            caseReasonList.push({ class: "optionClass", label: 'API issue', value: 'API issue'});
            caseReasonList.push({ class: "optionClass", label: 'API Request', value: 'API Request'});
            caseReasonList.push({ class: "optionClass", label: 'Archives', value: 'Archives'});
            caseReasonList.push({ class: "optionClass", label: 'Batch Post Delivery Support', value: 'Batch Post Delivery Support'});
            caseReasonList.push({ class: "optionClass", label: 'Billing Address change', value: 'Billing Address change'});
            caseReasonList.push({ class: "optionClass", label: 'Billing research request', value: 'Billing research request'});
            caseReasonList.push({ class: "optionClass", label: 'BIQ Enterprise', value: 'BIQ Enterprise'});
            caseReasonList.push({ class: "optionClass", label: 'BIQ issue', value: 'BIQ issue'});
            caseReasonList.push({ class: "optionClass", label: 'BizConnect', value: 'BizConnect'});
            caseReasonList.push({ class: "optionClass", label: 'BOP Request', value: 'BOP Request'});
            caseReasonList.push({ class: "optionClass", label: 'Client Assistance', value: 'Client Assistance'});
            caseReasonList.push({ class: "optionClass", label: 'Collections Research', value: 'Collections Research'});
            caseReasonList.push({ class: "optionClass", label: 'Commercial Relations Request', value: 'Commercial Relations Request'});
            caseReasonList.push({ class: "optionClass", label: 'Company', value: 'Company'});
            caseReasonList.push({ class: "optionClass", label: 'Contractor Check', value: 'Contractor Check'});
            caseReasonList.push({ class: "optionClass", label: 'CRDB', value: 'CRDB'});
            caseReasonList.push({ class: "optionClass", label: 'Create Additional Subcode', value: 'Create Additional Subcode'});
            caseReasonList.push({ class: "optionClass", label: 'Credit', value: 'Credit'});
            caseReasonList.push({ class: "optionClass", label: 'Debit', value: 'Debit'});
            caseReasonList.push({ class: "optionClass", label: 'Demo Request', value: 'Demo Request'});
            caseReasonList.push({ class: "optionClass", label: 'DIQ Premier Credit Apps', value: 'DIQ Premier Credit Apps'});
            caseReasonList.push({ class: "optionClass", label: 'DNS', value: 'DNS'});
            caseReasonList.push({ class: "optionClass", label: 'Email Client Copy', value: 'Email Client Copy'});
            caseReasonList.push({ class: "optionClass", label: 'End-user reset', value: 'End-user reset'});
            caseReasonList.push({ class: "optionClass", label: 'End-user set-up', value: 'End-user set-up'});
            caseReasonList.push({ class: "optionClass", label: 'Execute', value: 'Execute'});
            caseReasonList.push({ class: "optionClass", label: 'Existing Request', value: 'Existing Request'});
            caseReasonList.push({ class: "optionClass", label: 'Extractions', value: 'Extractions'});
            caseReasonList.push({ class: "optionClass", label: 'File Request', value: 'File Request'});
            caseReasonList.push({ class: "optionClass", label: 'Fusion IQ Request', value: 'Fusion IQ Request'});
            caseReasonList.push({ class: "optionClass", label: 'GDN', value: 'GDN'});
            caseReasonList.push({ class: "optionClass", label: 'GDN Request', value: 'GDN Request'});
            caseReasonList.push({ class: "optionClass", label: 'GFT Request', value: 'GFT Request'});
            caseReasonList.push({ class: "optionClass", label: 'HSD Change', value: 'HSD Change'});
            caseReasonList.push({ class: "optionClass", label: 'HSD Password reset', value: 'HSD Password reset'});
            caseReasonList.push({ class: "optionClass", label: 'Invoice copy', value: 'Invoice copy'});
            caseReasonList.push({ class: "optionClass", label: 'Managed Contract setup', value: 'Managed Contract setup'});
            caseReasonList.push({ class: "optionClass", label: 'Name Change', value: 'Name Change'});
            caseReasonList.push({ class: "optionClass", label: 'Netconnect issue', value: 'Netconnect issue'});
            caseReasonList.push({ class: "optionClass", label: 'New HD/SD Setup', value: 'New HD/SD Setup'});
            caseReasonList.push({ class: "optionClass", label: 'New implementation request', value: 'New implementation request'});
            caseReasonList.push({ class: "optionClass", label: 'New Product Request', value: 'New Product Request'});
            caseReasonList.push({ class: "optionClass", label: 'New Reseller Setup', value: 'New Reseller Setup'});
            caseReasonList.push({ class: "optionClass", label: 'New sub plan billing', value: 'New sub plan billing'});
            caseReasonList.push({ class: "optionClass", label: 'New Test Code', value: 'New Test Code'});
            caseReasonList.push({ class: "optionClass", label: 'Other', value: 'Other'});
            caseReasonList.push({ class: "optionClass", label: 'Other product issue', value: 'Other product issue'});
            caseReasonList.push({ class: "optionClass", label: 'Policy - Implementation', value: 'Policy - Implementation'});
            caseReasonList.push({ class: "optionClass", label: 'Policy - International', value: 'Policy - International'});
            caseReasonList.push({ class: "optionClass", label: 'Policy - Maintenance', value: 'Policy - Maintenance'});
            caseReasonList.push({ class: "optionClass", label: 'Policy - Research', value: 'Policy - Research'});
            caseReasonList.push({ class: "optionClass", label: 'Policy - Test Setup', value: 'Policy - Test Setup'});
            caseReasonList.push({ class: "optionClass", label: 'Portfolio Integration', value: 'Portfolio Integration'});
            caseReasonList.push({ class: "optionClass", label: 'Portfolio Linkage', value: 'Portfolio Linkage'});
            caseReasonList.push({ class: "optionClass", label: 'Port Scoring', value: 'Port Scoring'});
            caseReasonList.push({ class: "optionClass", label: 'Price Change Request', value: 'Price Change Request'});
            caseReasonList.push({ class: "optionClass", label: 'Production Request', value: 'Production Request'});
            caseReasonList.push({ class: "optionClass", label: 'Reports', value: 'Reports'});
            caseReasonList.push({ class: "optionClass", label: 'Reseller - ACS', value: 'Reseller - ACS'});
            caseReasonList.push({ class: "optionClass", label: 'Reseller - BizIQ issue', value: 'Reseller - BizIQ issue'});
            caseReasonList.push({ class: "optionClass", label: 'Reseller - BOP', value: 'Reseller - BOP'});
            caseReasonList.push({ class: "optionClass", label: 'Reseller - demo access', value: 'Reseller - demo access'});
            caseReasonList.push({ class: "optionClass", label: 'Reseller - invoicing', value: 'Reseller - invoicing'});
            caseReasonList.push({ class: "optionClass", label: 'Reseller - pricing', value: 'Reseller - pricing'});
            caseReasonList.push({ class: "optionClass", label: 'Reseller - subcode', value: 'Reseller - subcode'});
            caseReasonList.push({ class: "optionClass", label: 'Sales Assistance', value: 'Sales Assistance'});
            caseReasonList.push({ class: "optionClass", label: 'Score Model Change', value: 'Score Model Change'});
            caseReasonList.push({ class: "optionClass", label: 'Status Change', value: 'Status Change'});
            caseReasonList.push({ class: "optionClass", label: 'STS issue', value: 'STS issue'});
            caseReasonList.push({ class: "optionClass", label: 'STS Request', value: 'STS Request'});
            caseReasonList.push({ class: "optionClass", label: 'Subcode', value: 'Subcode'});
            caseReasonList.push({ class: "optionClass", label: 'Sub plan renewal', value: 'Sub plan renewal'});
            caseReasonList.push({ class: "optionClass", label: 'Test Setup', value: 'Test Setup'});
            caseReasonList.push({ class: "optionClass", label: 'Tier change', value: 'Tier change'});
            caseReasonList.push({ class: "optionClass", label: 'Training Request', value: 'Training Request'});
            caseReasonList.push({ class: "optionClass", label: 'User issue', value: 'User issue'});
            caseReasonList.push({ class: "optionClass", label: 'Volume/Revenue request', value: 'Volume/Revenue request'});
            caseReasonList.push({ class: "optionClass", label: 'Volume report request', value: 'Volume report request'});
            caseReasonList.push({ class: "optionClass", label: 'New User', value: 'New User'});


            if (component.find("inputReason") != null)
            {
                component.find("inputReason").set("v.options", caseReasonList);
            }
        } else if(caseType === 'CIS'){
            caseReasonList.push({ class: "optionClass", label: 'Account Review Advantage', value: 'Account Review Advantage'});
            caseReasonList.push({ class: "optionClass", label: 'Account Review Subcode', value: 'Account Review Subcode'});
            caseReasonList.push({ class: "optionClass", label: 'Additional HD/SD Setup', value: 'Additional HD/SD Setup'});
            caseReasonList.push({ class: "optionClass", label: 'Additional Subcode', value: 'Additional Subcode'});
            caseReasonList.push({ class: "optionClass", label: 'Add Monthly Minimum', value: 'Add Monthly Minimum'});
            caseReasonList.push({ class: "optionClass", label: 'Add Recurring Charge', value: 'Add Recurring Charge'});
            caseReasonList.push({ class: "optionClass", label: 'Address Change', value: 'Address Change'});
            caseReasonList.push({ class: "optionClass", label: 'Authentication Services', value: 'Authentication Services'});
            caseReasonList.push({ class: "optionClass", label: 'Balance Request', value: 'Balance Request'});
            caseReasonList.push({ class: "optionClass", label: 'Bank Mergers', value: 'Bank Mergers'});
            caseReasonList.push({ class: "optionClass", label: 'Base Product', value: 'Base Product'});
            caseReasonList.push({ class: "optionClass", label: 'Batch Pricing', value: 'Batch Pricing'});
            caseReasonList.push({ class: "optionClass", label: 'Billing', value: 'Billing'});
            caseReasonList.push({ class: "optionClass", label: 'Billing Question', value: 'Billing Question'});
            caseReasonList.push({ class: "optionClass", label: 'Branch/Subsidiary Add', value: 'Branch/Subsidiary Add'});
            caseReasonList.push({ class: "optionClass", label: 'Client', value: 'Client'});
            caseReasonList.push({ class: "optionClass", label: 'Collection Advantage', value: 'Collection Advantage'});
            caseReasonList.push({ class: "optionClass", label: 'Collections Triggers', value: 'Collections Triggers'});
            caseReasonList.push({ class: "optionClass", label: 'Company', value: 'Company'});
            caseReasonList.push({ class: "optionClass", label: 'Company Cancellation', value: 'Company Cancellation'});
            caseReasonList.push({ class: "optionClass", label: 'Company ID Maintenance', value: 'Company ID Maintenance'});
            caseReasonList.push({ class: "optionClass", label: 'Company Name Change', value: 'Company Name Change'});
            caseReasonList.push({ class: "optionClass", label: 'Consumer', value: 'Consumer'});
            caseReasonList.push({ class: "optionClass", label: 'Consumer Is A Minor', value: 'Consumer Is A Minor'});
            caseReasonList.push({ class: "optionClass", label: 'Contract Request', value: 'Contract Request'});
            caseReasonList.push({ class: "optionClass", label: 'Create Subcode', value: 'Create Subcode'});
            caseReasonList.push({ class: "optionClass", label: 'Credit', value: 'Credit'});
            caseReasonList.push({ class: "optionClass", label: 'Credit Request', value: 'Credit Request'});
            caseReasonList.push({ class: "optionClass", label: 'Data Issues', value: 'Data Issues'});
            caseReasonList.push({ class: "optionClass", label: 'Data Questions', value: 'Data Questions'});
            caseReasonList.push({ class: "optionClass", label: 'Data Reporting Questions', value: 'Data Reporting Questions'});
            caseReasonList.push({ class: "optionClass", label: 'Data Research', value: 'Data Research'});
            caseReasonList.push({ class: "optionClass", label: 'DBA/FBN', value: 'DBA/FBN'});
            caseReasonList.push({ class: "optionClass", label: 'Debit Request', value: 'Debit Request'});
            caseReasonList.push({ class: "optionClass", label: 'Debt Portfolio Evaluator', value: 'Debt Portfolio Evaluator'});
            caseReasonList.push({ class: "optionClass", label: 'DFMSC Termination – DH', value: 'DFMSC Termination – DH'});
            caseReasonList.push({ class: "optionClass", label: 'E-Oscar', value: 'E-Oscar'});
            caseReasonList.push({ class: "optionClass", label: 'First Sweep', value: 'First Sweep'});
            caseReasonList.push({ class: "optionClass", label: 'Fragmented File', value: 'Fragmented File'});
            caseReasonList.push({ class: "optionClass", label: 'General Questions', value: 'General Questions'});
            caseReasonList.push({ class: "optionClass", label: 'Inquiry Research', value: 'Inquiry Research'});
            caseReasonList.push({ class: "optionClass", label: 'Instant Prescreen', value: 'Instant Prescreen'});
            caseReasonList.push({ class: "optionClass", label: 'Invoice', value: 'Invoice'});
            caseReasonList.push({ class: "optionClass", label: 'Invoice Request', value: 'Invoice Request'});
            caseReasonList.push({ class: "optionClass", label: 'iScreen', value: 'iScreen'});
            caseReasonList.push({ class: "optionClass", label: 'KOB Change', value: 'KOB Change'});
            caseReasonList.push({ class: "optionClass", label: 'Legal Entity Change', value: 'Legal Entity Change'});
            caseReasonList.push({ class: "optionClass", label: 'Metronet', value: 'Metronet'});
            caseReasonList.push({ class: "optionClass", label: 'Mixed File', value: 'Mixed File'});
            caseReasonList.push({ class: "optionClass", label: 'MLA Standalone', value: 'MLA Standalone'});
            caseReasonList.push({ class: "optionClass", label: 'MLA w/ Credit Reports', value: 'MLA w/ Credit Reports'});
            caseReasonList.push({ class: "optionClass", label: 'Multiple', value: 'Multiple'});
            caseReasonList.push({ class: "optionClass", label: 'Name Change', value: 'Name Change'});
            caseReasonList.push({ class: "optionClass", label: 'NetConnect', value: 'NetConnect'});
            caseReasonList.push({ class: "optionClass", label: 'New Central Bill', value: 'New Central Bill'});
            caseReasonList.push({ class: "optionClass", label: 'New HD/SD Setup', value: 'New HD/SD Setup'});
            caseReasonList.push({ class: "optionClass", label: 'Optional Product', value: 'Optional Product'});
            caseReasonList.push({ class: "optionClass", label: 'Ownership Change', value: 'Ownership Change'});
            caseReasonList.push({ class: "optionClass", label: 'Physical', value: 'Physical'});
            caseReasonList.push({ class: "optionClass", label: 'Portfolio Health Check', value: 'Portfolio Health Check'});
            caseReasonList.push({ class: "optionClass", label: 'Portfolio Mgr Score Update', value: 'Portfolio Mgr Score Update'});
            caseReasonList.push({ class: "optionClass", label: 'Precise ID', value: 'Precise ID'});
            caseReasonList.push({ class: "optionClass", label: 'Prequalification', value: 'Prequalification'});
            caseReasonList.push({ class: "optionClass", label: 'Price Change', value: 'Price Change'});
            caseReasonList.push({ class: "optionClass", label: 'Pricing', value: 'Pricing'});
            caseReasonList.push({ class: "optionClass", label: 'Pricing Audit', value: 'Pricing Audit'});
            caseReasonList.push({ class: "optionClass", label: 'Pricing Question', value: 'Pricing Question'});
            caseReasonList.push({ class: "optionClass", label: 'Pricing Research', value: 'Pricing Research'});
            caseReasonList.push({ class: "optionClass", label: 'Product', value: 'Product'});
            caseReasonList.push({ class: "optionClass", label: 'Prospect Triggers', value: 'Prospect Triggers'});
            caseReasonList.push({ class: "optionClass", label: 'Quest', value: 'Quest'});
            caseReasonList.push({ class: "optionClass", label: 'Ratchet Pricing Changes', value: 'Ratchet Pricing Changes'});
            caseReasonList.push({ class: "optionClass", label: 'Recurring Report', value: 'Recurring Report'});
            caseReasonList.push({ class: "optionClass", label: 'Remove Monthly Minimum', value: 'Remove Monthly Minimum'});
            caseReasonList.push({ class: "optionClass", label: 'Remove Recurring Charge', value: 'Remove Recurring Charge'});
            caseReasonList.push({ class: "optionClass", label: 'Rentbureau Data Acquisition', value: 'Rentbureau Data Acquisition'});
            caseReasonList.push({ class: "optionClass", label: 'Return Mail', value: 'Return Mail'});
            caseReasonList.push({ class: "optionClass", label: 'RightOffer', value: 'RightOffer'});
            caseReasonList.push({ class: "optionClass", label: 'Risk & Retention Triggers', value: 'Risk & Retention Triggers'});
            caseReasonList.push({ class: "optionClass", label: 'RRP General Issue', value: 'RRP General Issue'});
            caseReasonList.push({ class: "optionClass", label: 'Sales', value: 'Sales'});
            caseReasonList.push({ class: "optionClass", label: 'Sales Support Meeting', value: 'Sales Support Meeting'});
            caseReasonList.push({ class: "optionClass", label: 'Sales Support Project', value: 'Sales Support Project'});
            caseReasonList.push({ class: "optionClass", label: 'Service Rule Change', value: 'Service Rule Change'});
            caseReasonList.push({ class: "optionClass", label: 'Smart Decision', value: 'Smart Decision'});
            caseReasonList.push({ class: "optionClass", label: 'Special Campaign', value: 'Special Campaign'});
            caseReasonList.push({ class: "optionClass", label: 'SSO Attestation', value: 'SSO Attestation'});
            caseReasonList.push({ class: "optionClass", label: 'Status Change', value: 'Status Change'});
            caseReasonList.push({ class: "optionClass", label: 'STS', value: 'STS'});
            caseReasonList.push({ class: "optionClass", label: 'Subcode', value: 'Subcode'});
            caseReasonList.push({ class: "optionClass", label: 'Subcode Cancellation', value: 'Subcode Cancellation'});
            caseReasonList.push({ class: "optionClass", label: 'Subcode Info', value: 'Subcode Info'});
            caseReasonList.push({ class: "optionClass", label: 'Subcode List', value: 'Subcode List'});
            caseReasonList.push({ class: "optionClass", label: 'Sub Type Change', value: 'Sub Type Change'});
            caseReasonList.push({ class: "optionClass", label: 'Technical Issue', value: 'Technical Issue'});
            caseReasonList.push({ class: "optionClass", label: 'Tenant Screening', value: 'Tenant Screening'});
            caseReasonList.push({ class: "optionClass", label: 'Tradeline Research', value: 'Tradeline Research'});
            caseReasonList.push({ class: "optionClass", label: 'True Trace', value: 'True Trace'});
            caseReasonList.push({ class: "optionClass", label: 'Two Existing Clients', value: 'Two Existing Clients'});
            caseReasonList.push({ class: "optionClass", label: 'User ID Issue', value: 'User ID Issue'});
            caseReasonList.push({ class: "optionClass", label: 'User ID List Request', value: 'User ID List Request'});
            caseReasonList.push({ class: "optionClass", label: 'Volume/Revenue', value: 'Volume/Revenue'});
            caseReasonList.push({ class: "optionClass", label: 'Wealth Insight', value: 'Wealth Insight'});
			caseReasonList.push({ class: "optionClass", label: 'New User', value: 'New User'});

            if (component.find("inputReason") != null)
            {
                component.find("inputReason").set("v.options", caseReasonList);
            }
        }

        
        
        
        // sub code population
        var opts =[];
        opts.push({ class: "optionClass", label: '--None--', value: ''});
		var action2 = component.get("c.getRTSubcode");
        action2.setParams({ RTName : caseAPI });
        console.log('caseAPI' + caseAPI);
        action2.setCallback(this, function(response) {
            console.log("inside callback action2");
            var state = response.getState();            
            if (state === "SUCCESS") {   
                
                var subCodes = response.getReturnValue();
                for (var key in subCodes) {
                    //component.set("v.newCase.RecordtypeId", key);
                    //console.log('key ' + key + ' ' + component.get("v.newCase.RecordtypeId"));
                    component.set("v.subCodes", subCodes[key]);
                    var testSubCodes = subCodes[key];
                    for(var codes in testSubCodes){
                        opts.push({ class: "optionClass", label: testSubCodes[codes], value: testSubCodes[codes]});
                    }
                    component.find("inputSubcode").set("v.options", opts);
                }
                
                
            } else {
                console.log("error");
            }
                
        });
         $A.enqueueAction(action2);
        
        
        
        // clone case
		var caseID = getUrlParameter('id');
            //set the src param value to my src attribute
        if (caseID === undefined) {
            console.log('case id null');
        } else {
            console.log('case id ' + caseID);
            var action = component.get("c.cloneCase");
        action.setParams({ caseID : caseID });        
        action.setCallback(this, function(response) {
            console.log("inside callback");
            var state = response.getState();            
            if (state === "SUCCESS") {   
                var clonedCase = response.getReturnValue();
                clonedCase.sobjectType = 'Case';
                //var fsNames = response.getReturnValue();
                component.set("v.newCase", clonedCase);
                console.log("test " + response.getReturnValue());
            } else {
                console.log("action error");
            }
        });
            $A.enqueueAction(action);
        }
	},
    
    //assign subcode after change
    changeCode : function(component, event, helper) {
        var selected = component.find("inputSubcode").get("v.value");
        component.set("v.newCase.Subcode__c", selected);
        console.log('subcode selected: ' + component.get("v.newCase.Subcode__c"));
    },
    
    //assign subcode after change
    changePriority : function(component, event, helper) {
        var selected = component.find("inputPriority").get("v.value");
        component.set("v.newCase.Priority", selected);
        console.log('subcode selected: ' + component.get("v.newCase.Subcode__c"));
    },
    
    caseCreate : function(component, event, helper) {
        var newCase = component.get("v.newCase");
        console.log("clicked submit " + component.get("v.newCase"));
     	var action = component.get("c.submitCase");
        action.setParams({ newCase : newCase, recordType : component.get("v.RT")});
        action.setCallback(this, function(response) {
            console.log("inside submit action");
            var state = response.getState();   
      
            if (state === "SUCCESS") {  
                var caseID = response.getReturnValue();
                console.log('caseid ' + caseID);
               var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": '/case/' + caseID
                });
                urlEvent.fire();
                
                
            } else {
                console.log("error");
            }
                
        });
         $A.enqueueAction(action);
    }
})