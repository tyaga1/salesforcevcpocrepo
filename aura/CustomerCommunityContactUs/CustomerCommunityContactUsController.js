({
	doInit : function(component, event, helper) {
		 google.charts.load('upcoming', {'packages': ['geochart']});
     google.charts.setOnLoadCallback(drawMarkersMap);

      function drawMarkersMap() {
      var data = google.visualization.arrayToDataTable([
        ['Country',   'ContactText' ],
       ['Australia','Australia : +61 (0)3 8699 0135'],
['China','China : +86 (0)10 59267994'],
['Hong Kong','Hong Kong : +86 (0)10 59267994'],
['India','India : +91 (0)22 6641 9020'],
['Indonesia','Indonesia : +60 (0)38 3215 618'],
['Malaysia','Malaysia : +60 (0)38 3215 618'],
['New Zealand','New Zealand : +61 (0)3 8699 0135'],
['Philippines','Philippines : +60 (0)38 3215 618'],
['Singapore','Singapore : +65 (0)6 5937 592'],
['South Korea','South Korea : +60 (0)38 3215 618'],
['Taiwan','Taiwan : +86 (0)10 59267994'],
['Thailand','Thailand : +66 (0)2 106 7869'],
['Vietnam','Vietnam : +66 (0)2 106 7869'],
['UK','UK : 0844 481 0605 '],
['EMEA - English','EMEA - English : +35 924620881'],
['South Africa','South Africa : 011 513 1202'],
['Turkey','Turkey : +9 021 293 91540'],
['Denmark','Denmark : +45(0)87 46 56 22'],
['Russia','Russia : +7 (495) 981 8416 ext. 188'],
['Italy','Italy : +377 9798 5439'],
['France','France : +377 9798 5486'],
['Spain','Spain : +34 912 902 295'],
['Colombia','Colombia : +57 1 3191519'],
['Chile','Chile : +56 2 28345313'],
['Argentina','Argentina : +54 11 43249890']



      ]);

      var options = {
        sizeAxis: {minSize:2 ,maxSize:2},
        //region: '155', // Western Europe
        displayMode: 'markers',
         defaultColor: '#26478d',
        colorAxis: {colors: '#e7711c',  minValue: '#e7711c', maxValue: '#e7711c'} // orange to blue
      };

      var chart = new google.visualization.GeoChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    };
	}
})