({
    helperMethod : function() {
        
    },

    sortBy : function(key, key2, reverse) {

        // Move smaller items towards the front
        // or back of the array depending on if
        // we want to sort the array in reverse
        // order or not.
        var moveSmaller = reverse ? 1 : -1;

        // Move larger items towards the front
        // or back of the array depending on if
        // we want to sort the array in reverse
        // order or not.
        var moveLarger = reverse ? -1 : 1;

        /**
        * @param  {*} a
        * @param  {*} b
        * @return {Number}
        */
        return function(a, b) {

            var left = a[key];
            var right = b[key];

            if (left == undefined) {
                left = '';
            }
            else {
                left = left.toLowerCase();
            }
            if (right == undefined) {
                right = '';
            }
            else {
                right = right.toLowerCase();
            }


            if (left < right) {
                // console.log('result: ' + a[key] + ' is smaller');
                return moveSmaller;
            }
            else if (left > right) {
                // console.log('result: ' + a[key] + ' is larger');
                return moveLarger;
            }
            else {
                // secondary sorting
                var left2 = a[key2];
                var right2 = b[key2];

                if (left2 == undefined) {
                    left2 = '';
                }
                else {
                    left2 = left2.toLowerCase();
                }
                if (right2 == undefined) {
                    right2 = '';
                }
                else {
                    left2 = left2.toLowerCase();
                }

                if (left2 < right2) {
                    // console.log('result: ' + a[key] + ' is smaller');
                    return 1;
                }
                else if (left2 > right2) {
                    // console.log('result: ' + a[key] + ' is larger');
                    return -1;
                }
                else {
                    return 0;
                }
            }

        };
    },

    toggleSpinner: function(cmp) {
        var spinner = cmp.find('spinner');


        if(!$A.util.hasClass(spinner, 'hide-div')){
            $A.util.addClass(spinner, 'hide-div');
         }      
        else {
            $A.util.removeClass(spinner, 'hide-div');
        }
    }
})