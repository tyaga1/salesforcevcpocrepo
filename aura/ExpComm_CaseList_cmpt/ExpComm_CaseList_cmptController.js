({
    doInit : function(component, event, helper) {
        component.set("v.orderBy", "CreatedDate");
        component.set("v.order", "DESC");
        component.find("CreatedDateDir").set("v.value","▼");
        
        var commu_prefix = component.get("v.communityPrefix");
        if (commu_prefix == undefined || commu_prefix == '') {
            commu_prefix = '/apex';
        }

        var action = component.get("c.findAll");
        action.setParams({"orderBy":component.get("v.orderBy"),"order":component.get("v.order")});

        var objAry = [];

        action.setCallback(this, function(a) {

            var state = a.getState();
            if (component.isValid() && state === "SUCCESS") {
                var respon = a.getReturnValue()

                var ServiceNowUrlTargerSystem = $A.get("$Label.c.ExpComm_ServiceNow_URL_target_system");

                for (var i in respon) {
                    var caseObj = {
                        type: respon[i].CaseOrigin,
                        CaseNumber: respon[i].CaseNumber,
                        Id: respon[i].Id,
                        Status: respon[i].Status,
                        Subject: respon[i].Subject,
                        CreatedDate: respon[i].CreatedDate,
                        LastModifiedDate: respon[i].LastModifiedDate,
                        Link: respon[i].Link
                    }

                    // if (caseObj.type == 'ServiceNow Case') {
                    //     caseObj.Link = 'https://experian' + ServiceNowUrlTargerSystem + '.service-now.com/sp?id=sc_request&table=sc_request&sys_id=' + caseObj.Id;
                    // }
                    // else {
                    //     caseObj.Link = location.protocol + '//' + location.host + commu_prefix + '/ExpComm_viewcase?id=' + caseObj.Id;
                    // }
                    objAry.push(caseObj);
                }

                objAry.sort(helper.sortBy('CreatedDate','CreatedDate',-1));  // <<- default sorting ?

                component.set("v.completeListCases", objAry);
                component.set("v.cases", objAry);


                var recordsPerPage = parseInt(component.get("v.recordsPerPage"));
                component.set("v.currentPageCases", component.get("v.cases").slice( 0, recordsPerPage));

                var totalPageNumber = Math.ceil(component.get("v.cases").length / recordsPerPage);

                component.set("v.lastPageNumber", totalPageNumber);

                
            }
            else if (component.isValid() && state === "ERROR") {
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                }
            }
            
            helper.toggleSpinner(component);
        });

        helper.toggleSpinner(component);

        $A.enqueueAction(action);
    },
    
    // searchKeyChange: function(component, event) {
    //     var searchKey = event.getParam("searchKey");
    //     //var searchType = event.getParam("searchType");
    //     var action = component.get("c.findBySearch");
    //     action.setParams({"searchKey": searchKey,
    //                       "searchType": component.find("searchTypeSelection").get("v.value"),
    //                       "orderBy":component.get("v.orderBy"),
    //                       "order":component.get("v.order")});
    //     action.setCallback(this, function(a) {component.set("v.cases", a.getReturnValue());});
    //     $A.enqueueAction(action);
    // },
    
    SearchKeyFireEvent: function(component, event) {
        var myEvent = $A.get("e.c:CaseListSearchKeyChange");
        myEvent.setParams({"searchKey": event.target.value});
        myEvent.fire();
    },
    
    // sort: function(component, event){


    //     var whichOne = event.getSource().getLocalId();

    //     if(whichOne == component.get("v.orderBy")) {
    //         if(component.get("v.order") == "DESC"){
    //             component.set("v.order","ASC");
    //         }else{
    //             component.set("v.order","DESC");
    //         }
    //     } else{
    //         component.set("v.orderBy",whichOne);
    //         component.set("v.order","DESC");
    //     }

        

    //     var action = component.get("c.findBySearch");
    //     var searchKey = event.getParam("searchKey");
    //     if (searchKey == undefined) {
    //         searchKey = '';
    //     }
        
    //     action.setParams({"searchKey": searchKey,
    //                       "searchType": component.find("searchTypeSelection").get("v.value"),
    //                       "orderBy" : component.get("v.orderBy"),
    //                       "order" : component.get("v.order")
    //                     });


    //     action.setCallback(this, function(response) {

    //         var state = response.getState();

    //         if (component.isValid() && state === "SUCCESS") {
    //             console.log(response.getReturnValue());
    //             component.set("v.cases", response.getReturnValue());
    //         }
    //         else if (component.isValid() && state === "INCOMPLETE") {
    //             // do something
    //         }
    //         else if (component.isValid() && state === "ERROR") {
    //             var errors = response.getError();
    //             if (errors) {
    //                 if (errors[0] && errors[0].message) {
    //                     console.log("Error message: " + 
    //                              errors[0].message);
    //                 }
    //             }
    //         }
    //     });
    //     $A.enqueueAction(action);
    // },
    

    sortLocal: function(component, event, helper){


        var whichOne = event.getSource().getLocalId();

        if(whichOne == component.get("v.orderBy")) {
            if(component.get("v.order") == "DESC"){
                component.set("v.order","ASC");
            }else{
                component.set("v.order","DESC");
            }
        } else{
            component.set("v.orderBy",whichOne);
            component.set("v.order","DESC");
        }

        var objAry = component.get("v.cases");


        if (component.get("v.order") == "DESC") {
            objAry.sort(helper.sortBy(whichOne, 'CreatedDate', -1));
        }
        else {
            objAry.sort(helper.sortBy(whichOne, 'CreatedDate'));
        }

        component.set("v.cases", objAry);
        var recordsPerPage = parseInt(component.get("v.recordsPerPage"));
        component.set("v.currentPageNumber", '1');
        component.set("v.currentPageCases", component.get("v.cases").slice( 0, recordsPerPage));

        
    },


    searchLocal : function(component, event, helper) {
        var searchKey = event.getParam("searchKey");
        var searchType = component.find("searchTypeSelection").get("v.value");


        var objAry = component.get("v.completeListCases");

        var filteredArray = [];

        if (searchKey.length > 0) {
            for (var i in objAry) {
                if (objAry[i][searchType] != undefined && objAry[i][searchType].toLowerCase().includes(searchKey.toLowerCase())) {
                    filteredArray.push(objAry[i]);
                }
            }

            component.set("v.cases", filteredArray);
        }
        else {
            component.set("v.cases", objAry);
        }
        var recordsPerPage = parseInt(component.get("v.recordsPerPage"));
        component.set("v.currentPageNumber", '1');
        component.set("v.currentPageCases", component.get("v.cases").slice( 0, recordsPerPage));
    },

    previousPage : function(component, event, helper) {

        var recordsPerPage = component.get("v.recordsPerPage");
        var currentPage = parseInt(component.get("v.currentPageNumber"));
        var lastPageNum = Math.ceil(component.get("v.cases").length / recordsPerPage);

        if (currentPage > 1) {
            currentPage--;

            component.set("v.currentPageNumber", currentPage);
            component.set("v.currentPageCases", component.get("v.cases").slice( (currentPage-1) * recordsPerPage , currentPage * recordsPerPage));
        }
    },

    nextPage : function(component, event, helper) {
        var recordsPerPage = component.get("v.recordsPerPage");
        var currentPage = parseInt(component.get("v.currentPageNumber"));
        var lastPageNum = Math.ceil(component.get("v.cases").length / recordsPerPage);

        if (currentPage < lastPageNum) {
            currentPage++;

            component.set("v.currentPageNumber", currentPage);
            component.set("v.currentPageCases", component.get("v.cases").slice( (currentPage-1) * recordsPerPage , currentPage * recordsPerPage));
        }
    },

    pageNumberChanged : function(cmp, event) {
        // check input valid
        var enteredPageNumber = cmp.get("v.currentPageNumber");
        var isPositiveInt = /^\+?([1-9]\d*)$/.test(enteredPageNumber);

        if (isPositiveInt == false) {
            cmp.set("v.currentPageNumber", cmp.get("v.lastValidPageNumber"));
            alert('Please enter a valid page number');
        }
        else {

            // Validate current page number within range (currently possible from 1 - infinity)
            var lastPageNum = parseInt(cmp.get("v.lastPageNumber"));
            var recordsPerPage = parseInt(cmp.get("v.recordsPerPage"));


            if (enteredPageNumber >= 1 && parseInt(enteredPageNumber) <= parseInt(lastPageNum)) {
                cmp.set("v.lastValidPageNumber", enteredPageNumber);
                cmp.set("v.currentPageCases", cmp.get("v.cases").slice( (enteredPageNumber-1) * recordsPerPage , enteredPageNumber * recordsPerPage));
            }
            else {
                cmp.set("v.currentPageNumber", cmp.get("v.lastValidPageNumber"));
                alert("Please enter a page number within 1 to " + lastPageNum);
            }
        }
    },

    openSfdcCreateCase : function(component, helper) {
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;
                                 //m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('send', 'event', 'Case List', 'click', 'Raise a Salesforce case');
        var commu_prefix = component.get("v.communityPrefix");
        if (commu_prefix == undefined || commu_prefix == '') {
            commu_prefix = '/apex';
        }
        window.open(commu_prefix + "/expcomm_caseCreate", "_self");
    },

    openServiceNowCreateCase : function () {
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;
                                 //a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('send', 'event', 'Case List', 'click', 'Raise an IT Service Catalog Request');
        window.open("https://experian.service-now.com/nav_to.do?uri=%2Fcatalog_home.do%3Fsysparm_view%3Dcatalog_default","_black");
    }

})