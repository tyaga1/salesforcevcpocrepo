({
	getUserAgreementFlag : function(component) {

		var action = component.get("c.getUserAgreementAlertFlag");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (component.isValid()) {

				if (state === "SUCCESS") {
					component.set("v.showUserAgreement", !response.getReturnValue());
				}
				else {
					component.set("v.showUserAgreement", true);
				}				
			}
		});
		$A.enqueueAction(action);
	},

	setUserAgreementFlag : function(component) {

		// Hide the user agreement before the callback because
		// we assume the update will succeed. 
		component.set("v.showUserAgreement");

		var action = component.get("c.setUserAgreementAlertFlag");
		action.setParams({ "value" : true });
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (component.isValid()) {

				if (state === "SUCCESS") {
					console.log("Retrieved flag: " + response.getReturnValue());
				}
				else {
					console.log("Failed to retrieve flag: " + response.getReturnValue());
				}
			}
		});
		$A.enqueueAction(action);
	},

})