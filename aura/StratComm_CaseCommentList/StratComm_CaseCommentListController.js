({
    doInit : function(component, event, helper) {
        if (component.get("v.recordId") == null || component.get("v.recordId") == '' )
        {
            return;
        }

        var checkCaseStatus = component.get("c.checkIfCaseIsClosed");
        checkCaseStatus.setParams({"caseID":component.get("v.recordId")});

        var action = component.get("c.findCaseComments");
        action.setParams({"caseID":component.get("v.recordId")});

        $A.enqueueAction(checkCaseStatus);
        $A.enqueueAction(action);

        var objAry = [];

        checkCaseStatus.setCallback(this, function(a){
            var state = a.getState();
            if (component.isValid() && state === "SUCCESS") {
                var respon = a.getReturnValue();

                component.set("v.closedCase", respon);
            }
        });

        action.setCallback(this, function(a){
            var state = a.getState();
            if (component.isValid() && state === "SUCCESS") {
                var respon = a.getReturnValue();

                for (var i in respon) {
                    var caseCommentObj = {
                        CommentBody: respon[i].CommentBody,
                        CreatorName: respon[i].CreatorName,
                        CreatedDate: respon[i].CreatedDate,
                        UserLink: respon[i].UserLink
                    }
                    objAry.push(caseCommentObj);
                }

                component.set("v.caseCommentList", objAry);
            }
            else if (component.isValid() && state === "ERROR") {
                alert('error');
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                }
            }
        });
    }
})