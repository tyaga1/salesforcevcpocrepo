({
    navigateBack : function(component){
        var transferData = JSON.parse($.base64.decode(window.location.hash.substring(1, window.location.hash.length)));
        var dataFromMainPage = transferData.dataFromMainPage;
        var mainPageDataJSON = JSON.parse(dataFromMainPage);
        var backURL = mainPageDataJSON.backUrl;
        var evt = $A.get("e.force:navigateToURL");
        evt.setParams({
            url: backURL,
            isredirect: true
        });
        evt.fire();
    }
})