({
    navigateBack : function(component){
        var dataFromMainPage = component.get('v.dataFromMainPage');
        var mainPageDataJSON = JSON.parse(dataFromMainPage);
        var backURL = mainPageDataJSON.backUrl;
        var evt = $A.get("e.force:navigateToURL");
        evt.setParams({
            url: backURL,
            isredirect: true
        });
        evt.fire();
    }
})