({
	doInit : function(component, event, helper) {
        // the function that reads the url parameters
            var getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };
        var priority =[];
        priority.push({ class: "optionClass", label: 'Service P1 - Critical', value: 'Service P1 - Critical'});
        priority.push({ class: "optionClass", label: 'Service P2 - High', value: 'Service P2 - High'});
        priority.push({ class: "optionClass", label: 'Service P3 - Medium', value: 'Service P3 - Medium'});
        priority.push({ class: "optionClass", label: 'Service P4 - Low', value: 'Service P4 - Low'});
        component.find("inputPriority").set("v.options", priority);
        
        var caseType = getUrlParameter('type');
        console.log('casetype: ' + caseType);        
        var caseAPI ='';
        
        if (caseType === 'BIS') {
            caseAPI = 'CSDA BIS Support';
        } else if(caseType === 'CIS'){
            caseAPI = 'CSDA CIS Support';
        } else if (caseType === undefined) {
            caseType = 'BIS';
            caseAPI = 'CSDA BIS Support';
        } else {
            caseType = 'BIS';
            caseAPI = 'CSDA BIS Support';
        }
        component.set("v.RT", caseAPI);
        console.log('caseAPI 123' + caseAPI);
        
        // sub code population
        var opts =[];
        opts.push({ class: "optionClass", label: '--None--', value: ''});
		var action2 = component.get("c.getRTSubcode");
        action2.setParams({ RTName : caseAPI });
        console.log('caseAPI' + caseAPI);
        action2.setCallback(this, function(response) {
            console.log("inside callback action2");
            var state = response.getState();            
            if (state === "SUCCESS") {   
                
                var subCodes = response.getReturnValue();
                for (var key in subCodes) {
                    //component.set("v.newCase.RecordtypeId", key);
                    //console.log('key ' + key + ' ' + component.get("v.newCase.RecordtypeId"));
                    component.set("v.subCodes", subCodes[key]);
                    var testSubCodes = subCodes[key];
                    for(var codes in testSubCodes){
                        opts.push({ class: "optionClass", label: testSubCodes[codes], value: testSubCodes[codes]});
                    }
                    component.find("inputSubcode").set("v.options", opts);
                }
                
                
            } else {
                console.log("error");
            }
                
        });
         $A.enqueueAction(action2);
        
        // clone case
		var caseID = getUrlParameter('id');
            //set the src param value to my src attribute
        if (caseID === undefined) {
            console.log('case id null');
        } else {
            console.log('case id ' + caseID);
            var action = component.get("c.cloneCase");
        action.setParams({ caseID : caseID });        
        action.setCallback(this, function(response) {
            console.log("inside callback");
            var state = response.getState();            
            if (state === "SUCCESS") {   
                var clonedCase = response.getReturnValue();
                clonedCase.sobjectType = 'Case';
                //var fsNames = response.getReturnValue();
                component.set("v.newCase", clonedCase);
                console.log("test " + response.getReturnValue());
            } else {
                console.log("action error");
            }
        });
            $A.enqueueAction(action);
        }
	},
    
    //assign subcode after change
    changeCode : function(component, event, helper) {
        var selected = component.find("inputSubcode").get("v.value");
        component.set("v.newCase.Subcode__c", selected);
        console.log('subcode selected: ' + component.get("v.newCase.Subcode__c"));
    },
    
    //assign subcode after change
    changePriority : function(component, event, helper) {
        var selected = component.find("inputPriority").get("v.value");
        component.set("v.newCase.Priority", selected);
        console.log('subcode selected: ' + component.get("v.newCase.Subcode__c"));
    },
    
    caseCreate : function(component, event, helper) {
        var newCase = component.get("v.newCase");
        console.log("clicked submit " + component.get("v.newCase"));
     	var action = component.get("c.submitCase");
        action.setParams({ newCase : newCase, recordType : component.get("v.RT")});
        action.setCallback(this, function(response) {
            console.log("inside submit action");
            var state = response.getState();   
      
            if (state === "SUCCESS") {  
                var caseID = response.getReturnValue();
                console.log('caseid ' + caseID);
               var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": '/case/' + caseID
                });
                urlEvent.fire();
                
                
            } else {
                console.log("error");
            }
                
        });
         $A.enqueueAction(action);
    }
})